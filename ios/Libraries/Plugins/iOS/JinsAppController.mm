//
//  JinsAppController.m
//  Unity-iPhone
//
//  Created by OtaniAtsushi1 on 2017/11/23.
//

#import "JinsAppController.h"
#import "UnityInterface.h"
#import <MEMELib/MEMELib.h>

//jins meme sdk のアプリ設定 https://developers.jins.com/ja/apps/
#define JINS_MEME_APP_CLIENT_ID @"019435665216120"
#define JINS_MEME_CLIENT_SECRET @"84241kociplq89g3mfc06hgqjv5upok1"

#if DEBUG
#define  DNSLog(...)  NSLog(__VA_ARGS__)
#else
#define  DNSLog(...)
#endif


//unity
#define CALL_UNITY_METHOD_NAME(name)   static const char* name = #name;
CALL_UNITY_METHOD_NAME(IsPrepared);
CALL_UNITY_METHOD_NAME(OnPrepared);
CALL_UNITY_METHOD_NAME(OnScanStarted);
CALL_UNITY_METHOD_NAME(OnPeripheralFound);
CALL_UNITY_METHOD_NAME(OnPeripheralConnected);
CALL_UNITY_METHOD_NAME(OnPeripheralDisconnected);
CALL_UNITY_METHOD_NAME(OnErrorOccured);

CALL_UNITY_METHOD_NAME(OnUpdateDere);
CALL_UNITY_METHOD_NAME(OnUpdateKyoro);
CALL_UNITY_METHOD_NAME(OnUpdateRelax);

const char *UnityObject = "JinsBridge";


//util
#define S_TO_C(string) [string cStringUsingEncoding:NSUTF8StringEncoding]



@implementation JinsAppController

#pragma mark life cycle

- (BOOL)application:(UIApplication*)application didFinishLaunchingWithOptions:(NSDictionary*)launchOptions{
    if (![super application:application didFinishLaunchingWithOptions:launchOptions]) {
        return NO;
    }
    
    //set app id / app secret
    [MEMELib setAppClientId:JINS_MEME_APP_CLIENT_ID clientSecret:JINS_MEME_CLIENT_SECRET];
    
    // Auto Connect
    [[MEMELib sharedInstance] setAutoConnect:YES];
    
    // Background fetch
    [[UIApplication sharedApplication] setMinimumBackgroundFetchInterval: UIApplicationBackgroundFetchIntervalMinimum];
    
    self.peripheralsFound = @[].mutableCopy;

    
    [[NSNotificationCenter defaultCenter] addObserver: self selector:@selector(memeAppAuthorized:) name: MEMELibAppAuthorizedNotification object: nil];
    [[NSNotificationCenter defaultCenter] addObserver: self selector:@selector(memePeripheralFound:) name: MEMELibPeripheralFoundNotification object: nil];
    [[NSNotificationCenter defaultCenter] addObserver: self selector:@selector(memePeripheralConnected:) name: MEMELibPeripheralConnectedNotification object: nil];
    [[NSNotificationCenter defaultCenter] addObserver: self selector:@selector(memePeripheralDisconnected:) name: MEMELibPeripheralDisconnetedNotification object: nil];
    [[NSNotificationCenter defaultCenter] addObserver: self selector:@selector(memeRealTimeModeDataReceived:) name: MEMELibRealtimeModeDataReceivedNotification object: nil];
    [[NSNotificationCenter defaultCenter] addObserver: self selector:@selector(notificationReceived:) name: MEMELibCommandResponseNotification object: nil];
    [[NSNotificationCenter defaultCenter] addObserver: self selector:@selector(notificationReceived:) name: MEMELibDeviceInfoUpdatedNotification object: nil];
    [[MEMELib sharedInstance] addObserver: self forKeyPath: @"centralManagerEnabled" options: NSKeyValueObservingOptionNew context:nil];

    return YES;
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    [super applicationDidEnterBackground:application];
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    [super applicationWillEnterForeground:application];
}


#pragma mark status methods

//start scanning
- (void) startScanPeripherals {
    MEMEStatus status = [[MEMELib sharedInstance] startScanningPeripherals];
    [self checkMEMEStatus: status];
    
    UnitySendMessage(UnityObject, OnScanStarted, "OK");
    DNSLog(@"Start scanning MEME Devices...");

}

- (void) stopScanPeripherals {
    MEMEStatus status = [[MEMELib sharedInstance] stopScanningPeripherals];
    [self checkMEMEStatus: status];
    
    DNSLog(@"Stop scanning MEME Devices...");
    
}

//start connecting
- (void) connectPeripheralWithIndex:(int) index
{
    if (index < 0 || index >= self.peripheralsFound.count) {
        NSString* msg = [NSString stringWithFormat:@"peripheral index(%d) is out of bounds. count = %d", index, (int)self.peripheralsFound.count];
        DNSLog(@"%@", msg);
        UnitySendMessage(UnityObject, OnErrorOccured, S_TO_C(msg));
        return;
    }
    
    CBPeripheral *peripheral = [self.peripheralsFound objectAtIndex: index];
    [self connectPeripheral:peripheral];
}
- (void) connectPeripheralWithId:(NSString*) uuid
{
    for (CBPeripheral* p in self.peripheralsFound) {
        if ([[p.identifier UUIDString] isEqualToString:uuid]) {
            [self connectPeripheral:p];
            return;
        }
    }
    
    NSString* msg = [NSString stringWithFormat:@"peripheral id(%@) does not exist.", uuid];
    DNSLog(@"%@", msg);
    UnitySendMessage(UnityObject, OnErrorOccured, S_TO_C(msg));
}
- (void) connectPeripheral:(CBPeripheral*) peripheral
{
    MEMEStatus status = [[MEMELib sharedInstance] connectPeripheral: peripheral];
    [self checkMEMEStatus: status];
    
    DNSLog(@"Start connecting to MEME Device...");

}
- (void) disconnectPeripheral{
    MEMEStatus status = [[MEMELib sharedInstance] disconnectPeripheral];
    [self checkMEMEStatus: status];

    DNSLog(@"Disconnecting to MEME Device...");
}

//start data reporting
- (void) startDataReportPeripheral{
    MEMEStatus status = [[MEMELib sharedInstance] startDataReport];
    [self checkMEMEStatus: status];
    DNSLog(@"DATA start report");
    
    [self resetGame];
}
- (void) stopDataReportPeripheral{
    MEMEStatus status = [[MEMELib sharedInstance] stopDataReport];
    [self checkMEMEStatus: status];
    DNSLog(@"DATA stop report");
}


#pragma mark delegates

// for Notification of Application Authorization
- (void) memeAppAuthorized:(NSNotification *) notification
{
    NSDictionary *userInfo = (NSDictionary *)[notification userInfo];
    MEMEStatus status = (MEMEStatus)[userInfo[MEMELibStatusCodeUserInfoKey] intValue];
    
    [self checkMEMEStatus: status];
    
    [self startScanPeripherals]; //認証完了したら、即スキャン開始
}

// for Notification of JINS MEME Found
- (void) memePeripheralFound: (NSNotification *) notification
{
    NSDictionary *userInfo = (NSDictionary *)[notification userInfo];
    CBPeripheral *peripheral = userInfo[MEMELibPeripheralUserInfoKey];
    [self.peripheralsFound addObject: peripheral];
    DNSLog(@"peripheral found %@", [peripheral.identifier UUIDString]);

    UnitySendMessage(UnityObject, OnPeripheralFound, S_TO_C([peripheral.identifier UUIDString]));
    
    //[self selectAndConnectPeripheral:0]; //スキャンできたら、即接続開始
}

// for Notification of JINS MEME Connected
- (void) memePeripheralConnected: (NSNotification *) notification
{
    NSDictionary *userInfo = (NSDictionary *)[notification userInfo];
    CBPeripheral *peripheral = userInfo[MEMELibPeripheralUserInfoKey];
    DNSLog(@"MEME Device Connected! %@", [peripheral.identifier UUIDString]);
    
    UnitySendMessage(UnityObject, OnPeripheralConnected, S_TO_C([peripheral.identifier UUIDString]));
    
    //[self startDataReportPeripheral]; //接続できたら、即レポート開始
}

// for Notification of JINS MEME Disconnected
- (void) memePeripheralDisconnected: (NSNotification *) notification
{
    NSDictionary *userInfo = (NSDictionary *)[notification userInfo];
    CBPeripheral *peripheral = userInfo[MEMELibPeripheralUserInfoKey];
    DNSLog(@"MEME Device Disconnected! %@", [peripheral.identifier UUIDString]);

    UnitySendMessage(UnityObject, OnPeripheralDisconnected, S_TO_C([peripheral.identifier UUIDString]));
}

// for Notification of RealtimeData received
- (void) memeRealTimeModeDataReceived: (NSNotification *) notification
{
    NSDictionary *userInfo = (NSDictionary *)[notification userInfo];
    MEMERealTimeData *data = userInfo[MEMELibRealtimeDataUserInfoKey];
    [self calculate:data];
    
    //DNSLog(@"MEME RealTimeMode Data! %@", [data description]);

}

// for Notification of CommandResponse and DeviceInfoUndated
- (void) notificationReceived: (NSNotification *) notification
{
    if ( notification.name == MEMELibCommandResponseNotification)
    {
        int resCode = (int)[notification.userInfo[MEMELibResponseEventCodeUserInfoKey] integerValue];
        bool result = [notification.userInfo[MEMELibResponseCommandResultUserInfoKey] boolValue];
        DNSLog(@"response code %d, result %d", resCode, result);
    }
    DNSLog(@"%@ %@", notification.name, notification.userInfo.description);
    
}

// for CoreBluetooth manager instance enabled
- (void) observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context
{
    if ([keyPath isEqualToString: @"centralManagerEnabled"]){
        UnitySendMessage(UnityObject, OnPrepared, "OK");

        [self startScanPeripherals];
    }
}


#pragma mark game

- (void) resetGame{
    DNSLog(@"RESET GAME");
    self.kkd = 60;
    self.kyoro = 60;
    self.relax = 60;
    self.relax_m1 = 60;
    self.relax_tmp = 60;
    self.relaxBuf = [@[@70.f, @100.f, @80.f] mutableCopy];
}

- (void) calculate:(MEMERealTimeData*)data{

    //好感度の演算
    if (data.blinkStrength > 40){
        if (self.kkd > 15) {
            self.kkd -= self.kkd / 2.8 - 4;
        }
    }
    else {
        self.kkd += 0.32;
        if (self.kkd > 100) {
            self.kkd = 100;
        }
    }
    
    //キョロキョロ度の演算
    if (data.eyeMoveUp > 0 || data.eyeMoveDown > 0 || data.eyeMoveLeft > 0 || data.eyeMoveRight > 0) {
        if (self.kyoro > 15) {
            self.kyoro -= self.kyoro * (data.eyeMoveUp + data.eyeMoveDown + data.eyeMoveLeft + data.eyeMoveRight + 2) / 12 - 5;
        }
    }
    else {
        self.kyoro += 0.4;
        if (self.kyoro > 100) {
            self.kyoro = 100;
        }
    }
    
    //リラックス値の演算
    if(data.blinkStrength > 40){
        [self.relaxBuf addObject:@((float)data.blinkStrength)];
        //[self.relaxBuf valueForKey:@"@avg.self"];
        [self.relaxBuf removeObjectAtIndex:0];
        
        self.relax_tmp = 100 - sqrt([self variance:self.relaxBuf]) * 2.5;
    }
    self.relax
    = ((self.relax_tmp - self.relax_m1) >  0.3) ? (self.relax_m1 + 0.3)
    : ((self.relax_tmp - self.relax_m1) < -0.3) ? (self.relax_m1 - 0.3)
    :   self.relax_tmp;
    self.relax = fmin(100.f, fmax(0.f, self.relax));
    self.relax_m1 = self.relax;

    DNSLog(@"blinkStrength=%d [kkd=%f, relax=%f]", data.blinkStrength, self.kkd, self.relax);
    DNSLog(@"eyeMove=(%d, %d, %d, %d) [kyoro=%f]", data.eyeMoveUp, data.eyeMoveRight, data.eyeMoveDown, data.eyeMoveLeft, self.kyoro);

    [self sendDataToUnity:data];
}

- (double) average:(NSArray*) data{
    if (!data || data.count == 0){
        return 0.0;
    }
    
    double avg = 0;
    for (int i = 0; i < data.count; ++i) {
        avg += [data[i] doubleValue];
    }
    return avg / data.count;
}

- (double) variance:(NSArray*) data{
    
    double ave = [self average:data];
    
    double varia = 0;
    for (int i = 0; i < data.count; ++i) {
        varia = varia + pow([data[i] doubleValue] - ave, 2);
    }
    return (varia / data.count);
}

- (void) sendDataToUnity:(MEMERealTimeData*)data{
    
    //Unityへデータを送信
    UnitySendMessage(UnityObject, OnUpdateDere,  [self getCharFromInt:MIN(100, self.kkd   * 1.0f)]);
    UnitySendMessage(UnityObject, OnUpdateKyoro, [self getCharFromInt:MIN(100, self.kyoro * 1.3f)]);
    UnitySendMessage(UnityObject, OnUpdateRelax, [self getCharFromInt:MIN(100, self.relax * 1.2f)]);
}


#pragma mark utility

//check and show error if needed
- (void) checkMEMEStatus: (MEMEStatus) status
{
    if (status == MEME_ERROR_APP_AUTH){
        [[[UIAlertView alloc] initWithTitle: @"App Auth Failed" message: @"Invalid Application ID or Client Secret " delegate: nil cancelButtonTitle: nil otherButtonTitles: @"OK", nil] show];
    } else if (status == MEME_ERROR_SDK_AUTH){
        [[[UIAlertView alloc] initWithTitle: @"SDK Auth Failed" message: @"Invalid SDK. Please update to the latest SDK." delegate: nil cancelButtonTitle: nil otherButtonTitles: @"OK", nil] show];
    } else if (status == MEME_ERROR_CONNECTION){
        //[[[UIAlertView alloc] initWithTitle: @"Connection Failed" message: @"cannot Connect to peripheral. Please confirm peripheral is power on." delegate: nil cancelButtonTitle: nil otherButtonTitles: @"OK", nil] show];
    } else if (status == MEME_DEVICE_INVALID){
        [[[UIAlertView alloc] initWithTitle: @"Device Invalid" message: @"Invalid Device. Please check peripheral is correct." delegate: nil cancelButtonTitle: nil otherButtonTitles: @"OK", nil] show];
    } else if (status == MEME_CMD_INVALID){
        [[[UIAlertView alloc] initWithTitle: @"Command Invalid" message: @"Invalid Command. Please restart the Game." delegate: nil cancelButtonTitle: nil otherButtonTitles: @"OK", nil] show];
    } else if (status == MEME_ERROR_FW_CHECK){
        [[[UIAlertView alloc] initWithTitle: @"Framework Error" message: @"Framework version error. Please update framework." delegate: nil cancelButtonTitle: nil otherButtonTitles: @"OK", nil] show];
    } else if (status == MEME_ERROR_BL_OFF){
        [[[UIAlertView alloc] initWithTitle: @"Bluetooth Error" message: @"Bluetooth is Off. Please check your device's settings." delegate: nil cancelButtonTitle: nil otherButtonTitles: @"OK", nil] show];
    } else if (status == MEME_ERROR){
        [[[UIAlertView alloc] initWithTitle: @"Error" message: @"misc error. Please restart the Game." delegate: nil cancelButtonTitle: nil otherButtonTitles: @"OK", nil] show];
    } else if (status == MEME_OK){
        DNSLog(@"Status: MEME_OK");
    }
}

- (const char*) getCharFromInt:(int)value{
    return [[NSString stringWithFormat:@"%d", value] cStringUsingEncoding:NSUTF8StringEncoding];
}


@end

IMPL_APP_CONTROLLER_SUBCLASS(JinsAppController)


