﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "object-internals.h"

// System.Action
struct Action_t3226471752;
// Utage.AdvEngineStarter
struct AdvEngineStarter_t330044300;
// Utage.AdvEngineStarter/<LoadEngineAsyncSub>c__Iterator2
struct U3CLoadEngineAsyncSubU3Ec__Iterator2_t1722329282;
// System.String
struct String_t;
// Utage.AssetFile
struct AssetFile_t3383013256;
// Utage.AdvChapterData
struct AdvChapterData_t685691324;
// Utage.AdvEngine
struct AdvEngine_t1176753927;
// SampleChapterTitle/<LoadChaptersAsync>c__Iterator0
struct U3CLoadChaptersAsyncU3Ec__Iterator0_t3084809847;
// SampleChapterTitle
struct SampleChapterTitle_t998764237;
// SampleChapterTitle/<LoadChaptersAsync>c__Iterator0/<LoadChaptersAsync>c__AnonStorey1
struct U3CLoadChaptersAsyncU3Ec__AnonStorey1_t2271407108;
// TransitionScript
struct TransitionScript_t3831648224;
// UtageRecieveMessageFromAdvComannd
struct UtageRecieveMessageFromAdvComannd_t4227868061;
// Utage.AdvBacklog
struct AdvBacklog_t3927301290;
// SampleChatLogItem
struct SampleChatLogItem_t3744651319;
// SampleLoadError
struct SampleLoadError_t2227685100;
// Utage.AdvSaveData
struct AdvSaveData_t4059487466;
// Utage.AdvCommandSendMessageByName
struct AdvCommandSendMessageByName_t3179247017;
// Utage.AdvEngineStarter/<LoadEngineAsyncSub>c__Iterator2/<LoadEngineAsyncSub>c__AnonStorey8
struct U3CLoadEngineAsyncSubU3Ec__AnonStorey8_t684401054;
// Utage.SoundManager
struct SoundManager_t1265124084;
// UnityChan.AutoBlinkforSD
struct AutoBlinkforSD_t2101017347;
// FadeManager
struct FadeManager_t2205509615;
// Utage.SampleCustomAssetBundleLoad
struct SampleCustomAssetBundleLoad_t3658166813;
// Utage.AdvImportScenarios
struct AdvImportScenarios_t1226385437;
// UnityChan.IdleChanger
struct IdleChanger_t2567672470;
// UnityEngine.Events.InvokableCallList
struct InvokableCallList_t2295673753;
// UnityEngine.Events.PersistentCallGroup
struct PersistentCallGroup_t339478082;
// UnityChan.RandomWind
struct RandomWind_t3585812647;
// UnityChan.AutoBlink
struct AutoBlink_t2157783575;
// TalkScript
struct TalkScript_t915888587;
// Utage.AdvCommandSendMessage
struct AdvCommandSendMessage_t330862209;
// UtageRecieveMessageSample
struct UtageRecieveMessageSample_t743719262;
// Utage.StringGridRow
struct StringGridRow_t4193237197;
// Utage.AdvEntityData
struct AdvEntityData_t1465354496;
// System.Collections.Generic.List`1<Utage.AssetFile>
struct List_1_t2752134388;
// Utage.AdvScenarioThread
struct AdvScenarioThread_t1270526825;
// SelectScript
struct SelectScript_t398995261;
// System.Collections.Generic.List`1<Utage.SampleCustomAssetBundleLoad/SampleAssetBundleVersionInfo>
struct List_1_t683720528;
// Utage.SampleCustomAssetBundleLoad/SampleAssetBundleVersionInfo
struct SampleAssetBundleVersionInfo_t1314599396;
// System.Char[]
struct CharU5BU5D_t1328083999;
// System.Object[]
struct ObjectU5BU5D_t3614634134;
// System.Void
struct Void_t1841601450;
// Utage.MinMaxFloat
struct MinMaxFloat_t1425080750;
// Utage.MinMaxInt
struct MinMaxInt_t3259591635;
// Utage.DrawerTest/DecoratorTest
struct DecoratorTest_t435098709;
// System.Collections.Generic.List`1<System.String>
struct List_1_t1398341365;
// Utage.AdvDataManager
struct AdvDataManager_t2481232830;
// Utage.AdvScenarioPlayer
struct AdvScenarioPlayer_t1295831480;
// Utage.AdvPage
struct AdvPage_t2715018132;
// Utage.AdvSelectionManager
struct AdvSelectionManager_t3429904078;
// Utage.AdvMessageWindowManager
struct AdvMessageWindowManager_t1170996005;
// Utage.AdvBacklogManager
struct AdvBacklogManager_t3496936997;
// Utage.AdvConfig
struct AdvConfig_t2941837115;
// Utage.AdvSystemSaveData
struct AdvSystemSaveData_t276732409;
// Utage.AdvSaveManager
struct AdvSaveManager_t2382020483;
// Utage.AdvGraphicManager
struct AdvGraphicManager_t3661251000;
// Utage.AdvEffectManager
struct AdvEffectManager_t3829746105;
// Utage.AdvUiManager
struct AdvUiManager_t4018716000;
// Utage.CameraManager
struct CameraManager_t586526220;
// Utage.AdvParamManager
struct AdvParamManager_t1816006425;
// System.Collections.Generic.List`1<Utage.AdvCustomCommandManager>
struct List_1_t3842602520;
// UnityEngine.Events.UnityEvent
struct UnityEvent_t408735097;
// Utage.OpenDialogEvent
struct OpenDialogEvent_t2352564994;
// Utage.AdvEvent
struct AdvEvent_t4160079193;
// System.Action`1<Utage.IBinaryIO>
struct Action_1_t1200141114;
// UnityEngine.CapsuleCollider
struct CapsuleCollider_t720607407;
// UnityEngine.Rigidbody
struct Rigidbody_t4233889191;
// UnityEngine.Animator
struct Animator_t69676727;
// UnityEngine.GameObject
struct GameObject_t1756533147;
// UnityEngine.UI.Text
struct Text_t356221433;
// UnityEngine.UI.RawImage
struct RawImage_t2749640213;
// UnityEngine.GameObject[]
struct GameObjectU5BU5D_t3057952154;
// UtageUguiTitle
struct UtageUguiTitle_t1483171752;
// UtageUguiLoadWait
struct UtageUguiLoadWait_t319861767;
// UtageUguiMainGame
struct UtageUguiMainGame_t4178963699;
// UnityEngine.Transform
struct Transform_t3275118058;
// System.Collections.Generic.List`1<UnityEngine.GameObject>
struct List_1_t1125654279;
// Utage.UguiNovelText
struct UguiNovelText_t4135744055;
// UnityEngine.UI.Button
struct Button_t2872111280;
// ToSettingScript
struct ToSettingScript_t3239318692;
// UnityEngine.UI.Slider
struct Slider_t297367283;
// UnityEngine.Camera
struct Camera_t189460977;
// Live2D.Cubism.Rendering.Masking.CubismMaskTexture
struct CubismMaskTexture_t949515734;
// Live2D.Cubism.Core.CubismModel
struct CubismModel_t2381008000;
// Live2D.Cubism.Framework.Raycasting.CubismRaycaster
struct CubismRaycaster_t3507460997;
// Live2D.Cubism.Framework.Raycasting.CubismRaycastHit[]
struct CubismRaycastHitU5BU5D_t1762906786;
// UnityEngine.AudioClip
struct AudioClip_t1932558630;
// UnityEngine.AudioSource
struct AudioSource_t1135106623;
// UnityEngine.SkinnedMeshRenderer
struct SkinnedMeshRenderer_t4220419316;
// UnityEngine.AnimationClip[]
struct AnimationClipU5BU5D_t3936083219;
// UnityEngine.MeshRenderer
struct MeshRenderer_t1268241104;
// UnityChan.SpringBone[]
struct SpringBoneU5BU5D_t3004447128;
// UnityChan.SpringCollider[]
struct SpringColliderU5BU5D_t2676523574;
// UnityChan.SpringManager
struct SpringManager_t4191634164;
// UnityEngine.AnimationCurve
struct AnimationCurve_t3306541151;
// UnityEngine.UI.InputField
struct InputField_t1631627530;




#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
#ifndef U3CLOADENGINEASYNCU3EC__ITERATOR0_T2663747088_H
#define U3CLOADENGINEASYNCU3EC__ITERATOR0_T2663747088_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.AdvEngineStarter/<LoadEngineAsync>c__Iterator0
struct  U3CLoadEngineAsyncU3Ec__Iterator0_t2663747088  : public RuntimeObject
{
public:
	// System.Action Utage.AdvEngineStarter/<LoadEngineAsync>c__Iterator0::onFailed
	Action_t3226471752 * ___onFailed_0;
	// Utage.AdvEngineStarter Utage.AdvEngineStarter/<LoadEngineAsync>c__Iterator0::$this
	AdvEngineStarter_t330044300 * ___U24this_1;
	// System.Object Utage.AdvEngineStarter/<LoadEngineAsync>c__Iterator0::$current
	RuntimeObject * ___U24current_2;
	// System.Boolean Utage.AdvEngineStarter/<LoadEngineAsync>c__Iterator0::$disposing
	bool ___U24disposing_3;
	// System.Int32 Utage.AdvEngineStarter/<LoadEngineAsync>c__Iterator0::$PC
	int32_t ___U24PC_4;

public:
	inline static int32_t get_offset_of_onFailed_0() { return static_cast<int32_t>(offsetof(U3CLoadEngineAsyncU3Ec__Iterator0_t2663747088, ___onFailed_0)); }
	inline Action_t3226471752 * get_onFailed_0() const { return ___onFailed_0; }
	inline Action_t3226471752 ** get_address_of_onFailed_0() { return &___onFailed_0; }
	inline void set_onFailed_0(Action_t3226471752 * value)
	{
		___onFailed_0 = value;
		Il2CppCodeGenWriteBarrier((&___onFailed_0), value);
	}

	inline static int32_t get_offset_of_U24this_1() { return static_cast<int32_t>(offsetof(U3CLoadEngineAsyncU3Ec__Iterator0_t2663747088, ___U24this_1)); }
	inline AdvEngineStarter_t330044300 * get_U24this_1() const { return ___U24this_1; }
	inline AdvEngineStarter_t330044300 ** get_address_of_U24this_1() { return &___U24this_1; }
	inline void set_U24this_1(AdvEngineStarter_t330044300 * value)
	{
		___U24this_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_1), value);
	}

	inline static int32_t get_offset_of_U24current_2() { return static_cast<int32_t>(offsetof(U3CLoadEngineAsyncU3Ec__Iterator0_t2663747088, ___U24current_2)); }
	inline RuntimeObject * get_U24current_2() const { return ___U24current_2; }
	inline RuntimeObject ** get_address_of_U24current_2() { return &___U24current_2; }
	inline void set_U24current_2(RuntimeObject * value)
	{
		___U24current_2 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_2), value);
	}

	inline static int32_t get_offset_of_U24disposing_3() { return static_cast<int32_t>(offsetof(U3CLoadEngineAsyncU3Ec__Iterator0_t2663747088, ___U24disposing_3)); }
	inline bool get_U24disposing_3() const { return ___U24disposing_3; }
	inline bool* get_address_of_U24disposing_3() { return &___U24disposing_3; }
	inline void set_U24disposing_3(bool value)
	{
		___U24disposing_3 = value;
	}

	inline static int32_t get_offset_of_U24PC_4() { return static_cast<int32_t>(offsetof(U3CLoadEngineAsyncU3Ec__Iterator0_t2663747088, ___U24PC_4)); }
	inline int32_t get_U24PC_4() const { return ___U24PC_4; }
	inline int32_t* get_address_of_U24PC_4() { return &___U24PC_4; }
	inline void set_U24PC_4(int32_t value)
	{
		___U24PC_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CLOADENGINEASYNCU3EC__ITERATOR0_T2663747088_H
#ifndef U3CLOADENGINEASYNCSUBU3EC__ANONSTOREY8_T684401054_H
#define U3CLOADENGINEASYNCSUBU3EC__ANONSTOREY8_T684401054_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.AdvEngineStarter/<LoadEngineAsyncSub>c__Iterator2/<LoadEngineAsyncSub>c__AnonStorey8
struct  U3CLoadEngineAsyncSubU3Ec__AnonStorey8_t684401054  : public RuntimeObject
{
public:
	// System.Boolean Utage.AdvEngineStarter/<LoadEngineAsyncSub>c__Iterator2/<LoadEngineAsyncSub>c__AnonStorey8::isFailed
	bool ___isFailed_0;
	// Utage.AdvEngineStarter/<LoadEngineAsyncSub>c__Iterator2 Utage.AdvEngineStarter/<LoadEngineAsyncSub>c__Iterator2/<LoadEngineAsyncSub>c__AnonStorey8::<>f__ref$2
	U3CLoadEngineAsyncSubU3Ec__Iterator2_t1722329282 * ___U3CU3Ef__refU242_1;

public:
	inline static int32_t get_offset_of_isFailed_0() { return static_cast<int32_t>(offsetof(U3CLoadEngineAsyncSubU3Ec__AnonStorey8_t684401054, ___isFailed_0)); }
	inline bool get_isFailed_0() const { return ___isFailed_0; }
	inline bool* get_address_of_isFailed_0() { return &___isFailed_0; }
	inline void set_isFailed_0(bool value)
	{
		___isFailed_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3Ef__refU242_1() { return static_cast<int32_t>(offsetof(U3CLoadEngineAsyncSubU3Ec__AnonStorey8_t684401054, ___U3CU3Ef__refU242_1)); }
	inline U3CLoadEngineAsyncSubU3Ec__Iterator2_t1722329282 * get_U3CU3Ef__refU242_1() const { return ___U3CU3Ef__refU242_1; }
	inline U3CLoadEngineAsyncSubU3Ec__Iterator2_t1722329282 ** get_address_of_U3CU3Ef__refU242_1() { return &___U3CU3Ef__refU242_1; }
	inline void set_U3CU3Ef__refU242_1(U3CLoadEngineAsyncSubU3Ec__Iterator2_t1722329282 * value)
	{
		___U3CU3Ef__refU242_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__refU242_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CLOADENGINEASYNCSUBU3EC__ANONSTOREY8_T684401054_H
#ifndef U3CLOADENGINEASYNCSUBU3EC__ITERATOR3_T1722329281_H
#define U3CLOADENGINEASYNCSUBU3EC__ITERATOR3_T1722329281_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.AdvEngineStarter/<LoadEngineAsyncSub>c__Iterator3
struct  U3CLoadEngineAsyncSubU3Ec__Iterator3_t1722329281  : public RuntimeObject
{
public:
	// System.Boolean Utage.AdvEngineStarter/<LoadEngineAsyncSub>c__Iterator3::<needsToLoadScenario>__0
	bool ___U3CneedsToLoadScenarioU3E__0_0;
	// Utage.AdvEngineStarter Utage.AdvEngineStarter/<LoadEngineAsyncSub>c__Iterator3::$this
	AdvEngineStarter_t330044300 * ___U24this_1;
	// System.Object Utage.AdvEngineStarter/<LoadEngineAsyncSub>c__Iterator3::$current
	RuntimeObject * ___U24current_2;
	// System.Boolean Utage.AdvEngineStarter/<LoadEngineAsyncSub>c__Iterator3::$disposing
	bool ___U24disposing_3;
	// System.Int32 Utage.AdvEngineStarter/<LoadEngineAsyncSub>c__Iterator3::$PC
	int32_t ___U24PC_4;

public:
	inline static int32_t get_offset_of_U3CneedsToLoadScenarioU3E__0_0() { return static_cast<int32_t>(offsetof(U3CLoadEngineAsyncSubU3Ec__Iterator3_t1722329281, ___U3CneedsToLoadScenarioU3E__0_0)); }
	inline bool get_U3CneedsToLoadScenarioU3E__0_0() const { return ___U3CneedsToLoadScenarioU3E__0_0; }
	inline bool* get_address_of_U3CneedsToLoadScenarioU3E__0_0() { return &___U3CneedsToLoadScenarioU3E__0_0; }
	inline void set_U3CneedsToLoadScenarioU3E__0_0(bool value)
	{
		___U3CneedsToLoadScenarioU3E__0_0 = value;
	}

	inline static int32_t get_offset_of_U24this_1() { return static_cast<int32_t>(offsetof(U3CLoadEngineAsyncSubU3Ec__Iterator3_t1722329281, ___U24this_1)); }
	inline AdvEngineStarter_t330044300 * get_U24this_1() const { return ___U24this_1; }
	inline AdvEngineStarter_t330044300 ** get_address_of_U24this_1() { return &___U24this_1; }
	inline void set_U24this_1(AdvEngineStarter_t330044300 * value)
	{
		___U24this_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_1), value);
	}

	inline static int32_t get_offset_of_U24current_2() { return static_cast<int32_t>(offsetof(U3CLoadEngineAsyncSubU3Ec__Iterator3_t1722329281, ___U24current_2)); }
	inline RuntimeObject * get_U24current_2() const { return ___U24current_2; }
	inline RuntimeObject ** get_address_of_U24current_2() { return &___U24current_2; }
	inline void set_U24current_2(RuntimeObject * value)
	{
		___U24current_2 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_2), value);
	}

	inline static int32_t get_offset_of_U24disposing_3() { return static_cast<int32_t>(offsetof(U3CLoadEngineAsyncSubU3Ec__Iterator3_t1722329281, ___U24disposing_3)); }
	inline bool get_U24disposing_3() const { return ___U24disposing_3; }
	inline bool* get_address_of_U24disposing_3() { return &___U24disposing_3; }
	inline void set_U24disposing_3(bool value)
	{
		___U24disposing_3 = value;
	}

	inline static int32_t get_offset_of_U24PC_4() { return static_cast<int32_t>(offsetof(U3CLoadEngineAsyncSubU3Ec__Iterator3_t1722329281, ___U24PC_4)); }
	inline int32_t get_U24PC_4() const { return ___U24PC_4; }
	inline int32_t* get_address_of_U24PC_4() { return &___U24PC_4; }
	inline void set_U24PC_4(int32_t value)
	{
		___U24PC_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CLOADENGINEASYNCSUBU3EC__ITERATOR3_T1722329281_H
#ifndef U3CLOADCHAPTERASYNCU3EC__ITERATOR1_T2472554227_H
#define U3CLOADCHAPTERASYNCU3EC__ITERATOR1_T2472554227_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.AdvEngine/<LoadChapterAsync>c__Iterator1
struct  U3CLoadChapterAsyncU3Ec__Iterator1_t2472554227  : public RuntimeObject
{
public:
	// System.String Utage.AdvEngine/<LoadChapterAsync>c__Iterator1::url
	String_t* ___url_0;
	// Utage.AssetFile Utage.AdvEngine/<LoadChapterAsync>c__Iterator1::<file>__0
	RuntimeObject* ___U3CfileU3E__0_1;
	// Utage.AdvChapterData Utage.AdvEngine/<LoadChapterAsync>c__Iterator1::<chapter>__0
	AdvChapterData_t685691324 * ___U3CchapterU3E__0_2;
	// Utage.AdvEngine Utage.AdvEngine/<LoadChapterAsync>c__Iterator1::$this
	AdvEngine_t1176753927 * ___U24this_3;
	// System.Object Utage.AdvEngine/<LoadChapterAsync>c__Iterator1::$current
	RuntimeObject * ___U24current_4;
	// System.Boolean Utage.AdvEngine/<LoadChapterAsync>c__Iterator1::$disposing
	bool ___U24disposing_5;
	// System.Int32 Utage.AdvEngine/<LoadChapterAsync>c__Iterator1::$PC
	int32_t ___U24PC_6;

public:
	inline static int32_t get_offset_of_url_0() { return static_cast<int32_t>(offsetof(U3CLoadChapterAsyncU3Ec__Iterator1_t2472554227, ___url_0)); }
	inline String_t* get_url_0() const { return ___url_0; }
	inline String_t** get_address_of_url_0() { return &___url_0; }
	inline void set_url_0(String_t* value)
	{
		___url_0 = value;
		Il2CppCodeGenWriteBarrier((&___url_0), value);
	}

	inline static int32_t get_offset_of_U3CfileU3E__0_1() { return static_cast<int32_t>(offsetof(U3CLoadChapterAsyncU3Ec__Iterator1_t2472554227, ___U3CfileU3E__0_1)); }
	inline RuntimeObject* get_U3CfileU3E__0_1() const { return ___U3CfileU3E__0_1; }
	inline RuntimeObject** get_address_of_U3CfileU3E__0_1() { return &___U3CfileU3E__0_1; }
	inline void set_U3CfileU3E__0_1(RuntimeObject* value)
	{
		___U3CfileU3E__0_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CfileU3E__0_1), value);
	}

	inline static int32_t get_offset_of_U3CchapterU3E__0_2() { return static_cast<int32_t>(offsetof(U3CLoadChapterAsyncU3Ec__Iterator1_t2472554227, ___U3CchapterU3E__0_2)); }
	inline AdvChapterData_t685691324 * get_U3CchapterU3E__0_2() const { return ___U3CchapterU3E__0_2; }
	inline AdvChapterData_t685691324 ** get_address_of_U3CchapterU3E__0_2() { return &___U3CchapterU3E__0_2; }
	inline void set_U3CchapterU3E__0_2(AdvChapterData_t685691324 * value)
	{
		___U3CchapterU3E__0_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CchapterU3E__0_2), value);
	}

	inline static int32_t get_offset_of_U24this_3() { return static_cast<int32_t>(offsetof(U3CLoadChapterAsyncU3Ec__Iterator1_t2472554227, ___U24this_3)); }
	inline AdvEngine_t1176753927 * get_U24this_3() const { return ___U24this_3; }
	inline AdvEngine_t1176753927 ** get_address_of_U24this_3() { return &___U24this_3; }
	inline void set_U24this_3(AdvEngine_t1176753927 * value)
	{
		___U24this_3 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_3), value);
	}

	inline static int32_t get_offset_of_U24current_4() { return static_cast<int32_t>(offsetof(U3CLoadChapterAsyncU3Ec__Iterator1_t2472554227, ___U24current_4)); }
	inline RuntimeObject * get_U24current_4() const { return ___U24current_4; }
	inline RuntimeObject ** get_address_of_U24current_4() { return &___U24current_4; }
	inline void set_U24current_4(RuntimeObject * value)
	{
		___U24current_4 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_4), value);
	}

	inline static int32_t get_offset_of_U24disposing_5() { return static_cast<int32_t>(offsetof(U3CLoadChapterAsyncU3Ec__Iterator1_t2472554227, ___U24disposing_5)); }
	inline bool get_U24disposing_5() const { return ___U24disposing_5; }
	inline bool* get_address_of_U24disposing_5() { return &___U24disposing_5; }
	inline void set_U24disposing_5(bool value)
	{
		___U24disposing_5 = value;
	}

	inline static int32_t get_offset_of_U24PC_6() { return static_cast<int32_t>(offsetof(U3CLoadChapterAsyncU3Ec__Iterator1_t2472554227, ___U24PC_6)); }
	inline int32_t get_U24PC_6() const { return ___U24PC_6; }
	inline int32_t* get_address_of_U24PC_6() { return &___U24PC_6; }
	inline void set_U24PC_6(int32_t value)
	{
		___U24PC_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CLOADCHAPTERASYNCU3EC__ITERATOR1_T2472554227_H
#ifndef U3CLOADCHAPTERSASYNCU3EC__ANONSTOREY1_T2271407108_H
#define U3CLOADCHAPTERSASYNCU3EC__ANONSTOREY1_T2271407108_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SampleChapterTitle/<LoadChaptersAsync>c__Iterator0/<LoadChaptersAsync>c__AnonStorey1
struct  U3CLoadChaptersAsyncU3Ec__AnonStorey1_t2271407108  : public RuntimeObject
{
public:
	// System.Int32 SampleChapterTitle/<LoadChaptersAsync>c__Iterator0/<LoadChaptersAsync>c__AnonStorey1::chapterIndex
	int32_t ___chapterIndex_0;
	// SampleChapterTitle/<LoadChaptersAsync>c__Iterator0 SampleChapterTitle/<LoadChaptersAsync>c__Iterator0/<LoadChaptersAsync>c__AnonStorey1::<>f__ref$0
	U3CLoadChaptersAsyncU3Ec__Iterator0_t3084809847 * ___U3CU3Ef__refU240_1;

public:
	inline static int32_t get_offset_of_chapterIndex_0() { return static_cast<int32_t>(offsetof(U3CLoadChaptersAsyncU3Ec__AnonStorey1_t2271407108, ___chapterIndex_0)); }
	inline int32_t get_chapterIndex_0() const { return ___chapterIndex_0; }
	inline int32_t* get_address_of_chapterIndex_0() { return &___chapterIndex_0; }
	inline void set_chapterIndex_0(int32_t value)
	{
		___chapterIndex_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3Ef__refU240_1() { return static_cast<int32_t>(offsetof(U3CLoadChaptersAsyncU3Ec__AnonStorey1_t2271407108, ___U3CU3Ef__refU240_1)); }
	inline U3CLoadChaptersAsyncU3Ec__Iterator0_t3084809847 * get_U3CU3Ef__refU240_1() const { return ___U3CU3Ef__refU240_1; }
	inline U3CLoadChaptersAsyncU3Ec__Iterator0_t3084809847 ** get_address_of_U3CU3Ef__refU240_1() { return &___U3CU3Ef__refU240_1; }
	inline void set_U3CU3Ef__refU240_1(U3CLoadChaptersAsyncU3Ec__Iterator0_t3084809847 * value)
	{
		___U3CU3Ef__refU240_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__refU240_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CLOADCHAPTERSASYNCU3EC__ANONSTOREY1_T2271407108_H
#ifndef U3CLOADCHAPTERSASYNCU3EC__ITERATOR0_T3084809847_H
#define U3CLOADCHAPTERSASYNCU3EC__ITERATOR0_T3084809847_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SampleChapterTitle/<LoadChaptersAsync>c__Iterator0
struct  U3CLoadChaptersAsyncU3Ec__Iterator0_t3084809847  : public RuntimeObject
{
public:
	// System.Int32 SampleChapterTitle/<LoadChaptersAsync>c__Iterator0::<i>__1
	int32_t ___U3CiU3E__1_0;
	// System.Int32 SampleChapterTitle/<LoadChaptersAsync>c__Iterator0::chapterIndex
	int32_t ___chapterIndex_1;
	// System.String SampleChapterTitle/<LoadChaptersAsync>c__Iterator0::<url>__2
	String_t* ___U3CurlU3E__2_2;
	// SampleChapterTitle SampleChapterTitle/<LoadChaptersAsync>c__Iterator0::$this
	SampleChapterTitle_t998764237 * ___U24this_3;
	// System.Object SampleChapterTitle/<LoadChaptersAsync>c__Iterator0::$current
	RuntimeObject * ___U24current_4;
	// System.Boolean SampleChapterTitle/<LoadChaptersAsync>c__Iterator0::$disposing
	bool ___U24disposing_5;
	// System.Int32 SampleChapterTitle/<LoadChaptersAsync>c__Iterator0::$PC
	int32_t ___U24PC_6;
	// SampleChapterTitle/<LoadChaptersAsync>c__Iterator0/<LoadChaptersAsync>c__AnonStorey1 SampleChapterTitle/<LoadChaptersAsync>c__Iterator0::$locvar0
	U3CLoadChaptersAsyncU3Ec__AnonStorey1_t2271407108 * ___U24locvar0_7;

public:
	inline static int32_t get_offset_of_U3CiU3E__1_0() { return static_cast<int32_t>(offsetof(U3CLoadChaptersAsyncU3Ec__Iterator0_t3084809847, ___U3CiU3E__1_0)); }
	inline int32_t get_U3CiU3E__1_0() const { return ___U3CiU3E__1_0; }
	inline int32_t* get_address_of_U3CiU3E__1_0() { return &___U3CiU3E__1_0; }
	inline void set_U3CiU3E__1_0(int32_t value)
	{
		___U3CiU3E__1_0 = value;
	}

	inline static int32_t get_offset_of_chapterIndex_1() { return static_cast<int32_t>(offsetof(U3CLoadChaptersAsyncU3Ec__Iterator0_t3084809847, ___chapterIndex_1)); }
	inline int32_t get_chapterIndex_1() const { return ___chapterIndex_1; }
	inline int32_t* get_address_of_chapterIndex_1() { return &___chapterIndex_1; }
	inline void set_chapterIndex_1(int32_t value)
	{
		___chapterIndex_1 = value;
	}

	inline static int32_t get_offset_of_U3CurlU3E__2_2() { return static_cast<int32_t>(offsetof(U3CLoadChaptersAsyncU3Ec__Iterator0_t3084809847, ___U3CurlU3E__2_2)); }
	inline String_t* get_U3CurlU3E__2_2() const { return ___U3CurlU3E__2_2; }
	inline String_t** get_address_of_U3CurlU3E__2_2() { return &___U3CurlU3E__2_2; }
	inline void set_U3CurlU3E__2_2(String_t* value)
	{
		___U3CurlU3E__2_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CurlU3E__2_2), value);
	}

	inline static int32_t get_offset_of_U24this_3() { return static_cast<int32_t>(offsetof(U3CLoadChaptersAsyncU3Ec__Iterator0_t3084809847, ___U24this_3)); }
	inline SampleChapterTitle_t998764237 * get_U24this_3() const { return ___U24this_3; }
	inline SampleChapterTitle_t998764237 ** get_address_of_U24this_3() { return &___U24this_3; }
	inline void set_U24this_3(SampleChapterTitle_t998764237 * value)
	{
		___U24this_3 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_3), value);
	}

	inline static int32_t get_offset_of_U24current_4() { return static_cast<int32_t>(offsetof(U3CLoadChaptersAsyncU3Ec__Iterator0_t3084809847, ___U24current_4)); }
	inline RuntimeObject * get_U24current_4() const { return ___U24current_4; }
	inline RuntimeObject ** get_address_of_U24current_4() { return &___U24current_4; }
	inline void set_U24current_4(RuntimeObject * value)
	{
		___U24current_4 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_4), value);
	}

	inline static int32_t get_offset_of_U24disposing_5() { return static_cast<int32_t>(offsetof(U3CLoadChaptersAsyncU3Ec__Iterator0_t3084809847, ___U24disposing_5)); }
	inline bool get_U24disposing_5() const { return ___U24disposing_5; }
	inline bool* get_address_of_U24disposing_5() { return &___U24disposing_5; }
	inline void set_U24disposing_5(bool value)
	{
		___U24disposing_5 = value;
	}

	inline static int32_t get_offset_of_U24PC_6() { return static_cast<int32_t>(offsetof(U3CLoadChaptersAsyncU3Ec__Iterator0_t3084809847, ___U24PC_6)); }
	inline int32_t get_U24PC_6() const { return ___U24PC_6; }
	inline int32_t* get_address_of_U24PC_6() { return &___U24PC_6; }
	inline void set_U24PC_6(int32_t value)
	{
		___U24PC_6 = value;
	}

	inline static int32_t get_offset_of_U24locvar0_7() { return static_cast<int32_t>(offsetof(U3CLoadChaptersAsyncU3Ec__Iterator0_t3084809847, ___U24locvar0_7)); }
	inline U3CLoadChaptersAsyncU3Ec__AnonStorey1_t2271407108 * get_U24locvar0_7() const { return ___U24locvar0_7; }
	inline U3CLoadChaptersAsyncU3Ec__AnonStorey1_t2271407108 ** get_address_of_U24locvar0_7() { return &___U24locvar0_7; }
	inline void set_U24locvar0_7(U3CLoadChaptersAsyncU3Ec__AnonStorey1_t2271407108 * value)
	{
		___U24locvar0_7 = value;
		Il2CppCodeGenWriteBarrier((&___U24locvar0_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CLOADCHAPTERSASYNCU3EC__ITERATOR0_T3084809847_H
#ifndef U3CEXITSCHAPTERU3EC__ANONSTOREY5_T3858776566_H
#define U3CEXITSCHAPTERU3EC__ANONSTOREY5_T3858776566_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.AdvEngine/<ExitsChapter>c__AnonStorey5
struct  U3CExitsChapterU3Ec__AnonStorey5_t3858776566  : public RuntimeObject
{
public:
	// System.String Utage.AdvEngine/<ExitsChapter>c__AnonStorey5::chapterAssetName
	String_t* ___chapterAssetName_0;

public:
	inline static int32_t get_offset_of_chapterAssetName_0() { return static_cast<int32_t>(offsetof(U3CExitsChapterU3Ec__AnonStorey5_t3858776566, ___chapterAssetName_0)); }
	inline String_t* get_chapterAssetName_0() const { return ___chapterAssetName_0; }
	inline String_t** get_address_of_chapterAssetName_0() { return &___chapterAssetName_0; }
	inline void set_chapterAssetName_0(String_t* value)
	{
		___chapterAssetName_0 = value;
		Il2CppCodeGenWriteBarrier((&___chapterAssetName_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CEXITSCHAPTERU3EC__ANONSTOREY5_T3858776566_H
#ifndef U3CTRANSITIONU3EC__ITERATOR0_T3085833305_H
#define U3CTRANSITIONU3EC__ITERATOR0_T3085833305_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TransitionScript/<Transition>c__Iterator0
struct  U3CTransitionU3Ec__Iterator0_t3085833305  : public RuntimeObject
{
public:
	// TransitionScript TransitionScript/<Transition>c__Iterator0::$this
	TransitionScript_t3831648224 * ___U24this_0;
	// System.Object TransitionScript/<Transition>c__Iterator0::$current
	RuntimeObject * ___U24current_1;
	// System.Boolean TransitionScript/<Transition>c__Iterator0::$disposing
	bool ___U24disposing_2;
	// System.Int32 TransitionScript/<Transition>c__Iterator0::$PC
	int32_t ___U24PC_3;

public:
	inline static int32_t get_offset_of_U24this_0() { return static_cast<int32_t>(offsetof(U3CTransitionU3Ec__Iterator0_t3085833305, ___U24this_0)); }
	inline TransitionScript_t3831648224 * get_U24this_0() const { return ___U24this_0; }
	inline TransitionScript_t3831648224 ** get_address_of_U24this_0() { return &___U24this_0; }
	inline void set_U24this_0(TransitionScript_t3831648224 * value)
	{
		___U24this_0 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_0), value);
	}

	inline static int32_t get_offset_of_U24current_1() { return static_cast<int32_t>(offsetof(U3CTransitionU3Ec__Iterator0_t3085833305, ___U24current_1)); }
	inline RuntimeObject * get_U24current_1() const { return ___U24current_1; }
	inline RuntimeObject ** get_address_of_U24current_1() { return &___U24current_1; }
	inline void set_U24current_1(RuntimeObject * value)
	{
		___U24current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_1), value);
	}

	inline static int32_t get_offset_of_U24disposing_2() { return static_cast<int32_t>(offsetof(U3CTransitionU3Ec__Iterator0_t3085833305, ___U24disposing_2)); }
	inline bool get_U24disposing_2() const { return ___U24disposing_2; }
	inline bool* get_address_of_U24disposing_2() { return &___U24disposing_2; }
	inline void set_U24disposing_2(bool value)
	{
		___U24disposing_2 = value;
	}

	inline static int32_t get_offset_of_U24PC_3() { return static_cast<int32_t>(offsetof(U3CTransitionU3Ec__Iterator0_t3085833305, ___U24PC_3)); }
	inline int32_t get_U24PC_3() const { return ___U24PC_3; }
	inline int32_t* get_address_of_U24PC_3() { return &___U24PC_3; }
	inline void set_U24PC_3(int32_t value)
	{
		___U24PC_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CTRANSITIONU3EC__ITERATOR0_T3085833305_H
#ifndef U3CCOROTATE3DU3EC__ITERATOR0_T4279666499_H
#define U3CCOROTATE3DU3EC__ITERATOR0_T4279666499_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UtageRecieveMessageFromAdvComannd/<CoRotate3D>c__Iterator0
struct  U3CCoRotate3DU3Ec__Iterator0_t4279666499  : public RuntimeObject
{
public:
	// UtageRecieveMessageFromAdvComannd UtageRecieveMessageFromAdvComannd/<CoRotate3D>c__Iterator0::$this
	UtageRecieveMessageFromAdvComannd_t4227868061 * ___U24this_0;
	// System.Object UtageRecieveMessageFromAdvComannd/<CoRotate3D>c__Iterator0::$current
	RuntimeObject * ___U24current_1;
	// System.Boolean UtageRecieveMessageFromAdvComannd/<CoRotate3D>c__Iterator0::$disposing
	bool ___U24disposing_2;
	// System.Int32 UtageRecieveMessageFromAdvComannd/<CoRotate3D>c__Iterator0::$PC
	int32_t ___U24PC_3;

public:
	inline static int32_t get_offset_of_U24this_0() { return static_cast<int32_t>(offsetof(U3CCoRotate3DU3Ec__Iterator0_t4279666499, ___U24this_0)); }
	inline UtageRecieveMessageFromAdvComannd_t4227868061 * get_U24this_0() const { return ___U24this_0; }
	inline UtageRecieveMessageFromAdvComannd_t4227868061 ** get_address_of_U24this_0() { return &___U24this_0; }
	inline void set_U24this_0(UtageRecieveMessageFromAdvComannd_t4227868061 * value)
	{
		___U24this_0 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_0), value);
	}

	inline static int32_t get_offset_of_U24current_1() { return static_cast<int32_t>(offsetof(U3CCoRotate3DU3Ec__Iterator0_t4279666499, ___U24current_1)); }
	inline RuntimeObject * get_U24current_1() const { return ___U24current_1; }
	inline RuntimeObject ** get_address_of_U24current_1() { return &___U24current_1; }
	inline void set_U24current_1(RuntimeObject * value)
	{
		___U24current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_1), value);
	}

	inline static int32_t get_offset_of_U24disposing_2() { return static_cast<int32_t>(offsetof(U3CCoRotate3DU3Ec__Iterator0_t4279666499, ___U24disposing_2)); }
	inline bool get_U24disposing_2() const { return ___U24disposing_2; }
	inline bool* get_address_of_U24disposing_2() { return &___U24disposing_2; }
	inline void set_U24disposing_2(bool value)
	{
		___U24disposing_2 = value;
	}

	inline static int32_t get_offset_of_U24PC_3() { return static_cast<int32_t>(offsetof(U3CCoRotate3DU3Ec__Iterator0_t4279666499, ___U24PC_3)); }
	inline int32_t get_U24PC_3() const { return ___U24PC_3; }
	inline int32_t* get_address_of_U24PC_3() { return &___U24PC_3; }
	inline void set_U24PC_3(int32_t value)
	{
		___U24PC_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CCOROTATE3DU3EC__ITERATOR0_T4279666499_H
#ifndef U3CONINITDATAU3EC__ANONSTOREY1_T3015087187_H
#define U3CONINITDATAU3EC__ANONSTOREY1_T3015087187_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SampleChatLogItem/<OnInitData>c__AnonStorey1
struct  U3COnInitDataU3Ec__AnonStorey1_t3015087187  : public RuntimeObject
{
public:
	// Utage.AdvBacklog SampleChatLogItem/<OnInitData>c__AnonStorey1::data
	AdvBacklog_t3927301290 * ___data_0;
	// SampleChatLogItem SampleChatLogItem/<OnInitData>c__AnonStorey1::$this
	SampleChatLogItem_t3744651319 * ___U24this_1;

public:
	inline static int32_t get_offset_of_data_0() { return static_cast<int32_t>(offsetof(U3COnInitDataU3Ec__AnonStorey1_t3015087187, ___data_0)); }
	inline AdvBacklog_t3927301290 * get_data_0() const { return ___data_0; }
	inline AdvBacklog_t3927301290 ** get_address_of_data_0() { return &___data_0; }
	inline void set_data_0(AdvBacklog_t3927301290 * value)
	{
		___data_0 = value;
		Il2CppCodeGenWriteBarrier((&___data_0), value);
	}

	inline static int32_t get_offset_of_U24this_1() { return static_cast<int32_t>(offsetof(U3COnInitDataU3Ec__AnonStorey1_t3015087187, ___U24this_1)); }
	inline SampleChatLogItem_t3744651319 * get_U24this_1() const { return ___U24this_1; }
	inline SampleChatLogItem_t3744651319 ** get_address_of_U24this_1() { return &___U24this_1; }
	inline void set_U24this_1(SampleChatLogItem_t3744651319 * value)
	{
		___U24this_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CONINITDATAU3EC__ANONSTOREY1_T3015087187_H
#ifndef U3CCUSTOMCALLBACKFILELOADERRORU3EC__ANONSTOREY1_T203029879_H
#define U3CCUSTOMCALLBACKFILELOADERRORU3EC__ANONSTOREY1_T203029879_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SampleLoadError/<CustomCallbackFileLoadError>c__AnonStorey1
struct  U3CCustomCallbackFileLoadErrorU3Ec__AnonStorey1_t203029879  : public RuntimeObject
{
public:
	// Utage.AssetFile SampleLoadError/<CustomCallbackFileLoadError>c__AnonStorey1::file
	RuntimeObject* ___file_0;
	// SampleLoadError SampleLoadError/<CustomCallbackFileLoadError>c__AnonStorey1::$this
	SampleLoadError_t2227685100 * ___U24this_1;

public:
	inline static int32_t get_offset_of_file_0() { return static_cast<int32_t>(offsetof(U3CCustomCallbackFileLoadErrorU3Ec__AnonStorey1_t203029879, ___file_0)); }
	inline RuntimeObject* get_file_0() const { return ___file_0; }
	inline RuntimeObject** get_address_of_file_0() { return &___file_0; }
	inline void set_file_0(RuntimeObject* value)
	{
		___file_0 = value;
		Il2CppCodeGenWriteBarrier((&___file_0), value);
	}

	inline static int32_t get_offset_of_U24this_1() { return static_cast<int32_t>(offsetof(U3CCustomCallbackFileLoadErrorU3Ec__AnonStorey1_t203029879, ___U24this_1)); }
	inline SampleLoadError_t2227685100 * get_U24this_1() const { return ___U24this_1; }
	inline SampleLoadError_t2227685100 ** get_address_of_U24this_1() { return &___U24this_1; }
	inline void set_U24this_1(SampleLoadError_t2227685100 * value)
	{
		___U24this_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CCUSTOMCALLBACKFILELOADERRORU3EC__ANONSTOREY1_T203029879_H
#ifndef U3CCOWAITRETRYU3EC__ITERATOR0_T3805080565_H
#define U3CCOWAITRETRYU3EC__ITERATOR0_T3805080565_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SampleLoadError/<CoWaitRetry>c__Iterator0
struct  U3CCoWaitRetryU3Ec__Iterator0_t3805080565  : public RuntimeObject
{
public:
	// Utage.AssetFile SampleLoadError/<CoWaitRetry>c__Iterator0::file
	RuntimeObject* ___file_0;
	// SampleLoadError SampleLoadError/<CoWaitRetry>c__Iterator0::$this
	SampleLoadError_t2227685100 * ___U24this_1;
	// System.Object SampleLoadError/<CoWaitRetry>c__Iterator0::$current
	RuntimeObject * ___U24current_2;
	// System.Boolean SampleLoadError/<CoWaitRetry>c__Iterator0::$disposing
	bool ___U24disposing_3;
	// System.Int32 SampleLoadError/<CoWaitRetry>c__Iterator0::$PC
	int32_t ___U24PC_4;

public:
	inline static int32_t get_offset_of_file_0() { return static_cast<int32_t>(offsetof(U3CCoWaitRetryU3Ec__Iterator0_t3805080565, ___file_0)); }
	inline RuntimeObject* get_file_0() const { return ___file_0; }
	inline RuntimeObject** get_address_of_file_0() { return &___file_0; }
	inline void set_file_0(RuntimeObject* value)
	{
		___file_0 = value;
		Il2CppCodeGenWriteBarrier((&___file_0), value);
	}

	inline static int32_t get_offset_of_U24this_1() { return static_cast<int32_t>(offsetof(U3CCoWaitRetryU3Ec__Iterator0_t3805080565, ___U24this_1)); }
	inline SampleLoadError_t2227685100 * get_U24this_1() const { return ___U24this_1; }
	inline SampleLoadError_t2227685100 ** get_address_of_U24this_1() { return &___U24this_1; }
	inline void set_U24this_1(SampleLoadError_t2227685100 * value)
	{
		___U24this_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_1), value);
	}

	inline static int32_t get_offset_of_U24current_2() { return static_cast<int32_t>(offsetof(U3CCoWaitRetryU3Ec__Iterator0_t3805080565, ___U24current_2)); }
	inline RuntimeObject * get_U24current_2() const { return ___U24current_2; }
	inline RuntimeObject ** get_address_of_U24current_2() { return &___U24current_2; }
	inline void set_U24current_2(RuntimeObject * value)
	{
		___U24current_2 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_2), value);
	}

	inline static int32_t get_offset_of_U24disposing_3() { return static_cast<int32_t>(offsetof(U3CCoWaitRetryU3Ec__Iterator0_t3805080565, ___U24disposing_3)); }
	inline bool get_U24disposing_3() const { return ___U24disposing_3; }
	inline bool* get_address_of_U24disposing_3() { return &___U24disposing_3; }
	inline void set_U24disposing_3(bool value)
	{
		___U24disposing_3 = value;
	}

	inline static int32_t get_offset_of_U24PC_4() { return static_cast<int32_t>(offsetof(U3CCoWaitRetryU3Ec__Iterator0_t3805080565, ___U24PC_4)); }
	inline int32_t get_U24PC_4() const { return ___U24PC_4; }
	inline int32_t* get_address_of_U24PC_4() { return &___U24PC_4; }
	inline void set_U24PC_4(int32_t value)
	{
		___U24PC_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CCOWAITRETRYU3EC__ITERATOR0_T3805080565_H
#ifndef U3CCOSTARTSAVEDATAU3EC__ITERATOR4_T1384462154_H
#define U3CCOSTARTSAVEDATAU3EC__ITERATOR4_T1384462154_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.AdvEngine/<CoStartSaveData>c__Iterator4
struct  U3CCoStartSaveDataU3Ec__Iterator4_t1384462154  : public RuntimeObject
{
public:
	// Utage.AdvSaveData Utage.AdvEngine/<CoStartSaveData>c__Iterator4::saveData
	AdvSaveData_t4059487466 * ___saveData_0;
	// Utage.AdvEngine Utage.AdvEngine/<CoStartSaveData>c__Iterator4::$this
	AdvEngine_t1176753927 * ___U24this_1;
	// System.Object Utage.AdvEngine/<CoStartSaveData>c__Iterator4::$current
	RuntimeObject * ___U24current_2;
	// System.Boolean Utage.AdvEngine/<CoStartSaveData>c__Iterator4::$disposing
	bool ___U24disposing_3;
	// System.Int32 Utage.AdvEngine/<CoStartSaveData>c__Iterator4::$PC
	int32_t ___U24PC_4;

public:
	inline static int32_t get_offset_of_saveData_0() { return static_cast<int32_t>(offsetof(U3CCoStartSaveDataU3Ec__Iterator4_t1384462154, ___saveData_0)); }
	inline AdvSaveData_t4059487466 * get_saveData_0() const { return ___saveData_0; }
	inline AdvSaveData_t4059487466 ** get_address_of_saveData_0() { return &___saveData_0; }
	inline void set_saveData_0(AdvSaveData_t4059487466 * value)
	{
		___saveData_0 = value;
		Il2CppCodeGenWriteBarrier((&___saveData_0), value);
	}

	inline static int32_t get_offset_of_U24this_1() { return static_cast<int32_t>(offsetof(U3CCoStartSaveDataU3Ec__Iterator4_t1384462154, ___U24this_1)); }
	inline AdvEngine_t1176753927 * get_U24this_1() const { return ___U24this_1; }
	inline AdvEngine_t1176753927 ** get_address_of_U24this_1() { return &___U24this_1; }
	inline void set_U24this_1(AdvEngine_t1176753927 * value)
	{
		___U24this_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_1), value);
	}

	inline static int32_t get_offset_of_U24current_2() { return static_cast<int32_t>(offsetof(U3CCoStartSaveDataU3Ec__Iterator4_t1384462154, ___U24current_2)); }
	inline RuntimeObject * get_U24current_2() const { return ___U24current_2; }
	inline RuntimeObject ** get_address_of_U24current_2() { return &___U24current_2; }
	inline void set_U24current_2(RuntimeObject * value)
	{
		___U24current_2 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_2), value);
	}

	inline static int32_t get_offset_of_U24disposing_3() { return static_cast<int32_t>(offsetof(U3CCoStartSaveDataU3Ec__Iterator4_t1384462154, ___U24disposing_3)); }
	inline bool get_U24disposing_3() const { return ___U24disposing_3; }
	inline bool* get_address_of_U24disposing_3() { return &___U24disposing_3; }
	inline void set_U24disposing_3(bool value)
	{
		___U24disposing_3 = value;
	}

	inline static int32_t get_offset_of_U24PC_4() { return static_cast<int32_t>(offsetof(U3CCoStartSaveDataU3Ec__Iterator4_t1384462154, ___U24PC_4)); }
	inline int32_t get_U24PC_4() const { return ___U24PC_4; }
	inline int32_t* get_address_of_U24PC_4() { return &___U24PC_4; }
	inline void set_U24PC_4(int32_t value)
	{
		___U24PC_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CCOSTARTSAVEDATAU3EC__ITERATOR4_T1384462154_H
#ifndef U3CCOSTARTSCENARIOU3EC__ITERATOR3_T3243498162_H
#define U3CCOSTARTSCENARIOU3EC__ITERATOR3_T3243498162_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.AdvEngine/<CoStartScenario>c__Iterator3
struct  U3CCoStartScenarioU3Ec__Iterator3_t3243498162  : public RuntimeObject
{
public:
	// System.String Utage.AdvEngine/<CoStartScenario>c__Iterator3::label
	String_t* ___label_0;
	// System.Int32 Utage.AdvEngine/<CoStartScenario>c__Iterator3::page
	int32_t ___page_1;
	// Utage.AdvEngine Utage.AdvEngine/<CoStartScenario>c__Iterator3::$this
	AdvEngine_t1176753927 * ___U24this_2;
	// System.Object Utage.AdvEngine/<CoStartScenario>c__Iterator3::$current
	RuntimeObject * ___U24current_3;
	// System.Boolean Utage.AdvEngine/<CoStartScenario>c__Iterator3::$disposing
	bool ___U24disposing_4;
	// System.Int32 Utage.AdvEngine/<CoStartScenario>c__Iterator3::$PC
	int32_t ___U24PC_5;

public:
	inline static int32_t get_offset_of_label_0() { return static_cast<int32_t>(offsetof(U3CCoStartScenarioU3Ec__Iterator3_t3243498162, ___label_0)); }
	inline String_t* get_label_0() const { return ___label_0; }
	inline String_t** get_address_of_label_0() { return &___label_0; }
	inline void set_label_0(String_t* value)
	{
		___label_0 = value;
		Il2CppCodeGenWriteBarrier((&___label_0), value);
	}

	inline static int32_t get_offset_of_page_1() { return static_cast<int32_t>(offsetof(U3CCoStartScenarioU3Ec__Iterator3_t3243498162, ___page_1)); }
	inline int32_t get_page_1() const { return ___page_1; }
	inline int32_t* get_address_of_page_1() { return &___page_1; }
	inline void set_page_1(int32_t value)
	{
		___page_1 = value;
	}

	inline static int32_t get_offset_of_U24this_2() { return static_cast<int32_t>(offsetof(U3CCoStartScenarioU3Ec__Iterator3_t3243498162, ___U24this_2)); }
	inline AdvEngine_t1176753927 * get_U24this_2() const { return ___U24this_2; }
	inline AdvEngine_t1176753927 ** get_address_of_U24this_2() { return &___U24this_2; }
	inline void set_U24this_2(AdvEngine_t1176753927 * value)
	{
		___U24this_2 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_2), value);
	}

	inline static int32_t get_offset_of_U24current_3() { return static_cast<int32_t>(offsetof(U3CCoStartScenarioU3Ec__Iterator3_t3243498162, ___U24current_3)); }
	inline RuntimeObject * get_U24current_3() const { return ___U24current_3; }
	inline RuntimeObject ** get_address_of_U24current_3() { return &___U24current_3; }
	inline void set_U24current_3(RuntimeObject * value)
	{
		___U24current_3 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_3), value);
	}

	inline static int32_t get_offset_of_U24disposing_4() { return static_cast<int32_t>(offsetof(U3CCoStartScenarioU3Ec__Iterator3_t3243498162, ___U24disposing_4)); }
	inline bool get_U24disposing_4() const { return ___U24disposing_4; }
	inline bool* get_address_of_U24disposing_4() { return &___U24disposing_4; }
	inline void set_U24disposing_4(bool value)
	{
		___U24disposing_4 = value;
	}

	inline static int32_t get_offset_of_U24PC_5() { return static_cast<int32_t>(offsetof(U3CCoStartScenarioU3Ec__Iterator3_t3243498162, ___U24PC_5)); }
	inline int32_t get_U24PC_5() const { return ___U24PC_5; }
	inline int32_t* get_address_of_U24PC_5() { return &___U24PC_5; }
	inline void set_U24PC_5(int32_t value)
	{
		___U24PC_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CCOSTARTSCENARIOU3EC__ITERATOR3_T3243498162_H
#ifndef U3CCOSTARTGAMESUBU3EC__ITERATOR2_T4101911903_H
#define U3CCOSTARTGAMESUBU3EC__ITERATOR2_T4101911903_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.AdvEngine/<CoStartGameSub>c__Iterator2
struct  U3CCoStartGameSubU3Ec__Iterator2_t4101911903  : public RuntimeObject
{
public:
	// System.String Utage.AdvEngine/<CoStartGameSub>c__Iterator2::scenarioLabel
	String_t* ___scenarioLabel_0;
	// Utage.AdvEngine Utage.AdvEngine/<CoStartGameSub>c__Iterator2::$this
	AdvEngine_t1176753927 * ___U24this_1;
	// System.Object Utage.AdvEngine/<CoStartGameSub>c__Iterator2::$current
	RuntimeObject * ___U24current_2;
	// System.Boolean Utage.AdvEngine/<CoStartGameSub>c__Iterator2::$disposing
	bool ___U24disposing_3;
	// System.Int32 Utage.AdvEngine/<CoStartGameSub>c__Iterator2::$PC
	int32_t ___U24PC_4;

public:
	inline static int32_t get_offset_of_scenarioLabel_0() { return static_cast<int32_t>(offsetof(U3CCoStartGameSubU3Ec__Iterator2_t4101911903, ___scenarioLabel_0)); }
	inline String_t* get_scenarioLabel_0() const { return ___scenarioLabel_0; }
	inline String_t** get_address_of_scenarioLabel_0() { return &___scenarioLabel_0; }
	inline void set_scenarioLabel_0(String_t* value)
	{
		___scenarioLabel_0 = value;
		Il2CppCodeGenWriteBarrier((&___scenarioLabel_0), value);
	}

	inline static int32_t get_offset_of_U24this_1() { return static_cast<int32_t>(offsetof(U3CCoStartGameSubU3Ec__Iterator2_t4101911903, ___U24this_1)); }
	inline AdvEngine_t1176753927 * get_U24this_1() const { return ___U24this_1; }
	inline AdvEngine_t1176753927 ** get_address_of_U24this_1() { return &___U24this_1; }
	inline void set_U24this_1(AdvEngine_t1176753927 * value)
	{
		___U24this_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_1), value);
	}

	inline static int32_t get_offset_of_U24current_2() { return static_cast<int32_t>(offsetof(U3CCoStartGameSubU3Ec__Iterator2_t4101911903, ___U24current_2)); }
	inline RuntimeObject * get_U24current_2() const { return ___U24current_2; }
	inline RuntimeObject ** get_address_of_U24current_2() { return &___U24current_2; }
	inline void set_U24current_2(RuntimeObject * value)
	{
		___U24current_2 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_2), value);
	}

	inline static int32_t get_offset_of_U24disposing_3() { return static_cast<int32_t>(offsetof(U3CCoStartGameSubU3Ec__Iterator2_t4101911903, ___U24disposing_3)); }
	inline bool get_U24disposing_3() const { return ___U24disposing_3; }
	inline bool* get_address_of_U24disposing_3() { return &___U24disposing_3; }
	inline void set_U24disposing_3(bool value)
	{
		___U24disposing_3 = value;
	}

	inline static int32_t get_offset_of_U24PC_4() { return static_cast<int32_t>(offsetof(U3CCoStartGameSubU3Ec__Iterator2_t4101911903, ___U24PC_4)); }
	inline int32_t get_U24PC_4() const { return ___U24PC_4; }
	inline int32_t* get_address_of_U24PC_4() { return &___U24PC_4; }
	inline void set_U24PC_4(int32_t value)
	{
		___U24PC_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CCOSTARTGAMESUBU3EC__ITERATOR2_T4101911903_H
#ifndef U3CCOWAITU3EC__ITERATOR0_T71556710_H
#define U3CCOWAITU3EC__ITERATOR0_T71556710_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SampleSendMessageByName/<CoWait>c__Iterator0
struct  U3CCoWaitU3Ec__Iterator0_t71556710  : public RuntimeObject
{
public:
	// Utage.AdvCommandSendMessageByName SampleSendMessageByName/<CoWait>c__Iterator0::command
	AdvCommandSendMessageByName_t3179247017 * ___command_0;
	// System.Single SampleSendMessageByName/<CoWait>c__Iterator0::<time>__0
	float ___U3CtimeU3E__0_1;
	// System.Object SampleSendMessageByName/<CoWait>c__Iterator0::$current
	RuntimeObject * ___U24current_2;
	// System.Boolean SampleSendMessageByName/<CoWait>c__Iterator0::$disposing
	bool ___U24disposing_3;
	// System.Int32 SampleSendMessageByName/<CoWait>c__Iterator0::$PC
	int32_t ___U24PC_4;

public:
	inline static int32_t get_offset_of_command_0() { return static_cast<int32_t>(offsetof(U3CCoWaitU3Ec__Iterator0_t71556710, ___command_0)); }
	inline AdvCommandSendMessageByName_t3179247017 * get_command_0() const { return ___command_0; }
	inline AdvCommandSendMessageByName_t3179247017 ** get_address_of_command_0() { return &___command_0; }
	inline void set_command_0(AdvCommandSendMessageByName_t3179247017 * value)
	{
		___command_0 = value;
		Il2CppCodeGenWriteBarrier((&___command_0), value);
	}

	inline static int32_t get_offset_of_U3CtimeU3E__0_1() { return static_cast<int32_t>(offsetof(U3CCoWaitU3Ec__Iterator0_t71556710, ___U3CtimeU3E__0_1)); }
	inline float get_U3CtimeU3E__0_1() const { return ___U3CtimeU3E__0_1; }
	inline float* get_address_of_U3CtimeU3E__0_1() { return &___U3CtimeU3E__0_1; }
	inline void set_U3CtimeU3E__0_1(float value)
	{
		___U3CtimeU3E__0_1 = value;
	}

	inline static int32_t get_offset_of_U24current_2() { return static_cast<int32_t>(offsetof(U3CCoWaitU3Ec__Iterator0_t71556710, ___U24current_2)); }
	inline RuntimeObject * get_U24current_2() const { return ___U24current_2; }
	inline RuntimeObject ** get_address_of_U24current_2() { return &___U24current_2; }
	inline void set_U24current_2(RuntimeObject * value)
	{
		___U24current_2 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_2), value);
	}

	inline static int32_t get_offset_of_U24disposing_3() { return static_cast<int32_t>(offsetof(U3CCoWaitU3Ec__Iterator0_t71556710, ___U24disposing_3)); }
	inline bool get_U24disposing_3() const { return ___U24disposing_3; }
	inline bool* get_address_of_U24disposing_3() { return &___U24disposing_3; }
	inline void set_U24disposing_3(bool value)
	{
		___U24disposing_3 = value;
	}

	inline static int32_t get_offset_of_U24PC_4() { return static_cast<int32_t>(offsetof(U3CCoWaitU3Ec__Iterator0_t71556710, ___U24PC_4)); }
	inline int32_t get_U24PC_4() const { return ___U24PC_4; }
	inline int32_t* get_address_of_U24PC_4() { return &___U24PC_4; }
	inline void set_U24PC_4(int32_t value)
	{
		___U24PC_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CCOWAITU3EC__ITERATOR0_T71556710_H
#ifndef U3CLOADENGINEASYNCFROMCACHEMANIFESTU3EC__ITERATOR1_T3219196200_H
#define U3CLOADENGINEASYNCFROMCACHEMANIFESTU3EC__ITERATOR1_T3219196200_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.AdvEngineStarter/<LoadEngineAsyncFromCacheManifest>c__Iterator1
struct  U3CLoadEngineAsyncFromCacheManifestU3Ec__Iterator1_t3219196200  : public RuntimeObject
{
public:
	// System.Action Utage.AdvEngineStarter/<LoadEngineAsyncFromCacheManifest>c__Iterator1::onFailed
	Action_t3226471752 * ___onFailed_0;
	// Utage.AdvEngineStarter Utage.AdvEngineStarter/<LoadEngineAsyncFromCacheManifest>c__Iterator1::$this
	AdvEngineStarter_t330044300 * ___U24this_1;
	// System.Object Utage.AdvEngineStarter/<LoadEngineAsyncFromCacheManifest>c__Iterator1::$current
	RuntimeObject * ___U24current_2;
	// System.Boolean Utage.AdvEngineStarter/<LoadEngineAsyncFromCacheManifest>c__Iterator1::$disposing
	bool ___U24disposing_3;
	// System.Int32 Utage.AdvEngineStarter/<LoadEngineAsyncFromCacheManifest>c__Iterator1::$PC
	int32_t ___U24PC_4;

public:
	inline static int32_t get_offset_of_onFailed_0() { return static_cast<int32_t>(offsetof(U3CLoadEngineAsyncFromCacheManifestU3Ec__Iterator1_t3219196200, ___onFailed_0)); }
	inline Action_t3226471752 * get_onFailed_0() const { return ___onFailed_0; }
	inline Action_t3226471752 ** get_address_of_onFailed_0() { return &___onFailed_0; }
	inline void set_onFailed_0(Action_t3226471752 * value)
	{
		___onFailed_0 = value;
		Il2CppCodeGenWriteBarrier((&___onFailed_0), value);
	}

	inline static int32_t get_offset_of_U24this_1() { return static_cast<int32_t>(offsetof(U3CLoadEngineAsyncFromCacheManifestU3Ec__Iterator1_t3219196200, ___U24this_1)); }
	inline AdvEngineStarter_t330044300 * get_U24this_1() const { return ___U24this_1; }
	inline AdvEngineStarter_t330044300 ** get_address_of_U24this_1() { return &___U24this_1; }
	inline void set_U24this_1(AdvEngineStarter_t330044300 * value)
	{
		___U24this_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_1), value);
	}

	inline static int32_t get_offset_of_U24current_2() { return static_cast<int32_t>(offsetof(U3CLoadEngineAsyncFromCacheManifestU3Ec__Iterator1_t3219196200, ___U24current_2)); }
	inline RuntimeObject * get_U24current_2() const { return ___U24current_2; }
	inline RuntimeObject ** get_address_of_U24current_2() { return &___U24current_2; }
	inline void set_U24current_2(RuntimeObject * value)
	{
		___U24current_2 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_2), value);
	}

	inline static int32_t get_offset_of_U24disposing_3() { return static_cast<int32_t>(offsetof(U3CLoadEngineAsyncFromCacheManifestU3Ec__Iterator1_t3219196200, ___U24disposing_3)); }
	inline bool get_U24disposing_3() const { return ___U24disposing_3; }
	inline bool* get_address_of_U24disposing_3() { return &___U24disposing_3; }
	inline void set_U24disposing_3(bool value)
	{
		___U24disposing_3 = value;
	}

	inline static int32_t get_offset_of_U24PC_4() { return static_cast<int32_t>(offsetof(U3CLoadEngineAsyncFromCacheManifestU3Ec__Iterator1_t3219196200, ___U24PC_4)); }
	inline int32_t get_U24PC_4() const { return ___U24PC_4; }
	inline int32_t* get_address_of_U24PC_4() { return &___U24PC_4; }
	inline void set_U24PC_4(int32_t value)
	{
		___U24PC_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CLOADENGINEASYNCFROMCACHEMANIFESTU3EC__ITERATOR1_T3219196200_H
#ifndef U3CLOADENGINEASYNCSUBU3EC__ITERATOR2_T1722329282_H
#define U3CLOADENGINEASYNCSUBU3EC__ITERATOR2_T1722329282_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.AdvEngineStarter/<LoadEngineAsyncSub>c__Iterator2
struct  U3CLoadEngineAsyncSubU3Ec__Iterator2_t1722329282  : public RuntimeObject
{
public:
	// System.Boolean Utage.AdvEngineStarter/<LoadEngineAsyncSub>c__Iterator2::loadManifestFromCache
	bool ___loadManifestFromCache_0;
	// System.Action Utage.AdvEngineStarter/<LoadEngineAsyncSub>c__Iterator2::onFailed
	Action_t3226471752 * ___onFailed_1;
	// Utage.AdvEngineStarter Utage.AdvEngineStarter/<LoadEngineAsyncSub>c__Iterator2::$this
	AdvEngineStarter_t330044300 * ___U24this_2;
	// System.Object Utage.AdvEngineStarter/<LoadEngineAsyncSub>c__Iterator2::$current
	RuntimeObject * ___U24current_3;
	// System.Boolean Utage.AdvEngineStarter/<LoadEngineAsyncSub>c__Iterator2::$disposing
	bool ___U24disposing_4;
	// System.Int32 Utage.AdvEngineStarter/<LoadEngineAsyncSub>c__Iterator2::$PC
	int32_t ___U24PC_5;
	// Utage.AdvEngineStarter/<LoadEngineAsyncSub>c__Iterator2/<LoadEngineAsyncSub>c__AnonStorey8 Utage.AdvEngineStarter/<LoadEngineAsyncSub>c__Iterator2::$locvar0
	U3CLoadEngineAsyncSubU3Ec__AnonStorey8_t684401054 * ___U24locvar0_6;

public:
	inline static int32_t get_offset_of_loadManifestFromCache_0() { return static_cast<int32_t>(offsetof(U3CLoadEngineAsyncSubU3Ec__Iterator2_t1722329282, ___loadManifestFromCache_0)); }
	inline bool get_loadManifestFromCache_0() const { return ___loadManifestFromCache_0; }
	inline bool* get_address_of_loadManifestFromCache_0() { return &___loadManifestFromCache_0; }
	inline void set_loadManifestFromCache_0(bool value)
	{
		___loadManifestFromCache_0 = value;
	}

	inline static int32_t get_offset_of_onFailed_1() { return static_cast<int32_t>(offsetof(U3CLoadEngineAsyncSubU3Ec__Iterator2_t1722329282, ___onFailed_1)); }
	inline Action_t3226471752 * get_onFailed_1() const { return ___onFailed_1; }
	inline Action_t3226471752 ** get_address_of_onFailed_1() { return &___onFailed_1; }
	inline void set_onFailed_1(Action_t3226471752 * value)
	{
		___onFailed_1 = value;
		Il2CppCodeGenWriteBarrier((&___onFailed_1), value);
	}

	inline static int32_t get_offset_of_U24this_2() { return static_cast<int32_t>(offsetof(U3CLoadEngineAsyncSubU3Ec__Iterator2_t1722329282, ___U24this_2)); }
	inline AdvEngineStarter_t330044300 * get_U24this_2() const { return ___U24this_2; }
	inline AdvEngineStarter_t330044300 ** get_address_of_U24this_2() { return &___U24this_2; }
	inline void set_U24this_2(AdvEngineStarter_t330044300 * value)
	{
		___U24this_2 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_2), value);
	}

	inline static int32_t get_offset_of_U24current_3() { return static_cast<int32_t>(offsetof(U3CLoadEngineAsyncSubU3Ec__Iterator2_t1722329282, ___U24current_3)); }
	inline RuntimeObject * get_U24current_3() const { return ___U24current_3; }
	inline RuntimeObject ** get_address_of_U24current_3() { return &___U24current_3; }
	inline void set_U24current_3(RuntimeObject * value)
	{
		___U24current_3 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_3), value);
	}

	inline static int32_t get_offset_of_U24disposing_4() { return static_cast<int32_t>(offsetof(U3CLoadEngineAsyncSubU3Ec__Iterator2_t1722329282, ___U24disposing_4)); }
	inline bool get_U24disposing_4() const { return ___U24disposing_4; }
	inline bool* get_address_of_U24disposing_4() { return &___U24disposing_4; }
	inline void set_U24disposing_4(bool value)
	{
		___U24disposing_4 = value;
	}

	inline static int32_t get_offset_of_U24PC_5() { return static_cast<int32_t>(offsetof(U3CLoadEngineAsyncSubU3Ec__Iterator2_t1722329282, ___U24PC_5)); }
	inline int32_t get_U24PC_5() const { return ___U24PC_5; }
	inline int32_t* get_address_of_U24PC_5() { return &___U24PC_5; }
	inline void set_U24PC_5(int32_t value)
	{
		___U24PC_5 = value;
	}

	inline static int32_t get_offset_of_U24locvar0_6() { return static_cast<int32_t>(offsetof(U3CLoadEngineAsyncSubU3Ec__Iterator2_t1722329282, ___U24locvar0_6)); }
	inline U3CLoadEngineAsyncSubU3Ec__AnonStorey8_t684401054 * get_U24locvar0_6() const { return ___U24locvar0_6; }
	inline U3CLoadEngineAsyncSubU3Ec__AnonStorey8_t684401054 ** get_address_of_U24locvar0_6() { return &___U24locvar0_6; }
	inline void set_U24locvar0_6(U3CLoadEngineAsyncSubU3Ec__AnonStorey8_t684401054 * value)
	{
		___U24locvar0_6 = value;
		Il2CppCodeGenWriteBarrier((&___U24locvar0_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CLOADENGINEASYNCSUBU3EC__ITERATOR2_T1722329282_H
#ifndef U3CCOPLAYVOICEU3EC__ITERATOR0_T886521713_H
#define U3CCOPLAYVOICEU3EC__ITERATOR0_T886521713_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SampleChatLogItem/<CoPlayVoice>c__Iterator0
struct  U3CCoPlayVoiceU3Ec__Iterator0_t886521713  : public RuntimeObject
{
public:
	// System.String SampleChatLogItem/<CoPlayVoice>c__Iterator0::voiceFileName
	String_t* ___voiceFileName_0;
	// Utage.AssetFile SampleChatLogItem/<CoPlayVoice>c__Iterator0::<file>__0
	RuntimeObject* ___U3CfileU3E__0_1;
	// Utage.SoundManager SampleChatLogItem/<CoPlayVoice>c__Iterator0::<manager>__0
	SoundManager_t1265124084 * ___U3CmanagerU3E__0_2;
	// System.String SampleChatLogItem/<CoPlayVoice>c__Iterator0::characterLabel
	String_t* ___characterLabel_3;
	// SampleChatLogItem SampleChatLogItem/<CoPlayVoice>c__Iterator0::$this
	SampleChatLogItem_t3744651319 * ___U24this_4;
	// System.Object SampleChatLogItem/<CoPlayVoice>c__Iterator0::$current
	RuntimeObject * ___U24current_5;
	// System.Boolean SampleChatLogItem/<CoPlayVoice>c__Iterator0::$disposing
	bool ___U24disposing_6;
	// System.Int32 SampleChatLogItem/<CoPlayVoice>c__Iterator0::$PC
	int32_t ___U24PC_7;

public:
	inline static int32_t get_offset_of_voiceFileName_0() { return static_cast<int32_t>(offsetof(U3CCoPlayVoiceU3Ec__Iterator0_t886521713, ___voiceFileName_0)); }
	inline String_t* get_voiceFileName_0() const { return ___voiceFileName_0; }
	inline String_t** get_address_of_voiceFileName_0() { return &___voiceFileName_0; }
	inline void set_voiceFileName_0(String_t* value)
	{
		___voiceFileName_0 = value;
		Il2CppCodeGenWriteBarrier((&___voiceFileName_0), value);
	}

	inline static int32_t get_offset_of_U3CfileU3E__0_1() { return static_cast<int32_t>(offsetof(U3CCoPlayVoiceU3Ec__Iterator0_t886521713, ___U3CfileU3E__0_1)); }
	inline RuntimeObject* get_U3CfileU3E__0_1() const { return ___U3CfileU3E__0_1; }
	inline RuntimeObject** get_address_of_U3CfileU3E__0_1() { return &___U3CfileU3E__0_1; }
	inline void set_U3CfileU3E__0_1(RuntimeObject* value)
	{
		___U3CfileU3E__0_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CfileU3E__0_1), value);
	}

	inline static int32_t get_offset_of_U3CmanagerU3E__0_2() { return static_cast<int32_t>(offsetof(U3CCoPlayVoiceU3Ec__Iterator0_t886521713, ___U3CmanagerU3E__0_2)); }
	inline SoundManager_t1265124084 * get_U3CmanagerU3E__0_2() const { return ___U3CmanagerU3E__0_2; }
	inline SoundManager_t1265124084 ** get_address_of_U3CmanagerU3E__0_2() { return &___U3CmanagerU3E__0_2; }
	inline void set_U3CmanagerU3E__0_2(SoundManager_t1265124084 * value)
	{
		___U3CmanagerU3E__0_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CmanagerU3E__0_2), value);
	}

	inline static int32_t get_offset_of_characterLabel_3() { return static_cast<int32_t>(offsetof(U3CCoPlayVoiceU3Ec__Iterator0_t886521713, ___characterLabel_3)); }
	inline String_t* get_characterLabel_3() const { return ___characterLabel_3; }
	inline String_t** get_address_of_characterLabel_3() { return &___characterLabel_3; }
	inline void set_characterLabel_3(String_t* value)
	{
		___characterLabel_3 = value;
		Il2CppCodeGenWriteBarrier((&___characterLabel_3), value);
	}

	inline static int32_t get_offset_of_U24this_4() { return static_cast<int32_t>(offsetof(U3CCoPlayVoiceU3Ec__Iterator0_t886521713, ___U24this_4)); }
	inline SampleChatLogItem_t3744651319 * get_U24this_4() const { return ___U24this_4; }
	inline SampleChatLogItem_t3744651319 ** get_address_of_U24this_4() { return &___U24this_4; }
	inline void set_U24this_4(SampleChatLogItem_t3744651319 * value)
	{
		___U24this_4 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_4), value);
	}

	inline static int32_t get_offset_of_U24current_5() { return static_cast<int32_t>(offsetof(U3CCoPlayVoiceU3Ec__Iterator0_t886521713, ___U24current_5)); }
	inline RuntimeObject * get_U24current_5() const { return ___U24current_5; }
	inline RuntimeObject ** get_address_of_U24current_5() { return &___U24current_5; }
	inline void set_U24current_5(RuntimeObject * value)
	{
		___U24current_5 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_5), value);
	}

	inline static int32_t get_offset_of_U24disposing_6() { return static_cast<int32_t>(offsetof(U3CCoPlayVoiceU3Ec__Iterator0_t886521713, ___U24disposing_6)); }
	inline bool get_U24disposing_6() const { return ___U24disposing_6; }
	inline bool* get_address_of_U24disposing_6() { return &___U24disposing_6; }
	inline void set_U24disposing_6(bool value)
	{
		___U24disposing_6 = value;
	}

	inline static int32_t get_offset_of_U24PC_7() { return static_cast<int32_t>(offsetof(U3CCoPlayVoiceU3Ec__Iterator0_t886521713, ___U24PC_7)); }
	inline int32_t get_U24PC_7() const { return ___U24PC_7; }
	inline int32_t* get_address_of_U24PC_7() { return &___U24PC_7; }
	inline void set_U24PC_7(int32_t value)
	{
		___U24PC_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CCOPLAYVOICEU3EC__ITERATOR0_T886521713_H
#ifndef U3CFINDMODELU3EC__ANONSTOREY1_T390684174_H
#define U3CFINDMODELU3EC__ANONSTOREY1_T390684174_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UtageRecieveMessageFromAdvComannd/<FindModel>c__AnonStorey1
struct  U3CFindModelU3Ec__AnonStorey1_t390684174  : public RuntimeObject
{
public:
	// System.String UtageRecieveMessageFromAdvComannd/<FindModel>c__AnonStorey1::name
	String_t* ___name_0;

public:
	inline static int32_t get_offset_of_name_0() { return static_cast<int32_t>(offsetof(U3CFindModelU3Ec__AnonStorey1_t390684174, ___name_0)); }
	inline String_t* get_name_0() const { return ___name_0; }
	inline String_t** get_address_of_name_0() { return &___name_0; }
	inline void set_name_0(String_t* value)
	{
		___name_0 = value;
		Il2CppCodeGenWriteBarrier((&___name_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CFINDMODELU3EC__ANONSTOREY1_T390684174_H
#ifndef U3CRANDOMCHANGEU3EC__ITERATOR0_T923176863_H
#define U3CRANDOMCHANGEU3EC__ITERATOR0_T923176863_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityChan.AutoBlinkforSD/<RandomChange>c__Iterator0
struct  U3CRandomChangeU3Ec__Iterator0_t923176863  : public RuntimeObject
{
public:
	// System.Single UnityChan.AutoBlinkforSD/<RandomChange>c__Iterator0::<_seed>__1
	float ___U3C_seedU3E__1_0;
	// UnityChan.AutoBlinkforSD UnityChan.AutoBlinkforSD/<RandomChange>c__Iterator0::$this
	AutoBlinkforSD_t2101017347 * ___U24this_1;
	// System.Object UnityChan.AutoBlinkforSD/<RandomChange>c__Iterator0::$current
	RuntimeObject * ___U24current_2;
	// System.Boolean UnityChan.AutoBlinkforSD/<RandomChange>c__Iterator0::$disposing
	bool ___U24disposing_3;
	// System.Int32 UnityChan.AutoBlinkforSD/<RandomChange>c__Iterator0::$PC
	int32_t ___U24PC_4;

public:
	inline static int32_t get_offset_of_U3C_seedU3E__1_0() { return static_cast<int32_t>(offsetof(U3CRandomChangeU3Ec__Iterator0_t923176863, ___U3C_seedU3E__1_0)); }
	inline float get_U3C_seedU3E__1_0() const { return ___U3C_seedU3E__1_0; }
	inline float* get_address_of_U3C_seedU3E__1_0() { return &___U3C_seedU3E__1_0; }
	inline void set_U3C_seedU3E__1_0(float value)
	{
		___U3C_seedU3E__1_0 = value;
	}

	inline static int32_t get_offset_of_U24this_1() { return static_cast<int32_t>(offsetof(U3CRandomChangeU3Ec__Iterator0_t923176863, ___U24this_1)); }
	inline AutoBlinkforSD_t2101017347 * get_U24this_1() const { return ___U24this_1; }
	inline AutoBlinkforSD_t2101017347 ** get_address_of_U24this_1() { return &___U24this_1; }
	inline void set_U24this_1(AutoBlinkforSD_t2101017347 * value)
	{
		___U24this_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_1), value);
	}

	inline static int32_t get_offset_of_U24current_2() { return static_cast<int32_t>(offsetof(U3CRandomChangeU3Ec__Iterator0_t923176863, ___U24current_2)); }
	inline RuntimeObject * get_U24current_2() const { return ___U24current_2; }
	inline RuntimeObject ** get_address_of_U24current_2() { return &___U24current_2; }
	inline void set_U24current_2(RuntimeObject * value)
	{
		___U24current_2 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_2), value);
	}

	inline static int32_t get_offset_of_U24disposing_3() { return static_cast<int32_t>(offsetof(U3CRandomChangeU3Ec__Iterator0_t923176863, ___U24disposing_3)); }
	inline bool get_U24disposing_3() const { return ___U24disposing_3; }
	inline bool* get_address_of_U24disposing_3() { return &___U24disposing_3; }
	inline void set_U24disposing_3(bool value)
	{
		___U24disposing_3 = value;
	}

	inline static int32_t get_offset_of_U24PC_4() { return static_cast<int32_t>(offsetof(U3CRandomChangeU3Ec__Iterator0_t923176863, ___U24PC_4)); }
	inline int32_t get_U24PC_4() const { return ___U24PC_4; }
	inline int32_t* get_address_of_U24PC_4() { return &___U24PC_4; }
	inline void set_U24PC_4(int32_t value)
	{
		___U24PC_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CRANDOMCHANGEU3EC__ITERATOR0_T923176863_H
#ifndef U3CTRANSSCENEU3EC__ITERATOR0_T3372958951_H
#define U3CTRANSSCENEU3EC__ITERATOR0_T3372958951_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// FadeManager/<TransScene>c__Iterator0
struct  U3CTransSceneU3Ec__Iterator0_t3372958951  : public RuntimeObject
{
public:
	// System.Single FadeManager/<TransScene>c__Iterator0::<time>__0
	float ___U3CtimeU3E__0_0;
	// System.Single FadeManager/<TransScene>c__Iterator0::interval
	float ___interval_1;
	// System.String FadeManager/<TransScene>c__Iterator0::scene
	String_t* ___scene_2;
	// FadeManager FadeManager/<TransScene>c__Iterator0::$this
	FadeManager_t2205509615 * ___U24this_3;
	// System.Object FadeManager/<TransScene>c__Iterator0::$current
	RuntimeObject * ___U24current_4;
	// System.Boolean FadeManager/<TransScene>c__Iterator0::$disposing
	bool ___U24disposing_5;
	// System.Int32 FadeManager/<TransScene>c__Iterator0::$PC
	int32_t ___U24PC_6;

public:
	inline static int32_t get_offset_of_U3CtimeU3E__0_0() { return static_cast<int32_t>(offsetof(U3CTransSceneU3Ec__Iterator0_t3372958951, ___U3CtimeU3E__0_0)); }
	inline float get_U3CtimeU3E__0_0() const { return ___U3CtimeU3E__0_0; }
	inline float* get_address_of_U3CtimeU3E__0_0() { return &___U3CtimeU3E__0_0; }
	inline void set_U3CtimeU3E__0_0(float value)
	{
		___U3CtimeU3E__0_0 = value;
	}

	inline static int32_t get_offset_of_interval_1() { return static_cast<int32_t>(offsetof(U3CTransSceneU3Ec__Iterator0_t3372958951, ___interval_1)); }
	inline float get_interval_1() const { return ___interval_1; }
	inline float* get_address_of_interval_1() { return &___interval_1; }
	inline void set_interval_1(float value)
	{
		___interval_1 = value;
	}

	inline static int32_t get_offset_of_scene_2() { return static_cast<int32_t>(offsetof(U3CTransSceneU3Ec__Iterator0_t3372958951, ___scene_2)); }
	inline String_t* get_scene_2() const { return ___scene_2; }
	inline String_t** get_address_of_scene_2() { return &___scene_2; }
	inline void set_scene_2(String_t* value)
	{
		___scene_2 = value;
		Il2CppCodeGenWriteBarrier((&___scene_2), value);
	}

	inline static int32_t get_offset_of_U24this_3() { return static_cast<int32_t>(offsetof(U3CTransSceneU3Ec__Iterator0_t3372958951, ___U24this_3)); }
	inline FadeManager_t2205509615 * get_U24this_3() const { return ___U24this_3; }
	inline FadeManager_t2205509615 ** get_address_of_U24this_3() { return &___U24this_3; }
	inline void set_U24this_3(FadeManager_t2205509615 * value)
	{
		___U24this_3 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_3), value);
	}

	inline static int32_t get_offset_of_U24current_4() { return static_cast<int32_t>(offsetof(U3CTransSceneU3Ec__Iterator0_t3372958951, ___U24current_4)); }
	inline RuntimeObject * get_U24current_4() const { return ___U24current_4; }
	inline RuntimeObject ** get_address_of_U24current_4() { return &___U24current_4; }
	inline void set_U24current_4(RuntimeObject * value)
	{
		___U24current_4 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_4), value);
	}

	inline static int32_t get_offset_of_U24disposing_5() { return static_cast<int32_t>(offsetof(U3CTransSceneU3Ec__Iterator0_t3372958951, ___U24disposing_5)); }
	inline bool get_U24disposing_5() const { return ___U24disposing_5; }
	inline bool* get_address_of_U24disposing_5() { return &___U24disposing_5; }
	inline void set_U24disposing_5(bool value)
	{
		___U24disposing_5 = value;
	}

	inline static int32_t get_offset_of_U24PC_6() { return static_cast<int32_t>(offsetof(U3CTransSceneU3Ec__Iterator0_t3372958951, ___U24PC_6)); }
	inline int32_t get_U24PC_6() const { return ___U24PC_6; }
	inline int32_t* get_address_of_U24PC_6() { return &___U24PC_6; }
	inline void set_U24PC_6(int32_t value)
	{
		___U24PC_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CTRANSSCENEU3EC__ITERATOR0_T3372958951_H
#ifndef U3CCOPLAYENGINEU3EC__ITERATOR2_T2048207881_H
#define U3CCOPLAYENGINEU3EC__ITERATOR2_T2048207881_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.SampleCustomAssetBundleLoad/<CoPlayEngine>c__Iterator2
struct  U3CCoPlayEngineU3Ec__Iterator2_t2048207881  : public RuntimeObject
{
public:
	// Utage.SampleCustomAssetBundleLoad Utage.SampleCustomAssetBundleLoad/<CoPlayEngine>c__Iterator2::$this
	SampleCustomAssetBundleLoad_t3658166813 * ___U24this_0;
	// System.Object Utage.SampleCustomAssetBundleLoad/<CoPlayEngine>c__Iterator2::$current
	RuntimeObject * ___U24current_1;
	// System.Boolean Utage.SampleCustomAssetBundleLoad/<CoPlayEngine>c__Iterator2::$disposing
	bool ___U24disposing_2;
	// System.Int32 Utage.SampleCustomAssetBundleLoad/<CoPlayEngine>c__Iterator2::$PC
	int32_t ___U24PC_3;

public:
	inline static int32_t get_offset_of_U24this_0() { return static_cast<int32_t>(offsetof(U3CCoPlayEngineU3Ec__Iterator2_t2048207881, ___U24this_0)); }
	inline SampleCustomAssetBundleLoad_t3658166813 * get_U24this_0() const { return ___U24this_0; }
	inline SampleCustomAssetBundleLoad_t3658166813 ** get_address_of_U24this_0() { return &___U24this_0; }
	inline void set_U24this_0(SampleCustomAssetBundleLoad_t3658166813 * value)
	{
		___U24this_0 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_0), value);
	}

	inline static int32_t get_offset_of_U24current_1() { return static_cast<int32_t>(offsetof(U3CCoPlayEngineU3Ec__Iterator2_t2048207881, ___U24current_1)); }
	inline RuntimeObject * get_U24current_1() const { return ___U24current_1; }
	inline RuntimeObject ** get_address_of_U24current_1() { return &___U24current_1; }
	inline void set_U24current_1(RuntimeObject * value)
	{
		___U24current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_1), value);
	}

	inline static int32_t get_offset_of_U24disposing_2() { return static_cast<int32_t>(offsetof(U3CCoPlayEngineU3Ec__Iterator2_t2048207881, ___U24disposing_2)); }
	inline bool get_U24disposing_2() const { return ___U24disposing_2; }
	inline bool* get_address_of_U24disposing_2() { return &___U24disposing_2; }
	inline void set_U24disposing_2(bool value)
	{
		___U24disposing_2 = value;
	}

	inline static int32_t get_offset_of_U24PC_3() { return static_cast<int32_t>(offsetof(U3CCoPlayEngineU3Ec__Iterator2_t2048207881, ___U24PC_3)); }
	inline int32_t get_U24PC_3() const { return ___U24PC_3; }
	inline int32_t* get_address_of_U24PC_3() { return &___U24PC_3; }
	inline void set_U24PC_3(int32_t value)
	{
		___U24PC_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CCOPLAYENGINEU3EC__ITERATOR2_T2048207881_H
#ifndef U3CLOADSCENARIOSASYNCU3EC__ITERATOR1_T1642848913_H
#define U3CLOADSCENARIOSASYNCU3EC__ITERATOR1_T1642848913_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.SampleCustomAssetBundleLoad/<LoadScenariosAsync>c__Iterator1
struct  U3CLoadScenariosAsyncU3Ec__Iterator1_t1642848913  : public RuntimeObject
{
public:
	// System.String Utage.SampleCustomAssetBundleLoad/<LoadScenariosAsync>c__Iterator1::url
	String_t* ___url_0;
	// Utage.AssetFile Utage.SampleCustomAssetBundleLoad/<LoadScenariosAsync>c__Iterator1::<file>__0
	RuntimeObject* ___U3CfileU3E__0_1;
	// Utage.AdvImportScenarios Utage.SampleCustomAssetBundleLoad/<LoadScenariosAsync>c__Iterator1::<scenarios>__0
	AdvImportScenarios_t1226385437 * ___U3CscenariosU3E__0_2;
	// Utage.SampleCustomAssetBundleLoad Utage.SampleCustomAssetBundleLoad/<LoadScenariosAsync>c__Iterator1::$this
	SampleCustomAssetBundleLoad_t3658166813 * ___U24this_3;
	// System.Object Utage.SampleCustomAssetBundleLoad/<LoadScenariosAsync>c__Iterator1::$current
	RuntimeObject * ___U24current_4;
	// System.Boolean Utage.SampleCustomAssetBundleLoad/<LoadScenariosAsync>c__Iterator1::$disposing
	bool ___U24disposing_5;
	// System.Int32 Utage.SampleCustomAssetBundleLoad/<LoadScenariosAsync>c__Iterator1::$PC
	int32_t ___U24PC_6;

public:
	inline static int32_t get_offset_of_url_0() { return static_cast<int32_t>(offsetof(U3CLoadScenariosAsyncU3Ec__Iterator1_t1642848913, ___url_0)); }
	inline String_t* get_url_0() const { return ___url_0; }
	inline String_t** get_address_of_url_0() { return &___url_0; }
	inline void set_url_0(String_t* value)
	{
		___url_0 = value;
		Il2CppCodeGenWriteBarrier((&___url_0), value);
	}

	inline static int32_t get_offset_of_U3CfileU3E__0_1() { return static_cast<int32_t>(offsetof(U3CLoadScenariosAsyncU3Ec__Iterator1_t1642848913, ___U3CfileU3E__0_1)); }
	inline RuntimeObject* get_U3CfileU3E__0_1() const { return ___U3CfileU3E__0_1; }
	inline RuntimeObject** get_address_of_U3CfileU3E__0_1() { return &___U3CfileU3E__0_1; }
	inline void set_U3CfileU3E__0_1(RuntimeObject* value)
	{
		___U3CfileU3E__0_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CfileU3E__0_1), value);
	}

	inline static int32_t get_offset_of_U3CscenariosU3E__0_2() { return static_cast<int32_t>(offsetof(U3CLoadScenariosAsyncU3Ec__Iterator1_t1642848913, ___U3CscenariosU3E__0_2)); }
	inline AdvImportScenarios_t1226385437 * get_U3CscenariosU3E__0_2() const { return ___U3CscenariosU3E__0_2; }
	inline AdvImportScenarios_t1226385437 ** get_address_of_U3CscenariosU3E__0_2() { return &___U3CscenariosU3E__0_2; }
	inline void set_U3CscenariosU3E__0_2(AdvImportScenarios_t1226385437 * value)
	{
		___U3CscenariosU3E__0_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CscenariosU3E__0_2), value);
	}

	inline static int32_t get_offset_of_U24this_3() { return static_cast<int32_t>(offsetof(U3CLoadScenariosAsyncU3Ec__Iterator1_t1642848913, ___U24this_3)); }
	inline SampleCustomAssetBundleLoad_t3658166813 * get_U24this_3() const { return ___U24this_3; }
	inline SampleCustomAssetBundleLoad_t3658166813 ** get_address_of_U24this_3() { return &___U24this_3; }
	inline void set_U24this_3(SampleCustomAssetBundleLoad_t3658166813 * value)
	{
		___U24this_3 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_3), value);
	}

	inline static int32_t get_offset_of_U24current_4() { return static_cast<int32_t>(offsetof(U3CLoadScenariosAsyncU3Ec__Iterator1_t1642848913, ___U24current_4)); }
	inline RuntimeObject * get_U24current_4() const { return ___U24current_4; }
	inline RuntimeObject ** get_address_of_U24current_4() { return &___U24current_4; }
	inline void set_U24current_4(RuntimeObject * value)
	{
		___U24current_4 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_4), value);
	}

	inline static int32_t get_offset_of_U24disposing_5() { return static_cast<int32_t>(offsetof(U3CLoadScenariosAsyncU3Ec__Iterator1_t1642848913, ___U24disposing_5)); }
	inline bool get_U24disposing_5() const { return ___U24disposing_5; }
	inline bool* get_address_of_U24disposing_5() { return &___U24disposing_5; }
	inline void set_U24disposing_5(bool value)
	{
		___U24disposing_5 = value;
	}

	inline static int32_t get_offset_of_U24PC_6() { return static_cast<int32_t>(offsetof(U3CLoadScenariosAsyncU3Ec__Iterator1_t1642848913, ___U24PC_6)); }
	inline int32_t get_U24PC_6() const { return ___U24PC_6; }
	inline int32_t* get_address_of_U24PC_6() { return &___U24PC_6; }
	inline void set_U24PC_6(int32_t value)
	{
		___U24PC_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CLOADSCENARIOSASYNCU3EC__ITERATOR1_T1642848913_H
#ifndef U3CRANDOMCHANGEU3EC__ITERATOR0_T3991869552_H
#define U3CRANDOMCHANGEU3EC__ITERATOR0_T3991869552_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityChan.IdleChanger/<RandomChange>c__Iterator0
struct  U3CRandomChangeU3Ec__Iterator0_t3991869552  : public RuntimeObject
{
public:
	// UnityChan.IdleChanger UnityChan.IdleChanger/<RandomChange>c__Iterator0::$this
	IdleChanger_t2567672470 * ___U24this_0;
	// System.Object UnityChan.IdleChanger/<RandomChange>c__Iterator0::$current
	RuntimeObject * ___U24current_1;
	// System.Boolean UnityChan.IdleChanger/<RandomChange>c__Iterator0::$disposing
	bool ___U24disposing_2;
	// System.Int32 UnityChan.IdleChanger/<RandomChange>c__Iterator0::$PC
	int32_t ___U24PC_3;

public:
	inline static int32_t get_offset_of_U24this_0() { return static_cast<int32_t>(offsetof(U3CRandomChangeU3Ec__Iterator0_t3991869552, ___U24this_0)); }
	inline IdleChanger_t2567672470 * get_U24this_0() const { return ___U24this_0; }
	inline IdleChanger_t2567672470 ** get_address_of_U24this_0() { return &___U24this_0; }
	inline void set_U24this_0(IdleChanger_t2567672470 * value)
	{
		___U24this_0 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_0), value);
	}

	inline static int32_t get_offset_of_U24current_1() { return static_cast<int32_t>(offsetof(U3CRandomChangeU3Ec__Iterator0_t3991869552, ___U24current_1)); }
	inline RuntimeObject * get_U24current_1() const { return ___U24current_1; }
	inline RuntimeObject ** get_address_of_U24current_1() { return &___U24current_1; }
	inline void set_U24current_1(RuntimeObject * value)
	{
		___U24current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_1), value);
	}

	inline static int32_t get_offset_of_U24disposing_2() { return static_cast<int32_t>(offsetof(U3CRandomChangeU3Ec__Iterator0_t3991869552, ___U24disposing_2)); }
	inline bool get_U24disposing_2() const { return ___U24disposing_2; }
	inline bool* get_address_of_U24disposing_2() { return &___U24disposing_2; }
	inline void set_U24disposing_2(bool value)
	{
		___U24disposing_2 = value;
	}

	inline static int32_t get_offset_of_U24PC_3() { return static_cast<int32_t>(offsetof(U3CRandomChangeU3Ec__Iterator0_t3991869552, ___U24PC_3)); }
	inline int32_t get_U24PC_3() const { return ___U24PC_3; }
	inline int32_t* get_address_of_U24PC_3() { return &___U24PC_3; }
	inline void set_U24PC_3(int32_t value)
	{
		___U24PC_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CRANDOMCHANGEU3EC__ITERATOR0_T3991869552_H
#ifndef VALUETYPE_T3507792607_H
#define VALUETYPE_T3507792607_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ValueType
struct  ValueType_t3507792607  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t3507792607_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t3507792607_marshaled_com
{
};
#endif // VALUETYPE_T3507792607_H
#ifndef UNITYEVENTBASE_T828812576_H
#define UNITYEVENTBASE_T828812576_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Events.UnityEventBase
struct  UnityEventBase_t828812576  : public RuntimeObject
{
public:
	// UnityEngine.Events.InvokableCallList UnityEngine.Events.UnityEventBase::m_Calls
	InvokableCallList_t2295673753 * ___m_Calls_0;
	// UnityEngine.Events.PersistentCallGroup UnityEngine.Events.UnityEventBase::m_PersistentCalls
	PersistentCallGroup_t339478082 * ___m_PersistentCalls_1;
	// System.String UnityEngine.Events.UnityEventBase::m_TypeName
	String_t* ___m_TypeName_2;
	// System.Boolean UnityEngine.Events.UnityEventBase::m_CallsDirty
	bool ___m_CallsDirty_3;

public:
	inline static int32_t get_offset_of_m_Calls_0() { return static_cast<int32_t>(offsetof(UnityEventBase_t828812576, ___m_Calls_0)); }
	inline InvokableCallList_t2295673753 * get_m_Calls_0() const { return ___m_Calls_0; }
	inline InvokableCallList_t2295673753 ** get_address_of_m_Calls_0() { return &___m_Calls_0; }
	inline void set_m_Calls_0(InvokableCallList_t2295673753 * value)
	{
		___m_Calls_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_Calls_0), value);
	}

	inline static int32_t get_offset_of_m_PersistentCalls_1() { return static_cast<int32_t>(offsetof(UnityEventBase_t828812576, ___m_PersistentCalls_1)); }
	inline PersistentCallGroup_t339478082 * get_m_PersistentCalls_1() const { return ___m_PersistentCalls_1; }
	inline PersistentCallGroup_t339478082 ** get_address_of_m_PersistentCalls_1() { return &___m_PersistentCalls_1; }
	inline void set_m_PersistentCalls_1(PersistentCallGroup_t339478082 * value)
	{
		___m_PersistentCalls_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_PersistentCalls_1), value);
	}

	inline static int32_t get_offset_of_m_TypeName_2() { return static_cast<int32_t>(offsetof(UnityEventBase_t828812576, ___m_TypeName_2)); }
	inline String_t* get_m_TypeName_2() const { return ___m_TypeName_2; }
	inline String_t** get_address_of_m_TypeName_2() { return &___m_TypeName_2; }
	inline void set_m_TypeName_2(String_t* value)
	{
		___m_TypeName_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_TypeName_2), value);
	}

	inline static int32_t get_offset_of_m_CallsDirty_3() { return static_cast<int32_t>(offsetof(UnityEventBase_t828812576, ___m_CallsDirty_3)); }
	inline bool get_m_CallsDirty_3() const { return ___m_CallsDirty_3; }
	inline bool* get_address_of_m_CallsDirty_3() { return &___m_CallsDirty_3; }
	inline void set_m_CallsDirty_3(bool value)
	{
		___m_CallsDirty_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYEVENTBASE_T828812576_H
#ifndef SAMPLEASSETBUNDLEVERSIONINFO_T1314599396_H
#define SAMPLEASSETBUNDLEVERSIONINFO_T1314599396_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.SampleCustomAssetBundleLoad/SampleAssetBundleVersionInfo
struct  SampleAssetBundleVersionInfo_t1314599396  : public RuntimeObject
{
public:
	// System.String Utage.SampleCustomAssetBundleLoad/SampleAssetBundleVersionInfo::resourcePath
	String_t* ___resourcePath_0;
	// System.String Utage.SampleCustomAssetBundleLoad/SampleAssetBundleVersionInfo::url
	String_t* ___url_1;
	// System.Int32 Utage.SampleCustomAssetBundleLoad/SampleAssetBundleVersionInfo::version
	int32_t ___version_2;

public:
	inline static int32_t get_offset_of_resourcePath_0() { return static_cast<int32_t>(offsetof(SampleAssetBundleVersionInfo_t1314599396, ___resourcePath_0)); }
	inline String_t* get_resourcePath_0() const { return ___resourcePath_0; }
	inline String_t** get_address_of_resourcePath_0() { return &___resourcePath_0; }
	inline void set_resourcePath_0(String_t* value)
	{
		___resourcePath_0 = value;
		Il2CppCodeGenWriteBarrier((&___resourcePath_0), value);
	}

	inline static int32_t get_offset_of_url_1() { return static_cast<int32_t>(offsetof(SampleAssetBundleVersionInfo_t1314599396, ___url_1)); }
	inline String_t* get_url_1() const { return ___url_1; }
	inline String_t** get_address_of_url_1() { return &___url_1; }
	inline void set_url_1(String_t* value)
	{
		___url_1 = value;
		Il2CppCodeGenWriteBarrier((&___url_1), value);
	}

	inline static int32_t get_offset_of_version_2() { return static_cast<int32_t>(offsetof(SampleAssetBundleVersionInfo_t1314599396, ___version_2)); }
	inline int32_t get_version_2() const { return ___version_2; }
	inline int32_t* get_address_of_version_2() { return &___version_2; }
	inline void set_version_2(int32_t value)
	{
		___version_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SAMPLEASSETBUNDLEVERSIONINFO_T1314599396_H
#ifndef U3CRANDOMCHANGEU3EC__ITERATOR0_T2567771009_H
#define U3CRANDOMCHANGEU3EC__ITERATOR0_T2567771009_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityChan.RandomWind/<RandomChange>c__Iterator0
struct  U3CRandomChangeU3Ec__Iterator0_t2567771009  : public RuntimeObject
{
public:
	// System.Single UnityChan.RandomWind/<RandomChange>c__Iterator0::<_seed>__1
	float ___U3C_seedU3E__1_0;
	// UnityChan.RandomWind UnityChan.RandomWind/<RandomChange>c__Iterator0::$this
	RandomWind_t3585812647 * ___U24this_1;
	// System.Object UnityChan.RandomWind/<RandomChange>c__Iterator0::$current
	RuntimeObject * ___U24current_2;
	// System.Boolean UnityChan.RandomWind/<RandomChange>c__Iterator0::$disposing
	bool ___U24disposing_3;
	// System.Int32 UnityChan.RandomWind/<RandomChange>c__Iterator0::$PC
	int32_t ___U24PC_4;

public:
	inline static int32_t get_offset_of_U3C_seedU3E__1_0() { return static_cast<int32_t>(offsetof(U3CRandomChangeU3Ec__Iterator0_t2567771009, ___U3C_seedU3E__1_0)); }
	inline float get_U3C_seedU3E__1_0() const { return ___U3C_seedU3E__1_0; }
	inline float* get_address_of_U3C_seedU3E__1_0() { return &___U3C_seedU3E__1_0; }
	inline void set_U3C_seedU3E__1_0(float value)
	{
		___U3C_seedU3E__1_0 = value;
	}

	inline static int32_t get_offset_of_U24this_1() { return static_cast<int32_t>(offsetof(U3CRandomChangeU3Ec__Iterator0_t2567771009, ___U24this_1)); }
	inline RandomWind_t3585812647 * get_U24this_1() const { return ___U24this_1; }
	inline RandomWind_t3585812647 ** get_address_of_U24this_1() { return &___U24this_1; }
	inline void set_U24this_1(RandomWind_t3585812647 * value)
	{
		___U24this_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_1), value);
	}

	inline static int32_t get_offset_of_U24current_2() { return static_cast<int32_t>(offsetof(U3CRandomChangeU3Ec__Iterator0_t2567771009, ___U24current_2)); }
	inline RuntimeObject * get_U24current_2() const { return ___U24current_2; }
	inline RuntimeObject ** get_address_of_U24current_2() { return &___U24current_2; }
	inline void set_U24current_2(RuntimeObject * value)
	{
		___U24current_2 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_2), value);
	}

	inline static int32_t get_offset_of_U24disposing_3() { return static_cast<int32_t>(offsetof(U3CRandomChangeU3Ec__Iterator0_t2567771009, ___U24disposing_3)); }
	inline bool get_U24disposing_3() const { return ___U24disposing_3; }
	inline bool* get_address_of_U24disposing_3() { return &___U24disposing_3; }
	inline void set_U24disposing_3(bool value)
	{
		___U24disposing_3 = value;
	}

	inline static int32_t get_offset_of_U24PC_4() { return static_cast<int32_t>(offsetof(U3CRandomChangeU3Ec__Iterator0_t2567771009, ___U24PC_4)); }
	inline int32_t get_U24PC_4() const { return ___U24PC_4; }
	inline int32_t* get_address_of_U24PC_4() { return &___U24PC_4; }
	inline void set_U24PC_4(int32_t value)
	{
		___U24PC_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CRANDOMCHANGEU3EC__ITERATOR0_T2567771009_H
#ifndef U3CRANDOMCHANGEU3EC__ITERATOR0_T1939068071_H
#define U3CRANDOMCHANGEU3EC__ITERATOR0_T1939068071_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityChan.AutoBlink/<RandomChange>c__Iterator0
struct  U3CRandomChangeU3Ec__Iterator0_t1939068071  : public RuntimeObject
{
public:
	// System.Single UnityChan.AutoBlink/<RandomChange>c__Iterator0::<_seed>__1
	float ___U3C_seedU3E__1_0;
	// UnityChan.AutoBlink UnityChan.AutoBlink/<RandomChange>c__Iterator0::$this
	AutoBlink_t2157783575 * ___U24this_1;
	// System.Object UnityChan.AutoBlink/<RandomChange>c__Iterator0::$current
	RuntimeObject * ___U24current_2;
	// System.Boolean UnityChan.AutoBlink/<RandomChange>c__Iterator0::$disposing
	bool ___U24disposing_3;
	// System.Int32 UnityChan.AutoBlink/<RandomChange>c__Iterator0::$PC
	int32_t ___U24PC_4;

public:
	inline static int32_t get_offset_of_U3C_seedU3E__1_0() { return static_cast<int32_t>(offsetof(U3CRandomChangeU3Ec__Iterator0_t1939068071, ___U3C_seedU3E__1_0)); }
	inline float get_U3C_seedU3E__1_0() const { return ___U3C_seedU3E__1_0; }
	inline float* get_address_of_U3C_seedU3E__1_0() { return &___U3C_seedU3E__1_0; }
	inline void set_U3C_seedU3E__1_0(float value)
	{
		___U3C_seedU3E__1_0 = value;
	}

	inline static int32_t get_offset_of_U24this_1() { return static_cast<int32_t>(offsetof(U3CRandomChangeU3Ec__Iterator0_t1939068071, ___U24this_1)); }
	inline AutoBlink_t2157783575 * get_U24this_1() const { return ___U24this_1; }
	inline AutoBlink_t2157783575 ** get_address_of_U24this_1() { return &___U24this_1; }
	inline void set_U24this_1(AutoBlink_t2157783575 * value)
	{
		___U24this_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_1), value);
	}

	inline static int32_t get_offset_of_U24current_2() { return static_cast<int32_t>(offsetof(U3CRandomChangeU3Ec__Iterator0_t1939068071, ___U24current_2)); }
	inline RuntimeObject * get_U24current_2() const { return ___U24current_2; }
	inline RuntimeObject ** get_address_of_U24current_2() { return &___U24current_2; }
	inline void set_U24current_2(RuntimeObject * value)
	{
		___U24current_2 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_2), value);
	}

	inline static int32_t get_offset_of_U24disposing_3() { return static_cast<int32_t>(offsetof(U3CRandomChangeU3Ec__Iterator0_t1939068071, ___U24disposing_3)); }
	inline bool get_U24disposing_3() const { return ___U24disposing_3; }
	inline bool* get_address_of_U24disposing_3() { return &___U24disposing_3; }
	inline void set_U24disposing_3(bool value)
	{
		___U24disposing_3 = value;
	}

	inline static int32_t get_offset_of_U24PC_4() { return static_cast<int32_t>(offsetof(U3CRandomChangeU3Ec__Iterator0_t1939068071, ___U24PC_4)); }
	inline int32_t get_U24PC_4() const { return ___U24PC_4; }
	inline int32_t* get_address_of_U24PC_4() { return &___U24PC_4; }
	inline void set_U24PC_4(int32_t value)
	{
		___U24PC_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CRANDOMCHANGEU3EC__ITERATOR0_T1939068071_H
#ifndef U3CCOTALKU3EC__ITERATOR0_T2384567481_H
#define U3CCOTALKU3EC__ITERATOR0_T2384567481_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TalkScript/<CoTalk>c__Iterator0
struct  U3CCoTalkU3Ec__Iterator0_t2384567481  : public RuntimeObject
{
public:
	// System.String TalkScript/<CoTalk>c__Iterator0::<scenarioLabel>__0
	String_t* ___U3CscenarioLabelU3E__0_0;
	// TalkScript TalkScript/<CoTalk>c__Iterator0::$this
	TalkScript_t915888587 * ___U24this_1;
	// System.Object TalkScript/<CoTalk>c__Iterator0::$current
	RuntimeObject * ___U24current_2;
	// System.Boolean TalkScript/<CoTalk>c__Iterator0::$disposing
	bool ___U24disposing_3;
	// System.Int32 TalkScript/<CoTalk>c__Iterator0::$PC
	int32_t ___U24PC_4;

public:
	inline static int32_t get_offset_of_U3CscenarioLabelU3E__0_0() { return static_cast<int32_t>(offsetof(U3CCoTalkU3Ec__Iterator0_t2384567481, ___U3CscenarioLabelU3E__0_0)); }
	inline String_t* get_U3CscenarioLabelU3E__0_0() const { return ___U3CscenarioLabelU3E__0_0; }
	inline String_t** get_address_of_U3CscenarioLabelU3E__0_0() { return &___U3CscenarioLabelU3E__0_0; }
	inline void set_U3CscenarioLabelU3E__0_0(String_t* value)
	{
		___U3CscenarioLabelU3E__0_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CscenarioLabelU3E__0_0), value);
	}

	inline static int32_t get_offset_of_U24this_1() { return static_cast<int32_t>(offsetof(U3CCoTalkU3Ec__Iterator0_t2384567481, ___U24this_1)); }
	inline TalkScript_t915888587 * get_U24this_1() const { return ___U24this_1; }
	inline TalkScript_t915888587 ** get_address_of_U24this_1() { return &___U24this_1; }
	inline void set_U24this_1(TalkScript_t915888587 * value)
	{
		___U24this_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_1), value);
	}

	inline static int32_t get_offset_of_U24current_2() { return static_cast<int32_t>(offsetof(U3CCoTalkU3Ec__Iterator0_t2384567481, ___U24current_2)); }
	inline RuntimeObject * get_U24current_2() const { return ___U24current_2; }
	inline RuntimeObject ** get_address_of_U24current_2() { return &___U24current_2; }
	inline void set_U24current_2(RuntimeObject * value)
	{
		___U24current_2 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_2), value);
	}

	inline static int32_t get_offset_of_U24disposing_3() { return static_cast<int32_t>(offsetof(U3CCoTalkU3Ec__Iterator0_t2384567481, ___U24disposing_3)); }
	inline bool get_U24disposing_3() const { return ___U24disposing_3; }
	inline bool* get_address_of_U24disposing_3() { return &___U24disposing_3; }
	inline void set_U24disposing_3(bool value)
	{
		___U24disposing_3 = value;
	}

	inline static int32_t get_offset_of_U24PC_4() { return static_cast<int32_t>(offsetof(U3CCoTalkU3Ec__Iterator0_t2384567481, ___U24PC_4)); }
	inline int32_t get_U24PC_4() const { return ___U24PC_4; }
	inline int32_t* get_address_of_U24PC_4() { return &___U24PC_4; }
	inline void set_U24PC_4(int32_t value)
	{
		___U24PC_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CCOTALKU3EC__ITERATOR0_T2384567481_H
#ifndef U3CCOBOOTFROMEXPORTDATAU3EC__ITERATOR0_T2686201055_H
#define U3CCOBOOTFROMEXPORTDATAU3EC__ITERATOR0_T2686201055_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.AdvEngine/<CoBootFromExportData>c__Iterator0
struct  U3CCoBootFromExportDataU3Ec__Iterator0_t2686201055  : public RuntimeObject
{
public:
	// Utage.AdvImportScenarios Utage.AdvEngine/<CoBootFromExportData>c__Iterator0::scenarios
	AdvImportScenarios_t1226385437 * ___scenarios_0;
	// System.String Utage.AdvEngine/<CoBootFromExportData>c__Iterator0::resourceDir
	String_t* ___resourceDir_1;
	// Utage.AdvEngine Utage.AdvEngine/<CoBootFromExportData>c__Iterator0::$this
	AdvEngine_t1176753927 * ___U24this_2;
	// System.Object Utage.AdvEngine/<CoBootFromExportData>c__Iterator0::$current
	RuntimeObject * ___U24current_3;
	// System.Boolean Utage.AdvEngine/<CoBootFromExportData>c__Iterator0::$disposing
	bool ___U24disposing_4;
	// System.Int32 Utage.AdvEngine/<CoBootFromExportData>c__Iterator0::$PC
	int32_t ___U24PC_5;

public:
	inline static int32_t get_offset_of_scenarios_0() { return static_cast<int32_t>(offsetof(U3CCoBootFromExportDataU3Ec__Iterator0_t2686201055, ___scenarios_0)); }
	inline AdvImportScenarios_t1226385437 * get_scenarios_0() const { return ___scenarios_0; }
	inline AdvImportScenarios_t1226385437 ** get_address_of_scenarios_0() { return &___scenarios_0; }
	inline void set_scenarios_0(AdvImportScenarios_t1226385437 * value)
	{
		___scenarios_0 = value;
		Il2CppCodeGenWriteBarrier((&___scenarios_0), value);
	}

	inline static int32_t get_offset_of_resourceDir_1() { return static_cast<int32_t>(offsetof(U3CCoBootFromExportDataU3Ec__Iterator0_t2686201055, ___resourceDir_1)); }
	inline String_t* get_resourceDir_1() const { return ___resourceDir_1; }
	inline String_t** get_address_of_resourceDir_1() { return &___resourceDir_1; }
	inline void set_resourceDir_1(String_t* value)
	{
		___resourceDir_1 = value;
		Il2CppCodeGenWriteBarrier((&___resourceDir_1), value);
	}

	inline static int32_t get_offset_of_U24this_2() { return static_cast<int32_t>(offsetof(U3CCoBootFromExportDataU3Ec__Iterator0_t2686201055, ___U24this_2)); }
	inline AdvEngine_t1176753927 * get_U24this_2() const { return ___U24this_2; }
	inline AdvEngine_t1176753927 ** get_address_of_U24this_2() { return &___U24this_2; }
	inline void set_U24this_2(AdvEngine_t1176753927 * value)
	{
		___U24this_2 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_2), value);
	}

	inline static int32_t get_offset_of_U24current_3() { return static_cast<int32_t>(offsetof(U3CCoBootFromExportDataU3Ec__Iterator0_t2686201055, ___U24current_3)); }
	inline RuntimeObject * get_U24current_3() const { return ___U24current_3; }
	inline RuntimeObject ** get_address_of_U24current_3() { return &___U24current_3; }
	inline void set_U24current_3(RuntimeObject * value)
	{
		___U24current_3 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_3), value);
	}

	inline static int32_t get_offset_of_U24disposing_4() { return static_cast<int32_t>(offsetof(U3CCoBootFromExportDataU3Ec__Iterator0_t2686201055, ___U24disposing_4)); }
	inline bool get_U24disposing_4() const { return ___U24disposing_4; }
	inline bool* get_address_of_U24disposing_4() { return &___U24disposing_4; }
	inline void set_U24disposing_4(bool value)
	{
		___U24disposing_4 = value;
	}

	inline static int32_t get_offset_of_U24PC_5() { return static_cast<int32_t>(offsetof(U3CCoBootFromExportDataU3Ec__Iterator0_t2686201055, ___U24PC_5)); }
	inline int32_t get_U24PC_5() const { return ___U24PC_5; }
	inline int32_t* get_address_of_U24PC_5() { return &___U24PC_5; }
	inline void set_U24PC_5(int32_t value)
	{
		___U24PC_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CCOBOOTFROMEXPORTDATAU3EC__ITERATOR0_T2686201055_H
#ifndef U3CINPUTFILELDU3EC__ANONSTOREY1_T3579112721_H
#define U3CINPUTFILELDU3EC__ANONSTOREY1_T3579112721_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UtageRecieveMessageSample/<InputFileld>c__AnonStorey1
struct  U3CInputFileldU3Ec__AnonStorey1_t3579112721  : public RuntimeObject
{
public:
	// Utage.AdvCommandSendMessage UtageRecieveMessageSample/<InputFileld>c__AnonStorey1::command
	AdvCommandSendMessage_t330862209 * ___command_0;
	// UtageRecieveMessageSample UtageRecieveMessageSample/<InputFileld>c__AnonStorey1::$this
	UtageRecieveMessageSample_t743719262 * ___U24this_1;

public:
	inline static int32_t get_offset_of_command_0() { return static_cast<int32_t>(offsetof(U3CInputFileldU3Ec__AnonStorey1_t3579112721, ___command_0)); }
	inline AdvCommandSendMessage_t330862209 * get_command_0() const { return ___command_0; }
	inline AdvCommandSendMessage_t330862209 ** get_address_of_command_0() { return &___command_0; }
	inline void set_command_0(AdvCommandSendMessage_t330862209 * value)
	{
		___command_0 = value;
		Il2CppCodeGenWriteBarrier((&___command_0), value);
	}

	inline static int32_t get_offset_of_U24this_1() { return static_cast<int32_t>(offsetof(U3CInputFileldU3Ec__AnonStorey1_t3579112721, ___U24this_1)); }
	inline UtageRecieveMessageSample_t743719262 * get_U24this_1() const { return ___U24this_1; }
	inline UtageRecieveMessageSample_t743719262 ** get_address_of_U24this_1() { return &___U24this_1; }
	inline void set_U24this_1(UtageRecieveMessageSample_t743719262 * value)
	{
		___U24this_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CINPUTFILELDU3EC__ANONSTOREY1_T3579112721_H
#ifndef ADVCOMMAND_T2859960984_H
#define ADVCOMMAND_T2859960984_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.AdvCommand
struct  AdvCommand_t2859960984  : public RuntimeObject
{
public:
	// Utage.StringGridRow Utage.AdvCommand::<RowData>k__BackingField
	StringGridRow_t4193237197 * ___U3CRowDataU3Ek__BackingField_2;
	// Utage.AdvEntityData Utage.AdvCommand::<EntityData>k__BackingField
	AdvEntityData_t1465354496 * ___U3CEntityDataU3Ek__BackingField_3;
	// System.String Utage.AdvCommand::<Id>k__BackingField
	String_t* ___U3CIdU3Ek__BackingField_4;
	// System.Collections.Generic.List`1<Utage.AssetFile> Utage.AdvCommand::loadFileList
	List_1_t2752134388 * ___loadFileList_5;
	// Utage.AdvScenarioThread Utage.AdvCommand::<CurrentTread>k__BackingField
	AdvScenarioThread_t1270526825 * ___U3CCurrentTreadU3Ek__BackingField_6;

public:
	inline static int32_t get_offset_of_U3CRowDataU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(AdvCommand_t2859960984, ___U3CRowDataU3Ek__BackingField_2)); }
	inline StringGridRow_t4193237197 * get_U3CRowDataU3Ek__BackingField_2() const { return ___U3CRowDataU3Ek__BackingField_2; }
	inline StringGridRow_t4193237197 ** get_address_of_U3CRowDataU3Ek__BackingField_2() { return &___U3CRowDataU3Ek__BackingField_2; }
	inline void set_U3CRowDataU3Ek__BackingField_2(StringGridRow_t4193237197 * value)
	{
		___U3CRowDataU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CRowDataU3Ek__BackingField_2), value);
	}

	inline static int32_t get_offset_of_U3CEntityDataU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(AdvCommand_t2859960984, ___U3CEntityDataU3Ek__BackingField_3)); }
	inline AdvEntityData_t1465354496 * get_U3CEntityDataU3Ek__BackingField_3() const { return ___U3CEntityDataU3Ek__BackingField_3; }
	inline AdvEntityData_t1465354496 ** get_address_of_U3CEntityDataU3Ek__BackingField_3() { return &___U3CEntityDataU3Ek__BackingField_3; }
	inline void set_U3CEntityDataU3Ek__BackingField_3(AdvEntityData_t1465354496 * value)
	{
		___U3CEntityDataU3Ek__BackingField_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CEntityDataU3Ek__BackingField_3), value);
	}

	inline static int32_t get_offset_of_U3CIdU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(AdvCommand_t2859960984, ___U3CIdU3Ek__BackingField_4)); }
	inline String_t* get_U3CIdU3Ek__BackingField_4() const { return ___U3CIdU3Ek__BackingField_4; }
	inline String_t** get_address_of_U3CIdU3Ek__BackingField_4() { return &___U3CIdU3Ek__BackingField_4; }
	inline void set_U3CIdU3Ek__BackingField_4(String_t* value)
	{
		___U3CIdU3Ek__BackingField_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CIdU3Ek__BackingField_4), value);
	}

	inline static int32_t get_offset_of_loadFileList_5() { return static_cast<int32_t>(offsetof(AdvCommand_t2859960984, ___loadFileList_5)); }
	inline List_1_t2752134388 * get_loadFileList_5() const { return ___loadFileList_5; }
	inline List_1_t2752134388 ** get_address_of_loadFileList_5() { return &___loadFileList_5; }
	inline void set_loadFileList_5(List_1_t2752134388 * value)
	{
		___loadFileList_5 = value;
		Il2CppCodeGenWriteBarrier((&___loadFileList_5), value);
	}

	inline static int32_t get_offset_of_U3CCurrentTreadU3Ek__BackingField_6() { return static_cast<int32_t>(offsetof(AdvCommand_t2859960984, ___U3CCurrentTreadU3Ek__BackingField_6)); }
	inline AdvScenarioThread_t1270526825 * get_U3CCurrentTreadU3Ek__BackingField_6() const { return ___U3CCurrentTreadU3Ek__BackingField_6; }
	inline AdvScenarioThread_t1270526825 ** get_address_of_U3CCurrentTreadU3Ek__BackingField_6() { return &___U3CCurrentTreadU3Ek__BackingField_6; }
	inline void set_U3CCurrentTreadU3Ek__BackingField_6(AdvScenarioThread_t1270526825 * value)
	{
		___U3CCurrentTreadU3Ek__BackingField_6 = value;
		Il2CppCodeGenWriteBarrier((&___U3CCurrentTreadU3Ek__BackingField_6), value);
	}
};

struct AdvCommand_t2859960984_StaticFields
{
public:
	// System.Boolean Utage.AdvCommand::isEditorErrorCheck
	bool ___isEditorErrorCheck_0;
	// System.Boolean Utage.AdvCommand::<IsEditorErrorCheckWaitType>k__BackingField
	bool ___U3CIsEditorErrorCheckWaitTypeU3Ek__BackingField_1;

public:
	inline static int32_t get_offset_of_isEditorErrorCheck_0() { return static_cast<int32_t>(offsetof(AdvCommand_t2859960984_StaticFields, ___isEditorErrorCheck_0)); }
	inline bool get_isEditorErrorCheck_0() const { return ___isEditorErrorCheck_0; }
	inline bool* get_address_of_isEditorErrorCheck_0() { return &___isEditorErrorCheck_0; }
	inline void set_isEditorErrorCheck_0(bool value)
	{
		___isEditorErrorCheck_0 = value;
	}

	inline static int32_t get_offset_of_U3CIsEditorErrorCheckWaitTypeU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(AdvCommand_t2859960984_StaticFields, ___U3CIsEditorErrorCheckWaitTypeU3Ek__BackingField_1)); }
	inline bool get_U3CIsEditorErrorCheckWaitTypeU3Ek__BackingField_1() const { return ___U3CIsEditorErrorCheckWaitTypeU3Ek__BackingField_1; }
	inline bool* get_address_of_U3CIsEditorErrorCheckWaitTypeU3Ek__BackingField_1() { return &___U3CIsEditorErrorCheckWaitTypeU3Ek__BackingField_1; }
	inline void set_U3CIsEditorErrorCheckWaitTypeU3Ek__BackingField_1(bool value)
	{
		___U3CIsEditorErrorCheckWaitTypeU3Ek__BackingField_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ADVCOMMAND_T2859960984_H
#ifndef U3CGOTONEXTSCENEU3EC__ITERATOR3_T82667986_H
#define U3CGOTONEXTSCENEU3EC__ITERATOR3_T82667986_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SelectScript/<gotoNextScene>c__Iterator3
struct  U3CgotoNextSceneU3Ec__Iterator3_t82667986  : public RuntimeObject
{
public:
	// System.Object SelectScript/<gotoNextScene>c__Iterator3::$current
	RuntimeObject * ___U24current_0;
	// System.Boolean SelectScript/<gotoNextScene>c__Iterator3::$disposing
	bool ___U24disposing_1;
	// System.Int32 SelectScript/<gotoNextScene>c__Iterator3::$PC
	int32_t ___U24PC_2;

public:
	inline static int32_t get_offset_of_U24current_0() { return static_cast<int32_t>(offsetof(U3CgotoNextSceneU3Ec__Iterator3_t82667986, ___U24current_0)); }
	inline RuntimeObject * get_U24current_0() const { return ___U24current_0; }
	inline RuntimeObject ** get_address_of_U24current_0() { return &___U24current_0; }
	inline void set_U24current_0(RuntimeObject * value)
	{
		___U24current_0 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_0), value);
	}

	inline static int32_t get_offset_of_U24disposing_1() { return static_cast<int32_t>(offsetof(U3CgotoNextSceneU3Ec__Iterator3_t82667986, ___U24disposing_1)); }
	inline bool get_U24disposing_1() const { return ___U24disposing_1; }
	inline bool* get_address_of_U24disposing_1() { return &___U24disposing_1; }
	inline void set_U24disposing_1(bool value)
	{
		___U24disposing_1 = value;
	}

	inline static int32_t get_offset_of_U24PC_2() { return static_cast<int32_t>(offsetof(U3CgotoNextSceneU3Ec__Iterator3_t82667986, ___U24PC_2)); }
	inline int32_t get_U24PC_2() const { return ___U24PC_2; }
	inline int32_t* get_address_of_U24PC_2() { return &___U24PC_2; }
	inline void set_U24PC_2(int32_t value)
	{
		___U24PC_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CGOTONEXTSCENEU3EC__ITERATOR3_T82667986_H
#ifndef U3CSHOWSTAYU3EC__ITERATOR2_T2760854739_H
#define U3CSHOWSTAYU3EC__ITERATOR2_T2760854739_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SelectScript/<ShowStay>c__Iterator2
struct  U3CShowStayU3Ec__Iterator2_t2760854739  : public RuntimeObject
{
public:
	// System.Single SelectScript/<ShowStay>c__Iterator2::<sec>__0
	float ___U3CsecU3E__0_0;
	// SelectScript SelectScript/<ShowStay>c__Iterator2::$this
	SelectScript_t398995261 * ___U24this_1;
	// System.Object SelectScript/<ShowStay>c__Iterator2::$current
	RuntimeObject * ___U24current_2;
	// System.Boolean SelectScript/<ShowStay>c__Iterator2::$disposing
	bool ___U24disposing_3;
	// System.Int32 SelectScript/<ShowStay>c__Iterator2::$PC
	int32_t ___U24PC_4;

public:
	inline static int32_t get_offset_of_U3CsecU3E__0_0() { return static_cast<int32_t>(offsetof(U3CShowStayU3Ec__Iterator2_t2760854739, ___U3CsecU3E__0_0)); }
	inline float get_U3CsecU3E__0_0() const { return ___U3CsecU3E__0_0; }
	inline float* get_address_of_U3CsecU3E__0_0() { return &___U3CsecU3E__0_0; }
	inline void set_U3CsecU3E__0_0(float value)
	{
		___U3CsecU3E__0_0 = value;
	}

	inline static int32_t get_offset_of_U24this_1() { return static_cast<int32_t>(offsetof(U3CShowStayU3Ec__Iterator2_t2760854739, ___U24this_1)); }
	inline SelectScript_t398995261 * get_U24this_1() const { return ___U24this_1; }
	inline SelectScript_t398995261 ** get_address_of_U24this_1() { return &___U24this_1; }
	inline void set_U24this_1(SelectScript_t398995261 * value)
	{
		___U24this_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_1), value);
	}

	inline static int32_t get_offset_of_U24current_2() { return static_cast<int32_t>(offsetof(U3CShowStayU3Ec__Iterator2_t2760854739, ___U24current_2)); }
	inline RuntimeObject * get_U24current_2() const { return ___U24current_2; }
	inline RuntimeObject ** get_address_of_U24current_2() { return &___U24current_2; }
	inline void set_U24current_2(RuntimeObject * value)
	{
		___U24current_2 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_2), value);
	}

	inline static int32_t get_offset_of_U24disposing_3() { return static_cast<int32_t>(offsetof(U3CShowStayU3Ec__Iterator2_t2760854739, ___U24disposing_3)); }
	inline bool get_U24disposing_3() const { return ___U24disposing_3; }
	inline bool* get_address_of_U24disposing_3() { return &___U24disposing_3; }
	inline void set_U24disposing_3(bool value)
	{
		___U24disposing_3 = value;
	}

	inline static int32_t get_offset_of_U24PC_4() { return static_cast<int32_t>(offsetof(U3CShowStayU3Ec__Iterator2_t2760854739, ___U24PC_4)); }
	inline int32_t get_U24PC_4() const { return ___U24PC_4; }
	inline int32_t* get_address_of_U24PC_4() { return &___U24PC_4; }
	inline void set_U24PC_4(int32_t value)
	{
		___U24PC_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CSHOWSTAYU3EC__ITERATOR2_T2760854739_H
#ifndef U3CSHOWBOYU3EC__ITERATOR1_T3879423835_H
#define U3CSHOWBOYU3EC__ITERATOR1_T3879423835_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SelectScript/<ShowBoy>c__Iterator1
struct  U3CShowBoyU3Ec__Iterator1_t3879423835  : public RuntimeObject
{
public:
	// System.Single SelectScript/<ShowBoy>c__Iterator1::<sec>__0
	float ___U3CsecU3E__0_0;
	// SelectScript SelectScript/<ShowBoy>c__Iterator1::$this
	SelectScript_t398995261 * ___U24this_1;
	// System.Object SelectScript/<ShowBoy>c__Iterator1::$current
	RuntimeObject * ___U24current_2;
	// System.Boolean SelectScript/<ShowBoy>c__Iterator1::$disposing
	bool ___U24disposing_3;
	// System.Int32 SelectScript/<ShowBoy>c__Iterator1::$PC
	int32_t ___U24PC_4;

public:
	inline static int32_t get_offset_of_U3CsecU3E__0_0() { return static_cast<int32_t>(offsetof(U3CShowBoyU3Ec__Iterator1_t3879423835, ___U3CsecU3E__0_0)); }
	inline float get_U3CsecU3E__0_0() const { return ___U3CsecU3E__0_0; }
	inline float* get_address_of_U3CsecU3E__0_0() { return &___U3CsecU3E__0_0; }
	inline void set_U3CsecU3E__0_0(float value)
	{
		___U3CsecU3E__0_0 = value;
	}

	inline static int32_t get_offset_of_U24this_1() { return static_cast<int32_t>(offsetof(U3CShowBoyU3Ec__Iterator1_t3879423835, ___U24this_1)); }
	inline SelectScript_t398995261 * get_U24this_1() const { return ___U24this_1; }
	inline SelectScript_t398995261 ** get_address_of_U24this_1() { return &___U24this_1; }
	inline void set_U24this_1(SelectScript_t398995261 * value)
	{
		___U24this_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_1), value);
	}

	inline static int32_t get_offset_of_U24current_2() { return static_cast<int32_t>(offsetof(U3CShowBoyU3Ec__Iterator1_t3879423835, ___U24current_2)); }
	inline RuntimeObject * get_U24current_2() const { return ___U24current_2; }
	inline RuntimeObject ** get_address_of_U24current_2() { return &___U24current_2; }
	inline void set_U24current_2(RuntimeObject * value)
	{
		___U24current_2 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_2), value);
	}

	inline static int32_t get_offset_of_U24disposing_3() { return static_cast<int32_t>(offsetof(U3CShowBoyU3Ec__Iterator1_t3879423835, ___U24disposing_3)); }
	inline bool get_U24disposing_3() const { return ___U24disposing_3; }
	inline bool* get_address_of_U24disposing_3() { return &___U24disposing_3; }
	inline void set_U24disposing_3(bool value)
	{
		___U24disposing_3 = value;
	}

	inline static int32_t get_offset_of_U24PC_4() { return static_cast<int32_t>(offsetof(U3CShowBoyU3Ec__Iterator1_t3879423835, ___U24PC_4)); }
	inline int32_t get_U24PC_4() const { return ___U24PC_4; }
	inline int32_t* get_address_of_U24PC_4() { return &___U24PC_4; }
	inline void set_U24PC_4(int32_t value)
	{
		___U24PC_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CSHOWBOYU3EC__ITERATOR1_T3879423835_H
#ifndef U3CSHOWGIRLU3EC__ITERATOR0_T648192688_H
#define U3CSHOWGIRLU3EC__ITERATOR0_T648192688_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SelectScript/<ShowGirl>c__Iterator0
struct  U3CShowGirlU3Ec__Iterator0_t648192688  : public RuntimeObject
{
public:
	// System.Single SelectScript/<ShowGirl>c__Iterator0::<sec>__0
	float ___U3CsecU3E__0_0;
	// SelectScript SelectScript/<ShowGirl>c__Iterator0::$this
	SelectScript_t398995261 * ___U24this_1;
	// System.Object SelectScript/<ShowGirl>c__Iterator0::$current
	RuntimeObject * ___U24current_2;
	// System.Boolean SelectScript/<ShowGirl>c__Iterator0::$disposing
	bool ___U24disposing_3;
	// System.Int32 SelectScript/<ShowGirl>c__Iterator0::$PC
	int32_t ___U24PC_4;

public:
	inline static int32_t get_offset_of_U3CsecU3E__0_0() { return static_cast<int32_t>(offsetof(U3CShowGirlU3Ec__Iterator0_t648192688, ___U3CsecU3E__0_0)); }
	inline float get_U3CsecU3E__0_0() const { return ___U3CsecU3E__0_0; }
	inline float* get_address_of_U3CsecU3E__0_0() { return &___U3CsecU3E__0_0; }
	inline void set_U3CsecU3E__0_0(float value)
	{
		___U3CsecU3E__0_0 = value;
	}

	inline static int32_t get_offset_of_U24this_1() { return static_cast<int32_t>(offsetof(U3CShowGirlU3Ec__Iterator0_t648192688, ___U24this_1)); }
	inline SelectScript_t398995261 * get_U24this_1() const { return ___U24this_1; }
	inline SelectScript_t398995261 ** get_address_of_U24this_1() { return &___U24this_1; }
	inline void set_U24this_1(SelectScript_t398995261 * value)
	{
		___U24this_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_1), value);
	}

	inline static int32_t get_offset_of_U24current_2() { return static_cast<int32_t>(offsetof(U3CShowGirlU3Ec__Iterator0_t648192688, ___U24current_2)); }
	inline RuntimeObject * get_U24current_2() const { return ___U24current_2; }
	inline RuntimeObject ** get_address_of_U24current_2() { return &___U24current_2; }
	inline void set_U24current_2(RuntimeObject * value)
	{
		___U24current_2 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_2), value);
	}

	inline static int32_t get_offset_of_U24disposing_3() { return static_cast<int32_t>(offsetof(U3CShowGirlU3Ec__Iterator0_t648192688, ___U24disposing_3)); }
	inline bool get_U24disposing_3() const { return ___U24disposing_3; }
	inline bool* get_address_of_U24disposing_3() { return &___U24disposing_3; }
	inline void set_U24disposing_3(bool value)
	{
		___U24disposing_3 = value;
	}

	inline static int32_t get_offset_of_U24PC_4() { return static_cast<int32_t>(offsetof(U3CShowGirlU3Ec__Iterator0_t648192688, ___U24PC_4)); }
	inline int32_t get_U24PC_4() const { return ___U24PC_4; }
	inline int32_t* get_address_of_U24PC_4() { return &___U24PC_4; }
	inline void set_U24PC_4(int32_t value)
	{
		___U24PC_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CSHOWGIRLU3EC__ITERATOR0_T648192688_H
#ifndef U3CCOAUTOLOADSUBU3EC__ITERATOR0_T963927007_H
#define U3CCOAUTOLOADSUBU3EC__ITERATOR0_T963927007_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UtageRecieveMessageSample/<CoAutoLoadSub>c__Iterator0
struct  U3CCoAutoLoadSubU3Ec__Iterator0_t963927007  : public RuntimeObject
{
public:
	// UtageRecieveMessageSample UtageRecieveMessageSample/<CoAutoLoadSub>c__Iterator0::$this
	UtageRecieveMessageSample_t743719262 * ___U24this_0;
	// System.Object UtageRecieveMessageSample/<CoAutoLoadSub>c__Iterator0::$current
	RuntimeObject * ___U24current_1;
	// System.Boolean UtageRecieveMessageSample/<CoAutoLoadSub>c__Iterator0::$disposing
	bool ___U24disposing_2;
	// System.Int32 UtageRecieveMessageSample/<CoAutoLoadSub>c__Iterator0::$PC
	int32_t ___U24PC_3;

public:
	inline static int32_t get_offset_of_U24this_0() { return static_cast<int32_t>(offsetof(U3CCoAutoLoadSubU3Ec__Iterator0_t963927007, ___U24this_0)); }
	inline UtageRecieveMessageSample_t743719262 * get_U24this_0() const { return ___U24this_0; }
	inline UtageRecieveMessageSample_t743719262 ** get_address_of_U24this_0() { return &___U24this_0; }
	inline void set_U24this_0(UtageRecieveMessageSample_t743719262 * value)
	{
		___U24this_0 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_0), value);
	}

	inline static int32_t get_offset_of_U24current_1() { return static_cast<int32_t>(offsetof(U3CCoAutoLoadSubU3Ec__Iterator0_t963927007, ___U24current_1)); }
	inline RuntimeObject * get_U24current_1() const { return ___U24current_1; }
	inline RuntimeObject ** get_address_of_U24current_1() { return &___U24current_1; }
	inline void set_U24current_1(RuntimeObject * value)
	{
		___U24current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_1), value);
	}

	inline static int32_t get_offset_of_U24disposing_2() { return static_cast<int32_t>(offsetof(U3CCoAutoLoadSubU3Ec__Iterator0_t963927007, ___U24disposing_2)); }
	inline bool get_U24disposing_2() const { return ___U24disposing_2; }
	inline bool* get_address_of_U24disposing_2() { return &___U24disposing_2; }
	inline void set_U24disposing_2(bool value)
	{
		___U24disposing_2 = value;
	}

	inline static int32_t get_offset_of_U24PC_3() { return static_cast<int32_t>(offsetof(U3CCoAutoLoadSubU3Ec__Iterator0_t963927007, ___U24PC_3)); }
	inline int32_t get_U24PC_3() const { return ___U24PC_3; }
	inline int32_t* get_address_of_U24PC_3() { return &___U24PC_3; }
	inline void set_U24PC_3(int32_t value)
	{
		___U24PC_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CCOAUTOLOADSUBU3EC__ITERATOR0_T963927007_H
#ifndef ANIMATORSTATEINFO_T2577870592_H
#define ANIMATORSTATEINFO_T2577870592_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.AnimatorStateInfo
struct  AnimatorStateInfo_t2577870592 
{
public:
	// System.Int32 UnityEngine.AnimatorStateInfo::m_Name
	int32_t ___m_Name_0;
	// System.Int32 UnityEngine.AnimatorStateInfo::m_Path
	int32_t ___m_Path_1;
	// System.Int32 UnityEngine.AnimatorStateInfo::m_FullPath
	int32_t ___m_FullPath_2;
	// System.Single UnityEngine.AnimatorStateInfo::m_NormalizedTime
	float ___m_NormalizedTime_3;
	// System.Single UnityEngine.AnimatorStateInfo::m_Length
	float ___m_Length_4;
	// System.Single UnityEngine.AnimatorStateInfo::m_Speed
	float ___m_Speed_5;
	// System.Single UnityEngine.AnimatorStateInfo::m_SpeedMultiplier
	float ___m_SpeedMultiplier_6;
	// System.Int32 UnityEngine.AnimatorStateInfo::m_Tag
	int32_t ___m_Tag_7;
	// System.Int32 UnityEngine.AnimatorStateInfo::m_Loop
	int32_t ___m_Loop_8;

public:
	inline static int32_t get_offset_of_m_Name_0() { return static_cast<int32_t>(offsetof(AnimatorStateInfo_t2577870592, ___m_Name_0)); }
	inline int32_t get_m_Name_0() const { return ___m_Name_0; }
	inline int32_t* get_address_of_m_Name_0() { return &___m_Name_0; }
	inline void set_m_Name_0(int32_t value)
	{
		___m_Name_0 = value;
	}

	inline static int32_t get_offset_of_m_Path_1() { return static_cast<int32_t>(offsetof(AnimatorStateInfo_t2577870592, ___m_Path_1)); }
	inline int32_t get_m_Path_1() const { return ___m_Path_1; }
	inline int32_t* get_address_of_m_Path_1() { return &___m_Path_1; }
	inline void set_m_Path_1(int32_t value)
	{
		___m_Path_1 = value;
	}

	inline static int32_t get_offset_of_m_FullPath_2() { return static_cast<int32_t>(offsetof(AnimatorStateInfo_t2577870592, ___m_FullPath_2)); }
	inline int32_t get_m_FullPath_2() const { return ___m_FullPath_2; }
	inline int32_t* get_address_of_m_FullPath_2() { return &___m_FullPath_2; }
	inline void set_m_FullPath_2(int32_t value)
	{
		___m_FullPath_2 = value;
	}

	inline static int32_t get_offset_of_m_NormalizedTime_3() { return static_cast<int32_t>(offsetof(AnimatorStateInfo_t2577870592, ___m_NormalizedTime_3)); }
	inline float get_m_NormalizedTime_3() const { return ___m_NormalizedTime_3; }
	inline float* get_address_of_m_NormalizedTime_3() { return &___m_NormalizedTime_3; }
	inline void set_m_NormalizedTime_3(float value)
	{
		___m_NormalizedTime_3 = value;
	}

	inline static int32_t get_offset_of_m_Length_4() { return static_cast<int32_t>(offsetof(AnimatorStateInfo_t2577870592, ___m_Length_4)); }
	inline float get_m_Length_4() const { return ___m_Length_4; }
	inline float* get_address_of_m_Length_4() { return &___m_Length_4; }
	inline void set_m_Length_4(float value)
	{
		___m_Length_4 = value;
	}

	inline static int32_t get_offset_of_m_Speed_5() { return static_cast<int32_t>(offsetof(AnimatorStateInfo_t2577870592, ___m_Speed_5)); }
	inline float get_m_Speed_5() const { return ___m_Speed_5; }
	inline float* get_address_of_m_Speed_5() { return &___m_Speed_5; }
	inline void set_m_Speed_5(float value)
	{
		___m_Speed_5 = value;
	}

	inline static int32_t get_offset_of_m_SpeedMultiplier_6() { return static_cast<int32_t>(offsetof(AnimatorStateInfo_t2577870592, ___m_SpeedMultiplier_6)); }
	inline float get_m_SpeedMultiplier_6() const { return ___m_SpeedMultiplier_6; }
	inline float* get_address_of_m_SpeedMultiplier_6() { return &___m_SpeedMultiplier_6; }
	inline void set_m_SpeedMultiplier_6(float value)
	{
		___m_SpeedMultiplier_6 = value;
	}

	inline static int32_t get_offset_of_m_Tag_7() { return static_cast<int32_t>(offsetof(AnimatorStateInfo_t2577870592, ___m_Tag_7)); }
	inline int32_t get_m_Tag_7() const { return ___m_Tag_7; }
	inline int32_t* get_address_of_m_Tag_7() { return &___m_Tag_7; }
	inline void set_m_Tag_7(int32_t value)
	{
		___m_Tag_7 = value;
	}

	inline static int32_t get_offset_of_m_Loop_8() { return static_cast<int32_t>(offsetof(AnimatorStateInfo_t2577870592, ___m_Loop_8)); }
	inline int32_t get_m_Loop_8() const { return ___m_Loop_8; }
	inline int32_t* get_address_of_m_Loop_8() { return &___m_Loop_8; }
	inline void set_m_Loop_8(int32_t value)
	{
		___m_Loop_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ANIMATORSTATEINFO_T2577870592_H
#ifndef QUATERNION_T4030073918_H
#define QUATERNION_T4030073918_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Quaternion
struct  Quaternion_t4030073918 
{
public:
	// System.Single UnityEngine.Quaternion::x
	float ___x_0;
	// System.Single UnityEngine.Quaternion::y
	float ___y_1;
	// System.Single UnityEngine.Quaternion::z
	float ___z_2;
	// System.Single UnityEngine.Quaternion::w
	float ___w_3;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Quaternion_t4030073918, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Quaternion_t4030073918, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}

	inline static int32_t get_offset_of_z_2() { return static_cast<int32_t>(offsetof(Quaternion_t4030073918, ___z_2)); }
	inline float get_z_2() const { return ___z_2; }
	inline float* get_address_of_z_2() { return &___z_2; }
	inline void set_z_2(float value)
	{
		___z_2 = value;
	}

	inline static int32_t get_offset_of_w_3() { return static_cast<int32_t>(offsetof(Quaternion_t4030073918, ___w_3)); }
	inline float get_w_3() const { return ___w_3; }
	inline float* get_address_of_w_3() { return &___w_3; }
	inline void set_w_3(float value)
	{
		___w_3 = value;
	}
};

struct Quaternion_t4030073918_StaticFields
{
public:
	// UnityEngine.Quaternion UnityEngine.Quaternion::identityQuaternion
	Quaternion_t4030073918  ___identityQuaternion_4;

public:
	inline static int32_t get_offset_of_identityQuaternion_4() { return static_cast<int32_t>(offsetof(Quaternion_t4030073918_StaticFields, ___identityQuaternion_4)); }
	inline Quaternion_t4030073918  get_identityQuaternion_4() const { return ___identityQuaternion_4; }
	inline Quaternion_t4030073918 * get_address_of_identityQuaternion_4() { return &___identityQuaternion_4; }
	inline void set_identityQuaternion_4(Quaternion_t4030073918  value)
	{
		___identityQuaternion_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // QUATERNION_T4030073918_H
#ifndef ENUMERATOR_T218450202_H
#define ENUMERATOR_T218450202_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.List`1/Enumerator<Utage.SampleCustomAssetBundleLoad/SampleAssetBundleVersionInfo>
struct  Enumerator_t218450202 
{
public:
	// System.Collections.Generic.List`1<T> System.Collections.Generic.List`1/Enumerator::l
	List_1_t683720528 * ___l_0;
	// System.Int32 System.Collections.Generic.List`1/Enumerator::next
	int32_t ___next_1;
	// System.Int32 System.Collections.Generic.List`1/Enumerator::ver
	int32_t ___ver_2;
	// T System.Collections.Generic.List`1/Enumerator::current
	SampleAssetBundleVersionInfo_t1314599396 * ___current_3;

public:
	inline static int32_t get_offset_of_l_0() { return static_cast<int32_t>(offsetof(Enumerator_t218450202, ___l_0)); }
	inline List_1_t683720528 * get_l_0() const { return ___l_0; }
	inline List_1_t683720528 ** get_address_of_l_0() { return &___l_0; }
	inline void set_l_0(List_1_t683720528 * value)
	{
		___l_0 = value;
		Il2CppCodeGenWriteBarrier((&___l_0), value);
	}

	inline static int32_t get_offset_of_next_1() { return static_cast<int32_t>(offsetof(Enumerator_t218450202, ___next_1)); }
	inline int32_t get_next_1() const { return ___next_1; }
	inline int32_t* get_address_of_next_1() { return &___next_1; }
	inline void set_next_1(int32_t value)
	{
		___next_1 = value;
	}

	inline static int32_t get_offset_of_ver_2() { return static_cast<int32_t>(offsetof(Enumerator_t218450202, ___ver_2)); }
	inline int32_t get_ver_2() const { return ___ver_2; }
	inline int32_t* get_address_of_ver_2() { return &___ver_2; }
	inline void set_ver_2(int32_t value)
	{
		___ver_2 = value;
	}

	inline static int32_t get_offset_of_current_3() { return static_cast<int32_t>(offsetof(Enumerator_t218450202, ___current_3)); }
	inline SampleAssetBundleVersionInfo_t1314599396 * get_current_3() const { return ___current_3; }
	inline SampleAssetBundleVersionInfo_t1314599396 ** get_address_of_current_3() { return &___current_3; }
	inline void set_current_3(SampleAssetBundleVersionInfo_t1314599396 * value)
	{
		___current_3 = value;
		Il2CppCodeGenWriteBarrier((&___current_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENUMERATOR_T218450202_H
#ifndef ENUM_T2459695545_H
#define ENUM_T2459695545_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Enum
struct  Enum_t2459695545  : public ValueType_t3507792607
{
public:

public:
};

struct Enum_t2459695545_StaticFields
{
public:
	// System.Char[] System.Enum::split_char
	CharU5BU5D_t1328083999* ___split_char_0;

public:
	inline static int32_t get_offset_of_split_char_0() { return static_cast<int32_t>(offsetof(Enum_t2459695545_StaticFields, ___split_char_0)); }
	inline CharU5BU5D_t1328083999* get_split_char_0() const { return ___split_char_0; }
	inline CharU5BU5D_t1328083999** get_address_of_split_char_0() { return &___split_char_0; }
	inline void set_split_char_0(CharU5BU5D_t1328083999* value)
	{
		___split_char_0 = value;
		Il2CppCodeGenWriteBarrier((&___split_char_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t2459695545_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t2459695545_marshaled_com
{
};
#endif // ENUM_T2459695545_H
#ifndef VECTOR3_T2243707580_H
#define VECTOR3_T2243707580_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector3
struct  Vector3_t2243707580 
{
public:
	// System.Single UnityEngine.Vector3::x
	float ___x_1;
	// System.Single UnityEngine.Vector3::y
	float ___y_2;
	// System.Single UnityEngine.Vector3::z
	float ___z_3;

public:
	inline static int32_t get_offset_of_x_1() { return static_cast<int32_t>(offsetof(Vector3_t2243707580, ___x_1)); }
	inline float get_x_1() const { return ___x_1; }
	inline float* get_address_of_x_1() { return &___x_1; }
	inline void set_x_1(float value)
	{
		___x_1 = value;
	}

	inline static int32_t get_offset_of_y_2() { return static_cast<int32_t>(offsetof(Vector3_t2243707580, ___y_2)); }
	inline float get_y_2() const { return ___y_2; }
	inline float* get_address_of_y_2() { return &___y_2; }
	inline void set_y_2(float value)
	{
		___y_2 = value;
	}

	inline static int32_t get_offset_of_z_3() { return static_cast<int32_t>(offsetof(Vector3_t2243707580, ___z_3)); }
	inline float get_z_3() const { return ___z_3; }
	inline float* get_address_of_z_3() { return &___z_3; }
	inline void set_z_3(float value)
	{
		___z_3 = value;
	}
};

struct Vector3_t2243707580_StaticFields
{
public:
	// UnityEngine.Vector3 UnityEngine.Vector3::zeroVector
	Vector3_t2243707580  ___zeroVector_4;
	// UnityEngine.Vector3 UnityEngine.Vector3::oneVector
	Vector3_t2243707580  ___oneVector_5;
	// UnityEngine.Vector3 UnityEngine.Vector3::upVector
	Vector3_t2243707580  ___upVector_6;
	// UnityEngine.Vector3 UnityEngine.Vector3::downVector
	Vector3_t2243707580  ___downVector_7;
	// UnityEngine.Vector3 UnityEngine.Vector3::leftVector
	Vector3_t2243707580  ___leftVector_8;
	// UnityEngine.Vector3 UnityEngine.Vector3::rightVector
	Vector3_t2243707580  ___rightVector_9;
	// UnityEngine.Vector3 UnityEngine.Vector3::forwardVector
	Vector3_t2243707580  ___forwardVector_10;
	// UnityEngine.Vector3 UnityEngine.Vector3::backVector
	Vector3_t2243707580  ___backVector_11;
	// UnityEngine.Vector3 UnityEngine.Vector3::positiveInfinityVector
	Vector3_t2243707580  ___positiveInfinityVector_12;
	// UnityEngine.Vector3 UnityEngine.Vector3::negativeInfinityVector
	Vector3_t2243707580  ___negativeInfinityVector_13;

public:
	inline static int32_t get_offset_of_zeroVector_4() { return static_cast<int32_t>(offsetof(Vector3_t2243707580_StaticFields, ___zeroVector_4)); }
	inline Vector3_t2243707580  get_zeroVector_4() const { return ___zeroVector_4; }
	inline Vector3_t2243707580 * get_address_of_zeroVector_4() { return &___zeroVector_4; }
	inline void set_zeroVector_4(Vector3_t2243707580  value)
	{
		___zeroVector_4 = value;
	}

	inline static int32_t get_offset_of_oneVector_5() { return static_cast<int32_t>(offsetof(Vector3_t2243707580_StaticFields, ___oneVector_5)); }
	inline Vector3_t2243707580  get_oneVector_5() const { return ___oneVector_5; }
	inline Vector3_t2243707580 * get_address_of_oneVector_5() { return &___oneVector_5; }
	inline void set_oneVector_5(Vector3_t2243707580  value)
	{
		___oneVector_5 = value;
	}

	inline static int32_t get_offset_of_upVector_6() { return static_cast<int32_t>(offsetof(Vector3_t2243707580_StaticFields, ___upVector_6)); }
	inline Vector3_t2243707580  get_upVector_6() const { return ___upVector_6; }
	inline Vector3_t2243707580 * get_address_of_upVector_6() { return &___upVector_6; }
	inline void set_upVector_6(Vector3_t2243707580  value)
	{
		___upVector_6 = value;
	}

	inline static int32_t get_offset_of_downVector_7() { return static_cast<int32_t>(offsetof(Vector3_t2243707580_StaticFields, ___downVector_7)); }
	inline Vector3_t2243707580  get_downVector_7() const { return ___downVector_7; }
	inline Vector3_t2243707580 * get_address_of_downVector_7() { return &___downVector_7; }
	inline void set_downVector_7(Vector3_t2243707580  value)
	{
		___downVector_7 = value;
	}

	inline static int32_t get_offset_of_leftVector_8() { return static_cast<int32_t>(offsetof(Vector3_t2243707580_StaticFields, ___leftVector_8)); }
	inline Vector3_t2243707580  get_leftVector_8() const { return ___leftVector_8; }
	inline Vector3_t2243707580 * get_address_of_leftVector_8() { return &___leftVector_8; }
	inline void set_leftVector_8(Vector3_t2243707580  value)
	{
		___leftVector_8 = value;
	}

	inline static int32_t get_offset_of_rightVector_9() { return static_cast<int32_t>(offsetof(Vector3_t2243707580_StaticFields, ___rightVector_9)); }
	inline Vector3_t2243707580  get_rightVector_9() const { return ___rightVector_9; }
	inline Vector3_t2243707580 * get_address_of_rightVector_9() { return &___rightVector_9; }
	inline void set_rightVector_9(Vector3_t2243707580  value)
	{
		___rightVector_9 = value;
	}

	inline static int32_t get_offset_of_forwardVector_10() { return static_cast<int32_t>(offsetof(Vector3_t2243707580_StaticFields, ___forwardVector_10)); }
	inline Vector3_t2243707580  get_forwardVector_10() const { return ___forwardVector_10; }
	inline Vector3_t2243707580 * get_address_of_forwardVector_10() { return &___forwardVector_10; }
	inline void set_forwardVector_10(Vector3_t2243707580  value)
	{
		___forwardVector_10 = value;
	}

	inline static int32_t get_offset_of_backVector_11() { return static_cast<int32_t>(offsetof(Vector3_t2243707580_StaticFields, ___backVector_11)); }
	inline Vector3_t2243707580  get_backVector_11() const { return ___backVector_11; }
	inline Vector3_t2243707580 * get_address_of_backVector_11() { return &___backVector_11; }
	inline void set_backVector_11(Vector3_t2243707580  value)
	{
		___backVector_11 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_12() { return static_cast<int32_t>(offsetof(Vector3_t2243707580_StaticFields, ___positiveInfinityVector_12)); }
	inline Vector3_t2243707580  get_positiveInfinityVector_12() const { return ___positiveInfinityVector_12; }
	inline Vector3_t2243707580 * get_address_of_positiveInfinityVector_12() { return &___positiveInfinityVector_12; }
	inline void set_positiveInfinityVector_12(Vector3_t2243707580  value)
	{
		___positiveInfinityVector_12 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_13() { return static_cast<int32_t>(offsetof(Vector3_t2243707580_StaticFields, ___negativeInfinityVector_13)); }
	inline Vector3_t2243707580  get_negativeInfinityVector_13() const { return ___negativeInfinityVector_13; }
	inline Vector3_t2243707580 * get_address_of_negativeInfinityVector_13() { return &___negativeInfinityVector_13; }
	inline void set_negativeInfinityVector_13(Vector3_t2243707580  value)
	{
		___negativeInfinityVector_13 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR3_T2243707580_H
#ifndef COLOR_T2020392075_H
#define COLOR_T2020392075_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Color
struct  Color_t2020392075 
{
public:
	// System.Single UnityEngine.Color::r
	float ___r_0;
	// System.Single UnityEngine.Color::g
	float ___g_1;
	// System.Single UnityEngine.Color::b
	float ___b_2;
	// System.Single UnityEngine.Color::a
	float ___a_3;

public:
	inline static int32_t get_offset_of_r_0() { return static_cast<int32_t>(offsetof(Color_t2020392075, ___r_0)); }
	inline float get_r_0() const { return ___r_0; }
	inline float* get_address_of_r_0() { return &___r_0; }
	inline void set_r_0(float value)
	{
		___r_0 = value;
	}

	inline static int32_t get_offset_of_g_1() { return static_cast<int32_t>(offsetof(Color_t2020392075, ___g_1)); }
	inline float get_g_1() const { return ___g_1; }
	inline float* get_address_of_g_1() { return &___g_1; }
	inline void set_g_1(float value)
	{
		___g_1 = value;
	}

	inline static int32_t get_offset_of_b_2() { return static_cast<int32_t>(offsetof(Color_t2020392075, ___b_2)); }
	inline float get_b_2() const { return ___b_2; }
	inline float* get_address_of_b_2() { return &___b_2; }
	inline void set_b_2(float value)
	{
		___b_2 = value;
	}

	inline static int32_t get_offset_of_a_3() { return static_cast<int32_t>(offsetof(Color_t2020392075, ___a_3)); }
	inline float get_a_3() const { return ___a_3; }
	inline float* get_address_of_a_3() { return &___a_3; }
	inline void set_a_3(float value)
	{
		___a_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLOR_T2020392075_H
#ifndef UNITYEVENT_1_T1215103942_H
#define UNITYEVENT_1_T1215103942_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Events.UnityEvent`1<Utage.AdvEngine>
struct  UnityEvent_1_t1215103942  : public UnityEventBase_t828812576
{
public:
	// System.Object[] UnityEngine.Events.UnityEvent`1::m_InvokeArray
	ObjectU5BU5D_t3614634134* ___m_InvokeArray_4;

public:
	inline static int32_t get_offset_of_m_InvokeArray_4() { return static_cast<int32_t>(offsetof(UnityEvent_1_t1215103942, ___m_InvokeArray_4)); }
	inline ObjectU5BU5D_t3614634134* get_m_InvokeArray_4() const { return ___m_InvokeArray_4; }
	inline ObjectU5BU5D_t3614634134** get_address_of_m_InvokeArray_4() { return &___m_InvokeArray_4; }
	inline void set_m_InvokeArray_4(ObjectU5BU5D_t3614634134* value)
	{
		___m_InvokeArray_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_InvokeArray_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYEVENT_1_T1215103942_H
#ifndef ADVCOMMANDPARAMTBLKEYCOUNT2_T1053315553_H
#define ADVCOMMANDPARAMTBLKEYCOUNT2_T1053315553_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.AdvCommandParamTblKeyCount2
struct  AdvCommandParamTblKeyCount2_t1053315553  : public AdvCommand_t2859960984
{
public:
	// System.String Utage.AdvCommandParamTblKeyCount2::paramName
	String_t* ___paramName_7;
	// System.String Utage.AdvCommandParamTblKeyCount2::tblName
	String_t* ___tblName_8;
	// System.String Utage.AdvCommandParamTblKeyCount2::valueName
	String_t* ___valueName_9;
	// System.String Utage.AdvCommandParamTblKeyCount2::countValue
	String_t* ___countValue_10;

public:
	inline static int32_t get_offset_of_paramName_7() { return static_cast<int32_t>(offsetof(AdvCommandParamTblKeyCount2_t1053315553, ___paramName_7)); }
	inline String_t* get_paramName_7() const { return ___paramName_7; }
	inline String_t** get_address_of_paramName_7() { return &___paramName_7; }
	inline void set_paramName_7(String_t* value)
	{
		___paramName_7 = value;
		Il2CppCodeGenWriteBarrier((&___paramName_7), value);
	}

	inline static int32_t get_offset_of_tblName_8() { return static_cast<int32_t>(offsetof(AdvCommandParamTblKeyCount2_t1053315553, ___tblName_8)); }
	inline String_t* get_tblName_8() const { return ___tblName_8; }
	inline String_t** get_address_of_tblName_8() { return &___tblName_8; }
	inline void set_tblName_8(String_t* value)
	{
		___tblName_8 = value;
		Il2CppCodeGenWriteBarrier((&___tblName_8), value);
	}

	inline static int32_t get_offset_of_valueName_9() { return static_cast<int32_t>(offsetof(AdvCommandParamTblKeyCount2_t1053315553, ___valueName_9)); }
	inline String_t* get_valueName_9() const { return ___valueName_9; }
	inline String_t** get_address_of_valueName_9() { return &___valueName_9; }
	inline void set_valueName_9(String_t* value)
	{
		___valueName_9 = value;
		Il2CppCodeGenWriteBarrier((&___valueName_9), value);
	}

	inline static int32_t get_offset_of_countValue_10() { return static_cast<int32_t>(offsetof(AdvCommandParamTblKeyCount2_t1053315553, ___countValue_10)); }
	inline String_t* get_countValue_10() const { return ___countValue_10; }
	inline String_t** get_address_of_countValue_10() { return &___countValue_10; }
	inline void set_countValue_10(String_t* value)
	{
		___countValue_10 = value;
		Il2CppCodeGenWriteBarrier((&___countValue_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ADVCOMMANDPARAMTBLKEYCOUNT2_T1053315553_H
#ifndef ADVCOMMANDPARAMTBLKEYCOUNT_T533749743_H
#define ADVCOMMANDPARAMTBLKEYCOUNT_T533749743_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.AdvCommandParamTblKeyCount
struct  AdvCommandParamTblKeyCount_t533749743  : public AdvCommand_t2859960984
{
public:
	// System.String Utage.AdvCommandParamTblKeyCount::paramName
	String_t* ___paramName_7;
	// System.String Utage.AdvCommandParamTblKeyCount::tblName
	String_t* ___tblName_8;

public:
	inline static int32_t get_offset_of_paramName_7() { return static_cast<int32_t>(offsetof(AdvCommandParamTblKeyCount_t533749743, ___paramName_7)); }
	inline String_t* get_paramName_7() const { return ___paramName_7; }
	inline String_t** get_address_of_paramName_7() { return &___paramName_7; }
	inline void set_paramName_7(String_t* value)
	{
		___paramName_7 = value;
		Il2CppCodeGenWriteBarrier((&___paramName_7), value);
	}

	inline static int32_t get_offset_of_tblName_8() { return static_cast<int32_t>(offsetof(AdvCommandParamTblKeyCount_t533749743, ___tblName_8)); }
	inline String_t* get_tblName_8() const { return ___tblName_8; }
	inline String_t** get_address_of_tblName_8() { return &___tblName_8; }
	inline void set_tblName_8(String_t* value)
	{
		___tblName_8 = value;
		Il2CppCodeGenWriteBarrier((&___tblName_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ADVCOMMANDPARAMTBLKEYCOUNT_T533749743_H
#ifndef INTPTR_T_H
#define INTPTR_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTPTR_T_H
#ifndef SAMPLECUSTOMADVCOMMANDTEXT_T3879361722_H
#define SAMPLECUSTOMADVCOMMANDTEXT_T3879361722_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.SampleCustomAdvCommandText
struct  SampleCustomAdvCommandText_t3879361722  : public AdvCommand_t2859960984
{
public:
	// System.String Utage.SampleCustomAdvCommandText::log
	String_t* ___log_7;

public:
	inline static int32_t get_offset_of_log_7() { return static_cast<int32_t>(offsetof(SampleCustomAdvCommandText_t3879361722, ___log_7)); }
	inline String_t* get_log_7() const { return ___log_7; }
	inline String_t** get_address_of_log_7() { return &___log_7; }
	inline void set_log_7(String_t* value)
	{
		___log_7 = value;
		Il2CppCodeGenWriteBarrier((&___log_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SAMPLECUSTOMADVCOMMANDTEXT_T3879361722_H
#ifndef SAMPLEADVCOMMANDDEBUGLOG_T679789377_H
#define SAMPLEADVCOMMANDDEBUGLOG_T679789377_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.SampleAdvCommandDebugLog
struct  SampleAdvCommandDebugLog_t679789377  : public AdvCommand_t2859960984
{
public:
	// System.String Utage.SampleAdvCommandDebugLog::log
	String_t* ___log_7;

public:
	inline static int32_t get_offset_of_log_7() { return static_cast<int32_t>(offsetof(SampleAdvCommandDebugLog_t679789377, ___log_7)); }
	inline String_t* get_log_7() const { return ___log_7; }
	inline String_t** get_address_of_log_7() { return &___log_7; }
	inline void set_log_7(String_t* value)
	{
		___log_7 = value;
		Il2CppCodeGenWriteBarrier((&___log_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SAMPLEADVCOMMANDDEBUGLOG_T679789377_H
#ifndef STATUS_T3601570505_H
#define STATUS_T3601570505_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityChan.AutoBlinkforSD/Status
struct  Status_t3601570505 
{
public:
	// System.Int32 UnityChan.AutoBlinkforSD/Status::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(Status_t3601570505, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STATUS_T3601570505_H
#ifndef U3CLOADENGINEASYNCU3EC__ITERATOR0_T2926810271_H
#define U3CLOADENGINEASYNCU3EC__ITERATOR0_T2926810271_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.SampleCustomAssetBundleLoad/<LoadEngineAsync>c__Iterator0
struct  U3CLoadEngineAsyncU3Ec__Iterator0_t2926810271  : public RuntimeObject
{
public:
	// System.Collections.Generic.List`1/Enumerator<Utage.SampleCustomAssetBundleLoad/SampleAssetBundleVersionInfo> Utage.SampleCustomAssetBundleLoad/<LoadEngineAsync>c__Iterator0::$locvar0
	Enumerator_t218450202  ___U24locvar0_0;
	// Utage.SampleCustomAssetBundleLoad Utage.SampleCustomAssetBundleLoad/<LoadEngineAsync>c__Iterator0::$this
	SampleCustomAssetBundleLoad_t3658166813 * ___U24this_1;
	// System.Object Utage.SampleCustomAssetBundleLoad/<LoadEngineAsync>c__Iterator0::$current
	RuntimeObject * ___U24current_2;
	// System.Boolean Utage.SampleCustomAssetBundleLoad/<LoadEngineAsync>c__Iterator0::$disposing
	bool ___U24disposing_3;
	// System.Int32 Utage.SampleCustomAssetBundleLoad/<LoadEngineAsync>c__Iterator0::$PC
	int32_t ___U24PC_4;

public:
	inline static int32_t get_offset_of_U24locvar0_0() { return static_cast<int32_t>(offsetof(U3CLoadEngineAsyncU3Ec__Iterator0_t2926810271, ___U24locvar0_0)); }
	inline Enumerator_t218450202  get_U24locvar0_0() const { return ___U24locvar0_0; }
	inline Enumerator_t218450202 * get_address_of_U24locvar0_0() { return &___U24locvar0_0; }
	inline void set_U24locvar0_0(Enumerator_t218450202  value)
	{
		___U24locvar0_0 = value;
	}

	inline static int32_t get_offset_of_U24this_1() { return static_cast<int32_t>(offsetof(U3CLoadEngineAsyncU3Ec__Iterator0_t2926810271, ___U24this_1)); }
	inline SampleCustomAssetBundleLoad_t3658166813 * get_U24this_1() const { return ___U24this_1; }
	inline SampleCustomAssetBundleLoad_t3658166813 ** get_address_of_U24this_1() { return &___U24this_1; }
	inline void set_U24this_1(SampleCustomAssetBundleLoad_t3658166813 * value)
	{
		___U24this_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_1), value);
	}

	inline static int32_t get_offset_of_U24current_2() { return static_cast<int32_t>(offsetof(U3CLoadEngineAsyncU3Ec__Iterator0_t2926810271, ___U24current_2)); }
	inline RuntimeObject * get_U24current_2() const { return ___U24current_2; }
	inline RuntimeObject ** get_address_of_U24current_2() { return &___U24current_2; }
	inline void set_U24current_2(RuntimeObject * value)
	{
		___U24current_2 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_2), value);
	}

	inline static int32_t get_offset_of_U24disposing_3() { return static_cast<int32_t>(offsetof(U3CLoadEngineAsyncU3Ec__Iterator0_t2926810271, ___U24disposing_3)); }
	inline bool get_U24disposing_3() const { return ___U24disposing_3; }
	inline bool* get_address_of_U24disposing_3() { return &___U24disposing_3; }
	inline void set_U24disposing_3(bool value)
	{
		___U24disposing_3 = value;
	}

	inline static int32_t get_offset_of_U24PC_4() { return static_cast<int32_t>(offsetof(U3CLoadEngineAsyncU3Ec__Iterator0_t2926810271, ___U24PC_4)); }
	inline int32_t get_U24PC_4() const { return ___U24PC_4; }
	inline int32_t* get_address_of_U24PC_4() { return &___U24PC_4; }
	inline void set_U24PC_4(int32_t value)
	{
		___U24PC_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CLOADENGINEASYNCU3EC__ITERATOR0_T2926810271_H
#ifndef MOUSEBUTTONDOWN_T87707387_H
#define MOUSEBUTTONDOWN_T87707387_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityChan.MouseButtonDown
struct  MouseButtonDown_t87707387 
{
public:
	// System.Int32 UnityChan.MouseButtonDown::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(MouseButtonDown_t87707387, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MOUSEBUTTONDOWN_T87707387_H
#ifndef LIMITENUM_T42509660_H
#define LIMITENUM_T42509660_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.DrawerTest/DecoratorTest/LimitEnum
struct  LimitEnum_t42509660 
{
public:
	// System.Int32 Utage.DrawerTest/DecoratorTest/LimitEnum::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(LimitEnum_t42509660, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LIMITENUM_T42509660_H
#ifndef ADVEVENT_T4160079193_H
#define ADVEVENT_T4160079193_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.AdvEvent
struct  AdvEvent_t4160079193  : public UnityEvent_1_t1215103942
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ADVEVENT_T4160079193_H
#ifndef STRAGETYPE_T3621906793_H
#define STRAGETYPE_T3621906793_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.AdvEngineStarter/StrageType
struct  StrageType_t3621906793 
{
public:
	// System.Int32 Utage.AdvEngineStarter/StrageType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(StrageType_t3621906793, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STRAGETYPE_T3621906793_H
#ifndef EVAL_T1707499340_H
#define EVAL_T1707499340_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TalkScript/Eval
struct  Eval_t1707499340 
{
public:
	// System.Int32 TalkScript/Eval::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(Eval_t1707499340, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EVAL_T1707499340_H
#ifndef OBJECT_T1021602117_H
#define OBJECT_T1021602117_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Object
struct  Object_t1021602117  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Object::m_CachedPtr
	intptr_t ___m_CachedPtr_0;

public:
	inline static int32_t get_offset_of_m_CachedPtr_0() { return static_cast<int32_t>(offsetof(Object_t1021602117, ___m_CachedPtr_0)); }
	inline intptr_t get_m_CachedPtr_0() const { return ___m_CachedPtr_0; }
	inline intptr_t* get_address_of_m_CachedPtr_0() { return &___m_CachedPtr_0; }
	inline void set_m_CachedPtr_0(intptr_t value)
	{
		___m_CachedPtr_0 = value;
	}
};

struct Object_t1021602117_StaticFields
{
public:
	// System.Int32 UnityEngine.Object::OffsetOfInstanceIDInCPlusPlusObject
	int32_t ___OffsetOfInstanceIDInCPlusPlusObject_1;

public:
	inline static int32_t get_offset_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return static_cast<int32_t>(offsetof(Object_t1021602117_StaticFields, ___OffsetOfInstanceIDInCPlusPlusObject_1)); }
	inline int32_t get_OffsetOfInstanceIDInCPlusPlusObject_1() const { return ___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline int32_t* get_address_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return &___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline void set_OffsetOfInstanceIDInCPlusPlusObject_1(int32_t value)
	{
		___OffsetOfInstanceIDInCPlusPlusObject_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.Object
struct Object_t1021602117_marshaled_pinvoke
{
	intptr_t ___m_CachedPtr_0;
};
// Native definition for COM marshalling of UnityEngine.Object
struct Object_t1021602117_marshaled_com
{
	intptr_t ___m_CachedPtr_0;
};
#endif // OBJECT_T1021602117_H
#ifndef TYPE_T820837084_H
#define TYPE_T820837084_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SelectScript/Type
struct  Type_t820837084 
{
public:
	// System.Int32 SelectScript/Type::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(Type_t820837084, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TYPE_T820837084_H
#ifndef STATUS_T1544330929_H
#define STATUS_T1544330929_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityChan.AutoBlink/Status
struct  Status_t1544330929 
{
public:
	// System.Int32 UnityChan.AutoBlink/Status::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(Status_t1544330929, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STATUS_T1544330929_H
#ifndef FLAGS_T1052878605_H
#define FLAGS_T1052878605_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.DrawerTest/DecoratorTest/Flags
struct  Flags_t1052878605 
{
public:
	// System.Int32 Utage.DrawerTest/DecoratorTest/Flags::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(Flags_t1052878605, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FLAGS_T1052878605_H
#ifndef DECORATORTEST_T435098709_H
#define DECORATORTEST_T435098709_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.DrawerTest/DecoratorTest
struct  DecoratorTest_t435098709  : public RuntimeObject
{
public:
	// System.String Utage.DrawerTest/DecoratorTest::helpBox
	String_t* ___helpBox_0;
	// System.Int32 Utage.DrawerTest/DecoratorTest::hide
	int32_t ___hide_1;
	// System.Int32 Utage.DrawerTest/DecoratorTest::notEditable
	int32_t ___notEditable_2;
	// Utage.DrawerTest/DecoratorTest/Flags Utage.DrawerTest/DecoratorTest::flags
	int32_t ___flags_3;
	// Utage.DrawerTest/DecoratorTest/LimitEnum Utage.DrawerTest/DecoratorTest::lmitEnum
	int32_t ___lmitEnum_4;
	// System.String Utage.DrawerTest/DecoratorTest::stringPopup
	String_t* ___stringPopup_5;
	// System.String Utage.DrawerTest/DecoratorTest::stringPopupFunction
	String_t* ___stringPopupFunction_6;
	// System.String Utage.DrawerTest/DecoratorTest::pushButton
	String_t* ___pushButton_7;
	// System.String Utage.DrawerTest/DecoratorTest::addButton
	String_t* ___addButton_8;
	// System.String Utage.DrawerTest/DecoratorTest::path
	String_t* ___path_9;
	// Utage.MinMaxFloat Utage.DrawerTest/DecoratorTest::intervalTime
	MinMaxFloat_t1425080750 * ___intervalTime_10;
	// Utage.MinMaxInt Utage.DrawerTest/DecoratorTest::intervalTimeInt
	MinMaxInt_t3259591635 * ___intervalTimeInt_11;
	// System.Int32 Utage.DrawerTest/DecoratorTest::overridePropertyDraw
	int32_t ___overridePropertyDraw_12;

public:
	inline static int32_t get_offset_of_helpBox_0() { return static_cast<int32_t>(offsetof(DecoratorTest_t435098709, ___helpBox_0)); }
	inline String_t* get_helpBox_0() const { return ___helpBox_0; }
	inline String_t** get_address_of_helpBox_0() { return &___helpBox_0; }
	inline void set_helpBox_0(String_t* value)
	{
		___helpBox_0 = value;
		Il2CppCodeGenWriteBarrier((&___helpBox_0), value);
	}

	inline static int32_t get_offset_of_hide_1() { return static_cast<int32_t>(offsetof(DecoratorTest_t435098709, ___hide_1)); }
	inline int32_t get_hide_1() const { return ___hide_1; }
	inline int32_t* get_address_of_hide_1() { return &___hide_1; }
	inline void set_hide_1(int32_t value)
	{
		___hide_1 = value;
	}

	inline static int32_t get_offset_of_notEditable_2() { return static_cast<int32_t>(offsetof(DecoratorTest_t435098709, ___notEditable_2)); }
	inline int32_t get_notEditable_2() const { return ___notEditable_2; }
	inline int32_t* get_address_of_notEditable_2() { return &___notEditable_2; }
	inline void set_notEditable_2(int32_t value)
	{
		___notEditable_2 = value;
	}

	inline static int32_t get_offset_of_flags_3() { return static_cast<int32_t>(offsetof(DecoratorTest_t435098709, ___flags_3)); }
	inline int32_t get_flags_3() const { return ___flags_3; }
	inline int32_t* get_address_of_flags_3() { return &___flags_3; }
	inline void set_flags_3(int32_t value)
	{
		___flags_3 = value;
	}

	inline static int32_t get_offset_of_lmitEnum_4() { return static_cast<int32_t>(offsetof(DecoratorTest_t435098709, ___lmitEnum_4)); }
	inline int32_t get_lmitEnum_4() const { return ___lmitEnum_4; }
	inline int32_t* get_address_of_lmitEnum_4() { return &___lmitEnum_4; }
	inline void set_lmitEnum_4(int32_t value)
	{
		___lmitEnum_4 = value;
	}

	inline static int32_t get_offset_of_stringPopup_5() { return static_cast<int32_t>(offsetof(DecoratorTest_t435098709, ___stringPopup_5)); }
	inline String_t* get_stringPopup_5() const { return ___stringPopup_5; }
	inline String_t** get_address_of_stringPopup_5() { return &___stringPopup_5; }
	inline void set_stringPopup_5(String_t* value)
	{
		___stringPopup_5 = value;
		Il2CppCodeGenWriteBarrier((&___stringPopup_5), value);
	}

	inline static int32_t get_offset_of_stringPopupFunction_6() { return static_cast<int32_t>(offsetof(DecoratorTest_t435098709, ___stringPopupFunction_6)); }
	inline String_t* get_stringPopupFunction_6() const { return ___stringPopupFunction_6; }
	inline String_t** get_address_of_stringPopupFunction_6() { return &___stringPopupFunction_6; }
	inline void set_stringPopupFunction_6(String_t* value)
	{
		___stringPopupFunction_6 = value;
		Il2CppCodeGenWriteBarrier((&___stringPopupFunction_6), value);
	}

	inline static int32_t get_offset_of_pushButton_7() { return static_cast<int32_t>(offsetof(DecoratorTest_t435098709, ___pushButton_7)); }
	inline String_t* get_pushButton_7() const { return ___pushButton_7; }
	inline String_t** get_address_of_pushButton_7() { return &___pushButton_7; }
	inline void set_pushButton_7(String_t* value)
	{
		___pushButton_7 = value;
		Il2CppCodeGenWriteBarrier((&___pushButton_7), value);
	}

	inline static int32_t get_offset_of_addButton_8() { return static_cast<int32_t>(offsetof(DecoratorTest_t435098709, ___addButton_8)); }
	inline String_t* get_addButton_8() const { return ___addButton_8; }
	inline String_t** get_address_of_addButton_8() { return &___addButton_8; }
	inline void set_addButton_8(String_t* value)
	{
		___addButton_8 = value;
		Il2CppCodeGenWriteBarrier((&___addButton_8), value);
	}

	inline static int32_t get_offset_of_path_9() { return static_cast<int32_t>(offsetof(DecoratorTest_t435098709, ___path_9)); }
	inline String_t* get_path_9() const { return ___path_9; }
	inline String_t** get_address_of_path_9() { return &___path_9; }
	inline void set_path_9(String_t* value)
	{
		___path_9 = value;
		Il2CppCodeGenWriteBarrier((&___path_9), value);
	}

	inline static int32_t get_offset_of_intervalTime_10() { return static_cast<int32_t>(offsetof(DecoratorTest_t435098709, ___intervalTime_10)); }
	inline MinMaxFloat_t1425080750 * get_intervalTime_10() const { return ___intervalTime_10; }
	inline MinMaxFloat_t1425080750 ** get_address_of_intervalTime_10() { return &___intervalTime_10; }
	inline void set_intervalTime_10(MinMaxFloat_t1425080750 * value)
	{
		___intervalTime_10 = value;
		Il2CppCodeGenWriteBarrier((&___intervalTime_10), value);
	}

	inline static int32_t get_offset_of_intervalTimeInt_11() { return static_cast<int32_t>(offsetof(DecoratorTest_t435098709, ___intervalTimeInt_11)); }
	inline MinMaxInt_t3259591635 * get_intervalTimeInt_11() const { return ___intervalTimeInt_11; }
	inline MinMaxInt_t3259591635 ** get_address_of_intervalTimeInt_11() { return &___intervalTimeInt_11; }
	inline void set_intervalTimeInt_11(MinMaxInt_t3259591635 * value)
	{
		___intervalTimeInt_11 = value;
		Il2CppCodeGenWriteBarrier((&___intervalTimeInt_11), value);
	}

	inline static int32_t get_offset_of_overridePropertyDraw_12() { return static_cast<int32_t>(offsetof(DecoratorTest_t435098709, ___overridePropertyDraw_12)); }
	inline int32_t get_overridePropertyDraw_12() const { return ___overridePropertyDraw_12; }
	inline int32_t* get_address_of_overridePropertyDraw_12() { return &___overridePropertyDraw_12; }
	inline void set_overridePropertyDraw_12(int32_t value)
	{
		___overridePropertyDraw_12 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DECORATORTEST_T435098709_H
#ifndef COMPONENT_T3819376471_H
#define COMPONENT_T3819376471_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Component
struct  Component_t3819376471  : public Object_t1021602117
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPONENT_T3819376471_H
#ifndef BEHAVIOUR_T955675639_H
#define BEHAVIOUR_T955675639_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Behaviour
struct  Behaviour_t955675639  : public Component_t3819376471
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BEHAVIOUR_T955675639_H
#ifndef MONOBEHAVIOUR_T1158329972_H
#define MONOBEHAVIOUR_T1158329972_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.MonoBehaviour
struct  MonoBehaviour_t1158329972  : public Behaviour_t955675639
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MONOBEHAVIOUR_T1158329972_H
#ifndef DRAWERTEST_T1088500511_H
#define DRAWERTEST_T1088500511_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.DrawerTest
struct  DrawerTest_t1088500511  : public MonoBehaviour_t1158329972
{
public:
	// System.String Utage.DrawerTest::helpBox
	String_t* ___helpBox_2;
	// Utage.DrawerTest/DecoratorTest Utage.DrawerTest::decoratorTest
	DecoratorTest_t435098709 * ___decoratorTest_3;
	// System.Boolean Utage.DrawerTest::isOverridePropertyDrawEditable
	bool ___isOverridePropertyDrawEditable_4;

public:
	inline static int32_t get_offset_of_helpBox_2() { return static_cast<int32_t>(offsetof(DrawerTest_t1088500511, ___helpBox_2)); }
	inline String_t* get_helpBox_2() const { return ___helpBox_2; }
	inline String_t** get_address_of_helpBox_2() { return &___helpBox_2; }
	inline void set_helpBox_2(String_t* value)
	{
		___helpBox_2 = value;
		Il2CppCodeGenWriteBarrier((&___helpBox_2), value);
	}

	inline static int32_t get_offset_of_decoratorTest_3() { return static_cast<int32_t>(offsetof(DrawerTest_t1088500511, ___decoratorTest_3)); }
	inline DecoratorTest_t435098709 * get_decoratorTest_3() const { return ___decoratorTest_3; }
	inline DecoratorTest_t435098709 ** get_address_of_decoratorTest_3() { return &___decoratorTest_3; }
	inline void set_decoratorTest_3(DecoratorTest_t435098709 * value)
	{
		___decoratorTest_3 = value;
		Il2CppCodeGenWriteBarrier((&___decoratorTest_3), value);
	}

	inline static int32_t get_offset_of_isOverridePropertyDrawEditable_4() { return static_cast<int32_t>(offsetof(DrawerTest_t1088500511, ___isOverridePropertyDrawEditable_4)); }
	inline bool get_isOverridePropertyDrawEditable_4() const { return ___isOverridePropertyDrawEditable_4; }
	inline bool* get_address_of_isOverridePropertyDrawEditable_4() { return &___isOverridePropertyDrawEditable_4; }
	inline void set_isOverridePropertyDrawEditable_4(bool value)
	{
		___isOverridePropertyDrawEditable_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DRAWERTEST_T1088500511_H
#ifndef SAMPLECUSTOMASSETBUNDLELOAD_T3658166813_H
#define SAMPLECUSTOMASSETBUNDLELOAD_T3658166813_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.SampleCustomAssetBundleLoad
struct  SampleCustomAssetBundleLoad_t3658166813  : public MonoBehaviour_t1158329972
{
public:
	// System.String Utage.SampleCustomAssetBundleLoad::startScenario
	String_t* ___startScenario_2;
	// Utage.AdvEngine Utage.SampleCustomAssetBundleLoad::engine
	AdvEngine_t1176753927 * ___engine_3;
	// System.Collections.Generic.List`1<Utage.SampleCustomAssetBundleLoad/SampleAssetBundleVersionInfo> Utage.SampleCustomAssetBundleLoad::assetBundleList
	List_1_t683720528 * ___assetBundleList_4;
	// Utage.AdvImportScenarios Utage.SampleCustomAssetBundleLoad::<Scenarios>k__BackingField
	AdvImportScenarios_t1226385437 * ___U3CScenariosU3Ek__BackingField_5;

public:
	inline static int32_t get_offset_of_startScenario_2() { return static_cast<int32_t>(offsetof(SampleCustomAssetBundleLoad_t3658166813, ___startScenario_2)); }
	inline String_t* get_startScenario_2() const { return ___startScenario_2; }
	inline String_t** get_address_of_startScenario_2() { return &___startScenario_2; }
	inline void set_startScenario_2(String_t* value)
	{
		___startScenario_2 = value;
		Il2CppCodeGenWriteBarrier((&___startScenario_2), value);
	}

	inline static int32_t get_offset_of_engine_3() { return static_cast<int32_t>(offsetof(SampleCustomAssetBundleLoad_t3658166813, ___engine_3)); }
	inline AdvEngine_t1176753927 * get_engine_3() const { return ___engine_3; }
	inline AdvEngine_t1176753927 ** get_address_of_engine_3() { return &___engine_3; }
	inline void set_engine_3(AdvEngine_t1176753927 * value)
	{
		___engine_3 = value;
		Il2CppCodeGenWriteBarrier((&___engine_3), value);
	}

	inline static int32_t get_offset_of_assetBundleList_4() { return static_cast<int32_t>(offsetof(SampleCustomAssetBundleLoad_t3658166813, ___assetBundleList_4)); }
	inline List_1_t683720528 * get_assetBundleList_4() const { return ___assetBundleList_4; }
	inline List_1_t683720528 ** get_address_of_assetBundleList_4() { return &___assetBundleList_4; }
	inline void set_assetBundleList_4(List_1_t683720528 * value)
	{
		___assetBundleList_4 = value;
		Il2CppCodeGenWriteBarrier((&___assetBundleList_4), value);
	}

	inline static int32_t get_offset_of_U3CScenariosU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(SampleCustomAssetBundleLoad_t3658166813, ___U3CScenariosU3Ek__BackingField_5)); }
	inline AdvImportScenarios_t1226385437 * get_U3CScenariosU3Ek__BackingField_5() const { return ___U3CScenariosU3Ek__BackingField_5; }
	inline AdvImportScenarios_t1226385437 ** get_address_of_U3CScenariosU3Ek__BackingField_5() { return &___U3CScenariosU3Ek__BackingField_5; }
	inline void set_U3CScenariosU3Ek__BackingField_5(AdvImportScenarios_t1226385437 * value)
	{
		___U3CScenariosU3Ek__BackingField_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3CScenariosU3Ek__BackingField_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SAMPLECUSTOMASSETBUNDLELOAD_T3658166813_H
#ifndef SAMPLETIPS_T2223945158_H
#define SAMPLETIPS_T2223945158_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.SampleTips
struct  SampleTips_t2223945158  : public MonoBehaviour_t1158329972
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SAMPLETIPS_T2223945158_H
#ifndef ADVENGINESTARTER_T330044300_H
#define ADVENGINESTARTER_T330044300_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.AdvEngineStarter
struct  AdvEngineStarter_t330044300  : public MonoBehaviour_t1158329972
{
public:
	// System.Boolean Utage.AdvEngineStarter::isLoadOnAwake
	bool ___isLoadOnAwake_2;
	// System.Boolean Utage.AdvEngineStarter::isAutomaticPlay
	bool ___isAutomaticPlay_3;
	// System.String Utage.AdvEngineStarter::startScenario
	String_t* ___startScenario_4;
	// Utage.AdvEngine Utage.AdvEngineStarter::engine
	AdvEngine_t1176753927 * ___engine_5;
	// Utage.AdvEngineStarter/StrageType Utage.AdvEngineStarter::strageType
	int32_t ___strageType_6;
	// Utage.AdvImportScenarios Utage.AdvEngineStarter::scenarios
	AdvImportScenarios_t1226385437 * ___scenarios_7;
	// System.String Utage.AdvEngineStarter::rootResourceDir
	String_t* ___rootResourceDir_8;
	// System.String Utage.AdvEngineStarter::serverUrl
	String_t* ___serverUrl_9;
	// System.String Utage.AdvEngineStarter::scenariosName
	String_t* ___scenariosName_10;
	// System.Boolean Utage.AdvEngineStarter::useChapter
	bool ___useChapter_11;
	// System.Collections.Generic.List`1<System.String> Utage.AdvEngineStarter::chapterNames
	List_1_t1398341365 * ___chapterNames_12;
	// System.Boolean Utage.AdvEngineStarter::<IsLoadStart>k__BackingField
	bool ___U3CIsLoadStartU3Ek__BackingField_13;

public:
	inline static int32_t get_offset_of_isLoadOnAwake_2() { return static_cast<int32_t>(offsetof(AdvEngineStarter_t330044300, ___isLoadOnAwake_2)); }
	inline bool get_isLoadOnAwake_2() const { return ___isLoadOnAwake_2; }
	inline bool* get_address_of_isLoadOnAwake_2() { return &___isLoadOnAwake_2; }
	inline void set_isLoadOnAwake_2(bool value)
	{
		___isLoadOnAwake_2 = value;
	}

	inline static int32_t get_offset_of_isAutomaticPlay_3() { return static_cast<int32_t>(offsetof(AdvEngineStarter_t330044300, ___isAutomaticPlay_3)); }
	inline bool get_isAutomaticPlay_3() const { return ___isAutomaticPlay_3; }
	inline bool* get_address_of_isAutomaticPlay_3() { return &___isAutomaticPlay_3; }
	inline void set_isAutomaticPlay_3(bool value)
	{
		___isAutomaticPlay_3 = value;
	}

	inline static int32_t get_offset_of_startScenario_4() { return static_cast<int32_t>(offsetof(AdvEngineStarter_t330044300, ___startScenario_4)); }
	inline String_t* get_startScenario_4() const { return ___startScenario_4; }
	inline String_t** get_address_of_startScenario_4() { return &___startScenario_4; }
	inline void set_startScenario_4(String_t* value)
	{
		___startScenario_4 = value;
		Il2CppCodeGenWriteBarrier((&___startScenario_4), value);
	}

	inline static int32_t get_offset_of_engine_5() { return static_cast<int32_t>(offsetof(AdvEngineStarter_t330044300, ___engine_5)); }
	inline AdvEngine_t1176753927 * get_engine_5() const { return ___engine_5; }
	inline AdvEngine_t1176753927 ** get_address_of_engine_5() { return &___engine_5; }
	inline void set_engine_5(AdvEngine_t1176753927 * value)
	{
		___engine_5 = value;
		Il2CppCodeGenWriteBarrier((&___engine_5), value);
	}

	inline static int32_t get_offset_of_strageType_6() { return static_cast<int32_t>(offsetof(AdvEngineStarter_t330044300, ___strageType_6)); }
	inline int32_t get_strageType_6() const { return ___strageType_6; }
	inline int32_t* get_address_of_strageType_6() { return &___strageType_6; }
	inline void set_strageType_6(int32_t value)
	{
		___strageType_6 = value;
	}

	inline static int32_t get_offset_of_scenarios_7() { return static_cast<int32_t>(offsetof(AdvEngineStarter_t330044300, ___scenarios_7)); }
	inline AdvImportScenarios_t1226385437 * get_scenarios_7() const { return ___scenarios_7; }
	inline AdvImportScenarios_t1226385437 ** get_address_of_scenarios_7() { return &___scenarios_7; }
	inline void set_scenarios_7(AdvImportScenarios_t1226385437 * value)
	{
		___scenarios_7 = value;
		Il2CppCodeGenWriteBarrier((&___scenarios_7), value);
	}

	inline static int32_t get_offset_of_rootResourceDir_8() { return static_cast<int32_t>(offsetof(AdvEngineStarter_t330044300, ___rootResourceDir_8)); }
	inline String_t* get_rootResourceDir_8() const { return ___rootResourceDir_8; }
	inline String_t** get_address_of_rootResourceDir_8() { return &___rootResourceDir_8; }
	inline void set_rootResourceDir_8(String_t* value)
	{
		___rootResourceDir_8 = value;
		Il2CppCodeGenWriteBarrier((&___rootResourceDir_8), value);
	}

	inline static int32_t get_offset_of_serverUrl_9() { return static_cast<int32_t>(offsetof(AdvEngineStarter_t330044300, ___serverUrl_9)); }
	inline String_t* get_serverUrl_9() const { return ___serverUrl_9; }
	inline String_t** get_address_of_serverUrl_9() { return &___serverUrl_9; }
	inline void set_serverUrl_9(String_t* value)
	{
		___serverUrl_9 = value;
		Il2CppCodeGenWriteBarrier((&___serverUrl_9), value);
	}

	inline static int32_t get_offset_of_scenariosName_10() { return static_cast<int32_t>(offsetof(AdvEngineStarter_t330044300, ___scenariosName_10)); }
	inline String_t* get_scenariosName_10() const { return ___scenariosName_10; }
	inline String_t** get_address_of_scenariosName_10() { return &___scenariosName_10; }
	inline void set_scenariosName_10(String_t* value)
	{
		___scenariosName_10 = value;
		Il2CppCodeGenWriteBarrier((&___scenariosName_10), value);
	}

	inline static int32_t get_offset_of_useChapter_11() { return static_cast<int32_t>(offsetof(AdvEngineStarter_t330044300, ___useChapter_11)); }
	inline bool get_useChapter_11() const { return ___useChapter_11; }
	inline bool* get_address_of_useChapter_11() { return &___useChapter_11; }
	inline void set_useChapter_11(bool value)
	{
		___useChapter_11 = value;
	}

	inline static int32_t get_offset_of_chapterNames_12() { return static_cast<int32_t>(offsetof(AdvEngineStarter_t330044300, ___chapterNames_12)); }
	inline List_1_t1398341365 * get_chapterNames_12() const { return ___chapterNames_12; }
	inline List_1_t1398341365 ** get_address_of_chapterNames_12() { return &___chapterNames_12; }
	inline void set_chapterNames_12(List_1_t1398341365 * value)
	{
		___chapterNames_12 = value;
		Il2CppCodeGenWriteBarrier((&___chapterNames_12), value);
	}

	inline static int32_t get_offset_of_U3CIsLoadStartU3Ek__BackingField_13() { return static_cast<int32_t>(offsetof(AdvEngineStarter_t330044300, ___U3CIsLoadStartU3Ek__BackingField_13)); }
	inline bool get_U3CIsLoadStartU3Ek__BackingField_13() const { return ___U3CIsLoadStartU3Ek__BackingField_13; }
	inline bool* get_address_of_U3CIsLoadStartU3Ek__BackingField_13() { return &___U3CIsLoadStartU3Ek__BackingField_13; }
	inline void set_U3CIsLoadStartU3Ek__BackingField_13(bool value)
	{
		___U3CIsLoadStartU3Ek__BackingField_13 = value;
	}
};

struct AdvEngineStarter_t330044300_StaticFields
{
public:
	// System.Action Utage.AdvEngineStarter::<>f__am$cache0
	Action_t3226471752 * ___U3CU3Ef__amU24cache0_14;

public:
	inline static int32_t get_offset_of_U3CU3Ef__amU24cache0_14() { return static_cast<int32_t>(offsetof(AdvEngineStarter_t330044300_StaticFields, ___U3CU3Ef__amU24cache0_14)); }
	inline Action_t3226471752 * get_U3CU3Ef__amU24cache0_14() const { return ___U3CU3Ef__amU24cache0_14; }
	inline Action_t3226471752 ** get_address_of_U3CU3Ef__amU24cache0_14() { return &___U3CU3Ef__amU24cache0_14; }
	inline void set_U3CU3Ef__amU24cache0_14(Action_t3226471752 * value)
	{
		___U3CU3Ef__amU24cache0_14 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache0_14), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ADVENGINESTARTER_T330044300_H
#ifndef ADVENGINE_T1176753927_H
#define ADVENGINE_T1176753927_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.AdvEngine
struct  AdvEngine_t1176753927  : public MonoBehaviour_t1158329972
{
public:
	// System.String Utage.AdvEngine::startScenarioLabel
	String_t* ___startScenarioLabel_2;
	// Utage.AdvDataManager Utage.AdvEngine::dataManager
	AdvDataManager_t2481232830 * ___dataManager_3;
	// Utage.AdvScenarioPlayer Utage.AdvEngine::scenarioPlayer
	AdvScenarioPlayer_t1295831480 * ___scenarioPlayer_4;
	// Utage.AdvPage Utage.AdvEngine::page
	AdvPage_t2715018132 * ___page_5;
	// Utage.AdvSelectionManager Utage.AdvEngine::selectionManager
	AdvSelectionManager_t3429904078 * ___selectionManager_6;
	// Utage.AdvMessageWindowManager Utage.AdvEngine::messageWindowManager
	AdvMessageWindowManager_t1170996005 * ___messageWindowManager_7;
	// Utage.AdvBacklogManager Utage.AdvEngine::backlogManager
	AdvBacklogManager_t3496936997 * ___backlogManager_8;
	// Utage.AdvConfig Utage.AdvEngine::config
	AdvConfig_t2941837115 * ___config_9;
	// Utage.AdvSystemSaveData Utage.AdvEngine::systemSaveData
	AdvSystemSaveData_t276732409 * ___systemSaveData_10;
	// Utage.AdvSaveManager Utage.AdvEngine::saveManager
	AdvSaveManager_t2382020483 * ___saveManager_11;
	// Utage.AdvGraphicManager Utage.AdvEngine::graphicManager
	AdvGraphicManager_t3661251000 * ___graphicManager_12;
	// Utage.AdvEffectManager Utage.AdvEngine::effectManager
	AdvEffectManager_t3829746105 * ___effectManager_13;
	// Utage.AdvUiManager Utage.AdvEngine::uiManager
	AdvUiManager_t4018716000 * ___uiManager_14;
	// Utage.SoundManager Utage.AdvEngine::soundManager
	SoundManager_t1265124084 * ___soundManager_15;
	// Utage.CameraManager Utage.AdvEngine::cameraManager
	CameraManager_t586526220 * ___cameraManager_16;
	// Utage.AdvParamManager Utage.AdvEngine::param
	AdvParamManager_t1816006425 * ___param_17;
	// System.Boolean Utage.AdvEngine::bootAsync
	bool ___bootAsync_18;
	// System.Boolean Utage.AdvEngine::isStopSoundOnStart
	bool ___isStopSoundOnStart_19;
	// System.Boolean Utage.AdvEngine::isStopSoundOnEnd
	bool ___isStopSoundOnEnd_20;
	// System.Collections.Generic.List`1<Utage.AdvCustomCommandManager> Utage.AdvEngine::customCommandManagerList
	List_1_t3842602520 * ___customCommandManagerList_21;
	// UnityEngine.Events.UnityEvent Utage.AdvEngine::onPreInit
	UnityEvent_t408735097 * ___onPreInit_22;
	// Utage.OpenDialogEvent Utage.AdvEngine::onOpenDialog
	OpenDialogEvent_t2352564994 * ___onOpenDialog_23;
	// Utage.AdvEvent Utage.AdvEngine::onPageTextChange
	AdvEvent_t4160079193 * ___onPageTextChange_24;
	// Utage.AdvEvent Utage.AdvEngine::OnClear
	AdvEvent_t4160079193 * ___OnClear_25;
	// Utage.AdvEvent Utage.AdvEngine::onChangeLanguage
	AdvEvent_t4160079193 * ___onChangeLanguage_26;
	// System.Boolean Utage.AdvEngine::isWaitBootLoading
	bool ___isWaitBootLoading_27;
	// System.Boolean Utage.AdvEngine::isStarted
	bool ___isStarted_28;
	// System.Boolean Utage.AdvEngine::isSceneGallery
	bool ___isSceneGallery_29;

public:
	inline static int32_t get_offset_of_startScenarioLabel_2() { return static_cast<int32_t>(offsetof(AdvEngine_t1176753927, ___startScenarioLabel_2)); }
	inline String_t* get_startScenarioLabel_2() const { return ___startScenarioLabel_2; }
	inline String_t** get_address_of_startScenarioLabel_2() { return &___startScenarioLabel_2; }
	inline void set_startScenarioLabel_2(String_t* value)
	{
		___startScenarioLabel_2 = value;
		Il2CppCodeGenWriteBarrier((&___startScenarioLabel_2), value);
	}

	inline static int32_t get_offset_of_dataManager_3() { return static_cast<int32_t>(offsetof(AdvEngine_t1176753927, ___dataManager_3)); }
	inline AdvDataManager_t2481232830 * get_dataManager_3() const { return ___dataManager_3; }
	inline AdvDataManager_t2481232830 ** get_address_of_dataManager_3() { return &___dataManager_3; }
	inline void set_dataManager_3(AdvDataManager_t2481232830 * value)
	{
		___dataManager_3 = value;
		Il2CppCodeGenWriteBarrier((&___dataManager_3), value);
	}

	inline static int32_t get_offset_of_scenarioPlayer_4() { return static_cast<int32_t>(offsetof(AdvEngine_t1176753927, ___scenarioPlayer_4)); }
	inline AdvScenarioPlayer_t1295831480 * get_scenarioPlayer_4() const { return ___scenarioPlayer_4; }
	inline AdvScenarioPlayer_t1295831480 ** get_address_of_scenarioPlayer_4() { return &___scenarioPlayer_4; }
	inline void set_scenarioPlayer_4(AdvScenarioPlayer_t1295831480 * value)
	{
		___scenarioPlayer_4 = value;
		Il2CppCodeGenWriteBarrier((&___scenarioPlayer_4), value);
	}

	inline static int32_t get_offset_of_page_5() { return static_cast<int32_t>(offsetof(AdvEngine_t1176753927, ___page_5)); }
	inline AdvPage_t2715018132 * get_page_5() const { return ___page_5; }
	inline AdvPage_t2715018132 ** get_address_of_page_5() { return &___page_5; }
	inline void set_page_5(AdvPage_t2715018132 * value)
	{
		___page_5 = value;
		Il2CppCodeGenWriteBarrier((&___page_5), value);
	}

	inline static int32_t get_offset_of_selectionManager_6() { return static_cast<int32_t>(offsetof(AdvEngine_t1176753927, ___selectionManager_6)); }
	inline AdvSelectionManager_t3429904078 * get_selectionManager_6() const { return ___selectionManager_6; }
	inline AdvSelectionManager_t3429904078 ** get_address_of_selectionManager_6() { return &___selectionManager_6; }
	inline void set_selectionManager_6(AdvSelectionManager_t3429904078 * value)
	{
		___selectionManager_6 = value;
		Il2CppCodeGenWriteBarrier((&___selectionManager_6), value);
	}

	inline static int32_t get_offset_of_messageWindowManager_7() { return static_cast<int32_t>(offsetof(AdvEngine_t1176753927, ___messageWindowManager_7)); }
	inline AdvMessageWindowManager_t1170996005 * get_messageWindowManager_7() const { return ___messageWindowManager_7; }
	inline AdvMessageWindowManager_t1170996005 ** get_address_of_messageWindowManager_7() { return &___messageWindowManager_7; }
	inline void set_messageWindowManager_7(AdvMessageWindowManager_t1170996005 * value)
	{
		___messageWindowManager_7 = value;
		Il2CppCodeGenWriteBarrier((&___messageWindowManager_7), value);
	}

	inline static int32_t get_offset_of_backlogManager_8() { return static_cast<int32_t>(offsetof(AdvEngine_t1176753927, ___backlogManager_8)); }
	inline AdvBacklogManager_t3496936997 * get_backlogManager_8() const { return ___backlogManager_8; }
	inline AdvBacklogManager_t3496936997 ** get_address_of_backlogManager_8() { return &___backlogManager_8; }
	inline void set_backlogManager_8(AdvBacklogManager_t3496936997 * value)
	{
		___backlogManager_8 = value;
		Il2CppCodeGenWriteBarrier((&___backlogManager_8), value);
	}

	inline static int32_t get_offset_of_config_9() { return static_cast<int32_t>(offsetof(AdvEngine_t1176753927, ___config_9)); }
	inline AdvConfig_t2941837115 * get_config_9() const { return ___config_9; }
	inline AdvConfig_t2941837115 ** get_address_of_config_9() { return &___config_9; }
	inline void set_config_9(AdvConfig_t2941837115 * value)
	{
		___config_9 = value;
		Il2CppCodeGenWriteBarrier((&___config_9), value);
	}

	inline static int32_t get_offset_of_systemSaveData_10() { return static_cast<int32_t>(offsetof(AdvEngine_t1176753927, ___systemSaveData_10)); }
	inline AdvSystemSaveData_t276732409 * get_systemSaveData_10() const { return ___systemSaveData_10; }
	inline AdvSystemSaveData_t276732409 ** get_address_of_systemSaveData_10() { return &___systemSaveData_10; }
	inline void set_systemSaveData_10(AdvSystemSaveData_t276732409 * value)
	{
		___systemSaveData_10 = value;
		Il2CppCodeGenWriteBarrier((&___systemSaveData_10), value);
	}

	inline static int32_t get_offset_of_saveManager_11() { return static_cast<int32_t>(offsetof(AdvEngine_t1176753927, ___saveManager_11)); }
	inline AdvSaveManager_t2382020483 * get_saveManager_11() const { return ___saveManager_11; }
	inline AdvSaveManager_t2382020483 ** get_address_of_saveManager_11() { return &___saveManager_11; }
	inline void set_saveManager_11(AdvSaveManager_t2382020483 * value)
	{
		___saveManager_11 = value;
		Il2CppCodeGenWriteBarrier((&___saveManager_11), value);
	}

	inline static int32_t get_offset_of_graphicManager_12() { return static_cast<int32_t>(offsetof(AdvEngine_t1176753927, ___graphicManager_12)); }
	inline AdvGraphicManager_t3661251000 * get_graphicManager_12() const { return ___graphicManager_12; }
	inline AdvGraphicManager_t3661251000 ** get_address_of_graphicManager_12() { return &___graphicManager_12; }
	inline void set_graphicManager_12(AdvGraphicManager_t3661251000 * value)
	{
		___graphicManager_12 = value;
		Il2CppCodeGenWriteBarrier((&___graphicManager_12), value);
	}

	inline static int32_t get_offset_of_effectManager_13() { return static_cast<int32_t>(offsetof(AdvEngine_t1176753927, ___effectManager_13)); }
	inline AdvEffectManager_t3829746105 * get_effectManager_13() const { return ___effectManager_13; }
	inline AdvEffectManager_t3829746105 ** get_address_of_effectManager_13() { return &___effectManager_13; }
	inline void set_effectManager_13(AdvEffectManager_t3829746105 * value)
	{
		___effectManager_13 = value;
		Il2CppCodeGenWriteBarrier((&___effectManager_13), value);
	}

	inline static int32_t get_offset_of_uiManager_14() { return static_cast<int32_t>(offsetof(AdvEngine_t1176753927, ___uiManager_14)); }
	inline AdvUiManager_t4018716000 * get_uiManager_14() const { return ___uiManager_14; }
	inline AdvUiManager_t4018716000 ** get_address_of_uiManager_14() { return &___uiManager_14; }
	inline void set_uiManager_14(AdvUiManager_t4018716000 * value)
	{
		___uiManager_14 = value;
		Il2CppCodeGenWriteBarrier((&___uiManager_14), value);
	}

	inline static int32_t get_offset_of_soundManager_15() { return static_cast<int32_t>(offsetof(AdvEngine_t1176753927, ___soundManager_15)); }
	inline SoundManager_t1265124084 * get_soundManager_15() const { return ___soundManager_15; }
	inline SoundManager_t1265124084 ** get_address_of_soundManager_15() { return &___soundManager_15; }
	inline void set_soundManager_15(SoundManager_t1265124084 * value)
	{
		___soundManager_15 = value;
		Il2CppCodeGenWriteBarrier((&___soundManager_15), value);
	}

	inline static int32_t get_offset_of_cameraManager_16() { return static_cast<int32_t>(offsetof(AdvEngine_t1176753927, ___cameraManager_16)); }
	inline CameraManager_t586526220 * get_cameraManager_16() const { return ___cameraManager_16; }
	inline CameraManager_t586526220 ** get_address_of_cameraManager_16() { return &___cameraManager_16; }
	inline void set_cameraManager_16(CameraManager_t586526220 * value)
	{
		___cameraManager_16 = value;
		Il2CppCodeGenWriteBarrier((&___cameraManager_16), value);
	}

	inline static int32_t get_offset_of_param_17() { return static_cast<int32_t>(offsetof(AdvEngine_t1176753927, ___param_17)); }
	inline AdvParamManager_t1816006425 * get_param_17() const { return ___param_17; }
	inline AdvParamManager_t1816006425 ** get_address_of_param_17() { return &___param_17; }
	inline void set_param_17(AdvParamManager_t1816006425 * value)
	{
		___param_17 = value;
		Il2CppCodeGenWriteBarrier((&___param_17), value);
	}

	inline static int32_t get_offset_of_bootAsync_18() { return static_cast<int32_t>(offsetof(AdvEngine_t1176753927, ___bootAsync_18)); }
	inline bool get_bootAsync_18() const { return ___bootAsync_18; }
	inline bool* get_address_of_bootAsync_18() { return &___bootAsync_18; }
	inline void set_bootAsync_18(bool value)
	{
		___bootAsync_18 = value;
	}

	inline static int32_t get_offset_of_isStopSoundOnStart_19() { return static_cast<int32_t>(offsetof(AdvEngine_t1176753927, ___isStopSoundOnStart_19)); }
	inline bool get_isStopSoundOnStart_19() const { return ___isStopSoundOnStart_19; }
	inline bool* get_address_of_isStopSoundOnStart_19() { return &___isStopSoundOnStart_19; }
	inline void set_isStopSoundOnStart_19(bool value)
	{
		___isStopSoundOnStart_19 = value;
	}

	inline static int32_t get_offset_of_isStopSoundOnEnd_20() { return static_cast<int32_t>(offsetof(AdvEngine_t1176753927, ___isStopSoundOnEnd_20)); }
	inline bool get_isStopSoundOnEnd_20() const { return ___isStopSoundOnEnd_20; }
	inline bool* get_address_of_isStopSoundOnEnd_20() { return &___isStopSoundOnEnd_20; }
	inline void set_isStopSoundOnEnd_20(bool value)
	{
		___isStopSoundOnEnd_20 = value;
	}

	inline static int32_t get_offset_of_customCommandManagerList_21() { return static_cast<int32_t>(offsetof(AdvEngine_t1176753927, ___customCommandManagerList_21)); }
	inline List_1_t3842602520 * get_customCommandManagerList_21() const { return ___customCommandManagerList_21; }
	inline List_1_t3842602520 ** get_address_of_customCommandManagerList_21() { return &___customCommandManagerList_21; }
	inline void set_customCommandManagerList_21(List_1_t3842602520 * value)
	{
		___customCommandManagerList_21 = value;
		Il2CppCodeGenWriteBarrier((&___customCommandManagerList_21), value);
	}

	inline static int32_t get_offset_of_onPreInit_22() { return static_cast<int32_t>(offsetof(AdvEngine_t1176753927, ___onPreInit_22)); }
	inline UnityEvent_t408735097 * get_onPreInit_22() const { return ___onPreInit_22; }
	inline UnityEvent_t408735097 ** get_address_of_onPreInit_22() { return &___onPreInit_22; }
	inline void set_onPreInit_22(UnityEvent_t408735097 * value)
	{
		___onPreInit_22 = value;
		Il2CppCodeGenWriteBarrier((&___onPreInit_22), value);
	}

	inline static int32_t get_offset_of_onOpenDialog_23() { return static_cast<int32_t>(offsetof(AdvEngine_t1176753927, ___onOpenDialog_23)); }
	inline OpenDialogEvent_t2352564994 * get_onOpenDialog_23() const { return ___onOpenDialog_23; }
	inline OpenDialogEvent_t2352564994 ** get_address_of_onOpenDialog_23() { return &___onOpenDialog_23; }
	inline void set_onOpenDialog_23(OpenDialogEvent_t2352564994 * value)
	{
		___onOpenDialog_23 = value;
		Il2CppCodeGenWriteBarrier((&___onOpenDialog_23), value);
	}

	inline static int32_t get_offset_of_onPageTextChange_24() { return static_cast<int32_t>(offsetof(AdvEngine_t1176753927, ___onPageTextChange_24)); }
	inline AdvEvent_t4160079193 * get_onPageTextChange_24() const { return ___onPageTextChange_24; }
	inline AdvEvent_t4160079193 ** get_address_of_onPageTextChange_24() { return &___onPageTextChange_24; }
	inline void set_onPageTextChange_24(AdvEvent_t4160079193 * value)
	{
		___onPageTextChange_24 = value;
		Il2CppCodeGenWriteBarrier((&___onPageTextChange_24), value);
	}

	inline static int32_t get_offset_of_OnClear_25() { return static_cast<int32_t>(offsetof(AdvEngine_t1176753927, ___OnClear_25)); }
	inline AdvEvent_t4160079193 * get_OnClear_25() const { return ___OnClear_25; }
	inline AdvEvent_t4160079193 ** get_address_of_OnClear_25() { return &___OnClear_25; }
	inline void set_OnClear_25(AdvEvent_t4160079193 * value)
	{
		___OnClear_25 = value;
		Il2CppCodeGenWriteBarrier((&___OnClear_25), value);
	}

	inline static int32_t get_offset_of_onChangeLanguage_26() { return static_cast<int32_t>(offsetof(AdvEngine_t1176753927, ___onChangeLanguage_26)); }
	inline AdvEvent_t4160079193 * get_onChangeLanguage_26() const { return ___onChangeLanguage_26; }
	inline AdvEvent_t4160079193 ** get_address_of_onChangeLanguage_26() { return &___onChangeLanguage_26; }
	inline void set_onChangeLanguage_26(AdvEvent_t4160079193 * value)
	{
		___onChangeLanguage_26 = value;
		Il2CppCodeGenWriteBarrier((&___onChangeLanguage_26), value);
	}

	inline static int32_t get_offset_of_isWaitBootLoading_27() { return static_cast<int32_t>(offsetof(AdvEngine_t1176753927, ___isWaitBootLoading_27)); }
	inline bool get_isWaitBootLoading_27() const { return ___isWaitBootLoading_27; }
	inline bool* get_address_of_isWaitBootLoading_27() { return &___isWaitBootLoading_27; }
	inline void set_isWaitBootLoading_27(bool value)
	{
		___isWaitBootLoading_27 = value;
	}

	inline static int32_t get_offset_of_isStarted_28() { return static_cast<int32_t>(offsetof(AdvEngine_t1176753927, ___isStarted_28)); }
	inline bool get_isStarted_28() const { return ___isStarted_28; }
	inline bool* get_address_of_isStarted_28() { return &___isStarted_28; }
	inline void set_isStarted_28(bool value)
	{
		___isStarted_28 = value;
	}

	inline static int32_t get_offset_of_isSceneGallery_29() { return static_cast<int32_t>(offsetof(AdvEngine_t1176753927, ___isSceneGallery_29)); }
	inline bool get_isSceneGallery_29() const { return ___isSceneGallery_29; }
	inline bool* get_address_of_isSceneGallery_29() { return &___isSceneGallery_29; }
	inline void set_isSceneGallery_29(bool value)
	{
		___isSceneGallery_29 = value;
	}
};

struct AdvEngine_t1176753927_StaticFields
{
public:
	// System.Action`1<Utage.IBinaryIO> Utage.AdvEngine::<>f__am$cache0
	Action_1_t1200141114 * ___U3CU3Ef__amU24cache0_30;
	// System.Action`1<Utage.IBinaryIO> Utage.AdvEngine::<>f__am$cache1
	Action_1_t1200141114 * ___U3CU3Ef__amU24cache1_31;

public:
	inline static int32_t get_offset_of_U3CU3Ef__amU24cache0_30() { return static_cast<int32_t>(offsetof(AdvEngine_t1176753927_StaticFields, ___U3CU3Ef__amU24cache0_30)); }
	inline Action_1_t1200141114 * get_U3CU3Ef__amU24cache0_30() const { return ___U3CU3Ef__amU24cache0_30; }
	inline Action_1_t1200141114 ** get_address_of_U3CU3Ef__amU24cache0_30() { return &___U3CU3Ef__amU24cache0_30; }
	inline void set_U3CU3Ef__amU24cache0_30(Action_1_t1200141114 * value)
	{
		___U3CU3Ef__amU24cache0_30 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache0_30), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache1_31() { return static_cast<int32_t>(offsetof(AdvEngine_t1176753927_StaticFields, ___U3CU3Ef__amU24cache1_31)); }
	inline Action_1_t1200141114 * get_U3CU3Ef__amU24cache1_31() const { return ___U3CU3Ef__amU24cache1_31; }
	inline Action_1_t1200141114 ** get_address_of_U3CU3Ef__amU24cache1_31() { return &___U3CU3Ef__amU24cache1_31; }
	inline void set_U3CU3Ef__amU24cache1_31(Action_1_t1200141114 * value)
	{
		___U3CU3Ef__amU24cache1_31 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache1_31), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ADVENGINE_T1176753927_H
#ifndef ADVCUSTOMCOMMANDMANAGER_T178514092_H
#define ADVCUSTOMCOMMANDMANAGER_T178514092_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.AdvCustomCommandManager
struct  AdvCustomCommandManager_t178514092  : public MonoBehaviour_t1158329972
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ADVCUSTOMCOMMANDMANAGER_T178514092_H
#ifndef UNITYCHANCONTROLSCRIPTWITHRGIDBODY_T1891187877_H
#define UNITYCHANCONTROLSCRIPTWITHRGIDBODY_T1891187877_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityChan.UnityChanControlScriptWithRgidBody
struct  UnityChanControlScriptWithRgidBody_t1891187877  : public MonoBehaviour_t1158329972
{
public:
	// System.Single UnityChan.UnityChanControlScriptWithRgidBody::animSpeed
	float ___animSpeed_2;
	// System.Single UnityChan.UnityChanControlScriptWithRgidBody::lookSmoother
	float ___lookSmoother_3;
	// System.Boolean UnityChan.UnityChanControlScriptWithRgidBody::useCurves
	bool ___useCurves_4;
	// System.Single UnityChan.UnityChanControlScriptWithRgidBody::useCurvesHeight
	float ___useCurvesHeight_5;
	// System.Single UnityChan.UnityChanControlScriptWithRgidBody::forwardSpeed
	float ___forwardSpeed_6;
	// System.Single UnityChan.UnityChanControlScriptWithRgidBody::backwardSpeed
	float ___backwardSpeed_7;
	// System.Single UnityChan.UnityChanControlScriptWithRgidBody::rotateSpeed
	float ___rotateSpeed_8;
	// System.Single UnityChan.UnityChanControlScriptWithRgidBody::jumpPower
	float ___jumpPower_9;
	// UnityEngine.CapsuleCollider UnityChan.UnityChanControlScriptWithRgidBody::col
	CapsuleCollider_t720607407 * ___col_10;
	// UnityEngine.Rigidbody UnityChan.UnityChanControlScriptWithRgidBody::rb
	Rigidbody_t4233889191 * ___rb_11;
	// UnityEngine.Vector3 UnityChan.UnityChanControlScriptWithRgidBody::velocity
	Vector3_t2243707580  ___velocity_12;
	// System.Single UnityChan.UnityChanControlScriptWithRgidBody::orgColHight
	float ___orgColHight_13;
	// UnityEngine.Vector3 UnityChan.UnityChanControlScriptWithRgidBody::orgVectColCenter
	Vector3_t2243707580  ___orgVectColCenter_14;
	// UnityEngine.Animator UnityChan.UnityChanControlScriptWithRgidBody::anim
	Animator_t69676727 * ___anim_15;
	// UnityEngine.AnimatorStateInfo UnityChan.UnityChanControlScriptWithRgidBody::currentBaseState
	AnimatorStateInfo_t2577870592  ___currentBaseState_16;
	// UnityEngine.GameObject UnityChan.UnityChanControlScriptWithRgidBody::cameraObject
	GameObject_t1756533147 * ___cameraObject_17;

public:
	inline static int32_t get_offset_of_animSpeed_2() { return static_cast<int32_t>(offsetof(UnityChanControlScriptWithRgidBody_t1891187877, ___animSpeed_2)); }
	inline float get_animSpeed_2() const { return ___animSpeed_2; }
	inline float* get_address_of_animSpeed_2() { return &___animSpeed_2; }
	inline void set_animSpeed_2(float value)
	{
		___animSpeed_2 = value;
	}

	inline static int32_t get_offset_of_lookSmoother_3() { return static_cast<int32_t>(offsetof(UnityChanControlScriptWithRgidBody_t1891187877, ___lookSmoother_3)); }
	inline float get_lookSmoother_3() const { return ___lookSmoother_3; }
	inline float* get_address_of_lookSmoother_3() { return &___lookSmoother_3; }
	inline void set_lookSmoother_3(float value)
	{
		___lookSmoother_3 = value;
	}

	inline static int32_t get_offset_of_useCurves_4() { return static_cast<int32_t>(offsetof(UnityChanControlScriptWithRgidBody_t1891187877, ___useCurves_4)); }
	inline bool get_useCurves_4() const { return ___useCurves_4; }
	inline bool* get_address_of_useCurves_4() { return &___useCurves_4; }
	inline void set_useCurves_4(bool value)
	{
		___useCurves_4 = value;
	}

	inline static int32_t get_offset_of_useCurvesHeight_5() { return static_cast<int32_t>(offsetof(UnityChanControlScriptWithRgidBody_t1891187877, ___useCurvesHeight_5)); }
	inline float get_useCurvesHeight_5() const { return ___useCurvesHeight_5; }
	inline float* get_address_of_useCurvesHeight_5() { return &___useCurvesHeight_5; }
	inline void set_useCurvesHeight_5(float value)
	{
		___useCurvesHeight_5 = value;
	}

	inline static int32_t get_offset_of_forwardSpeed_6() { return static_cast<int32_t>(offsetof(UnityChanControlScriptWithRgidBody_t1891187877, ___forwardSpeed_6)); }
	inline float get_forwardSpeed_6() const { return ___forwardSpeed_6; }
	inline float* get_address_of_forwardSpeed_6() { return &___forwardSpeed_6; }
	inline void set_forwardSpeed_6(float value)
	{
		___forwardSpeed_6 = value;
	}

	inline static int32_t get_offset_of_backwardSpeed_7() { return static_cast<int32_t>(offsetof(UnityChanControlScriptWithRgidBody_t1891187877, ___backwardSpeed_7)); }
	inline float get_backwardSpeed_7() const { return ___backwardSpeed_7; }
	inline float* get_address_of_backwardSpeed_7() { return &___backwardSpeed_7; }
	inline void set_backwardSpeed_7(float value)
	{
		___backwardSpeed_7 = value;
	}

	inline static int32_t get_offset_of_rotateSpeed_8() { return static_cast<int32_t>(offsetof(UnityChanControlScriptWithRgidBody_t1891187877, ___rotateSpeed_8)); }
	inline float get_rotateSpeed_8() const { return ___rotateSpeed_8; }
	inline float* get_address_of_rotateSpeed_8() { return &___rotateSpeed_8; }
	inline void set_rotateSpeed_8(float value)
	{
		___rotateSpeed_8 = value;
	}

	inline static int32_t get_offset_of_jumpPower_9() { return static_cast<int32_t>(offsetof(UnityChanControlScriptWithRgidBody_t1891187877, ___jumpPower_9)); }
	inline float get_jumpPower_9() const { return ___jumpPower_9; }
	inline float* get_address_of_jumpPower_9() { return &___jumpPower_9; }
	inline void set_jumpPower_9(float value)
	{
		___jumpPower_9 = value;
	}

	inline static int32_t get_offset_of_col_10() { return static_cast<int32_t>(offsetof(UnityChanControlScriptWithRgidBody_t1891187877, ___col_10)); }
	inline CapsuleCollider_t720607407 * get_col_10() const { return ___col_10; }
	inline CapsuleCollider_t720607407 ** get_address_of_col_10() { return &___col_10; }
	inline void set_col_10(CapsuleCollider_t720607407 * value)
	{
		___col_10 = value;
		Il2CppCodeGenWriteBarrier((&___col_10), value);
	}

	inline static int32_t get_offset_of_rb_11() { return static_cast<int32_t>(offsetof(UnityChanControlScriptWithRgidBody_t1891187877, ___rb_11)); }
	inline Rigidbody_t4233889191 * get_rb_11() const { return ___rb_11; }
	inline Rigidbody_t4233889191 ** get_address_of_rb_11() { return &___rb_11; }
	inline void set_rb_11(Rigidbody_t4233889191 * value)
	{
		___rb_11 = value;
		Il2CppCodeGenWriteBarrier((&___rb_11), value);
	}

	inline static int32_t get_offset_of_velocity_12() { return static_cast<int32_t>(offsetof(UnityChanControlScriptWithRgidBody_t1891187877, ___velocity_12)); }
	inline Vector3_t2243707580  get_velocity_12() const { return ___velocity_12; }
	inline Vector3_t2243707580 * get_address_of_velocity_12() { return &___velocity_12; }
	inline void set_velocity_12(Vector3_t2243707580  value)
	{
		___velocity_12 = value;
	}

	inline static int32_t get_offset_of_orgColHight_13() { return static_cast<int32_t>(offsetof(UnityChanControlScriptWithRgidBody_t1891187877, ___orgColHight_13)); }
	inline float get_orgColHight_13() const { return ___orgColHight_13; }
	inline float* get_address_of_orgColHight_13() { return &___orgColHight_13; }
	inline void set_orgColHight_13(float value)
	{
		___orgColHight_13 = value;
	}

	inline static int32_t get_offset_of_orgVectColCenter_14() { return static_cast<int32_t>(offsetof(UnityChanControlScriptWithRgidBody_t1891187877, ___orgVectColCenter_14)); }
	inline Vector3_t2243707580  get_orgVectColCenter_14() const { return ___orgVectColCenter_14; }
	inline Vector3_t2243707580 * get_address_of_orgVectColCenter_14() { return &___orgVectColCenter_14; }
	inline void set_orgVectColCenter_14(Vector3_t2243707580  value)
	{
		___orgVectColCenter_14 = value;
	}

	inline static int32_t get_offset_of_anim_15() { return static_cast<int32_t>(offsetof(UnityChanControlScriptWithRgidBody_t1891187877, ___anim_15)); }
	inline Animator_t69676727 * get_anim_15() const { return ___anim_15; }
	inline Animator_t69676727 ** get_address_of_anim_15() { return &___anim_15; }
	inline void set_anim_15(Animator_t69676727 * value)
	{
		___anim_15 = value;
		Il2CppCodeGenWriteBarrier((&___anim_15), value);
	}

	inline static int32_t get_offset_of_currentBaseState_16() { return static_cast<int32_t>(offsetof(UnityChanControlScriptWithRgidBody_t1891187877, ___currentBaseState_16)); }
	inline AnimatorStateInfo_t2577870592  get_currentBaseState_16() const { return ___currentBaseState_16; }
	inline AnimatorStateInfo_t2577870592 * get_address_of_currentBaseState_16() { return &___currentBaseState_16; }
	inline void set_currentBaseState_16(AnimatorStateInfo_t2577870592  value)
	{
		___currentBaseState_16 = value;
	}

	inline static int32_t get_offset_of_cameraObject_17() { return static_cast<int32_t>(offsetof(UnityChanControlScriptWithRgidBody_t1891187877, ___cameraObject_17)); }
	inline GameObject_t1756533147 * get_cameraObject_17() const { return ___cameraObject_17; }
	inline GameObject_t1756533147 ** get_address_of_cameraObject_17() { return &___cameraObject_17; }
	inline void set_cameraObject_17(GameObject_t1756533147 * value)
	{
		___cameraObject_17 = value;
		Il2CppCodeGenWriteBarrier((&___cameraObject_17), value);
	}
};

struct UnityChanControlScriptWithRgidBody_t1891187877_StaticFields
{
public:
	// System.Int32 UnityChan.UnityChanControlScriptWithRgidBody::idleState
	int32_t ___idleState_18;
	// System.Int32 UnityChan.UnityChanControlScriptWithRgidBody::locoState
	int32_t ___locoState_19;
	// System.Int32 UnityChan.UnityChanControlScriptWithRgidBody::jumpState
	int32_t ___jumpState_20;
	// System.Int32 UnityChan.UnityChanControlScriptWithRgidBody::restState
	int32_t ___restState_21;

public:
	inline static int32_t get_offset_of_idleState_18() { return static_cast<int32_t>(offsetof(UnityChanControlScriptWithRgidBody_t1891187877_StaticFields, ___idleState_18)); }
	inline int32_t get_idleState_18() const { return ___idleState_18; }
	inline int32_t* get_address_of_idleState_18() { return &___idleState_18; }
	inline void set_idleState_18(int32_t value)
	{
		___idleState_18 = value;
	}

	inline static int32_t get_offset_of_locoState_19() { return static_cast<int32_t>(offsetof(UnityChanControlScriptWithRgidBody_t1891187877_StaticFields, ___locoState_19)); }
	inline int32_t get_locoState_19() const { return ___locoState_19; }
	inline int32_t* get_address_of_locoState_19() { return &___locoState_19; }
	inline void set_locoState_19(int32_t value)
	{
		___locoState_19 = value;
	}

	inline static int32_t get_offset_of_jumpState_20() { return static_cast<int32_t>(offsetof(UnityChanControlScriptWithRgidBody_t1891187877_StaticFields, ___jumpState_20)); }
	inline int32_t get_jumpState_20() const { return ___jumpState_20; }
	inline int32_t* get_address_of_jumpState_20() { return &___jumpState_20; }
	inline void set_jumpState_20(int32_t value)
	{
		___jumpState_20 = value;
	}

	inline static int32_t get_offset_of_restState_21() { return static_cast<int32_t>(offsetof(UnityChanControlScriptWithRgidBody_t1891187877_StaticFields, ___restState_21)); }
	inline int32_t get_restState_21() const { return ___restState_21; }
	inline int32_t* get_address_of_restState_21() { return &___restState_21; }
	inline void set_restState_21(int32_t value)
	{
		___restState_21 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYCHANCONTROLSCRIPTWITHRGIDBODY_T1891187877_H
#ifndef TALKSCRIPT_T915888587_H
#define TALKSCRIPT_T915888587_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TalkScript
struct  TalkScript_t915888587  : public MonoBehaviour_t1158329972
{
public:
	// Utage.AdvEngine TalkScript::engine
	AdvEngine_t1176753927 * ___engine_2;
	// UnityEngine.UI.Text TalkScript::dereValue
	Text_t356221433 * ___dereValue_3;
	// UnityEngine.UI.Text TalkScript::kyoroValue
	Text_t356221433 * ___kyoroValue_4;
	// UnityEngine.UI.Text TalkScript::relaxValue
	Text_t356221433 * ___relaxValue_5;
	// UnityEngine.UI.RawImage TalkScript::dereGauge
	RawImage_t2749640213 * ___dereGauge_6;
	// UnityEngine.UI.RawImage TalkScript::kyoroGauge
	RawImage_t2749640213 * ___kyoroGauge_7;
	// UnityEngine.UI.RawImage TalkScript::relaxGauge
	RawImage_t2749640213 * ___relaxGauge_8;
	// Utage.AdvConfig TalkScript::config
	AdvConfig_t2941837115 * ___config_9;
	// UnityEngine.Color TalkScript::c1
	Color_t2020392075  ___c1_13;
	// UnityEngine.Color TalkScript::c2
	Color_t2020392075  ___c2_14;
	// UnityEngine.Color TalkScript::c3
	Color_t2020392075  ___c3_15;

public:
	inline static int32_t get_offset_of_engine_2() { return static_cast<int32_t>(offsetof(TalkScript_t915888587, ___engine_2)); }
	inline AdvEngine_t1176753927 * get_engine_2() const { return ___engine_2; }
	inline AdvEngine_t1176753927 ** get_address_of_engine_2() { return &___engine_2; }
	inline void set_engine_2(AdvEngine_t1176753927 * value)
	{
		___engine_2 = value;
		Il2CppCodeGenWriteBarrier((&___engine_2), value);
	}

	inline static int32_t get_offset_of_dereValue_3() { return static_cast<int32_t>(offsetof(TalkScript_t915888587, ___dereValue_3)); }
	inline Text_t356221433 * get_dereValue_3() const { return ___dereValue_3; }
	inline Text_t356221433 ** get_address_of_dereValue_3() { return &___dereValue_3; }
	inline void set_dereValue_3(Text_t356221433 * value)
	{
		___dereValue_3 = value;
		Il2CppCodeGenWriteBarrier((&___dereValue_3), value);
	}

	inline static int32_t get_offset_of_kyoroValue_4() { return static_cast<int32_t>(offsetof(TalkScript_t915888587, ___kyoroValue_4)); }
	inline Text_t356221433 * get_kyoroValue_4() const { return ___kyoroValue_4; }
	inline Text_t356221433 ** get_address_of_kyoroValue_4() { return &___kyoroValue_4; }
	inline void set_kyoroValue_4(Text_t356221433 * value)
	{
		___kyoroValue_4 = value;
		Il2CppCodeGenWriteBarrier((&___kyoroValue_4), value);
	}

	inline static int32_t get_offset_of_relaxValue_5() { return static_cast<int32_t>(offsetof(TalkScript_t915888587, ___relaxValue_5)); }
	inline Text_t356221433 * get_relaxValue_5() const { return ___relaxValue_5; }
	inline Text_t356221433 ** get_address_of_relaxValue_5() { return &___relaxValue_5; }
	inline void set_relaxValue_5(Text_t356221433 * value)
	{
		___relaxValue_5 = value;
		Il2CppCodeGenWriteBarrier((&___relaxValue_5), value);
	}

	inline static int32_t get_offset_of_dereGauge_6() { return static_cast<int32_t>(offsetof(TalkScript_t915888587, ___dereGauge_6)); }
	inline RawImage_t2749640213 * get_dereGauge_6() const { return ___dereGauge_6; }
	inline RawImage_t2749640213 ** get_address_of_dereGauge_6() { return &___dereGauge_6; }
	inline void set_dereGauge_6(RawImage_t2749640213 * value)
	{
		___dereGauge_6 = value;
		Il2CppCodeGenWriteBarrier((&___dereGauge_6), value);
	}

	inline static int32_t get_offset_of_kyoroGauge_7() { return static_cast<int32_t>(offsetof(TalkScript_t915888587, ___kyoroGauge_7)); }
	inline RawImage_t2749640213 * get_kyoroGauge_7() const { return ___kyoroGauge_7; }
	inline RawImage_t2749640213 ** get_address_of_kyoroGauge_7() { return &___kyoroGauge_7; }
	inline void set_kyoroGauge_7(RawImage_t2749640213 * value)
	{
		___kyoroGauge_7 = value;
		Il2CppCodeGenWriteBarrier((&___kyoroGauge_7), value);
	}

	inline static int32_t get_offset_of_relaxGauge_8() { return static_cast<int32_t>(offsetof(TalkScript_t915888587, ___relaxGauge_8)); }
	inline RawImage_t2749640213 * get_relaxGauge_8() const { return ___relaxGauge_8; }
	inline RawImage_t2749640213 ** get_address_of_relaxGauge_8() { return &___relaxGauge_8; }
	inline void set_relaxGauge_8(RawImage_t2749640213 * value)
	{
		___relaxGauge_8 = value;
		Il2CppCodeGenWriteBarrier((&___relaxGauge_8), value);
	}

	inline static int32_t get_offset_of_config_9() { return static_cast<int32_t>(offsetof(TalkScript_t915888587, ___config_9)); }
	inline AdvConfig_t2941837115 * get_config_9() const { return ___config_9; }
	inline AdvConfig_t2941837115 ** get_address_of_config_9() { return &___config_9; }
	inline void set_config_9(AdvConfig_t2941837115 * value)
	{
		___config_9 = value;
		Il2CppCodeGenWriteBarrier((&___config_9), value);
	}

	inline static int32_t get_offset_of_c1_13() { return static_cast<int32_t>(offsetof(TalkScript_t915888587, ___c1_13)); }
	inline Color_t2020392075  get_c1_13() const { return ___c1_13; }
	inline Color_t2020392075 * get_address_of_c1_13() { return &___c1_13; }
	inline void set_c1_13(Color_t2020392075  value)
	{
		___c1_13 = value;
	}

	inline static int32_t get_offset_of_c2_14() { return static_cast<int32_t>(offsetof(TalkScript_t915888587, ___c2_14)); }
	inline Color_t2020392075  get_c2_14() const { return ___c2_14; }
	inline Color_t2020392075 * get_address_of_c2_14() { return &___c2_14; }
	inline void set_c2_14(Color_t2020392075  value)
	{
		___c2_14 = value;
	}

	inline static int32_t get_offset_of_c3_15() { return static_cast<int32_t>(offsetof(TalkScript_t915888587, ___c3_15)); }
	inline Color_t2020392075  get_c3_15() const { return ___c3_15; }
	inline Color_t2020392075 * get_address_of_c3_15() { return &___c3_15; }
	inline void set_c3_15(Color_t2020392075  value)
	{
		___c3_15 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TALKSCRIPT_T915888587_H
#ifndef TOSETTINGSCRIPT_T3239318692_H
#define TOSETTINGSCRIPT_T3239318692_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ToSettingScript
struct  ToSettingScript_t3239318692  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.GameObject[] ToSettingScript::disableObjectList
	GameObjectU5BU5D_t3057952154* ___disableObjectList_2;

public:
	inline static int32_t get_offset_of_disableObjectList_2() { return static_cast<int32_t>(offsetof(ToSettingScript_t3239318692, ___disableObjectList_2)); }
	inline GameObjectU5BU5D_t3057952154* get_disableObjectList_2() const { return ___disableObjectList_2; }
	inline GameObjectU5BU5D_t3057952154** get_address_of_disableObjectList_2() { return &___disableObjectList_2; }
	inline void set_disableObjectList_2(GameObjectU5BU5D_t3057952154* value)
	{
		___disableObjectList_2 = value;
		Il2CppCodeGenWriteBarrier((&___disableObjectList_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TOSETTINGSCRIPT_T3239318692_H
#ifndef TRANSITIONSCRIPT_T3831648224_H
#define TRANSITIONSCRIPT_T3831648224_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TransitionScript
struct  TransitionScript_t3831648224  : public MonoBehaviour_t1158329972
{
public:
	// System.String TransitionScript::toSceneName
	String_t* ___toSceneName_2;
	// System.Single TransitionScript::waitTime
	float ___waitTime_3;
	// System.Single TransitionScript::fadeTime
	float ___fadeTime_4;

public:
	inline static int32_t get_offset_of_toSceneName_2() { return static_cast<int32_t>(offsetof(TransitionScript_t3831648224, ___toSceneName_2)); }
	inline String_t* get_toSceneName_2() const { return ___toSceneName_2; }
	inline String_t** get_address_of_toSceneName_2() { return &___toSceneName_2; }
	inline void set_toSceneName_2(String_t* value)
	{
		___toSceneName_2 = value;
		Il2CppCodeGenWriteBarrier((&___toSceneName_2), value);
	}

	inline static int32_t get_offset_of_waitTime_3() { return static_cast<int32_t>(offsetof(TransitionScript_t3831648224, ___waitTime_3)); }
	inline float get_waitTime_3() const { return ___waitTime_3; }
	inline float* get_address_of_waitTime_3() { return &___waitTime_3; }
	inline void set_waitTime_3(float value)
	{
		___waitTime_3 = value;
	}

	inline static int32_t get_offset_of_fadeTime_4() { return static_cast<int32_t>(offsetof(TransitionScript_t3831648224, ___fadeTime_4)); }
	inline float get_fadeTime_4() const { return ___fadeTime_4; }
	inline float* get_address_of_fadeTime_4() { return &___fadeTime_4; }
	inline void set_fadeTime_4(float value)
	{
		___fadeTime_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRANSITIONSCRIPT_T3831648224_H
#ifndef AUTOSTARTGAME_T2128369979_H
#define AUTOSTARTGAME_T2128369979_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// AutoStartGame
struct  AutoStartGame_t2128369979  : public MonoBehaviour_t1158329972
{
public:
	// UtageUguiTitle AutoStartGame::title
	UtageUguiTitle_t1483171752 * ___title_2;
	// System.Single AutoStartGame::timeLimit
	float ___timeLimit_3;
	// System.Single AutoStartGame::time
	float ___time_4;

public:
	inline static int32_t get_offset_of_title_2() { return static_cast<int32_t>(offsetof(AutoStartGame_t2128369979, ___title_2)); }
	inline UtageUguiTitle_t1483171752 * get_title_2() const { return ___title_2; }
	inline UtageUguiTitle_t1483171752 ** get_address_of_title_2() { return &___title_2; }
	inline void set_title_2(UtageUguiTitle_t1483171752 * value)
	{
		___title_2 = value;
		Il2CppCodeGenWriteBarrier((&___title_2), value);
	}

	inline static int32_t get_offset_of_timeLimit_3() { return static_cast<int32_t>(offsetof(AutoStartGame_t2128369979, ___timeLimit_3)); }
	inline float get_timeLimit_3() const { return ___timeLimit_3; }
	inline float* get_address_of_timeLimit_3() { return &___timeLimit_3; }
	inline void set_timeLimit_3(float value)
	{
		___timeLimit_3 = value;
	}

	inline static int32_t get_offset_of_time_4() { return static_cast<int32_t>(offsetof(AutoStartGame_t2128369979, ___time_4)); }
	inline float get_time_4() const { return ___time_4; }
	inline float* get_address_of_time_4() { return &___time_4; }
	inline void set_time_4(float value)
	{
		___time_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AUTOSTARTGAME_T2128369979_H
#ifndef SAMPLECHAPTERTITLE_T998764237_H
#define SAMPLECHAPTERTITLE_T998764237_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SampleChapterTitle
struct  SampleChapterTitle_t998764237  : public MonoBehaviour_t1158329972
{
public:
	// Utage.AdvEngine SampleChapterTitle::engine
	AdvEngine_t1176753927 * ___engine_2;
	// UtageUguiTitle SampleChapterTitle::title
	UtageUguiTitle_t1483171752 * ___title_3;
	// UtageUguiLoadWait SampleChapterTitle::loadWait
	UtageUguiLoadWait_t319861767 * ___loadWait_4;
	// UtageUguiMainGame SampleChapterTitle::mainGame
	UtageUguiMainGame_t4178963699 * ___mainGame_5;
	// System.Collections.Generic.List`1<System.String> SampleChapterTitle::chapterUrlList
	List_1_t1398341365 * ___chapterUrlList_6;
	// System.Collections.Generic.List`1<System.String> SampleChapterTitle::startLabel
	List_1_t1398341365 * ___startLabel_7;

public:
	inline static int32_t get_offset_of_engine_2() { return static_cast<int32_t>(offsetof(SampleChapterTitle_t998764237, ___engine_2)); }
	inline AdvEngine_t1176753927 * get_engine_2() const { return ___engine_2; }
	inline AdvEngine_t1176753927 ** get_address_of_engine_2() { return &___engine_2; }
	inline void set_engine_2(AdvEngine_t1176753927 * value)
	{
		___engine_2 = value;
		Il2CppCodeGenWriteBarrier((&___engine_2), value);
	}

	inline static int32_t get_offset_of_title_3() { return static_cast<int32_t>(offsetof(SampleChapterTitle_t998764237, ___title_3)); }
	inline UtageUguiTitle_t1483171752 * get_title_3() const { return ___title_3; }
	inline UtageUguiTitle_t1483171752 ** get_address_of_title_3() { return &___title_3; }
	inline void set_title_3(UtageUguiTitle_t1483171752 * value)
	{
		___title_3 = value;
		Il2CppCodeGenWriteBarrier((&___title_3), value);
	}

	inline static int32_t get_offset_of_loadWait_4() { return static_cast<int32_t>(offsetof(SampleChapterTitle_t998764237, ___loadWait_4)); }
	inline UtageUguiLoadWait_t319861767 * get_loadWait_4() const { return ___loadWait_4; }
	inline UtageUguiLoadWait_t319861767 ** get_address_of_loadWait_4() { return &___loadWait_4; }
	inline void set_loadWait_4(UtageUguiLoadWait_t319861767 * value)
	{
		___loadWait_4 = value;
		Il2CppCodeGenWriteBarrier((&___loadWait_4), value);
	}

	inline static int32_t get_offset_of_mainGame_5() { return static_cast<int32_t>(offsetof(SampleChapterTitle_t998764237, ___mainGame_5)); }
	inline UtageUguiMainGame_t4178963699 * get_mainGame_5() const { return ___mainGame_5; }
	inline UtageUguiMainGame_t4178963699 ** get_address_of_mainGame_5() { return &___mainGame_5; }
	inline void set_mainGame_5(UtageUguiMainGame_t4178963699 * value)
	{
		___mainGame_5 = value;
		Il2CppCodeGenWriteBarrier((&___mainGame_5), value);
	}

	inline static int32_t get_offset_of_chapterUrlList_6() { return static_cast<int32_t>(offsetof(SampleChapterTitle_t998764237, ___chapterUrlList_6)); }
	inline List_1_t1398341365 * get_chapterUrlList_6() const { return ___chapterUrlList_6; }
	inline List_1_t1398341365 ** get_address_of_chapterUrlList_6() { return &___chapterUrlList_6; }
	inline void set_chapterUrlList_6(List_1_t1398341365 * value)
	{
		___chapterUrlList_6 = value;
		Il2CppCodeGenWriteBarrier((&___chapterUrlList_6), value);
	}

	inline static int32_t get_offset_of_startLabel_7() { return static_cast<int32_t>(offsetof(SampleChapterTitle_t998764237, ___startLabel_7)); }
	inline List_1_t1398341365 * get_startLabel_7() const { return ___startLabel_7; }
	inline List_1_t1398341365 ** get_address_of_startLabel_7() { return &___startLabel_7; }
	inline void set_startLabel_7(List_1_t1398341365 * value)
	{
		___startLabel_7 = value;
		Il2CppCodeGenWriteBarrier((&___startLabel_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SAMPLECHAPTERTITLE_T998764237_H
#ifndef SAMPLECHARACTERGRAYOUTCONTROLLERRECIEVEMESSAGE_T2772336740_H
#define SAMPLECHARACTERGRAYOUTCONTROLLERRECIEVEMESSAGE_T2772336740_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.SampleCharacterGrayOutControllerRecieveMessage
struct  SampleCharacterGrayOutControllerRecieveMessage_t2772336740  : public MonoBehaviour_t1158329972
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SAMPLECHARACTERGRAYOUTCONTROLLERRECIEVEMESSAGE_T2772336740_H
#ifndef SAMPLECHATLOG_T3834130706_H
#define SAMPLECHATLOG_T3834130706_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SampleChatLog
struct  SampleChatLog_t3834130706  : public MonoBehaviour_t1158329972
{
public:
	// Utage.AdvEngine SampleChatLog::engine
	AdvEngine_t1176753927 * ___engine_2;
	// UnityEngine.GameObject SampleChatLog::itemPrefab
	GameObject_t1756533147 * ___itemPrefab_3;
	// UnityEngine.Transform SampleChatLog::targetRoot
	Transform_t3275118058 * ___targetRoot_4;
	// System.Int32 SampleChatLog::maxLog
	int32_t ___maxLog_5;
	// System.Collections.Generic.List`1<UnityEngine.GameObject> SampleChatLog::logs
	List_1_t1125654279 * ___logs_6;

public:
	inline static int32_t get_offset_of_engine_2() { return static_cast<int32_t>(offsetof(SampleChatLog_t3834130706, ___engine_2)); }
	inline AdvEngine_t1176753927 * get_engine_2() const { return ___engine_2; }
	inline AdvEngine_t1176753927 ** get_address_of_engine_2() { return &___engine_2; }
	inline void set_engine_2(AdvEngine_t1176753927 * value)
	{
		___engine_2 = value;
		Il2CppCodeGenWriteBarrier((&___engine_2), value);
	}

	inline static int32_t get_offset_of_itemPrefab_3() { return static_cast<int32_t>(offsetof(SampleChatLog_t3834130706, ___itemPrefab_3)); }
	inline GameObject_t1756533147 * get_itemPrefab_3() const { return ___itemPrefab_3; }
	inline GameObject_t1756533147 ** get_address_of_itemPrefab_3() { return &___itemPrefab_3; }
	inline void set_itemPrefab_3(GameObject_t1756533147 * value)
	{
		___itemPrefab_3 = value;
		Il2CppCodeGenWriteBarrier((&___itemPrefab_3), value);
	}

	inline static int32_t get_offset_of_targetRoot_4() { return static_cast<int32_t>(offsetof(SampleChatLog_t3834130706, ___targetRoot_4)); }
	inline Transform_t3275118058 * get_targetRoot_4() const { return ___targetRoot_4; }
	inline Transform_t3275118058 ** get_address_of_targetRoot_4() { return &___targetRoot_4; }
	inline void set_targetRoot_4(Transform_t3275118058 * value)
	{
		___targetRoot_4 = value;
		Il2CppCodeGenWriteBarrier((&___targetRoot_4), value);
	}

	inline static int32_t get_offset_of_maxLog_5() { return static_cast<int32_t>(offsetof(SampleChatLog_t3834130706, ___maxLog_5)); }
	inline int32_t get_maxLog_5() const { return ___maxLog_5; }
	inline int32_t* get_address_of_maxLog_5() { return &___maxLog_5; }
	inline void set_maxLog_5(int32_t value)
	{
		___maxLog_5 = value;
	}

	inline static int32_t get_offset_of_logs_6() { return static_cast<int32_t>(offsetof(SampleChatLog_t3834130706, ___logs_6)); }
	inline List_1_t1125654279 * get_logs_6() const { return ___logs_6; }
	inline List_1_t1125654279 ** get_address_of_logs_6() { return &___logs_6; }
	inline void set_logs_6(List_1_t1125654279 * value)
	{
		___logs_6 = value;
		Il2CppCodeGenWriteBarrier((&___logs_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SAMPLECHATLOG_T3834130706_H
#ifndef SAMPLECHATLOGITEM_T3744651319_H
#define SAMPLECHATLOGITEM_T3744651319_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SampleChatLogItem
struct  SampleChatLogItem_t3744651319  : public MonoBehaviour_t1158329972
{
public:
	// Utage.UguiNovelText SampleChatLogItem::text
	UguiNovelText_t4135744055 * ___text_2;
	// UnityEngine.UI.Text SampleChatLogItem::characterName
	Text_t356221433 * ___characterName_3;
	// UnityEngine.GameObject SampleChatLogItem::soundIcon
	GameObject_t1756533147 * ___soundIcon_4;
	// UnityEngine.UI.Button SampleChatLogItem::button
	Button_t2872111280 * ___button_5;
	// System.Boolean SampleChatLogItem::isMultiTextInPage
	bool ___isMultiTextInPage_6;
	// Utage.AdvBacklog SampleChatLogItem::data
	AdvBacklog_t3927301290 * ___data_7;

public:
	inline static int32_t get_offset_of_text_2() { return static_cast<int32_t>(offsetof(SampleChatLogItem_t3744651319, ___text_2)); }
	inline UguiNovelText_t4135744055 * get_text_2() const { return ___text_2; }
	inline UguiNovelText_t4135744055 ** get_address_of_text_2() { return &___text_2; }
	inline void set_text_2(UguiNovelText_t4135744055 * value)
	{
		___text_2 = value;
		Il2CppCodeGenWriteBarrier((&___text_2), value);
	}

	inline static int32_t get_offset_of_characterName_3() { return static_cast<int32_t>(offsetof(SampleChatLogItem_t3744651319, ___characterName_3)); }
	inline Text_t356221433 * get_characterName_3() const { return ___characterName_3; }
	inline Text_t356221433 ** get_address_of_characterName_3() { return &___characterName_3; }
	inline void set_characterName_3(Text_t356221433 * value)
	{
		___characterName_3 = value;
		Il2CppCodeGenWriteBarrier((&___characterName_3), value);
	}

	inline static int32_t get_offset_of_soundIcon_4() { return static_cast<int32_t>(offsetof(SampleChatLogItem_t3744651319, ___soundIcon_4)); }
	inline GameObject_t1756533147 * get_soundIcon_4() const { return ___soundIcon_4; }
	inline GameObject_t1756533147 ** get_address_of_soundIcon_4() { return &___soundIcon_4; }
	inline void set_soundIcon_4(GameObject_t1756533147 * value)
	{
		___soundIcon_4 = value;
		Il2CppCodeGenWriteBarrier((&___soundIcon_4), value);
	}

	inline static int32_t get_offset_of_button_5() { return static_cast<int32_t>(offsetof(SampleChatLogItem_t3744651319, ___button_5)); }
	inline Button_t2872111280 * get_button_5() const { return ___button_5; }
	inline Button_t2872111280 ** get_address_of_button_5() { return &___button_5; }
	inline void set_button_5(Button_t2872111280 * value)
	{
		___button_5 = value;
		Il2CppCodeGenWriteBarrier((&___button_5), value);
	}

	inline static int32_t get_offset_of_isMultiTextInPage_6() { return static_cast<int32_t>(offsetof(SampleChatLogItem_t3744651319, ___isMultiTextInPage_6)); }
	inline bool get_isMultiTextInPage_6() const { return ___isMultiTextInPage_6; }
	inline bool* get_address_of_isMultiTextInPage_6() { return &___isMultiTextInPage_6; }
	inline void set_isMultiTextInPage_6(bool value)
	{
		___isMultiTextInPage_6 = value;
	}

	inline static int32_t get_offset_of_data_7() { return static_cast<int32_t>(offsetof(SampleChatLogItem_t3744651319, ___data_7)); }
	inline AdvBacklog_t3927301290 * get_data_7() const { return ___data_7; }
	inline AdvBacklog_t3927301290 ** get_address_of_data_7() { return &___data_7; }
	inline void set_data_7(AdvBacklog_t3927301290 * value)
	{
		___data_7 = value;
		Il2CppCodeGenWriteBarrier((&___data_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SAMPLECHATLOGITEM_T3744651319_H
#ifndef SAMPLECHECKUNITY56NEWER_T3908425173_H
#define SAMPLECHECKUNITY56NEWER_T3908425173_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SampleCheckUnity56Newer
struct  SampleCheckUnity56Newer_t3908425173  : public MonoBehaviour_t1158329972
{
public:
	// Utage.AdvEngine SampleCheckUnity56Newer::engine
	AdvEngine_t1176753927 * ___engine_2;
	// System.Boolean SampleCheckUnity56Newer::<IsInit>k__BackingField
	bool ___U3CIsInitU3Ek__BackingField_3;

public:
	inline static int32_t get_offset_of_engine_2() { return static_cast<int32_t>(offsetof(SampleCheckUnity56Newer_t3908425173, ___engine_2)); }
	inline AdvEngine_t1176753927 * get_engine_2() const { return ___engine_2; }
	inline AdvEngine_t1176753927 ** get_address_of_engine_2() { return &___engine_2; }
	inline void set_engine_2(AdvEngine_t1176753927 * value)
	{
		___engine_2 = value;
		Il2CppCodeGenWriteBarrier((&___engine_2), value);
	}

	inline static int32_t get_offset_of_U3CIsInitU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(SampleCheckUnity56Newer_t3908425173, ___U3CIsInitU3Ek__BackingField_3)); }
	inline bool get_U3CIsInitU3Ek__BackingField_3() const { return ___U3CIsInitU3Ek__BackingField_3; }
	inline bool* get_address_of_U3CIsInitU3Ek__BackingField_3() { return &___U3CIsInitU3Ek__BackingField_3; }
	inline void set_U3CIsInitU3Ek__BackingField_3(bool value)
	{
		___U3CIsInitU3Ek__BackingField_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SAMPLECHECKUNITY56NEWER_T3908425173_H
#ifndef SETTINGSOBJECTSCRIPT_T2863710617_H
#define SETTINGSOBJECTSCRIPT_T2863710617_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SettingsObjectScript
struct  SettingsObjectScript_t2863710617  : public MonoBehaviour_t1158329972
{
public:
	// System.Boolean SettingsObjectScript::peripheralConnected
	bool ___peripheralConnected_3;
	// UnityEngine.Transform SettingsObjectScript::scrollContent
	Transform_t3275118058 * ___scrollContent_4;
	// UnityEngine.Transform SettingsObjectScript::scrollCell
	Transform_t3275118058 * ___scrollCell_5;
	// UnityEngine.UI.Text SettingsObjectScript::scanButtonLabel
	Text_t356221433 * ___scanButtonLabel_6;
	// UnityEngine.UI.Text SettingsObjectScript::scanStatusLabel
	Text_t356221433 * ___scanStatusLabel_7;
	// UnityEngine.UI.Text SettingsObjectScript::logLabel
	Text_t356221433 * ___logLabel_8;
	// UnityEngine.UI.Slider SettingsObjectScript::bgmVolumeSlider
	Slider_t297367283 * ___bgmVolumeSlider_9;
	// UnityEngine.UI.Text SettingsObjectScript::bgVolumeLabel
	Text_t356221433 * ___bgVolumeLabel_10;
	// System.Boolean SettingsObjectScript::isScanning
	bool ___isScanning_11;

public:
	inline static int32_t get_offset_of_peripheralConnected_3() { return static_cast<int32_t>(offsetof(SettingsObjectScript_t2863710617, ___peripheralConnected_3)); }
	inline bool get_peripheralConnected_3() const { return ___peripheralConnected_3; }
	inline bool* get_address_of_peripheralConnected_3() { return &___peripheralConnected_3; }
	inline void set_peripheralConnected_3(bool value)
	{
		___peripheralConnected_3 = value;
	}

	inline static int32_t get_offset_of_scrollContent_4() { return static_cast<int32_t>(offsetof(SettingsObjectScript_t2863710617, ___scrollContent_4)); }
	inline Transform_t3275118058 * get_scrollContent_4() const { return ___scrollContent_4; }
	inline Transform_t3275118058 ** get_address_of_scrollContent_4() { return &___scrollContent_4; }
	inline void set_scrollContent_4(Transform_t3275118058 * value)
	{
		___scrollContent_4 = value;
		Il2CppCodeGenWriteBarrier((&___scrollContent_4), value);
	}

	inline static int32_t get_offset_of_scrollCell_5() { return static_cast<int32_t>(offsetof(SettingsObjectScript_t2863710617, ___scrollCell_5)); }
	inline Transform_t3275118058 * get_scrollCell_5() const { return ___scrollCell_5; }
	inline Transform_t3275118058 ** get_address_of_scrollCell_5() { return &___scrollCell_5; }
	inline void set_scrollCell_5(Transform_t3275118058 * value)
	{
		___scrollCell_5 = value;
		Il2CppCodeGenWriteBarrier((&___scrollCell_5), value);
	}

	inline static int32_t get_offset_of_scanButtonLabel_6() { return static_cast<int32_t>(offsetof(SettingsObjectScript_t2863710617, ___scanButtonLabel_6)); }
	inline Text_t356221433 * get_scanButtonLabel_6() const { return ___scanButtonLabel_6; }
	inline Text_t356221433 ** get_address_of_scanButtonLabel_6() { return &___scanButtonLabel_6; }
	inline void set_scanButtonLabel_6(Text_t356221433 * value)
	{
		___scanButtonLabel_6 = value;
		Il2CppCodeGenWriteBarrier((&___scanButtonLabel_6), value);
	}

	inline static int32_t get_offset_of_scanStatusLabel_7() { return static_cast<int32_t>(offsetof(SettingsObjectScript_t2863710617, ___scanStatusLabel_7)); }
	inline Text_t356221433 * get_scanStatusLabel_7() const { return ___scanStatusLabel_7; }
	inline Text_t356221433 ** get_address_of_scanStatusLabel_7() { return &___scanStatusLabel_7; }
	inline void set_scanStatusLabel_7(Text_t356221433 * value)
	{
		___scanStatusLabel_7 = value;
		Il2CppCodeGenWriteBarrier((&___scanStatusLabel_7), value);
	}

	inline static int32_t get_offset_of_logLabel_8() { return static_cast<int32_t>(offsetof(SettingsObjectScript_t2863710617, ___logLabel_8)); }
	inline Text_t356221433 * get_logLabel_8() const { return ___logLabel_8; }
	inline Text_t356221433 ** get_address_of_logLabel_8() { return &___logLabel_8; }
	inline void set_logLabel_8(Text_t356221433 * value)
	{
		___logLabel_8 = value;
		Il2CppCodeGenWriteBarrier((&___logLabel_8), value);
	}

	inline static int32_t get_offset_of_bgmVolumeSlider_9() { return static_cast<int32_t>(offsetof(SettingsObjectScript_t2863710617, ___bgmVolumeSlider_9)); }
	inline Slider_t297367283 * get_bgmVolumeSlider_9() const { return ___bgmVolumeSlider_9; }
	inline Slider_t297367283 ** get_address_of_bgmVolumeSlider_9() { return &___bgmVolumeSlider_9; }
	inline void set_bgmVolumeSlider_9(Slider_t297367283 * value)
	{
		___bgmVolumeSlider_9 = value;
		Il2CppCodeGenWriteBarrier((&___bgmVolumeSlider_9), value);
	}

	inline static int32_t get_offset_of_bgVolumeLabel_10() { return static_cast<int32_t>(offsetof(SettingsObjectScript_t2863710617, ___bgVolumeLabel_10)); }
	inline Text_t356221433 * get_bgVolumeLabel_10() const { return ___bgVolumeLabel_10; }
	inline Text_t356221433 ** get_address_of_bgVolumeLabel_10() { return &___bgVolumeLabel_10; }
	inline void set_bgVolumeLabel_10(Text_t356221433 * value)
	{
		___bgVolumeLabel_10 = value;
		Il2CppCodeGenWriteBarrier((&___bgVolumeLabel_10), value);
	}

	inline static int32_t get_offset_of_isScanning_11() { return static_cast<int32_t>(offsetof(SettingsObjectScript_t2863710617, ___isScanning_11)); }
	inline bool get_isScanning_11() const { return ___isScanning_11; }
	inline bool* get_address_of_isScanning_11() { return &___isScanning_11; }
	inline void set_isScanning_11(bool value)
	{
		___isScanning_11 = value;
	}
};

struct SettingsObjectScript_t2863710617_StaticFields
{
public:
	// ToSettingScript SettingsObjectScript::callerObject
	ToSettingScript_t3239318692 * ___callerObject_2;

public:
	inline static int32_t get_offset_of_callerObject_2() { return static_cast<int32_t>(offsetof(SettingsObjectScript_t2863710617_StaticFields, ___callerObject_2)); }
	inline ToSettingScript_t3239318692 * get_callerObject_2() const { return ___callerObject_2; }
	inline ToSettingScript_t3239318692 ** get_address_of_callerObject_2() { return &___callerObject_2; }
	inline void set_callerObject_2(ToSettingScript_t3239318692 * value)
	{
		___callerObject_2 = value;
		Il2CppCodeGenWriteBarrier((&___callerObject_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SETTINGSOBJECTSCRIPT_T2863710617_H
#ifndef FPSCOUNTER_T2606976575_H
#define FPSCOUNTER_T2606976575_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Live2D.Cubism.Samples.AsyncBenchmark.FpsCounter
struct  FpsCounter_t2606976575  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.UI.Text Live2D.Cubism.Samples.AsyncBenchmark.FpsCounter::FpsUi
	Text_t356221433 * ___FpsUi_2;
	// System.Single Live2D.Cubism.Samples.AsyncBenchmark.FpsCounter::<DeltaTime>k__BackingField
	float ___U3CDeltaTimeU3Ek__BackingField_3;

public:
	inline static int32_t get_offset_of_FpsUi_2() { return static_cast<int32_t>(offsetof(FpsCounter_t2606976575, ___FpsUi_2)); }
	inline Text_t356221433 * get_FpsUi_2() const { return ___FpsUi_2; }
	inline Text_t356221433 ** get_address_of_FpsUi_2() { return &___FpsUi_2; }
	inline void set_FpsUi_2(Text_t356221433 * value)
	{
		___FpsUi_2 = value;
		Il2CppCodeGenWriteBarrier((&___FpsUi_2), value);
	}

	inline static int32_t get_offset_of_U3CDeltaTimeU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(FpsCounter_t2606976575, ___U3CDeltaTimeU3Ek__BackingField_3)); }
	inline float get_U3CDeltaTimeU3Ek__BackingField_3() const { return ___U3CDeltaTimeU3Ek__BackingField_3; }
	inline float* get_address_of_U3CDeltaTimeU3Ek__BackingField_3() { return &___U3CDeltaTimeU3Ek__BackingField_3; }
	inline void set_U3CDeltaTimeU3Ek__BackingField_3(float value)
	{
		___U3CDeltaTimeU3Ek__BackingField_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FPSCOUNTER_T2606976575_H
#ifndef MODELSPAWNER_T2910617673_H
#define MODELSPAWNER_T2910617673_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Live2D.Cubism.Samples.AsyncBenchmark.ModelSpawner
struct  ModelSpawner_t2910617673  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.GameObject Live2D.Cubism.Samples.AsyncBenchmark.ModelSpawner::ModelPrefab
	GameObject_t1756533147 * ___ModelPrefab_2;
	// UnityEngine.UI.Text Live2D.Cubism.Samples.AsyncBenchmark.ModelSpawner::ModelCountUi
	Text_t356221433 * ___ModelCountUi_3;
	// System.Collections.Generic.List`1<UnityEngine.GameObject> Live2D.Cubism.Samples.AsyncBenchmark.ModelSpawner::<Instances>k__BackingField
	List_1_t1125654279 * ___U3CInstancesU3Ek__BackingField_4;

public:
	inline static int32_t get_offset_of_ModelPrefab_2() { return static_cast<int32_t>(offsetof(ModelSpawner_t2910617673, ___ModelPrefab_2)); }
	inline GameObject_t1756533147 * get_ModelPrefab_2() const { return ___ModelPrefab_2; }
	inline GameObject_t1756533147 ** get_address_of_ModelPrefab_2() { return &___ModelPrefab_2; }
	inline void set_ModelPrefab_2(GameObject_t1756533147 * value)
	{
		___ModelPrefab_2 = value;
		Il2CppCodeGenWriteBarrier((&___ModelPrefab_2), value);
	}

	inline static int32_t get_offset_of_ModelCountUi_3() { return static_cast<int32_t>(offsetof(ModelSpawner_t2910617673, ___ModelCountUi_3)); }
	inline Text_t356221433 * get_ModelCountUi_3() const { return ___ModelCountUi_3; }
	inline Text_t356221433 ** get_address_of_ModelCountUi_3() { return &___ModelCountUi_3; }
	inline void set_ModelCountUi_3(Text_t356221433 * value)
	{
		___ModelCountUi_3 = value;
		Il2CppCodeGenWriteBarrier((&___ModelCountUi_3), value);
	}

	inline static int32_t get_offset_of_U3CInstancesU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(ModelSpawner_t2910617673, ___U3CInstancesU3Ek__BackingField_4)); }
	inline List_1_t1125654279 * get_U3CInstancesU3Ek__BackingField_4() const { return ___U3CInstancesU3Ek__BackingField_4; }
	inline List_1_t1125654279 ** get_address_of_U3CInstancesU3Ek__BackingField_4() { return &___U3CInstancesU3Ek__BackingField_4; }
	inline void set_U3CInstancesU3Ek__BackingField_4(List_1_t1125654279 * value)
	{
		___U3CInstancesU3Ek__BackingField_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CInstancesU3Ek__BackingField_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MODELSPAWNER_T2910617673_H
#ifndef BILLBOARDER_T1346851594_H
#define BILLBOARDER_T1346851594_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Live2D.Cubism.Samples.LookAt.Billboarder
struct  Billboarder_t1346851594  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.Camera Live2D.Cubism.Samples.LookAt.Billboarder::CameraToFace
	Camera_t189460977 * ___CameraToFace_2;

public:
	inline static int32_t get_offset_of_CameraToFace_2() { return static_cast<int32_t>(offsetof(Billboarder_t1346851594, ___CameraToFace_2)); }
	inline Camera_t189460977 * get_CameraToFace_2() const { return ___CameraToFace_2; }
	inline Camera_t189460977 ** get_address_of_CameraToFace_2() { return &___CameraToFace_2; }
	inline void set_CameraToFace_2(Camera_t189460977 * value)
	{
		___CameraToFace_2 = value;
		Il2CppCodeGenWriteBarrier((&___CameraToFace_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BILLBOARDER_T1346851594_H
#ifndef MASKTEXTUREPREVIEW_T295566249_H
#define MASKTEXTUREPREVIEW_T295566249_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Live2D.Cubism.Samples.Masking.MaskTexturePreview
struct  MaskTexturePreview_t295566249  : public MonoBehaviour_t1158329972
{
public:
	// Live2D.Cubism.Rendering.Masking.CubismMaskTexture Live2D.Cubism.Samples.Masking.MaskTexturePreview::MaskTexture
	CubismMaskTexture_t949515734 * ___MaskTexture_2;

public:
	inline static int32_t get_offset_of_MaskTexture_2() { return static_cast<int32_t>(offsetof(MaskTexturePreview_t295566249, ___MaskTexture_2)); }
	inline CubismMaskTexture_t949515734 * get_MaskTexture_2() const { return ___MaskTexture_2; }
	inline CubismMaskTexture_t949515734 ** get_address_of_MaskTexture_2() { return &___MaskTexture_2; }
	inline void set_MaskTexture_2(CubismMaskTexture_t949515734 * value)
	{
		___MaskTexture_2 = value;
		Il2CppCodeGenWriteBarrier((&___MaskTexture_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MASKTEXTUREPREVIEW_T295566249_H
#ifndef RAYCASTHITDISPLAY_T1519583226_H
#define RAYCASTHITDISPLAY_T1519583226_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Live2D.Cubism.Samples.Raycasting.RaycastHitDisplay
struct  RaycastHitDisplay_t1519583226  : public MonoBehaviour_t1158329972
{
public:
	// Live2D.Cubism.Core.CubismModel Live2D.Cubism.Samples.Raycasting.RaycastHitDisplay::Model
	CubismModel_t2381008000 * ___Model_2;
	// UnityEngine.UI.Text Live2D.Cubism.Samples.Raycasting.RaycastHitDisplay::ResultsText
	Text_t356221433 * ___ResultsText_3;
	// Live2D.Cubism.Framework.Raycasting.CubismRaycaster Live2D.Cubism.Samples.Raycasting.RaycastHitDisplay::<Raycaster>k__BackingField
	CubismRaycaster_t3507460997 * ___U3CRaycasterU3Ek__BackingField_4;
	// Live2D.Cubism.Framework.Raycasting.CubismRaycastHit[] Live2D.Cubism.Samples.Raycasting.RaycastHitDisplay::<Results>k__BackingField
	CubismRaycastHitU5BU5D_t1762906786* ___U3CResultsU3Ek__BackingField_5;

public:
	inline static int32_t get_offset_of_Model_2() { return static_cast<int32_t>(offsetof(RaycastHitDisplay_t1519583226, ___Model_2)); }
	inline CubismModel_t2381008000 * get_Model_2() const { return ___Model_2; }
	inline CubismModel_t2381008000 ** get_address_of_Model_2() { return &___Model_2; }
	inline void set_Model_2(CubismModel_t2381008000 * value)
	{
		___Model_2 = value;
		Il2CppCodeGenWriteBarrier((&___Model_2), value);
	}

	inline static int32_t get_offset_of_ResultsText_3() { return static_cast<int32_t>(offsetof(RaycastHitDisplay_t1519583226, ___ResultsText_3)); }
	inline Text_t356221433 * get_ResultsText_3() const { return ___ResultsText_3; }
	inline Text_t356221433 ** get_address_of_ResultsText_3() { return &___ResultsText_3; }
	inline void set_ResultsText_3(Text_t356221433 * value)
	{
		___ResultsText_3 = value;
		Il2CppCodeGenWriteBarrier((&___ResultsText_3), value);
	}

	inline static int32_t get_offset_of_U3CRaycasterU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(RaycastHitDisplay_t1519583226, ___U3CRaycasterU3Ek__BackingField_4)); }
	inline CubismRaycaster_t3507460997 * get_U3CRaycasterU3Ek__BackingField_4() const { return ___U3CRaycasterU3Ek__BackingField_4; }
	inline CubismRaycaster_t3507460997 ** get_address_of_U3CRaycasterU3Ek__BackingField_4() { return &___U3CRaycasterU3Ek__BackingField_4; }
	inline void set_U3CRaycasterU3Ek__BackingField_4(CubismRaycaster_t3507460997 * value)
	{
		___U3CRaycasterU3Ek__BackingField_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CRaycasterU3Ek__BackingField_4), value);
	}

	inline static int32_t get_offset_of_U3CResultsU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(RaycastHitDisplay_t1519583226, ___U3CResultsU3Ek__BackingField_5)); }
	inline CubismRaycastHitU5BU5D_t1762906786* get_U3CResultsU3Ek__BackingField_5() const { return ___U3CResultsU3Ek__BackingField_5; }
	inline CubismRaycastHitU5BU5D_t1762906786** get_address_of_U3CResultsU3Ek__BackingField_5() { return &___U3CResultsU3Ek__BackingField_5; }
	inline void set_U3CResultsU3Ek__BackingField_5(CubismRaycastHitU5BU5D_t1762906786* value)
	{
		___U3CResultsU3Ek__BackingField_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3CResultsU3Ek__BackingField_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RAYCASTHITDISPLAY_T1519583226_H
#ifndef FADESAMPLE_T2284280808_H
#define FADESAMPLE_T2284280808_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// FadeSample
struct  FadeSample_t2284280808  : public MonoBehaviour_t1158329972
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FADESAMPLE_T2284280808_H
#ifndef FADEMANAGER_T2205509615_H
#define FADEMANAGER_T2205509615_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// FadeManager
struct  FadeManager_t2205509615  : public MonoBehaviour_t1158329972
{
public:
	// System.Boolean FadeManager::DebugMode
	bool ___DebugMode_3;
	// System.Single FadeManager::fadeAlpha
	float ___fadeAlpha_4;
	// System.Boolean FadeManager::isFading
	bool ___isFading_5;
	// UnityEngine.Color FadeManager::fadeColor
	Color_t2020392075  ___fadeColor_6;

public:
	inline static int32_t get_offset_of_DebugMode_3() { return static_cast<int32_t>(offsetof(FadeManager_t2205509615, ___DebugMode_3)); }
	inline bool get_DebugMode_3() const { return ___DebugMode_3; }
	inline bool* get_address_of_DebugMode_3() { return &___DebugMode_3; }
	inline void set_DebugMode_3(bool value)
	{
		___DebugMode_3 = value;
	}

	inline static int32_t get_offset_of_fadeAlpha_4() { return static_cast<int32_t>(offsetof(FadeManager_t2205509615, ___fadeAlpha_4)); }
	inline float get_fadeAlpha_4() const { return ___fadeAlpha_4; }
	inline float* get_address_of_fadeAlpha_4() { return &___fadeAlpha_4; }
	inline void set_fadeAlpha_4(float value)
	{
		___fadeAlpha_4 = value;
	}

	inline static int32_t get_offset_of_isFading_5() { return static_cast<int32_t>(offsetof(FadeManager_t2205509615, ___isFading_5)); }
	inline bool get_isFading_5() const { return ___isFading_5; }
	inline bool* get_address_of_isFading_5() { return &___isFading_5; }
	inline void set_isFading_5(bool value)
	{
		___isFading_5 = value;
	}

	inline static int32_t get_offset_of_fadeColor_6() { return static_cast<int32_t>(offsetof(FadeManager_t2205509615, ___fadeColor_6)); }
	inline Color_t2020392075  get_fadeColor_6() const { return ___fadeColor_6; }
	inline Color_t2020392075 * get_address_of_fadeColor_6() { return &___fadeColor_6; }
	inline void set_fadeColor_6(Color_t2020392075  value)
	{
		___fadeColor_6 = value;
	}
};

struct FadeManager_t2205509615_StaticFields
{
public:
	// FadeManager FadeManager::instance
	FadeManager_t2205509615 * ___instance_2;

public:
	inline static int32_t get_offset_of_instance_2() { return static_cast<int32_t>(offsetof(FadeManager_t2205509615_StaticFields, ___instance_2)); }
	inline FadeManager_t2205509615 * get_instance_2() const { return ___instance_2; }
	inline FadeManager_t2205509615 ** get_address_of_instance_2() { return &___instance_2; }
	inline void set_instance_2(FadeManager_t2205509615 * value)
	{
		___instance_2 = value;
		Il2CppCodeGenWriteBarrier((&___instance_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FADEMANAGER_T2205509615_H
#ifndef JINSBRIDGESCRIPT_T4122526304_H
#define JINSBRIDGESCRIPT_T4122526304_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// JinsBridgeScript
struct  JinsBridgeScript_t4122526304  : public MonoBehaviour_t1158329972
{
public:
	// System.Double JinsBridgeScript::<dere>k__BackingField
	double ___U3CdereU3Ek__BackingField_4;
	// System.Double JinsBridgeScript::<kyoro>k__BackingField
	double ___U3CkyoroU3Ek__BackingField_5;
	// System.Double JinsBridgeScript::<relax>k__BackingField
	double ___U3CrelaxU3Ek__BackingField_6;
	// System.Boolean JinsBridgeScript::<peripheralConnected>k__BackingField
	bool ___U3CperipheralConnectedU3Ek__BackingField_7;
	// System.String JinsBridgeScript::sceneName
	String_t* ___sceneName_8;
	// System.Single JinsBridgeScript::bgmVolume
	float ___bgmVolume_9;

public:
	inline static int32_t get_offset_of_U3CdereU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(JinsBridgeScript_t4122526304, ___U3CdereU3Ek__BackingField_4)); }
	inline double get_U3CdereU3Ek__BackingField_4() const { return ___U3CdereU3Ek__BackingField_4; }
	inline double* get_address_of_U3CdereU3Ek__BackingField_4() { return &___U3CdereU3Ek__BackingField_4; }
	inline void set_U3CdereU3Ek__BackingField_4(double value)
	{
		___U3CdereU3Ek__BackingField_4 = value;
	}

	inline static int32_t get_offset_of_U3CkyoroU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(JinsBridgeScript_t4122526304, ___U3CkyoroU3Ek__BackingField_5)); }
	inline double get_U3CkyoroU3Ek__BackingField_5() const { return ___U3CkyoroU3Ek__BackingField_5; }
	inline double* get_address_of_U3CkyoroU3Ek__BackingField_5() { return &___U3CkyoroU3Ek__BackingField_5; }
	inline void set_U3CkyoroU3Ek__BackingField_5(double value)
	{
		___U3CkyoroU3Ek__BackingField_5 = value;
	}

	inline static int32_t get_offset_of_U3CrelaxU3Ek__BackingField_6() { return static_cast<int32_t>(offsetof(JinsBridgeScript_t4122526304, ___U3CrelaxU3Ek__BackingField_6)); }
	inline double get_U3CrelaxU3Ek__BackingField_6() const { return ___U3CrelaxU3Ek__BackingField_6; }
	inline double* get_address_of_U3CrelaxU3Ek__BackingField_6() { return &___U3CrelaxU3Ek__BackingField_6; }
	inline void set_U3CrelaxU3Ek__BackingField_6(double value)
	{
		___U3CrelaxU3Ek__BackingField_6 = value;
	}

	inline static int32_t get_offset_of_U3CperipheralConnectedU3Ek__BackingField_7() { return static_cast<int32_t>(offsetof(JinsBridgeScript_t4122526304, ___U3CperipheralConnectedU3Ek__BackingField_7)); }
	inline bool get_U3CperipheralConnectedU3Ek__BackingField_7() const { return ___U3CperipheralConnectedU3Ek__BackingField_7; }
	inline bool* get_address_of_U3CperipheralConnectedU3Ek__BackingField_7() { return &___U3CperipheralConnectedU3Ek__BackingField_7; }
	inline void set_U3CperipheralConnectedU3Ek__BackingField_7(bool value)
	{
		___U3CperipheralConnectedU3Ek__BackingField_7 = value;
	}

	inline static int32_t get_offset_of_sceneName_8() { return static_cast<int32_t>(offsetof(JinsBridgeScript_t4122526304, ___sceneName_8)); }
	inline String_t* get_sceneName_8() const { return ___sceneName_8; }
	inline String_t** get_address_of_sceneName_8() { return &___sceneName_8; }
	inline void set_sceneName_8(String_t* value)
	{
		___sceneName_8 = value;
		Il2CppCodeGenWriteBarrier((&___sceneName_8), value);
	}

	inline static int32_t get_offset_of_bgmVolume_9() { return static_cast<int32_t>(offsetof(JinsBridgeScript_t4122526304, ___bgmVolume_9)); }
	inline float get_bgmVolume_9() const { return ___bgmVolume_9; }
	inline float* get_address_of_bgmVolume_9() { return &___bgmVolume_9; }
	inline void set_bgmVolume_9(float value)
	{
		___bgmVolume_9 = value;
	}
};

struct JinsBridgeScript_t4122526304_StaticFields
{
public:
	// JinsBridgeScript JinsBridgeScript::<Instance>k__BackingField
	JinsBridgeScript_t4122526304 * ___U3CInstanceU3Ek__BackingField_3;

public:
	inline static int32_t get_offset_of_U3CInstanceU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(JinsBridgeScript_t4122526304_StaticFields, ___U3CInstanceU3Ek__BackingField_3)); }
	inline JinsBridgeScript_t4122526304 * get_U3CInstanceU3Ek__BackingField_3() const { return ___U3CInstanceU3Ek__BackingField_3; }
	inline JinsBridgeScript_t4122526304 ** get_address_of_U3CInstanceU3Ek__BackingField_3() { return &___U3CInstanceU3Ek__BackingField_3; }
	inline void set_U3CInstanceU3Ek__BackingField_3(JinsBridgeScript_t4122526304 * value)
	{
		___U3CInstanceU3Ek__BackingField_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CInstanceU3Ek__BackingField_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // JINSBRIDGESCRIPT_T4122526304_H
#ifndef SELECTSCRIPT_T398995261_H
#define SELECTSCRIPT_T398995261_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SelectScript
struct  SelectScript_t398995261  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.UI.Button SelectScript::startButton
	Button_t2872111280 * ___startButton_2;
	// UnityEngine.UI.Button SelectScript::backButton
	Button_t2872111280 * ___backButton_3;
	// UnityEngine.GameObject SelectScript::slider
	GameObject_t1756533147 * ___slider_4;
	// UnityEngine.GameObject SelectScript::girl
	GameObject_t1756533147 * ___girl_5;
	// UnityEngine.GameObject SelectScript::boy
	GameObject_t1756533147 * ___boy_6;
	// UnityEngine.AudioClip SelectScript::girlSelectSe
	AudioClip_t1932558630 * ___girlSelectSe_7;
	// UnityEngine.AudioClip SelectScript::girlStartSe
	AudioClip_t1932558630 * ___girlStartSe_8;
	// UnityEngine.AudioClip SelectScript::boySelectSe
	AudioClip_t1932558630 * ___boySelectSe_9;
	// UnityEngine.AudioClip SelectScript::boyStartSe
	AudioClip_t1932558630 * ___boyStartSe_10;
	// UnityEngine.AudioSource SelectScript::girlAudio
	AudioSource_t1135106623 * ___girlAudio_11;
	// UnityEngine.AudioSource SelectScript::boyAudio
	AudioSource_t1135106623 * ___boyAudio_12;
	// ToSettingScript SelectScript::toSettings
	ToSettingScript_t3239318692 * ___toSettings_13;
	// SelectScript/Type SelectScript::chara
	int32_t ___chara_14;
	// System.Boolean SelectScript::touchEnabled
	bool ___touchEnabled_15;
	// UnityEngine.Vector3 SelectScript::girlPos
	Vector3_t2243707580  ___girlPos_16;
	// UnityEngine.Vector3 SelectScript::boyPos
	Vector3_t2243707580  ___boyPos_17;

public:
	inline static int32_t get_offset_of_startButton_2() { return static_cast<int32_t>(offsetof(SelectScript_t398995261, ___startButton_2)); }
	inline Button_t2872111280 * get_startButton_2() const { return ___startButton_2; }
	inline Button_t2872111280 ** get_address_of_startButton_2() { return &___startButton_2; }
	inline void set_startButton_2(Button_t2872111280 * value)
	{
		___startButton_2 = value;
		Il2CppCodeGenWriteBarrier((&___startButton_2), value);
	}

	inline static int32_t get_offset_of_backButton_3() { return static_cast<int32_t>(offsetof(SelectScript_t398995261, ___backButton_3)); }
	inline Button_t2872111280 * get_backButton_3() const { return ___backButton_3; }
	inline Button_t2872111280 ** get_address_of_backButton_3() { return &___backButton_3; }
	inline void set_backButton_3(Button_t2872111280 * value)
	{
		___backButton_3 = value;
		Il2CppCodeGenWriteBarrier((&___backButton_3), value);
	}

	inline static int32_t get_offset_of_slider_4() { return static_cast<int32_t>(offsetof(SelectScript_t398995261, ___slider_4)); }
	inline GameObject_t1756533147 * get_slider_4() const { return ___slider_4; }
	inline GameObject_t1756533147 ** get_address_of_slider_4() { return &___slider_4; }
	inline void set_slider_4(GameObject_t1756533147 * value)
	{
		___slider_4 = value;
		Il2CppCodeGenWriteBarrier((&___slider_4), value);
	}

	inline static int32_t get_offset_of_girl_5() { return static_cast<int32_t>(offsetof(SelectScript_t398995261, ___girl_5)); }
	inline GameObject_t1756533147 * get_girl_5() const { return ___girl_5; }
	inline GameObject_t1756533147 ** get_address_of_girl_5() { return &___girl_5; }
	inline void set_girl_5(GameObject_t1756533147 * value)
	{
		___girl_5 = value;
		Il2CppCodeGenWriteBarrier((&___girl_5), value);
	}

	inline static int32_t get_offset_of_boy_6() { return static_cast<int32_t>(offsetof(SelectScript_t398995261, ___boy_6)); }
	inline GameObject_t1756533147 * get_boy_6() const { return ___boy_6; }
	inline GameObject_t1756533147 ** get_address_of_boy_6() { return &___boy_6; }
	inline void set_boy_6(GameObject_t1756533147 * value)
	{
		___boy_6 = value;
		Il2CppCodeGenWriteBarrier((&___boy_6), value);
	}

	inline static int32_t get_offset_of_girlSelectSe_7() { return static_cast<int32_t>(offsetof(SelectScript_t398995261, ___girlSelectSe_7)); }
	inline AudioClip_t1932558630 * get_girlSelectSe_7() const { return ___girlSelectSe_7; }
	inline AudioClip_t1932558630 ** get_address_of_girlSelectSe_7() { return &___girlSelectSe_7; }
	inline void set_girlSelectSe_7(AudioClip_t1932558630 * value)
	{
		___girlSelectSe_7 = value;
		Il2CppCodeGenWriteBarrier((&___girlSelectSe_7), value);
	}

	inline static int32_t get_offset_of_girlStartSe_8() { return static_cast<int32_t>(offsetof(SelectScript_t398995261, ___girlStartSe_8)); }
	inline AudioClip_t1932558630 * get_girlStartSe_8() const { return ___girlStartSe_8; }
	inline AudioClip_t1932558630 ** get_address_of_girlStartSe_8() { return &___girlStartSe_8; }
	inline void set_girlStartSe_8(AudioClip_t1932558630 * value)
	{
		___girlStartSe_8 = value;
		Il2CppCodeGenWriteBarrier((&___girlStartSe_8), value);
	}

	inline static int32_t get_offset_of_boySelectSe_9() { return static_cast<int32_t>(offsetof(SelectScript_t398995261, ___boySelectSe_9)); }
	inline AudioClip_t1932558630 * get_boySelectSe_9() const { return ___boySelectSe_9; }
	inline AudioClip_t1932558630 ** get_address_of_boySelectSe_9() { return &___boySelectSe_9; }
	inline void set_boySelectSe_9(AudioClip_t1932558630 * value)
	{
		___boySelectSe_9 = value;
		Il2CppCodeGenWriteBarrier((&___boySelectSe_9), value);
	}

	inline static int32_t get_offset_of_boyStartSe_10() { return static_cast<int32_t>(offsetof(SelectScript_t398995261, ___boyStartSe_10)); }
	inline AudioClip_t1932558630 * get_boyStartSe_10() const { return ___boyStartSe_10; }
	inline AudioClip_t1932558630 ** get_address_of_boyStartSe_10() { return &___boyStartSe_10; }
	inline void set_boyStartSe_10(AudioClip_t1932558630 * value)
	{
		___boyStartSe_10 = value;
		Il2CppCodeGenWriteBarrier((&___boyStartSe_10), value);
	}

	inline static int32_t get_offset_of_girlAudio_11() { return static_cast<int32_t>(offsetof(SelectScript_t398995261, ___girlAudio_11)); }
	inline AudioSource_t1135106623 * get_girlAudio_11() const { return ___girlAudio_11; }
	inline AudioSource_t1135106623 ** get_address_of_girlAudio_11() { return &___girlAudio_11; }
	inline void set_girlAudio_11(AudioSource_t1135106623 * value)
	{
		___girlAudio_11 = value;
		Il2CppCodeGenWriteBarrier((&___girlAudio_11), value);
	}

	inline static int32_t get_offset_of_boyAudio_12() { return static_cast<int32_t>(offsetof(SelectScript_t398995261, ___boyAudio_12)); }
	inline AudioSource_t1135106623 * get_boyAudio_12() const { return ___boyAudio_12; }
	inline AudioSource_t1135106623 ** get_address_of_boyAudio_12() { return &___boyAudio_12; }
	inline void set_boyAudio_12(AudioSource_t1135106623 * value)
	{
		___boyAudio_12 = value;
		Il2CppCodeGenWriteBarrier((&___boyAudio_12), value);
	}

	inline static int32_t get_offset_of_toSettings_13() { return static_cast<int32_t>(offsetof(SelectScript_t398995261, ___toSettings_13)); }
	inline ToSettingScript_t3239318692 * get_toSettings_13() const { return ___toSettings_13; }
	inline ToSettingScript_t3239318692 ** get_address_of_toSettings_13() { return &___toSettings_13; }
	inline void set_toSettings_13(ToSettingScript_t3239318692 * value)
	{
		___toSettings_13 = value;
		Il2CppCodeGenWriteBarrier((&___toSettings_13), value);
	}

	inline static int32_t get_offset_of_chara_14() { return static_cast<int32_t>(offsetof(SelectScript_t398995261, ___chara_14)); }
	inline int32_t get_chara_14() const { return ___chara_14; }
	inline int32_t* get_address_of_chara_14() { return &___chara_14; }
	inline void set_chara_14(int32_t value)
	{
		___chara_14 = value;
	}

	inline static int32_t get_offset_of_touchEnabled_15() { return static_cast<int32_t>(offsetof(SelectScript_t398995261, ___touchEnabled_15)); }
	inline bool get_touchEnabled_15() const { return ___touchEnabled_15; }
	inline bool* get_address_of_touchEnabled_15() { return &___touchEnabled_15; }
	inline void set_touchEnabled_15(bool value)
	{
		___touchEnabled_15 = value;
	}

	inline static int32_t get_offset_of_girlPos_16() { return static_cast<int32_t>(offsetof(SelectScript_t398995261, ___girlPos_16)); }
	inline Vector3_t2243707580  get_girlPos_16() const { return ___girlPos_16; }
	inline Vector3_t2243707580 * get_address_of_girlPos_16() { return &___girlPos_16; }
	inline void set_girlPos_16(Vector3_t2243707580  value)
	{
		___girlPos_16 = value;
	}

	inline static int32_t get_offset_of_boyPos_17() { return static_cast<int32_t>(offsetof(SelectScript_t398995261, ___boyPos_17)); }
	inline Vector3_t2243707580  get_boyPos_17() const { return ___boyPos_17; }
	inline Vector3_t2243707580 * get_address_of_boyPos_17() { return &___boyPos_17; }
	inline void set_boyPos_17(Vector3_t2243707580  value)
	{
		___boyPos_17 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SELECTSCRIPT_T398995261_H
#ifndef AUTOBLINKFORSD_T2101017347_H
#define AUTOBLINKFORSD_T2101017347_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityChan.AutoBlinkforSD
struct  AutoBlinkforSD_t2101017347  : public MonoBehaviour_t1158329972
{
public:
	// System.Boolean UnityChan.AutoBlinkforSD::isActive
	bool ___isActive_2;
	// UnityEngine.SkinnedMeshRenderer UnityChan.AutoBlinkforSD::ref_face
	SkinnedMeshRenderer_t4220419316 * ___ref_face_3;
	// System.Single UnityChan.AutoBlinkforSD::ratio_Close
	float ___ratio_Close_4;
	// System.Single UnityChan.AutoBlinkforSD::ratio_HalfClose
	float ___ratio_HalfClose_5;
	// System.Int32 UnityChan.AutoBlinkforSD::index_EYE_blk
	int32_t ___index_EYE_blk_6;
	// System.Int32 UnityChan.AutoBlinkforSD::index_EYE_sml
	int32_t ___index_EYE_sml_7;
	// System.Int32 UnityChan.AutoBlinkforSD::index_EYE_dmg
	int32_t ___index_EYE_dmg_8;
	// System.Single UnityChan.AutoBlinkforSD::ratio_Open
	float ___ratio_Open_9;
	// System.Boolean UnityChan.AutoBlinkforSD::timerStarted
	bool ___timerStarted_10;
	// System.Boolean UnityChan.AutoBlinkforSD::isBlink
	bool ___isBlink_11;
	// System.Single UnityChan.AutoBlinkforSD::timeBlink
	float ___timeBlink_12;
	// System.Single UnityChan.AutoBlinkforSD::timeRemining
	float ___timeRemining_13;
	// System.Single UnityChan.AutoBlinkforSD::threshold
	float ___threshold_14;
	// System.Single UnityChan.AutoBlinkforSD::interval
	float ___interval_15;
	// UnityChan.AutoBlinkforSD/Status UnityChan.AutoBlinkforSD::eyeStatus
	int32_t ___eyeStatus_16;

public:
	inline static int32_t get_offset_of_isActive_2() { return static_cast<int32_t>(offsetof(AutoBlinkforSD_t2101017347, ___isActive_2)); }
	inline bool get_isActive_2() const { return ___isActive_2; }
	inline bool* get_address_of_isActive_2() { return &___isActive_2; }
	inline void set_isActive_2(bool value)
	{
		___isActive_2 = value;
	}

	inline static int32_t get_offset_of_ref_face_3() { return static_cast<int32_t>(offsetof(AutoBlinkforSD_t2101017347, ___ref_face_3)); }
	inline SkinnedMeshRenderer_t4220419316 * get_ref_face_3() const { return ___ref_face_3; }
	inline SkinnedMeshRenderer_t4220419316 ** get_address_of_ref_face_3() { return &___ref_face_3; }
	inline void set_ref_face_3(SkinnedMeshRenderer_t4220419316 * value)
	{
		___ref_face_3 = value;
		Il2CppCodeGenWriteBarrier((&___ref_face_3), value);
	}

	inline static int32_t get_offset_of_ratio_Close_4() { return static_cast<int32_t>(offsetof(AutoBlinkforSD_t2101017347, ___ratio_Close_4)); }
	inline float get_ratio_Close_4() const { return ___ratio_Close_4; }
	inline float* get_address_of_ratio_Close_4() { return &___ratio_Close_4; }
	inline void set_ratio_Close_4(float value)
	{
		___ratio_Close_4 = value;
	}

	inline static int32_t get_offset_of_ratio_HalfClose_5() { return static_cast<int32_t>(offsetof(AutoBlinkforSD_t2101017347, ___ratio_HalfClose_5)); }
	inline float get_ratio_HalfClose_5() const { return ___ratio_HalfClose_5; }
	inline float* get_address_of_ratio_HalfClose_5() { return &___ratio_HalfClose_5; }
	inline void set_ratio_HalfClose_5(float value)
	{
		___ratio_HalfClose_5 = value;
	}

	inline static int32_t get_offset_of_index_EYE_blk_6() { return static_cast<int32_t>(offsetof(AutoBlinkforSD_t2101017347, ___index_EYE_blk_6)); }
	inline int32_t get_index_EYE_blk_6() const { return ___index_EYE_blk_6; }
	inline int32_t* get_address_of_index_EYE_blk_6() { return &___index_EYE_blk_6; }
	inline void set_index_EYE_blk_6(int32_t value)
	{
		___index_EYE_blk_6 = value;
	}

	inline static int32_t get_offset_of_index_EYE_sml_7() { return static_cast<int32_t>(offsetof(AutoBlinkforSD_t2101017347, ___index_EYE_sml_7)); }
	inline int32_t get_index_EYE_sml_7() const { return ___index_EYE_sml_7; }
	inline int32_t* get_address_of_index_EYE_sml_7() { return &___index_EYE_sml_7; }
	inline void set_index_EYE_sml_7(int32_t value)
	{
		___index_EYE_sml_7 = value;
	}

	inline static int32_t get_offset_of_index_EYE_dmg_8() { return static_cast<int32_t>(offsetof(AutoBlinkforSD_t2101017347, ___index_EYE_dmg_8)); }
	inline int32_t get_index_EYE_dmg_8() const { return ___index_EYE_dmg_8; }
	inline int32_t* get_address_of_index_EYE_dmg_8() { return &___index_EYE_dmg_8; }
	inline void set_index_EYE_dmg_8(int32_t value)
	{
		___index_EYE_dmg_8 = value;
	}

	inline static int32_t get_offset_of_ratio_Open_9() { return static_cast<int32_t>(offsetof(AutoBlinkforSD_t2101017347, ___ratio_Open_9)); }
	inline float get_ratio_Open_9() const { return ___ratio_Open_9; }
	inline float* get_address_of_ratio_Open_9() { return &___ratio_Open_9; }
	inline void set_ratio_Open_9(float value)
	{
		___ratio_Open_9 = value;
	}

	inline static int32_t get_offset_of_timerStarted_10() { return static_cast<int32_t>(offsetof(AutoBlinkforSD_t2101017347, ___timerStarted_10)); }
	inline bool get_timerStarted_10() const { return ___timerStarted_10; }
	inline bool* get_address_of_timerStarted_10() { return &___timerStarted_10; }
	inline void set_timerStarted_10(bool value)
	{
		___timerStarted_10 = value;
	}

	inline static int32_t get_offset_of_isBlink_11() { return static_cast<int32_t>(offsetof(AutoBlinkforSD_t2101017347, ___isBlink_11)); }
	inline bool get_isBlink_11() const { return ___isBlink_11; }
	inline bool* get_address_of_isBlink_11() { return &___isBlink_11; }
	inline void set_isBlink_11(bool value)
	{
		___isBlink_11 = value;
	}

	inline static int32_t get_offset_of_timeBlink_12() { return static_cast<int32_t>(offsetof(AutoBlinkforSD_t2101017347, ___timeBlink_12)); }
	inline float get_timeBlink_12() const { return ___timeBlink_12; }
	inline float* get_address_of_timeBlink_12() { return &___timeBlink_12; }
	inline void set_timeBlink_12(float value)
	{
		___timeBlink_12 = value;
	}

	inline static int32_t get_offset_of_timeRemining_13() { return static_cast<int32_t>(offsetof(AutoBlinkforSD_t2101017347, ___timeRemining_13)); }
	inline float get_timeRemining_13() const { return ___timeRemining_13; }
	inline float* get_address_of_timeRemining_13() { return &___timeRemining_13; }
	inline void set_timeRemining_13(float value)
	{
		___timeRemining_13 = value;
	}

	inline static int32_t get_offset_of_threshold_14() { return static_cast<int32_t>(offsetof(AutoBlinkforSD_t2101017347, ___threshold_14)); }
	inline float get_threshold_14() const { return ___threshold_14; }
	inline float* get_address_of_threshold_14() { return &___threshold_14; }
	inline void set_threshold_14(float value)
	{
		___threshold_14 = value;
	}

	inline static int32_t get_offset_of_interval_15() { return static_cast<int32_t>(offsetof(AutoBlinkforSD_t2101017347, ___interval_15)); }
	inline float get_interval_15() const { return ___interval_15; }
	inline float* get_address_of_interval_15() { return &___interval_15; }
	inline void set_interval_15(float value)
	{
		___interval_15 = value;
	}

	inline static int32_t get_offset_of_eyeStatus_16() { return static_cast<int32_t>(offsetof(AutoBlinkforSD_t2101017347, ___eyeStatus_16)); }
	inline int32_t get_eyeStatus_16() const { return ___eyeStatus_16; }
	inline int32_t* get_address_of_eyeStatus_16() { return &___eyeStatus_16; }
	inline void set_eyeStatus_16(int32_t value)
	{
		___eyeStatus_16 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AUTOBLINKFORSD_T2101017347_H
#ifndef CAMERACONTROLLER_T231271813_H
#define CAMERACONTROLLER_T231271813_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityChan.CameraController
struct  CameraController_t231271813  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.Vector3 UnityChan.CameraController::focus
	Vector3_t2243707580  ___focus_2;
	// UnityEngine.GameObject UnityChan.CameraController::focusObj
	GameObject_t1756533147 * ___focusObj_3;
	// System.Boolean UnityChan.CameraController::showInstWindow
	bool ___showInstWindow_4;
	// UnityEngine.Vector3 UnityChan.CameraController::oldPos
	Vector3_t2243707580  ___oldPos_5;

public:
	inline static int32_t get_offset_of_focus_2() { return static_cast<int32_t>(offsetof(CameraController_t231271813, ___focus_2)); }
	inline Vector3_t2243707580  get_focus_2() const { return ___focus_2; }
	inline Vector3_t2243707580 * get_address_of_focus_2() { return &___focus_2; }
	inline void set_focus_2(Vector3_t2243707580  value)
	{
		___focus_2 = value;
	}

	inline static int32_t get_offset_of_focusObj_3() { return static_cast<int32_t>(offsetof(CameraController_t231271813, ___focusObj_3)); }
	inline GameObject_t1756533147 * get_focusObj_3() const { return ___focusObj_3; }
	inline GameObject_t1756533147 ** get_address_of_focusObj_3() { return &___focusObj_3; }
	inline void set_focusObj_3(GameObject_t1756533147 * value)
	{
		___focusObj_3 = value;
		Il2CppCodeGenWriteBarrier((&___focusObj_3), value);
	}

	inline static int32_t get_offset_of_showInstWindow_4() { return static_cast<int32_t>(offsetof(CameraController_t231271813, ___showInstWindow_4)); }
	inline bool get_showInstWindow_4() const { return ___showInstWindow_4; }
	inline bool* get_address_of_showInstWindow_4() { return &___showInstWindow_4; }
	inline void set_showInstWindow_4(bool value)
	{
		___showInstWindow_4 = value;
	}

	inline static int32_t get_offset_of_oldPos_5() { return static_cast<int32_t>(offsetof(CameraController_t231271813, ___oldPos_5)); }
	inline Vector3_t2243707580  get_oldPos_5() const { return ___oldPos_5; }
	inline Vector3_t2243707580 * get_address_of_oldPos_5() { return &___oldPos_5; }
	inline void set_oldPos_5(Vector3_t2243707580  value)
	{
		___oldPos_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CAMERACONTROLLER_T231271813_H
#ifndef FACEUPDATE_T2436360706_H
#define FACEUPDATE_T2436360706_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityChan.FaceUpdate
struct  FaceUpdate_t2436360706  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.AnimationClip[] UnityChan.FaceUpdate::animations
	AnimationClipU5BU5D_t3936083219* ___animations_2;
	// UnityEngine.Animator UnityChan.FaceUpdate::anim
	Animator_t69676727 * ___anim_3;
	// System.Single UnityChan.FaceUpdate::delayWeight
	float ___delayWeight_4;
	// System.Boolean UnityChan.FaceUpdate::isKeepFace
	bool ___isKeepFace_5;
	// System.Boolean UnityChan.FaceUpdate::isGUI
	bool ___isGUI_6;
	// System.Single UnityChan.FaceUpdate::current
	float ___current_7;

public:
	inline static int32_t get_offset_of_animations_2() { return static_cast<int32_t>(offsetof(FaceUpdate_t2436360706, ___animations_2)); }
	inline AnimationClipU5BU5D_t3936083219* get_animations_2() const { return ___animations_2; }
	inline AnimationClipU5BU5D_t3936083219** get_address_of_animations_2() { return &___animations_2; }
	inline void set_animations_2(AnimationClipU5BU5D_t3936083219* value)
	{
		___animations_2 = value;
		Il2CppCodeGenWriteBarrier((&___animations_2), value);
	}

	inline static int32_t get_offset_of_anim_3() { return static_cast<int32_t>(offsetof(FaceUpdate_t2436360706, ___anim_3)); }
	inline Animator_t69676727 * get_anim_3() const { return ___anim_3; }
	inline Animator_t69676727 ** get_address_of_anim_3() { return &___anim_3; }
	inline void set_anim_3(Animator_t69676727 * value)
	{
		___anim_3 = value;
		Il2CppCodeGenWriteBarrier((&___anim_3), value);
	}

	inline static int32_t get_offset_of_delayWeight_4() { return static_cast<int32_t>(offsetof(FaceUpdate_t2436360706, ___delayWeight_4)); }
	inline float get_delayWeight_4() const { return ___delayWeight_4; }
	inline float* get_address_of_delayWeight_4() { return &___delayWeight_4; }
	inline void set_delayWeight_4(float value)
	{
		___delayWeight_4 = value;
	}

	inline static int32_t get_offset_of_isKeepFace_5() { return static_cast<int32_t>(offsetof(FaceUpdate_t2436360706, ___isKeepFace_5)); }
	inline bool get_isKeepFace_5() const { return ___isKeepFace_5; }
	inline bool* get_address_of_isKeepFace_5() { return &___isKeepFace_5; }
	inline void set_isKeepFace_5(bool value)
	{
		___isKeepFace_5 = value;
	}

	inline static int32_t get_offset_of_isGUI_6() { return static_cast<int32_t>(offsetof(FaceUpdate_t2436360706, ___isGUI_6)); }
	inline bool get_isGUI_6() const { return ___isGUI_6; }
	inline bool* get_address_of_isGUI_6() { return &___isGUI_6; }
	inline void set_isGUI_6(bool value)
	{
		___isGUI_6 = value;
	}

	inline static int32_t get_offset_of_current_7() { return static_cast<int32_t>(offsetof(FaceUpdate_t2436360706, ___current_7)); }
	inline float get_current_7() const { return ___current_7; }
	inline float* get_address_of_current_7() { return &___current_7; }
	inline void set_current_7(float value)
	{
		___current_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FACEUPDATE_T2436360706_H
#ifndef IDLECHANGER_T2567672470_H
#define IDLECHANGER_T2567672470_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityChan.IdleChanger
struct  IdleChanger_t2567672470  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.Animator UnityChan.IdleChanger::anim
	Animator_t69676727 * ___anim_2;
	// UnityEngine.AnimatorStateInfo UnityChan.IdleChanger::currentState
	AnimatorStateInfo_t2577870592  ___currentState_3;
	// UnityEngine.AnimatorStateInfo UnityChan.IdleChanger::previousState
	AnimatorStateInfo_t2577870592  ___previousState_4;
	// System.Boolean UnityChan.IdleChanger::_random
	bool ____random_5;
	// System.Single UnityChan.IdleChanger::_threshold
	float ____threshold_6;
	// System.Single UnityChan.IdleChanger::_interval
	float ____interval_7;
	// System.Boolean UnityChan.IdleChanger::isGUI
	bool ___isGUI_8;

public:
	inline static int32_t get_offset_of_anim_2() { return static_cast<int32_t>(offsetof(IdleChanger_t2567672470, ___anim_2)); }
	inline Animator_t69676727 * get_anim_2() const { return ___anim_2; }
	inline Animator_t69676727 ** get_address_of_anim_2() { return &___anim_2; }
	inline void set_anim_2(Animator_t69676727 * value)
	{
		___anim_2 = value;
		Il2CppCodeGenWriteBarrier((&___anim_2), value);
	}

	inline static int32_t get_offset_of_currentState_3() { return static_cast<int32_t>(offsetof(IdleChanger_t2567672470, ___currentState_3)); }
	inline AnimatorStateInfo_t2577870592  get_currentState_3() const { return ___currentState_3; }
	inline AnimatorStateInfo_t2577870592 * get_address_of_currentState_3() { return &___currentState_3; }
	inline void set_currentState_3(AnimatorStateInfo_t2577870592  value)
	{
		___currentState_3 = value;
	}

	inline static int32_t get_offset_of_previousState_4() { return static_cast<int32_t>(offsetof(IdleChanger_t2567672470, ___previousState_4)); }
	inline AnimatorStateInfo_t2577870592  get_previousState_4() const { return ___previousState_4; }
	inline AnimatorStateInfo_t2577870592 * get_address_of_previousState_4() { return &___previousState_4; }
	inline void set_previousState_4(AnimatorStateInfo_t2577870592  value)
	{
		___previousState_4 = value;
	}

	inline static int32_t get_offset_of__random_5() { return static_cast<int32_t>(offsetof(IdleChanger_t2567672470, ____random_5)); }
	inline bool get__random_5() const { return ____random_5; }
	inline bool* get_address_of__random_5() { return &____random_5; }
	inline void set__random_5(bool value)
	{
		____random_5 = value;
	}

	inline static int32_t get_offset_of__threshold_6() { return static_cast<int32_t>(offsetof(IdleChanger_t2567672470, ____threshold_6)); }
	inline float get__threshold_6() const { return ____threshold_6; }
	inline float* get_address_of__threshold_6() { return &____threshold_6; }
	inline void set__threshold_6(float value)
	{
		____threshold_6 = value;
	}

	inline static int32_t get_offset_of__interval_7() { return static_cast<int32_t>(offsetof(IdleChanger_t2567672470, ____interval_7)); }
	inline float get__interval_7() const { return ____interval_7; }
	inline float* get_address_of__interval_7() { return &____interval_7; }
	inline void set__interval_7(float value)
	{
		____interval_7 = value;
	}

	inline static int32_t get_offset_of_isGUI_8() { return static_cast<int32_t>(offsetof(IdleChanger_t2567672470, ___isGUI_8)); }
	inline bool get_isGUI_8() const { return ___isGUI_8; }
	inline bool* get_address_of_isGUI_8() { return &___isGUI_8; }
	inline void set_isGUI_8(bool value)
	{
		___isGUI_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IDLECHANGER_T2567672470_H
#ifndef IKCTRLRIGHTHAND_T3240365066_H
#define IKCTRLRIGHTHAND_T3240365066_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityChan.IKCtrlRightHand
struct  IKCtrlRightHand_t3240365066  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.Animator UnityChan.IKCtrlRightHand::anim
	Animator_t69676727 * ___anim_2;
	// UnityEngine.Transform UnityChan.IKCtrlRightHand::targetObj
	Transform_t3275118058 * ___targetObj_3;
	// System.Boolean UnityChan.IKCtrlRightHand::isIkActive
	bool ___isIkActive_4;
	// System.Single UnityChan.IKCtrlRightHand::mixWeight
	float ___mixWeight_5;

public:
	inline static int32_t get_offset_of_anim_2() { return static_cast<int32_t>(offsetof(IKCtrlRightHand_t3240365066, ___anim_2)); }
	inline Animator_t69676727 * get_anim_2() const { return ___anim_2; }
	inline Animator_t69676727 ** get_address_of_anim_2() { return &___anim_2; }
	inline void set_anim_2(Animator_t69676727 * value)
	{
		___anim_2 = value;
		Il2CppCodeGenWriteBarrier((&___anim_2), value);
	}

	inline static int32_t get_offset_of_targetObj_3() { return static_cast<int32_t>(offsetof(IKCtrlRightHand_t3240365066, ___targetObj_3)); }
	inline Transform_t3275118058 * get_targetObj_3() const { return ___targetObj_3; }
	inline Transform_t3275118058 ** get_address_of_targetObj_3() { return &___targetObj_3; }
	inline void set_targetObj_3(Transform_t3275118058 * value)
	{
		___targetObj_3 = value;
		Il2CppCodeGenWriteBarrier((&___targetObj_3), value);
	}

	inline static int32_t get_offset_of_isIkActive_4() { return static_cast<int32_t>(offsetof(IKCtrlRightHand_t3240365066, ___isIkActive_4)); }
	inline bool get_isIkActive_4() const { return ___isIkActive_4; }
	inline bool* get_address_of_isIkActive_4() { return &___isIkActive_4; }
	inline void set_isIkActive_4(bool value)
	{
		___isIkActive_4 = value;
	}

	inline static int32_t get_offset_of_mixWeight_5() { return static_cast<int32_t>(offsetof(IKCtrlRightHand_t3240365066, ___mixWeight_5)); }
	inline float get_mixWeight_5() const { return ___mixWeight_5; }
	inline float* get_address_of_mixWeight_5() { return &___mixWeight_5; }
	inline void set_mixWeight_5(float value)
	{
		___mixWeight_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IKCTRLRIGHTHAND_T3240365066_H
#ifndef IKLOOKAT_T1884959938_H
#define IKLOOKAT_T1884959938_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityChan.IKLookAt
struct  IKLookAt_t1884959938  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.Animator UnityChan.IKLookAt::avator
	Animator_t69676727 * ___avator_2;
	// UnityEngine.MeshRenderer UnityChan.IKLookAt::target
	MeshRenderer_t1268241104 * ___target_3;
	// System.Boolean UnityChan.IKLookAt::ikActive
	bool ___ikActive_4;
	// UnityEngine.Transform UnityChan.IKLookAt::lookAtObj
	Transform_t3275118058 * ___lookAtObj_5;
	// System.Single UnityChan.IKLookAt::lookAtWeight
	float ___lookAtWeight_6;
	// System.Single UnityChan.IKLookAt::bodyWeight
	float ___bodyWeight_7;
	// System.Single UnityChan.IKLookAt::headWeight
	float ___headWeight_8;
	// System.Single UnityChan.IKLookAt::eyesWeight
	float ___eyesWeight_9;
	// System.Single UnityChan.IKLookAt::clampWeight
	float ___clampWeight_10;
	// System.Boolean UnityChan.IKLookAt::isGUI
	bool ___isGUI_11;

public:
	inline static int32_t get_offset_of_avator_2() { return static_cast<int32_t>(offsetof(IKLookAt_t1884959938, ___avator_2)); }
	inline Animator_t69676727 * get_avator_2() const { return ___avator_2; }
	inline Animator_t69676727 ** get_address_of_avator_2() { return &___avator_2; }
	inline void set_avator_2(Animator_t69676727 * value)
	{
		___avator_2 = value;
		Il2CppCodeGenWriteBarrier((&___avator_2), value);
	}

	inline static int32_t get_offset_of_target_3() { return static_cast<int32_t>(offsetof(IKLookAt_t1884959938, ___target_3)); }
	inline MeshRenderer_t1268241104 * get_target_3() const { return ___target_3; }
	inline MeshRenderer_t1268241104 ** get_address_of_target_3() { return &___target_3; }
	inline void set_target_3(MeshRenderer_t1268241104 * value)
	{
		___target_3 = value;
		Il2CppCodeGenWriteBarrier((&___target_3), value);
	}

	inline static int32_t get_offset_of_ikActive_4() { return static_cast<int32_t>(offsetof(IKLookAt_t1884959938, ___ikActive_4)); }
	inline bool get_ikActive_4() const { return ___ikActive_4; }
	inline bool* get_address_of_ikActive_4() { return &___ikActive_4; }
	inline void set_ikActive_4(bool value)
	{
		___ikActive_4 = value;
	}

	inline static int32_t get_offset_of_lookAtObj_5() { return static_cast<int32_t>(offsetof(IKLookAt_t1884959938, ___lookAtObj_5)); }
	inline Transform_t3275118058 * get_lookAtObj_5() const { return ___lookAtObj_5; }
	inline Transform_t3275118058 ** get_address_of_lookAtObj_5() { return &___lookAtObj_5; }
	inline void set_lookAtObj_5(Transform_t3275118058 * value)
	{
		___lookAtObj_5 = value;
		Il2CppCodeGenWriteBarrier((&___lookAtObj_5), value);
	}

	inline static int32_t get_offset_of_lookAtWeight_6() { return static_cast<int32_t>(offsetof(IKLookAt_t1884959938, ___lookAtWeight_6)); }
	inline float get_lookAtWeight_6() const { return ___lookAtWeight_6; }
	inline float* get_address_of_lookAtWeight_6() { return &___lookAtWeight_6; }
	inline void set_lookAtWeight_6(float value)
	{
		___lookAtWeight_6 = value;
	}

	inline static int32_t get_offset_of_bodyWeight_7() { return static_cast<int32_t>(offsetof(IKLookAt_t1884959938, ___bodyWeight_7)); }
	inline float get_bodyWeight_7() const { return ___bodyWeight_7; }
	inline float* get_address_of_bodyWeight_7() { return &___bodyWeight_7; }
	inline void set_bodyWeight_7(float value)
	{
		___bodyWeight_7 = value;
	}

	inline static int32_t get_offset_of_headWeight_8() { return static_cast<int32_t>(offsetof(IKLookAt_t1884959938, ___headWeight_8)); }
	inline float get_headWeight_8() const { return ___headWeight_8; }
	inline float* get_address_of_headWeight_8() { return &___headWeight_8; }
	inline void set_headWeight_8(float value)
	{
		___headWeight_8 = value;
	}

	inline static int32_t get_offset_of_eyesWeight_9() { return static_cast<int32_t>(offsetof(IKLookAt_t1884959938, ___eyesWeight_9)); }
	inline float get_eyesWeight_9() const { return ___eyesWeight_9; }
	inline float* get_address_of_eyesWeight_9() { return &___eyesWeight_9; }
	inline void set_eyesWeight_9(float value)
	{
		___eyesWeight_9 = value;
	}

	inline static int32_t get_offset_of_clampWeight_10() { return static_cast<int32_t>(offsetof(IKLookAt_t1884959938, ___clampWeight_10)); }
	inline float get_clampWeight_10() const { return ___clampWeight_10; }
	inline float* get_address_of_clampWeight_10() { return &___clampWeight_10; }
	inline void set_clampWeight_10(float value)
	{
		___clampWeight_10 = value;
	}

	inline static int32_t get_offset_of_isGUI_11() { return static_cast<int32_t>(offsetof(IKLookAt_t1884959938, ___isGUI_11)); }
	inline bool get_isGUI_11() const { return ___isGUI_11; }
	inline bool* get_address_of_isGUI_11() { return &___isGUI_11; }
	inline void set_isGUI_11(bool value)
	{
		___isGUI_11 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IKLOOKAT_T1884959938_H
#ifndef RANDOMWIND_T3585812647_H
#define RANDOMWIND_T3585812647_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityChan.RandomWind
struct  RandomWind_t3585812647  : public MonoBehaviour_t1158329972
{
public:
	// UnityChan.SpringBone[] UnityChan.RandomWind::springBones
	SpringBoneU5BU5D_t3004447128* ___springBones_2;
	// System.Boolean UnityChan.RandomWind::isWindActive
	bool ___isWindActive_3;
	// System.Boolean UnityChan.RandomWind::isMinus
	bool ___isMinus_4;
	// System.Single UnityChan.RandomWind::threshold
	float ___threshold_5;
	// System.Single UnityChan.RandomWind::interval
	float ___interval_6;
	// System.Single UnityChan.RandomWind::windPower
	float ___windPower_7;
	// System.Single UnityChan.RandomWind::gravity
	float ___gravity_8;
	// System.Boolean UnityChan.RandomWind::isGUI
	bool ___isGUI_9;

public:
	inline static int32_t get_offset_of_springBones_2() { return static_cast<int32_t>(offsetof(RandomWind_t3585812647, ___springBones_2)); }
	inline SpringBoneU5BU5D_t3004447128* get_springBones_2() const { return ___springBones_2; }
	inline SpringBoneU5BU5D_t3004447128** get_address_of_springBones_2() { return &___springBones_2; }
	inline void set_springBones_2(SpringBoneU5BU5D_t3004447128* value)
	{
		___springBones_2 = value;
		Il2CppCodeGenWriteBarrier((&___springBones_2), value);
	}

	inline static int32_t get_offset_of_isWindActive_3() { return static_cast<int32_t>(offsetof(RandomWind_t3585812647, ___isWindActive_3)); }
	inline bool get_isWindActive_3() const { return ___isWindActive_3; }
	inline bool* get_address_of_isWindActive_3() { return &___isWindActive_3; }
	inline void set_isWindActive_3(bool value)
	{
		___isWindActive_3 = value;
	}

	inline static int32_t get_offset_of_isMinus_4() { return static_cast<int32_t>(offsetof(RandomWind_t3585812647, ___isMinus_4)); }
	inline bool get_isMinus_4() const { return ___isMinus_4; }
	inline bool* get_address_of_isMinus_4() { return &___isMinus_4; }
	inline void set_isMinus_4(bool value)
	{
		___isMinus_4 = value;
	}

	inline static int32_t get_offset_of_threshold_5() { return static_cast<int32_t>(offsetof(RandomWind_t3585812647, ___threshold_5)); }
	inline float get_threshold_5() const { return ___threshold_5; }
	inline float* get_address_of_threshold_5() { return &___threshold_5; }
	inline void set_threshold_5(float value)
	{
		___threshold_5 = value;
	}

	inline static int32_t get_offset_of_interval_6() { return static_cast<int32_t>(offsetof(RandomWind_t3585812647, ___interval_6)); }
	inline float get_interval_6() const { return ___interval_6; }
	inline float* get_address_of_interval_6() { return &___interval_6; }
	inline void set_interval_6(float value)
	{
		___interval_6 = value;
	}

	inline static int32_t get_offset_of_windPower_7() { return static_cast<int32_t>(offsetof(RandomWind_t3585812647, ___windPower_7)); }
	inline float get_windPower_7() const { return ___windPower_7; }
	inline float* get_address_of_windPower_7() { return &___windPower_7; }
	inline void set_windPower_7(float value)
	{
		___windPower_7 = value;
	}

	inline static int32_t get_offset_of_gravity_8() { return static_cast<int32_t>(offsetof(RandomWind_t3585812647, ___gravity_8)); }
	inline float get_gravity_8() const { return ___gravity_8; }
	inline float* get_address_of_gravity_8() { return &___gravity_8; }
	inline void set_gravity_8(float value)
	{
		___gravity_8 = value;
	}

	inline static int32_t get_offset_of_isGUI_9() { return static_cast<int32_t>(offsetof(RandomWind_t3585812647, ___isGUI_9)); }
	inline bool get_isGUI_9() const { return ___isGUI_9; }
	inline bool* get_address_of_isGUI_9() { return &___isGUI_9; }
	inline void set_isGUI_9(bool value)
	{
		___isGUI_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RANDOMWIND_T3585812647_H
#ifndef SPRINGBONE_T3136191221_H
#define SPRINGBONE_T3136191221_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityChan.SpringBone
struct  SpringBone_t3136191221  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.Transform UnityChan.SpringBone::child
	Transform_t3275118058 * ___child_2;
	// UnityEngine.Vector3 UnityChan.SpringBone::boneAxis
	Vector3_t2243707580  ___boneAxis_3;
	// System.Single UnityChan.SpringBone::radius
	float ___radius_4;
	// System.Boolean UnityChan.SpringBone::isUseEachBoneForceSettings
	bool ___isUseEachBoneForceSettings_5;
	// System.Single UnityChan.SpringBone::stiffnessForce
	float ___stiffnessForce_6;
	// System.Single UnityChan.SpringBone::dragForce
	float ___dragForce_7;
	// UnityEngine.Vector3 UnityChan.SpringBone::springForce
	Vector3_t2243707580  ___springForce_8;
	// UnityChan.SpringCollider[] UnityChan.SpringBone::colliders
	SpringColliderU5BU5D_t2676523574* ___colliders_9;
	// System.Boolean UnityChan.SpringBone::debug
	bool ___debug_10;
	// System.Single UnityChan.SpringBone::threshold
	float ___threshold_11;
	// System.Single UnityChan.SpringBone::springLength
	float ___springLength_12;
	// UnityEngine.Quaternion UnityChan.SpringBone::localRotation
	Quaternion_t4030073918  ___localRotation_13;
	// UnityEngine.Transform UnityChan.SpringBone::trs
	Transform_t3275118058 * ___trs_14;
	// UnityEngine.Vector3 UnityChan.SpringBone::currTipPos
	Vector3_t2243707580  ___currTipPos_15;
	// UnityEngine.Vector3 UnityChan.SpringBone::prevTipPos
	Vector3_t2243707580  ___prevTipPos_16;
	// UnityEngine.Transform UnityChan.SpringBone::org
	Transform_t3275118058 * ___org_17;
	// UnityChan.SpringManager UnityChan.SpringBone::managerRef
	SpringManager_t4191634164 * ___managerRef_18;

public:
	inline static int32_t get_offset_of_child_2() { return static_cast<int32_t>(offsetof(SpringBone_t3136191221, ___child_2)); }
	inline Transform_t3275118058 * get_child_2() const { return ___child_2; }
	inline Transform_t3275118058 ** get_address_of_child_2() { return &___child_2; }
	inline void set_child_2(Transform_t3275118058 * value)
	{
		___child_2 = value;
		Il2CppCodeGenWriteBarrier((&___child_2), value);
	}

	inline static int32_t get_offset_of_boneAxis_3() { return static_cast<int32_t>(offsetof(SpringBone_t3136191221, ___boneAxis_3)); }
	inline Vector3_t2243707580  get_boneAxis_3() const { return ___boneAxis_3; }
	inline Vector3_t2243707580 * get_address_of_boneAxis_3() { return &___boneAxis_3; }
	inline void set_boneAxis_3(Vector3_t2243707580  value)
	{
		___boneAxis_3 = value;
	}

	inline static int32_t get_offset_of_radius_4() { return static_cast<int32_t>(offsetof(SpringBone_t3136191221, ___radius_4)); }
	inline float get_radius_4() const { return ___radius_4; }
	inline float* get_address_of_radius_4() { return &___radius_4; }
	inline void set_radius_4(float value)
	{
		___radius_4 = value;
	}

	inline static int32_t get_offset_of_isUseEachBoneForceSettings_5() { return static_cast<int32_t>(offsetof(SpringBone_t3136191221, ___isUseEachBoneForceSettings_5)); }
	inline bool get_isUseEachBoneForceSettings_5() const { return ___isUseEachBoneForceSettings_5; }
	inline bool* get_address_of_isUseEachBoneForceSettings_5() { return &___isUseEachBoneForceSettings_5; }
	inline void set_isUseEachBoneForceSettings_5(bool value)
	{
		___isUseEachBoneForceSettings_5 = value;
	}

	inline static int32_t get_offset_of_stiffnessForce_6() { return static_cast<int32_t>(offsetof(SpringBone_t3136191221, ___stiffnessForce_6)); }
	inline float get_stiffnessForce_6() const { return ___stiffnessForce_6; }
	inline float* get_address_of_stiffnessForce_6() { return &___stiffnessForce_6; }
	inline void set_stiffnessForce_6(float value)
	{
		___stiffnessForce_6 = value;
	}

	inline static int32_t get_offset_of_dragForce_7() { return static_cast<int32_t>(offsetof(SpringBone_t3136191221, ___dragForce_7)); }
	inline float get_dragForce_7() const { return ___dragForce_7; }
	inline float* get_address_of_dragForce_7() { return &___dragForce_7; }
	inline void set_dragForce_7(float value)
	{
		___dragForce_7 = value;
	}

	inline static int32_t get_offset_of_springForce_8() { return static_cast<int32_t>(offsetof(SpringBone_t3136191221, ___springForce_8)); }
	inline Vector3_t2243707580  get_springForce_8() const { return ___springForce_8; }
	inline Vector3_t2243707580 * get_address_of_springForce_8() { return &___springForce_8; }
	inline void set_springForce_8(Vector3_t2243707580  value)
	{
		___springForce_8 = value;
	}

	inline static int32_t get_offset_of_colliders_9() { return static_cast<int32_t>(offsetof(SpringBone_t3136191221, ___colliders_9)); }
	inline SpringColliderU5BU5D_t2676523574* get_colliders_9() const { return ___colliders_9; }
	inline SpringColliderU5BU5D_t2676523574** get_address_of_colliders_9() { return &___colliders_9; }
	inline void set_colliders_9(SpringColliderU5BU5D_t2676523574* value)
	{
		___colliders_9 = value;
		Il2CppCodeGenWriteBarrier((&___colliders_9), value);
	}

	inline static int32_t get_offset_of_debug_10() { return static_cast<int32_t>(offsetof(SpringBone_t3136191221, ___debug_10)); }
	inline bool get_debug_10() const { return ___debug_10; }
	inline bool* get_address_of_debug_10() { return &___debug_10; }
	inline void set_debug_10(bool value)
	{
		___debug_10 = value;
	}

	inline static int32_t get_offset_of_threshold_11() { return static_cast<int32_t>(offsetof(SpringBone_t3136191221, ___threshold_11)); }
	inline float get_threshold_11() const { return ___threshold_11; }
	inline float* get_address_of_threshold_11() { return &___threshold_11; }
	inline void set_threshold_11(float value)
	{
		___threshold_11 = value;
	}

	inline static int32_t get_offset_of_springLength_12() { return static_cast<int32_t>(offsetof(SpringBone_t3136191221, ___springLength_12)); }
	inline float get_springLength_12() const { return ___springLength_12; }
	inline float* get_address_of_springLength_12() { return &___springLength_12; }
	inline void set_springLength_12(float value)
	{
		___springLength_12 = value;
	}

	inline static int32_t get_offset_of_localRotation_13() { return static_cast<int32_t>(offsetof(SpringBone_t3136191221, ___localRotation_13)); }
	inline Quaternion_t4030073918  get_localRotation_13() const { return ___localRotation_13; }
	inline Quaternion_t4030073918 * get_address_of_localRotation_13() { return &___localRotation_13; }
	inline void set_localRotation_13(Quaternion_t4030073918  value)
	{
		___localRotation_13 = value;
	}

	inline static int32_t get_offset_of_trs_14() { return static_cast<int32_t>(offsetof(SpringBone_t3136191221, ___trs_14)); }
	inline Transform_t3275118058 * get_trs_14() const { return ___trs_14; }
	inline Transform_t3275118058 ** get_address_of_trs_14() { return &___trs_14; }
	inline void set_trs_14(Transform_t3275118058 * value)
	{
		___trs_14 = value;
		Il2CppCodeGenWriteBarrier((&___trs_14), value);
	}

	inline static int32_t get_offset_of_currTipPos_15() { return static_cast<int32_t>(offsetof(SpringBone_t3136191221, ___currTipPos_15)); }
	inline Vector3_t2243707580  get_currTipPos_15() const { return ___currTipPos_15; }
	inline Vector3_t2243707580 * get_address_of_currTipPos_15() { return &___currTipPos_15; }
	inline void set_currTipPos_15(Vector3_t2243707580  value)
	{
		___currTipPos_15 = value;
	}

	inline static int32_t get_offset_of_prevTipPos_16() { return static_cast<int32_t>(offsetof(SpringBone_t3136191221, ___prevTipPos_16)); }
	inline Vector3_t2243707580  get_prevTipPos_16() const { return ___prevTipPos_16; }
	inline Vector3_t2243707580 * get_address_of_prevTipPos_16() { return &___prevTipPos_16; }
	inline void set_prevTipPos_16(Vector3_t2243707580  value)
	{
		___prevTipPos_16 = value;
	}

	inline static int32_t get_offset_of_org_17() { return static_cast<int32_t>(offsetof(SpringBone_t3136191221, ___org_17)); }
	inline Transform_t3275118058 * get_org_17() const { return ___org_17; }
	inline Transform_t3275118058 ** get_address_of_org_17() { return &___org_17; }
	inline void set_org_17(Transform_t3275118058 * value)
	{
		___org_17 = value;
		Il2CppCodeGenWriteBarrier((&___org_17), value);
	}

	inline static int32_t get_offset_of_managerRef_18() { return static_cast<int32_t>(offsetof(SpringBone_t3136191221, ___managerRef_18)); }
	inline SpringManager_t4191634164 * get_managerRef_18() const { return ___managerRef_18; }
	inline SpringManager_t4191634164 ** get_address_of_managerRef_18() { return &___managerRef_18; }
	inline void set_managerRef_18(SpringManager_t4191634164 * value)
	{
		___managerRef_18 = value;
		Il2CppCodeGenWriteBarrier((&___managerRef_18), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SPRINGBONE_T3136191221_H
#ifndef SPRINGCOLLIDER_T3514471471_H
#define SPRINGCOLLIDER_T3514471471_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityChan.SpringCollider
struct  SpringCollider_t3514471471  : public MonoBehaviour_t1158329972
{
public:
	// System.Single UnityChan.SpringCollider::radius
	float ___radius_2;

public:
	inline static int32_t get_offset_of_radius_2() { return static_cast<int32_t>(offsetof(SpringCollider_t3514471471, ___radius_2)); }
	inline float get_radius_2() const { return ___radius_2; }
	inline float* get_address_of_radius_2() { return &___radius_2; }
	inline void set_radius_2(float value)
	{
		___radius_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SPRINGCOLLIDER_T3514471471_H
#ifndef SPRINGMANAGER_T4191634164_H
#define SPRINGMANAGER_T4191634164_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityChan.SpringManager
struct  SpringManager_t4191634164  : public MonoBehaviour_t1158329972
{
public:
	// System.Single UnityChan.SpringManager::dynamicRatio
	float ___dynamicRatio_2;
	// System.Single UnityChan.SpringManager::stiffnessForce
	float ___stiffnessForce_3;
	// UnityEngine.AnimationCurve UnityChan.SpringManager::stiffnessCurve
	AnimationCurve_t3306541151 * ___stiffnessCurve_4;
	// System.Single UnityChan.SpringManager::dragForce
	float ___dragForce_5;
	// UnityEngine.AnimationCurve UnityChan.SpringManager::dragCurve
	AnimationCurve_t3306541151 * ___dragCurve_6;
	// UnityChan.SpringBone[] UnityChan.SpringManager::springBones
	SpringBoneU5BU5D_t3004447128* ___springBones_7;

public:
	inline static int32_t get_offset_of_dynamicRatio_2() { return static_cast<int32_t>(offsetof(SpringManager_t4191634164, ___dynamicRatio_2)); }
	inline float get_dynamicRatio_2() const { return ___dynamicRatio_2; }
	inline float* get_address_of_dynamicRatio_2() { return &___dynamicRatio_2; }
	inline void set_dynamicRatio_2(float value)
	{
		___dynamicRatio_2 = value;
	}

	inline static int32_t get_offset_of_stiffnessForce_3() { return static_cast<int32_t>(offsetof(SpringManager_t4191634164, ___stiffnessForce_3)); }
	inline float get_stiffnessForce_3() const { return ___stiffnessForce_3; }
	inline float* get_address_of_stiffnessForce_3() { return &___stiffnessForce_3; }
	inline void set_stiffnessForce_3(float value)
	{
		___stiffnessForce_3 = value;
	}

	inline static int32_t get_offset_of_stiffnessCurve_4() { return static_cast<int32_t>(offsetof(SpringManager_t4191634164, ___stiffnessCurve_4)); }
	inline AnimationCurve_t3306541151 * get_stiffnessCurve_4() const { return ___stiffnessCurve_4; }
	inline AnimationCurve_t3306541151 ** get_address_of_stiffnessCurve_4() { return &___stiffnessCurve_4; }
	inline void set_stiffnessCurve_4(AnimationCurve_t3306541151 * value)
	{
		___stiffnessCurve_4 = value;
		Il2CppCodeGenWriteBarrier((&___stiffnessCurve_4), value);
	}

	inline static int32_t get_offset_of_dragForce_5() { return static_cast<int32_t>(offsetof(SpringManager_t4191634164, ___dragForce_5)); }
	inline float get_dragForce_5() const { return ___dragForce_5; }
	inline float* get_address_of_dragForce_5() { return &___dragForce_5; }
	inline void set_dragForce_5(float value)
	{
		___dragForce_5 = value;
	}

	inline static int32_t get_offset_of_dragCurve_6() { return static_cast<int32_t>(offsetof(SpringManager_t4191634164, ___dragCurve_6)); }
	inline AnimationCurve_t3306541151 * get_dragCurve_6() const { return ___dragCurve_6; }
	inline AnimationCurve_t3306541151 ** get_address_of_dragCurve_6() { return &___dragCurve_6; }
	inline void set_dragCurve_6(AnimationCurve_t3306541151 * value)
	{
		___dragCurve_6 = value;
		Il2CppCodeGenWriteBarrier((&___dragCurve_6), value);
	}

	inline static int32_t get_offset_of_springBones_7() { return static_cast<int32_t>(offsetof(SpringManager_t4191634164, ___springBones_7)); }
	inline SpringBoneU5BU5D_t3004447128* get_springBones_7() const { return ___springBones_7; }
	inline SpringBoneU5BU5D_t3004447128** get_address_of_springBones_7() { return &___springBones_7; }
	inline void set_springBones_7(SpringBoneU5BU5D_t3004447128* value)
	{
		___springBones_7 = value;
		Il2CppCodeGenWriteBarrier((&___springBones_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SPRINGMANAGER_T4191634164_H
#ifndef THIRDPERSONCAMERA_T1407538279_H
#define THIRDPERSONCAMERA_T1407538279_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityChan.ThirdPersonCamera
struct  ThirdPersonCamera_t1407538279  : public MonoBehaviour_t1158329972
{
public:
	// System.Single UnityChan.ThirdPersonCamera::smooth
	float ___smooth_2;
	// UnityEngine.Transform UnityChan.ThirdPersonCamera::standardPos
	Transform_t3275118058 * ___standardPos_3;
	// UnityEngine.Transform UnityChan.ThirdPersonCamera::frontPos
	Transform_t3275118058 * ___frontPos_4;
	// UnityEngine.Transform UnityChan.ThirdPersonCamera::jumpPos
	Transform_t3275118058 * ___jumpPos_5;
	// System.Boolean UnityChan.ThirdPersonCamera::bQuickSwitch
	bool ___bQuickSwitch_6;

public:
	inline static int32_t get_offset_of_smooth_2() { return static_cast<int32_t>(offsetof(ThirdPersonCamera_t1407538279, ___smooth_2)); }
	inline float get_smooth_2() const { return ___smooth_2; }
	inline float* get_address_of_smooth_2() { return &___smooth_2; }
	inline void set_smooth_2(float value)
	{
		___smooth_2 = value;
	}

	inline static int32_t get_offset_of_standardPos_3() { return static_cast<int32_t>(offsetof(ThirdPersonCamera_t1407538279, ___standardPos_3)); }
	inline Transform_t3275118058 * get_standardPos_3() const { return ___standardPos_3; }
	inline Transform_t3275118058 ** get_address_of_standardPos_3() { return &___standardPos_3; }
	inline void set_standardPos_3(Transform_t3275118058 * value)
	{
		___standardPos_3 = value;
		Il2CppCodeGenWriteBarrier((&___standardPos_3), value);
	}

	inline static int32_t get_offset_of_frontPos_4() { return static_cast<int32_t>(offsetof(ThirdPersonCamera_t1407538279, ___frontPos_4)); }
	inline Transform_t3275118058 * get_frontPos_4() const { return ___frontPos_4; }
	inline Transform_t3275118058 ** get_address_of_frontPos_4() { return &___frontPos_4; }
	inline void set_frontPos_4(Transform_t3275118058 * value)
	{
		___frontPos_4 = value;
		Il2CppCodeGenWriteBarrier((&___frontPos_4), value);
	}

	inline static int32_t get_offset_of_jumpPos_5() { return static_cast<int32_t>(offsetof(ThirdPersonCamera_t1407538279, ___jumpPos_5)); }
	inline Transform_t3275118058 * get_jumpPos_5() const { return ___jumpPos_5; }
	inline Transform_t3275118058 ** get_address_of_jumpPos_5() { return &___jumpPos_5; }
	inline void set_jumpPos_5(Transform_t3275118058 * value)
	{
		___jumpPos_5 = value;
		Il2CppCodeGenWriteBarrier((&___jumpPos_5), value);
	}

	inline static int32_t get_offset_of_bQuickSwitch_6() { return static_cast<int32_t>(offsetof(ThirdPersonCamera_t1407538279, ___bQuickSwitch_6)); }
	inline bool get_bQuickSwitch_6() const { return ___bQuickSwitch_6; }
	inline bool* get_address_of_bQuickSwitch_6() { return &___bQuickSwitch_6; }
	inline void set_bQuickSwitch_6(bool value)
	{
		___bQuickSwitch_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // THIRDPERSONCAMERA_T1407538279_H
#ifndef ASYNCTOGGLER_T306543624_H
#define ASYNCTOGGLER_T306543624_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Live2D.Cubism.Samples.AsyncBenchmark.AsyncToggler
struct  AsyncToggler_t306543624  : public MonoBehaviour_t1158329972
{
public:
	// System.Boolean Live2D.Cubism.Samples.AsyncBenchmark.AsyncToggler::EnableAsync
	bool ___EnableAsync_2;
	// System.Boolean Live2D.Cubism.Samples.AsyncBenchmark.AsyncToggler::<LastEnableSync>k__BackingField
	bool ___U3CLastEnableSyncU3Ek__BackingField_3;

public:
	inline static int32_t get_offset_of_EnableAsync_2() { return static_cast<int32_t>(offsetof(AsyncToggler_t306543624, ___EnableAsync_2)); }
	inline bool get_EnableAsync_2() const { return ___EnableAsync_2; }
	inline bool* get_address_of_EnableAsync_2() { return &___EnableAsync_2; }
	inline void set_EnableAsync_2(bool value)
	{
		___EnableAsync_2 = value;
	}

	inline static int32_t get_offset_of_U3CLastEnableSyncU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(AsyncToggler_t306543624, ___U3CLastEnableSyncU3Ek__BackingField_3)); }
	inline bool get_U3CLastEnableSyncU3Ek__BackingField_3() const { return ___U3CLastEnableSyncU3Ek__BackingField_3; }
	inline bool* get_address_of_U3CLastEnableSyncU3Ek__BackingField_3() { return &___U3CLastEnableSyncU3Ek__BackingField_3; }
	inline void set_U3CLastEnableSyncU3Ek__BackingField_3(bool value)
	{
		___U3CLastEnableSyncU3Ek__BackingField_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ASYNCTOGGLER_T306543624_H
#ifndef SAMPLEJUMPBUTTON_T2074145434_H
#define SAMPLEJUMPBUTTON_T2074145434_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SampleJumpButton
struct  SampleJumpButton_t2074145434  : public MonoBehaviour_t1158329972
{
public:
	// Utage.AdvEngine SampleJumpButton::engine
	AdvEngine_t1176753927 * ___engine_2;
	// System.String SampleJumpButton::scenarioLabel
	String_t* ___scenarioLabel_3;

public:
	inline static int32_t get_offset_of_engine_2() { return static_cast<int32_t>(offsetof(SampleJumpButton_t2074145434, ___engine_2)); }
	inline AdvEngine_t1176753927 * get_engine_2() const { return ___engine_2; }
	inline AdvEngine_t1176753927 ** get_address_of_engine_2() { return &___engine_2; }
	inline void set_engine_2(AdvEngine_t1176753927 * value)
	{
		___engine_2 = value;
		Il2CppCodeGenWriteBarrier((&___engine_2), value);
	}

	inline static int32_t get_offset_of_scenarioLabel_3() { return static_cast<int32_t>(offsetof(SampleJumpButton_t2074145434, ___scenarioLabel_3)); }
	inline String_t* get_scenarioLabel_3() const { return ___scenarioLabel_3; }
	inline String_t** get_address_of_scenarioLabel_3() { return &___scenarioLabel_3; }
	inline void set_scenarioLabel_3(String_t* value)
	{
		___scenarioLabel_3 = value;
		Il2CppCodeGenWriteBarrier((&___scenarioLabel_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SAMPLEJUMPBUTTON_T2074145434_H
#ifndef SAMPLELOADERROR_T2227685100_H
#define SAMPLELOADERROR_T2227685100_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SampleLoadError
struct  SampleLoadError_t2227685100  : public MonoBehaviour_t1158329972
{
public:
	// System.Boolean SampleLoadError::isWaitingRetry
	bool ___isWaitingRetry_2;

public:
	inline static int32_t get_offset_of_isWaitingRetry_2() { return static_cast<int32_t>(offsetof(SampleLoadError_t2227685100, ___isWaitingRetry_2)); }
	inline bool get_isWaitingRetry_2() const { return ___isWaitingRetry_2; }
	inline bool* get_address_of_isWaitingRetry_2() { return &___isWaitingRetry_2; }
	inline void set_isWaitingRetry_2(bool value)
	{
		___isWaitingRetry_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SAMPLELOADERROR_T2227685100_H
#ifndef SAMPLEPAGEEVENT_T2849724703_H
#define SAMPLEPAGEEVENT_T2849724703_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SamplePageEvent
struct  SamplePageEvent_t2849724703  : public MonoBehaviour_t1158329972
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SAMPLEPAGEEVENT_T2849724703_H
#ifndef SAMPLEPARAM_T32919575_H
#define SAMPLEPARAM_T32919575_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SampleParam
struct  SampleParam_t32919575  : public MonoBehaviour_t1158329972
{
public:
	// Utage.AdvEngine SampleParam::engine
	AdvEngine_t1176753927 * ___engine_2;

public:
	inline static int32_t get_offset_of_engine_2() { return static_cast<int32_t>(offsetof(SampleParam_t32919575, ___engine_2)); }
	inline AdvEngine_t1176753927 * get_engine_2() const { return ___engine_2; }
	inline AdvEngine_t1176753927 ** get_address_of_engine_2() { return &___engine_2; }
	inline void set_engine_2(AdvEngine_t1176753927 * value)
	{
		___engine_2 = value;
		Il2CppCodeGenWriteBarrier((&___engine_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SAMPLEPARAM_T32919575_H
#ifndef SAMPLESENDMESSAGEBYNAME_T109769559_H
#define SAMPLESENDMESSAGEBYNAME_T109769559_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SampleSendMessageByName
struct  SampleSendMessageByName_t109769559  : public MonoBehaviour_t1158329972
{
public:
	// System.Boolean SampleSendMessageByName::isAdOpen
	bool ___isAdOpen_2;

public:
	inline static int32_t get_offset_of_isAdOpen_2() { return static_cast<int32_t>(offsetof(SampleSendMessageByName_t109769559, ___isAdOpen_2)); }
	inline bool get_isAdOpen_2() const { return ___isAdOpen_2; }
	inline bool* get_address_of_isAdOpen_2() { return &___isAdOpen_2; }
	inline void set_isAdOpen_2(bool value)
	{
		___isAdOpen_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SAMPLESENDMESSAGEBYNAME_T109769559_H
#ifndef UNITYCHANFACEUPDATESIMPLE_T1648928779_H
#define UNITYCHANFACEUPDATESIMPLE_T1648928779_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.UnityChanFaceUpdateSimple
struct  UnityChanFaceUpdateSimple_t1648928779  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.AnimationClip[] Utage.UnityChanFaceUpdateSimple::animations
	AnimationClipU5BU5D_t3936083219* ___animations_2;

public:
	inline static int32_t get_offset_of_animations_2() { return static_cast<int32_t>(offsetof(UnityChanFaceUpdateSimple_t1648928779, ___animations_2)); }
	inline AnimationClipU5BU5D_t3936083219* get_animations_2() const { return ___animations_2; }
	inline AnimationClipU5BU5D_t3936083219** get_address_of_animations_2() { return &___animations_2; }
	inline void set_animations_2(AnimationClipU5BU5D_t3936083219* value)
	{
		___animations_2 = value;
		Il2CppCodeGenWriteBarrier((&___animations_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYCHANFACEUPDATESIMPLE_T1648928779_H
#ifndef UTAGERECIEVEMESSAGEFROMADVCOMANND_T4227868061_H
#define UTAGERECIEVEMESSAGEFROMADVCOMANND_T4227868061_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UtageRecieveMessageFromAdvComannd
struct  UtageRecieveMessageFromAdvComannd_t4227868061  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.GameObject UtageRecieveMessageFromAdvComannd::root3d
	GameObject_t1756533147 * ___root3d_2;
	// UnityEngine.GameObject UtageRecieveMessageFromAdvComannd::rotateRoot
	GameObject_t1756533147 * ___rotateRoot_3;
	// UnityEngine.GameObject[] UtageRecieveMessageFromAdvComannd::models
	GameObjectU5BU5D_t3057952154* ___models_4;
	// System.Single UtageRecieveMessageFromAdvComannd::rotSpped
	float ___rotSpped_5;

public:
	inline static int32_t get_offset_of_root3d_2() { return static_cast<int32_t>(offsetof(UtageRecieveMessageFromAdvComannd_t4227868061, ___root3d_2)); }
	inline GameObject_t1756533147 * get_root3d_2() const { return ___root3d_2; }
	inline GameObject_t1756533147 ** get_address_of_root3d_2() { return &___root3d_2; }
	inline void set_root3d_2(GameObject_t1756533147 * value)
	{
		___root3d_2 = value;
		Il2CppCodeGenWriteBarrier((&___root3d_2), value);
	}

	inline static int32_t get_offset_of_rotateRoot_3() { return static_cast<int32_t>(offsetof(UtageRecieveMessageFromAdvComannd_t4227868061, ___rotateRoot_3)); }
	inline GameObject_t1756533147 * get_rotateRoot_3() const { return ___rotateRoot_3; }
	inline GameObject_t1756533147 ** get_address_of_rotateRoot_3() { return &___rotateRoot_3; }
	inline void set_rotateRoot_3(GameObject_t1756533147 * value)
	{
		___rotateRoot_3 = value;
		Il2CppCodeGenWriteBarrier((&___rotateRoot_3), value);
	}

	inline static int32_t get_offset_of_models_4() { return static_cast<int32_t>(offsetof(UtageRecieveMessageFromAdvComannd_t4227868061, ___models_4)); }
	inline GameObjectU5BU5D_t3057952154* get_models_4() const { return ___models_4; }
	inline GameObjectU5BU5D_t3057952154** get_address_of_models_4() { return &___models_4; }
	inline void set_models_4(GameObjectU5BU5D_t3057952154* value)
	{
		___models_4 = value;
		Il2CppCodeGenWriteBarrier((&___models_4), value);
	}

	inline static int32_t get_offset_of_rotSpped_5() { return static_cast<int32_t>(offsetof(UtageRecieveMessageFromAdvComannd_t4227868061, ___rotSpped_5)); }
	inline float get_rotSpped_5() const { return ___rotSpped_5; }
	inline float* get_address_of_rotSpped_5() { return &___rotSpped_5; }
	inline void set_rotSpped_5(float value)
	{
		___rotSpped_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UTAGERECIEVEMESSAGEFROMADVCOMANND_T4227868061_H
#ifndef UTAGERECIEVEMESSAGESAMPLE_T743719262_H
#define UTAGERECIEVEMESSAGESAMPLE_T743719262_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UtageRecieveMessageSample
struct  UtageRecieveMessageSample_t743719262  : public MonoBehaviour_t1158329972
{
public:
	// Utage.AdvEngine UtageRecieveMessageSample::engine
	AdvEngine_t1176753927 * ___engine_2;
	// UnityEngine.UI.InputField UtageRecieveMessageSample::inputFiled
	InputField_t1631627530 * ___inputFiled_3;

public:
	inline static int32_t get_offset_of_engine_2() { return static_cast<int32_t>(offsetof(UtageRecieveMessageSample_t743719262, ___engine_2)); }
	inline AdvEngine_t1176753927 * get_engine_2() const { return ___engine_2; }
	inline AdvEngine_t1176753927 ** get_address_of_engine_2() { return &___engine_2; }
	inline void set_engine_2(AdvEngine_t1176753927 * value)
	{
		___engine_2 = value;
		Il2CppCodeGenWriteBarrier((&___engine_2), value);
	}

	inline static int32_t get_offset_of_inputFiled_3() { return static_cast<int32_t>(offsetof(UtageRecieveMessageSample_t743719262, ___inputFiled_3)); }
	inline InputField_t1631627530 * get_inputFiled_3() const { return ___inputFiled_3; }
	inline InputField_t1631627530 ** get_address_of_inputFiled_3() { return &___inputFiled_3; }
	inline void set_inputFiled_3(InputField_t1631627530 * value)
	{
		___inputFiled_3 = value;
		Il2CppCodeGenWriteBarrier((&___inputFiled_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UTAGERECIEVEMESSAGESAMPLE_T743719262_H
#ifndef AUTOBLINK_T2157783575_H
#define AUTOBLINK_T2157783575_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityChan.AutoBlink
struct  AutoBlink_t2157783575  : public MonoBehaviour_t1158329972
{
public:
	// System.Boolean UnityChan.AutoBlink::isActive
	bool ___isActive_2;
	// UnityEngine.SkinnedMeshRenderer UnityChan.AutoBlink::ref_SMR_EYE_DEF
	SkinnedMeshRenderer_t4220419316 * ___ref_SMR_EYE_DEF_3;
	// UnityEngine.SkinnedMeshRenderer UnityChan.AutoBlink::ref_SMR_EL_DEF
	SkinnedMeshRenderer_t4220419316 * ___ref_SMR_EL_DEF_4;
	// System.Single UnityChan.AutoBlink::ratio_Close
	float ___ratio_Close_5;
	// System.Single UnityChan.AutoBlink::ratio_HalfClose
	float ___ratio_HalfClose_6;
	// System.Single UnityChan.AutoBlink::ratio_Open
	float ___ratio_Open_7;
	// System.Boolean UnityChan.AutoBlink::timerStarted
	bool ___timerStarted_8;
	// System.Boolean UnityChan.AutoBlink::isBlink
	bool ___isBlink_9;
	// System.Single UnityChan.AutoBlink::timeBlink
	float ___timeBlink_10;
	// System.Single UnityChan.AutoBlink::timeRemining
	float ___timeRemining_11;
	// System.Single UnityChan.AutoBlink::threshold
	float ___threshold_12;
	// System.Single UnityChan.AutoBlink::interval
	float ___interval_13;
	// UnityChan.AutoBlink/Status UnityChan.AutoBlink::eyeStatus
	int32_t ___eyeStatus_14;

public:
	inline static int32_t get_offset_of_isActive_2() { return static_cast<int32_t>(offsetof(AutoBlink_t2157783575, ___isActive_2)); }
	inline bool get_isActive_2() const { return ___isActive_2; }
	inline bool* get_address_of_isActive_2() { return &___isActive_2; }
	inline void set_isActive_2(bool value)
	{
		___isActive_2 = value;
	}

	inline static int32_t get_offset_of_ref_SMR_EYE_DEF_3() { return static_cast<int32_t>(offsetof(AutoBlink_t2157783575, ___ref_SMR_EYE_DEF_3)); }
	inline SkinnedMeshRenderer_t4220419316 * get_ref_SMR_EYE_DEF_3() const { return ___ref_SMR_EYE_DEF_3; }
	inline SkinnedMeshRenderer_t4220419316 ** get_address_of_ref_SMR_EYE_DEF_3() { return &___ref_SMR_EYE_DEF_3; }
	inline void set_ref_SMR_EYE_DEF_3(SkinnedMeshRenderer_t4220419316 * value)
	{
		___ref_SMR_EYE_DEF_3 = value;
		Il2CppCodeGenWriteBarrier((&___ref_SMR_EYE_DEF_3), value);
	}

	inline static int32_t get_offset_of_ref_SMR_EL_DEF_4() { return static_cast<int32_t>(offsetof(AutoBlink_t2157783575, ___ref_SMR_EL_DEF_4)); }
	inline SkinnedMeshRenderer_t4220419316 * get_ref_SMR_EL_DEF_4() const { return ___ref_SMR_EL_DEF_4; }
	inline SkinnedMeshRenderer_t4220419316 ** get_address_of_ref_SMR_EL_DEF_4() { return &___ref_SMR_EL_DEF_4; }
	inline void set_ref_SMR_EL_DEF_4(SkinnedMeshRenderer_t4220419316 * value)
	{
		___ref_SMR_EL_DEF_4 = value;
		Il2CppCodeGenWriteBarrier((&___ref_SMR_EL_DEF_4), value);
	}

	inline static int32_t get_offset_of_ratio_Close_5() { return static_cast<int32_t>(offsetof(AutoBlink_t2157783575, ___ratio_Close_5)); }
	inline float get_ratio_Close_5() const { return ___ratio_Close_5; }
	inline float* get_address_of_ratio_Close_5() { return &___ratio_Close_5; }
	inline void set_ratio_Close_5(float value)
	{
		___ratio_Close_5 = value;
	}

	inline static int32_t get_offset_of_ratio_HalfClose_6() { return static_cast<int32_t>(offsetof(AutoBlink_t2157783575, ___ratio_HalfClose_6)); }
	inline float get_ratio_HalfClose_6() const { return ___ratio_HalfClose_6; }
	inline float* get_address_of_ratio_HalfClose_6() { return &___ratio_HalfClose_6; }
	inline void set_ratio_HalfClose_6(float value)
	{
		___ratio_HalfClose_6 = value;
	}

	inline static int32_t get_offset_of_ratio_Open_7() { return static_cast<int32_t>(offsetof(AutoBlink_t2157783575, ___ratio_Open_7)); }
	inline float get_ratio_Open_7() const { return ___ratio_Open_7; }
	inline float* get_address_of_ratio_Open_7() { return &___ratio_Open_7; }
	inline void set_ratio_Open_7(float value)
	{
		___ratio_Open_7 = value;
	}

	inline static int32_t get_offset_of_timerStarted_8() { return static_cast<int32_t>(offsetof(AutoBlink_t2157783575, ___timerStarted_8)); }
	inline bool get_timerStarted_8() const { return ___timerStarted_8; }
	inline bool* get_address_of_timerStarted_8() { return &___timerStarted_8; }
	inline void set_timerStarted_8(bool value)
	{
		___timerStarted_8 = value;
	}

	inline static int32_t get_offset_of_isBlink_9() { return static_cast<int32_t>(offsetof(AutoBlink_t2157783575, ___isBlink_9)); }
	inline bool get_isBlink_9() const { return ___isBlink_9; }
	inline bool* get_address_of_isBlink_9() { return &___isBlink_9; }
	inline void set_isBlink_9(bool value)
	{
		___isBlink_9 = value;
	}

	inline static int32_t get_offset_of_timeBlink_10() { return static_cast<int32_t>(offsetof(AutoBlink_t2157783575, ___timeBlink_10)); }
	inline float get_timeBlink_10() const { return ___timeBlink_10; }
	inline float* get_address_of_timeBlink_10() { return &___timeBlink_10; }
	inline void set_timeBlink_10(float value)
	{
		___timeBlink_10 = value;
	}

	inline static int32_t get_offset_of_timeRemining_11() { return static_cast<int32_t>(offsetof(AutoBlink_t2157783575, ___timeRemining_11)); }
	inline float get_timeRemining_11() const { return ___timeRemining_11; }
	inline float* get_address_of_timeRemining_11() { return &___timeRemining_11; }
	inline void set_timeRemining_11(float value)
	{
		___timeRemining_11 = value;
	}

	inline static int32_t get_offset_of_threshold_12() { return static_cast<int32_t>(offsetof(AutoBlink_t2157783575, ___threshold_12)); }
	inline float get_threshold_12() const { return ___threshold_12; }
	inline float* get_address_of_threshold_12() { return &___threshold_12; }
	inline void set_threshold_12(float value)
	{
		___threshold_12 = value;
	}

	inline static int32_t get_offset_of_interval_13() { return static_cast<int32_t>(offsetof(AutoBlink_t2157783575, ___interval_13)); }
	inline float get_interval_13() const { return ___interval_13; }
	inline float* get_address_of_interval_13() { return &___interval_13; }
	inline void set_interval_13(float value)
	{
		___interval_13 = value;
	}

	inline static int32_t get_offset_of_eyeStatus_14() { return static_cast<int32_t>(offsetof(AutoBlink_t2157783575, ___eyeStatus_14)); }
	inline int32_t get_eyeStatus_14() const { return ___eyeStatus_14; }
	inline int32_t* get_address_of_eyeStatus_14() { return &___eyeStatus_14; }
	inline void set_eyeStatus_14(int32_t value)
	{
		___eyeStatus_14 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AUTOBLINK_T2157783575_H
#ifndef SAMPLECUSTOMCOMMAND_T1819922052_H
#define SAMPLECUSTOMCOMMAND_T1819922052_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.SampleCustomCommand
struct  SampleCustomCommand_t1819922052  : public AdvCustomCommandManager_t178514092
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SAMPLECUSTOMCOMMAND_T1819922052_H
#ifndef SAMPLECUSTOMCOMMANDPARAM_T1524232535_H
#define SAMPLECUSTOMCOMMANDPARAM_T1524232535_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.SampleCustomCommandParam
struct  SampleCustomCommandParam_t1524232535  : public AdvCustomCommandManager_t178514092
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SAMPLECUSTOMCOMMANDPARAM_T1524232535_H





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2100 = { sizeof (AsyncToggler_t306543624), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2100[2] = 
{
	AsyncToggler_t306543624::get_offset_of_EnableAsync_2(),
	AsyncToggler_t306543624::get_offset_of_U3CLastEnableSyncU3Ek__BackingField_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2101 = { sizeof (FpsCounter_t2606976575), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2101[2] = 
{
	FpsCounter_t2606976575::get_offset_of_FpsUi_2(),
	FpsCounter_t2606976575::get_offset_of_U3CDeltaTimeU3Ek__BackingField_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2102 = { sizeof (ModelSpawner_t2910617673), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2102[3] = 
{
	ModelSpawner_t2910617673::get_offset_of_ModelPrefab_2(),
	ModelSpawner_t2910617673::get_offset_of_ModelCountUi_3(),
	ModelSpawner_t2910617673::get_offset_of_U3CInstancesU3Ek__BackingField_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2103 = { sizeof (Billboarder_t1346851594), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2103[1] = 
{
	Billboarder_t1346851594::get_offset_of_CameraToFace_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2104 = { sizeof (MaskTexturePreview_t295566249), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2104[1] = 
{
	MaskTexturePreview_t295566249::get_offset_of_MaskTexture_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2105 = { sizeof (RaycastHitDisplay_t1519583226), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2105[4] = 
{
	RaycastHitDisplay_t1519583226::get_offset_of_Model_2(),
	RaycastHitDisplay_t1519583226::get_offset_of_ResultsText_3(),
	RaycastHitDisplay_t1519583226::get_offset_of_U3CRaycasterU3Ek__BackingField_4(),
	RaycastHitDisplay_t1519583226::get_offset_of_U3CResultsU3Ek__BackingField_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2106 = { sizeof (FadeSample_t2284280808), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2107 = { sizeof (FadeManager_t2205509615), -1, sizeof(FadeManager_t2205509615_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2107[5] = 
{
	FadeManager_t2205509615_StaticFields::get_offset_of_instance_2(),
	FadeManager_t2205509615::get_offset_of_DebugMode_3(),
	FadeManager_t2205509615::get_offset_of_fadeAlpha_4(),
	FadeManager_t2205509615::get_offset_of_isFading_5(),
	FadeManager_t2205509615::get_offset_of_fadeColor_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2108 = { sizeof (U3CTransSceneU3Ec__Iterator0_t3372958951), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2108[7] = 
{
	U3CTransSceneU3Ec__Iterator0_t3372958951::get_offset_of_U3CtimeU3E__0_0(),
	U3CTransSceneU3Ec__Iterator0_t3372958951::get_offset_of_interval_1(),
	U3CTransSceneU3Ec__Iterator0_t3372958951::get_offset_of_scene_2(),
	U3CTransSceneU3Ec__Iterator0_t3372958951::get_offset_of_U24this_3(),
	U3CTransSceneU3Ec__Iterator0_t3372958951::get_offset_of_U24current_4(),
	U3CTransSceneU3Ec__Iterator0_t3372958951::get_offset_of_U24disposing_5(),
	U3CTransSceneU3Ec__Iterator0_t3372958951::get_offset_of_U24PC_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2109 = { sizeof (JinsBridgeScript_t4122526304), -1, sizeof(JinsBridgeScript_t4122526304_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2109[8] = 
{
	0,
	JinsBridgeScript_t4122526304_StaticFields::get_offset_of_U3CInstanceU3Ek__BackingField_3(),
	JinsBridgeScript_t4122526304::get_offset_of_U3CdereU3Ek__BackingField_4(),
	JinsBridgeScript_t4122526304::get_offset_of_U3CkyoroU3Ek__BackingField_5(),
	JinsBridgeScript_t4122526304::get_offset_of_U3CrelaxU3Ek__BackingField_6(),
	JinsBridgeScript_t4122526304::get_offset_of_U3CperipheralConnectedU3Ek__BackingField_7(),
	JinsBridgeScript_t4122526304::get_offset_of_sceneName_8(),
	JinsBridgeScript_t4122526304::get_offset_of_bgmVolume_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2110 = { sizeof (SelectScript_t398995261), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2110[16] = 
{
	SelectScript_t398995261::get_offset_of_startButton_2(),
	SelectScript_t398995261::get_offset_of_backButton_3(),
	SelectScript_t398995261::get_offset_of_slider_4(),
	SelectScript_t398995261::get_offset_of_girl_5(),
	SelectScript_t398995261::get_offset_of_boy_6(),
	SelectScript_t398995261::get_offset_of_girlSelectSe_7(),
	SelectScript_t398995261::get_offset_of_girlStartSe_8(),
	SelectScript_t398995261::get_offset_of_boySelectSe_9(),
	SelectScript_t398995261::get_offset_of_boyStartSe_10(),
	SelectScript_t398995261::get_offset_of_girlAudio_11(),
	SelectScript_t398995261::get_offset_of_boyAudio_12(),
	SelectScript_t398995261::get_offset_of_toSettings_13(),
	SelectScript_t398995261::get_offset_of_chara_14(),
	SelectScript_t398995261::get_offset_of_touchEnabled_15(),
	SelectScript_t398995261::get_offset_of_girlPos_16(),
	SelectScript_t398995261::get_offset_of_boyPos_17(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2111 = { sizeof (Type_t820837084)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2111[4] = 
{
	Type_t820837084::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2112 = { sizeof (U3CShowGirlU3Ec__Iterator0_t648192688), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2112[5] = 
{
	U3CShowGirlU3Ec__Iterator0_t648192688::get_offset_of_U3CsecU3E__0_0(),
	U3CShowGirlU3Ec__Iterator0_t648192688::get_offset_of_U24this_1(),
	U3CShowGirlU3Ec__Iterator0_t648192688::get_offset_of_U24current_2(),
	U3CShowGirlU3Ec__Iterator0_t648192688::get_offset_of_U24disposing_3(),
	U3CShowGirlU3Ec__Iterator0_t648192688::get_offset_of_U24PC_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2113 = { sizeof (U3CShowBoyU3Ec__Iterator1_t3879423835), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2113[5] = 
{
	U3CShowBoyU3Ec__Iterator1_t3879423835::get_offset_of_U3CsecU3E__0_0(),
	U3CShowBoyU3Ec__Iterator1_t3879423835::get_offset_of_U24this_1(),
	U3CShowBoyU3Ec__Iterator1_t3879423835::get_offset_of_U24current_2(),
	U3CShowBoyU3Ec__Iterator1_t3879423835::get_offset_of_U24disposing_3(),
	U3CShowBoyU3Ec__Iterator1_t3879423835::get_offset_of_U24PC_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2114 = { sizeof (U3CShowStayU3Ec__Iterator2_t2760854739), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2114[5] = 
{
	U3CShowStayU3Ec__Iterator2_t2760854739::get_offset_of_U3CsecU3E__0_0(),
	U3CShowStayU3Ec__Iterator2_t2760854739::get_offset_of_U24this_1(),
	U3CShowStayU3Ec__Iterator2_t2760854739::get_offset_of_U24current_2(),
	U3CShowStayU3Ec__Iterator2_t2760854739::get_offset_of_U24disposing_3(),
	U3CShowStayU3Ec__Iterator2_t2760854739::get_offset_of_U24PC_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2115 = { sizeof (U3CgotoNextSceneU3Ec__Iterator3_t82667986), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2115[3] = 
{
	U3CgotoNextSceneU3Ec__Iterator3_t82667986::get_offset_of_U24current_0(),
	U3CgotoNextSceneU3Ec__Iterator3_t82667986::get_offset_of_U24disposing_1(),
	U3CgotoNextSceneU3Ec__Iterator3_t82667986::get_offset_of_U24PC_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2116 = { sizeof (SettingsObjectScript_t2863710617), -1, sizeof(SettingsObjectScript_t2863710617_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2116[10] = 
{
	SettingsObjectScript_t2863710617_StaticFields::get_offset_of_callerObject_2(),
	SettingsObjectScript_t2863710617::get_offset_of_peripheralConnected_3(),
	SettingsObjectScript_t2863710617::get_offset_of_scrollContent_4(),
	SettingsObjectScript_t2863710617::get_offset_of_scrollCell_5(),
	SettingsObjectScript_t2863710617::get_offset_of_scanButtonLabel_6(),
	SettingsObjectScript_t2863710617::get_offset_of_scanStatusLabel_7(),
	SettingsObjectScript_t2863710617::get_offset_of_logLabel_8(),
	SettingsObjectScript_t2863710617::get_offset_of_bgmVolumeSlider_9(),
	SettingsObjectScript_t2863710617::get_offset_of_bgVolumeLabel_10(),
	SettingsObjectScript_t2863710617::get_offset_of_isScanning_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2117 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2117[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2118 = { sizeof (TalkScript_t915888587), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2118[14] = 
{
	TalkScript_t915888587::get_offset_of_engine_2(),
	TalkScript_t915888587::get_offset_of_dereValue_3(),
	TalkScript_t915888587::get_offset_of_kyoroValue_4(),
	TalkScript_t915888587::get_offset_of_relaxValue_5(),
	TalkScript_t915888587::get_offset_of_dereGauge_6(),
	TalkScript_t915888587::get_offset_of_kyoroGauge_7(),
	TalkScript_t915888587::get_offset_of_relaxGauge_8(),
	TalkScript_t915888587::get_offset_of_config_9(),
	0,
	0,
	0,
	TalkScript_t915888587::get_offset_of_c1_13(),
	TalkScript_t915888587::get_offset_of_c2_14(),
	TalkScript_t915888587::get_offset_of_c3_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2119 = { sizeof (Eval_t1707499340)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2119[4] = 
{
	Eval_t1707499340::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2120 = { sizeof (U3CCoTalkU3Ec__Iterator0_t2384567481), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2120[5] = 
{
	U3CCoTalkU3Ec__Iterator0_t2384567481::get_offset_of_U3CscenarioLabelU3E__0_0(),
	U3CCoTalkU3Ec__Iterator0_t2384567481::get_offset_of_U24this_1(),
	U3CCoTalkU3Ec__Iterator0_t2384567481::get_offset_of_U24current_2(),
	U3CCoTalkU3Ec__Iterator0_t2384567481::get_offset_of_U24disposing_3(),
	U3CCoTalkU3Ec__Iterator0_t2384567481::get_offset_of_U24PC_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2121 = { sizeof (ToSettingScript_t3239318692), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2121[1] = 
{
	ToSettingScript_t3239318692::get_offset_of_disableObjectList_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2122 = { sizeof (TransitionScript_t3831648224), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2122[3] = 
{
	TransitionScript_t3831648224::get_offset_of_toSceneName_2(),
	TransitionScript_t3831648224::get_offset_of_waitTime_3(),
	TransitionScript_t3831648224::get_offset_of_fadeTime_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2123 = { sizeof (U3CTransitionU3Ec__Iterator0_t3085833305), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2123[4] = 
{
	U3CTransitionU3Ec__Iterator0_t3085833305::get_offset_of_U24this_0(),
	U3CTransitionU3Ec__Iterator0_t3085833305::get_offset_of_U24current_1(),
	U3CTransitionU3Ec__Iterator0_t3085833305::get_offset_of_U24disposing_2(),
	U3CTransitionU3Ec__Iterator0_t3085833305::get_offset_of_U24PC_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2124 = { sizeof (AutoStartGame_t2128369979), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2124[3] = 
{
	AutoStartGame_t2128369979::get_offset_of_title_2(),
	AutoStartGame_t2128369979::get_offset_of_timeLimit_3(),
	AutoStartGame_t2128369979::get_offset_of_time_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2125 = { sizeof (SampleChapterTitle_t998764237), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2125[6] = 
{
	SampleChapterTitle_t998764237::get_offset_of_engine_2(),
	SampleChapterTitle_t998764237::get_offset_of_title_3(),
	SampleChapterTitle_t998764237::get_offset_of_loadWait_4(),
	SampleChapterTitle_t998764237::get_offset_of_mainGame_5(),
	SampleChapterTitle_t998764237::get_offset_of_chapterUrlList_6(),
	SampleChapterTitle_t998764237::get_offset_of_startLabel_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2126 = { sizeof (U3CLoadChaptersAsyncU3Ec__Iterator0_t3084809847), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2126[8] = 
{
	U3CLoadChaptersAsyncU3Ec__Iterator0_t3084809847::get_offset_of_U3CiU3E__1_0(),
	U3CLoadChaptersAsyncU3Ec__Iterator0_t3084809847::get_offset_of_chapterIndex_1(),
	U3CLoadChaptersAsyncU3Ec__Iterator0_t3084809847::get_offset_of_U3CurlU3E__2_2(),
	U3CLoadChaptersAsyncU3Ec__Iterator0_t3084809847::get_offset_of_U24this_3(),
	U3CLoadChaptersAsyncU3Ec__Iterator0_t3084809847::get_offset_of_U24current_4(),
	U3CLoadChaptersAsyncU3Ec__Iterator0_t3084809847::get_offset_of_U24disposing_5(),
	U3CLoadChaptersAsyncU3Ec__Iterator0_t3084809847::get_offset_of_U24PC_6(),
	U3CLoadChaptersAsyncU3Ec__Iterator0_t3084809847::get_offset_of_U24locvar0_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2127 = { sizeof (U3CLoadChaptersAsyncU3Ec__AnonStorey1_t2271407108), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2127[2] = 
{
	U3CLoadChaptersAsyncU3Ec__AnonStorey1_t2271407108::get_offset_of_chapterIndex_0(),
	U3CLoadChaptersAsyncU3Ec__AnonStorey1_t2271407108::get_offset_of_U3CU3Ef__refU240_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2128 = { sizeof (SampleCharacterGrayOutControllerRecieveMessage_t2772336740), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2129 = { sizeof (SampleChatLog_t3834130706), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2129[5] = 
{
	SampleChatLog_t3834130706::get_offset_of_engine_2(),
	SampleChatLog_t3834130706::get_offset_of_itemPrefab_3(),
	SampleChatLog_t3834130706::get_offset_of_targetRoot_4(),
	SampleChatLog_t3834130706::get_offset_of_maxLog_5(),
	SampleChatLog_t3834130706::get_offset_of_logs_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2130 = { sizeof (SampleChatLogItem_t3744651319), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2130[6] = 
{
	SampleChatLogItem_t3744651319::get_offset_of_text_2(),
	SampleChatLogItem_t3744651319::get_offset_of_characterName_3(),
	SampleChatLogItem_t3744651319::get_offset_of_soundIcon_4(),
	SampleChatLogItem_t3744651319::get_offset_of_button_5(),
	SampleChatLogItem_t3744651319::get_offset_of_isMultiTextInPage_6(),
	SampleChatLogItem_t3744651319::get_offset_of_data_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2131 = { sizeof (U3COnInitDataU3Ec__AnonStorey1_t3015087187), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2131[2] = 
{
	U3COnInitDataU3Ec__AnonStorey1_t3015087187::get_offset_of_data_0(),
	U3COnInitDataU3Ec__AnonStorey1_t3015087187::get_offset_of_U24this_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2132 = { sizeof (U3CCoPlayVoiceU3Ec__Iterator0_t886521713), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2132[8] = 
{
	U3CCoPlayVoiceU3Ec__Iterator0_t886521713::get_offset_of_voiceFileName_0(),
	U3CCoPlayVoiceU3Ec__Iterator0_t886521713::get_offset_of_U3CfileU3E__0_1(),
	U3CCoPlayVoiceU3Ec__Iterator0_t886521713::get_offset_of_U3CmanagerU3E__0_2(),
	U3CCoPlayVoiceU3Ec__Iterator0_t886521713::get_offset_of_characterLabel_3(),
	U3CCoPlayVoiceU3Ec__Iterator0_t886521713::get_offset_of_U24this_4(),
	U3CCoPlayVoiceU3Ec__Iterator0_t886521713::get_offset_of_U24current_5(),
	U3CCoPlayVoiceU3Ec__Iterator0_t886521713::get_offset_of_U24disposing_6(),
	U3CCoPlayVoiceU3Ec__Iterator0_t886521713::get_offset_of_U24PC_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2133 = { sizeof (SampleCheckUnity56Newer_t3908425173), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2133[2] = 
{
	SampleCheckUnity56Newer_t3908425173::get_offset_of_engine_2(),
	SampleCheckUnity56Newer_t3908425173::get_offset_of_U3CIsInitU3Ek__BackingField_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2134 = { sizeof (SampleCustomCommand_t1819922052), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2135 = { sizeof (SampleAdvCommandDebugLog_t679789377), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2135[1] = 
{
	SampleAdvCommandDebugLog_t679789377::get_offset_of_log_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2136 = { sizeof (SampleCustomAdvCommandText_t3879361722), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2136[1] = 
{
	SampleCustomAdvCommandText_t3879361722::get_offset_of_log_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2137 = { sizeof (SampleCustomCommandParam_t1524232535), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2138 = { sizeof (AdvCommandParamTblKeyCount_t533749743), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2138[2] = 
{
	AdvCommandParamTblKeyCount_t533749743::get_offset_of_paramName_7(),
	AdvCommandParamTblKeyCount_t533749743::get_offset_of_tblName_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2139 = { sizeof (AdvCommandParamTblKeyCount2_t1053315553), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2139[4] = 
{
	AdvCommandParamTblKeyCount2_t1053315553::get_offset_of_paramName_7(),
	AdvCommandParamTblKeyCount2_t1053315553::get_offset_of_tblName_8(),
	AdvCommandParamTblKeyCount2_t1053315553::get_offset_of_valueName_9(),
	AdvCommandParamTblKeyCount2_t1053315553::get_offset_of_countValue_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2140 = { sizeof (SampleJumpButton_t2074145434), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2140[2] = 
{
	SampleJumpButton_t2074145434::get_offset_of_engine_2(),
	SampleJumpButton_t2074145434::get_offset_of_scenarioLabel_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2141 = { sizeof (SampleLoadError_t2227685100), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2141[1] = 
{
	SampleLoadError_t2227685100::get_offset_of_isWaitingRetry_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2142 = { sizeof (U3CCustomCallbackFileLoadErrorU3Ec__AnonStorey1_t203029879), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2142[2] = 
{
	U3CCustomCallbackFileLoadErrorU3Ec__AnonStorey1_t203029879::get_offset_of_file_0(),
	U3CCustomCallbackFileLoadErrorU3Ec__AnonStorey1_t203029879::get_offset_of_U24this_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2143 = { sizeof (U3CCoWaitRetryU3Ec__Iterator0_t3805080565), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2143[5] = 
{
	U3CCoWaitRetryU3Ec__Iterator0_t3805080565::get_offset_of_file_0(),
	U3CCoWaitRetryU3Ec__Iterator0_t3805080565::get_offset_of_U24this_1(),
	U3CCoWaitRetryU3Ec__Iterator0_t3805080565::get_offset_of_U24current_2(),
	U3CCoWaitRetryU3Ec__Iterator0_t3805080565::get_offset_of_U24disposing_3(),
	U3CCoWaitRetryU3Ec__Iterator0_t3805080565::get_offset_of_U24PC_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2144 = { sizeof (SamplePageEvent_t2849724703), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2145 = { sizeof (SampleParam_t32919575), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2145[1] = 
{
	SampleParam_t32919575::get_offset_of_engine_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2146 = { sizeof (SampleSendMessageByName_t109769559), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2146[2] = 
{
	SampleSendMessageByName_t109769559::get_offset_of_isAdOpen_2(),
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2147 = { sizeof (U3CCoWaitU3Ec__Iterator0_t71556710), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2147[5] = 
{
	U3CCoWaitU3Ec__Iterator0_t71556710::get_offset_of_command_0(),
	U3CCoWaitU3Ec__Iterator0_t71556710::get_offset_of_U3CtimeU3E__0_1(),
	U3CCoWaitU3Ec__Iterator0_t71556710::get_offset_of_U24current_2(),
	U3CCoWaitU3Ec__Iterator0_t71556710::get_offset_of_U24disposing_3(),
	U3CCoWaitU3Ec__Iterator0_t71556710::get_offset_of_U24PC_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2148 = { sizeof (UnityChanFaceUpdateSimple_t1648928779), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2148[1] = 
{
	UnityChanFaceUpdateSimple_t1648928779::get_offset_of_animations_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2149 = { sizeof (UtageRecieveMessageFromAdvComannd_t4227868061), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2149[4] = 
{
	UtageRecieveMessageFromAdvComannd_t4227868061::get_offset_of_root3d_2(),
	UtageRecieveMessageFromAdvComannd_t4227868061::get_offset_of_rotateRoot_3(),
	UtageRecieveMessageFromAdvComannd_t4227868061::get_offset_of_models_4(),
	UtageRecieveMessageFromAdvComannd_t4227868061::get_offset_of_rotSpped_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2150 = { sizeof (U3CCoRotate3DU3Ec__Iterator0_t4279666499), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2150[4] = 
{
	U3CCoRotate3DU3Ec__Iterator0_t4279666499::get_offset_of_U24this_0(),
	U3CCoRotate3DU3Ec__Iterator0_t4279666499::get_offset_of_U24current_1(),
	U3CCoRotate3DU3Ec__Iterator0_t4279666499::get_offset_of_U24disposing_2(),
	U3CCoRotate3DU3Ec__Iterator0_t4279666499::get_offset_of_U24PC_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2151 = { sizeof (U3CFindModelU3Ec__AnonStorey1_t390684174), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2151[1] = 
{
	U3CFindModelU3Ec__AnonStorey1_t390684174::get_offset_of_name_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2152 = { sizeof (UtageRecieveMessageSample_t743719262), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2152[2] = 
{
	UtageRecieveMessageSample_t743719262::get_offset_of_engine_2(),
	UtageRecieveMessageSample_t743719262::get_offset_of_inputFiled_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2153 = { sizeof (U3CInputFileldU3Ec__AnonStorey1_t3579112721), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2153[2] = 
{
	U3CInputFileldU3Ec__AnonStorey1_t3579112721::get_offset_of_command_0(),
	U3CInputFileldU3Ec__AnonStorey1_t3579112721::get_offset_of_U24this_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2154 = { sizeof (U3CCoAutoLoadSubU3Ec__Iterator0_t963927007), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2154[4] = 
{
	U3CCoAutoLoadSubU3Ec__Iterator0_t963927007::get_offset_of_U24this_0(),
	U3CCoAutoLoadSubU3Ec__Iterator0_t963927007::get_offset_of_U24current_1(),
	U3CCoAutoLoadSubU3Ec__Iterator0_t963927007::get_offset_of_U24disposing_2(),
	U3CCoAutoLoadSubU3Ec__Iterator0_t963927007::get_offset_of_U24PC_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2155 = { sizeof (AutoBlink_t2157783575), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2155[13] = 
{
	AutoBlink_t2157783575::get_offset_of_isActive_2(),
	AutoBlink_t2157783575::get_offset_of_ref_SMR_EYE_DEF_3(),
	AutoBlink_t2157783575::get_offset_of_ref_SMR_EL_DEF_4(),
	AutoBlink_t2157783575::get_offset_of_ratio_Close_5(),
	AutoBlink_t2157783575::get_offset_of_ratio_HalfClose_6(),
	AutoBlink_t2157783575::get_offset_of_ratio_Open_7(),
	AutoBlink_t2157783575::get_offset_of_timerStarted_8(),
	AutoBlink_t2157783575::get_offset_of_isBlink_9(),
	AutoBlink_t2157783575::get_offset_of_timeBlink_10(),
	AutoBlink_t2157783575::get_offset_of_timeRemining_11(),
	AutoBlink_t2157783575::get_offset_of_threshold_12(),
	AutoBlink_t2157783575::get_offset_of_interval_13(),
	AutoBlink_t2157783575::get_offset_of_eyeStatus_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2156 = { sizeof (Status_t1544330929)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2156[4] = 
{
	Status_t1544330929::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2157 = { sizeof (U3CRandomChangeU3Ec__Iterator0_t1939068071), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2157[5] = 
{
	U3CRandomChangeU3Ec__Iterator0_t1939068071::get_offset_of_U3C_seedU3E__1_0(),
	U3CRandomChangeU3Ec__Iterator0_t1939068071::get_offset_of_U24this_1(),
	U3CRandomChangeU3Ec__Iterator0_t1939068071::get_offset_of_U24current_2(),
	U3CRandomChangeU3Ec__Iterator0_t1939068071::get_offset_of_U24disposing_3(),
	U3CRandomChangeU3Ec__Iterator0_t1939068071::get_offset_of_U24PC_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2158 = { sizeof (AutoBlinkforSD_t2101017347), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2158[15] = 
{
	AutoBlinkforSD_t2101017347::get_offset_of_isActive_2(),
	AutoBlinkforSD_t2101017347::get_offset_of_ref_face_3(),
	AutoBlinkforSD_t2101017347::get_offset_of_ratio_Close_4(),
	AutoBlinkforSD_t2101017347::get_offset_of_ratio_HalfClose_5(),
	AutoBlinkforSD_t2101017347::get_offset_of_index_EYE_blk_6(),
	AutoBlinkforSD_t2101017347::get_offset_of_index_EYE_sml_7(),
	AutoBlinkforSD_t2101017347::get_offset_of_index_EYE_dmg_8(),
	AutoBlinkforSD_t2101017347::get_offset_of_ratio_Open_9(),
	AutoBlinkforSD_t2101017347::get_offset_of_timerStarted_10(),
	AutoBlinkforSD_t2101017347::get_offset_of_isBlink_11(),
	AutoBlinkforSD_t2101017347::get_offset_of_timeBlink_12(),
	AutoBlinkforSD_t2101017347::get_offset_of_timeRemining_13(),
	AutoBlinkforSD_t2101017347::get_offset_of_threshold_14(),
	AutoBlinkforSD_t2101017347::get_offset_of_interval_15(),
	AutoBlinkforSD_t2101017347::get_offset_of_eyeStatus_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2159 = { sizeof (Status_t3601570505)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2159[4] = 
{
	Status_t3601570505::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2160 = { sizeof (U3CRandomChangeU3Ec__Iterator0_t923176863), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2160[5] = 
{
	U3CRandomChangeU3Ec__Iterator0_t923176863::get_offset_of_U3C_seedU3E__1_0(),
	U3CRandomChangeU3Ec__Iterator0_t923176863::get_offset_of_U24this_1(),
	U3CRandomChangeU3Ec__Iterator0_t923176863::get_offset_of_U24current_2(),
	U3CRandomChangeU3Ec__Iterator0_t923176863::get_offset_of_U24disposing_3(),
	U3CRandomChangeU3Ec__Iterator0_t923176863::get_offset_of_U24PC_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2161 = { sizeof (MouseButtonDown_t87707387)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2161[4] = 
{
	MouseButtonDown_t87707387::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2162 = { sizeof (CameraController_t231271813), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2162[4] = 
{
	CameraController_t231271813::get_offset_of_focus_2(),
	CameraController_t231271813::get_offset_of_focusObj_3(),
	CameraController_t231271813::get_offset_of_showInstWindow_4(),
	CameraController_t231271813::get_offset_of_oldPos_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2163 = { sizeof (FaceUpdate_t2436360706), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2163[6] = 
{
	FaceUpdate_t2436360706::get_offset_of_animations_2(),
	FaceUpdate_t2436360706::get_offset_of_anim_3(),
	FaceUpdate_t2436360706::get_offset_of_delayWeight_4(),
	FaceUpdate_t2436360706::get_offset_of_isKeepFace_5(),
	FaceUpdate_t2436360706::get_offset_of_isGUI_6(),
	FaceUpdate_t2436360706::get_offset_of_current_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2164 = { sizeof (IdleChanger_t2567672470), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2164[7] = 
{
	IdleChanger_t2567672470::get_offset_of_anim_2(),
	IdleChanger_t2567672470::get_offset_of_currentState_3(),
	IdleChanger_t2567672470::get_offset_of_previousState_4(),
	IdleChanger_t2567672470::get_offset_of__random_5(),
	IdleChanger_t2567672470::get_offset_of__threshold_6(),
	IdleChanger_t2567672470::get_offset_of__interval_7(),
	IdleChanger_t2567672470::get_offset_of_isGUI_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2165 = { sizeof (U3CRandomChangeU3Ec__Iterator0_t3991869552), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2165[4] = 
{
	U3CRandomChangeU3Ec__Iterator0_t3991869552::get_offset_of_U24this_0(),
	U3CRandomChangeU3Ec__Iterator0_t3991869552::get_offset_of_U24current_1(),
	U3CRandomChangeU3Ec__Iterator0_t3991869552::get_offset_of_U24disposing_2(),
	U3CRandomChangeU3Ec__Iterator0_t3991869552::get_offset_of_U24PC_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2166 = { sizeof (IKCtrlRightHand_t3240365066), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2166[4] = 
{
	IKCtrlRightHand_t3240365066::get_offset_of_anim_2(),
	IKCtrlRightHand_t3240365066::get_offset_of_targetObj_3(),
	IKCtrlRightHand_t3240365066::get_offset_of_isIkActive_4(),
	IKCtrlRightHand_t3240365066::get_offset_of_mixWeight_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2167 = { sizeof (IKLookAt_t1884959938), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2167[10] = 
{
	IKLookAt_t1884959938::get_offset_of_avator_2(),
	IKLookAt_t1884959938::get_offset_of_target_3(),
	IKLookAt_t1884959938::get_offset_of_ikActive_4(),
	IKLookAt_t1884959938::get_offset_of_lookAtObj_5(),
	IKLookAt_t1884959938::get_offset_of_lookAtWeight_6(),
	IKLookAt_t1884959938::get_offset_of_bodyWeight_7(),
	IKLookAt_t1884959938::get_offset_of_headWeight_8(),
	IKLookAt_t1884959938::get_offset_of_eyesWeight_9(),
	IKLookAt_t1884959938::get_offset_of_clampWeight_10(),
	IKLookAt_t1884959938::get_offset_of_isGUI_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2168 = { sizeof (RandomWind_t3585812647), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2168[8] = 
{
	RandomWind_t3585812647::get_offset_of_springBones_2(),
	RandomWind_t3585812647::get_offset_of_isWindActive_3(),
	RandomWind_t3585812647::get_offset_of_isMinus_4(),
	RandomWind_t3585812647::get_offset_of_threshold_5(),
	RandomWind_t3585812647::get_offset_of_interval_6(),
	RandomWind_t3585812647::get_offset_of_windPower_7(),
	RandomWind_t3585812647::get_offset_of_gravity_8(),
	RandomWind_t3585812647::get_offset_of_isGUI_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2169 = { sizeof (U3CRandomChangeU3Ec__Iterator0_t2567771009), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2169[5] = 
{
	U3CRandomChangeU3Ec__Iterator0_t2567771009::get_offset_of_U3C_seedU3E__1_0(),
	U3CRandomChangeU3Ec__Iterator0_t2567771009::get_offset_of_U24this_1(),
	U3CRandomChangeU3Ec__Iterator0_t2567771009::get_offset_of_U24current_2(),
	U3CRandomChangeU3Ec__Iterator0_t2567771009::get_offset_of_U24disposing_3(),
	U3CRandomChangeU3Ec__Iterator0_t2567771009::get_offset_of_U24PC_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2170 = { sizeof (SpringBone_t3136191221), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2170[17] = 
{
	SpringBone_t3136191221::get_offset_of_child_2(),
	SpringBone_t3136191221::get_offset_of_boneAxis_3(),
	SpringBone_t3136191221::get_offset_of_radius_4(),
	SpringBone_t3136191221::get_offset_of_isUseEachBoneForceSettings_5(),
	SpringBone_t3136191221::get_offset_of_stiffnessForce_6(),
	SpringBone_t3136191221::get_offset_of_dragForce_7(),
	SpringBone_t3136191221::get_offset_of_springForce_8(),
	SpringBone_t3136191221::get_offset_of_colliders_9(),
	SpringBone_t3136191221::get_offset_of_debug_10(),
	SpringBone_t3136191221::get_offset_of_threshold_11(),
	SpringBone_t3136191221::get_offset_of_springLength_12(),
	SpringBone_t3136191221::get_offset_of_localRotation_13(),
	SpringBone_t3136191221::get_offset_of_trs_14(),
	SpringBone_t3136191221::get_offset_of_currTipPos_15(),
	SpringBone_t3136191221::get_offset_of_prevTipPos_16(),
	SpringBone_t3136191221::get_offset_of_org_17(),
	SpringBone_t3136191221::get_offset_of_managerRef_18(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2171 = { sizeof (SpringCollider_t3514471471), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2171[1] = 
{
	SpringCollider_t3514471471::get_offset_of_radius_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2172 = { sizeof (SpringManager_t4191634164), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2172[6] = 
{
	SpringManager_t4191634164::get_offset_of_dynamicRatio_2(),
	SpringManager_t4191634164::get_offset_of_stiffnessForce_3(),
	SpringManager_t4191634164::get_offset_of_stiffnessCurve_4(),
	SpringManager_t4191634164::get_offset_of_dragForce_5(),
	SpringManager_t4191634164::get_offset_of_dragCurve_6(),
	SpringManager_t4191634164::get_offset_of_springBones_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2173 = { sizeof (ThirdPersonCamera_t1407538279), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2173[5] = 
{
	ThirdPersonCamera_t1407538279::get_offset_of_smooth_2(),
	ThirdPersonCamera_t1407538279::get_offset_of_standardPos_3(),
	ThirdPersonCamera_t1407538279::get_offset_of_frontPos_4(),
	ThirdPersonCamera_t1407538279::get_offset_of_jumpPos_5(),
	ThirdPersonCamera_t1407538279::get_offset_of_bQuickSwitch_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2174 = { sizeof (UnityChanControlScriptWithRgidBody_t1891187877), -1, sizeof(UnityChanControlScriptWithRgidBody_t1891187877_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2174[20] = 
{
	UnityChanControlScriptWithRgidBody_t1891187877::get_offset_of_animSpeed_2(),
	UnityChanControlScriptWithRgidBody_t1891187877::get_offset_of_lookSmoother_3(),
	UnityChanControlScriptWithRgidBody_t1891187877::get_offset_of_useCurves_4(),
	UnityChanControlScriptWithRgidBody_t1891187877::get_offset_of_useCurvesHeight_5(),
	UnityChanControlScriptWithRgidBody_t1891187877::get_offset_of_forwardSpeed_6(),
	UnityChanControlScriptWithRgidBody_t1891187877::get_offset_of_backwardSpeed_7(),
	UnityChanControlScriptWithRgidBody_t1891187877::get_offset_of_rotateSpeed_8(),
	UnityChanControlScriptWithRgidBody_t1891187877::get_offset_of_jumpPower_9(),
	UnityChanControlScriptWithRgidBody_t1891187877::get_offset_of_col_10(),
	UnityChanControlScriptWithRgidBody_t1891187877::get_offset_of_rb_11(),
	UnityChanControlScriptWithRgidBody_t1891187877::get_offset_of_velocity_12(),
	UnityChanControlScriptWithRgidBody_t1891187877::get_offset_of_orgColHight_13(),
	UnityChanControlScriptWithRgidBody_t1891187877::get_offset_of_orgVectColCenter_14(),
	UnityChanControlScriptWithRgidBody_t1891187877::get_offset_of_anim_15(),
	UnityChanControlScriptWithRgidBody_t1891187877::get_offset_of_currentBaseState_16(),
	UnityChanControlScriptWithRgidBody_t1891187877::get_offset_of_cameraObject_17(),
	UnityChanControlScriptWithRgidBody_t1891187877_StaticFields::get_offset_of_idleState_18(),
	UnityChanControlScriptWithRgidBody_t1891187877_StaticFields::get_offset_of_locoState_19(),
	UnityChanControlScriptWithRgidBody_t1891187877_StaticFields::get_offset_of_jumpState_20(),
	UnityChanControlScriptWithRgidBody_t1891187877_StaticFields::get_offset_of_restState_21(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2175 = { sizeof (DrawerTest_t1088500511), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2175[3] = 
{
	DrawerTest_t1088500511::get_offset_of_helpBox_2(),
	DrawerTest_t1088500511::get_offset_of_decoratorTest_3(),
	DrawerTest_t1088500511::get_offset_of_isOverridePropertyDrawEditable_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2176 = { sizeof (DecoratorTest_t435098709), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2176[13] = 
{
	DecoratorTest_t435098709::get_offset_of_helpBox_0(),
	DecoratorTest_t435098709::get_offset_of_hide_1(),
	DecoratorTest_t435098709::get_offset_of_notEditable_2(),
	DecoratorTest_t435098709::get_offset_of_flags_3(),
	DecoratorTest_t435098709::get_offset_of_lmitEnum_4(),
	DecoratorTest_t435098709::get_offset_of_stringPopup_5(),
	DecoratorTest_t435098709::get_offset_of_stringPopupFunction_6(),
	DecoratorTest_t435098709::get_offset_of_pushButton_7(),
	DecoratorTest_t435098709::get_offset_of_addButton_8(),
	DecoratorTest_t435098709::get_offset_of_path_9(),
	DecoratorTest_t435098709::get_offset_of_intervalTime_10(),
	DecoratorTest_t435098709::get_offset_of_intervalTimeInt_11(),
	DecoratorTest_t435098709::get_offset_of_overridePropertyDraw_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2177 = { sizeof (Flags_t1052878605)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2177[4] = 
{
	Flags_t1052878605::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2178 = { sizeof (LimitEnum_t42509660)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2178[4] = 
{
	LimitEnum_t42509660::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2179 = { sizeof (SampleCustomAssetBundleLoad_t3658166813), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2179[4] = 
{
	SampleCustomAssetBundleLoad_t3658166813::get_offset_of_startScenario_2(),
	SampleCustomAssetBundleLoad_t3658166813::get_offset_of_engine_3(),
	SampleCustomAssetBundleLoad_t3658166813::get_offset_of_assetBundleList_4(),
	SampleCustomAssetBundleLoad_t3658166813::get_offset_of_U3CScenariosU3Ek__BackingField_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2180 = { sizeof (SampleAssetBundleVersionInfo_t1314599396), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2180[3] = 
{
	SampleAssetBundleVersionInfo_t1314599396::get_offset_of_resourcePath_0(),
	SampleAssetBundleVersionInfo_t1314599396::get_offset_of_url_1(),
	SampleAssetBundleVersionInfo_t1314599396::get_offset_of_version_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2181 = { sizeof (U3CLoadEngineAsyncU3Ec__Iterator0_t2926810271), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2181[5] = 
{
	U3CLoadEngineAsyncU3Ec__Iterator0_t2926810271::get_offset_of_U24locvar0_0(),
	U3CLoadEngineAsyncU3Ec__Iterator0_t2926810271::get_offset_of_U24this_1(),
	U3CLoadEngineAsyncU3Ec__Iterator0_t2926810271::get_offset_of_U24current_2(),
	U3CLoadEngineAsyncU3Ec__Iterator0_t2926810271::get_offset_of_U24disposing_3(),
	U3CLoadEngineAsyncU3Ec__Iterator0_t2926810271::get_offset_of_U24PC_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2182 = { sizeof (U3CLoadScenariosAsyncU3Ec__Iterator1_t1642848913), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2182[7] = 
{
	U3CLoadScenariosAsyncU3Ec__Iterator1_t1642848913::get_offset_of_url_0(),
	U3CLoadScenariosAsyncU3Ec__Iterator1_t1642848913::get_offset_of_U3CfileU3E__0_1(),
	U3CLoadScenariosAsyncU3Ec__Iterator1_t1642848913::get_offset_of_U3CscenariosU3E__0_2(),
	U3CLoadScenariosAsyncU3Ec__Iterator1_t1642848913::get_offset_of_U24this_3(),
	U3CLoadScenariosAsyncU3Ec__Iterator1_t1642848913::get_offset_of_U24current_4(),
	U3CLoadScenariosAsyncU3Ec__Iterator1_t1642848913::get_offset_of_U24disposing_5(),
	U3CLoadScenariosAsyncU3Ec__Iterator1_t1642848913::get_offset_of_U24PC_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2183 = { sizeof (U3CCoPlayEngineU3Ec__Iterator2_t2048207881), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2183[4] = 
{
	U3CCoPlayEngineU3Ec__Iterator2_t2048207881::get_offset_of_U24this_0(),
	U3CCoPlayEngineU3Ec__Iterator2_t2048207881::get_offset_of_U24current_1(),
	U3CCoPlayEngineU3Ec__Iterator2_t2048207881::get_offset_of_U24disposing_2(),
	U3CCoPlayEngineU3Ec__Iterator2_t2048207881::get_offset_of_U24PC_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2184 = { sizeof (SampleTips_t2223945158), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2185 = { sizeof (AdvEvent_t4160079193), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2186 = { sizeof (AdvEngine_t1176753927), -1, sizeof(AdvEngine_t1176753927_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2186[30] = 
{
	AdvEngine_t1176753927::get_offset_of_startScenarioLabel_2(),
	AdvEngine_t1176753927::get_offset_of_dataManager_3(),
	AdvEngine_t1176753927::get_offset_of_scenarioPlayer_4(),
	AdvEngine_t1176753927::get_offset_of_page_5(),
	AdvEngine_t1176753927::get_offset_of_selectionManager_6(),
	AdvEngine_t1176753927::get_offset_of_messageWindowManager_7(),
	AdvEngine_t1176753927::get_offset_of_backlogManager_8(),
	AdvEngine_t1176753927::get_offset_of_config_9(),
	AdvEngine_t1176753927::get_offset_of_systemSaveData_10(),
	AdvEngine_t1176753927::get_offset_of_saveManager_11(),
	AdvEngine_t1176753927::get_offset_of_graphicManager_12(),
	AdvEngine_t1176753927::get_offset_of_effectManager_13(),
	AdvEngine_t1176753927::get_offset_of_uiManager_14(),
	AdvEngine_t1176753927::get_offset_of_soundManager_15(),
	AdvEngine_t1176753927::get_offset_of_cameraManager_16(),
	AdvEngine_t1176753927::get_offset_of_param_17(),
	AdvEngine_t1176753927::get_offset_of_bootAsync_18(),
	AdvEngine_t1176753927::get_offset_of_isStopSoundOnStart_19(),
	AdvEngine_t1176753927::get_offset_of_isStopSoundOnEnd_20(),
	AdvEngine_t1176753927::get_offset_of_customCommandManagerList_21(),
	AdvEngine_t1176753927::get_offset_of_onPreInit_22(),
	AdvEngine_t1176753927::get_offset_of_onOpenDialog_23(),
	AdvEngine_t1176753927::get_offset_of_onPageTextChange_24(),
	AdvEngine_t1176753927::get_offset_of_OnClear_25(),
	AdvEngine_t1176753927::get_offset_of_onChangeLanguage_26(),
	AdvEngine_t1176753927::get_offset_of_isWaitBootLoading_27(),
	AdvEngine_t1176753927::get_offset_of_isStarted_28(),
	AdvEngine_t1176753927::get_offset_of_isSceneGallery_29(),
	AdvEngine_t1176753927_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_30(),
	AdvEngine_t1176753927_StaticFields::get_offset_of_U3CU3Ef__amU24cache1_31(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2187 = { sizeof (U3CCoBootFromExportDataU3Ec__Iterator0_t2686201055), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2187[6] = 
{
	U3CCoBootFromExportDataU3Ec__Iterator0_t2686201055::get_offset_of_scenarios_0(),
	U3CCoBootFromExportDataU3Ec__Iterator0_t2686201055::get_offset_of_resourceDir_1(),
	U3CCoBootFromExportDataU3Ec__Iterator0_t2686201055::get_offset_of_U24this_2(),
	U3CCoBootFromExportDataU3Ec__Iterator0_t2686201055::get_offset_of_U24current_3(),
	U3CCoBootFromExportDataU3Ec__Iterator0_t2686201055::get_offset_of_U24disposing_4(),
	U3CCoBootFromExportDataU3Ec__Iterator0_t2686201055::get_offset_of_U24PC_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2188 = { sizeof (U3CExitsChapterU3Ec__AnonStorey5_t3858776566), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2188[1] = 
{
	U3CExitsChapterU3Ec__AnonStorey5_t3858776566::get_offset_of_chapterAssetName_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2189 = { sizeof (U3CLoadChapterAsyncU3Ec__Iterator1_t2472554227), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2189[7] = 
{
	U3CLoadChapterAsyncU3Ec__Iterator1_t2472554227::get_offset_of_url_0(),
	U3CLoadChapterAsyncU3Ec__Iterator1_t2472554227::get_offset_of_U3CfileU3E__0_1(),
	U3CLoadChapterAsyncU3Ec__Iterator1_t2472554227::get_offset_of_U3CchapterU3E__0_2(),
	U3CLoadChapterAsyncU3Ec__Iterator1_t2472554227::get_offset_of_U24this_3(),
	U3CLoadChapterAsyncU3Ec__Iterator1_t2472554227::get_offset_of_U24current_4(),
	U3CLoadChapterAsyncU3Ec__Iterator1_t2472554227::get_offset_of_U24disposing_5(),
	U3CLoadChapterAsyncU3Ec__Iterator1_t2472554227::get_offset_of_U24PC_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2190 = { sizeof (U3CCoStartGameSubU3Ec__Iterator2_t4101911903), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2190[5] = 
{
	U3CCoStartGameSubU3Ec__Iterator2_t4101911903::get_offset_of_scenarioLabel_0(),
	U3CCoStartGameSubU3Ec__Iterator2_t4101911903::get_offset_of_U24this_1(),
	U3CCoStartGameSubU3Ec__Iterator2_t4101911903::get_offset_of_U24current_2(),
	U3CCoStartGameSubU3Ec__Iterator2_t4101911903::get_offset_of_U24disposing_3(),
	U3CCoStartGameSubU3Ec__Iterator2_t4101911903::get_offset_of_U24PC_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2191 = { sizeof (U3CCoStartScenarioU3Ec__Iterator3_t3243498162), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2191[6] = 
{
	U3CCoStartScenarioU3Ec__Iterator3_t3243498162::get_offset_of_label_0(),
	U3CCoStartScenarioU3Ec__Iterator3_t3243498162::get_offset_of_page_1(),
	U3CCoStartScenarioU3Ec__Iterator3_t3243498162::get_offset_of_U24this_2(),
	U3CCoStartScenarioU3Ec__Iterator3_t3243498162::get_offset_of_U24current_3(),
	U3CCoStartScenarioU3Ec__Iterator3_t3243498162::get_offset_of_U24disposing_4(),
	U3CCoStartScenarioU3Ec__Iterator3_t3243498162::get_offset_of_U24PC_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2192 = { sizeof (U3CCoStartSaveDataU3Ec__Iterator4_t1384462154), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2192[5] = 
{
	U3CCoStartSaveDataU3Ec__Iterator4_t1384462154::get_offset_of_saveData_0(),
	U3CCoStartSaveDataU3Ec__Iterator4_t1384462154::get_offset_of_U24this_1(),
	U3CCoStartSaveDataU3Ec__Iterator4_t1384462154::get_offset_of_U24current_2(),
	U3CCoStartSaveDataU3Ec__Iterator4_t1384462154::get_offset_of_U24disposing_3(),
	U3CCoStartSaveDataU3Ec__Iterator4_t1384462154::get_offset_of_U24PC_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2193 = { sizeof (AdvEngineStarter_t330044300), -1, sizeof(AdvEngineStarter_t330044300_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2193[13] = 
{
	AdvEngineStarter_t330044300::get_offset_of_isLoadOnAwake_2(),
	AdvEngineStarter_t330044300::get_offset_of_isAutomaticPlay_3(),
	AdvEngineStarter_t330044300::get_offset_of_startScenario_4(),
	AdvEngineStarter_t330044300::get_offset_of_engine_5(),
	AdvEngineStarter_t330044300::get_offset_of_strageType_6(),
	AdvEngineStarter_t330044300::get_offset_of_scenarios_7(),
	AdvEngineStarter_t330044300::get_offset_of_rootResourceDir_8(),
	AdvEngineStarter_t330044300::get_offset_of_serverUrl_9(),
	AdvEngineStarter_t330044300::get_offset_of_scenariosName_10(),
	AdvEngineStarter_t330044300::get_offset_of_useChapter_11(),
	AdvEngineStarter_t330044300::get_offset_of_chapterNames_12(),
	AdvEngineStarter_t330044300::get_offset_of_U3CIsLoadStartU3Ek__BackingField_13(),
	AdvEngineStarter_t330044300_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2194 = { sizeof (StrageType_t3621906793)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2194[7] = 
{
	StrageType_t3621906793::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2195 = { sizeof (U3CLoadEngineAsyncU3Ec__Iterator0_t2663747088), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2195[5] = 
{
	U3CLoadEngineAsyncU3Ec__Iterator0_t2663747088::get_offset_of_onFailed_0(),
	U3CLoadEngineAsyncU3Ec__Iterator0_t2663747088::get_offset_of_U24this_1(),
	U3CLoadEngineAsyncU3Ec__Iterator0_t2663747088::get_offset_of_U24current_2(),
	U3CLoadEngineAsyncU3Ec__Iterator0_t2663747088::get_offset_of_U24disposing_3(),
	U3CLoadEngineAsyncU3Ec__Iterator0_t2663747088::get_offset_of_U24PC_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2196 = { sizeof (U3CLoadEngineAsyncFromCacheManifestU3Ec__Iterator1_t3219196200), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2196[5] = 
{
	U3CLoadEngineAsyncFromCacheManifestU3Ec__Iterator1_t3219196200::get_offset_of_onFailed_0(),
	U3CLoadEngineAsyncFromCacheManifestU3Ec__Iterator1_t3219196200::get_offset_of_U24this_1(),
	U3CLoadEngineAsyncFromCacheManifestU3Ec__Iterator1_t3219196200::get_offset_of_U24current_2(),
	U3CLoadEngineAsyncFromCacheManifestU3Ec__Iterator1_t3219196200::get_offset_of_U24disposing_3(),
	U3CLoadEngineAsyncFromCacheManifestU3Ec__Iterator1_t3219196200::get_offset_of_U24PC_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2197 = { sizeof (U3CLoadEngineAsyncSubU3Ec__Iterator2_t1722329282), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2197[7] = 
{
	U3CLoadEngineAsyncSubU3Ec__Iterator2_t1722329282::get_offset_of_loadManifestFromCache_0(),
	U3CLoadEngineAsyncSubU3Ec__Iterator2_t1722329282::get_offset_of_onFailed_1(),
	U3CLoadEngineAsyncSubU3Ec__Iterator2_t1722329282::get_offset_of_U24this_2(),
	U3CLoadEngineAsyncSubU3Ec__Iterator2_t1722329282::get_offset_of_U24current_3(),
	U3CLoadEngineAsyncSubU3Ec__Iterator2_t1722329282::get_offset_of_U24disposing_4(),
	U3CLoadEngineAsyncSubU3Ec__Iterator2_t1722329282::get_offset_of_U24PC_5(),
	U3CLoadEngineAsyncSubU3Ec__Iterator2_t1722329282::get_offset_of_U24locvar0_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2198 = { sizeof (U3CLoadEngineAsyncSubU3Ec__AnonStorey8_t684401054), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2198[2] = 
{
	U3CLoadEngineAsyncSubU3Ec__AnonStorey8_t684401054::get_offset_of_isFailed_0(),
	U3CLoadEngineAsyncSubU3Ec__AnonStorey8_t684401054::get_offset_of_U3CU3Ef__refU242_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2199 = { sizeof (U3CLoadEngineAsyncSubU3Ec__Iterator3_t1722329281), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2199[5] = 
{
	U3CLoadEngineAsyncSubU3Ec__Iterator3_t1722329281::get_offset_of_U3CneedsToLoadScenarioU3E__0_0(),
	U3CLoadEngineAsyncSubU3Ec__Iterator3_t1722329281::get_offset_of_U24this_1(),
	U3CLoadEngineAsyncSubU3Ec__Iterator3_t1722329281::get_offset_of_U24current_2(),
	U3CLoadEngineAsyncSubU3Ec__Iterator3_t1722329281::get_offset_of_U24disposing_3(),
	U3CLoadEngineAsyncSubU3Ec__Iterator3_t1722329281::get_offset_of_U24PC_4(),
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
