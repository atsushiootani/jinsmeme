﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "object-internals.h"

// System.Collections.Generic.List`1<Live2D.Cubism.Rendering.Masking.CubismMaskController/MasksMaskedsPair>
struct List_1_t474262102;
// UnityEngine.MaterialPropertyBlock
struct MaterialPropertyBlock_t3303648957;
// Live2D.Cubism.Rendering.CubismRenderer
struct CubismRenderer_t1960351136;
// UnityEngine.Material
struct Material_t193706927;
// Live2D.Cubism.Rendering.Masking.ICubismMaskTextureCommandSource
struct ICubismMaskTextureCommandSource_t2847927181;
// Live2D.Cubism.Framework.Physics.CubismPhysicsSubRig[]
struct CubismPhysicsSubRigU5BU5D_t3856368233;
// Live2D.Cubism.Framework.Physics.CubismPhysicsController
struct CubismPhysicsController_t2863219344;
// System.Boolean[]
struct BooleanU5BU5D_t3568034315;
// Live2D.Cubism.Rendering.Masking.ICubismMaskCommandSource
struct ICubismMaskCommandSource_t483560162;
// System.String[]
struct StringU5BU5D_t1642385972;
// Live2D.Cubism.Core.Unmanaged.CubismUnmanagedFloatArrayView
struct CubismUnmanagedFloatArrayView_t1153227075;
// System.Collections.Generic.Queue`1<Live2D.Cubism.Core.ICubismTask>
struct Queue_1_t3877289046;
// System.Threading.Thread
struct Thread_t241561612;
// System.Threading.ManualResetEvent
struct ManualResetEvent_t926074657;
// Live2D.Cubism.Core.CubismTaskQueue/CubismTaskHandler
struct CubismTaskHandler_t1959870214;
// System.Threading.ThreadStart
struct ThreadStart_t3437517264;
// System.String
struct String_t;
// Live2D.Cubism.Rendering.CubismRenderer[]
struct CubismRendererU5BU5D_t2721529825;
// System.Collections.Generic.List`1<Live2D.Cubism.Rendering.CubismRenderer>
struct List_1_t1329472268;
// Live2D.Cubism.Rendering.Masking.CubismMaskTile[]
struct CubismMaskTileU5BU5D_t1795731440;
// System.Single[]
struct SingleU5BU5D_t577127397;
// System.Void
struct Void_t1841601450;
// System.Char[]
struct CharU5BU5D_t1328083999;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// System.DelegateData
struct DelegateData_t1572802995;
// Live2D.Cubism.Core.CubismDrawable
struct CubismDrawable_t2709211313;
// Live2D.Cubism.Framework.Json.CubismMotion3Json/SerializableCurve[]
struct SerializableCurveU5BU5D_t1674681539;
// System.Collections.Generic.Dictionary`2<System.Single,Live2D.Cubism.Framework.Json.CubismMotion3Json/SegmentParser>
struct Dictionary_2_t1306250649;
// Live2D.Cubism.Framework.Json.CubismMotion3Json/SegmentParser
struct SegmentParser_t456729962;
// Live2D.Cubism.Core.Unmanaged.CubismUnmanagedParameters
struct CubismUnmanagedParameters_t2764188193;
// Live2D.Cubism.Core.Unmanaged.CubismUnmanagedParts
struct CubismUnmanagedParts_t1124036275;
// Live2D.Cubism.Core.Unmanaged.CubismUnmanagedDrawables
struct CubismUnmanagedDrawables_t771654660;
// Live2D.Cubism.Framework.Json.CubismModel3Json/LoadAssetAtPathHandler
struct LoadAssetAtPathHandler_t1714529023;
// Live2D.Cubism.Framework.Json.CubismModel3Json/SerializableGroup[]
struct SerializableGroupU5BU5D_t4046225562;
// UnityEngine.Texture2D[]
struct Texture2DU5BU5D_t2724090252;
// Live2D.Cubism.Framework.Json.CubismModel3Json/MaterialPicker
struct MaterialPicker_t1430258468;
// Live2D.Cubism.Framework.Json.CubismModel3Json/TexturePicker
struct TexturePicker_t1644688768;
// Live2D.Cubism.Framework.Json.CubismPhysics3Json/SerializableInput[]
struct SerializableInputU5BU5D_t2341458125;
// Live2D.Cubism.Framework.Json.CubismPhysics3Json/SerializableOutput[]
struct SerializableOutputU5BU5D_t1536831822;
// Live2D.Cubism.Framework.Json.CubismPhysics3Json/SerializableVertex[]
struct SerializableVertexU5BU5D_t2006479567;
// Live2D.Cubism.Rendering.Masking.CubismMaskProperties
struct CubismMaskProperties_t4215298192;
// Live2D.Cubism.Rendering.Masking.CubismMaskRenderer[]
struct CubismMaskRendererU5BU5D_t3366738177;
// Live2D.Cubism.Rendering.Masking.CubismMaskTexture
struct CubismMaskTexture_t949515734;
// Live2D.Cubism.Core.CubismParameter
struct CubismParameter_t3864677546;
// Live2D.Cubism.Framework.Physics.CubismPhysicsOutput/ValueGetter
struct ValueGetter_t1451636242;
// Live2D.Cubism.Framework.Physics.CubismPhysicsOutput/ScaleGetter
struct ScaleGetter_t3351733751;
// Live2D.Cubism.Framework.Physics.CubismPhysicsInput/NormalizedParameterValueGetter
struct NormalizedParameterValueGetter_t2024973737;
// Live2D.Cubism.Framework.Physics.CubismPhysicsInput[]
struct CubismPhysicsInputU5BU5D_t2102055817;
// Live2D.Cubism.Framework.Physics.CubismPhysicsOutput[]
struct CubismPhysicsOutputU5BU5D_t3543821074;
// Live2D.Cubism.Framework.Physics.CubismPhysicsParticle[]
struct CubismPhysicsParticleU5BU5D_t487538331;
// Live2D.Cubism.Framework.Physics.CubismPhysicsRig
struct CubismPhysicsRig_t856779234;
// Live2D.Cubism.Framework.Json.CubismModel3Json
struct CubismModel3Json_t1938818895;
// System.IAsyncResult
struct IAsyncResult_t1999651008;
// System.AsyncCallback
struct AsyncCallback_t163412349;
// System.Type
struct Type_t;
// UnityEngine.Texture2D
struct Texture2D_t3542995729;
// System.Collections.Generic.List`1<UnityEngine.Keyframe>
struct List_1_t818592472;
// Live2D.Cubism.Framework.Json.CubismPhysics3Json/SerializablePhysicsSettings[]
struct SerializablePhysicsSettingsU5BU5D_t3063461145;
// Live2D.Cubism.Rendering.Masking.CubismMaskTilePool
struct CubismMaskTilePool_t2959163307;
// UnityEngine.RenderTexture
struct RenderTexture_t2666733923;
// System.Collections.Generic.List`1<Live2D.Cubism.Rendering.Masking.CubismMaskTexture/SourcesItem>
struct List_1_t1928514839;
// Live2D.Cubism.Framework.HarmonicMotion.CubismHarmonicMotionParameter[]
struct CubismHarmonicMotionParameterU5BU5D_t1324606222;
// Live2D.Cubism.Core.CubismParameter[]
struct CubismParameterU5BU5D_t2848401775;
// UnityEngine.Object
struct Object_t1021602117;
// Live2D.Cubism.Framework.LookAt.ICubismLookTarget
struct ICubismLookTarget_t878774326;
// UnityEngine.Transform
struct Transform_t3275118058;
// Live2D.Cubism.Framework.LookAt.CubismLookParameter[]
struct CubismLookParameterU5BU5D_t1300568610;
// Live2D.Cubism.Framework.CubismEyeBlinkController
struct CubismEyeBlinkController_t4113316510;
// System.Collections.Generic.List`1<Live2D.Cubism.Rendering.Masking.ICubismMaskCommandSource>
struct List_1_t4147648590;
// UnityEngine.Rendering.CommandBuffer
struct CommandBuffer_t1204166949;
// UnityEngine.Mesh[]
struct MeshU5BU5D_t894826206;
// UnityEngine.MeshFilter
struct MeshFilter_t3026937449;
// UnityEngine.MeshRenderer
struct MeshRenderer_t1268241104;
// UnityEngine.Color[]
struct ColorU5BU5D_t672350442;
// UnityEngine.Camera
struct Camera_t189460977;
// Live2D.Cubism.Rendering.ICubismDrawOrderHandler
struct ICubismDrawOrderHandler_t3249096814;
// Live2D.Cubism.Rendering.ICubismOpacityHandler
struct ICubismOpacityHandler_t4232278755;
// UnityEngine.AudioSource
struct AudioSource_t1135106623;
// Live2D.Cubism.Framework.MouthMovement.CubismMouthController
struct CubismMouthController_t3330055306;
// Live2D.Cubism.Rendering.Masking.CubismMaskMaskedJunction[]
struct CubismMaskMaskedJunctionU5BU5D_t3161680575;

struct CubismMaskTile_t3996512637 ;
struct SerializableInput_t1668668964_marshaled_pinvoke;
struct SerializableOutput_t2803511543_marshaled_pinvoke;
struct SerializableVertex_t1882814154 ;
struct SerializableInput_t1668668964_marshaled_com;
struct SerializableOutput_t2803511543_marshaled_com;



#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
#ifndef OBJECTEXTENSIONMETHODS_T2018374342_H
#define OBJECTEXTENSIONMETHODS_T2018374342_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Live2D.Cubism.Framework.ObjectExtensionMethods
struct  ObjectExtensionMethods_t2018374342  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OBJECTEXTENSIONMETHODS_T2018374342_H
#ifndef VALUETYPE_T3507792607_H
#define VALUETYPE_T3507792607_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ValueType
struct  ValueType_t3507792607  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t3507792607_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t3507792607_marshaled_com
{
};
#endif // VALUETYPE_T3507792607_H
#ifndef ATTRIBUTE_T542643598_H
#define ATTRIBUTE_T542643598_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Attribute
struct  Attribute_t542643598  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ATTRIBUTE_T542643598_H
#ifndef MASKSMASKEDSPAIRS_T4062112651_H
#define MASKSMASKEDSPAIRS_T4062112651_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Live2D.Cubism.Rendering.Masking.CubismMaskController/MasksMaskedsPairs
struct  MasksMaskedsPairs_t4062112651  : public RuntimeObject
{
public:
	// System.Collections.Generic.List`1<Live2D.Cubism.Rendering.Masking.CubismMaskController/MasksMaskedsPair> Live2D.Cubism.Rendering.Masking.CubismMaskController/MasksMaskedsPairs::Entries
	List_1_t474262102 * ___Entries_0;

public:
	inline static int32_t get_offset_of_Entries_0() { return static_cast<int32_t>(offsetof(MasksMaskedsPairs_t4062112651, ___Entries_0)); }
	inline List_1_t474262102 * get_Entries_0() const { return ___Entries_0; }
	inline List_1_t474262102 ** get_address_of_Entries_0() { return &___Entries_0; }
	inline void set_Entries_0(List_1_t474262102 * value)
	{
		___Entries_0 = value;
		Il2CppCodeGenWriteBarrier((&___Entries_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MASKSMASKEDSPAIRS_T4062112651_H
#ifndef CUBISMBUILTINPICKERS_T3599713253_H
#define CUBISMBUILTINPICKERS_T3599713253_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Live2D.Cubism.Framework.Json.CubismBuiltinPickers
struct  CubismBuiltinPickers_t3599713253  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CUBISMBUILTINPICKERS_T3599713253_H
#ifndef CUBISMMASKRENDERER_T2642533120_H
#define CUBISMMASKRENDERER_T2642533120_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Live2D.Cubism.Rendering.Masking.CubismMaskRenderer
struct  CubismMaskRenderer_t2642533120  : public RuntimeObject
{
public:
	// UnityEngine.MaterialPropertyBlock Live2D.Cubism.Rendering.Masking.CubismMaskRenderer::<MaskProperties>k__BackingField
	MaterialPropertyBlock_t3303648957 * ___U3CMaskPropertiesU3Ek__BackingField_0;
	// Live2D.Cubism.Rendering.CubismRenderer Live2D.Cubism.Rendering.Masking.CubismMaskRenderer::<MainRenderer>k__BackingField
	CubismRenderer_t1960351136 * ___U3CMainRendererU3Ek__BackingField_1;
	// UnityEngine.Material Live2D.Cubism.Rendering.Masking.CubismMaskRenderer::<MaskMaterial>k__BackingField
	Material_t193706927 * ___U3CMaskMaterialU3Ek__BackingField_2;

public:
	inline static int32_t get_offset_of_U3CMaskPropertiesU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(CubismMaskRenderer_t2642533120, ___U3CMaskPropertiesU3Ek__BackingField_0)); }
	inline MaterialPropertyBlock_t3303648957 * get_U3CMaskPropertiesU3Ek__BackingField_0() const { return ___U3CMaskPropertiesU3Ek__BackingField_0; }
	inline MaterialPropertyBlock_t3303648957 ** get_address_of_U3CMaskPropertiesU3Ek__BackingField_0() { return &___U3CMaskPropertiesU3Ek__BackingField_0; }
	inline void set_U3CMaskPropertiesU3Ek__BackingField_0(MaterialPropertyBlock_t3303648957 * value)
	{
		___U3CMaskPropertiesU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CMaskPropertiesU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_U3CMainRendererU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(CubismMaskRenderer_t2642533120, ___U3CMainRendererU3Ek__BackingField_1)); }
	inline CubismRenderer_t1960351136 * get_U3CMainRendererU3Ek__BackingField_1() const { return ___U3CMainRendererU3Ek__BackingField_1; }
	inline CubismRenderer_t1960351136 ** get_address_of_U3CMainRendererU3Ek__BackingField_1() { return &___U3CMainRendererU3Ek__BackingField_1; }
	inline void set_U3CMainRendererU3Ek__BackingField_1(CubismRenderer_t1960351136 * value)
	{
		___U3CMainRendererU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CMainRendererU3Ek__BackingField_1), value);
	}

	inline static int32_t get_offset_of_U3CMaskMaterialU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(CubismMaskRenderer_t2642533120, ___U3CMaskMaterialU3Ek__BackingField_2)); }
	inline Material_t193706927 * get_U3CMaskMaterialU3Ek__BackingField_2() const { return ___U3CMaskMaterialU3Ek__BackingField_2; }
	inline Material_t193706927 ** get_address_of_U3CMaskMaterialU3Ek__BackingField_2() { return &___U3CMaskMaterialU3Ek__BackingField_2; }
	inline void set_U3CMaskMaterialU3Ek__BackingField_2(Material_t193706927 * value)
	{
		___U3CMaskMaterialU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CMaskMaterialU3Ek__BackingField_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CUBISMMASKRENDERER_T2642533120_H
#ifndef U3CREMOVESOURCEU3EC__ANONSTOREY1_T4159041179_H
#define U3CREMOVESOURCEU3EC__ANONSTOREY1_T4159041179_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Live2D.Cubism.Rendering.Masking.CubismMaskTexture/<RemoveSource>c__AnonStorey1
struct  U3CRemoveSourceU3Ec__AnonStorey1_t4159041179  : public RuntimeObject
{
public:
	// Live2D.Cubism.Rendering.Masking.ICubismMaskTextureCommandSource Live2D.Cubism.Rendering.Masking.CubismMaskTexture/<RemoveSource>c__AnonStorey1::source
	RuntimeObject* ___source_0;

public:
	inline static int32_t get_offset_of_source_0() { return static_cast<int32_t>(offsetof(U3CRemoveSourceU3Ec__AnonStorey1_t4159041179, ___source_0)); }
	inline RuntimeObject* get_source_0() const { return ___source_0; }
	inline RuntimeObject** get_address_of_source_0() { return &___source_0; }
	inline void set_source_0(RuntimeObject* value)
	{
		___source_0 = value;
		Il2CppCodeGenWriteBarrier((&___source_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CREMOVESOURCEU3EC__ANONSTOREY1_T4159041179_H
#ifndef CUBISMPHYSICSRIG_T856779234_H
#define CUBISMPHYSICSRIG_T856779234_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Live2D.Cubism.Framework.Physics.CubismPhysicsRig
struct  CubismPhysicsRig_t856779234  : public RuntimeObject
{
public:
	// Live2D.Cubism.Framework.Physics.CubismPhysicsSubRig[] Live2D.Cubism.Framework.Physics.CubismPhysicsRig::SubRigs
	CubismPhysicsSubRigU5BU5D_t3856368233* ___SubRigs_0;
	// Live2D.Cubism.Framework.Physics.CubismPhysicsController Live2D.Cubism.Framework.Physics.CubismPhysicsRig::<Controller>k__BackingField
	CubismPhysicsController_t2863219344 * ___U3CControllerU3Ek__BackingField_1;

public:
	inline static int32_t get_offset_of_SubRigs_0() { return static_cast<int32_t>(offsetof(CubismPhysicsRig_t856779234, ___SubRigs_0)); }
	inline CubismPhysicsSubRigU5BU5D_t3856368233* get_SubRigs_0() const { return ___SubRigs_0; }
	inline CubismPhysicsSubRigU5BU5D_t3856368233** get_address_of_SubRigs_0() { return &___SubRigs_0; }
	inline void set_SubRigs_0(CubismPhysicsSubRigU5BU5D_t3856368233* value)
	{
		___SubRigs_0 = value;
		Il2CppCodeGenWriteBarrier((&___SubRigs_0), value);
	}

	inline static int32_t get_offset_of_U3CControllerU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(CubismPhysicsRig_t856779234, ___U3CControllerU3Ek__BackingField_1)); }
	inline CubismPhysicsController_t2863219344 * get_U3CControllerU3Ek__BackingField_1() const { return ___U3CControllerU3Ek__BackingField_1; }
	inline CubismPhysicsController_t2863219344 ** get_address_of_U3CControllerU3Ek__BackingField_1() { return &___U3CControllerU3Ek__BackingField_1; }
	inline void set_U3CControllerU3Ek__BackingField_1(CubismPhysicsController_t2863219344 * value)
	{
		___U3CControllerU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CControllerU3Ek__BackingField_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CUBISMPHYSICSRIG_T856779234_H
#ifndef CUBISMMASKTILEPOOL_T2959163307_H
#define CUBISMMASKTILEPOOL_T2959163307_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Live2D.Cubism.Rendering.Masking.CubismMaskTilePool
struct  CubismMaskTilePool_t2959163307  : public RuntimeObject
{
public:
	// System.Int32 Live2D.Cubism.Rendering.Masking.CubismMaskTilePool::<Subdivisions>k__BackingField
	int32_t ___U3CSubdivisionsU3Ek__BackingField_0;
	// System.Boolean[] Live2D.Cubism.Rendering.Masking.CubismMaskTilePool::<Slots>k__BackingField
	BooleanU5BU5D_t3568034315* ___U3CSlotsU3Ek__BackingField_1;

public:
	inline static int32_t get_offset_of_U3CSubdivisionsU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(CubismMaskTilePool_t2959163307, ___U3CSubdivisionsU3Ek__BackingField_0)); }
	inline int32_t get_U3CSubdivisionsU3Ek__BackingField_0() const { return ___U3CSubdivisionsU3Ek__BackingField_0; }
	inline int32_t* get_address_of_U3CSubdivisionsU3Ek__BackingField_0() { return &___U3CSubdivisionsU3Ek__BackingField_0; }
	inline void set_U3CSubdivisionsU3Ek__BackingField_0(int32_t value)
	{
		___U3CSubdivisionsU3Ek__BackingField_0 = value;
	}

	inline static int32_t get_offset_of_U3CSlotsU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(CubismMaskTilePool_t2959163307, ___U3CSlotsU3Ek__BackingField_1)); }
	inline BooleanU5BU5D_t3568034315* get_U3CSlotsU3Ek__BackingField_1() const { return ___U3CSlotsU3Ek__BackingField_1; }
	inline BooleanU5BU5D_t3568034315** get_address_of_U3CSlotsU3Ek__BackingField_1() { return &___U3CSlotsU3Ek__BackingField_1; }
	inline void set_U3CSlotsU3Ek__BackingField_1(BooleanU5BU5D_t3568034315* value)
	{
		___U3CSlotsU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CSlotsU3Ek__BackingField_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CUBISMMASKTILEPOOL_T2959163307_H
#ifndef U3CADDSOURCEU3EC__ANONSTOREY0_T3314713517_H
#define U3CADDSOURCEU3EC__ANONSTOREY0_T3314713517_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Live2D.Cubism.Rendering.Masking.CubismMaskTexture/<AddSource>c__AnonStorey0
struct  U3CAddSourceU3Ec__AnonStorey0_t3314713517  : public RuntimeObject
{
public:
	// Live2D.Cubism.Rendering.Masking.ICubismMaskTextureCommandSource Live2D.Cubism.Rendering.Masking.CubismMaskTexture/<AddSource>c__AnonStorey0::source
	RuntimeObject* ___source_0;

public:
	inline static int32_t get_offset_of_source_0() { return static_cast<int32_t>(offsetof(U3CAddSourceU3Ec__AnonStorey0_t3314713517, ___source_0)); }
	inline RuntimeObject* get_source_0() const { return ___source_0; }
	inline RuntimeObject** get_address_of_source_0() { return &___source_0; }
	inline void set_source_0(RuntimeObject* value)
	{
		___source_0 = value;
		Il2CppCodeGenWriteBarrier((&___source_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CADDSOURCEU3EC__ANONSTOREY0_T3314713517_H
#ifndef CUBISMMASKRENDEREREXTENSIONMETHODS_T1294840099_H
#define CUBISMMASKRENDEREREXTENSIONMETHODS_T1294840099_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Live2D.Cubism.Rendering.Masking.CubismMaskRendererExtensionMethods
struct  CubismMaskRendererExtensionMethods_t1294840099  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CUBISMMASKRENDEREREXTENSIONMETHODS_T1294840099_H
#ifndef INTEXTENSIONMETHODS_T515585024_H
#define INTEXTENSIONMETHODS_T515585024_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Live2D.Cubism.Rendering.Masking.IntExtensionMethods
struct  IntExtensionMethods_t515585024  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTEXTENSIONMETHODS_T515585024_H
#ifndef U3CREMOVESOURCEU3EC__ANONSTOREY0_T1891205932_H
#define U3CREMOVESOURCEU3EC__ANONSTOREY0_T1891205932_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Live2D.Cubism.Rendering.Masking.CubismMaskCommandBuffer/<RemoveSource>c__AnonStorey0
struct  U3CRemoveSourceU3Ec__AnonStorey0_t1891205932  : public RuntimeObject
{
public:
	// Live2D.Cubism.Rendering.Masking.ICubismMaskCommandSource Live2D.Cubism.Rendering.Masking.CubismMaskCommandBuffer/<RemoveSource>c__AnonStorey0::source
	RuntimeObject* ___source_0;

public:
	inline static int32_t get_offset_of_source_0() { return static_cast<int32_t>(offsetof(U3CRemoveSourceU3Ec__AnonStorey0_t1891205932, ___source_0)); }
	inline RuntimeObject* get_source_0() const { return ___source_0; }
	inline RuntimeObject** get_address_of_source_0() { return &___source_0; }
	inline void set_source_0(RuntimeObject* value)
	{
		___source_0 = value;
		Il2CppCodeGenWriteBarrier((&___source_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CREMOVESOURCEU3EC__ANONSTOREY0_T1891205932_H
#ifndef ARRAYEXTENSIONMETHODS_T2010526758_H
#define ARRAYEXTENSIONMETHODS_T2010526758_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Live2D.Cubism.Rendering.ArrayExtensionMethods
struct  ArrayExtensionMethods_t2010526758  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ARRAYEXTENSIONMETHODS_T2010526758_H
#ifndef CUBISMPHYSICSMATH_T1623149510_H
#define CUBISMPHYSICSMATH_T1623149510_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Live2D.Cubism.Framework.Physics.CubismPhysicsMath
struct  CubismPhysicsMath_t1623149510  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CUBISMPHYSICSMATH_T1623149510_H
#ifndef COMPONENTEXTENSIONMETHODS_T3489337152_H
#define COMPONENTEXTENSIONMETHODS_T3489337152_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Live2D.Cubism.Framework.ComponentExtensionMethods
struct  ComponentExtensionMethods_t3489337152  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPONENTEXTENSIONMETHODS_T3489337152_H
#ifndef CUBISMUNMANAGEDPARTS_T1124036275_H
#define CUBISMUNMANAGEDPARTS_T1124036275_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Live2D.Cubism.Core.Unmanaged.CubismUnmanagedParts
struct  CubismUnmanagedParts_t1124036275  : public RuntimeObject
{
public:
	// System.Int32 Live2D.Cubism.Core.Unmanaged.CubismUnmanagedParts::<Count>k__BackingField
	int32_t ___U3CCountU3Ek__BackingField_0;
	// System.String[] Live2D.Cubism.Core.Unmanaged.CubismUnmanagedParts::<Ids>k__BackingField
	StringU5BU5D_t1642385972* ___U3CIdsU3Ek__BackingField_1;
	// Live2D.Cubism.Core.Unmanaged.CubismUnmanagedFloatArrayView Live2D.Cubism.Core.Unmanaged.CubismUnmanagedParts::<Opacities>k__BackingField
	CubismUnmanagedFloatArrayView_t1153227075 * ___U3COpacitiesU3Ek__BackingField_2;

public:
	inline static int32_t get_offset_of_U3CCountU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(CubismUnmanagedParts_t1124036275, ___U3CCountU3Ek__BackingField_0)); }
	inline int32_t get_U3CCountU3Ek__BackingField_0() const { return ___U3CCountU3Ek__BackingField_0; }
	inline int32_t* get_address_of_U3CCountU3Ek__BackingField_0() { return &___U3CCountU3Ek__BackingField_0; }
	inline void set_U3CCountU3Ek__BackingField_0(int32_t value)
	{
		___U3CCountU3Ek__BackingField_0 = value;
	}

	inline static int32_t get_offset_of_U3CIdsU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(CubismUnmanagedParts_t1124036275, ___U3CIdsU3Ek__BackingField_1)); }
	inline StringU5BU5D_t1642385972* get_U3CIdsU3Ek__BackingField_1() const { return ___U3CIdsU3Ek__BackingField_1; }
	inline StringU5BU5D_t1642385972** get_address_of_U3CIdsU3Ek__BackingField_1() { return &___U3CIdsU3Ek__BackingField_1; }
	inline void set_U3CIdsU3Ek__BackingField_1(StringU5BU5D_t1642385972* value)
	{
		___U3CIdsU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CIdsU3Ek__BackingField_1), value);
	}

	inline static int32_t get_offset_of_U3COpacitiesU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(CubismUnmanagedParts_t1124036275, ___U3COpacitiesU3Ek__BackingField_2)); }
	inline CubismUnmanagedFloatArrayView_t1153227075 * get_U3COpacitiesU3Ek__BackingField_2() const { return ___U3COpacitiesU3Ek__BackingField_2; }
	inline CubismUnmanagedFloatArrayView_t1153227075 ** get_address_of_U3COpacitiesU3Ek__BackingField_2() { return &___U3COpacitiesU3Ek__BackingField_2; }
	inline void set_U3COpacitiesU3Ek__BackingField_2(CubismUnmanagedFloatArrayView_t1153227075 * value)
	{
		___U3COpacitiesU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3COpacitiesU3Ek__BackingField_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CUBISMUNMANAGEDPARTS_T1124036275_H
#ifndef CUBISMUNMANAGEDPARAMETERS_T2764188193_H
#define CUBISMUNMANAGEDPARAMETERS_T2764188193_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Live2D.Cubism.Core.Unmanaged.CubismUnmanagedParameters
struct  CubismUnmanagedParameters_t2764188193  : public RuntimeObject
{
public:
	// System.Int32 Live2D.Cubism.Core.Unmanaged.CubismUnmanagedParameters::<Count>k__BackingField
	int32_t ___U3CCountU3Ek__BackingField_0;
	// System.String[] Live2D.Cubism.Core.Unmanaged.CubismUnmanagedParameters::<Ids>k__BackingField
	StringU5BU5D_t1642385972* ___U3CIdsU3Ek__BackingField_1;
	// Live2D.Cubism.Core.Unmanaged.CubismUnmanagedFloatArrayView Live2D.Cubism.Core.Unmanaged.CubismUnmanagedParameters::<MinimumValues>k__BackingField
	CubismUnmanagedFloatArrayView_t1153227075 * ___U3CMinimumValuesU3Ek__BackingField_2;
	// Live2D.Cubism.Core.Unmanaged.CubismUnmanagedFloatArrayView Live2D.Cubism.Core.Unmanaged.CubismUnmanagedParameters::<MaximumValues>k__BackingField
	CubismUnmanagedFloatArrayView_t1153227075 * ___U3CMaximumValuesU3Ek__BackingField_3;
	// Live2D.Cubism.Core.Unmanaged.CubismUnmanagedFloatArrayView Live2D.Cubism.Core.Unmanaged.CubismUnmanagedParameters::<DefaultValues>k__BackingField
	CubismUnmanagedFloatArrayView_t1153227075 * ___U3CDefaultValuesU3Ek__BackingField_4;
	// Live2D.Cubism.Core.Unmanaged.CubismUnmanagedFloatArrayView Live2D.Cubism.Core.Unmanaged.CubismUnmanagedParameters::<Values>k__BackingField
	CubismUnmanagedFloatArrayView_t1153227075 * ___U3CValuesU3Ek__BackingField_5;

public:
	inline static int32_t get_offset_of_U3CCountU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(CubismUnmanagedParameters_t2764188193, ___U3CCountU3Ek__BackingField_0)); }
	inline int32_t get_U3CCountU3Ek__BackingField_0() const { return ___U3CCountU3Ek__BackingField_0; }
	inline int32_t* get_address_of_U3CCountU3Ek__BackingField_0() { return &___U3CCountU3Ek__BackingField_0; }
	inline void set_U3CCountU3Ek__BackingField_0(int32_t value)
	{
		___U3CCountU3Ek__BackingField_0 = value;
	}

	inline static int32_t get_offset_of_U3CIdsU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(CubismUnmanagedParameters_t2764188193, ___U3CIdsU3Ek__BackingField_1)); }
	inline StringU5BU5D_t1642385972* get_U3CIdsU3Ek__BackingField_1() const { return ___U3CIdsU3Ek__BackingField_1; }
	inline StringU5BU5D_t1642385972** get_address_of_U3CIdsU3Ek__BackingField_1() { return &___U3CIdsU3Ek__BackingField_1; }
	inline void set_U3CIdsU3Ek__BackingField_1(StringU5BU5D_t1642385972* value)
	{
		___U3CIdsU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CIdsU3Ek__BackingField_1), value);
	}

	inline static int32_t get_offset_of_U3CMinimumValuesU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(CubismUnmanagedParameters_t2764188193, ___U3CMinimumValuesU3Ek__BackingField_2)); }
	inline CubismUnmanagedFloatArrayView_t1153227075 * get_U3CMinimumValuesU3Ek__BackingField_2() const { return ___U3CMinimumValuesU3Ek__BackingField_2; }
	inline CubismUnmanagedFloatArrayView_t1153227075 ** get_address_of_U3CMinimumValuesU3Ek__BackingField_2() { return &___U3CMinimumValuesU3Ek__BackingField_2; }
	inline void set_U3CMinimumValuesU3Ek__BackingField_2(CubismUnmanagedFloatArrayView_t1153227075 * value)
	{
		___U3CMinimumValuesU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CMinimumValuesU3Ek__BackingField_2), value);
	}

	inline static int32_t get_offset_of_U3CMaximumValuesU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(CubismUnmanagedParameters_t2764188193, ___U3CMaximumValuesU3Ek__BackingField_3)); }
	inline CubismUnmanagedFloatArrayView_t1153227075 * get_U3CMaximumValuesU3Ek__BackingField_3() const { return ___U3CMaximumValuesU3Ek__BackingField_3; }
	inline CubismUnmanagedFloatArrayView_t1153227075 ** get_address_of_U3CMaximumValuesU3Ek__BackingField_3() { return &___U3CMaximumValuesU3Ek__BackingField_3; }
	inline void set_U3CMaximumValuesU3Ek__BackingField_3(CubismUnmanagedFloatArrayView_t1153227075 * value)
	{
		___U3CMaximumValuesU3Ek__BackingField_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CMaximumValuesU3Ek__BackingField_3), value);
	}

	inline static int32_t get_offset_of_U3CDefaultValuesU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(CubismUnmanagedParameters_t2764188193, ___U3CDefaultValuesU3Ek__BackingField_4)); }
	inline CubismUnmanagedFloatArrayView_t1153227075 * get_U3CDefaultValuesU3Ek__BackingField_4() const { return ___U3CDefaultValuesU3Ek__BackingField_4; }
	inline CubismUnmanagedFloatArrayView_t1153227075 ** get_address_of_U3CDefaultValuesU3Ek__BackingField_4() { return &___U3CDefaultValuesU3Ek__BackingField_4; }
	inline void set_U3CDefaultValuesU3Ek__BackingField_4(CubismUnmanagedFloatArrayView_t1153227075 * value)
	{
		___U3CDefaultValuesU3Ek__BackingField_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CDefaultValuesU3Ek__BackingField_4), value);
	}

	inline static int32_t get_offset_of_U3CValuesU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(CubismUnmanagedParameters_t2764188193, ___U3CValuesU3Ek__BackingField_5)); }
	inline CubismUnmanagedFloatArrayView_t1153227075 * get_U3CValuesU3Ek__BackingField_5() const { return ___U3CValuesU3Ek__BackingField_5; }
	inline CubismUnmanagedFloatArrayView_t1153227075 ** get_address_of_U3CValuesU3Ek__BackingField_5() { return &___U3CValuesU3Ek__BackingField_5; }
	inline void set_U3CValuesU3Ek__BackingField_5(CubismUnmanagedFloatArrayView_t1153227075 * value)
	{
		___U3CValuesU3Ek__BackingField_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3CValuesU3Ek__BackingField_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CUBISMUNMANAGEDPARAMETERS_T2764188193_H
#ifndef CUBISMBUILTINASYNCTASKHANDLER_T2731907693_H
#define CUBISMBUILTINASYNCTASKHANDLER_T2731907693_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Live2D.Cubism.Framework.Tasking.CubismBuiltinAsyncTaskHandler
struct  CubismBuiltinAsyncTaskHandler_t2731907693  : public RuntimeObject
{
public:

public:
};

struct CubismBuiltinAsyncTaskHandler_t2731907693_StaticFields
{
public:
	// System.Collections.Generic.Queue`1<Live2D.Cubism.Core.ICubismTask> Live2D.Cubism.Framework.Tasking.CubismBuiltinAsyncTaskHandler::<Tasks>k__BackingField
	Queue_1_t3877289046 * ___U3CTasksU3Ek__BackingField_0;
	// System.Threading.Thread Live2D.Cubism.Framework.Tasking.CubismBuiltinAsyncTaskHandler::<Worker>k__BackingField
	Thread_t241561612 * ___U3CWorkerU3Ek__BackingField_1;
	// System.Object Live2D.Cubism.Framework.Tasking.CubismBuiltinAsyncTaskHandler::<Lock>k__BackingField
	RuntimeObject * ___U3CLockU3Ek__BackingField_2;
	// System.Threading.ManualResetEvent Live2D.Cubism.Framework.Tasking.CubismBuiltinAsyncTaskHandler::<Signal>k__BackingField
	ManualResetEvent_t926074657 * ___U3CSignalU3Ek__BackingField_3;
	// System.Boolean Live2D.Cubism.Framework.Tasking.CubismBuiltinAsyncTaskHandler::_callItADay
	bool ____callItADay_4;
	// Live2D.Cubism.Core.CubismTaskQueue/CubismTaskHandler Live2D.Cubism.Framework.Tasking.CubismBuiltinAsyncTaskHandler::<>f__mg$cache0
	CubismTaskHandler_t1959870214 * ___U3CU3Ef__mgU24cache0_5;
	// System.Threading.ThreadStart Live2D.Cubism.Framework.Tasking.CubismBuiltinAsyncTaskHandler::<>f__mg$cache1
	ThreadStart_t3437517264 * ___U3CU3Ef__mgU24cache1_6;
	// Live2D.Cubism.Core.CubismTaskQueue/CubismTaskHandler Live2D.Cubism.Framework.Tasking.CubismBuiltinAsyncTaskHandler::<>f__mg$cache2
	CubismTaskHandler_t1959870214 * ___U3CU3Ef__mgU24cache2_7;
	// Live2D.Cubism.Core.CubismTaskQueue/CubismTaskHandler Live2D.Cubism.Framework.Tasking.CubismBuiltinAsyncTaskHandler::<>f__mg$cache3
	CubismTaskHandler_t1959870214 * ___U3CU3Ef__mgU24cache3_8;

public:
	inline static int32_t get_offset_of_U3CTasksU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(CubismBuiltinAsyncTaskHandler_t2731907693_StaticFields, ___U3CTasksU3Ek__BackingField_0)); }
	inline Queue_1_t3877289046 * get_U3CTasksU3Ek__BackingField_0() const { return ___U3CTasksU3Ek__BackingField_0; }
	inline Queue_1_t3877289046 ** get_address_of_U3CTasksU3Ek__BackingField_0() { return &___U3CTasksU3Ek__BackingField_0; }
	inline void set_U3CTasksU3Ek__BackingField_0(Queue_1_t3877289046 * value)
	{
		___U3CTasksU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CTasksU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_U3CWorkerU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(CubismBuiltinAsyncTaskHandler_t2731907693_StaticFields, ___U3CWorkerU3Ek__BackingField_1)); }
	inline Thread_t241561612 * get_U3CWorkerU3Ek__BackingField_1() const { return ___U3CWorkerU3Ek__BackingField_1; }
	inline Thread_t241561612 ** get_address_of_U3CWorkerU3Ek__BackingField_1() { return &___U3CWorkerU3Ek__BackingField_1; }
	inline void set_U3CWorkerU3Ek__BackingField_1(Thread_t241561612 * value)
	{
		___U3CWorkerU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CWorkerU3Ek__BackingField_1), value);
	}

	inline static int32_t get_offset_of_U3CLockU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(CubismBuiltinAsyncTaskHandler_t2731907693_StaticFields, ___U3CLockU3Ek__BackingField_2)); }
	inline RuntimeObject * get_U3CLockU3Ek__BackingField_2() const { return ___U3CLockU3Ek__BackingField_2; }
	inline RuntimeObject ** get_address_of_U3CLockU3Ek__BackingField_2() { return &___U3CLockU3Ek__BackingField_2; }
	inline void set_U3CLockU3Ek__BackingField_2(RuntimeObject * value)
	{
		___U3CLockU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CLockU3Ek__BackingField_2), value);
	}

	inline static int32_t get_offset_of_U3CSignalU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(CubismBuiltinAsyncTaskHandler_t2731907693_StaticFields, ___U3CSignalU3Ek__BackingField_3)); }
	inline ManualResetEvent_t926074657 * get_U3CSignalU3Ek__BackingField_3() const { return ___U3CSignalU3Ek__BackingField_3; }
	inline ManualResetEvent_t926074657 ** get_address_of_U3CSignalU3Ek__BackingField_3() { return &___U3CSignalU3Ek__BackingField_3; }
	inline void set_U3CSignalU3Ek__BackingField_3(ManualResetEvent_t926074657 * value)
	{
		___U3CSignalU3Ek__BackingField_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CSignalU3Ek__BackingField_3), value);
	}

	inline static int32_t get_offset_of__callItADay_4() { return static_cast<int32_t>(offsetof(CubismBuiltinAsyncTaskHandler_t2731907693_StaticFields, ____callItADay_4)); }
	inline bool get__callItADay_4() const { return ____callItADay_4; }
	inline bool* get_address_of__callItADay_4() { return &____callItADay_4; }
	inline void set__callItADay_4(bool value)
	{
		____callItADay_4 = value;
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache0_5() { return static_cast<int32_t>(offsetof(CubismBuiltinAsyncTaskHandler_t2731907693_StaticFields, ___U3CU3Ef__mgU24cache0_5)); }
	inline CubismTaskHandler_t1959870214 * get_U3CU3Ef__mgU24cache0_5() const { return ___U3CU3Ef__mgU24cache0_5; }
	inline CubismTaskHandler_t1959870214 ** get_address_of_U3CU3Ef__mgU24cache0_5() { return &___U3CU3Ef__mgU24cache0_5; }
	inline void set_U3CU3Ef__mgU24cache0_5(CubismTaskHandler_t1959870214 * value)
	{
		___U3CU3Ef__mgU24cache0_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache0_5), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache1_6() { return static_cast<int32_t>(offsetof(CubismBuiltinAsyncTaskHandler_t2731907693_StaticFields, ___U3CU3Ef__mgU24cache1_6)); }
	inline ThreadStart_t3437517264 * get_U3CU3Ef__mgU24cache1_6() const { return ___U3CU3Ef__mgU24cache1_6; }
	inline ThreadStart_t3437517264 ** get_address_of_U3CU3Ef__mgU24cache1_6() { return &___U3CU3Ef__mgU24cache1_6; }
	inline void set_U3CU3Ef__mgU24cache1_6(ThreadStart_t3437517264 * value)
	{
		___U3CU3Ef__mgU24cache1_6 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache1_6), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache2_7() { return static_cast<int32_t>(offsetof(CubismBuiltinAsyncTaskHandler_t2731907693_StaticFields, ___U3CU3Ef__mgU24cache2_7)); }
	inline CubismTaskHandler_t1959870214 * get_U3CU3Ef__mgU24cache2_7() const { return ___U3CU3Ef__mgU24cache2_7; }
	inline CubismTaskHandler_t1959870214 ** get_address_of_U3CU3Ef__mgU24cache2_7() { return &___U3CU3Ef__mgU24cache2_7; }
	inline void set_U3CU3Ef__mgU24cache2_7(CubismTaskHandler_t1959870214 * value)
	{
		___U3CU3Ef__mgU24cache2_7 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache2_7), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache3_8() { return static_cast<int32_t>(offsetof(CubismBuiltinAsyncTaskHandler_t2731907693_StaticFields, ___U3CU3Ef__mgU24cache3_8)); }
	inline CubismTaskHandler_t1959870214 * get_U3CU3Ef__mgU24cache3_8() const { return ___U3CU3Ef__mgU24cache3_8; }
	inline CubismTaskHandler_t1959870214 ** get_address_of_U3CU3Ef__mgU24cache3_8() { return &___U3CU3Ef__mgU24cache3_8; }
	inline void set_U3CU3Ef__mgU24cache3_8(CubismTaskHandler_t1959870214 * value)
	{
		___U3CU3Ef__mgU24cache3_8 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache3_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CUBISMBUILTINASYNCTASKHANDLER_T2731907693_H
#ifndef CUBISMBUILTINMATERIALS_T3009607876_H
#define CUBISMBUILTINMATERIALS_T3009607876_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Live2D.Cubism.Rendering.CubismBuiltinMaterials
struct  CubismBuiltinMaterials_t3009607876  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CUBISMBUILTINMATERIALS_T3009607876_H
#ifndef CUBISMPARAMETEREXTENSIONMETHODS_T3888452717_H
#define CUBISMPARAMETEREXTENSIONMETHODS_T3888452717_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Live2D.Cubism.Framework.CubismParameterExtensionMethods
struct  CubismParameterExtensionMethods_t3888452717  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CUBISMPARAMETEREXTENSIONMETHODS_T3888452717_H
#ifndef CUBISMSORTINGMODEEXTENSIONMETHODS_T3847127943_H
#define CUBISMSORTINGMODEEXTENSIONMETHODS_T3847127943_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Live2D.Cubism.Rendering.CubismSortingModeExtensionMethods
struct  CubismSortingModeExtensionMethods_t3847127943  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CUBISMSORTINGMODEEXTENSIONMETHODS_T3847127943_H
#ifndef CUBISMSHADERVARIABLES_T4252307807_H
#define CUBISMSHADERVARIABLES_T4252307807_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Live2D.Cubism.Rendering.CubismShaderVariables
struct  CubismShaderVariables_t4252307807  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CUBISMSHADERVARIABLES_T4252307807_H
#ifndef CUBISMBUILTINSHADERS_T2033756950_H
#define CUBISMBUILTINSHADERS_T2033756950_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Live2D.Cubism.Rendering.CubismBuiltinShaders
struct  CubismBuiltinShaders_t2033756950  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CUBISMBUILTINSHADERS_T2033756950_H
#ifndef MASKSMASKEDSPAIR_T1105140970_H
#define MASKSMASKEDSPAIR_T1105140970_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Live2D.Cubism.Rendering.Masking.CubismMaskController/MasksMaskedsPair
struct  MasksMaskedsPair_t1105140970 
{
public:
	// Live2D.Cubism.Rendering.CubismRenderer[] Live2D.Cubism.Rendering.Masking.CubismMaskController/MasksMaskedsPair::Masks
	CubismRendererU5BU5D_t2721529825* ___Masks_0;
	// System.Collections.Generic.List`1<Live2D.Cubism.Rendering.CubismRenderer> Live2D.Cubism.Rendering.Masking.CubismMaskController/MasksMaskedsPair::Maskeds
	List_1_t1329472268 * ___Maskeds_1;

public:
	inline static int32_t get_offset_of_Masks_0() { return static_cast<int32_t>(offsetof(MasksMaskedsPair_t1105140970, ___Masks_0)); }
	inline CubismRendererU5BU5D_t2721529825* get_Masks_0() const { return ___Masks_0; }
	inline CubismRendererU5BU5D_t2721529825** get_address_of_Masks_0() { return &___Masks_0; }
	inline void set_Masks_0(CubismRendererU5BU5D_t2721529825* value)
	{
		___Masks_0 = value;
		Il2CppCodeGenWriteBarrier((&___Masks_0), value);
	}

	inline static int32_t get_offset_of_Maskeds_1() { return static_cast<int32_t>(offsetof(MasksMaskedsPair_t1105140970, ___Maskeds_1)); }
	inline List_1_t1329472268 * get_Maskeds_1() const { return ___Maskeds_1; }
	inline List_1_t1329472268 ** get_address_of_Maskeds_1() { return &___Maskeds_1; }
	inline void set_Maskeds_1(List_1_t1329472268 * value)
	{
		___Maskeds_1 = value;
		Il2CppCodeGenWriteBarrier((&___Maskeds_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of Live2D.Cubism.Rendering.Masking.CubismMaskController/MasksMaskedsPair
struct MasksMaskedsPair_t1105140970_marshaled_pinvoke
{
	CubismRendererU5BU5D_t2721529825* ___Masks_0;
	List_1_t1329472268 * ___Maskeds_1;
};
// Native definition for COM marshalling of Live2D.Cubism.Rendering.Masking.CubismMaskController/MasksMaskedsPair
struct MasksMaskedsPair_t1105140970_marshaled_com
{
	CubismRendererU5BU5D_t2721529825* ___Masks_0;
	List_1_t1329472268 * ___Maskeds_1;
};
#endif // MASKSMASKEDSPAIR_T1105140970_H
#ifndef SERIALIZABLENORMALIZATIONVALUE_T1779412570_H
#define SERIALIZABLENORMALIZATIONVALUE_T1779412570_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Live2D.Cubism.Framework.Json.CubismPhysics3Json/SerializableNormalizationValue
struct  SerializableNormalizationValue_t1779412570 
{
public:
	// System.Single Live2D.Cubism.Framework.Json.CubismPhysics3Json/SerializableNormalizationValue::Minimum
	float ___Minimum_0;
	// System.Single Live2D.Cubism.Framework.Json.CubismPhysics3Json/SerializableNormalizationValue::Default
	float ___Default_1;
	// System.Single Live2D.Cubism.Framework.Json.CubismPhysics3Json/SerializableNormalizationValue::Maximum
	float ___Maximum_2;

public:
	inline static int32_t get_offset_of_Minimum_0() { return static_cast<int32_t>(offsetof(SerializableNormalizationValue_t1779412570, ___Minimum_0)); }
	inline float get_Minimum_0() const { return ___Minimum_0; }
	inline float* get_address_of_Minimum_0() { return &___Minimum_0; }
	inline void set_Minimum_0(float value)
	{
		___Minimum_0 = value;
	}

	inline static int32_t get_offset_of_Default_1() { return static_cast<int32_t>(offsetof(SerializableNormalizationValue_t1779412570, ___Default_1)); }
	inline float get_Default_1() const { return ___Default_1; }
	inline float* get_address_of_Default_1() { return &___Default_1; }
	inline void set_Default_1(float value)
	{
		___Default_1 = value;
	}

	inline static int32_t get_offset_of_Maximum_2() { return static_cast<int32_t>(offsetof(SerializableNormalizationValue_t1779412570, ___Maximum_2)); }
	inline float get_Maximum_2() const { return ___Maximum_2; }
	inline float* get_address_of_Maximum_2() { return &___Maximum_2; }
	inline void set_Maximum_2(float value)
	{
		___Maximum_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SERIALIZABLENORMALIZATIONVALUE_T1779412570_H
#ifndef SERIALIZABLEPARAMETER_T2924283429_H
#define SERIALIZABLEPARAMETER_T2924283429_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Live2D.Cubism.Framework.Json.CubismPhysics3Json/SerializableParameter
struct  SerializableParameter_t2924283429 
{
public:
	// System.String Live2D.Cubism.Framework.Json.CubismPhysics3Json/SerializableParameter::Target
	String_t* ___Target_0;
	// System.String Live2D.Cubism.Framework.Json.CubismPhysics3Json/SerializableParameter::Id
	String_t* ___Id_1;

public:
	inline static int32_t get_offset_of_Target_0() { return static_cast<int32_t>(offsetof(SerializableParameter_t2924283429, ___Target_0)); }
	inline String_t* get_Target_0() const { return ___Target_0; }
	inline String_t** get_address_of_Target_0() { return &___Target_0; }
	inline void set_Target_0(String_t* value)
	{
		___Target_0 = value;
		Il2CppCodeGenWriteBarrier((&___Target_0), value);
	}

	inline static int32_t get_offset_of_Id_1() { return static_cast<int32_t>(offsetof(SerializableParameter_t2924283429, ___Id_1)); }
	inline String_t* get_Id_1() const { return ___Id_1; }
	inline String_t** get_address_of_Id_1() { return &___Id_1; }
	inline void set_Id_1(String_t* value)
	{
		___Id_1 = value;
		Il2CppCodeGenWriteBarrier((&___Id_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of Live2D.Cubism.Framework.Json.CubismPhysics3Json/SerializableParameter
struct SerializableParameter_t2924283429_marshaled_pinvoke
{
	char* ___Target_0;
	char* ___Id_1;
};
// Native definition for COM marshalling of Live2D.Cubism.Framework.Json.CubismPhysics3Json/SerializableParameter
struct SerializableParameter_t2924283429_marshaled_com
{
	Il2CppChar* ___Target_0;
	Il2CppChar* ___Id_1;
};
#endif // SERIALIZABLEPARAMETER_T2924283429_H
#ifndef SWAPINFO_T4238099863_H
#define SWAPINFO_T4238099863_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Live2D.Cubism.Rendering.CubismRenderer/SwapInfo
struct  SwapInfo_t4238099863 
{
public:
	union
	{
		struct
		{
			// System.Boolean Live2D.Cubism.Rendering.CubismRenderer/SwapInfo::<NewVertexPositions>k__BackingField
			bool ___U3CNewVertexPositionsU3Ek__BackingField_0;
			// System.Boolean Live2D.Cubism.Rendering.CubismRenderer/SwapInfo::<NewVertexColors>k__BackingField
			bool ___U3CNewVertexColorsU3Ek__BackingField_1;
			// System.Boolean Live2D.Cubism.Rendering.CubismRenderer/SwapInfo::<DidBecomeVisible>k__BackingField
			bool ___U3CDidBecomeVisibleU3Ek__BackingField_2;
		};
		uint8_t SwapInfo_t4238099863__padding[1];
	};

public:
	inline static int32_t get_offset_of_U3CNewVertexPositionsU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(SwapInfo_t4238099863, ___U3CNewVertexPositionsU3Ek__BackingField_0)); }
	inline bool get_U3CNewVertexPositionsU3Ek__BackingField_0() const { return ___U3CNewVertexPositionsU3Ek__BackingField_0; }
	inline bool* get_address_of_U3CNewVertexPositionsU3Ek__BackingField_0() { return &___U3CNewVertexPositionsU3Ek__BackingField_0; }
	inline void set_U3CNewVertexPositionsU3Ek__BackingField_0(bool value)
	{
		___U3CNewVertexPositionsU3Ek__BackingField_0 = value;
	}

	inline static int32_t get_offset_of_U3CNewVertexColorsU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(SwapInfo_t4238099863, ___U3CNewVertexColorsU3Ek__BackingField_1)); }
	inline bool get_U3CNewVertexColorsU3Ek__BackingField_1() const { return ___U3CNewVertexColorsU3Ek__BackingField_1; }
	inline bool* get_address_of_U3CNewVertexColorsU3Ek__BackingField_1() { return &___U3CNewVertexColorsU3Ek__BackingField_1; }
	inline void set_U3CNewVertexColorsU3Ek__BackingField_1(bool value)
	{
		___U3CNewVertexColorsU3Ek__BackingField_1 = value;
	}

	inline static int32_t get_offset_of_U3CDidBecomeVisibleU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(SwapInfo_t4238099863, ___U3CDidBecomeVisibleU3Ek__BackingField_2)); }
	inline bool get_U3CDidBecomeVisibleU3Ek__BackingField_2() const { return ___U3CDidBecomeVisibleU3Ek__BackingField_2; }
	inline bool* get_address_of_U3CDidBecomeVisibleU3Ek__BackingField_2() { return &___U3CDidBecomeVisibleU3Ek__BackingField_2; }
	inline void set_U3CDidBecomeVisibleU3Ek__BackingField_2(bool value)
	{
		___U3CDidBecomeVisibleU3Ek__BackingField_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of Live2D.Cubism.Rendering.CubismRenderer/SwapInfo
struct SwapInfo_t4238099863_marshaled_pinvoke
{
	union
	{
		struct
		{
			int32_t ___U3CNewVertexPositionsU3Ek__BackingField_0;
			int32_t ___U3CNewVertexColorsU3Ek__BackingField_1;
			int32_t ___U3CDidBecomeVisibleU3Ek__BackingField_2;
		};
		uint8_t SwapInfo_t4238099863__padding[1];
	};
};
// Native definition for COM marshalling of Live2D.Cubism.Rendering.CubismRenderer/SwapInfo
struct SwapInfo_t4238099863_marshaled_com
{
	union
	{
		struct
		{
			int32_t ___U3CNewVertexPositionsU3Ek__BackingField_0;
			int32_t ___U3CNewVertexColorsU3Ek__BackingField_1;
			int32_t ___U3CDidBecomeVisibleU3Ek__BackingField_2;
		};
		uint8_t SwapInfo_t4238099863__padding[1];
	};
};
#endif // SWAPINFO_T4238099863_H
#ifndef SOURCESITEM_T2559393707_H
#define SOURCESITEM_T2559393707_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Live2D.Cubism.Rendering.Masking.CubismMaskTexture/SourcesItem
struct  SourcesItem_t2559393707 
{
public:
	// Live2D.Cubism.Rendering.Masking.ICubismMaskTextureCommandSource Live2D.Cubism.Rendering.Masking.CubismMaskTexture/SourcesItem::Source
	RuntimeObject* ___Source_0;
	// Live2D.Cubism.Rendering.Masking.CubismMaskTile[] Live2D.Cubism.Rendering.Masking.CubismMaskTexture/SourcesItem::Tiles
	CubismMaskTileU5BU5D_t1795731440* ___Tiles_1;

public:
	inline static int32_t get_offset_of_Source_0() { return static_cast<int32_t>(offsetof(SourcesItem_t2559393707, ___Source_0)); }
	inline RuntimeObject* get_Source_0() const { return ___Source_0; }
	inline RuntimeObject** get_address_of_Source_0() { return &___Source_0; }
	inline void set_Source_0(RuntimeObject* value)
	{
		___Source_0 = value;
		Il2CppCodeGenWriteBarrier((&___Source_0), value);
	}

	inline static int32_t get_offset_of_Tiles_1() { return static_cast<int32_t>(offsetof(SourcesItem_t2559393707, ___Tiles_1)); }
	inline CubismMaskTileU5BU5D_t1795731440* get_Tiles_1() const { return ___Tiles_1; }
	inline CubismMaskTileU5BU5D_t1795731440** get_address_of_Tiles_1() { return &___Tiles_1; }
	inline void set_Tiles_1(CubismMaskTileU5BU5D_t1795731440* value)
	{
		___Tiles_1 = value;
		Il2CppCodeGenWriteBarrier((&___Tiles_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of Live2D.Cubism.Rendering.Masking.CubismMaskTexture/SourcesItem
struct SourcesItem_t2559393707_marshaled_pinvoke
{
	RuntimeObject* ___Source_0;
	CubismMaskTile_t3996512637 * ___Tiles_1;
};
// Native definition for COM marshalling of Live2D.Cubism.Rendering.Masking.CubismMaskTexture/SourcesItem
struct SourcesItem_t2559393707_marshaled_com
{
	RuntimeObject* ___Source_0;
	CubismMaskTile_t3996512637 * ___Tiles_1;
};
#endif // SOURCESITEM_T2559393707_H
#ifndef SERIALIZABLEVECTOR2_T65906335_H
#define SERIALIZABLEVECTOR2_T65906335_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Live2D.Cubism.Framework.Json.CubismPhysics3Json/SerializableVector2
struct  SerializableVector2_t65906335 
{
public:
	// System.Single Live2D.Cubism.Framework.Json.CubismPhysics3Json/SerializableVector2::X
	float ___X_0;
	// System.Single Live2D.Cubism.Framework.Json.CubismPhysics3Json/SerializableVector2::Y
	float ___Y_1;

public:
	inline static int32_t get_offset_of_X_0() { return static_cast<int32_t>(offsetof(SerializableVector2_t65906335, ___X_0)); }
	inline float get_X_0() const { return ___X_0; }
	inline float* get_address_of_X_0() { return &___X_0; }
	inline void set_X_0(float value)
	{
		___X_0 = value;
	}

	inline static int32_t get_offset_of_Y_1() { return static_cast<int32_t>(offsetof(SerializableVector2_t65906335, ___Y_1)); }
	inline float get_Y_1() const { return ___Y_1; }
	inline float* get_address_of_Y_1() { return &___Y_1; }
	inline void set_Y_1(float value)
	{
		___Y_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SERIALIZABLEVECTOR2_T65906335_H
#ifndef VECTOR3_T2243707580_H
#define VECTOR3_T2243707580_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector3
struct  Vector3_t2243707580 
{
public:
	// System.Single UnityEngine.Vector3::x
	float ___x_1;
	// System.Single UnityEngine.Vector3::y
	float ___y_2;
	// System.Single UnityEngine.Vector3::z
	float ___z_3;

public:
	inline static int32_t get_offset_of_x_1() { return static_cast<int32_t>(offsetof(Vector3_t2243707580, ___x_1)); }
	inline float get_x_1() const { return ___x_1; }
	inline float* get_address_of_x_1() { return &___x_1; }
	inline void set_x_1(float value)
	{
		___x_1 = value;
	}

	inline static int32_t get_offset_of_y_2() { return static_cast<int32_t>(offsetof(Vector3_t2243707580, ___y_2)); }
	inline float get_y_2() const { return ___y_2; }
	inline float* get_address_of_y_2() { return &___y_2; }
	inline void set_y_2(float value)
	{
		___y_2 = value;
	}

	inline static int32_t get_offset_of_z_3() { return static_cast<int32_t>(offsetof(Vector3_t2243707580, ___z_3)); }
	inline float get_z_3() const { return ___z_3; }
	inline float* get_address_of_z_3() { return &___z_3; }
	inline void set_z_3(float value)
	{
		___z_3 = value;
	}
};

struct Vector3_t2243707580_StaticFields
{
public:
	// UnityEngine.Vector3 UnityEngine.Vector3::zeroVector
	Vector3_t2243707580  ___zeroVector_4;
	// UnityEngine.Vector3 UnityEngine.Vector3::oneVector
	Vector3_t2243707580  ___oneVector_5;
	// UnityEngine.Vector3 UnityEngine.Vector3::upVector
	Vector3_t2243707580  ___upVector_6;
	// UnityEngine.Vector3 UnityEngine.Vector3::downVector
	Vector3_t2243707580  ___downVector_7;
	// UnityEngine.Vector3 UnityEngine.Vector3::leftVector
	Vector3_t2243707580  ___leftVector_8;
	// UnityEngine.Vector3 UnityEngine.Vector3::rightVector
	Vector3_t2243707580  ___rightVector_9;
	// UnityEngine.Vector3 UnityEngine.Vector3::forwardVector
	Vector3_t2243707580  ___forwardVector_10;
	// UnityEngine.Vector3 UnityEngine.Vector3::backVector
	Vector3_t2243707580  ___backVector_11;
	// UnityEngine.Vector3 UnityEngine.Vector3::positiveInfinityVector
	Vector3_t2243707580  ___positiveInfinityVector_12;
	// UnityEngine.Vector3 UnityEngine.Vector3::negativeInfinityVector
	Vector3_t2243707580  ___negativeInfinityVector_13;

public:
	inline static int32_t get_offset_of_zeroVector_4() { return static_cast<int32_t>(offsetof(Vector3_t2243707580_StaticFields, ___zeroVector_4)); }
	inline Vector3_t2243707580  get_zeroVector_4() const { return ___zeroVector_4; }
	inline Vector3_t2243707580 * get_address_of_zeroVector_4() { return &___zeroVector_4; }
	inline void set_zeroVector_4(Vector3_t2243707580  value)
	{
		___zeroVector_4 = value;
	}

	inline static int32_t get_offset_of_oneVector_5() { return static_cast<int32_t>(offsetof(Vector3_t2243707580_StaticFields, ___oneVector_5)); }
	inline Vector3_t2243707580  get_oneVector_5() const { return ___oneVector_5; }
	inline Vector3_t2243707580 * get_address_of_oneVector_5() { return &___oneVector_5; }
	inline void set_oneVector_5(Vector3_t2243707580  value)
	{
		___oneVector_5 = value;
	}

	inline static int32_t get_offset_of_upVector_6() { return static_cast<int32_t>(offsetof(Vector3_t2243707580_StaticFields, ___upVector_6)); }
	inline Vector3_t2243707580  get_upVector_6() const { return ___upVector_6; }
	inline Vector3_t2243707580 * get_address_of_upVector_6() { return &___upVector_6; }
	inline void set_upVector_6(Vector3_t2243707580  value)
	{
		___upVector_6 = value;
	}

	inline static int32_t get_offset_of_downVector_7() { return static_cast<int32_t>(offsetof(Vector3_t2243707580_StaticFields, ___downVector_7)); }
	inline Vector3_t2243707580  get_downVector_7() const { return ___downVector_7; }
	inline Vector3_t2243707580 * get_address_of_downVector_7() { return &___downVector_7; }
	inline void set_downVector_7(Vector3_t2243707580  value)
	{
		___downVector_7 = value;
	}

	inline static int32_t get_offset_of_leftVector_8() { return static_cast<int32_t>(offsetof(Vector3_t2243707580_StaticFields, ___leftVector_8)); }
	inline Vector3_t2243707580  get_leftVector_8() const { return ___leftVector_8; }
	inline Vector3_t2243707580 * get_address_of_leftVector_8() { return &___leftVector_8; }
	inline void set_leftVector_8(Vector3_t2243707580  value)
	{
		___leftVector_8 = value;
	}

	inline static int32_t get_offset_of_rightVector_9() { return static_cast<int32_t>(offsetof(Vector3_t2243707580_StaticFields, ___rightVector_9)); }
	inline Vector3_t2243707580  get_rightVector_9() const { return ___rightVector_9; }
	inline Vector3_t2243707580 * get_address_of_rightVector_9() { return &___rightVector_9; }
	inline void set_rightVector_9(Vector3_t2243707580  value)
	{
		___rightVector_9 = value;
	}

	inline static int32_t get_offset_of_forwardVector_10() { return static_cast<int32_t>(offsetof(Vector3_t2243707580_StaticFields, ___forwardVector_10)); }
	inline Vector3_t2243707580  get_forwardVector_10() const { return ___forwardVector_10; }
	inline Vector3_t2243707580 * get_address_of_forwardVector_10() { return &___forwardVector_10; }
	inline void set_forwardVector_10(Vector3_t2243707580  value)
	{
		___forwardVector_10 = value;
	}

	inline static int32_t get_offset_of_backVector_11() { return static_cast<int32_t>(offsetof(Vector3_t2243707580_StaticFields, ___backVector_11)); }
	inline Vector3_t2243707580  get_backVector_11() const { return ___backVector_11; }
	inline Vector3_t2243707580 * get_address_of_backVector_11() { return &___backVector_11; }
	inline void set_backVector_11(Vector3_t2243707580  value)
	{
		___backVector_11 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_12() { return static_cast<int32_t>(offsetof(Vector3_t2243707580_StaticFields, ___positiveInfinityVector_12)); }
	inline Vector3_t2243707580  get_positiveInfinityVector_12() const { return ___positiveInfinityVector_12; }
	inline Vector3_t2243707580 * get_address_of_positiveInfinityVector_12() { return &___positiveInfinityVector_12; }
	inline void set_positiveInfinityVector_12(Vector3_t2243707580  value)
	{
		___positiveInfinityVector_12 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_13() { return static_cast<int32_t>(offsetof(Vector3_t2243707580_StaticFields, ___negativeInfinityVector_13)); }
	inline Vector3_t2243707580  get_negativeInfinityVector_13() const { return ___negativeInfinityVector_13; }
	inline Vector3_t2243707580 * get_address_of_negativeInfinityVector_13() { return &___negativeInfinityVector_13; }
	inline void set_negativeInfinityVector_13(Vector3_t2243707580  value)
	{
		___negativeInfinityVector_13 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR3_T2243707580_H
#ifndef CUBISMMOVEONREIMPORTCOPYCOMPONENTSONLY_T4098471002_H
#define CUBISMMOVEONREIMPORTCOPYCOMPONENTSONLY_T4098471002_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Live2D.Cubism.Framework.CubismMoveOnReimportCopyComponentsOnly
struct  CubismMoveOnReimportCopyComponentsOnly_t4098471002  : public Attribute_t542643598
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CUBISMMOVEONREIMPORTCOPYCOMPONENTSONLY_T4098471002_H
#ifndef VECTOR2_T2243707579_H
#define VECTOR2_T2243707579_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector2
struct  Vector2_t2243707579 
{
public:
	// System.Single UnityEngine.Vector2::x
	float ___x_0;
	// System.Single UnityEngine.Vector2::y
	float ___y_1;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Vector2_t2243707579, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Vector2_t2243707579, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}
};

struct Vector2_t2243707579_StaticFields
{
public:
	// UnityEngine.Vector2 UnityEngine.Vector2::zeroVector
	Vector2_t2243707579  ___zeroVector_2;
	// UnityEngine.Vector2 UnityEngine.Vector2::oneVector
	Vector2_t2243707579  ___oneVector_3;
	// UnityEngine.Vector2 UnityEngine.Vector2::upVector
	Vector2_t2243707579  ___upVector_4;
	// UnityEngine.Vector2 UnityEngine.Vector2::downVector
	Vector2_t2243707579  ___downVector_5;
	// UnityEngine.Vector2 UnityEngine.Vector2::leftVector
	Vector2_t2243707579  ___leftVector_6;
	// UnityEngine.Vector2 UnityEngine.Vector2::rightVector
	Vector2_t2243707579  ___rightVector_7;
	// UnityEngine.Vector2 UnityEngine.Vector2::positiveInfinityVector
	Vector2_t2243707579  ___positiveInfinityVector_8;
	// UnityEngine.Vector2 UnityEngine.Vector2::negativeInfinityVector
	Vector2_t2243707579  ___negativeInfinityVector_9;

public:
	inline static int32_t get_offset_of_zeroVector_2() { return static_cast<int32_t>(offsetof(Vector2_t2243707579_StaticFields, ___zeroVector_2)); }
	inline Vector2_t2243707579  get_zeroVector_2() const { return ___zeroVector_2; }
	inline Vector2_t2243707579 * get_address_of_zeroVector_2() { return &___zeroVector_2; }
	inline void set_zeroVector_2(Vector2_t2243707579  value)
	{
		___zeroVector_2 = value;
	}

	inline static int32_t get_offset_of_oneVector_3() { return static_cast<int32_t>(offsetof(Vector2_t2243707579_StaticFields, ___oneVector_3)); }
	inline Vector2_t2243707579  get_oneVector_3() const { return ___oneVector_3; }
	inline Vector2_t2243707579 * get_address_of_oneVector_3() { return &___oneVector_3; }
	inline void set_oneVector_3(Vector2_t2243707579  value)
	{
		___oneVector_3 = value;
	}

	inline static int32_t get_offset_of_upVector_4() { return static_cast<int32_t>(offsetof(Vector2_t2243707579_StaticFields, ___upVector_4)); }
	inline Vector2_t2243707579  get_upVector_4() const { return ___upVector_4; }
	inline Vector2_t2243707579 * get_address_of_upVector_4() { return &___upVector_4; }
	inline void set_upVector_4(Vector2_t2243707579  value)
	{
		___upVector_4 = value;
	}

	inline static int32_t get_offset_of_downVector_5() { return static_cast<int32_t>(offsetof(Vector2_t2243707579_StaticFields, ___downVector_5)); }
	inline Vector2_t2243707579  get_downVector_5() const { return ___downVector_5; }
	inline Vector2_t2243707579 * get_address_of_downVector_5() { return &___downVector_5; }
	inline void set_downVector_5(Vector2_t2243707579  value)
	{
		___downVector_5 = value;
	}

	inline static int32_t get_offset_of_leftVector_6() { return static_cast<int32_t>(offsetof(Vector2_t2243707579_StaticFields, ___leftVector_6)); }
	inline Vector2_t2243707579  get_leftVector_6() const { return ___leftVector_6; }
	inline Vector2_t2243707579 * get_address_of_leftVector_6() { return &___leftVector_6; }
	inline void set_leftVector_6(Vector2_t2243707579  value)
	{
		___leftVector_6 = value;
	}

	inline static int32_t get_offset_of_rightVector_7() { return static_cast<int32_t>(offsetof(Vector2_t2243707579_StaticFields, ___rightVector_7)); }
	inline Vector2_t2243707579  get_rightVector_7() const { return ___rightVector_7; }
	inline Vector2_t2243707579 * get_address_of_rightVector_7() { return &___rightVector_7; }
	inline void set_rightVector_7(Vector2_t2243707579  value)
	{
		___rightVector_7 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_8() { return static_cast<int32_t>(offsetof(Vector2_t2243707579_StaticFields, ___positiveInfinityVector_8)); }
	inline Vector2_t2243707579  get_positiveInfinityVector_8() const { return ___positiveInfinityVector_8; }
	inline Vector2_t2243707579 * get_address_of_positiveInfinityVector_8() { return &___positiveInfinityVector_8; }
	inline void set_positiveInfinityVector_8(Vector2_t2243707579  value)
	{
		___positiveInfinityVector_8 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_9() { return static_cast<int32_t>(offsetof(Vector2_t2243707579_StaticFields, ___negativeInfinityVector_9)); }
	inline Vector2_t2243707579  get_negativeInfinityVector_9() const { return ___negativeInfinityVector_9; }
	inline Vector2_t2243707579 * get_address_of_negativeInfinityVector_9() { return &___negativeInfinityVector_9; }
	inline void set_negativeInfinityVector_9(Vector2_t2243707579  value)
	{
		___negativeInfinityVector_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR2_T2243707579_H
#ifndef SINGLE_T2076509932_H
#define SINGLE_T2076509932_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Single
struct  Single_t2076509932 
{
public:
	// System.Single System.Single::m_value
	float ___m_value_7;

public:
	inline static int32_t get_offset_of_m_value_7() { return static_cast<int32_t>(offsetof(Single_t2076509932, ___m_value_7)); }
	inline float get_m_value_7() const { return ___m_value_7; }
	inline float* get_address_of_m_value_7() { return &___m_value_7; }
	inline void set_m_value_7(float value)
	{
		___m_value_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SINGLE_T2076509932_H
#ifndef CUBISMDONTMOVEONREIMPORTATTRIBUTE_T3341164094_H
#define CUBISMDONTMOVEONREIMPORTATTRIBUTE_T3341164094_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Live2D.Cubism.Framework.CubismDontMoveOnReimportAttribute
struct  CubismDontMoveOnReimportAttribute_t3341164094  : public Attribute_t542643598
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CUBISMDONTMOVEONREIMPORTATTRIBUTE_T3341164094_H
#ifndef COLOR_T2020392075_H
#define COLOR_T2020392075_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Color
struct  Color_t2020392075 
{
public:
	// System.Single UnityEngine.Color::r
	float ___r_0;
	// System.Single UnityEngine.Color::g
	float ___g_1;
	// System.Single UnityEngine.Color::b
	float ___b_2;
	// System.Single UnityEngine.Color::a
	float ___a_3;

public:
	inline static int32_t get_offset_of_r_0() { return static_cast<int32_t>(offsetof(Color_t2020392075, ___r_0)); }
	inline float get_r_0() const { return ___r_0; }
	inline float* get_address_of_r_0() { return &___r_0; }
	inline void set_r_0(float value)
	{
		___r_0 = value;
	}

	inline static int32_t get_offset_of_g_1() { return static_cast<int32_t>(offsetof(Color_t2020392075, ___g_1)); }
	inline float get_g_1() const { return ___g_1; }
	inline float* get_address_of_g_1() { return &___g_1; }
	inline void set_g_1(float value)
	{
		___g_1 = value;
	}

	inline static int32_t get_offset_of_b_2() { return static_cast<int32_t>(offsetof(Color_t2020392075, ___b_2)); }
	inline float get_b_2() const { return ___b_2; }
	inline float* get_address_of_b_2() { return &___b_2; }
	inline void set_b_2(float value)
	{
		___b_2 = value;
	}

	inline static int32_t get_offset_of_a_3() { return static_cast<int32_t>(offsetof(Color_t2020392075, ___a_3)); }
	inline float get_a_3() const { return ___a_3; }
	inline float* get_address_of_a_3() { return &___a_3; }
	inline void set_a_3(float value)
	{
		___a_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLOR_T2020392075_H
#ifndef CUBISMPHYSICSNORMALIZATIONTUPLET_T3360392585_H
#define CUBISMPHYSICSNORMALIZATIONTUPLET_T3360392585_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Live2D.Cubism.Framework.Physics.CubismPhysicsNormalizationTuplet
struct  CubismPhysicsNormalizationTuplet_t3360392585 
{
public:
	// System.Single Live2D.Cubism.Framework.Physics.CubismPhysicsNormalizationTuplet::Maximum
	float ___Maximum_0;
	// System.Single Live2D.Cubism.Framework.Physics.CubismPhysicsNormalizationTuplet::Minimum
	float ___Minimum_1;
	// System.Single Live2D.Cubism.Framework.Physics.CubismPhysicsNormalizationTuplet::Default
	float ___Default_2;

public:
	inline static int32_t get_offset_of_Maximum_0() { return static_cast<int32_t>(offsetof(CubismPhysicsNormalizationTuplet_t3360392585, ___Maximum_0)); }
	inline float get_Maximum_0() const { return ___Maximum_0; }
	inline float* get_address_of_Maximum_0() { return &___Maximum_0; }
	inline void set_Maximum_0(float value)
	{
		___Maximum_0 = value;
	}

	inline static int32_t get_offset_of_Minimum_1() { return static_cast<int32_t>(offsetof(CubismPhysicsNormalizationTuplet_t3360392585, ___Minimum_1)); }
	inline float get_Minimum_1() const { return ___Minimum_1; }
	inline float* get_address_of_Minimum_1() { return &___Minimum_1; }
	inline void set_Minimum_1(float value)
	{
		___Minimum_1 = value;
	}

	inline static int32_t get_offset_of_Default_2() { return static_cast<int32_t>(offsetof(CubismPhysicsNormalizationTuplet_t3360392585, ___Default_2)); }
	inline float get_Default_2() const { return ___Default_2; }
	inline float* get_address_of_Default_2() { return &___Default_2; }
	inline void set_Default_2(float value)
	{
		___Default_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CUBISMPHYSICSNORMALIZATIONTUPLET_T3360392585_H
#ifndef INT32_T2071877448_H
#define INT32_T2071877448_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Int32
struct  Int32_t2071877448 
{
public:
	// System.Int32 System.Int32::m_value
	int32_t ___m_value_2;

public:
	inline static int32_t get_offset_of_m_value_2() { return static_cast<int32_t>(offsetof(Int32_t2071877448, ___m_value_2)); }
	inline int32_t get_m_value_2() const { return ___m_value_2; }
	inline int32_t* get_address_of_m_value_2() { return &___m_value_2; }
	inline void set_m_value_2(int32_t value)
	{
		___m_value_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INT32_T2071877448_H
#ifndef SERIALIZABLEMETA_T2740270858_H
#define SERIALIZABLEMETA_T2740270858_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Live2D.Cubism.Framework.Json.CubismMotion3Json/SerializableMeta
struct  SerializableMeta_t2740270858 
{
public:
	// System.Single Live2D.Cubism.Framework.Json.CubismMotion3Json/SerializableMeta::Duration
	float ___Duration_0;
	// System.Single Live2D.Cubism.Framework.Json.CubismMotion3Json/SerializableMeta::Fps
	float ___Fps_1;
	// System.Boolean Live2D.Cubism.Framework.Json.CubismMotion3Json/SerializableMeta::Loop
	bool ___Loop_2;
	// System.Int32 Live2D.Cubism.Framework.Json.CubismMotion3Json/SerializableMeta::CurveCount
	int32_t ___CurveCount_3;
	// System.Int32 Live2D.Cubism.Framework.Json.CubismMotion3Json/SerializableMeta::TotalSegmentCount
	int32_t ___TotalSegmentCount_4;
	// System.Int32 Live2D.Cubism.Framework.Json.CubismMotion3Json/SerializableMeta::TotalPointCount
	int32_t ___TotalPointCount_5;
	// System.Boolean Live2D.Cubism.Framework.Json.CubismMotion3Json/SerializableMeta::AreBeziersRestricted
	bool ___AreBeziersRestricted_6;

public:
	inline static int32_t get_offset_of_Duration_0() { return static_cast<int32_t>(offsetof(SerializableMeta_t2740270858, ___Duration_0)); }
	inline float get_Duration_0() const { return ___Duration_0; }
	inline float* get_address_of_Duration_0() { return &___Duration_0; }
	inline void set_Duration_0(float value)
	{
		___Duration_0 = value;
	}

	inline static int32_t get_offset_of_Fps_1() { return static_cast<int32_t>(offsetof(SerializableMeta_t2740270858, ___Fps_1)); }
	inline float get_Fps_1() const { return ___Fps_1; }
	inline float* get_address_of_Fps_1() { return &___Fps_1; }
	inline void set_Fps_1(float value)
	{
		___Fps_1 = value;
	}

	inline static int32_t get_offset_of_Loop_2() { return static_cast<int32_t>(offsetof(SerializableMeta_t2740270858, ___Loop_2)); }
	inline bool get_Loop_2() const { return ___Loop_2; }
	inline bool* get_address_of_Loop_2() { return &___Loop_2; }
	inline void set_Loop_2(bool value)
	{
		___Loop_2 = value;
	}

	inline static int32_t get_offset_of_CurveCount_3() { return static_cast<int32_t>(offsetof(SerializableMeta_t2740270858, ___CurveCount_3)); }
	inline int32_t get_CurveCount_3() const { return ___CurveCount_3; }
	inline int32_t* get_address_of_CurveCount_3() { return &___CurveCount_3; }
	inline void set_CurveCount_3(int32_t value)
	{
		___CurveCount_3 = value;
	}

	inline static int32_t get_offset_of_TotalSegmentCount_4() { return static_cast<int32_t>(offsetof(SerializableMeta_t2740270858, ___TotalSegmentCount_4)); }
	inline int32_t get_TotalSegmentCount_4() const { return ___TotalSegmentCount_4; }
	inline int32_t* get_address_of_TotalSegmentCount_4() { return &___TotalSegmentCount_4; }
	inline void set_TotalSegmentCount_4(int32_t value)
	{
		___TotalSegmentCount_4 = value;
	}

	inline static int32_t get_offset_of_TotalPointCount_5() { return static_cast<int32_t>(offsetof(SerializableMeta_t2740270858, ___TotalPointCount_5)); }
	inline int32_t get_TotalPointCount_5() const { return ___TotalPointCount_5; }
	inline int32_t* get_address_of_TotalPointCount_5() { return &___TotalPointCount_5; }
	inline void set_TotalPointCount_5(int32_t value)
	{
		___TotalPointCount_5 = value;
	}

	inline static int32_t get_offset_of_AreBeziersRestricted_6() { return static_cast<int32_t>(offsetof(SerializableMeta_t2740270858, ___AreBeziersRestricted_6)); }
	inline bool get_AreBeziersRestricted_6() const { return ___AreBeziersRestricted_6; }
	inline bool* get_address_of_AreBeziersRestricted_6() { return &___AreBeziersRestricted_6; }
	inline void set_AreBeziersRestricted_6(bool value)
	{
		___AreBeziersRestricted_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of Live2D.Cubism.Framework.Json.CubismMotion3Json/SerializableMeta
struct SerializableMeta_t2740270858_marshaled_pinvoke
{
	float ___Duration_0;
	float ___Fps_1;
	int32_t ___Loop_2;
	int32_t ___CurveCount_3;
	int32_t ___TotalSegmentCount_4;
	int32_t ___TotalPointCount_5;
	int32_t ___AreBeziersRestricted_6;
};
// Native definition for COM marshalling of Live2D.Cubism.Framework.Json.CubismMotion3Json/SerializableMeta
struct SerializableMeta_t2740270858_marshaled_com
{
	float ___Duration_0;
	float ___Fps_1;
	int32_t ___Loop_2;
	int32_t ___CurveCount_3;
	int32_t ___TotalSegmentCount_4;
	int32_t ___TotalPointCount_5;
	int32_t ___AreBeziersRestricted_6;
};
#endif // SERIALIZABLEMETA_T2740270858_H
#ifndef CUBISMMASKTILE_T3996512637_H
#define CUBISMMASKTILE_T3996512637_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Live2D.Cubism.Rendering.Masking.CubismMaskTile
struct  CubismMaskTile_t3996512637 
{
public:
	// System.Single Live2D.Cubism.Rendering.Masking.CubismMaskTile::Channel
	float ___Channel_0;
	// System.Single Live2D.Cubism.Rendering.Masking.CubismMaskTile::Column
	float ___Column_1;
	// System.Single Live2D.Cubism.Rendering.Masking.CubismMaskTile::Row
	float ___Row_2;
	// System.Single Live2D.Cubism.Rendering.Masking.CubismMaskTile::Size
	float ___Size_3;

public:
	inline static int32_t get_offset_of_Channel_0() { return static_cast<int32_t>(offsetof(CubismMaskTile_t3996512637, ___Channel_0)); }
	inline float get_Channel_0() const { return ___Channel_0; }
	inline float* get_address_of_Channel_0() { return &___Channel_0; }
	inline void set_Channel_0(float value)
	{
		___Channel_0 = value;
	}

	inline static int32_t get_offset_of_Column_1() { return static_cast<int32_t>(offsetof(CubismMaskTile_t3996512637, ___Column_1)); }
	inline float get_Column_1() const { return ___Column_1; }
	inline float* get_address_of_Column_1() { return &___Column_1; }
	inline void set_Column_1(float value)
	{
		___Column_1 = value;
	}

	inline static int32_t get_offset_of_Row_2() { return static_cast<int32_t>(offsetof(CubismMaskTile_t3996512637, ___Row_2)); }
	inline float get_Row_2() const { return ___Row_2; }
	inline float* get_address_of_Row_2() { return &___Row_2; }
	inline void set_Row_2(float value)
	{
		___Row_2 = value;
	}

	inline static int32_t get_offset_of_Size_3() { return static_cast<int32_t>(offsetof(CubismMaskTile_t3996512637, ___Size_3)); }
	inline float get_Size_3() const { return ___Size_3; }
	inline float* get_address_of_Size_3() { return &___Size_3; }
	inline void set_Size_3(float value)
	{
		___Size_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CUBISMMASKTILE_T3996512637_H
#ifndef SERIALIZABLECURVE_T3734873318_H
#define SERIALIZABLECURVE_T3734873318_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Live2D.Cubism.Framework.Json.CubismMotion3Json/SerializableCurve
struct  SerializableCurve_t3734873318 
{
public:
	// System.String Live2D.Cubism.Framework.Json.CubismMotion3Json/SerializableCurve::Target
	String_t* ___Target_0;
	// System.String Live2D.Cubism.Framework.Json.CubismMotion3Json/SerializableCurve::Id
	String_t* ___Id_1;
	// System.Single[] Live2D.Cubism.Framework.Json.CubismMotion3Json/SerializableCurve::Segments
	SingleU5BU5D_t577127397* ___Segments_2;

public:
	inline static int32_t get_offset_of_Target_0() { return static_cast<int32_t>(offsetof(SerializableCurve_t3734873318, ___Target_0)); }
	inline String_t* get_Target_0() const { return ___Target_0; }
	inline String_t** get_address_of_Target_0() { return &___Target_0; }
	inline void set_Target_0(String_t* value)
	{
		___Target_0 = value;
		Il2CppCodeGenWriteBarrier((&___Target_0), value);
	}

	inline static int32_t get_offset_of_Id_1() { return static_cast<int32_t>(offsetof(SerializableCurve_t3734873318, ___Id_1)); }
	inline String_t* get_Id_1() const { return ___Id_1; }
	inline String_t** get_address_of_Id_1() { return &___Id_1; }
	inline void set_Id_1(String_t* value)
	{
		___Id_1 = value;
		Il2CppCodeGenWriteBarrier((&___Id_1), value);
	}

	inline static int32_t get_offset_of_Segments_2() { return static_cast<int32_t>(offsetof(SerializableCurve_t3734873318, ___Segments_2)); }
	inline SingleU5BU5D_t577127397* get_Segments_2() const { return ___Segments_2; }
	inline SingleU5BU5D_t577127397** get_address_of_Segments_2() { return &___Segments_2; }
	inline void set_Segments_2(SingleU5BU5D_t577127397* value)
	{
		___Segments_2 = value;
		Il2CppCodeGenWriteBarrier((&___Segments_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of Live2D.Cubism.Framework.Json.CubismMotion3Json/SerializableCurve
struct SerializableCurve_t3734873318_marshaled_pinvoke
{
	char* ___Target_0;
	char* ___Id_1;
	float* ___Segments_2;
};
// Native definition for COM marshalling of Live2D.Cubism.Framework.Json.CubismMotion3Json/SerializableCurve
struct SerializableCurve_t3734873318_marshaled_com
{
	Il2CppChar* ___Target_0;
	Il2CppChar* ___Id_1;
	float* ___Segments_2;
};
#endif // SERIALIZABLECURVE_T3734873318_H
#ifndef SERIALIZABLEFILEREFERENCES_T1075727736_H
#define SERIALIZABLEFILEREFERENCES_T1075727736_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Live2D.Cubism.Framework.Json.CubismModel3Json/SerializableFileReferences
struct  SerializableFileReferences_t1075727736 
{
public:
	// System.String Live2D.Cubism.Framework.Json.CubismModel3Json/SerializableFileReferences::Moc
	String_t* ___Moc_0;
	// System.String[] Live2D.Cubism.Framework.Json.CubismModel3Json/SerializableFileReferences::Textures
	StringU5BU5D_t1642385972* ___Textures_1;
	// System.String Live2D.Cubism.Framework.Json.CubismModel3Json/SerializableFileReferences::Physics
	String_t* ___Physics_2;

public:
	inline static int32_t get_offset_of_Moc_0() { return static_cast<int32_t>(offsetof(SerializableFileReferences_t1075727736, ___Moc_0)); }
	inline String_t* get_Moc_0() const { return ___Moc_0; }
	inline String_t** get_address_of_Moc_0() { return &___Moc_0; }
	inline void set_Moc_0(String_t* value)
	{
		___Moc_0 = value;
		Il2CppCodeGenWriteBarrier((&___Moc_0), value);
	}

	inline static int32_t get_offset_of_Textures_1() { return static_cast<int32_t>(offsetof(SerializableFileReferences_t1075727736, ___Textures_1)); }
	inline StringU5BU5D_t1642385972* get_Textures_1() const { return ___Textures_1; }
	inline StringU5BU5D_t1642385972** get_address_of_Textures_1() { return &___Textures_1; }
	inline void set_Textures_1(StringU5BU5D_t1642385972* value)
	{
		___Textures_1 = value;
		Il2CppCodeGenWriteBarrier((&___Textures_1), value);
	}

	inline static int32_t get_offset_of_Physics_2() { return static_cast<int32_t>(offsetof(SerializableFileReferences_t1075727736, ___Physics_2)); }
	inline String_t* get_Physics_2() const { return ___Physics_2; }
	inline String_t** get_address_of_Physics_2() { return &___Physics_2; }
	inline void set_Physics_2(String_t* value)
	{
		___Physics_2 = value;
		Il2CppCodeGenWriteBarrier((&___Physics_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of Live2D.Cubism.Framework.Json.CubismModel3Json/SerializableFileReferences
struct SerializableFileReferences_t1075727736_marshaled_pinvoke
{
	char* ___Moc_0;
	char** ___Textures_1;
	char* ___Physics_2;
};
// Native definition for COM marshalling of Live2D.Cubism.Framework.Json.CubismModel3Json/SerializableFileReferences
struct SerializableFileReferences_t1075727736_marshaled_com
{
	Il2CppChar* ___Moc_0;
	Il2CppChar** ___Textures_1;
	Il2CppChar* ___Physics_2;
};
#endif // SERIALIZABLEFILEREFERENCES_T1075727736_H
#ifndef SERIALIZABLEGROUP_T739349147_H
#define SERIALIZABLEGROUP_T739349147_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Live2D.Cubism.Framework.Json.CubismModel3Json/SerializableGroup
struct  SerializableGroup_t739349147 
{
public:
	// System.String Live2D.Cubism.Framework.Json.CubismModel3Json/SerializableGroup::Target
	String_t* ___Target_0;
	// System.String Live2D.Cubism.Framework.Json.CubismModel3Json/SerializableGroup::Name
	String_t* ___Name_1;
	// System.String[] Live2D.Cubism.Framework.Json.CubismModel3Json/SerializableGroup::Ids
	StringU5BU5D_t1642385972* ___Ids_2;

public:
	inline static int32_t get_offset_of_Target_0() { return static_cast<int32_t>(offsetof(SerializableGroup_t739349147, ___Target_0)); }
	inline String_t* get_Target_0() const { return ___Target_0; }
	inline String_t** get_address_of_Target_0() { return &___Target_0; }
	inline void set_Target_0(String_t* value)
	{
		___Target_0 = value;
		Il2CppCodeGenWriteBarrier((&___Target_0), value);
	}

	inline static int32_t get_offset_of_Name_1() { return static_cast<int32_t>(offsetof(SerializableGroup_t739349147, ___Name_1)); }
	inline String_t* get_Name_1() const { return ___Name_1; }
	inline String_t** get_address_of_Name_1() { return &___Name_1; }
	inline void set_Name_1(String_t* value)
	{
		___Name_1 = value;
		Il2CppCodeGenWriteBarrier((&___Name_1), value);
	}

	inline static int32_t get_offset_of_Ids_2() { return static_cast<int32_t>(offsetof(SerializableGroup_t739349147, ___Ids_2)); }
	inline StringU5BU5D_t1642385972* get_Ids_2() const { return ___Ids_2; }
	inline StringU5BU5D_t1642385972** get_address_of_Ids_2() { return &___Ids_2; }
	inline void set_Ids_2(StringU5BU5D_t1642385972* value)
	{
		___Ids_2 = value;
		Il2CppCodeGenWriteBarrier((&___Ids_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of Live2D.Cubism.Framework.Json.CubismModel3Json/SerializableGroup
struct SerializableGroup_t739349147_marshaled_pinvoke
{
	char* ___Target_0;
	char* ___Name_1;
	char** ___Ids_2;
};
// Native definition for COM marshalling of Live2D.Cubism.Framework.Json.CubismModel3Json/SerializableGroup
struct SerializableGroup_t739349147_marshaled_com
{
	Il2CppChar* ___Target_0;
	Il2CppChar* ___Name_1;
	Il2CppChar** ___Ids_2;
};
#endif // SERIALIZABLEGROUP_T739349147_H
#ifndef INTPTR_T_H
#define INTPTR_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTPTR_T_H
#ifndef ENUM_T2459695545_H
#define ENUM_T2459695545_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Enum
struct  Enum_t2459695545  : public ValueType_t3507792607
{
public:

public:
};

struct Enum_t2459695545_StaticFields
{
public:
	// System.Char[] System.Enum::split_char
	CharU5BU5D_t1328083999* ___split_char_0;

public:
	inline static int32_t get_offset_of_split_char_0() { return static_cast<int32_t>(offsetof(Enum_t2459695545_StaticFields, ___split_char_0)); }
	inline CharU5BU5D_t1328083999* get_split_char_0() const { return ___split_char_0; }
	inline CharU5BU5D_t1328083999** get_address_of_split_char_0() { return &___split_char_0; }
	inline void set_split_char_0(CharU5BU5D_t1328083999* value)
	{
		___split_char_0 = value;
		Il2CppCodeGenWriteBarrier((&___split_char_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t2459695545_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t2459695545_marshaled_com
{
};
#endif // ENUM_T2459695545_H
#ifndef VOID_T1841601450_H
#define VOID_T1841601450_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Void
struct  Void_t1841601450 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VOID_T1841601450_H
#ifndef CUBISMPHYSICSSOURCECOMPONENT_T1993572628_H
#define CUBISMPHYSICSSOURCECOMPONENT_T1993572628_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Live2D.Cubism.Framework.Physics.CubismPhysicsSourceComponent
struct  CubismPhysicsSourceComponent_t1993572628 
{
public:
	// System.Int32 Live2D.Cubism.Framework.Physics.CubismPhysicsSourceComponent::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(CubismPhysicsSourceComponent_t1993572628, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CUBISMPHYSICSSOURCECOMPONENT_T1993572628_H
#ifndef CUBISMRAYCASTABLEPRECISION_T2374123498_H
#define CUBISMRAYCASTABLEPRECISION_T2374123498_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Live2D.Cubism.Framework.Raycasting.CubismRaycastablePrecision
struct  CubismRaycastablePrecision_t2374123498 
{
public:
	// System.Int32 Live2D.Cubism.Framework.Raycasting.CubismRaycastablePrecision::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(CubismRaycastablePrecision_t2374123498, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CUBISMRAYCASTABLEPRECISION_T2374123498_H
#ifndef CUBISMPHYSICSPARTICLE_T2883958254_H
#define CUBISMPHYSICSPARTICLE_T2883958254_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Live2D.Cubism.Framework.Physics.CubismPhysicsParticle
struct  CubismPhysicsParticle_t2883958254 
{
public:
	// UnityEngine.Vector2 Live2D.Cubism.Framework.Physics.CubismPhysicsParticle::InitialPosition
	Vector2_t2243707579  ___InitialPosition_0;
	// System.Single Live2D.Cubism.Framework.Physics.CubismPhysicsParticle::Mobility
	float ___Mobility_1;
	// System.Single Live2D.Cubism.Framework.Physics.CubismPhysicsParticle::Delay
	float ___Delay_2;
	// System.Single Live2D.Cubism.Framework.Physics.CubismPhysicsParticle::Acceleration
	float ___Acceleration_3;
	// System.Single Live2D.Cubism.Framework.Physics.CubismPhysicsParticle::Radius
	float ___Radius_4;
	// UnityEngine.Vector2 Live2D.Cubism.Framework.Physics.CubismPhysicsParticle::Position
	Vector2_t2243707579  ___Position_5;
	// UnityEngine.Vector2 Live2D.Cubism.Framework.Physics.CubismPhysicsParticle::LastPosition
	Vector2_t2243707579  ___LastPosition_6;
	// UnityEngine.Vector2 Live2D.Cubism.Framework.Physics.CubismPhysicsParticle::LastGravity
	Vector2_t2243707579  ___LastGravity_7;
	// UnityEngine.Vector2 Live2D.Cubism.Framework.Physics.CubismPhysicsParticle::Force
	Vector2_t2243707579  ___Force_8;
	// UnityEngine.Vector2 Live2D.Cubism.Framework.Physics.CubismPhysicsParticle::Velocity
	Vector2_t2243707579  ___Velocity_9;

public:
	inline static int32_t get_offset_of_InitialPosition_0() { return static_cast<int32_t>(offsetof(CubismPhysicsParticle_t2883958254, ___InitialPosition_0)); }
	inline Vector2_t2243707579  get_InitialPosition_0() const { return ___InitialPosition_0; }
	inline Vector2_t2243707579 * get_address_of_InitialPosition_0() { return &___InitialPosition_0; }
	inline void set_InitialPosition_0(Vector2_t2243707579  value)
	{
		___InitialPosition_0 = value;
	}

	inline static int32_t get_offset_of_Mobility_1() { return static_cast<int32_t>(offsetof(CubismPhysicsParticle_t2883958254, ___Mobility_1)); }
	inline float get_Mobility_1() const { return ___Mobility_1; }
	inline float* get_address_of_Mobility_1() { return &___Mobility_1; }
	inline void set_Mobility_1(float value)
	{
		___Mobility_1 = value;
	}

	inline static int32_t get_offset_of_Delay_2() { return static_cast<int32_t>(offsetof(CubismPhysicsParticle_t2883958254, ___Delay_2)); }
	inline float get_Delay_2() const { return ___Delay_2; }
	inline float* get_address_of_Delay_2() { return &___Delay_2; }
	inline void set_Delay_2(float value)
	{
		___Delay_2 = value;
	}

	inline static int32_t get_offset_of_Acceleration_3() { return static_cast<int32_t>(offsetof(CubismPhysicsParticle_t2883958254, ___Acceleration_3)); }
	inline float get_Acceleration_3() const { return ___Acceleration_3; }
	inline float* get_address_of_Acceleration_3() { return &___Acceleration_3; }
	inline void set_Acceleration_3(float value)
	{
		___Acceleration_3 = value;
	}

	inline static int32_t get_offset_of_Radius_4() { return static_cast<int32_t>(offsetof(CubismPhysicsParticle_t2883958254, ___Radius_4)); }
	inline float get_Radius_4() const { return ___Radius_4; }
	inline float* get_address_of_Radius_4() { return &___Radius_4; }
	inline void set_Radius_4(float value)
	{
		___Radius_4 = value;
	}

	inline static int32_t get_offset_of_Position_5() { return static_cast<int32_t>(offsetof(CubismPhysicsParticle_t2883958254, ___Position_5)); }
	inline Vector2_t2243707579  get_Position_5() const { return ___Position_5; }
	inline Vector2_t2243707579 * get_address_of_Position_5() { return &___Position_5; }
	inline void set_Position_5(Vector2_t2243707579  value)
	{
		___Position_5 = value;
	}

	inline static int32_t get_offset_of_LastPosition_6() { return static_cast<int32_t>(offsetof(CubismPhysicsParticle_t2883958254, ___LastPosition_6)); }
	inline Vector2_t2243707579  get_LastPosition_6() const { return ___LastPosition_6; }
	inline Vector2_t2243707579 * get_address_of_LastPosition_6() { return &___LastPosition_6; }
	inline void set_LastPosition_6(Vector2_t2243707579  value)
	{
		___LastPosition_6 = value;
	}

	inline static int32_t get_offset_of_LastGravity_7() { return static_cast<int32_t>(offsetof(CubismPhysicsParticle_t2883958254, ___LastGravity_7)); }
	inline Vector2_t2243707579  get_LastGravity_7() const { return ___LastGravity_7; }
	inline Vector2_t2243707579 * get_address_of_LastGravity_7() { return &___LastGravity_7; }
	inline void set_LastGravity_7(Vector2_t2243707579  value)
	{
		___LastGravity_7 = value;
	}

	inline static int32_t get_offset_of_Force_8() { return static_cast<int32_t>(offsetof(CubismPhysicsParticle_t2883958254, ___Force_8)); }
	inline Vector2_t2243707579  get_Force_8() const { return ___Force_8; }
	inline Vector2_t2243707579 * get_address_of_Force_8() { return &___Force_8; }
	inline void set_Force_8(Vector2_t2243707579  value)
	{
		___Force_8 = value;
	}

	inline static int32_t get_offset_of_Velocity_9() { return static_cast<int32_t>(offsetof(CubismPhysicsParticle_t2883958254, ___Velocity_9)); }
	inline Vector2_t2243707579  get_Velocity_9() const { return ___Velocity_9; }
	inline Vector2_t2243707579 * get_address_of_Velocity_9() { return &___Velocity_9; }
	inline void set_Velocity_9(Vector2_t2243707579  value)
	{
		___Velocity_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CUBISMPHYSICSPARTICLE_T2883958254_H
#ifndef OBJECT_T1021602117_H
#define OBJECT_T1021602117_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Object
struct  Object_t1021602117  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Object::m_CachedPtr
	intptr_t ___m_CachedPtr_0;

public:
	inline static int32_t get_offset_of_m_CachedPtr_0() { return static_cast<int32_t>(offsetof(Object_t1021602117, ___m_CachedPtr_0)); }
	inline intptr_t get_m_CachedPtr_0() const { return ___m_CachedPtr_0; }
	inline intptr_t* get_address_of_m_CachedPtr_0() { return &___m_CachedPtr_0; }
	inline void set_m_CachedPtr_0(intptr_t value)
	{
		___m_CachedPtr_0 = value;
	}
};

struct Object_t1021602117_StaticFields
{
public:
	// System.Int32 UnityEngine.Object::OffsetOfInstanceIDInCPlusPlusObject
	int32_t ___OffsetOfInstanceIDInCPlusPlusObject_1;

public:
	inline static int32_t get_offset_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return static_cast<int32_t>(offsetof(Object_t1021602117_StaticFields, ___OffsetOfInstanceIDInCPlusPlusObject_1)); }
	inline int32_t get_OffsetOfInstanceIDInCPlusPlusObject_1() const { return ___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline int32_t* get_address_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return &___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline void set_OffsetOfInstanceIDInCPlusPlusObject_1(int32_t value)
	{
		___OffsetOfInstanceIDInCPlusPlusObject_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.Object
struct Object_t1021602117_marshaled_pinvoke
{
	intptr_t ___m_CachedPtr_0;
};
// Native definition for COM marshalling of UnityEngine.Object
struct Object_t1021602117_marshaled_com
{
	intptr_t ___m_CachedPtr_0;
};
#endif // OBJECT_T1021602117_H
#ifndef DELEGATE_T3022476291_H
#define DELEGATE_T3022476291_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Delegate
struct  Delegate_t3022476291  : public RuntimeObject
{
public:
	// System.IntPtr System.Delegate::method_ptr
	Il2CppMethodPointer ___method_ptr_0;
	// System.IntPtr System.Delegate::invoke_impl
	intptr_t ___invoke_impl_1;
	// System.Object System.Delegate::m_target
	RuntimeObject * ___m_target_2;
	// System.IntPtr System.Delegate::method
	intptr_t ___method_3;
	// System.IntPtr System.Delegate::delegate_trampoline
	intptr_t ___delegate_trampoline_4;
	// System.IntPtr System.Delegate::method_code
	intptr_t ___method_code_5;
	// System.Reflection.MethodInfo System.Delegate::method_info
	MethodInfo_t * ___method_info_6;
	// System.Reflection.MethodInfo System.Delegate::original_method_info
	MethodInfo_t * ___original_method_info_7;
	// System.DelegateData System.Delegate::data
	DelegateData_t1572802995 * ___data_8;

public:
	inline static int32_t get_offset_of_method_ptr_0() { return static_cast<int32_t>(offsetof(Delegate_t3022476291, ___method_ptr_0)); }
	inline Il2CppMethodPointer get_method_ptr_0() const { return ___method_ptr_0; }
	inline Il2CppMethodPointer* get_address_of_method_ptr_0() { return &___method_ptr_0; }
	inline void set_method_ptr_0(Il2CppMethodPointer value)
	{
		___method_ptr_0 = value;
	}

	inline static int32_t get_offset_of_invoke_impl_1() { return static_cast<int32_t>(offsetof(Delegate_t3022476291, ___invoke_impl_1)); }
	inline intptr_t get_invoke_impl_1() const { return ___invoke_impl_1; }
	inline intptr_t* get_address_of_invoke_impl_1() { return &___invoke_impl_1; }
	inline void set_invoke_impl_1(intptr_t value)
	{
		___invoke_impl_1 = value;
	}

	inline static int32_t get_offset_of_m_target_2() { return static_cast<int32_t>(offsetof(Delegate_t3022476291, ___m_target_2)); }
	inline RuntimeObject * get_m_target_2() const { return ___m_target_2; }
	inline RuntimeObject ** get_address_of_m_target_2() { return &___m_target_2; }
	inline void set_m_target_2(RuntimeObject * value)
	{
		___m_target_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_target_2), value);
	}

	inline static int32_t get_offset_of_method_3() { return static_cast<int32_t>(offsetof(Delegate_t3022476291, ___method_3)); }
	inline intptr_t get_method_3() const { return ___method_3; }
	inline intptr_t* get_address_of_method_3() { return &___method_3; }
	inline void set_method_3(intptr_t value)
	{
		___method_3 = value;
	}

	inline static int32_t get_offset_of_delegate_trampoline_4() { return static_cast<int32_t>(offsetof(Delegate_t3022476291, ___delegate_trampoline_4)); }
	inline intptr_t get_delegate_trampoline_4() const { return ___delegate_trampoline_4; }
	inline intptr_t* get_address_of_delegate_trampoline_4() { return &___delegate_trampoline_4; }
	inline void set_delegate_trampoline_4(intptr_t value)
	{
		___delegate_trampoline_4 = value;
	}

	inline static int32_t get_offset_of_method_code_5() { return static_cast<int32_t>(offsetof(Delegate_t3022476291, ___method_code_5)); }
	inline intptr_t get_method_code_5() const { return ___method_code_5; }
	inline intptr_t* get_address_of_method_code_5() { return &___method_code_5; }
	inline void set_method_code_5(intptr_t value)
	{
		___method_code_5 = value;
	}

	inline static int32_t get_offset_of_method_info_6() { return static_cast<int32_t>(offsetof(Delegate_t3022476291, ___method_info_6)); }
	inline MethodInfo_t * get_method_info_6() const { return ___method_info_6; }
	inline MethodInfo_t ** get_address_of_method_info_6() { return &___method_info_6; }
	inline void set_method_info_6(MethodInfo_t * value)
	{
		___method_info_6 = value;
		Il2CppCodeGenWriteBarrier((&___method_info_6), value);
	}

	inline static int32_t get_offset_of_original_method_info_7() { return static_cast<int32_t>(offsetof(Delegate_t3022476291, ___original_method_info_7)); }
	inline MethodInfo_t * get_original_method_info_7() const { return ___original_method_info_7; }
	inline MethodInfo_t ** get_address_of_original_method_info_7() { return &___original_method_info_7; }
	inline void set_original_method_info_7(MethodInfo_t * value)
	{
		___original_method_info_7 = value;
		Il2CppCodeGenWriteBarrier((&___original_method_info_7), value);
	}

	inline static int32_t get_offset_of_data_8() { return static_cast<int32_t>(offsetof(Delegate_t3022476291, ___data_8)); }
	inline DelegateData_t1572802995 * get_data_8() const { return ___data_8; }
	inline DelegateData_t1572802995 ** get_address_of_data_8() { return &___data_8; }
	inline void set_data_8(DelegateData_t1572802995 * value)
	{
		___data_8 = value;
		Il2CppCodeGenWriteBarrier((&___data_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DELEGATE_T3022476291_H
#ifndef CUBISMMASKTRANSFORM_T2215722789_H
#define CUBISMMASKTRANSFORM_T2215722789_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Live2D.Cubism.Rendering.Masking.CubismMaskTransform
struct  CubismMaskTransform_t2215722789 
{
public:
	// UnityEngine.Vector2 Live2D.Cubism.Rendering.Masking.CubismMaskTransform::Offset
	Vector2_t2243707579  ___Offset_1;
	// System.Single Live2D.Cubism.Rendering.Masking.CubismMaskTransform::Scale
	float ___Scale_2;

public:
	inline static int32_t get_offset_of_Offset_1() { return static_cast<int32_t>(offsetof(CubismMaskTransform_t2215722789, ___Offset_1)); }
	inline Vector2_t2243707579  get_Offset_1() const { return ___Offset_1; }
	inline Vector2_t2243707579 * get_address_of_Offset_1() { return &___Offset_1; }
	inline void set_Offset_1(Vector2_t2243707579  value)
	{
		___Offset_1 = value;
	}

	inline static int32_t get_offset_of_Scale_2() { return static_cast<int32_t>(offsetof(CubismMaskTransform_t2215722789, ___Scale_2)); }
	inline float get_Scale_2() const { return ___Scale_2; }
	inline float* get_address_of_Scale_2() { return &___Scale_2; }
	inline void set_Scale_2(float value)
	{
		___Scale_2 = value;
	}
};

struct CubismMaskTransform_t2215722789_StaticFields
{
public:
	// System.Int32 Live2D.Cubism.Rendering.Masking.CubismMaskTransform::_uniqueId
	int32_t ____uniqueId_0;

public:
	inline static int32_t get_offset_of__uniqueId_0() { return static_cast<int32_t>(offsetof(CubismMaskTransform_t2215722789_StaticFields, ____uniqueId_0)); }
	inline int32_t get__uniqueId_0() const { return ____uniqueId_0; }
	inline int32_t* get_address_of__uniqueId_0() { return &____uniqueId_0; }
	inline void set__uniqueId_0(int32_t value)
	{
		____uniqueId_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CUBISMMASKTRANSFORM_T2215722789_H
#ifndef CUBISMUNMANAGEDMOC_T2103361662_H
#define CUBISMUNMANAGEDMOC_T2103361662_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Live2D.Cubism.Core.Unmanaged.CubismUnmanagedMoc
struct  CubismUnmanagedMoc_t2103361662  : public RuntimeObject
{
public:
	// System.IntPtr Live2D.Cubism.Core.Unmanaged.CubismUnmanagedMoc::<Ptr>k__BackingField
	intptr_t ___U3CPtrU3Ek__BackingField_0;

public:
	inline static int32_t get_offset_of_U3CPtrU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(CubismUnmanagedMoc_t2103361662, ___U3CPtrU3Ek__BackingField_0)); }
	inline intptr_t get_U3CPtrU3Ek__BackingField_0() const { return ___U3CPtrU3Ek__BackingField_0; }
	inline intptr_t* get_address_of_U3CPtrU3Ek__BackingField_0() { return &___U3CPtrU3Ek__BackingField_0; }
	inline void set_U3CPtrU3Ek__BackingField_0(intptr_t value)
	{
		___U3CPtrU3Ek__BackingField_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CUBISMUNMANAGEDMOC_T2103361662_H
#ifndef CUBISMSORTINGMODE_T663667736_H
#define CUBISMSORTINGMODE_T663667736_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Live2D.Cubism.Rendering.CubismSortingMode
struct  CubismSortingMode_t663667736 
{
public:
	// System.Int32 Live2D.Cubism.Rendering.CubismSortingMode::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(CubismSortingMode_t663667736, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CUBISMSORTINGMODE_T663667736_H
#ifndef CUBISMRAYCASTHIT_T1208447795_H
#define CUBISMRAYCASTHIT_T1208447795_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Live2D.Cubism.Framework.Raycasting.CubismRaycastHit
struct  CubismRaycastHit_t1208447795 
{
public:
	// Live2D.Cubism.Core.CubismDrawable Live2D.Cubism.Framework.Raycasting.CubismRaycastHit::Drawable
	CubismDrawable_t2709211313 * ___Drawable_0;
	// System.Single Live2D.Cubism.Framework.Raycasting.CubismRaycastHit::Distance
	float ___Distance_1;
	// UnityEngine.Vector3 Live2D.Cubism.Framework.Raycasting.CubismRaycastHit::LocalPosition
	Vector3_t2243707580  ___LocalPosition_2;
	// UnityEngine.Vector3 Live2D.Cubism.Framework.Raycasting.CubismRaycastHit::WorldPosition
	Vector3_t2243707580  ___WorldPosition_3;

public:
	inline static int32_t get_offset_of_Drawable_0() { return static_cast<int32_t>(offsetof(CubismRaycastHit_t1208447795, ___Drawable_0)); }
	inline CubismDrawable_t2709211313 * get_Drawable_0() const { return ___Drawable_0; }
	inline CubismDrawable_t2709211313 ** get_address_of_Drawable_0() { return &___Drawable_0; }
	inline void set_Drawable_0(CubismDrawable_t2709211313 * value)
	{
		___Drawable_0 = value;
		Il2CppCodeGenWriteBarrier((&___Drawable_0), value);
	}

	inline static int32_t get_offset_of_Distance_1() { return static_cast<int32_t>(offsetof(CubismRaycastHit_t1208447795, ___Distance_1)); }
	inline float get_Distance_1() const { return ___Distance_1; }
	inline float* get_address_of_Distance_1() { return &___Distance_1; }
	inline void set_Distance_1(float value)
	{
		___Distance_1 = value;
	}

	inline static int32_t get_offset_of_LocalPosition_2() { return static_cast<int32_t>(offsetof(CubismRaycastHit_t1208447795, ___LocalPosition_2)); }
	inline Vector3_t2243707580  get_LocalPosition_2() const { return ___LocalPosition_2; }
	inline Vector3_t2243707580 * get_address_of_LocalPosition_2() { return &___LocalPosition_2; }
	inline void set_LocalPosition_2(Vector3_t2243707580  value)
	{
		___LocalPosition_2 = value;
	}

	inline static int32_t get_offset_of_WorldPosition_3() { return static_cast<int32_t>(offsetof(CubismRaycastHit_t1208447795, ___WorldPosition_3)); }
	inline Vector3_t2243707580  get_WorldPosition_3() const { return ___WorldPosition_3; }
	inline Vector3_t2243707580 * get_address_of_WorldPosition_3() { return &___WorldPosition_3; }
	inline void set_WorldPosition_3(Vector3_t2243707580  value)
	{
		___WorldPosition_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of Live2D.Cubism.Framework.Raycasting.CubismRaycastHit
struct CubismRaycastHit_t1208447795_marshaled_pinvoke
{
	CubismDrawable_t2709211313 * ___Drawable_0;
	float ___Distance_1;
	Vector3_t2243707580  ___LocalPosition_2;
	Vector3_t2243707580  ___WorldPosition_3;
};
// Native definition for COM marshalling of Live2D.Cubism.Framework.Raycasting.CubismRaycastHit
struct CubismRaycastHit_t1208447795_marshaled_com
{
	CubismDrawable_t2709211313 * ___Drawable_0;
	float ___Distance_1;
	Vector3_t2243707580  ___LocalPosition_2;
	Vector3_t2243707580  ___WorldPosition_3;
};
#endif // CUBISMRAYCASTHIT_T1208447795_H
#ifndef CUBISMPHYSICSNORMALIZATION_T2601092345_H
#define CUBISMPHYSICSNORMALIZATION_T2601092345_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Live2D.Cubism.Framework.Physics.CubismPhysicsNormalization
struct  CubismPhysicsNormalization_t2601092345 
{
public:
	// Live2D.Cubism.Framework.Physics.CubismPhysicsNormalizationTuplet Live2D.Cubism.Framework.Physics.CubismPhysicsNormalization::Position
	CubismPhysicsNormalizationTuplet_t3360392585  ___Position_0;
	// Live2D.Cubism.Framework.Physics.CubismPhysicsNormalizationTuplet Live2D.Cubism.Framework.Physics.CubismPhysicsNormalization::Angle
	CubismPhysicsNormalizationTuplet_t3360392585  ___Angle_1;

public:
	inline static int32_t get_offset_of_Position_0() { return static_cast<int32_t>(offsetof(CubismPhysicsNormalization_t2601092345, ___Position_0)); }
	inline CubismPhysicsNormalizationTuplet_t3360392585  get_Position_0() const { return ___Position_0; }
	inline CubismPhysicsNormalizationTuplet_t3360392585 * get_address_of_Position_0() { return &___Position_0; }
	inline void set_Position_0(CubismPhysicsNormalizationTuplet_t3360392585  value)
	{
		___Position_0 = value;
	}

	inline static int32_t get_offset_of_Angle_1() { return static_cast<int32_t>(offsetof(CubismPhysicsNormalization_t2601092345, ___Angle_1)); }
	inline CubismPhysicsNormalizationTuplet_t3360392585  get_Angle_1() const { return ___Angle_1; }
	inline CubismPhysicsNormalizationTuplet_t3360392585 * get_address_of_Angle_1() { return &___Angle_1; }
	inline void set_Angle_1(CubismPhysicsNormalizationTuplet_t3360392585  value)
	{
		___Angle_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CUBISMPHYSICSNORMALIZATION_T2601092345_H
#ifndef CUBISMPHYSICS_T1257910384_H
#define CUBISMPHYSICS_T1257910384_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Live2D.Cubism.Framework.Physics.CubismPhysics
struct  CubismPhysics_t1257910384  : public RuntimeObject
{
public:

public:
};

struct CubismPhysics_t1257910384_StaticFields
{
public:
	// UnityEngine.Vector2 Live2D.Cubism.Framework.Physics.CubismPhysics::Wind
	Vector2_t2243707579  ___Wind_0;
	// System.Single Live2D.Cubism.Framework.Physics.CubismPhysics::AirResistance
	float ___AirResistance_1;
	// System.Single Live2D.Cubism.Framework.Physics.CubismPhysics::MaximumWeight
	float ___MaximumWeight_2;
	// System.Boolean Live2D.Cubism.Framework.Physics.CubismPhysics::UseFixedDeltaTime
	bool ___UseFixedDeltaTime_3;
	// System.Boolean Live2D.Cubism.Framework.Physics.CubismPhysics::UseAngleCorrection
	bool ___UseAngleCorrection_4;

public:
	inline static int32_t get_offset_of_Wind_0() { return static_cast<int32_t>(offsetof(CubismPhysics_t1257910384_StaticFields, ___Wind_0)); }
	inline Vector2_t2243707579  get_Wind_0() const { return ___Wind_0; }
	inline Vector2_t2243707579 * get_address_of_Wind_0() { return &___Wind_0; }
	inline void set_Wind_0(Vector2_t2243707579  value)
	{
		___Wind_0 = value;
	}

	inline static int32_t get_offset_of_AirResistance_1() { return static_cast<int32_t>(offsetof(CubismPhysics_t1257910384_StaticFields, ___AirResistance_1)); }
	inline float get_AirResistance_1() const { return ___AirResistance_1; }
	inline float* get_address_of_AirResistance_1() { return &___AirResistance_1; }
	inline void set_AirResistance_1(float value)
	{
		___AirResistance_1 = value;
	}

	inline static int32_t get_offset_of_MaximumWeight_2() { return static_cast<int32_t>(offsetof(CubismPhysics_t1257910384_StaticFields, ___MaximumWeight_2)); }
	inline float get_MaximumWeight_2() const { return ___MaximumWeight_2; }
	inline float* get_address_of_MaximumWeight_2() { return &___MaximumWeight_2; }
	inline void set_MaximumWeight_2(float value)
	{
		___MaximumWeight_2 = value;
	}

	inline static int32_t get_offset_of_UseFixedDeltaTime_3() { return static_cast<int32_t>(offsetof(CubismPhysics_t1257910384_StaticFields, ___UseFixedDeltaTime_3)); }
	inline bool get_UseFixedDeltaTime_3() const { return ___UseFixedDeltaTime_3; }
	inline bool* get_address_of_UseFixedDeltaTime_3() { return &___UseFixedDeltaTime_3; }
	inline void set_UseFixedDeltaTime_3(bool value)
	{
		___UseFixedDeltaTime_3 = value;
	}

	inline static int32_t get_offset_of_UseAngleCorrection_4() { return static_cast<int32_t>(offsetof(CubismPhysics_t1257910384_StaticFields, ___UseAngleCorrection_4)); }
	inline bool get_UseAngleCorrection_4() const { return ___UseAngleCorrection_4; }
	inline bool* get_address_of_UseAngleCorrection_4() { return &___UseAngleCorrection_4; }
	inline void set_UseAngleCorrection_4(bool value)
	{
		___UseAngleCorrection_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CUBISMPHYSICS_T1257910384_H
#ifndef SERIALIZABLEINPUT_T1668668964_H
#define SERIALIZABLEINPUT_T1668668964_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Live2D.Cubism.Framework.Json.CubismPhysics3Json/SerializableInput
struct  SerializableInput_t1668668964 
{
public:
	// Live2D.Cubism.Framework.Json.CubismPhysics3Json/SerializableParameter Live2D.Cubism.Framework.Json.CubismPhysics3Json/SerializableInput::Source
	SerializableParameter_t2924283429  ___Source_0;
	// System.Single Live2D.Cubism.Framework.Json.CubismPhysics3Json/SerializableInput::Weight
	float ___Weight_1;
	// System.String Live2D.Cubism.Framework.Json.CubismPhysics3Json/SerializableInput::Type
	String_t* ___Type_2;
	// System.Boolean Live2D.Cubism.Framework.Json.CubismPhysics3Json/SerializableInput::Reflect
	bool ___Reflect_3;

public:
	inline static int32_t get_offset_of_Source_0() { return static_cast<int32_t>(offsetof(SerializableInput_t1668668964, ___Source_0)); }
	inline SerializableParameter_t2924283429  get_Source_0() const { return ___Source_0; }
	inline SerializableParameter_t2924283429 * get_address_of_Source_0() { return &___Source_0; }
	inline void set_Source_0(SerializableParameter_t2924283429  value)
	{
		___Source_0 = value;
	}

	inline static int32_t get_offset_of_Weight_1() { return static_cast<int32_t>(offsetof(SerializableInput_t1668668964, ___Weight_1)); }
	inline float get_Weight_1() const { return ___Weight_1; }
	inline float* get_address_of_Weight_1() { return &___Weight_1; }
	inline void set_Weight_1(float value)
	{
		___Weight_1 = value;
	}

	inline static int32_t get_offset_of_Type_2() { return static_cast<int32_t>(offsetof(SerializableInput_t1668668964, ___Type_2)); }
	inline String_t* get_Type_2() const { return ___Type_2; }
	inline String_t** get_address_of_Type_2() { return &___Type_2; }
	inline void set_Type_2(String_t* value)
	{
		___Type_2 = value;
		Il2CppCodeGenWriteBarrier((&___Type_2), value);
	}

	inline static int32_t get_offset_of_Reflect_3() { return static_cast<int32_t>(offsetof(SerializableInput_t1668668964, ___Reflect_3)); }
	inline bool get_Reflect_3() const { return ___Reflect_3; }
	inline bool* get_address_of_Reflect_3() { return &___Reflect_3; }
	inline void set_Reflect_3(bool value)
	{
		___Reflect_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of Live2D.Cubism.Framework.Json.CubismPhysics3Json/SerializableInput
struct SerializableInput_t1668668964_marshaled_pinvoke
{
	SerializableParameter_t2924283429_marshaled_pinvoke ___Source_0;
	float ___Weight_1;
	char* ___Type_2;
	int32_t ___Reflect_3;
};
// Native definition for COM marshalling of Live2D.Cubism.Framework.Json.CubismPhysics3Json/SerializableInput
struct SerializableInput_t1668668964_marshaled_com
{
	SerializableParameter_t2924283429_marshaled_com ___Source_0;
	float ___Weight_1;
	Il2CppChar* ___Type_2;
	int32_t ___Reflect_3;
};
#endif // SERIALIZABLEINPUT_T1668668964_H
#ifndef SERIALIZABLEOUTPUT_T2803511543_H
#define SERIALIZABLEOUTPUT_T2803511543_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Live2D.Cubism.Framework.Json.CubismPhysics3Json/SerializableOutput
struct  SerializableOutput_t2803511543 
{
public:
	// Live2D.Cubism.Framework.Json.CubismPhysics3Json/SerializableParameter Live2D.Cubism.Framework.Json.CubismPhysics3Json/SerializableOutput::Destination
	SerializableParameter_t2924283429  ___Destination_0;
	// System.Int32 Live2D.Cubism.Framework.Json.CubismPhysics3Json/SerializableOutput::VertexIndex
	int32_t ___VertexIndex_1;
	// System.Single Live2D.Cubism.Framework.Json.CubismPhysics3Json/SerializableOutput::Scale
	float ___Scale_2;
	// System.Single Live2D.Cubism.Framework.Json.CubismPhysics3Json/SerializableOutput::Weight
	float ___Weight_3;
	// System.String Live2D.Cubism.Framework.Json.CubismPhysics3Json/SerializableOutput::Type
	String_t* ___Type_4;
	// System.Boolean Live2D.Cubism.Framework.Json.CubismPhysics3Json/SerializableOutput::Reflect
	bool ___Reflect_5;

public:
	inline static int32_t get_offset_of_Destination_0() { return static_cast<int32_t>(offsetof(SerializableOutput_t2803511543, ___Destination_0)); }
	inline SerializableParameter_t2924283429  get_Destination_0() const { return ___Destination_0; }
	inline SerializableParameter_t2924283429 * get_address_of_Destination_0() { return &___Destination_0; }
	inline void set_Destination_0(SerializableParameter_t2924283429  value)
	{
		___Destination_0 = value;
	}

	inline static int32_t get_offset_of_VertexIndex_1() { return static_cast<int32_t>(offsetof(SerializableOutput_t2803511543, ___VertexIndex_1)); }
	inline int32_t get_VertexIndex_1() const { return ___VertexIndex_1; }
	inline int32_t* get_address_of_VertexIndex_1() { return &___VertexIndex_1; }
	inline void set_VertexIndex_1(int32_t value)
	{
		___VertexIndex_1 = value;
	}

	inline static int32_t get_offset_of_Scale_2() { return static_cast<int32_t>(offsetof(SerializableOutput_t2803511543, ___Scale_2)); }
	inline float get_Scale_2() const { return ___Scale_2; }
	inline float* get_address_of_Scale_2() { return &___Scale_2; }
	inline void set_Scale_2(float value)
	{
		___Scale_2 = value;
	}

	inline static int32_t get_offset_of_Weight_3() { return static_cast<int32_t>(offsetof(SerializableOutput_t2803511543, ___Weight_3)); }
	inline float get_Weight_3() const { return ___Weight_3; }
	inline float* get_address_of_Weight_3() { return &___Weight_3; }
	inline void set_Weight_3(float value)
	{
		___Weight_3 = value;
	}

	inline static int32_t get_offset_of_Type_4() { return static_cast<int32_t>(offsetof(SerializableOutput_t2803511543, ___Type_4)); }
	inline String_t* get_Type_4() const { return ___Type_4; }
	inline String_t** get_address_of_Type_4() { return &___Type_4; }
	inline void set_Type_4(String_t* value)
	{
		___Type_4 = value;
		Il2CppCodeGenWriteBarrier((&___Type_4), value);
	}

	inline static int32_t get_offset_of_Reflect_5() { return static_cast<int32_t>(offsetof(SerializableOutput_t2803511543, ___Reflect_5)); }
	inline bool get_Reflect_5() const { return ___Reflect_5; }
	inline bool* get_address_of_Reflect_5() { return &___Reflect_5; }
	inline void set_Reflect_5(bool value)
	{
		___Reflect_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of Live2D.Cubism.Framework.Json.CubismPhysics3Json/SerializableOutput
struct SerializableOutput_t2803511543_marshaled_pinvoke
{
	SerializableParameter_t2924283429_marshaled_pinvoke ___Destination_0;
	int32_t ___VertexIndex_1;
	float ___Scale_2;
	float ___Weight_3;
	char* ___Type_4;
	int32_t ___Reflect_5;
};
// Native definition for COM marshalling of Live2D.Cubism.Framework.Json.CubismPhysics3Json/SerializableOutput
struct SerializableOutput_t2803511543_marshaled_com
{
	SerializableParameter_t2924283429_marshaled_com ___Destination_0;
	int32_t ___VertexIndex_1;
	float ___Scale_2;
	float ___Weight_3;
	Il2CppChar* ___Type_4;
	int32_t ___Reflect_5;
};
#endif // SERIALIZABLEOUTPUT_T2803511543_H
#ifndef SERIALIZABLEVERTEX_T1882814154_H
#define SERIALIZABLEVERTEX_T1882814154_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Live2D.Cubism.Framework.Json.CubismPhysics3Json/SerializableVertex
struct  SerializableVertex_t1882814154 
{
public:
	// Live2D.Cubism.Framework.Json.CubismPhysics3Json/SerializableVector2 Live2D.Cubism.Framework.Json.CubismPhysics3Json/SerializableVertex::Position
	SerializableVector2_t65906335  ___Position_0;
	// System.Single Live2D.Cubism.Framework.Json.CubismPhysics3Json/SerializableVertex::Mobility
	float ___Mobility_1;
	// System.Single Live2D.Cubism.Framework.Json.CubismPhysics3Json/SerializableVertex::Delay
	float ___Delay_2;
	// System.Single Live2D.Cubism.Framework.Json.CubismPhysics3Json/SerializableVertex::Acceleration
	float ___Acceleration_3;
	// System.Single Live2D.Cubism.Framework.Json.CubismPhysics3Json/SerializableVertex::Radius
	float ___Radius_4;

public:
	inline static int32_t get_offset_of_Position_0() { return static_cast<int32_t>(offsetof(SerializableVertex_t1882814154, ___Position_0)); }
	inline SerializableVector2_t65906335  get_Position_0() const { return ___Position_0; }
	inline SerializableVector2_t65906335 * get_address_of_Position_0() { return &___Position_0; }
	inline void set_Position_0(SerializableVector2_t65906335  value)
	{
		___Position_0 = value;
	}

	inline static int32_t get_offset_of_Mobility_1() { return static_cast<int32_t>(offsetof(SerializableVertex_t1882814154, ___Mobility_1)); }
	inline float get_Mobility_1() const { return ___Mobility_1; }
	inline float* get_address_of_Mobility_1() { return &___Mobility_1; }
	inline void set_Mobility_1(float value)
	{
		___Mobility_1 = value;
	}

	inline static int32_t get_offset_of_Delay_2() { return static_cast<int32_t>(offsetof(SerializableVertex_t1882814154, ___Delay_2)); }
	inline float get_Delay_2() const { return ___Delay_2; }
	inline float* get_address_of_Delay_2() { return &___Delay_2; }
	inline void set_Delay_2(float value)
	{
		___Delay_2 = value;
	}

	inline static int32_t get_offset_of_Acceleration_3() { return static_cast<int32_t>(offsetof(SerializableVertex_t1882814154, ___Acceleration_3)); }
	inline float get_Acceleration_3() const { return ___Acceleration_3; }
	inline float* get_address_of_Acceleration_3() { return &___Acceleration_3; }
	inline void set_Acceleration_3(float value)
	{
		___Acceleration_3 = value;
	}

	inline static int32_t get_offset_of_Radius_4() { return static_cast<int32_t>(offsetof(SerializableVertex_t1882814154, ___Radius_4)); }
	inline float get_Radius_4() const { return ___Radius_4; }
	inline float* get_address_of_Radius_4() { return &___Radius_4; }
	inline void set_Radius_4(float value)
	{
		___Radius_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SERIALIZABLEVERTEX_T1882814154_H
#ifndef SERIALIZABLENORMALIZATION_T793291037_H
#define SERIALIZABLENORMALIZATION_T793291037_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Live2D.Cubism.Framework.Json.CubismPhysics3Json/SerializableNormalization
struct  SerializableNormalization_t793291037 
{
public:
	// Live2D.Cubism.Framework.Json.CubismPhysics3Json/SerializableNormalizationValue Live2D.Cubism.Framework.Json.CubismPhysics3Json/SerializableNormalization::Position
	SerializableNormalizationValue_t1779412570  ___Position_0;
	// Live2D.Cubism.Framework.Json.CubismPhysics3Json/SerializableNormalizationValue Live2D.Cubism.Framework.Json.CubismPhysics3Json/SerializableNormalization::Angle
	SerializableNormalizationValue_t1779412570  ___Angle_1;

public:
	inline static int32_t get_offset_of_Position_0() { return static_cast<int32_t>(offsetof(SerializableNormalization_t793291037, ___Position_0)); }
	inline SerializableNormalizationValue_t1779412570  get_Position_0() const { return ___Position_0; }
	inline SerializableNormalizationValue_t1779412570 * get_address_of_Position_0() { return &___Position_0; }
	inline void set_Position_0(SerializableNormalizationValue_t1779412570  value)
	{
		___Position_0 = value;
	}

	inline static int32_t get_offset_of_Angle_1() { return static_cast<int32_t>(offsetof(SerializableNormalization_t793291037, ___Angle_1)); }
	inline SerializableNormalizationValue_t1779412570  get_Angle_1() const { return ___Angle_1; }
	inline SerializableNormalizationValue_t1779412570 * get_address_of_Angle_1() { return &___Angle_1; }
	inline void set_Angle_1(SerializableNormalizationValue_t1779412570  value)
	{
		___Angle_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SERIALIZABLENORMALIZATION_T793291037_H
#ifndef CUBISMAUDIOSAMPLINGQUALITY_T1105222369_H
#define CUBISMAUDIOSAMPLINGQUALITY_T1105222369_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Live2D.Cubism.Framework.MouthMovement.CubismAudioSamplingQuality
struct  CubismAudioSamplingQuality_t1105222369 
{
public:
	// System.Int32 Live2D.Cubism.Framework.MouthMovement.CubismAudioSamplingQuality::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(CubismAudioSamplingQuality_t1105222369, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CUBISMAUDIOSAMPLINGQUALITY_T1105222369_H
#ifndef SERIALIZABLEEFFECTIVEFORCES_T82278831_H
#define SERIALIZABLEEFFECTIVEFORCES_T82278831_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Live2D.Cubism.Framework.Json.CubismPhysics3Json/SerializableEffectiveForces
struct  SerializableEffectiveForces_t82278831 
{
public:
	// Live2D.Cubism.Framework.Json.CubismPhysics3Json/SerializableVector2 Live2D.Cubism.Framework.Json.CubismPhysics3Json/SerializableEffectiveForces::Gravity
	SerializableVector2_t65906335  ___Gravity_0;
	// Live2D.Cubism.Framework.Json.CubismPhysics3Json/SerializableVector2 Live2D.Cubism.Framework.Json.CubismPhysics3Json/SerializableEffectiveForces::Wind
	SerializableVector2_t65906335  ___Wind_1;

public:
	inline static int32_t get_offset_of_Gravity_0() { return static_cast<int32_t>(offsetof(SerializableEffectiveForces_t82278831, ___Gravity_0)); }
	inline SerializableVector2_t65906335  get_Gravity_0() const { return ___Gravity_0; }
	inline SerializableVector2_t65906335 * get_address_of_Gravity_0() { return &___Gravity_0; }
	inline void set_Gravity_0(SerializableVector2_t65906335  value)
	{
		___Gravity_0 = value;
	}

	inline static int32_t get_offset_of_Wind_1() { return static_cast<int32_t>(offsetof(SerializableEffectiveForces_t82278831, ___Wind_1)); }
	inline SerializableVector2_t65906335  get_Wind_1() const { return ___Wind_1; }
	inline SerializableVector2_t65906335 * get_address_of_Wind_1() { return &___Wind_1; }
	inline void set_Wind_1(SerializableVector2_t65906335  value)
	{
		___Wind_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SERIALIZABLEEFFECTIVEFORCES_T82278831_H
#ifndef CUBISMMOTION3JSON_T3423029264_H
#define CUBISMMOTION3JSON_T3423029264_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Live2D.Cubism.Framework.Json.CubismMotion3Json
struct  CubismMotion3Json_t3423029264  : public RuntimeObject
{
public:
	// System.Int32 Live2D.Cubism.Framework.Json.CubismMotion3Json::Version
	int32_t ___Version_0;
	// Live2D.Cubism.Framework.Json.CubismMotion3Json/SerializableMeta Live2D.Cubism.Framework.Json.CubismMotion3Json::Meta
	SerializableMeta_t2740270858  ___Meta_1;
	// Live2D.Cubism.Framework.Json.CubismMotion3Json/SerializableCurve[] Live2D.Cubism.Framework.Json.CubismMotion3Json::Curves
	SerializableCurveU5BU5D_t1674681539* ___Curves_2;

public:
	inline static int32_t get_offset_of_Version_0() { return static_cast<int32_t>(offsetof(CubismMotion3Json_t3423029264, ___Version_0)); }
	inline int32_t get_Version_0() const { return ___Version_0; }
	inline int32_t* get_address_of_Version_0() { return &___Version_0; }
	inline void set_Version_0(int32_t value)
	{
		___Version_0 = value;
	}

	inline static int32_t get_offset_of_Meta_1() { return static_cast<int32_t>(offsetof(CubismMotion3Json_t3423029264, ___Meta_1)); }
	inline SerializableMeta_t2740270858  get_Meta_1() const { return ___Meta_1; }
	inline SerializableMeta_t2740270858 * get_address_of_Meta_1() { return &___Meta_1; }
	inline void set_Meta_1(SerializableMeta_t2740270858  value)
	{
		___Meta_1 = value;
	}

	inline static int32_t get_offset_of_Curves_2() { return static_cast<int32_t>(offsetof(CubismMotion3Json_t3423029264, ___Curves_2)); }
	inline SerializableCurveU5BU5D_t1674681539* get_Curves_2() const { return ___Curves_2; }
	inline SerializableCurveU5BU5D_t1674681539** get_address_of_Curves_2() { return &___Curves_2; }
	inline void set_Curves_2(SerializableCurveU5BU5D_t1674681539* value)
	{
		___Curves_2 = value;
		Il2CppCodeGenWriteBarrier((&___Curves_2), value);
	}
};

struct CubismMotion3Json_t3423029264_StaticFields
{
public:
	// System.Collections.Generic.Dictionary`2<System.Single,Live2D.Cubism.Framework.Json.CubismMotion3Json/SegmentParser> Live2D.Cubism.Framework.Json.CubismMotion3Json::Parsers
	Dictionary_2_t1306250649 * ___Parsers_4;
	// Live2D.Cubism.Framework.Json.CubismMotion3Json/SegmentParser Live2D.Cubism.Framework.Json.CubismMotion3Json::<>f__mg$cache0
	SegmentParser_t456729962 * ___U3CU3Ef__mgU24cache0_5;
	// Live2D.Cubism.Framework.Json.CubismMotion3Json/SegmentParser Live2D.Cubism.Framework.Json.CubismMotion3Json::<>f__mg$cache1
	SegmentParser_t456729962 * ___U3CU3Ef__mgU24cache1_6;
	// Live2D.Cubism.Framework.Json.CubismMotion3Json/SegmentParser Live2D.Cubism.Framework.Json.CubismMotion3Json::<>f__mg$cache2
	SegmentParser_t456729962 * ___U3CU3Ef__mgU24cache2_7;
	// Live2D.Cubism.Framework.Json.CubismMotion3Json/SegmentParser Live2D.Cubism.Framework.Json.CubismMotion3Json::<>f__mg$cache3
	SegmentParser_t456729962 * ___U3CU3Ef__mgU24cache3_8;

public:
	inline static int32_t get_offset_of_Parsers_4() { return static_cast<int32_t>(offsetof(CubismMotion3Json_t3423029264_StaticFields, ___Parsers_4)); }
	inline Dictionary_2_t1306250649 * get_Parsers_4() const { return ___Parsers_4; }
	inline Dictionary_2_t1306250649 ** get_address_of_Parsers_4() { return &___Parsers_4; }
	inline void set_Parsers_4(Dictionary_2_t1306250649 * value)
	{
		___Parsers_4 = value;
		Il2CppCodeGenWriteBarrier((&___Parsers_4), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache0_5() { return static_cast<int32_t>(offsetof(CubismMotion3Json_t3423029264_StaticFields, ___U3CU3Ef__mgU24cache0_5)); }
	inline SegmentParser_t456729962 * get_U3CU3Ef__mgU24cache0_5() const { return ___U3CU3Ef__mgU24cache0_5; }
	inline SegmentParser_t456729962 ** get_address_of_U3CU3Ef__mgU24cache0_5() { return &___U3CU3Ef__mgU24cache0_5; }
	inline void set_U3CU3Ef__mgU24cache0_5(SegmentParser_t456729962 * value)
	{
		___U3CU3Ef__mgU24cache0_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache0_5), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache1_6() { return static_cast<int32_t>(offsetof(CubismMotion3Json_t3423029264_StaticFields, ___U3CU3Ef__mgU24cache1_6)); }
	inline SegmentParser_t456729962 * get_U3CU3Ef__mgU24cache1_6() const { return ___U3CU3Ef__mgU24cache1_6; }
	inline SegmentParser_t456729962 ** get_address_of_U3CU3Ef__mgU24cache1_6() { return &___U3CU3Ef__mgU24cache1_6; }
	inline void set_U3CU3Ef__mgU24cache1_6(SegmentParser_t456729962 * value)
	{
		___U3CU3Ef__mgU24cache1_6 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache1_6), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache2_7() { return static_cast<int32_t>(offsetof(CubismMotion3Json_t3423029264_StaticFields, ___U3CU3Ef__mgU24cache2_7)); }
	inline SegmentParser_t456729962 * get_U3CU3Ef__mgU24cache2_7() const { return ___U3CU3Ef__mgU24cache2_7; }
	inline SegmentParser_t456729962 ** get_address_of_U3CU3Ef__mgU24cache2_7() { return &___U3CU3Ef__mgU24cache2_7; }
	inline void set_U3CU3Ef__mgU24cache2_7(SegmentParser_t456729962 * value)
	{
		___U3CU3Ef__mgU24cache2_7 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache2_7), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache3_8() { return static_cast<int32_t>(offsetof(CubismMotion3Json_t3423029264_StaticFields, ___U3CU3Ef__mgU24cache3_8)); }
	inline SegmentParser_t456729962 * get_U3CU3Ef__mgU24cache3_8() const { return ___U3CU3Ef__mgU24cache3_8; }
	inline SegmentParser_t456729962 ** get_address_of_U3CU3Ef__mgU24cache3_8() { return &___U3CU3Ef__mgU24cache3_8; }
	inline void set_U3CU3Ef__mgU24cache3_8(SegmentParser_t456729962 * value)
	{
		___U3CU3Ef__mgU24cache3_8 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache3_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CUBISMMOTION3JSON_T3423029264_H
#ifndef CUBISMUNMANAGEDMODEL_T586274666_H
#define CUBISMUNMANAGEDMODEL_T586274666_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Live2D.Cubism.Core.Unmanaged.CubismUnmanagedModel
struct  CubismUnmanagedModel_t586274666  : public RuntimeObject
{
public:
	// Live2D.Cubism.Core.Unmanaged.CubismUnmanagedParameters Live2D.Cubism.Core.Unmanaged.CubismUnmanagedModel::<Parameters>k__BackingField
	CubismUnmanagedParameters_t2764188193 * ___U3CParametersU3Ek__BackingField_0;
	// Live2D.Cubism.Core.Unmanaged.CubismUnmanagedParts Live2D.Cubism.Core.Unmanaged.CubismUnmanagedModel::<Parts>k__BackingField
	CubismUnmanagedParts_t1124036275 * ___U3CPartsU3Ek__BackingField_1;
	// Live2D.Cubism.Core.Unmanaged.CubismUnmanagedDrawables Live2D.Cubism.Core.Unmanaged.CubismUnmanagedModel::<Drawables>k__BackingField
	CubismUnmanagedDrawables_t771654660 * ___U3CDrawablesU3Ek__BackingField_2;
	// System.IntPtr Live2D.Cubism.Core.Unmanaged.CubismUnmanagedModel::<Ptr>k__BackingField
	intptr_t ___U3CPtrU3Ek__BackingField_3;

public:
	inline static int32_t get_offset_of_U3CParametersU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(CubismUnmanagedModel_t586274666, ___U3CParametersU3Ek__BackingField_0)); }
	inline CubismUnmanagedParameters_t2764188193 * get_U3CParametersU3Ek__BackingField_0() const { return ___U3CParametersU3Ek__BackingField_0; }
	inline CubismUnmanagedParameters_t2764188193 ** get_address_of_U3CParametersU3Ek__BackingField_0() { return &___U3CParametersU3Ek__BackingField_0; }
	inline void set_U3CParametersU3Ek__BackingField_0(CubismUnmanagedParameters_t2764188193 * value)
	{
		___U3CParametersU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CParametersU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_U3CPartsU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(CubismUnmanagedModel_t586274666, ___U3CPartsU3Ek__BackingField_1)); }
	inline CubismUnmanagedParts_t1124036275 * get_U3CPartsU3Ek__BackingField_1() const { return ___U3CPartsU3Ek__BackingField_1; }
	inline CubismUnmanagedParts_t1124036275 ** get_address_of_U3CPartsU3Ek__BackingField_1() { return &___U3CPartsU3Ek__BackingField_1; }
	inline void set_U3CPartsU3Ek__BackingField_1(CubismUnmanagedParts_t1124036275 * value)
	{
		___U3CPartsU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CPartsU3Ek__BackingField_1), value);
	}

	inline static int32_t get_offset_of_U3CDrawablesU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(CubismUnmanagedModel_t586274666, ___U3CDrawablesU3Ek__BackingField_2)); }
	inline CubismUnmanagedDrawables_t771654660 * get_U3CDrawablesU3Ek__BackingField_2() const { return ___U3CDrawablesU3Ek__BackingField_2; }
	inline CubismUnmanagedDrawables_t771654660 ** get_address_of_U3CDrawablesU3Ek__BackingField_2() { return &___U3CDrawablesU3Ek__BackingField_2; }
	inline void set_U3CDrawablesU3Ek__BackingField_2(CubismUnmanagedDrawables_t771654660 * value)
	{
		___U3CDrawablesU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CDrawablesU3Ek__BackingField_2), value);
	}

	inline static int32_t get_offset_of_U3CPtrU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(CubismUnmanagedModel_t586274666, ___U3CPtrU3Ek__BackingField_3)); }
	inline intptr_t get_U3CPtrU3Ek__BackingField_3() const { return ___U3CPtrU3Ek__BackingField_3; }
	inline intptr_t* get_address_of_U3CPtrU3Ek__BackingField_3() { return &___U3CPtrU3Ek__BackingField_3; }
	inline void set_U3CPtrU3Ek__BackingField_3(intptr_t value)
	{
		___U3CPtrU3Ek__BackingField_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CUBISMUNMANAGEDMODEL_T586274666_H
#ifndef CUBISMLOOKAXIS_T3392553031_H
#define CUBISMLOOKAXIS_T3392553031_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Live2D.Cubism.Framework.LookAt.CubismLookAxis
struct  CubismLookAxis_t3392553031 
{
public:
	// System.Int32 Live2D.Cubism.Framework.LookAt.CubismLookAxis::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(CubismLookAxis_t3392553031, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CUBISMLOOKAXIS_T3392553031_H
#ifndef PHASE_T3766709578_H
#define PHASE_T3766709578_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Live2D.Cubism.Framework.CubismAutoEyeBlinkInput/Phase
struct  Phase_t3766709578 
{
public:
	// System.Int32 Live2D.Cubism.Framework.CubismAutoEyeBlinkInput/Phase::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(Phase_t3766709578, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PHASE_T3766709578_H
#ifndef CUBISMPARAMETERBLENDMODE_T532829278_H
#define CUBISMPARAMETERBLENDMODE_T532829278_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Live2D.Cubism.Framework.CubismParameterBlendMode
struct  CubismParameterBlendMode_t532829278 
{
public:
	// System.Int32 Live2D.Cubism.Framework.CubismParameterBlendMode::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(CubismParameterBlendMode_t532829278, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CUBISMPARAMETERBLENDMODE_T532829278_H
#ifndef CUBISMMODEL3JSON_T1938818895_H
#define CUBISMMODEL3JSON_T1938818895_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Live2D.Cubism.Framework.Json.CubismModel3Json
struct  CubismModel3Json_t1938818895  : public RuntimeObject
{
public:
	// System.String Live2D.Cubism.Framework.Json.CubismModel3Json::<AssetPath>k__BackingField
	String_t* ___U3CAssetPathU3Ek__BackingField_0;
	// Live2D.Cubism.Framework.Json.CubismModel3Json/LoadAssetAtPathHandler Live2D.Cubism.Framework.Json.CubismModel3Json::<LoadAssetAtPath>k__BackingField
	LoadAssetAtPathHandler_t1714529023 * ___U3CLoadAssetAtPathU3Ek__BackingField_1;
	// System.Int32 Live2D.Cubism.Framework.Json.CubismModel3Json::Version
	int32_t ___Version_2;
	// Live2D.Cubism.Framework.Json.CubismModel3Json/SerializableFileReferences Live2D.Cubism.Framework.Json.CubismModel3Json::FileReferences
	SerializableFileReferences_t1075727736  ___FileReferences_3;
	// Live2D.Cubism.Framework.Json.CubismModel3Json/SerializableGroup[] Live2D.Cubism.Framework.Json.CubismModel3Json::Groups
	SerializableGroupU5BU5D_t4046225562* ___Groups_4;
	// UnityEngine.Texture2D[] Live2D.Cubism.Framework.Json.CubismModel3Json::_textures
	Texture2DU5BU5D_t2724090252* ____textures_5;

public:
	inline static int32_t get_offset_of_U3CAssetPathU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(CubismModel3Json_t1938818895, ___U3CAssetPathU3Ek__BackingField_0)); }
	inline String_t* get_U3CAssetPathU3Ek__BackingField_0() const { return ___U3CAssetPathU3Ek__BackingField_0; }
	inline String_t** get_address_of_U3CAssetPathU3Ek__BackingField_0() { return &___U3CAssetPathU3Ek__BackingField_0; }
	inline void set_U3CAssetPathU3Ek__BackingField_0(String_t* value)
	{
		___U3CAssetPathU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CAssetPathU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_U3CLoadAssetAtPathU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(CubismModel3Json_t1938818895, ___U3CLoadAssetAtPathU3Ek__BackingField_1)); }
	inline LoadAssetAtPathHandler_t1714529023 * get_U3CLoadAssetAtPathU3Ek__BackingField_1() const { return ___U3CLoadAssetAtPathU3Ek__BackingField_1; }
	inline LoadAssetAtPathHandler_t1714529023 ** get_address_of_U3CLoadAssetAtPathU3Ek__BackingField_1() { return &___U3CLoadAssetAtPathU3Ek__BackingField_1; }
	inline void set_U3CLoadAssetAtPathU3Ek__BackingField_1(LoadAssetAtPathHandler_t1714529023 * value)
	{
		___U3CLoadAssetAtPathU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CLoadAssetAtPathU3Ek__BackingField_1), value);
	}

	inline static int32_t get_offset_of_Version_2() { return static_cast<int32_t>(offsetof(CubismModel3Json_t1938818895, ___Version_2)); }
	inline int32_t get_Version_2() const { return ___Version_2; }
	inline int32_t* get_address_of_Version_2() { return &___Version_2; }
	inline void set_Version_2(int32_t value)
	{
		___Version_2 = value;
	}

	inline static int32_t get_offset_of_FileReferences_3() { return static_cast<int32_t>(offsetof(CubismModel3Json_t1938818895, ___FileReferences_3)); }
	inline SerializableFileReferences_t1075727736  get_FileReferences_3() const { return ___FileReferences_3; }
	inline SerializableFileReferences_t1075727736 * get_address_of_FileReferences_3() { return &___FileReferences_3; }
	inline void set_FileReferences_3(SerializableFileReferences_t1075727736  value)
	{
		___FileReferences_3 = value;
	}

	inline static int32_t get_offset_of_Groups_4() { return static_cast<int32_t>(offsetof(CubismModel3Json_t1938818895, ___Groups_4)); }
	inline SerializableGroupU5BU5D_t4046225562* get_Groups_4() const { return ___Groups_4; }
	inline SerializableGroupU5BU5D_t4046225562** get_address_of_Groups_4() { return &___Groups_4; }
	inline void set_Groups_4(SerializableGroupU5BU5D_t4046225562* value)
	{
		___Groups_4 = value;
		Il2CppCodeGenWriteBarrier((&___Groups_4), value);
	}

	inline static int32_t get_offset_of__textures_5() { return static_cast<int32_t>(offsetof(CubismModel3Json_t1938818895, ____textures_5)); }
	inline Texture2DU5BU5D_t2724090252* get__textures_5() const { return ____textures_5; }
	inline Texture2DU5BU5D_t2724090252** get_address_of__textures_5() { return &____textures_5; }
	inline void set__textures_5(Texture2DU5BU5D_t2724090252* value)
	{
		____textures_5 = value;
		Il2CppCodeGenWriteBarrier((&____textures_5), value);
	}
};

struct CubismModel3Json_t1938818895_StaticFields
{
public:
	// Live2D.Cubism.Framework.Json.CubismModel3Json/LoadAssetAtPathHandler Live2D.Cubism.Framework.Json.CubismModel3Json::<>f__mg$cache0
	LoadAssetAtPathHandler_t1714529023 * ___U3CU3Ef__mgU24cache0_6;
	// Live2D.Cubism.Framework.Json.CubismModel3Json/MaterialPicker Live2D.Cubism.Framework.Json.CubismModel3Json::<>f__mg$cache1
	MaterialPicker_t1430258468 * ___U3CU3Ef__mgU24cache1_7;
	// Live2D.Cubism.Framework.Json.CubismModel3Json/TexturePicker Live2D.Cubism.Framework.Json.CubismModel3Json::<>f__mg$cache2
	TexturePicker_t1644688768 * ___U3CU3Ef__mgU24cache2_8;

public:
	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache0_6() { return static_cast<int32_t>(offsetof(CubismModel3Json_t1938818895_StaticFields, ___U3CU3Ef__mgU24cache0_6)); }
	inline LoadAssetAtPathHandler_t1714529023 * get_U3CU3Ef__mgU24cache0_6() const { return ___U3CU3Ef__mgU24cache0_6; }
	inline LoadAssetAtPathHandler_t1714529023 ** get_address_of_U3CU3Ef__mgU24cache0_6() { return &___U3CU3Ef__mgU24cache0_6; }
	inline void set_U3CU3Ef__mgU24cache0_6(LoadAssetAtPathHandler_t1714529023 * value)
	{
		___U3CU3Ef__mgU24cache0_6 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache0_6), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache1_7() { return static_cast<int32_t>(offsetof(CubismModel3Json_t1938818895_StaticFields, ___U3CU3Ef__mgU24cache1_7)); }
	inline MaterialPicker_t1430258468 * get_U3CU3Ef__mgU24cache1_7() const { return ___U3CU3Ef__mgU24cache1_7; }
	inline MaterialPicker_t1430258468 ** get_address_of_U3CU3Ef__mgU24cache1_7() { return &___U3CU3Ef__mgU24cache1_7; }
	inline void set_U3CU3Ef__mgU24cache1_7(MaterialPicker_t1430258468 * value)
	{
		___U3CU3Ef__mgU24cache1_7 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache1_7), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache2_8() { return static_cast<int32_t>(offsetof(CubismModel3Json_t1938818895_StaticFields, ___U3CU3Ef__mgU24cache2_8)); }
	inline TexturePicker_t1644688768 * get_U3CU3Ef__mgU24cache2_8() const { return ___U3CU3Ef__mgU24cache2_8; }
	inline TexturePicker_t1644688768 ** get_address_of_U3CU3Ef__mgU24cache2_8() { return &___U3CU3Ef__mgU24cache2_8; }
	inline void set_U3CU3Ef__mgU24cache2_8(TexturePicker_t1644688768 * value)
	{
		___U3CU3Ef__mgU24cache2_8 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache2_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CUBISMMODEL3JSON_T1938818895_H
#ifndef CUBISMHARMONICMOTIONDIRECTION_T2553246843_H
#define CUBISMHARMONICMOTIONDIRECTION_T2553246843_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Live2D.Cubism.Framework.HarmonicMotion.CubismHarmonicMotionDirection
struct  CubismHarmonicMotionDirection_t2553246843 
{
public:
	// System.Int32 Live2D.Cubism.Framework.HarmonicMotion.CubismHarmonicMotionDirection::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(CubismHarmonicMotionDirection_t2553246843, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CUBISMHARMONICMOTIONDIRECTION_T2553246843_H
#ifndef SERIALIZABLEMETA_T1007752097_H
#define SERIALIZABLEMETA_T1007752097_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Live2D.Cubism.Framework.Json.CubismPhysics3Json/SerializableMeta
struct  SerializableMeta_t1007752097 
{
public:
	// System.Int32 Live2D.Cubism.Framework.Json.CubismPhysics3Json/SerializableMeta::PhysicsSettingCount
	int32_t ___PhysicsSettingCount_0;
	// System.Int32 Live2D.Cubism.Framework.Json.CubismPhysics3Json/SerializableMeta::TotalInputCount
	int32_t ___TotalInputCount_1;
	// System.Int32 Live2D.Cubism.Framework.Json.CubismPhysics3Json/SerializableMeta::TotalOutputCount
	int32_t ___TotalOutputCount_2;
	// System.Int32 Live2D.Cubism.Framework.Json.CubismPhysics3Json/SerializableMeta::TotalVertexCount
	int32_t ___TotalVertexCount_3;
	// Live2D.Cubism.Framework.Json.CubismPhysics3Json/SerializableEffectiveForces Live2D.Cubism.Framework.Json.CubismPhysics3Json/SerializableMeta::EffectiveForces
	SerializableEffectiveForces_t82278831  ___EffectiveForces_4;

public:
	inline static int32_t get_offset_of_PhysicsSettingCount_0() { return static_cast<int32_t>(offsetof(SerializableMeta_t1007752097, ___PhysicsSettingCount_0)); }
	inline int32_t get_PhysicsSettingCount_0() const { return ___PhysicsSettingCount_0; }
	inline int32_t* get_address_of_PhysicsSettingCount_0() { return &___PhysicsSettingCount_0; }
	inline void set_PhysicsSettingCount_0(int32_t value)
	{
		___PhysicsSettingCount_0 = value;
	}

	inline static int32_t get_offset_of_TotalInputCount_1() { return static_cast<int32_t>(offsetof(SerializableMeta_t1007752097, ___TotalInputCount_1)); }
	inline int32_t get_TotalInputCount_1() const { return ___TotalInputCount_1; }
	inline int32_t* get_address_of_TotalInputCount_1() { return &___TotalInputCount_1; }
	inline void set_TotalInputCount_1(int32_t value)
	{
		___TotalInputCount_1 = value;
	}

	inline static int32_t get_offset_of_TotalOutputCount_2() { return static_cast<int32_t>(offsetof(SerializableMeta_t1007752097, ___TotalOutputCount_2)); }
	inline int32_t get_TotalOutputCount_2() const { return ___TotalOutputCount_2; }
	inline int32_t* get_address_of_TotalOutputCount_2() { return &___TotalOutputCount_2; }
	inline void set_TotalOutputCount_2(int32_t value)
	{
		___TotalOutputCount_2 = value;
	}

	inline static int32_t get_offset_of_TotalVertexCount_3() { return static_cast<int32_t>(offsetof(SerializableMeta_t1007752097, ___TotalVertexCount_3)); }
	inline int32_t get_TotalVertexCount_3() const { return ___TotalVertexCount_3; }
	inline int32_t* get_address_of_TotalVertexCount_3() { return &___TotalVertexCount_3; }
	inline void set_TotalVertexCount_3(int32_t value)
	{
		___TotalVertexCount_3 = value;
	}

	inline static int32_t get_offset_of_EffectiveForces_4() { return static_cast<int32_t>(offsetof(SerializableMeta_t1007752097, ___EffectiveForces_4)); }
	inline SerializableEffectiveForces_t82278831  get_EffectiveForces_4() const { return ___EffectiveForces_4; }
	inline SerializableEffectiveForces_t82278831 * get_address_of_EffectiveForces_4() { return &___EffectiveForces_4; }
	inline void set_EffectiveForces_4(SerializableEffectiveForces_t82278831  value)
	{
		___EffectiveForces_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SERIALIZABLEMETA_T1007752097_H
#ifndef SERIALIZABLEPHYSICSSETTINGS_T496799432_H
#define SERIALIZABLEPHYSICSSETTINGS_T496799432_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Live2D.Cubism.Framework.Json.CubismPhysics3Json/SerializablePhysicsSettings
struct  SerializablePhysicsSettings_t496799432 
{
public:
	// System.String Live2D.Cubism.Framework.Json.CubismPhysics3Json/SerializablePhysicsSettings::Id
	String_t* ___Id_0;
	// Live2D.Cubism.Framework.Json.CubismPhysics3Json/SerializableInput[] Live2D.Cubism.Framework.Json.CubismPhysics3Json/SerializablePhysicsSettings::Input
	SerializableInputU5BU5D_t2341458125* ___Input_1;
	// Live2D.Cubism.Framework.Json.CubismPhysics3Json/SerializableOutput[] Live2D.Cubism.Framework.Json.CubismPhysics3Json/SerializablePhysicsSettings::Output
	SerializableOutputU5BU5D_t1536831822* ___Output_2;
	// Live2D.Cubism.Framework.Json.CubismPhysics3Json/SerializableVertex[] Live2D.Cubism.Framework.Json.CubismPhysics3Json/SerializablePhysicsSettings::Vertices
	SerializableVertexU5BU5D_t2006479567* ___Vertices_3;
	// Live2D.Cubism.Framework.Json.CubismPhysics3Json/SerializableNormalization Live2D.Cubism.Framework.Json.CubismPhysics3Json/SerializablePhysicsSettings::Normalization
	SerializableNormalization_t793291037  ___Normalization_4;

public:
	inline static int32_t get_offset_of_Id_0() { return static_cast<int32_t>(offsetof(SerializablePhysicsSettings_t496799432, ___Id_0)); }
	inline String_t* get_Id_0() const { return ___Id_0; }
	inline String_t** get_address_of_Id_0() { return &___Id_0; }
	inline void set_Id_0(String_t* value)
	{
		___Id_0 = value;
		Il2CppCodeGenWriteBarrier((&___Id_0), value);
	}

	inline static int32_t get_offset_of_Input_1() { return static_cast<int32_t>(offsetof(SerializablePhysicsSettings_t496799432, ___Input_1)); }
	inline SerializableInputU5BU5D_t2341458125* get_Input_1() const { return ___Input_1; }
	inline SerializableInputU5BU5D_t2341458125** get_address_of_Input_1() { return &___Input_1; }
	inline void set_Input_1(SerializableInputU5BU5D_t2341458125* value)
	{
		___Input_1 = value;
		Il2CppCodeGenWriteBarrier((&___Input_1), value);
	}

	inline static int32_t get_offset_of_Output_2() { return static_cast<int32_t>(offsetof(SerializablePhysicsSettings_t496799432, ___Output_2)); }
	inline SerializableOutputU5BU5D_t1536831822* get_Output_2() const { return ___Output_2; }
	inline SerializableOutputU5BU5D_t1536831822** get_address_of_Output_2() { return &___Output_2; }
	inline void set_Output_2(SerializableOutputU5BU5D_t1536831822* value)
	{
		___Output_2 = value;
		Il2CppCodeGenWriteBarrier((&___Output_2), value);
	}

	inline static int32_t get_offset_of_Vertices_3() { return static_cast<int32_t>(offsetof(SerializablePhysicsSettings_t496799432, ___Vertices_3)); }
	inline SerializableVertexU5BU5D_t2006479567* get_Vertices_3() const { return ___Vertices_3; }
	inline SerializableVertexU5BU5D_t2006479567** get_address_of_Vertices_3() { return &___Vertices_3; }
	inline void set_Vertices_3(SerializableVertexU5BU5D_t2006479567* value)
	{
		___Vertices_3 = value;
		Il2CppCodeGenWriteBarrier((&___Vertices_3), value);
	}

	inline static int32_t get_offset_of_Normalization_4() { return static_cast<int32_t>(offsetof(SerializablePhysicsSettings_t496799432, ___Normalization_4)); }
	inline SerializableNormalization_t793291037  get_Normalization_4() const { return ___Normalization_4; }
	inline SerializableNormalization_t793291037 * get_address_of_Normalization_4() { return &___Normalization_4; }
	inline void set_Normalization_4(SerializableNormalization_t793291037  value)
	{
		___Normalization_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of Live2D.Cubism.Framework.Json.CubismPhysics3Json/SerializablePhysicsSettings
struct SerializablePhysicsSettings_t496799432_marshaled_pinvoke
{
	char* ___Id_0;
	SerializableInput_t1668668964_marshaled_pinvoke* ___Input_1;
	SerializableOutput_t2803511543_marshaled_pinvoke* ___Output_2;
	SerializableVertex_t1882814154 * ___Vertices_3;
	SerializableNormalization_t793291037  ___Normalization_4;
};
// Native definition for COM marshalling of Live2D.Cubism.Framework.Json.CubismPhysics3Json/SerializablePhysicsSettings
struct SerializablePhysicsSettings_t496799432_marshaled_com
{
	Il2CppChar* ___Id_0;
	SerializableInput_t1668668964_marshaled_com* ___Input_1;
	SerializableOutput_t2803511543_marshaled_com* ___Output_2;
	SerializableVertex_t1882814154 * ___Vertices_3;
	SerializableNormalization_t793291037  ___Normalization_4;
};
#endif // SERIALIZABLEPHYSICSSETTINGS_T496799432_H
#ifndef CUBISMMASKMASKEDJUNCTION_T3351726490_H
#define CUBISMMASKMASKEDJUNCTION_T3351726490_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Live2D.Cubism.Rendering.Masking.CubismMaskMaskedJunction
struct  CubismMaskMaskedJunction_t3351726490  : public RuntimeObject
{
public:
	// Live2D.Cubism.Rendering.Masking.CubismMaskRenderer[] Live2D.Cubism.Rendering.Masking.CubismMaskMaskedJunction::<Masks>k__BackingField
	CubismMaskRendererU5BU5D_t3366738177* ___U3CMasksU3Ek__BackingField_1;
	// Live2D.Cubism.Rendering.CubismRenderer[] Live2D.Cubism.Rendering.Masking.CubismMaskMaskedJunction::<Maskeds>k__BackingField
	CubismRendererU5BU5D_t2721529825* ___U3CMaskedsU3Ek__BackingField_2;
	// Live2D.Cubism.Rendering.Masking.CubismMaskTexture Live2D.Cubism.Rendering.Masking.CubismMaskMaskedJunction::<MaskTexture>k__BackingField
	CubismMaskTexture_t949515734 * ___U3CMaskTextureU3Ek__BackingField_3;
	// Live2D.Cubism.Rendering.Masking.CubismMaskTile Live2D.Cubism.Rendering.Masking.CubismMaskMaskedJunction::<MaskTile>k__BackingField
	CubismMaskTile_t3996512637  ___U3CMaskTileU3Ek__BackingField_4;
	// Live2D.Cubism.Rendering.Masking.CubismMaskTransform Live2D.Cubism.Rendering.Masking.CubismMaskMaskedJunction::<MaskTransform>k__BackingField
	CubismMaskTransform_t2215722789  ___U3CMaskTransformU3Ek__BackingField_5;

public:
	inline static int32_t get_offset_of_U3CMasksU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(CubismMaskMaskedJunction_t3351726490, ___U3CMasksU3Ek__BackingField_1)); }
	inline CubismMaskRendererU5BU5D_t3366738177* get_U3CMasksU3Ek__BackingField_1() const { return ___U3CMasksU3Ek__BackingField_1; }
	inline CubismMaskRendererU5BU5D_t3366738177** get_address_of_U3CMasksU3Ek__BackingField_1() { return &___U3CMasksU3Ek__BackingField_1; }
	inline void set_U3CMasksU3Ek__BackingField_1(CubismMaskRendererU5BU5D_t3366738177* value)
	{
		___U3CMasksU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CMasksU3Ek__BackingField_1), value);
	}

	inline static int32_t get_offset_of_U3CMaskedsU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(CubismMaskMaskedJunction_t3351726490, ___U3CMaskedsU3Ek__BackingField_2)); }
	inline CubismRendererU5BU5D_t2721529825* get_U3CMaskedsU3Ek__BackingField_2() const { return ___U3CMaskedsU3Ek__BackingField_2; }
	inline CubismRendererU5BU5D_t2721529825** get_address_of_U3CMaskedsU3Ek__BackingField_2() { return &___U3CMaskedsU3Ek__BackingField_2; }
	inline void set_U3CMaskedsU3Ek__BackingField_2(CubismRendererU5BU5D_t2721529825* value)
	{
		___U3CMaskedsU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CMaskedsU3Ek__BackingField_2), value);
	}

	inline static int32_t get_offset_of_U3CMaskTextureU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(CubismMaskMaskedJunction_t3351726490, ___U3CMaskTextureU3Ek__BackingField_3)); }
	inline CubismMaskTexture_t949515734 * get_U3CMaskTextureU3Ek__BackingField_3() const { return ___U3CMaskTextureU3Ek__BackingField_3; }
	inline CubismMaskTexture_t949515734 ** get_address_of_U3CMaskTextureU3Ek__BackingField_3() { return &___U3CMaskTextureU3Ek__BackingField_3; }
	inline void set_U3CMaskTextureU3Ek__BackingField_3(CubismMaskTexture_t949515734 * value)
	{
		___U3CMaskTextureU3Ek__BackingField_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CMaskTextureU3Ek__BackingField_3), value);
	}

	inline static int32_t get_offset_of_U3CMaskTileU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(CubismMaskMaskedJunction_t3351726490, ___U3CMaskTileU3Ek__BackingField_4)); }
	inline CubismMaskTile_t3996512637  get_U3CMaskTileU3Ek__BackingField_4() const { return ___U3CMaskTileU3Ek__BackingField_4; }
	inline CubismMaskTile_t3996512637 * get_address_of_U3CMaskTileU3Ek__BackingField_4() { return &___U3CMaskTileU3Ek__BackingField_4; }
	inline void set_U3CMaskTileU3Ek__BackingField_4(CubismMaskTile_t3996512637  value)
	{
		___U3CMaskTileU3Ek__BackingField_4 = value;
	}

	inline static int32_t get_offset_of_U3CMaskTransformU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(CubismMaskMaskedJunction_t3351726490, ___U3CMaskTransformU3Ek__BackingField_5)); }
	inline CubismMaskTransform_t2215722789  get_U3CMaskTransformU3Ek__BackingField_5() const { return ___U3CMaskTransformU3Ek__BackingField_5; }
	inline CubismMaskTransform_t2215722789 * get_address_of_U3CMaskTransformU3Ek__BackingField_5() { return &___U3CMaskTransformU3Ek__BackingField_5; }
	inline void set_U3CMaskTransformU3Ek__BackingField_5(CubismMaskTransform_t2215722789  value)
	{
		___U3CMaskTransformU3Ek__BackingField_5 = value;
	}
};

struct CubismMaskMaskedJunction_t3351726490_StaticFields
{
public:
	// Live2D.Cubism.Rendering.Masking.CubismMaskProperties Live2D.Cubism.Rendering.Masking.CubismMaskMaskedJunction::<SharedMaskProperties>k__BackingField
	CubismMaskProperties_t4215298192 * ___U3CSharedMaskPropertiesU3Ek__BackingField_0;

public:
	inline static int32_t get_offset_of_U3CSharedMaskPropertiesU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(CubismMaskMaskedJunction_t3351726490_StaticFields, ___U3CSharedMaskPropertiesU3Ek__BackingField_0)); }
	inline CubismMaskProperties_t4215298192 * get_U3CSharedMaskPropertiesU3Ek__BackingField_0() const { return ___U3CSharedMaskPropertiesU3Ek__BackingField_0; }
	inline CubismMaskProperties_t4215298192 ** get_address_of_U3CSharedMaskPropertiesU3Ek__BackingField_0() { return &___U3CSharedMaskPropertiesU3Ek__BackingField_0; }
	inline void set_U3CSharedMaskPropertiesU3Ek__BackingField_0(CubismMaskProperties_t4215298192 * value)
	{
		___U3CSharedMaskPropertiesU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CSharedMaskPropertiesU3Ek__BackingField_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CUBISMMASKMASKEDJUNCTION_T3351726490_H
#ifndef CUBISMMASKPROPERTIES_T4215298192_H
#define CUBISMMASKPROPERTIES_T4215298192_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Live2D.Cubism.Rendering.Masking.CubismMaskProperties
struct  CubismMaskProperties_t4215298192  : public RuntimeObject
{
public:
	// Live2D.Cubism.Rendering.Masking.CubismMaskTexture Live2D.Cubism.Rendering.Masking.CubismMaskProperties::Texture
	CubismMaskTexture_t949515734 * ___Texture_0;
	// Live2D.Cubism.Rendering.Masking.CubismMaskTile Live2D.Cubism.Rendering.Masking.CubismMaskProperties::Tile
	CubismMaskTile_t3996512637  ___Tile_1;
	// Live2D.Cubism.Rendering.Masking.CubismMaskTransform Live2D.Cubism.Rendering.Masking.CubismMaskProperties::Transform
	CubismMaskTransform_t2215722789  ___Transform_2;

public:
	inline static int32_t get_offset_of_Texture_0() { return static_cast<int32_t>(offsetof(CubismMaskProperties_t4215298192, ___Texture_0)); }
	inline CubismMaskTexture_t949515734 * get_Texture_0() const { return ___Texture_0; }
	inline CubismMaskTexture_t949515734 ** get_address_of_Texture_0() { return &___Texture_0; }
	inline void set_Texture_0(CubismMaskTexture_t949515734 * value)
	{
		___Texture_0 = value;
		Il2CppCodeGenWriteBarrier((&___Texture_0), value);
	}

	inline static int32_t get_offset_of_Tile_1() { return static_cast<int32_t>(offsetof(CubismMaskProperties_t4215298192, ___Tile_1)); }
	inline CubismMaskTile_t3996512637  get_Tile_1() const { return ___Tile_1; }
	inline CubismMaskTile_t3996512637 * get_address_of_Tile_1() { return &___Tile_1; }
	inline void set_Tile_1(CubismMaskTile_t3996512637  value)
	{
		___Tile_1 = value;
	}

	inline static int32_t get_offset_of_Transform_2() { return static_cast<int32_t>(offsetof(CubismMaskProperties_t4215298192, ___Transform_2)); }
	inline CubismMaskTransform_t2215722789  get_Transform_2() const { return ___Transform_2; }
	inline CubismMaskTransform_t2215722789 * get_address_of_Transform_2() { return &___Transform_2; }
	inline void set_Transform_2(CubismMaskTransform_t2215722789  value)
	{
		___Transform_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CUBISMMASKPROPERTIES_T4215298192_H
#ifndef MULTICASTDELEGATE_T3201952435_H
#define MULTICASTDELEGATE_T3201952435_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.MulticastDelegate
struct  MulticastDelegate_t3201952435  : public Delegate_t3022476291
{
public:
	// System.MulticastDelegate System.MulticastDelegate::prev
	MulticastDelegate_t3201952435 * ___prev_9;
	// System.MulticastDelegate System.MulticastDelegate::kpm_next
	MulticastDelegate_t3201952435 * ___kpm_next_10;

public:
	inline static int32_t get_offset_of_prev_9() { return static_cast<int32_t>(offsetof(MulticastDelegate_t3201952435, ___prev_9)); }
	inline MulticastDelegate_t3201952435 * get_prev_9() const { return ___prev_9; }
	inline MulticastDelegate_t3201952435 ** get_address_of_prev_9() { return &___prev_9; }
	inline void set_prev_9(MulticastDelegate_t3201952435 * value)
	{
		___prev_9 = value;
		Il2CppCodeGenWriteBarrier((&___prev_9), value);
	}

	inline static int32_t get_offset_of_kpm_next_10() { return static_cast<int32_t>(offsetof(MulticastDelegate_t3201952435, ___kpm_next_10)); }
	inline MulticastDelegate_t3201952435 * get_kpm_next_10() const { return ___kpm_next_10; }
	inline MulticastDelegate_t3201952435 ** get_address_of_kpm_next_10() { return &___kpm_next_10; }
	inline void set_kpm_next_10(MulticastDelegate_t3201952435 * value)
	{
		___kpm_next_10 = value;
		Il2CppCodeGenWriteBarrier((&___kpm_next_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MULTICASTDELEGATE_T3201952435_H
#ifndef SCRIPTABLEOBJECT_T1975622470_H
#define SCRIPTABLEOBJECT_T1975622470_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.ScriptableObject
struct  ScriptableObject_t1975622470  : public Object_t1021602117
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.ScriptableObject
struct ScriptableObject_t1975622470_marshaled_pinvoke : public Object_t1021602117_marshaled_pinvoke
{
};
// Native definition for COM marshalling of UnityEngine.ScriptableObject
struct ScriptableObject_t1975622470_marshaled_com : public Object_t1021602117_marshaled_com
{
};
#endif // SCRIPTABLEOBJECT_T1975622470_H
#ifndef CUBISMPHYSICSOUTPUT_T1090441091_H
#define CUBISMPHYSICSOUTPUT_T1090441091_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Live2D.Cubism.Framework.Physics.CubismPhysicsOutput
struct  CubismPhysicsOutput_t1090441091 
{
public:
	// System.String Live2D.Cubism.Framework.Physics.CubismPhysicsOutput::DestinationId
	String_t* ___DestinationId_0;
	// System.Int32 Live2D.Cubism.Framework.Physics.CubismPhysicsOutput::ParticleIndex
	int32_t ___ParticleIndex_1;
	// UnityEngine.Vector2 Live2D.Cubism.Framework.Physics.CubismPhysicsOutput::TranslationScale
	Vector2_t2243707579  ___TranslationScale_2;
	// System.Single Live2D.Cubism.Framework.Physics.CubismPhysicsOutput::AngleScale
	float ___AngleScale_3;
	// System.Single Live2D.Cubism.Framework.Physics.CubismPhysicsOutput::Weight
	float ___Weight_4;
	// Live2D.Cubism.Framework.Physics.CubismPhysicsSourceComponent Live2D.Cubism.Framework.Physics.CubismPhysicsOutput::SourceComponent
	int32_t ___SourceComponent_5;
	// System.Boolean Live2D.Cubism.Framework.Physics.CubismPhysicsOutput::IsInverted
	bool ___IsInverted_6;
	// System.Single Live2D.Cubism.Framework.Physics.CubismPhysicsOutput::ValueBelowMinimum
	float ___ValueBelowMinimum_7;
	// System.Single Live2D.Cubism.Framework.Physics.CubismPhysicsOutput::ValueExceededMaximum
	float ___ValueExceededMaximum_8;
	// Live2D.Cubism.Core.CubismParameter Live2D.Cubism.Framework.Physics.CubismPhysicsOutput::Destination
	CubismParameter_t3864677546 * ___Destination_9;
	// Live2D.Cubism.Framework.Physics.CubismPhysicsOutput/ValueGetter Live2D.Cubism.Framework.Physics.CubismPhysicsOutput::GetValue
	ValueGetter_t1451636242 * ___GetValue_10;
	// Live2D.Cubism.Framework.Physics.CubismPhysicsOutput/ScaleGetter Live2D.Cubism.Framework.Physics.CubismPhysicsOutput::GetScale
	ScaleGetter_t3351733751 * ___GetScale_11;

public:
	inline static int32_t get_offset_of_DestinationId_0() { return static_cast<int32_t>(offsetof(CubismPhysicsOutput_t1090441091, ___DestinationId_0)); }
	inline String_t* get_DestinationId_0() const { return ___DestinationId_0; }
	inline String_t** get_address_of_DestinationId_0() { return &___DestinationId_0; }
	inline void set_DestinationId_0(String_t* value)
	{
		___DestinationId_0 = value;
		Il2CppCodeGenWriteBarrier((&___DestinationId_0), value);
	}

	inline static int32_t get_offset_of_ParticleIndex_1() { return static_cast<int32_t>(offsetof(CubismPhysicsOutput_t1090441091, ___ParticleIndex_1)); }
	inline int32_t get_ParticleIndex_1() const { return ___ParticleIndex_1; }
	inline int32_t* get_address_of_ParticleIndex_1() { return &___ParticleIndex_1; }
	inline void set_ParticleIndex_1(int32_t value)
	{
		___ParticleIndex_1 = value;
	}

	inline static int32_t get_offset_of_TranslationScale_2() { return static_cast<int32_t>(offsetof(CubismPhysicsOutput_t1090441091, ___TranslationScale_2)); }
	inline Vector2_t2243707579  get_TranslationScale_2() const { return ___TranslationScale_2; }
	inline Vector2_t2243707579 * get_address_of_TranslationScale_2() { return &___TranslationScale_2; }
	inline void set_TranslationScale_2(Vector2_t2243707579  value)
	{
		___TranslationScale_2 = value;
	}

	inline static int32_t get_offset_of_AngleScale_3() { return static_cast<int32_t>(offsetof(CubismPhysicsOutput_t1090441091, ___AngleScale_3)); }
	inline float get_AngleScale_3() const { return ___AngleScale_3; }
	inline float* get_address_of_AngleScale_3() { return &___AngleScale_3; }
	inline void set_AngleScale_3(float value)
	{
		___AngleScale_3 = value;
	}

	inline static int32_t get_offset_of_Weight_4() { return static_cast<int32_t>(offsetof(CubismPhysicsOutput_t1090441091, ___Weight_4)); }
	inline float get_Weight_4() const { return ___Weight_4; }
	inline float* get_address_of_Weight_4() { return &___Weight_4; }
	inline void set_Weight_4(float value)
	{
		___Weight_4 = value;
	}

	inline static int32_t get_offset_of_SourceComponent_5() { return static_cast<int32_t>(offsetof(CubismPhysicsOutput_t1090441091, ___SourceComponent_5)); }
	inline int32_t get_SourceComponent_5() const { return ___SourceComponent_5; }
	inline int32_t* get_address_of_SourceComponent_5() { return &___SourceComponent_5; }
	inline void set_SourceComponent_5(int32_t value)
	{
		___SourceComponent_5 = value;
	}

	inline static int32_t get_offset_of_IsInverted_6() { return static_cast<int32_t>(offsetof(CubismPhysicsOutput_t1090441091, ___IsInverted_6)); }
	inline bool get_IsInverted_6() const { return ___IsInverted_6; }
	inline bool* get_address_of_IsInverted_6() { return &___IsInverted_6; }
	inline void set_IsInverted_6(bool value)
	{
		___IsInverted_6 = value;
	}

	inline static int32_t get_offset_of_ValueBelowMinimum_7() { return static_cast<int32_t>(offsetof(CubismPhysicsOutput_t1090441091, ___ValueBelowMinimum_7)); }
	inline float get_ValueBelowMinimum_7() const { return ___ValueBelowMinimum_7; }
	inline float* get_address_of_ValueBelowMinimum_7() { return &___ValueBelowMinimum_7; }
	inline void set_ValueBelowMinimum_7(float value)
	{
		___ValueBelowMinimum_7 = value;
	}

	inline static int32_t get_offset_of_ValueExceededMaximum_8() { return static_cast<int32_t>(offsetof(CubismPhysicsOutput_t1090441091, ___ValueExceededMaximum_8)); }
	inline float get_ValueExceededMaximum_8() const { return ___ValueExceededMaximum_8; }
	inline float* get_address_of_ValueExceededMaximum_8() { return &___ValueExceededMaximum_8; }
	inline void set_ValueExceededMaximum_8(float value)
	{
		___ValueExceededMaximum_8 = value;
	}

	inline static int32_t get_offset_of_Destination_9() { return static_cast<int32_t>(offsetof(CubismPhysicsOutput_t1090441091, ___Destination_9)); }
	inline CubismParameter_t3864677546 * get_Destination_9() const { return ___Destination_9; }
	inline CubismParameter_t3864677546 ** get_address_of_Destination_9() { return &___Destination_9; }
	inline void set_Destination_9(CubismParameter_t3864677546 * value)
	{
		___Destination_9 = value;
		Il2CppCodeGenWriteBarrier((&___Destination_9), value);
	}

	inline static int32_t get_offset_of_GetValue_10() { return static_cast<int32_t>(offsetof(CubismPhysicsOutput_t1090441091, ___GetValue_10)); }
	inline ValueGetter_t1451636242 * get_GetValue_10() const { return ___GetValue_10; }
	inline ValueGetter_t1451636242 ** get_address_of_GetValue_10() { return &___GetValue_10; }
	inline void set_GetValue_10(ValueGetter_t1451636242 * value)
	{
		___GetValue_10 = value;
		Il2CppCodeGenWriteBarrier((&___GetValue_10), value);
	}

	inline static int32_t get_offset_of_GetScale_11() { return static_cast<int32_t>(offsetof(CubismPhysicsOutput_t1090441091, ___GetScale_11)); }
	inline ScaleGetter_t3351733751 * get_GetScale_11() const { return ___GetScale_11; }
	inline ScaleGetter_t3351733751 ** get_address_of_GetScale_11() { return &___GetScale_11; }
	inline void set_GetScale_11(ScaleGetter_t3351733751 * value)
	{
		___GetScale_11 = value;
		Il2CppCodeGenWriteBarrier((&___GetScale_11), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of Live2D.Cubism.Framework.Physics.CubismPhysicsOutput
struct CubismPhysicsOutput_t1090441091_marshaled_pinvoke
{
	char* ___DestinationId_0;
	int32_t ___ParticleIndex_1;
	Vector2_t2243707579  ___TranslationScale_2;
	float ___AngleScale_3;
	float ___Weight_4;
	int32_t ___SourceComponent_5;
	int32_t ___IsInverted_6;
	float ___ValueBelowMinimum_7;
	float ___ValueExceededMaximum_8;
	CubismParameter_t3864677546 * ___Destination_9;
	Il2CppMethodPointer ___GetValue_10;
	Il2CppMethodPointer ___GetScale_11;
};
// Native definition for COM marshalling of Live2D.Cubism.Framework.Physics.CubismPhysicsOutput
struct CubismPhysicsOutput_t1090441091_marshaled_com
{
	Il2CppChar* ___DestinationId_0;
	int32_t ___ParticleIndex_1;
	Vector2_t2243707579  ___TranslationScale_2;
	float ___AngleScale_3;
	float ___Weight_4;
	int32_t ___SourceComponent_5;
	int32_t ___IsInverted_6;
	float ___ValueBelowMinimum_7;
	float ___ValueExceededMaximum_8;
	CubismParameter_t3864677546 * ___Destination_9;
	Il2CppMethodPointer ___GetValue_10;
	Il2CppMethodPointer ___GetScale_11;
};
#endif // CUBISMPHYSICSOUTPUT_T1090441091_H
#ifndef CUBISMPHYSICSINPUT_T2391170328_H
#define CUBISMPHYSICSINPUT_T2391170328_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Live2D.Cubism.Framework.Physics.CubismPhysicsInput
struct  CubismPhysicsInput_t2391170328 
{
public:
	// System.String Live2D.Cubism.Framework.Physics.CubismPhysicsInput::SourceId
	String_t* ___SourceId_0;
	// UnityEngine.Vector2 Live2D.Cubism.Framework.Physics.CubismPhysicsInput::ScaleOfTranslation
	Vector2_t2243707579  ___ScaleOfTranslation_1;
	// System.Single Live2D.Cubism.Framework.Physics.CubismPhysicsInput::AngleScale
	float ___AngleScale_2;
	// System.Single Live2D.Cubism.Framework.Physics.CubismPhysicsInput::Weight
	float ___Weight_3;
	// Live2D.Cubism.Framework.Physics.CubismPhysicsSourceComponent Live2D.Cubism.Framework.Physics.CubismPhysicsInput::SourceComponent
	int32_t ___SourceComponent_4;
	// System.Boolean Live2D.Cubism.Framework.Physics.CubismPhysicsInput::IsInverted
	bool ___IsInverted_5;
	// Live2D.Cubism.Core.CubismParameter Live2D.Cubism.Framework.Physics.CubismPhysicsInput::Source
	CubismParameter_t3864677546 * ___Source_6;
	// Live2D.Cubism.Framework.Physics.CubismPhysicsInput/NormalizedParameterValueGetter Live2D.Cubism.Framework.Physics.CubismPhysicsInput::GetNormalizedParameterValue
	NormalizedParameterValueGetter_t2024973737 * ___GetNormalizedParameterValue_7;

public:
	inline static int32_t get_offset_of_SourceId_0() { return static_cast<int32_t>(offsetof(CubismPhysicsInput_t2391170328, ___SourceId_0)); }
	inline String_t* get_SourceId_0() const { return ___SourceId_0; }
	inline String_t** get_address_of_SourceId_0() { return &___SourceId_0; }
	inline void set_SourceId_0(String_t* value)
	{
		___SourceId_0 = value;
		Il2CppCodeGenWriteBarrier((&___SourceId_0), value);
	}

	inline static int32_t get_offset_of_ScaleOfTranslation_1() { return static_cast<int32_t>(offsetof(CubismPhysicsInput_t2391170328, ___ScaleOfTranslation_1)); }
	inline Vector2_t2243707579  get_ScaleOfTranslation_1() const { return ___ScaleOfTranslation_1; }
	inline Vector2_t2243707579 * get_address_of_ScaleOfTranslation_1() { return &___ScaleOfTranslation_1; }
	inline void set_ScaleOfTranslation_1(Vector2_t2243707579  value)
	{
		___ScaleOfTranslation_1 = value;
	}

	inline static int32_t get_offset_of_AngleScale_2() { return static_cast<int32_t>(offsetof(CubismPhysicsInput_t2391170328, ___AngleScale_2)); }
	inline float get_AngleScale_2() const { return ___AngleScale_2; }
	inline float* get_address_of_AngleScale_2() { return &___AngleScale_2; }
	inline void set_AngleScale_2(float value)
	{
		___AngleScale_2 = value;
	}

	inline static int32_t get_offset_of_Weight_3() { return static_cast<int32_t>(offsetof(CubismPhysicsInput_t2391170328, ___Weight_3)); }
	inline float get_Weight_3() const { return ___Weight_3; }
	inline float* get_address_of_Weight_3() { return &___Weight_3; }
	inline void set_Weight_3(float value)
	{
		___Weight_3 = value;
	}

	inline static int32_t get_offset_of_SourceComponent_4() { return static_cast<int32_t>(offsetof(CubismPhysicsInput_t2391170328, ___SourceComponent_4)); }
	inline int32_t get_SourceComponent_4() const { return ___SourceComponent_4; }
	inline int32_t* get_address_of_SourceComponent_4() { return &___SourceComponent_4; }
	inline void set_SourceComponent_4(int32_t value)
	{
		___SourceComponent_4 = value;
	}

	inline static int32_t get_offset_of_IsInverted_5() { return static_cast<int32_t>(offsetof(CubismPhysicsInput_t2391170328, ___IsInverted_5)); }
	inline bool get_IsInverted_5() const { return ___IsInverted_5; }
	inline bool* get_address_of_IsInverted_5() { return &___IsInverted_5; }
	inline void set_IsInverted_5(bool value)
	{
		___IsInverted_5 = value;
	}

	inline static int32_t get_offset_of_Source_6() { return static_cast<int32_t>(offsetof(CubismPhysicsInput_t2391170328, ___Source_6)); }
	inline CubismParameter_t3864677546 * get_Source_6() const { return ___Source_6; }
	inline CubismParameter_t3864677546 ** get_address_of_Source_6() { return &___Source_6; }
	inline void set_Source_6(CubismParameter_t3864677546 * value)
	{
		___Source_6 = value;
		Il2CppCodeGenWriteBarrier((&___Source_6), value);
	}

	inline static int32_t get_offset_of_GetNormalizedParameterValue_7() { return static_cast<int32_t>(offsetof(CubismPhysicsInput_t2391170328, ___GetNormalizedParameterValue_7)); }
	inline NormalizedParameterValueGetter_t2024973737 * get_GetNormalizedParameterValue_7() const { return ___GetNormalizedParameterValue_7; }
	inline NormalizedParameterValueGetter_t2024973737 ** get_address_of_GetNormalizedParameterValue_7() { return &___GetNormalizedParameterValue_7; }
	inline void set_GetNormalizedParameterValue_7(NormalizedParameterValueGetter_t2024973737 * value)
	{
		___GetNormalizedParameterValue_7 = value;
		Il2CppCodeGenWriteBarrier((&___GetNormalizedParameterValue_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of Live2D.Cubism.Framework.Physics.CubismPhysicsInput
struct CubismPhysicsInput_t2391170328_marshaled_pinvoke
{
	char* ___SourceId_0;
	Vector2_t2243707579  ___ScaleOfTranslation_1;
	float ___AngleScale_2;
	float ___Weight_3;
	int32_t ___SourceComponent_4;
	int32_t ___IsInverted_5;
	CubismParameter_t3864677546 * ___Source_6;
	Il2CppMethodPointer ___GetNormalizedParameterValue_7;
};
// Native definition for COM marshalling of Live2D.Cubism.Framework.Physics.CubismPhysicsInput
struct CubismPhysicsInput_t2391170328_marshaled_com
{
	Il2CppChar* ___SourceId_0;
	Vector2_t2243707579  ___ScaleOfTranslation_1;
	float ___AngleScale_2;
	float ___Weight_3;
	int32_t ___SourceComponent_4;
	int32_t ___IsInverted_5;
	CubismParameter_t3864677546 * ___Source_6;
	Il2CppMethodPointer ___GetNormalizedParameterValue_7;
};
#endif // CUBISMPHYSICSINPUT_T2391170328_H
#ifndef CUBISMPHYSICSSUBRIG_T1824845752_H
#define CUBISMPHYSICSSUBRIG_T1824845752_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Live2D.Cubism.Framework.Physics.CubismPhysicsSubRig
struct  CubismPhysicsSubRig_t1824845752  : public RuntimeObject
{
public:
	// Live2D.Cubism.Framework.Physics.CubismPhysicsInput[] Live2D.Cubism.Framework.Physics.CubismPhysicsSubRig::Input
	CubismPhysicsInputU5BU5D_t2102055817* ___Input_0;
	// Live2D.Cubism.Framework.Physics.CubismPhysicsOutput[] Live2D.Cubism.Framework.Physics.CubismPhysicsSubRig::Output
	CubismPhysicsOutputU5BU5D_t3543821074* ___Output_1;
	// Live2D.Cubism.Framework.Physics.CubismPhysicsParticle[] Live2D.Cubism.Framework.Physics.CubismPhysicsSubRig::Particles
	CubismPhysicsParticleU5BU5D_t487538331* ___Particles_2;
	// Live2D.Cubism.Framework.Physics.CubismPhysicsNormalization Live2D.Cubism.Framework.Physics.CubismPhysicsSubRig::Normalization
	CubismPhysicsNormalization_t2601092345  ___Normalization_3;
	// Live2D.Cubism.Framework.Physics.CubismPhysicsRig Live2D.Cubism.Framework.Physics.CubismPhysicsSubRig::_rig
	CubismPhysicsRig_t856779234 * ____rig_4;

public:
	inline static int32_t get_offset_of_Input_0() { return static_cast<int32_t>(offsetof(CubismPhysicsSubRig_t1824845752, ___Input_0)); }
	inline CubismPhysicsInputU5BU5D_t2102055817* get_Input_0() const { return ___Input_0; }
	inline CubismPhysicsInputU5BU5D_t2102055817** get_address_of_Input_0() { return &___Input_0; }
	inline void set_Input_0(CubismPhysicsInputU5BU5D_t2102055817* value)
	{
		___Input_0 = value;
		Il2CppCodeGenWriteBarrier((&___Input_0), value);
	}

	inline static int32_t get_offset_of_Output_1() { return static_cast<int32_t>(offsetof(CubismPhysicsSubRig_t1824845752, ___Output_1)); }
	inline CubismPhysicsOutputU5BU5D_t3543821074* get_Output_1() const { return ___Output_1; }
	inline CubismPhysicsOutputU5BU5D_t3543821074** get_address_of_Output_1() { return &___Output_1; }
	inline void set_Output_1(CubismPhysicsOutputU5BU5D_t3543821074* value)
	{
		___Output_1 = value;
		Il2CppCodeGenWriteBarrier((&___Output_1), value);
	}

	inline static int32_t get_offset_of_Particles_2() { return static_cast<int32_t>(offsetof(CubismPhysicsSubRig_t1824845752, ___Particles_2)); }
	inline CubismPhysicsParticleU5BU5D_t487538331* get_Particles_2() const { return ___Particles_2; }
	inline CubismPhysicsParticleU5BU5D_t487538331** get_address_of_Particles_2() { return &___Particles_2; }
	inline void set_Particles_2(CubismPhysicsParticleU5BU5D_t487538331* value)
	{
		___Particles_2 = value;
		Il2CppCodeGenWriteBarrier((&___Particles_2), value);
	}

	inline static int32_t get_offset_of_Normalization_3() { return static_cast<int32_t>(offsetof(CubismPhysicsSubRig_t1824845752, ___Normalization_3)); }
	inline CubismPhysicsNormalization_t2601092345  get_Normalization_3() const { return ___Normalization_3; }
	inline CubismPhysicsNormalization_t2601092345 * get_address_of_Normalization_3() { return &___Normalization_3; }
	inline void set_Normalization_3(CubismPhysicsNormalization_t2601092345  value)
	{
		___Normalization_3 = value;
	}

	inline static int32_t get_offset_of__rig_4() { return static_cast<int32_t>(offsetof(CubismPhysicsSubRig_t1824845752, ____rig_4)); }
	inline CubismPhysicsRig_t856779234 * get__rig_4() const { return ____rig_4; }
	inline CubismPhysicsRig_t856779234 ** get_address_of__rig_4() { return &____rig_4; }
	inline void set__rig_4(CubismPhysicsRig_t856779234 * value)
	{
		____rig_4 = value;
		Il2CppCodeGenWriteBarrier((&____rig_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CUBISMPHYSICSSUBRIG_T1824845752_H
#ifndef COMPONENT_T3819376471_H
#define COMPONENT_T3819376471_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Component
struct  Component_t3819376471  : public Object_t1021602117
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPONENT_T3819376471_H
#ifndef MATERIALPICKER_T1430258468_H
#define MATERIALPICKER_T1430258468_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Live2D.Cubism.Framework.Json.CubismModel3Json/MaterialPicker
struct  MaterialPicker_t1430258468  : public MulticastDelegate_t3201952435
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MATERIALPICKER_T1430258468_H
#ifndef LOADASSETATPATHHANDLER_T1714529023_H
#define LOADASSETATPATHHANDLER_T1714529023_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Live2D.Cubism.Framework.Json.CubismModel3Json/LoadAssetAtPathHandler
struct  LoadAssetAtPathHandler_t1714529023  : public MulticastDelegate_t3201952435
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LOADASSETATPATHHANDLER_T1714529023_H
#ifndef TEXTUREPICKER_T1644688768_H
#define TEXTUREPICKER_T1644688768_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Live2D.Cubism.Framework.Json.CubismModel3Json/TexturePicker
struct  TexturePicker_t1644688768  : public MulticastDelegate_t3201952435
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TEXTUREPICKER_T1644688768_H
#ifndef NORMALIZEDPARAMETERVALUEGETTER_T2024973737_H
#define NORMALIZEDPARAMETERVALUEGETTER_T2024973737_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Live2D.Cubism.Framework.Physics.CubismPhysicsInput/NormalizedParameterValueGetter
struct  NormalizedParameterValueGetter_t2024973737  : public MulticastDelegate_t3201952435
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NORMALIZEDPARAMETERVALUEGETTER_T2024973737_H
#ifndef SEGMENTPARSER_T456729962_H
#define SEGMENTPARSER_T456729962_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Live2D.Cubism.Framework.Json.CubismMotion3Json/SegmentParser
struct  SegmentParser_t456729962  : public MulticastDelegate_t3201952435
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SEGMENTPARSER_T456729962_H
#ifndef CUBISMPHYSICS3JSON_T875977367_H
#define CUBISMPHYSICS3JSON_T875977367_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Live2D.Cubism.Framework.Json.CubismPhysics3Json
struct  CubismPhysics3Json_t875977367  : public RuntimeObject
{
public:
	// System.Int32 Live2D.Cubism.Framework.Json.CubismPhysics3Json::Version
	int32_t ___Version_0;
	// Live2D.Cubism.Framework.Json.CubismPhysics3Json/SerializableMeta Live2D.Cubism.Framework.Json.CubismPhysics3Json::Meta
	SerializableMeta_t1007752097  ___Meta_1;
	// Live2D.Cubism.Framework.Json.CubismPhysics3Json/SerializablePhysicsSettings[] Live2D.Cubism.Framework.Json.CubismPhysics3Json::PhysicsSettings
	SerializablePhysicsSettingsU5BU5D_t3063461145* ___PhysicsSettings_2;

public:
	inline static int32_t get_offset_of_Version_0() { return static_cast<int32_t>(offsetof(CubismPhysics3Json_t875977367, ___Version_0)); }
	inline int32_t get_Version_0() const { return ___Version_0; }
	inline int32_t* get_address_of_Version_0() { return &___Version_0; }
	inline void set_Version_0(int32_t value)
	{
		___Version_0 = value;
	}

	inline static int32_t get_offset_of_Meta_1() { return static_cast<int32_t>(offsetof(CubismPhysics3Json_t875977367, ___Meta_1)); }
	inline SerializableMeta_t1007752097  get_Meta_1() const { return ___Meta_1; }
	inline SerializableMeta_t1007752097 * get_address_of_Meta_1() { return &___Meta_1; }
	inline void set_Meta_1(SerializableMeta_t1007752097  value)
	{
		___Meta_1 = value;
	}

	inline static int32_t get_offset_of_PhysicsSettings_2() { return static_cast<int32_t>(offsetof(CubismPhysics3Json_t875977367, ___PhysicsSettings_2)); }
	inline SerializablePhysicsSettingsU5BU5D_t3063461145* get_PhysicsSettings_2() const { return ___PhysicsSettings_2; }
	inline SerializablePhysicsSettingsU5BU5D_t3063461145** get_address_of_PhysicsSettings_2() { return &___PhysicsSettings_2; }
	inline void set_PhysicsSettings_2(SerializablePhysicsSettingsU5BU5D_t3063461145* value)
	{
		___PhysicsSettings_2 = value;
		Il2CppCodeGenWriteBarrier((&___PhysicsSettings_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CUBISMPHYSICS3JSON_T875977367_H
#ifndef CUBISMMASKTEXTURE_T949515734_H
#define CUBISMMASKTEXTURE_T949515734_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Live2D.Cubism.Rendering.Masking.CubismMaskTexture
struct  CubismMaskTexture_t949515734  : public ScriptableObject_t1975622470
{
public:
	// System.Int32 Live2D.Cubism.Rendering.Masking.CubismMaskTexture::_size
	int32_t ____size_2;
	// System.Int32 Live2D.Cubism.Rendering.Masking.CubismMaskTexture::_subdivisions
	int32_t ____subdivisions_3;
	// Live2D.Cubism.Rendering.Masking.CubismMaskTilePool Live2D.Cubism.Rendering.Masking.CubismMaskTexture::<TilePool>k__BackingField
	CubismMaskTilePool_t2959163307 * ___U3CTilePoolU3Ek__BackingField_4;
	// UnityEngine.RenderTexture Live2D.Cubism.Rendering.Masking.CubismMaskTexture::_renderTexture
	RenderTexture_t2666733923 * ____renderTexture_5;
	// System.Collections.Generic.List`1<Live2D.Cubism.Rendering.Masking.CubismMaskTexture/SourcesItem> Live2D.Cubism.Rendering.Masking.CubismMaskTexture::<Sources>k__BackingField
	List_1_t1928514839 * ___U3CSourcesU3Ek__BackingField_6;

public:
	inline static int32_t get_offset_of__size_2() { return static_cast<int32_t>(offsetof(CubismMaskTexture_t949515734, ____size_2)); }
	inline int32_t get__size_2() const { return ____size_2; }
	inline int32_t* get_address_of__size_2() { return &____size_2; }
	inline void set__size_2(int32_t value)
	{
		____size_2 = value;
	}

	inline static int32_t get_offset_of__subdivisions_3() { return static_cast<int32_t>(offsetof(CubismMaskTexture_t949515734, ____subdivisions_3)); }
	inline int32_t get__subdivisions_3() const { return ____subdivisions_3; }
	inline int32_t* get_address_of__subdivisions_3() { return &____subdivisions_3; }
	inline void set__subdivisions_3(int32_t value)
	{
		____subdivisions_3 = value;
	}

	inline static int32_t get_offset_of_U3CTilePoolU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(CubismMaskTexture_t949515734, ___U3CTilePoolU3Ek__BackingField_4)); }
	inline CubismMaskTilePool_t2959163307 * get_U3CTilePoolU3Ek__BackingField_4() const { return ___U3CTilePoolU3Ek__BackingField_4; }
	inline CubismMaskTilePool_t2959163307 ** get_address_of_U3CTilePoolU3Ek__BackingField_4() { return &___U3CTilePoolU3Ek__BackingField_4; }
	inline void set_U3CTilePoolU3Ek__BackingField_4(CubismMaskTilePool_t2959163307 * value)
	{
		___U3CTilePoolU3Ek__BackingField_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CTilePoolU3Ek__BackingField_4), value);
	}

	inline static int32_t get_offset_of__renderTexture_5() { return static_cast<int32_t>(offsetof(CubismMaskTexture_t949515734, ____renderTexture_5)); }
	inline RenderTexture_t2666733923 * get__renderTexture_5() const { return ____renderTexture_5; }
	inline RenderTexture_t2666733923 ** get_address_of__renderTexture_5() { return &____renderTexture_5; }
	inline void set__renderTexture_5(RenderTexture_t2666733923 * value)
	{
		____renderTexture_5 = value;
		Il2CppCodeGenWriteBarrier((&____renderTexture_5), value);
	}

	inline static int32_t get_offset_of_U3CSourcesU3Ek__BackingField_6() { return static_cast<int32_t>(offsetof(CubismMaskTexture_t949515734, ___U3CSourcesU3Ek__BackingField_6)); }
	inline List_1_t1928514839 * get_U3CSourcesU3Ek__BackingField_6() const { return ___U3CSourcesU3Ek__BackingField_6; }
	inline List_1_t1928514839 ** get_address_of_U3CSourcesU3Ek__BackingField_6() { return &___U3CSourcesU3Ek__BackingField_6; }
	inline void set_U3CSourcesU3Ek__BackingField_6(List_1_t1928514839 * value)
	{
		___U3CSourcesU3Ek__BackingField_6 = value;
		Il2CppCodeGenWriteBarrier((&___U3CSourcesU3Ek__BackingField_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CUBISMMASKTEXTURE_T949515734_H
#ifndef BEHAVIOUR_T955675639_H
#define BEHAVIOUR_T955675639_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Behaviour
struct  Behaviour_t955675639  : public Component_t3819376471
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BEHAVIOUR_T955675639_H
#ifndef VALUEGETTER_T1451636242_H
#define VALUEGETTER_T1451636242_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Live2D.Cubism.Framework.Physics.CubismPhysicsOutput/ValueGetter
struct  ValueGetter_t1451636242  : public MulticastDelegate_t3201952435
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VALUEGETTER_T1451636242_H
#ifndef SCALEGETTER_T3351733751_H
#define SCALEGETTER_T3351733751_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Live2D.Cubism.Framework.Physics.CubismPhysicsOutput/ScaleGetter
struct  ScaleGetter_t3351733751  : public MulticastDelegate_t3201952435
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SCALEGETTER_T3351733751_H
#ifndef MONOBEHAVIOUR_T1158329972_H
#define MONOBEHAVIOUR_T1158329972_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.MonoBehaviour
struct  MonoBehaviour_t1158329972  : public Behaviour_t955675639
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MONOBEHAVIOUR_T1158329972_H
#ifndef CUBISMHARMONICMOTIONCONTROLLER_T427238442_H
#define CUBISMHARMONICMOTIONCONTROLLER_T427238442_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Live2D.Cubism.Framework.HarmonicMotion.CubismHarmonicMotionController
struct  CubismHarmonicMotionController_t427238442  : public MonoBehaviour_t1158329972
{
public:
	// Live2D.Cubism.Framework.CubismParameterBlendMode Live2D.Cubism.Framework.HarmonicMotion.CubismHarmonicMotionController::BlendMode
	int32_t ___BlendMode_3;
	// System.Single[] Live2D.Cubism.Framework.HarmonicMotion.CubismHarmonicMotionController::ChannelTimescales
	SingleU5BU5D_t577127397* ___ChannelTimescales_4;
	// Live2D.Cubism.Framework.HarmonicMotion.CubismHarmonicMotionParameter[] Live2D.Cubism.Framework.HarmonicMotion.CubismHarmonicMotionController::<Sources>k__BackingField
	CubismHarmonicMotionParameterU5BU5D_t1324606222* ___U3CSourcesU3Ek__BackingField_5;
	// Live2D.Cubism.Core.CubismParameter[] Live2D.Cubism.Framework.HarmonicMotion.CubismHarmonicMotionController::<Destinations>k__BackingField
	CubismParameterU5BU5D_t2848401775* ___U3CDestinationsU3Ek__BackingField_6;

public:
	inline static int32_t get_offset_of_BlendMode_3() { return static_cast<int32_t>(offsetof(CubismHarmonicMotionController_t427238442, ___BlendMode_3)); }
	inline int32_t get_BlendMode_3() const { return ___BlendMode_3; }
	inline int32_t* get_address_of_BlendMode_3() { return &___BlendMode_3; }
	inline void set_BlendMode_3(int32_t value)
	{
		___BlendMode_3 = value;
	}

	inline static int32_t get_offset_of_ChannelTimescales_4() { return static_cast<int32_t>(offsetof(CubismHarmonicMotionController_t427238442, ___ChannelTimescales_4)); }
	inline SingleU5BU5D_t577127397* get_ChannelTimescales_4() const { return ___ChannelTimescales_4; }
	inline SingleU5BU5D_t577127397** get_address_of_ChannelTimescales_4() { return &___ChannelTimescales_4; }
	inline void set_ChannelTimescales_4(SingleU5BU5D_t577127397* value)
	{
		___ChannelTimescales_4 = value;
		Il2CppCodeGenWriteBarrier((&___ChannelTimescales_4), value);
	}

	inline static int32_t get_offset_of_U3CSourcesU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(CubismHarmonicMotionController_t427238442, ___U3CSourcesU3Ek__BackingField_5)); }
	inline CubismHarmonicMotionParameterU5BU5D_t1324606222* get_U3CSourcesU3Ek__BackingField_5() const { return ___U3CSourcesU3Ek__BackingField_5; }
	inline CubismHarmonicMotionParameterU5BU5D_t1324606222** get_address_of_U3CSourcesU3Ek__BackingField_5() { return &___U3CSourcesU3Ek__BackingField_5; }
	inline void set_U3CSourcesU3Ek__BackingField_5(CubismHarmonicMotionParameterU5BU5D_t1324606222* value)
	{
		___U3CSourcesU3Ek__BackingField_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3CSourcesU3Ek__BackingField_5), value);
	}

	inline static int32_t get_offset_of_U3CDestinationsU3Ek__BackingField_6() { return static_cast<int32_t>(offsetof(CubismHarmonicMotionController_t427238442, ___U3CDestinationsU3Ek__BackingField_6)); }
	inline CubismParameterU5BU5D_t2848401775* get_U3CDestinationsU3Ek__BackingField_6() const { return ___U3CDestinationsU3Ek__BackingField_6; }
	inline CubismParameterU5BU5D_t2848401775** get_address_of_U3CDestinationsU3Ek__BackingField_6() { return &___U3CDestinationsU3Ek__BackingField_6; }
	inline void set_U3CDestinationsU3Ek__BackingField_6(CubismParameterU5BU5D_t2848401775* value)
	{
		___U3CDestinationsU3Ek__BackingField_6 = value;
		Il2CppCodeGenWriteBarrier((&___U3CDestinationsU3Ek__BackingField_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CUBISMHARMONICMOTIONCONTROLLER_T427238442_H
#ifndef CUBISMPARTSINSPECTOR_T370324902_H
#define CUBISMPARTSINSPECTOR_T370324902_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Live2D.Cubism.Framework.CubismPartsInspector
struct  CubismPartsInspector_t370324902  : public MonoBehaviour_t1158329972
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CUBISMPARTSINSPECTOR_T370324902_H
#ifndef CUBISMLOOKCONTROLLER_T1393487782_H
#define CUBISMLOOKCONTROLLER_T1393487782_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Live2D.Cubism.Framework.LookAt.CubismLookController
struct  CubismLookController_t1393487782  : public MonoBehaviour_t1158329972
{
public:
	// Live2D.Cubism.Framework.CubismParameterBlendMode Live2D.Cubism.Framework.LookAt.CubismLookController::BlendMode
	int32_t ___BlendMode_2;
	// UnityEngine.Object Live2D.Cubism.Framework.LookAt.CubismLookController::_target
	Object_t1021602117 * ____target_3;
	// Live2D.Cubism.Framework.LookAt.ICubismLookTarget Live2D.Cubism.Framework.LookAt.CubismLookController::_targetInterface
	RuntimeObject* ____targetInterface_4;
	// UnityEngine.Transform Live2D.Cubism.Framework.LookAt.CubismLookController::Center
	Transform_t3275118058 * ___Center_5;
	// System.Single Live2D.Cubism.Framework.LookAt.CubismLookController::Damping
	float ___Damping_6;
	// Live2D.Cubism.Framework.LookAt.CubismLookParameter[] Live2D.Cubism.Framework.LookAt.CubismLookController::<Sources>k__BackingField
	CubismLookParameterU5BU5D_t1300568610* ___U3CSourcesU3Ek__BackingField_7;
	// Live2D.Cubism.Core.CubismParameter[] Live2D.Cubism.Framework.LookAt.CubismLookController::<Destinations>k__BackingField
	CubismParameterU5BU5D_t2848401775* ___U3CDestinationsU3Ek__BackingField_8;
	// UnityEngine.Vector3 Live2D.Cubism.Framework.LookAt.CubismLookController::<LastPosition>k__BackingField
	Vector3_t2243707580  ___U3CLastPositionU3Ek__BackingField_9;
	// UnityEngine.Vector3 Live2D.Cubism.Framework.LookAt.CubismLookController::<GoalPosition>k__BackingField
	Vector3_t2243707580  ___U3CGoalPositionU3Ek__BackingField_10;
	// UnityEngine.Vector3 Live2D.Cubism.Framework.LookAt.CubismLookController::VelocityBuffer
	Vector3_t2243707580  ___VelocityBuffer_11;

public:
	inline static int32_t get_offset_of_BlendMode_2() { return static_cast<int32_t>(offsetof(CubismLookController_t1393487782, ___BlendMode_2)); }
	inline int32_t get_BlendMode_2() const { return ___BlendMode_2; }
	inline int32_t* get_address_of_BlendMode_2() { return &___BlendMode_2; }
	inline void set_BlendMode_2(int32_t value)
	{
		___BlendMode_2 = value;
	}

	inline static int32_t get_offset_of__target_3() { return static_cast<int32_t>(offsetof(CubismLookController_t1393487782, ____target_3)); }
	inline Object_t1021602117 * get__target_3() const { return ____target_3; }
	inline Object_t1021602117 ** get_address_of__target_3() { return &____target_3; }
	inline void set__target_3(Object_t1021602117 * value)
	{
		____target_3 = value;
		Il2CppCodeGenWriteBarrier((&____target_3), value);
	}

	inline static int32_t get_offset_of__targetInterface_4() { return static_cast<int32_t>(offsetof(CubismLookController_t1393487782, ____targetInterface_4)); }
	inline RuntimeObject* get__targetInterface_4() const { return ____targetInterface_4; }
	inline RuntimeObject** get_address_of__targetInterface_4() { return &____targetInterface_4; }
	inline void set__targetInterface_4(RuntimeObject* value)
	{
		____targetInterface_4 = value;
		Il2CppCodeGenWriteBarrier((&____targetInterface_4), value);
	}

	inline static int32_t get_offset_of_Center_5() { return static_cast<int32_t>(offsetof(CubismLookController_t1393487782, ___Center_5)); }
	inline Transform_t3275118058 * get_Center_5() const { return ___Center_5; }
	inline Transform_t3275118058 ** get_address_of_Center_5() { return &___Center_5; }
	inline void set_Center_5(Transform_t3275118058 * value)
	{
		___Center_5 = value;
		Il2CppCodeGenWriteBarrier((&___Center_5), value);
	}

	inline static int32_t get_offset_of_Damping_6() { return static_cast<int32_t>(offsetof(CubismLookController_t1393487782, ___Damping_6)); }
	inline float get_Damping_6() const { return ___Damping_6; }
	inline float* get_address_of_Damping_6() { return &___Damping_6; }
	inline void set_Damping_6(float value)
	{
		___Damping_6 = value;
	}

	inline static int32_t get_offset_of_U3CSourcesU3Ek__BackingField_7() { return static_cast<int32_t>(offsetof(CubismLookController_t1393487782, ___U3CSourcesU3Ek__BackingField_7)); }
	inline CubismLookParameterU5BU5D_t1300568610* get_U3CSourcesU3Ek__BackingField_7() const { return ___U3CSourcesU3Ek__BackingField_7; }
	inline CubismLookParameterU5BU5D_t1300568610** get_address_of_U3CSourcesU3Ek__BackingField_7() { return &___U3CSourcesU3Ek__BackingField_7; }
	inline void set_U3CSourcesU3Ek__BackingField_7(CubismLookParameterU5BU5D_t1300568610* value)
	{
		___U3CSourcesU3Ek__BackingField_7 = value;
		Il2CppCodeGenWriteBarrier((&___U3CSourcesU3Ek__BackingField_7), value);
	}

	inline static int32_t get_offset_of_U3CDestinationsU3Ek__BackingField_8() { return static_cast<int32_t>(offsetof(CubismLookController_t1393487782, ___U3CDestinationsU3Ek__BackingField_8)); }
	inline CubismParameterU5BU5D_t2848401775* get_U3CDestinationsU3Ek__BackingField_8() const { return ___U3CDestinationsU3Ek__BackingField_8; }
	inline CubismParameterU5BU5D_t2848401775** get_address_of_U3CDestinationsU3Ek__BackingField_8() { return &___U3CDestinationsU3Ek__BackingField_8; }
	inline void set_U3CDestinationsU3Ek__BackingField_8(CubismParameterU5BU5D_t2848401775* value)
	{
		___U3CDestinationsU3Ek__BackingField_8 = value;
		Il2CppCodeGenWriteBarrier((&___U3CDestinationsU3Ek__BackingField_8), value);
	}

	inline static int32_t get_offset_of_U3CLastPositionU3Ek__BackingField_9() { return static_cast<int32_t>(offsetof(CubismLookController_t1393487782, ___U3CLastPositionU3Ek__BackingField_9)); }
	inline Vector3_t2243707580  get_U3CLastPositionU3Ek__BackingField_9() const { return ___U3CLastPositionU3Ek__BackingField_9; }
	inline Vector3_t2243707580 * get_address_of_U3CLastPositionU3Ek__BackingField_9() { return &___U3CLastPositionU3Ek__BackingField_9; }
	inline void set_U3CLastPositionU3Ek__BackingField_9(Vector3_t2243707580  value)
	{
		___U3CLastPositionU3Ek__BackingField_9 = value;
	}

	inline static int32_t get_offset_of_U3CGoalPositionU3Ek__BackingField_10() { return static_cast<int32_t>(offsetof(CubismLookController_t1393487782, ___U3CGoalPositionU3Ek__BackingField_10)); }
	inline Vector3_t2243707580  get_U3CGoalPositionU3Ek__BackingField_10() const { return ___U3CGoalPositionU3Ek__BackingField_10; }
	inline Vector3_t2243707580 * get_address_of_U3CGoalPositionU3Ek__BackingField_10() { return &___U3CGoalPositionU3Ek__BackingField_10; }
	inline void set_U3CGoalPositionU3Ek__BackingField_10(Vector3_t2243707580  value)
	{
		___U3CGoalPositionU3Ek__BackingField_10 = value;
	}

	inline static int32_t get_offset_of_VelocityBuffer_11() { return static_cast<int32_t>(offsetof(CubismLookController_t1393487782, ___VelocityBuffer_11)); }
	inline Vector3_t2243707580  get_VelocityBuffer_11() const { return ___VelocityBuffer_11; }
	inline Vector3_t2243707580 * get_address_of_VelocityBuffer_11() { return &___VelocityBuffer_11; }
	inline void set_VelocityBuffer_11(Vector3_t2243707580  value)
	{
		___VelocityBuffer_11 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CUBISMLOOKCONTROLLER_T1393487782_H
#ifndef CUBISMPARAMETERSINSPECTOR_T624078792_H
#define CUBISMPARAMETERSINSPECTOR_T624078792_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Live2D.Cubism.Framework.CubismParametersInspector
struct  CubismParametersInspector_t624078792  : public MonoBehaviour_t1158329972
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CUBISMPARAMETERSINSPECTOR_T624078792_H
#ifndef CUBISMAUTOEYEBLINKINPUT_T1359520417_H
#define CUBISMAUTOEYEBLINKINPUT_T1359520417_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Live2D.Cubism.Framework.CubismAutoEyeBlinkInput
struct  CubismAutoEyeBlinkInput_t1359520417  : public MonoBehaviour_t1158329972
{
public:
	// System.Single Live2D.Cubism.Framework.CubismAutoEyeBlinkInput::Mean
	float ___Mean_2;
	// System.Single Live2D.Cubism.Framework.CubismAutoEyeBlinkInput::MaximumDeviation
	float ___MaximumDeviation_3;
	// System.Single Live2D.Cubism.Framework.CubismAutoEyeBlinkInput::Timescale
	float ___Timescale_4;
	// Live2D.Cubism.Framework.CubismEyeBlinkController Live2D.Cubism.Framework.CubismAutoEyeBlinkInput::<Controller>k__BackingField
	CubismEyeBlinkController_t4113316510 * ___U3CControllerU3Ek__BackingField_5;
	// System.Single Live2D.Cubism.Framework.CubismAutoEyeBlinkInput::<T>k__BackingField
	float ___U3CTU3Ek__BackingField_6;
	// Live2D.Cubism.Framework.CubismAutoEyeBlinkInput/Phase Live2D.Cubism.Framework.CubismAutoEyeBlinkInput::<CurrentPhase>k__BackingField
	int32_t ___U3CCurrentPhaseU3Ek__BackingField_7;
	// System.Single Live2D.Cubism.Framework.CubismAutoEyeBlinkInput::<LastValue>k__BackingField
	float ___U3CLastValueU3Ek__BackingField_8;

public:
	inline static int32_t get_offset_of_Mean_2() { return static_cast<int32_t>(offsetof(CubismAutoEyeBlinkInput_t1359520417, ___Mean_2)); }
	inline float get_Mean_2() const { return ___Mean_2; }
	inline float* get_address_of_Mean_2() { return &___Mean_2; }
	inline void set_Mean_2(float value)
	{
		___Mean_2 = value;
	}

	inline static int32_t get_offset_of_MaximumDeviation_3() { return static_cast<int32_t>(offsetof(CubismAutoEyeBlinkInput_t1359520417, ___MaximumDeviation_3)); }
	inline float get_MaximumDeviation_3() const { return ___MaximumDeviation_3; }
	inline float* get_address_of_MaximumDeviation_3() { return &___MaximumDeviation_3; }
	inline void set_MaximumDeviation_3(float value)
	{
		___MaximumDeviation_3 = value;
	}

	inline static int32_t get_offset_of_Timescale_4() { return static_cast<int32_t>(offsetof(CubismAutoEyeBlinkInput_t1359520417, ___Timescale_4)); }
	inline float get_Timescale_4() const { return ___Timescale_4; }
	inline float* get_address_of_Timescale_4() { return &___Timescale_4; }
	inline void set_Timescale_4(float value)
	{
		___Timescale_4 = value;
	}

	inline static int32_t get_offset_of_U3CControllerU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(CubismAutoEyeBlinkInput_t1359520417, ___U3CControllerU3Ek__BackingField_5)); }
	inline CubismEyeBlinkController_t4113316510 * get_U3CControllerU3Ek__BackingField_5() const { return ___U3CControllerU3Ek__BackingField_5; }
	inline CubismEyeBlinkController_t4113316510 ** get_address_of_U3CControllerU3Ek__BackingField_5() { return &___U3CControllerU3Ek__BackingField_5; }
	inline void set_U3CControllerU3Ek__BackingField_5(CubismEyeBlinkController_t4113316510 * value)
	{
		___U3CControllerU3Ek__BackingField_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3CControllerU3Ek__BackingField_5), value);
	}

	inline static int32_t get_offset_of_U3CTU3Ek__BackingField_6() { return static_cast<int32_t>(offsetof(CubismAutoEyeBlinkInput_t1359520417, ___U3CTU3Ek__BackingField_6)); }
	inline float get_U3CTU3Ek__BackingField_6() const { return ___U3CTU3Ek__BackingField_6; }
	inline float* get_address_of_U3CTU3Ek__BackingField_6() { return &___U3CTU3Ek__BackingField_6; }
	inline void set_U3CTU3Ek__BackingField_6(float value)
	{
		___U3CTU3Ek__BackingField_6 = value;
	}

	inline static int32_t get_offset_of_U3CCurrentPhaseU3Ek__BackingField_7() { return static_cast<int32_t>(offsetof(CubismAutoEyeBlinkInput_t1359520417, ___U3CCurrentPhaseU3Ek__BackingField_7)); }
	inline int32_t get_U3CCurrentPhaseU3Ek__BackingField_7() const { return ___U3CCurrentPhaseU3Ek__BackingField_7; }
	inline int32_t* get_address_of_U3CCurrentPhaseU3Ek__BackingField_7() { return &___U3CCurrentPhaseU3Ek__BackingField_7; }
	inline void set_U3CCurrentPhaseU3Ek__BackingField_7(int32_t value)
	{
		___U3CCurrentPhaseU3Ek__BackingField_7 = value;
	}

	inline static int32_t get_offset_of_U3CLastValueU3Ek__BackingField_8() { return static_cast<int32_t>(offsetof(CubismAutoEyeBlinkInput_t1359520417, ___U3CLastValueU3Ek__BackingField_8)); }
	inline float get_U3CLastValueU3Ek__BackingField_8() const { return ___U3CLastValueU3Ek__BackingField_8; }
	inline float* get_address_of_U3CLastValueU3Ek__BackingField_8() { return &___U3CLastValueU3Ek__BackingField_8; }
	inline void set_U3CLastValueU3Ek__BackingField_8(float value)
	{
		___U3CLastValueU3Ek__BackingField_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CUBISMAUTOEYEBLINKINPUT_T1359520417_H
#ifndef CUBISMEYEBLINKPARAMETER_T3108136695_H
#define CUBISMEYEBLINKPARAMETER_T3108136695_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Live2D.Cubism.Framework.CubismEyeBlinkParameter
struct  CubismEyeBlinkParameter_t3108136695  : public MonoBehaviour_t1158329972
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CUBISMEYEBLINKPARAMETER_T3108136695_H
#ifndef CUBISMEYEBLINKCONTROLLER_T4113316510_H
#define CUBISMEYEBLINKCONTROLLER_T4113316510_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Live2D.Cubism.Framework.CubismEyeBlinkController
struct  CubismEyeBlinkController_t4113316510  : public MonoBehaviour_t1158329972
{
public:
	// Live2D.Cubism.Framework.CubismParameterBlendMode Live2D.Cubism.Framework.CubismEyeBlinkController::BlendMode
	int32_t ___BlendMode_2;
	// System.Single Live2D.Cubism.Framework.CubismEyeBlinkController::EyeOpening
	float ___EyeOpening_3;
	// Live2D.Cubism.Core.CubismParameter[] Live2D.Cubism.Framework.CubismEyeBlinkController::<Destinations>k__BackingField
	CubismParameterU5BU5D_t2848401775* ___U3CDestinationsU3Ek__BackingField_4;

public:
	inline static int32_t get_offset_of_BlendMode_2() { return static_cast<int32_t>(offsetof(CubismEyeBlinkController_t4113316510, ___BlendMode_2)); }
	inline int32_t get_BlendMode_2() const { return ___BlendMode_2; }
	inline int32_t* get_address_of_BlendMode_2() { return &___BlendMode_2; }
	inline void set_BlendMode_2(int32_t value)
	{
		___BlendMode_2 = value;
	}

	inline static int32_t get_offset_of_EyeOpening_3() { return static_cast<int32_t>(offsetof(CubismEyeBlinkController_t4113316510, ___EyeOpening_3)); }
	inline float get_EyeOpening_3() const { return ___EyeOpening_3; }
	inline float* get_address_of_EyeOpening_3() { return &___EyeOpening_3; }
	inline void set_EyeOpening_3(float value)
	{
		___EyeOpening_3 = value;
	}

	inline static int32_t get_offset_of_U3CDestinationsU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(CubismEyeBlinkController_t4113316510, ___U3CDestinationsU3Ek__BackingField_4)); }
	inline CubismParameterU5BU5D_t2848401775* get_U3CDestinationsU3Ek__BackingField_4() const { return ___U3CDestinationsU3Ek__BackingField_4; }
	inline CubismParameterU5BU5D_t2848401775** get_address_of_U3CDestinationsU3Ek__BackingField_4() { return &___U3CDestinationsU3Ek__BackingField_4; }
	inline void set_U3CDestinationsU3Ek__BackingField_4(CubismParameterU5BU5D_t2848401775* value)
	{
		___U3CDestinationsU3Ek__BackingField_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CDestinationsU3Ek__BackingField_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CUBISMEYEBLINKCONTROLLER_T4113316510_H
#ifndef CUBISMMASKCOMMANDBUFFER_T876406890_H
#define CUBISMMASKCOMMANDBUFFER_T876406890_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Live2D.Cubism.Rendering.Masking.CubismMaskCommandBuffer
struct  CubismMaskCommandBuffer_t876406890  : public MonoBehaviour_t1158329972
{
public:

public:
};

struct CubismMaskCommandBuffer_t876406890_StaticFields
{
public:
	// System.Collections.Generic.List`1<Live2D.Cubism.Rendering.Masking.ICubismMaskCommandSource> Live2D.Cubism.Rendering.Masking.CubismMaskCommandBuffer::<Sources>k__BackingField
	List_1_t4147648590 * ___U3CSourcesU3Ek__BackingField_2;
	// UnityEngine.Rendering.CommandBuffer Live2D.Cubism.Rendering.Masking.CubismMaskCommandBuffer::<Buffer>k__BackingField
	CommandBuffer_t1204166949 * ___U3CBufferU3Ek__BackingField_3;

public:
	inline static int32_t get_offset_of_U3CSourcesU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(CubismMaskCommandBuffer_t876406890_StaticFields, ___U3CSourcesU3Ek__BackingField_2)); }
	inline List_1_t4147648590 * get_U3CSourcesU3Ek__BackingField_2() const { return ___U3CSourcesU3Ek__BackingField_2; }
	inline List_1_t4147648590 ** get_address_of_U3CSourcesU3Ek__BackingField_2() { return &___U3CSourcesU3Ek__BackingField_2; }
	inline void set_U3CSourcesU3Ek__BackingField_2(List_1_t4147648590 * value)
	{
		___U3CSourcesU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CSourcesU3Ek__BackingField_2), value);
	}

	inline static int32_t get_offset_of_U3CBufferU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(CubismMaskCommandBuffer_t876406890_StaticFields, ___U3CBufferU3Ek__BackingField_3)); }
	inline CommandBuffer_t1204166949 * get_U3CBufferU3Ek__BackingField_3() const { return ___U3CBufferU3Ek__BackingField_3; }
	inline CommandBuffer_t1204166949 ** get_address_of_U3CBufferU3Ek__BackingField_3() { return &___U3CBufferU3Ek__BackingField_3; }
	inline void set_U3CBufferU3Ek__BackingField_3(CommandBuffer_t1204166949 * value)
	{
		___U3CBufferU3Ek__BackingField_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CBufferU3Ek__BackingField_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CUBISMMASKCOMMANDBUFFER_T876406890_H
#ifndef CUBISMRENDERER_T1960351136_H
#define CUBISMRENDERER_T1960351136_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Live2D.Cubism.Rendering.CubismRenderer
struct  CubismRenderer_t1960351136  : public MonoBehaviour_t1158329972
{
public:
	// System.Int32 Live2D.Cubism.Rendering.CubismRenderer::_localSortingOrder
	int32_t ____localSortingOrder_2;
	// UnityEngine.Color Live2D.Cubism.Rendering.CubismRenderer::_color
	Color_t2020392075  ____color_3;
	// UnityEngine.Texture2D Live2D.Cubism.Rendering.CubismRenderer::_mainTexture
	Texture2D_t3542995729 * ____mainTexture_4;
	// UnityEngine.Mesh[] Live2D.Cubism.Rendering.CubismRenderer::<Meshes>k__BackingField
	MeshU5BU5D_t894826206* ___U3CMeshesU3Ek__BackingField_5;
	// System.Int32 Live2D.Cubism.Rendering.CubismRenderer::<FrontMesh>k__BackingField
	int32_t ___U3CFrontMeshU3Ek__BackingField_6;
	// System.Int32 Live2D.Cubism.Rendering.CubismRenderer::<BackMesh>k__BackingField
	int32_t ___U3CBackMeshU3Ek__BackingField_7;
	// UnityEngine.MeshFilter Live2D.Cubism.Rendering.CubismRenderer::_meshFilter
	MeshFilter_t3026937449 * ____meshFilter_8;
	// UnityEngine.MeshRenderer Live2D.Cubism.Rendering.CubismRenderer::_meshRenderer
	MeshRenderer_t1268241104 * ____meshRenderer_9;
	// Live2D.Cubism.Rendering.CubismSortingMode Live2D.Cubism.Rendering.CubismRenderer::_sortingMode
	int32_t ____sortingMode_10;
	// System.Int32 Live2D.Cubism.Rendering.CubismRenderer::_sortingOrder
	int32_t ____sortingOrder_11;
	// System.Int32 Live2D.Cubism.Rendering.CubismRenderer::_renderOrder
	int32_t ____renderOrder_12;
	// System.Single Live2D.Cubism.Rendering.CubismRenderer::_depthOffset
	float ____depthOffset_13;
	// System.Single Live2D.Cubism.Rendering.CubismRenderer::_opacity
	float ____opacity_14;
	// UnityEngine.Color[] Live2D.Cubism.Rendering.CubismRenderer::<VertexColors>k__BackingField
	ColorU5BU5D_t672350442* ___U3CVertexColorsU3Ek__BackingField_15;
	// Live2D.Cubism.Rendering.CubismRenderer/SwapInfo Live2D.Cubism.Rendering.CubismRenderer::<LastSwap>k__BackingField
	SwapInfo_t4238099863  ___U3CLastSwapU3Ek__BackingField_16;
	// Live2D.Cubism.Rendering.CubismRenderer/SwapInfo Live2D.Cubism.Rendering.CubismRenderer::<ThisSwap>k__BackingField
	SwapInfo_t4238099863  ___U3CThisSwapU3Ek__BackingField_17;

public:
	inline static int32_t get_offset_of__localSortingOrder_2() { return static_cast<int32_t>(offsetof(CubismRenderer_t1960351136, ____localSortingOrder_2)); }
	inline int32_t get__localSortingOrder_2() const { return ____localSortingOrder_2; }
	inline int32_t* get_address_of__localSortingOrder_2() { return &____localSortingOrder_2; }
	inline void set__localSortingOrder_2(int32_t value)
	{
		____localSortingOrder_2 = value;
	}

	inline static int32_t get_offset_of__color_3() { return static_cast<int32_t>(offsetof(CubismRenderer_t1960351136, ____color_3)); }
	inline Color_t2020392075  get__color_3() const { return ____color_3; }
	inline Color_t2020392075 * get_address_of__color_3() { return &____color_3; }
	inline void set__color_3(Color_t2020392075  value)
	{
		____color_3 = value;
	}

	inline static int32_t get_offset_of__mainTexture_4() { return static_cast<int32_t>(offsetof(CubismRenderer_t1960351136, ____mainTexture_4)); }
	inline Texture2D_t3542995729 * get__mainTexture_4() const { return ____mainTexture_4; }
	inline Texture2D_t3542995729 ** get_address_of__mainTexture_4() { return &____mainTexture_4; }
	inline void set__mainTexture_4(Texture2D_t3542995729 * value)
	{
		____mainTexture_4 = value;
		Il2CppCodeGenWriteBarrier((&____mainTexture_4), value);
	}

	inline static int32_t get_offset_of_U3CMeshesU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(CubismRenderer_t1960351136, ___U3CMeshesU3Ek__BackingField_5)); }
	inline MeshU5BU5D_t894826206* get_U3CMeshesU3Ek__BackingField_5() const { return ___U3CMeshesU3Ek__BackingField_5; }
	inline MeshU5BU5D_t894826206** get_address_of_U3CMeshesU3Ek__BackingField_5() { return &___U3CMeshesU3Ek__BackingField_5; }
	inline void set_U3CMeshesU3Ek__BackingField_5(MeshU5BU5D_t894826206* value)
	{
		___U3CMeshesU3Ek__BackingField_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3CMeshesU3Ek__BackingField_5), value);
	}

	inline static int32_t get_offset_of_U3CFrontMeshU3Ek__BackingField_6() { return static_cast<int32_t>(offsetof(CubismRenderer_t1960351136, ___U3CFrontMeshU3Ek__BackingField_6)); }
	inline int32_t get_U3CFrontMeshU3Ek__BackingField_6() const { return ___U3CFrontMeshU3Ek__BackingField_6; }
	inline int32_t* get_address_of_U3CFrontMeshU3Ek__BackingField_6() { return &___U3CFrontMeshU3Ek__BackingField_6; }
	inline void set_U3CFrontMeshU3Ek__BackingField_6(int32_t value)
	{
		___U3CFrontMeshU3Ek__BackingField_6 = value;
	}

	inline static int32_t get_offset_of_U3CBackMeshU3Ek__BackingField_7() { return static_cast<int32_t>(offsetof(CubismRenderer_t1960351136, ___U3CBackMeshU3Ek__BackingField_7)); }
	inline int32_t get_U3CBackMeshU3Ek__BackingField_7() const { return ___U3CBackMeshU3Ek__BackingField_7; }
	inline int32_t* get_address_of_U3CBackMeshU3Ek__BackingField_7() { return &___U3CBackMeshU3Ek__BackingField_7; }
	inline void set_U3CBackMeshU3Ek__BackingField_7(int32_t value)
	{
		___U3CBackMeshU3Ek__BackingField_7 = value;
	}

	inline static int32_t get_offset_of__meshFilter_8() { return static_cast<int32_t>(offsetof(CubismRenderer_t1960351136, ____meshFilter_8)); }
	inline MeshFilter_t3026937449 * get__meshFilter_8() const { return ____meshFilter_8; }
	inline MeshFilter_t3026937449 ** get_address_of__meshFilter_8() { return &____meshFilter_8; }
	inline void set__meshFilter_8(MeshFilter_t3026937449 * value)
	{
		____meshFilter_8 = value;
		Il2CppCodeGenWriteBarrier((&____meshFilter_8), value);
	}

	inline static int32_t get_offset_of__meshRenderer_9() { return static_cast<int32_t>(offsetof(CubismRenderer_t1960351136, ____meshRenderer_9)); }
	inline MeshRenderer_t1268241104 * get__meshRenderer_9() const { return ____meshRenderer_9; }
	inline MeshRenderer_t1268241104 ** get_address_of__meshRenderer_9() { return &____meshRenderer_9; }
	inline void set__meshRenderer_9(MeshRenderer_t1268241104 * value)
	{
		____meshRenderer_9 = value;
		Il2CppCodeGenWriteBarrier((&____meshRenderer_9), value);
	}

	inline static int32_t get_offset_of__sortingMode_10() { return static_cast<int32_t>(offsetof(CubismRenderer_t1960351136, ____sortingMode_10)); }
	inline int32_t get__sortingMode_10() const { return ____sortingMode_10; }
	inline int32_t* get_address_of__sortingMode_10() { return &____sortingMode_10; }
	inline void set__sortingMode_10(int32_t value)
	{
		____sortingMode_10 = value;
	}

	inline static int32_t get_offset_of__sortingOrder_11() { return static_cast<int32_t>(offsetof(CubismRenderer_t1960351136, ____sortingOrder_11)); }
	inline int32_t get__sortingOrder_11() const { return ____sortingOrder_11; }
	inline int32_t* get_address_of__sortingOrder_11() { return &____sortingOrder_11; }
	inline void set__sortingOrder_11(int32_t value)
	{
		____sortingOrder_11 = value;
	}

	inline static int32_t get_offset_of__renderOrder_12() { return static_cast<int32_t>(offsetof(CubismRenderer_t1960351136, ____renderOrder_12)); }
	inline int32_t get__renderOrder_12() const { return ____renderOrder_12; }
	inline int32_t* get_address_of__renderOrder_12() { return &____renderOrder_12; }
	inline void set__renderOrder_12(int32_t value)
	{
		____renderOrder_12 = value;
	}

	inline static int32_t get_offset_of__depthOffset_13() { return static_cast<int32_t>(offsetof(CubismRenderer_t1960351136, ____depthOffset_13)); }
	inline float get__depthOffset_13() const { return ____depthOffset_13; }
	inline float* get_address_of__depthOffset_13() { return &____depthOffset_13; }
	inline void set__depthOffset_13(float value)
	{
		____depthOffset_13 = value;
	}

	inline static int32_t get_offset_of__opacity_14() { return static_cast<int32_t>(offsetof(CubismRenderer_t1960351136, ____opacity_14)); }
	inline float get__opacity_14() const { return ____opacity_14; }
	inline float* get_address_of__opacity_14() { return &____opacity_14; }
	inline void set__opacity_14(float value)
	{
		____opacity_14 = value;
	}

	inline static int32_t get_offset_of_U3CVertexColorsU3Ek__BackingField_15() { return static_cast<int32_t>(offsetof(CubismRenderer_t1960351136, ___U3CVertexColorsU3Ek__BackingField_15)); }
	inline ColorU5BU5D_t672350442* get_U3CVertexColorsU3Ek__BackingField_15() const { return ___U3CVertexColorsU3Ek__BackingField_15; }
	inline ColorU5BU5D_t672350442** get_address_of_U3CVertexColorsU3Ek__BackingField_15() { return &___U3CVertexColorsU3Ek__BackingField_15; }
	inline void set_U3CVertexColorsU3Ek__BackingField_15(ColorU5BU5D_t672350442* value)
	{
		___U3CVertexColorsU3Ek__BackingField_15 = value;
		Il2CppCodeGenWriteBarrier((&___U3CVertexColorsU3Ek__BackingField_15), value);
	}

	inline static int32_t get_offset_of_U3CLastSwapU3Ek__BackingField_16() { return static_cast<int32_t>(offsetof(CubismRenderer_t1960351136, ___U3CLastSwapU3Ek__BackingField_16)); }
	inline SwapInfo_t4238099863  get_U3CLastSwapU3Ek__BackingField_16() const { return ___U3CLastSwapU3Ek__BackingField_16; }
	inline SwapInfo_t4238099863 * get_address_of_U3CLastSwapU3Ek__BackingField_16() { return &___U3CLastSwapU3Ek__BackingField_16; }
	inline void set_U3CLastSwapU3Ek__BackingField_16(SwapInfo_t4238099863  value)
	{
		___U3CLastSwapU3Ek__BackingField_16 = value;
	}

	inline static int32_t get_offset_of_U3CThisSwapU3Ek__BackingField_17() { return static_cast<int32_t>(offsetof(CubismRenderer_t1960351136, ___U3CThisSwapU3Ek__BackingField_17)); }
	inline SwapInfo_t4238099863  get_U3CThisSwapU3Ek__BackingField_17() const { return ___U3CThisSwapU3Ek__BackingField_17; }
	inline SwapInfo_t4238099863 * get_address_of_U3CThisSwapU3Ek__BackingField_17() { return &___U3CThisSwapU3Ek__BackingField_17; }
	inline void set_U3CThisSwapU3Ek__BackingField_17(SwapInfo_t4238099863  value)
	{
		___U3CThisSwapU3Ek__BackingField_17 = value;
	}
};

struct CubismRenderer_t1960351136_StaticFields
{
public:
	// UnityEngine.MaterialPropertyBlock Live2D.Cubism.Rendering.CubismRenderer::_sharedPropertyBlock
	MaterialPropertyBlock_t3303648957 * ____sharedPropertyBlock_18;

public:
	inline static int32_t get_offset_of__sharedPropertyBlock_18() { return static_cast<int32_t>(offsetof(CubismRenderer_t1960351136_StaticFields, ____sharedPropertyBlock_18)); }
	inline MaterialPropertyBlock_t3303648957 * get__sharedPropertyBlock_18() const { return ____sharedPropertyBlock_18; }
	inline MaterialPropertyBlock_t3303648957 ** get_address_of__sharedPropertyBlock_18() { return &____sharedPropertyBlock_18; }
	inline void set__sharedPropertyBlock_18(MaterialPropertyBlock_t3303648957 * value)
	{
		____sharedPropertyBlock_18 = value;
		Il2CppCodeGenWriteBarrier((&____sharedPropertyBlock_18), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CUBISMRENDERER_T1960351136_H
#ifndef CUBISMRENDERCONTROLLER_T3290083685_H
#define CUBISMRENDERCONTROLLER_T3290083685_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Live2D.Cubism.Rendering.CubismRenderController
struct  CubismRenderController_t3290083685  : public MonoBehaviour_t1158329972
{
public:
	// System.Single Live2D.Cubism.Rendering.CubismRenderController::Opacity
	float ___Opacity_2;
	// System.Single Live2D.Cubism.Rendering.CubismRenderController::_lastOpacity
	float ____lastOpacity_3;
	// System.Int32 Live2D.Cubism.Rendering.CubismRenderController::_sortingLayerId
	int32_t ____sortingLayerId_4;
	// Live2D.Cubism.Rendering.CubismSortingMode Live2D.Cubism.Rendering.CubismRenderController::_sortingMode
	int32_t ____sortingMode_5;
	// System.Int32 Live2D.Cubism.Rendering.CubismRenderController::_sortingOrder
	int32_t ____sortingOrder_6;
	// UnityEngine.Camera Live2D.Cubism.Rendering.CubismRenderController::CameraToFace
	Camera_t189460977 * ___CameraToFace_7;
	// UnityEngine.Object Live2D.Cubism.Rendering.CubismRenderController::_drawOrderHandler
	Object_t1021602117 * ____drawOrderHandler_8;
	// Live2D.Cubism.Rendering.ICubismDrawOrderHandler Live2D.Cubism.Rendering.CubismRenderController::_drawOrderHandlerInterface
	RuntimeObject* ____drawOrderHandlerInterface_9;
	// UnityEngine.Object Live2D.Cubism.Rendering.CubismRenderController::_opacityHandler
	Object_t1021602117 * ____opacityHandler_10;
	// Live2D.Cubism.Rendering.ICubismOpacityHandler Live2D.Cubism.Rendering.CubismRenderController::_opacityHandlerInterface
	RuntimeObject* ____opacityHandlerInterface_11;
	// System.Single Live2D.Cubism.Rendering.CubismRenderController::_depthOffset
	float ____depthOffset_12;
	// UnityEngine.Transform Live2D.Cubism.Rendering.CubismRenderController::_drawablesRootTransform
	Transform_t3275118058 * ____drawablesRootTransform_13;
	// Live2D.Cubism.Rendering.CubismRenderer[] Live2D.Cubism.Rendering.CubismRenderController::_renderers
	CubismRendererU5BU5D_t2721529825* ____renderers_14;

public:
	inline static int32_t get_offset_of_Opacity_2() { return static_cast<int32_t>(offsetof(CubismRenderController_t3290083685, ___Opacity_2)); }
	inline float get_Opacity_2() const { return ___Opacity_2; }
	inline float* get_address_of_Opacity_2() { return &___Opacity_2; }
	inline void set_Opacity_2(float value)
	{
		___Opacity_2 = value;
	}

	inline static int32_t get_offset_of__lastOpacity_3() { return static_cast<int32_t>(offsetof(CubismRenderController_t3290083685, ____lastOpacity_3)); }
	inline float get__lastOpacity_3() const { return ____lastOpacity_3; }
	inline float* get_address_of__lastOpacity_3() { return &____lastOpacity_3; }
	inline void set__lastOpacity_3(float value)
	{
		____lastOpacity_3 = value;
	}

	inline static int32_t get_offset_of__sortingLayerId_4() { return static_cast<int32_t>(offsetof(CubismRenderController_t3290083685, ____sortingLayerId_4)); }
	inline int32_t get__sortingLayerId_4() const { return ____sortingLayerId_4; }
	inline int32_t* get_address_of__sortingLayerId_4() { return &____sortingLayerId_4; }
	inline void set__sortingLayerId_4(int32_t value)
	{
		____sortingLayerId_4 = value;
	}

	inline static int32_t get_offset_of__sortingMode_5() { return static_cast<int32_t>(offsetof(CubismRenderController_t3290083685, ____sortingMode_5)); }
	inline int32_t get__sortingMode_5() const { return ____sortingMode_5; }
	inline int32_t* get_address_of__sortingMode_5() { return &____sortingMode_5; }
	inline void set__sortingMode_5(int32_t value)
	{
		____sortingMode_5 = value;
	}

	inline static int32_t get_offset_of__sortingOrder_6() { return static_cast<int32_t>(offsetof(CubismRenderController_t3290083685, ____sortingOrder_6)); }
	inline int32_t get__sortingOrder_6() const { return ____sortingOrder_6; }
	inline int32_t* get_address_of__sortingOrder_6() { return &____sortingOrder_6; }
	inline void set__sortingOrder_6(int32_t value)
	{
		____sortingOrder_6 = value;
	}

	inline static int32_t get_offset_of_CameraToFace_7() { return static_cast<int32_t>(offsetof(CubismRenderController_t3290083685, ___CameraToFace_7)); }
	inline Camera_t189460977 * get_CameraToFace_7() const { return ___CameraToFace_7; }
	inline Camera_t189460977 ** get_address_of_CameraToFace_7() { return &___CameraToFace_7; }
	inline void set_CameraToFace_7(Camera_t189460977 * value)
	{
		___CameraToFace_7 = value;
		Il2CppCodeGenWriteBarrier((&___CameraToFace_7), value);
	}

	inline static int32_t get_offset_of__drawOrderHandler_8() { return static_cast<int32_t>(offsetof(CubismRenderController_t3290083685, ____drawOrderHandler_8)); }
	inline Object_t1021602117 * get__drawOrderHandler_8() const { return ____drawOrderHandler_8; }
	inline Object_t1021602117 ** get_address_of__drawOrderHandler_8() { return &____drawOrderHandler_8; }
	inline void set__drawOrderHandler_8(Object_t1021602117 * value)
	{
		____drawOrderHandler_8 = value;
		Il2CppCodeGenWriteBarrier((&____drawOrderHandler_8), value);
	}

	inline static int32_t get_offset_of__drawOrderHandlerInterface_9() { return static_cast<int32_t>(offsetof(CubismRenderController_t3290083685, ____drawOrderHandlerInterface_9)); }
	inline RuntimeObject* get__drawOrderHandlerInterface_9() const { return ____drawOrderHandlerInterface_9; }
	inline RuntimeObject** get_address_of__drawOrderHandlerInterface_9() { return &____drawOrderHandlerInterface_9; }
	inline void set__drawOrderHandlerInterface_9(RuntimeObject* value)
	{
		____drawOrderHandlerInterface_9 = value;
		Il2CppCodeGenWriteBarrier((&____drawOrderHandlerInterface_9), value);
	}

	inline static int32_t get_offset_of__opacityHandler_10() { return static_cast<int32_t>(offsetof(CubismRenderController_t3290083685, ____opacityHandler_10)); }
	inline Object_t1021602117 * get__opacityHandler_10() const { return ____opacityHandler_10; }
	inline Object_t1021602117 ** get_address_of__opacityHandler_10() { return &____opacityHandler_10; }
	inline void set__opacityHandler_10(Object_t1021602117 * value)
	{
		____opacityHandler_10 = value;
		Il2CppCodeGenWriteBarrier((&____opacityHandler_10), value);
	}

	inline static int32_t get_offset_of__opacityHandlerInterface_11() { return static_cast<int32_t>(offsetof(CubismRenderController_t3290083685, ____opacityHandlerInterface_11)); }
	inline RuntimeObject* get__opacityHandlerInterface_11() const { return ____opacityHandlerInterface_11; }
	inline RuntimeObject** get_address_of__opacityHandlerInterface_11() { return &____opacityHandlerInterface_11; }
	inline void set__opacityHandlerInterface_11(RuntimeObject* value)
	{
		____opacityHandlerInterface_11 = value;
		Il2CppCodeGenWriteBarrier((&____opacityHandlerInterface_11), value);
	}

	inline static int32_t get_offset_of__depthOffset_12() { return static_cast<int32_t>(offsetof(CubismRenderController_t3290083685, ____depthOffset_12)); }
	inline float get__depthOffset_12() const { return ____depthOffset_12; }
	inline float* get_address_of__depthOffset_12() { return &____depthOffset_12; }
	inline void set__depthOffset_12(float value)
	{
		____depthOffset_12 = value;
	}

	inline static int32_t get_offset_of__drawablesRootTransform_13() { return static_cast<int32_t>(offsetof(CubismRenderController_t3290083685, ____drawablesRootTransform_13)); }
	inline Transform_t3275118058 * get__drawablesRootTransform_13() const { return ____drawablesRootTransform_13; }
	inline Transform_t3275118058 ** get_address_of__drawablesRootTransform_13() { return &____drawablesRootTransform_13; }
	inline void set__drawablesRootTransform_13(Transform_t3275118058 * value)
	{
		____drawablesRootTransform_13 = value;
		Il2CppCodeGenWriteBarrier((&____drawablesRootTransform_13), value);
	}

	inline static int32_t get_offset_of__renderers_14() { return static_cast<int32_t>(offsetof(CubismRenderController_t3290083685, ____renderers_14)); }
	inline CubismRendererU5BU5D_t2721529825* get__renderers_14() const { return ____renderers_14; }
	inline CubismRendererU5BU5D_t2721529825** get_address_of__renderers_14() { return &____renderers_14; }
	inline void set__renderers_14(CubismRendererU5BU5D_t2721529825* value)
	{
		____renderers_14 = value;
		Il2CppCodeGenWriteBarrier((&____renderers_14), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CUBISMRENDERCONTROLLER_T3290083685_H
#ifndef CUBISMAUDIOMOUTHINPUT_T1379032486_H
#define CUBISMAUDIOMOUTHINPUT_T1379032486_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Live2D.Cubism.Framework.MouthMovement.CubismAudioMouthInput
struct  CubismAudioMouthInput_t1379032486  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.AudioSource Live2D.Cubism.Framework.MouthMovement.CubismAudioMouthInput::AudioInput
	AudioSource_t1135106623 * ___AudioInput_2;
	// Live2D.Cubism.Framework.MouthMovement.CubismAudioSamplingQuality Live2D.Cubism.Framework.MouthMovement.CubismAudioMouthInput::SamplingQuality
	int32_t ___SamplingQuality_3;
	// System.Single Live2D.Cubism.Framework.MouthMovement.CubismAudioMouthInput::Gain
	float ___Gain_4;
	// System.Single Live2D.Cubism.Framework.MouthMovement.CubismAudioMouthInput::Smoothing
	float ___Smoothing_5;
	// System.Single[] Live2D.Cubism.Framework.MouthMovement.CubismAudioMouthInput::<Samples>k__BackingField
	SingleU5BU5D_t577127397* ___U3CSamplesU3Ek__BackingField_6;
	// System.Single Live2D.Cubism.Framework.MouthMovement.CubismAudioMouthInput::<LastRms>k__BackingField
	float ___U3CLastRmsU3Ek__BackingField_7;
	// System.Single Live2D.Cubism.Framework.MouthMovement.CubismAudioMouthInput::VelocityBuffer
	float ___VelocityBuffer_8;
	// Live2D.Cubism.Framework.MouthMovement.CubismMouthController Live2D.Cubism.Framework.MouthMovement.CubismAudioMouthInput::<Target>k__BackingField
	CubismMouthController_t3330055306 * ___U3CTargetU3Ek__BackingField_9;

public:
	inline static int32_t get_offset_of_AudioInput_2() { return static_cast<int32_t>(offsetof(CubismAudioMouthInput_t1379032486, ___AudioInput_2)); }
	inline AudioSource_t1135106623 * get_AudioInput_2() const { return ___AudioInput_2; }
	inline AudioSource_t1135106623 ** get_address_of_AudioInput_2() { return &___AudioInput_2; }
	inline void set_AudioInput_2(AudioSource_t1135106623 * value)
	{
		___AudioInput_2 = value;
		Il2CppCodeGenWriteBarrier((&___AudioInput_2), value);
	}

	inline static int32_t get_offset_of_SamplingQuality_3() { return static_cast<int32_t>(offsetof(CubismAudioMouthInput_t1379032486, ___SamplingQuality_3)); }
	inline int32_t get_SamplingQuality_3() const { return ___SamplingQuality_3; }
	inline int32_t* get_address_of_SamplingQuality_3() { return &___SamplingQuality_3; }
	inline void set_SamplingQuality_3(int32_t value)
	{
		___SamplingQuality_3 = value;
	}

	inline static int32_t get_offset_of_Gain_4() { return static_cast<int32_t>(offsetof(CubismAudioMouthInput_t1379032486, ___Gain_4)); }
	inline float get_Gain_4() const { return ___Gain_4; }
	inline float* get_address_of_Gain_4() { return &___Gain_4; }
	inline void set_Gain_4(float value)
	{
		___Gain_4 = value;
	}

	inline static int32_t get_offset_of_Smoothing_5() { return static_cast<int32_t>(offsetof(CubismAudioMouthInput_t1379032486, ___Smoothing_5)); }
	inline float get_Smoothing_5() const { return ___Smoothing_5; }
	inline float* get_address_of_Smoothing_5() { return &___Smoothing_5; }
	inline void set_Smoothing_5(float value)
	{
		___Smoothing_5 = value;
	}

	inline static int32_t get_offset_of_U3CSamplesU3Ek__BackingField_6() { return static_cast<int32_t>(offsetof(CubismAudioMouthInput_t1379032486, ___U3CSamplesU3Ek__BackingField_6)); }
	inline SingleU5BU5D_t577127397* get_U3CSamplesU3Ek__BackingField_6() const { return ___U3CSamplesU3Ek__BackingField_6; }
	inline SingleU5BU5D_t577127397** get_address_of_U3CSamplesU3Ek__BackingField_6() { return &___U3CSamplesU3Ek__BackingField_6; }
	inline void set_U3CSamplesU3Ek__BackingField_6(SingleU5BU5D_t577127397* value)
	{
		___U3CSamplesU3Ek__BackingField_6 = value;
		Il2CppCodeGenWriteBarrier((&___U3CSamplesU3Ek__BackingField_6), value);
	}

	inline static int32_t get_offset_of_U3CLastRmsU3Ek__BackingField_7() { return static_cast<int32_t>(offsetof(CubismAudioMouthInput_t1379032486, ___U3CLastRmsU3Ek__BackingField_7)); }
	inline float get_U3CLastRmsU3Ek__BackingField_7() const { return ___U3CLastRmsU3Ek__BackingField_7; }
	inline float* get_address_of_U3CLastRmsU3Ek__BackingField_7() { return &___U3CLastRmsU3Ek__BackingField_7; }
	inline void set_U3CLastRmsU3Ek__BackingField_7(float value)
	{
		___U3CLastRmsU3Ek__BackingField_7 = value;
	}

	inline static int32_t get_offset_of_VelocityBuffer_8() { return static_cast<int32_t>(offsetof(CubismAudioMouthInput_t1379032486, ___VelocityBuffer_8)); }
	inline float get_VelocityBuffer_8() const { return ___VelocityBuffer_8; }
	inline float* get_address_of_VelocityBuffer_8() { return &___VelocityBuffer_8; }
	inline void set_VelocityBuffer_8(float value)
	{
		___VelocityBuffer_8 = value;
	}

	inline static int32_t get_offset_of_U3CTargetU3Ek__BackingField_9() { return static_cast<int32_t>(offsetof(CubismAudioMouthInput_t1379032486, ___U3CTargetU3Ek__BackingField_9)); }
	inline CubismMouthController_t3330055306 * get_U3CTargetU3Ek__BackingField_9() const { return ___U3CTargetU3Ek__BackingField_9; }
	inline CubismMouthController_t3330055306 ** get_address_of_U3CTargetU3Ek__BackingField_9() { return &___U3CTargetU3Ek__BackingField_9; }
	inline void set_U3CTargetU3Ek__BackingField_9(CubismMouthController_t3330055306 * value)
	{
		___U3CTargetU3Ek__BackingField_9 = value;
		Il2CppCodeGenWriteBarrier((&___U3CTargetU3Ek__BackingField_9), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CUBISMAUDIOMOUTHINPUT_T1379032486_H
#ifndef CUBISMAUTOMOUTHINPUT_T1722567851_H
#define CUBISMAUTOMOUTHINPUT_T1722567851_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Live2D.Cubism.Framework.MouthMovement.CubismAutoMouthInput
struct  CubismAutoMouthInput_t1722567851  : public MonoBehaviour_t1158329972
{
public:
	// System.Single Live2D.Cubism.Framework.MouthMovement.CubismAutoMouthInput::Timescale
	float ___Timescale_2;
	// Live2D.Cubism.Framework.MouthMovement.CubismMouthController Live2D.Cubism.Framework.MouthMovement.CubismAutoMouthInput::<Controller>k__BackingField
	CubismMouthController_t3330055306 * ___U3CControllerU3Ek__BackingField_3;
	// System.Single Live2D.Cubism.Framework.MouthMovement.CubismAutoMouthInput::<T>k__BackingField
	float ___U3CTU3Ek__BackingField_4;

public:
	inline static int32_t get_offset_of_Timescale_2() { return static_cast<int32_t>(offsetof(CubismAutoMouthInput_t1722567851, ___Timescale_2)); }
	inline float get_Timescale_2() const { return ___Timescale_2; }
	inline float* get_address_of_Timescale_2() { return &___Timescale_2; }
	inline void set_Timescale_2(float value)
	{
		___Timescale_2 = value;
	}

	inline static int32_t get_offset_of_U3CControllerU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(CubismAutoMouthInput_t1722567851, ___U3CControllerU3Ek__BackingField_3)); }
	inline CubismMouthController_t3330055306 * get_U3CControllerU3Ek__BackingField_3() const { return ___U3CControllerU3Ek__BackingField_3; }
	inline CubismMouthController_t3330055306 ** get_address_of_U3CControllerU3Ek__BackingField_3() { return &___U3CControllerU3Ek__BackingField_3; }
	inline void set_U3CControllerU3Ek__BackingField_3(CubismMouthController_t3330055306 * value)
	{
		___U3CControllerU3Ek__BackingField_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CControllerU3Ek__BackingField_3), value);
	}

	inline static int32_t get_offset_of_U3CTU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(CubismAutoMouthInput_t1722567851, ___U3CTU3Ek__BackingField_4)); }
	inline float get_U3CTU3Ek__BackingField_4() const { return ___U3CTU3Ek__BackingField_4; }
	inline float* get_address_of_U3CTU3Ek__BackingField_4() { return &___U3CTU3Ek__BackingField_4; }
	inline void set_U3CTU3Ek__BackingField_4(float value)
	{
		___U3CTU3Ek__BackingField_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CUBISMAUTOMOUTHINPUT_T1722567851_H
#ifndef CUBISMMOUTHCONTROLLER_T3330055306_H
#define CUBISMMOUTHCONTROLLER_T3330055306_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Live2D.Cubism.Framework.MouthMovement.CubismMouthController
struct  CubismMouthController_t3330055306  : public MonoBehaviour_t1158329972
{
public:
	// Live2D.Cubism.Framework.CubismParameterBlendMode Live2D.Cubism.Framework.MouthMovement.CubismMouthController::BlendMode
	int32_t ___BlendMode_2;
	// System.Single Live2D.Cubism.Framework.MouthMovement.CubismMouthController::MouthOpening
	float ___MouthOpening_3;
	// Live2D.Cubism.Core.CubismParameter[] Live2D.Cubism.Framework.MouthMovement.CubismMouthController::<Destinations>k__BackingField
	CubismParameterU5BU5D_t2848401775* ___U3CDestinationsU3Ek__BackingField_4;

public:
	inline static int32_t get_offset_of_BlendMode_2() { return static_cast<int32_t>(offsetof(CubismMouthController_t3330055306, ___BlendMode_2)); }
	inline int32_t get_BlendMode_2() const { return ___BlendMode_2; }
	inline int32_t* get_address_of_BlendMode_2() { return &___BlendMode_2; }
	inline void set_BlendMode_2(int32_t value)
	{
		___BlendMode_2 = value;
	}

	inline static int32_t get_offset_of_MouthOpening_3() { return static_cast<int32_t>(offsetof(CubismMouthController_t3330055306, ___MouthOpening_3)); }
	inline float get_MouthOpening_3() const { return ___MouthOpening_3; }
	inline float* get_address_of_MouthOpening_3() { return &___MouthOpening_3; }
	inline void set_MouthOpening_3(float value)
	{
		___MouthOpening_3 = value;
	}

	inline static int32_t get_offset_of_U3CDestinationsU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(CubismMouthController_t3330055306, ___U3CDestinationsU3Ek__BackingField_4)); }
	inline CubismParameterU5BU5D_t2848401775* get_U3CDestinationsU3Ek__BackingField_4() const { return ___U3CDestinationsU3Ek__BackingField_4; }
	inline CubismParameterU5BU5D_t2848401775** get_address_of_U3CDestinationsU3Ek__BackingField_4() { return &___U3CDestinationsU3Ek__BackingField_4; }
	inline void set_U3CDestinationsU3Ek__BackingField_4(CubismParameterU5BU5D_t2848401775* value)
	{
		___U3CDestinationsU3Ek__BackingField_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CDestinationsU3Ek__BackingField_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CUBISMMOUTHCONTROLLER_T3330055306_H
#ifndef CUBISMMOUTHPARAMETER_T2844692355_H
#define CUBISMMOUTHPARAMETER_T2844692355_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Live2D.Cubism.Framework.MouthMovement.CubismMouthParameter
struct  CubismMouthParameter_t2844692355  : public MonoBehaviour_t1158329972
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CUBISMMOUTHPARAMETER_T2844692355_H
#ifndef CUBISMPHYSICSCONTROLLER_T2863219344_H
#define CUBISMPHYSICSCONTROLLER_T2863219344_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Live2D.Cubism.Framework.Physics.CubismPhysicsController
struct  CubismPhysicsController_t2863219344  : public MonoBehaviour_t1158329972
{
public:
	// Live2D.Cubism.Framework.Physics.CubismPhysicsRig Live2D.Cubism.Framework.Physics.CubismPhysicsController::_rig
	CubismPhysicsRig_t856779234 * ____rig_2;
	// Live2D.Cubism.Core.CubismParameter[] Live2D.Cubism.Framework.Physics.CubismPhysicsController::<Parameters>k__BackingField
	CubismParameterU5BU5D_t2848401775* ___U3CParametersU3Ek__BackingField_3;

public:
	inline static int32_t get_offset_of__rig_2() { return static_cast<int32_t>(offsetof(CubismPhysicsController_t2863219344, ____rig_2)); }
	inline CubismPhysicsRig_t856779234 * get__rig_2() const { return ____rig_2; }
	inline CubismPhysicsRig_t856779234 ** get_address_of__rig_2() { return &____rig_2; }
	inline void set__rig_2(CubismPhysicsRig_t856779234 * value)
	{
		____rig_2 = value;
		Il2CppCodeGenWriteBarrier((&____rig_2), value);
	}

	inline static int32_t get_offset_of_U3CParametersU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(CubismPhysicsController_t2863219344, ___U3CParametersU3Ek__BackingField_3)); }
	inline CubismParameterU5BU5D_t2848401775* get_U3CParametersU3Ek__BackingField_3() const { return ___U3CParametersU3Ek__BackingField_3; }
	inline CubismParameterU5BU5D_t2848401775** get_address_of_U3CParametersU3Ek__BackingField_3() { return &___U3CParametersU3Ek__BackingField_3; }
	inline void set_U3CParametersU3Ek__BackingField_3(CubismParameterU5BU5D_t2848401775* value)
	{
		___U3CParametersU3Ek__BackingField_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CParametersU3Ek__BackingField_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CUBISMPHYSICSCONTROLLER_T2863219344_H
#ifndef CUBISMHARMONICMOTIONPARAMETER_T3368981047_H
#define CUBISMHARMONICMOTIONPARAMETER_T3368981047_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Live2D.Cubism.Framework.HarmonicMotion.CubismHarmonicMotionParameter
struct  CubismHarmonicMotionParameter_t3368981047  : public MonoBehaviour_t1158329972
{
public:
	// System.Int32 Live2D.Cubism.Framework.HarmonicMotion.CubismHarmonicMotionParameter::Channel
	int32_t ___Channel_2;
	// Live2D.Cubism.Framework.HarmonicMotion.CubismHarmonicMotionDirection Live2D.Cubism.Framework.HarmonicMotion.CubismHarmonicMotionParameter::Direction
	int32_t ___Direction_3;
	// System.Single Live2D.Cubism.Framework.HarmonicMotion.CubismHarmonicMotionParameter::NormalizedOrigin
	float ___NormalizedOrigin_4;
	// System.Single Live2D.Cubism.Framework.HarmonicMotion.CubismHarmonicMotionParameter::NormalizedRange
	float ___NormalizedRange_5;
	// System.Single Live2D.Cubism.Framework.HarmonicMotion.CubismHarmonicMotionParameter::Duration
	float ___Duration_6;
	// System.Single Live2D.Cubism.Framework.HarmonicMotion.CubismHarmonicMotionParameter::<MaximumValue>k__BackingField
	float ___U3CMaximumValueU3Ek__BackingField_7;
	// System.Single Live2D.Cubism.Framework.HarmonicMotion.CubismHarmonicMotionParameter::<MinimumValue>k__BackingField
	float ___U3CMinimumValueU3Ek__BackingField_8;
	// System.Single Live2D.Cubism.Framework.HarmonicMotion.CubismHarmonicMotionParameter::<ValueRange>k__BackingField
	float ___U3CValueRangeU3Ek__BackingField_9;
	// System.Single Live2D.Cubism.Framework.HarmonicMotion.CubismHarmonicMotionParameter::<T>k__BackingField
	float ___U3CTU3Ek__BackingField_10;

public:
	inline static int32_t get_offset_of_Channel_2() { return static_cast<int32_t>(offsetof(CubismHarmonicMotionParameter_t3368981047, ___Channel_2)); }
	inline int32_t get_Channel_2() const { return ___Channel_2; }
	inline int32_t* get_address_of_Channel_2() { return &___Channel_2; }
	inline void set_Channel_2(int32_t value)
	{
		___Channel_2 = value;
	}

	inline static int32_t get_offset_of_Direction_3() { return static_cast<int32_t>(offsetof(CubismHarmonicMotionParameter_t3368981047, ___Direction_3)); }
	inline int32_t get_Direction_3() const { return ___Direction_3; }
	inline int32_t* get_address_of_Direction_3() { return &___Direction_3; }
	inline void set_Direction_3(int32_t value)
	{
		___Direction_3 = value;
	}

	inline static int32_t get_offset_of_NormalizedOrigin_4() { return static_cast<int32_t>(offsetof(CubismHarmonicMotionParameter_t3368981047, ___NormalizedOrigin_4)); }
	inline float get_NormalizedOrigin_4() const { return ___NormalizedOrigin_4; }
	inline float* get_address_of_NormalizedOrigin_4() { return &___NormalizedOrigin_4; }
	inline void set_NormalizedOrigin_4(float value)
	{
		___NormalizedOrigin_4 = value;
	}

	inline static int32_t get_offset_of_NormalizedRange_5() { return static_cast<int32_t>(offsetof(CubismHarmonicMotionParameter_t3368981047, ___NormalizedRange_5)); }
	inline float get_NormalizedRange_5() const { return ___NormalizedRange_5; }
	inline float* get_address_of_NormalizedRange_5() { return &___NormalizedRange_5; }
	inline void set_NormalizedRange_5(float value)
	{
		___NormalizedRange_5 = value;
	}

	inline static int32_t get_offset_of_Duration_6() { return static_cast<int32_t>(offsetof(CubismHarmonicMotionParameter_t3368981047, ___Duration_6)); }
	inline float get_Duration_6() const { return ___Duration_6; }
	inline float* get_address_of_Duration_6() { return &___Duration_6; }
	inline void set_Duration_6(float value)
	{
		___Duration_6 = value;
	}

	inline static int32_t get_offset_of_U3CMaximumValueU3Ek__BackingField_7() { return static_cast<int32_t>(offsetof(CubismHarmonicMotionParameter_t3368981047, ___U3CMaximumValueU3Ek__BackingField_7)); }
	inline float get_U3CMaximumValueU3Ek__BackingField_7() const { return ___U3CMaximumValueU3Ek__BackingField_7; }
	inline float* get_address_of_U3CMaximumValueU3Ek__BackingField_7() { return &___U3CMaximumValueU3Ek__BackingField_7; }
	inline void set_U3CMaximumValueU3Ek__BackingField_7(float value)
	{
		___U3CMaximumValueU3Ek__BackingField_7 = value;
	}

	inline static int32_t get_offset_of_U3CMinimumValueU3Ek__BackingField_8() { return static_cast<int32_t>(offsetof(CubismHarmonicMotionParameter_t3368981047, ___U3CMinimumValueU3Ek__BackingField_8)); }
	inline float get_U3CMinimumValueU3Ek__BackingField_8() const { return ___U3CMinimumValueU3Ek__BackingField_8; }
	inline float* get_address_of_U3CMinimumValueU3Ek__BackingField_8() { return &___U3CMinimumValueU3Ek__BackingField_8; }
	inline void set_U3CMinimumValueU3Ek__BackingField_8(float value)
	{
		___U3CMinimumValueU3Ek__BackingField_8 = value;
	}

	inline static int32_t get_offset_of_U3CValueRangeU3Ek__BackingField_9() { return static_cast<int32_t>(offsetof(CubismHarmonicMotionParameter_t3368981047, ___U3CValueRangeU3Ek__BackingField_9)); }
	inline float get_U3CValueRangeU3Ek__BackingField_9() const { return ___U3CValueRangeU3Ek__BackingField_9; }
	inline float* get_address_of_U3CValueRangeU3Ek__BackingField_9() { return &___U3CValueRangeU3Ek__BackingField_9; }
	inline void set_U3CValueRangeU3Ek__BackingField_9(float value)
	{
		___U3CValueRangeU3Ek__BackingField_9 = value;
	}

	inline static int32_t get_offset_of_U3CTU3Ek__BackingField_10() { return static_cast<int32_t>(offsetof(CubismHarmonicMotionParameter_t3368981047, ___U3CTU3Ek__BackingField_10)); }
	inline float get_U3CTU3Ek__BackingField_10() const { return ___U3CTU3Ek__BackingField_10; }
	inline float* get_address_of_U3CTU3Ek__BackingField_10() { return &___U3CTU3Ek__BackingField_10; }
	inline void set_U3CTU3Ek__BackingField_10(float value)
	{
		___U3CTU3Ek__BackingField_10 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CUBISMHARMONICMOTIONPARAMETER_T3368981047_H
#ifndef CUBISMLOOKTARGETBEHAVIOUR_T3218231626_H
#define CUBISMLOOKTARGETBEHAVIOUR_T3218231626_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Live2D.Cubism.Framework.LookAt.CubismLookTargetBehaviour
struct  CubismLookTargetBehaviour_t3218231626  : public MonoBehaviour_t1158329972
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CUBISMLOOKTARGETBEHAVIOUR_T3218231626_H
#ifndef CUBISMMASKCONTROLLER_T3462095181_H
#define CUBISMMASKCONTROLLER_T3462095181_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Live2D.Cubism.Rendering.Masking.CubismMaskController
struct  CubismMaskController_t3462095181  : public MonoBehaviour_t1158329972
{
public:
	// Live2D.Cubism.Rendering.Masking.CubismMaskTexture Live2D.Cubism.Rendering.Masking.CubismMaskController::_maskTexture
	CubismMaskTexture_t949515734 * ____maskTexture_2;
	// Live2D.Cubism.Rendering.Masking.CubismMaskMaskedJunction[] Live2D.Cubism.Rendering.Masking.CubismMaskController::<Junctions>k__BackingField
	CubismMaskMaskedJunctionU5BU5D_t3161680575* ___U3CJunctionsU3Ek__BackingField_3;

public:
	inline static int32_t get_offset_of__maskTexture_2() { return static_cast<int32_t>(offsetof(CubismMaskController_t3462095181, ____maskTexture_2)); }
	inline CubismMaskTexture_t949515734 * get__maskTexture_2() const { return ____maskTexture_2; }
	inline CubismMaskTexture_t949515734 ** get_address_of__maskTexture_2() { return &____maskTexture_2; }
	inline void set__maskTexture_2(CubismMaskTexture_t949515734 * value)
	{
		____maskTexture_2 = value;
		Il2CppCodeGenWriteBarrier((&____maskTexture_2), value);
	}

	inline static int32_t get_offset_of_U3CJunctionsU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(CubismMaskController_t3462095181, ___U3CJunctionsU3Ek__BackingField_3)); }
	inline CubismMaskMaskedJunctionU5BU5D_t3161680575* get_U3CJunctionsU3Ek__BackingField_3() const { return ___U3CJunctionsU3Ek__BackingField_3; }
	inline CubismMaskMaskedJunctionU5BU5D_t3161680575** get_address_of_U3CJunctionsU3Ek__BackingField_3() { return &___U3CJunctionsU3Ek__BackingField_3; }
	inline void set_U3CJunctionsU3Ek__BackingField_3(CubismMaskMaskedJunctionU5BU5D_t3161680575* value)
	{
		___U3CJunctionsU3Ek__BackingField_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CJunctionsU3Ek__BackingField_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CUBISMMASKCONTROLLER_T3462095181_H
#ifndef CUBISMRAYCASTABLE_T2586420030_H
#define CUBISMRAYCASTABLE_T2586420030_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Live2D.Cubism.Framework.Raycasting.CubismRaycastable
struct  CubismRaycastable_t2586420030  : public MonoBehaviour_t1158329972
{
public:
	// Live2D.Cubism.Framework.Raycasting.CubismRaycastablePrecision Live2D.Cubism.Framework.Raycasting.CubismRaycastable::Precision
	int32_t ___Precision_2;

public:
	inline static int32_t get_offset_of_Precision_2() { return static_cast<int32_t>(offsetof(CubismRaycastable_t2586420030, ___Precision_2)); }
	inline int32_t get_Precision_2() const { return ___Precision_2; }
	inline int32_t* get_address_of_Precision_2() { return &___Precision_2; }
	inline void set_Precision_2(int32_t value)
	{
		___Precision_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CUBISMRAYCASTABLE_T2586420030_H
#ifndef CUBISMRAYCASTER_T3507460997_H
#define CUBISMRAYCASTER_T3507460997_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Live2D.Cubism.Framework.Raycasting.CubismRaycaster
struct  CubismRaycaster_t3507460997  : public MonoBehaviour_t1158329972
{
public:
	// Live2D.Cubism.Rendering.CubismRenderer[] Live2D.Cubism.Framework.Raycasting.CubismRaycaster::<Raycastables>k__BackingField
	CubismRendererU5BU5D_t2721529825* ___U3CRaycastablesU3Ek__BackingField_2;

public:
	inline static int32_t get_offset_of_U3CRaycastablesU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(CubismRaycaster_t3507460997, ___U3CRaycastablesU3Ek__BackingField_2)); }
	inline CubismRendererU5BU5D_t2721529825* get_U3CRaycastablesU3Ek__BackingField_2() const { return ___U3CRaycastablesU3Ek__BackingField_2; }
	inline CubismRendererU5BU5D_t2721529825** get_address_of_U3CRaycastablesU3Ek__BackingField_2() { return &___U3CRaycastablesU3Ek__BackingField_2; }
	inline void set_U3CRaycastablesU3Ek__BackingField_2(CubismRendererU5BU5D_t2721529825* value)
	{
		___U3CRaycastablesU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CRaycastablesU3Ek__BackingField_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CUBISMRAYCASTER_T3507460997_H
#ifndef CUBISMLOOKPARAMETER_T3070566835_H
#define CUBISMLOOKPARAMETER_T3070566835_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Live2D.Cubism.Framework.LookAt.CubismLookParameter
struct  CubismLookParameter_t3070566835  : public MonoBehaviour_t1158329972
{
public:
	// Live2D.Cubism.Framework.LookAt.CubismLookAxis Live2D.Cubism.Framework.LookAt.CubismLookParameter::Axis
	int32_t ___Axis_2;
	// System.Single Live2D.Cubism.Framework.LookAt.CubismLookParameter::Factor
	float ___Factor_3;

public:
	inline static int32_t get_offset_of_Axis_2() { return static_cast<int32_t>(offsetof(CubismLookParameter_t3070566835, ___Axis_2)); }
	inline int32_t get_Axis_2() const { return ___Axis_2; }
	inline int32_t* get_address_of_Axis_2() { return &___Axis_2; }
	inline void set_Axis_2(int32_t value)
	{
		___Axis_2 = value;
	}

	inline static int32_t get_offset_of_Factor_3() { return static_cast<int32_t>(offsetof(CubismLookParameter_t3070566835, ___Factor_3)); }
	inline float get_Factor_3() const { return ___Factor_3; }
	inline float* get_address_of_Factor_3() { return &___Factor_3; }
	inline void set_Factor_3(float value)
	{
		___Factor_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CUBISMLOOKPARAMETER_T3070566835_H





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2000 = { sizeof (CubismUnmanagedMoc_t2103361662), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2000[1] = 
{
	CubismUnmanagedMoc_t2103361662::get_offset_of_U3CPtrU3Ek__BackingField_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2001 = { sizeof (CubismUnmanagedModel_t586274666), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2001[4] = 
{
	CubismUnmanagedModel_t586274666::get_offset_of_U3CParametersU3Ek__BackingField_0(),
	CubismUnmanagedModel_t586274666::get_offset_of_U3CPartsU3Ek__BackingField_1(),
	CubismUnmanagedModel_t586274666::get_offset_of_U3CDrawablesU3Ek__BackingField_2(),
	CubismUnmanagedModel_t586274666::get_offset_of_U3CPtrU3Ek__BackingField_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2002 = { sizeof (CubismUnmanagedParameters_t2764188193), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2002[6] = 
{
	CubismUnmanagedParameters_t2764188193::get_offset_of_U3CCountU3Ek__BackingField_0(),
	CubismUnmanagedParameters_t2764188193::get_offset_of_U3CIdsU3Ek__BackingField_1(),
	CubismUnmanagedParameters_t2764188193::get_offset_of_U3CMinimumValuesU3Ek__BackingField_2(),
	CubismUnmanagedParameters_t2764188193::get_offset_of_U3CMaximumValuesU3Ek__BackingField_3(),
	CubismUnmanagedParameters_t2764188193::get_offset_of_U3CDefaultValuesU3Ek__BackingField_4(),
	CubismUnmanagedParameters_t2764188193::get_offset_of_U3CValuesU3Ek__BackingField_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2003 = { sizeof (CubismUnmanagedParts_t1124036275), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2003[3] = 
{
	CubismUnmanagedParts_t1124036275::get_offset_of_U3CCountU3Ek__BackingField_0(),
	CubismUnmanagedParts_t1124036275::get_offset_of_U3CIdsU3Ek__BackingField_1(),
	CubismUnmanagedParts_t1124036275::get_offset_of_U3COpacitiesU3Ek__BackingField_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2004 = { sizeof (ComponentExtensionMethods_t3489337152), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2005 = { sizeof (CubismAutoEyeBlinkInput_t1359520417), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2005[7] = 
{
	CubismAutoEyeBlinkInput_t1359520417::get_offset_of_Mean_2(),
	CubismAutoEyeBlinkInput_t1359520417::get_offset_of_MaximumDeviation_3(),
	CubismAutoEyeBlinkInput_t1359520417::get_offset_of_Timescale_4(),
	CubismAutoEyeBlinkInput_t1359520417::get_offset_of_U3CControllerU3Ek__BackingField_5(),
	CubismAutoEyeBlinkInput_t1359520417::get_offset_of_U3CTU3Ek__BackingField_6(),
	CubismAutoEyeBlinkInput_t1359520417::get_offset_of_U3CCurrentPhaseU3Ek__BackingField_7(),
	CubismAutoEyeBlinkInput_t1359520417::get_offset_of_U3CLastValueU3Ek__BackingField_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2006 = { sizeof (Phase_t3766709578)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2006[4] = 
{
	Phase_t3766709578::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2007 = { sizeof (CubismDontMoveOnReimportAttribute_t3341164094), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2008 = { sizeof (CubismEyeBlinkController_t4113316510), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2008[3] = 
{
	CubismEyeBlinkController_t4113316510::get_offset_of_BlendMode_2(),
	CubismEyeBlinkController_t4113316510::get_offset_of_EyeOpening_3(),
	CubismEyeBlinkController_t4113316510::get_offset_of_U3CDestinationsU3Ek__BackingField_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2009 = { sizeof (CubismEyeBlinkParameter_t3108136695), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2010 = { sizeof (CubismMoveOnReimportCopyComponentsOnly_t4098471002), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2011 = { sizeof (CubismParameterBlendMode_t532829278)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2011[4] = 
{
	CubismParameterBlendMode_t532829278::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2012 = { sizeof (CubismParameterExtensionMethods_t3888452717), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2013 = { sizeof (CubismParametersInspector_t624078792), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2014 = { sizeof (CubismPartsInspector_t370324902), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2015 = { sizeof (CubismHarmonicMotionController_t427238442), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2015[5] = 
{
	0,
	CubismHarmonicMotionController_t427238442::get_offset_of_BlendMode_3(),
	CubismHarmonicMotionController_t427238442::get_offset_of_ChannelTimescales_4(),
	CubismHarmonicMotionController_t427238442::get_offset_of_U3CSourcesU3Ek__BackingField_5(),
	CubismHarmonicMotionController_t427238442::get_offset_of_U3CDestinationsU3Ek__BackingField_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2016 = { sizeof (CubismHarmonicMotionDirection_t2553246843)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2016[4] = 
{
	CubismHarmonicMotionDirection_t2553246843::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2017 = { sizeof (CubismHarmonicMotionParameter_t3368981047), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2017[9] = 
{
	CubismHarmonicMotionParameter_t3368981047::get_offset_of_Channel_2(),
	CubismHarmonicMotionParameter_t3368981047::get_offset_of_Direction_3(),
	CubismHarmonicMotionParameter_t3368981047::get_offset_of_NormalizedOrigin_4(),
	CubismHarmonicMotionParameter_t3368981047::get_offset_of_NormalizedRange_5(),
	CubismHarmonicMotionParameter_t3368981047::get_offset_of_Duration_6(),
	CubismHarmonicMotionParameter_t3368981047::get_offset_of_U3CMaximumValueU3Ek__BackingField_7(),
	CubismHarmonicMotionParameter_t3368981047::get_offset_of_U3CMinimumValueU3Ek__BackingField_8(),
	CubismHarmonicMotionParameter_t3368981047::get_offset_of_U3CValueRangeU3Ek__BackingField_9(),
	CubismHarmonicMotionParameter_t3368981047::get_offset_of_U3CTU3Ek__BackingField_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2018 = { sizeof (CubismBuiltinPickers_t3599713253), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2019 = { sizeof (CubismModel3Json_t1938818895), -1, sizeof(CubismModel3Json_t1938818895_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2019[9] = 
{
	CubismModel3Json_t1938818895::get_offset_of_U3CAssetPathU3Ek__BackingField_0(),
	CubismModel3Json_t1938818895::get_offset_of_U3CLoadAssetAtPathU3Ek__BackingField_1(),
	CubismModel3Json_t1938818895::get_offset_of_Version_2(),
	CubismModel3Json_t1938818895::get_offset_of_FileReferences_3(),
	CubismModel3Json_t1938818895::get_offset_of_Groups_4(),
	CubismModel3Json_t1938818895::get_offset_of__textures_5(),
	CubismModel3Json_t1938818895_StaticFields::get_offset_of_U3CU3Ef__mgU24cache0_6(),
	CubismModel3Json_t1938818895_StaticFields::get_offset_of_U3CU3Ef__mgU24cache1_7(),
	CubismModel3Json_t1938818895_StaticFields::get_offset_of_U3CU3Ef__mgU24cache2_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2020 = { sizeof (LoadAssetAtPathHandler_t1714529023), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2021 = { sizeof (MaterialPicker_t1430258468), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2022 = { sizeof (TexturePicker_t1644688768), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2023 = { sizeof (SerializableFileReferences_t1075727736)+ sizeof (RuntimeObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2023[3] = 
{
	SerializableFileReferences_t1075727736::get_offset_of_Moc_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	SerializableFileReferences_t1075727736::get_offset_of_Textures_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	SerializableFileReferences_t1075727736::get_offset_of_Physics_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2024 = { sizeof (SerializableGroup_t739349147)+ sizeof (RuntimeObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2024[3] = 
{
	SerializableGroup_t739349147::get_offset_of_Target_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	SerializableGroup_t739349147::get_offset_of_Name_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	SerializableGroup_t739349147::get_offset_of_Ids_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2025 = { sizeof (CubismMotion3Json_t3423029264), -1, sizeof(CubismMotion3Json_t3423029264_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2025[9] = 
{
	CubismMotion3Json_t3423029264::get_offset_of_Version_0(),
	CubismMotion3Json_t3423029264::get_offset_of_Meta_1(),
	CubismMotion3Json_t3423029264::get_offset_of_Curves_2(),
	0,
	CubismMotion3Json_t3423029264_StaticFields::get_offset_of_Parsers_4(),
	CubismMotion3Json_t3423029264_StaticFields::get_offset_of_U3CU3Ef__mgU24cache0_5(),
	CubismMotion3Json_t3423029264_StaticFields::get_offset_of_U3CU3Ef__mgU24cache1_6(),
	CubismMotion3Json_t3423029264_StaticFields::get_offset_of_U3CU3Ef__mgU24cache2_7(),
	CubismMotion3Json_t3423029264_StaticFields::get_offset_of_U3CU3Ef__mgU24cache3_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2026 = { sizeof (SegmentParser_t456729962), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2027 = { sizeof (SerializableMeta_t2740270858)+ sizeof (RuntimeObject), sizeof(SerializableMeta_t2740270858_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable2027[7] = 
{
	SerializableMeta_t2740270858::get_offset_of_Duration_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	SerializableMeta_t2740270858::get_offset_of_Fps_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	SerializableMeta_t2740270858::get_offset_of_Loop_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	SerializableMeta_t2740270858::get_offset_of_CurveCount_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
	SerializableMeta_t2740270858::get_offset_of_TotalSegmentCount_4() + static_cast<int32_t>(sizeof(RuntimeObject)),
	SerializableMeta_t2740270858::get_offset_of_TotalPointCount_5() + static_cast<int32_t>(sizeof(RuntimeObject)),
	SerializableMeta_t2740270858::get_offset_of_AreBeziersRestricted_6() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2028 = { sizeof (SerializableCurve_t3734873318)+ sizeof (RuntimeObject), sizeof(SerializableCurve_t3734873318_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable2028[3] = 
{
	SerializableCurve_t3734873318::get_offset_of_Target_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	SerializableCurve_t3734873318::get_offset_of_Id_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	SerializableCurve_t3734873318::get_offset_of_Segments_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2029 = { sizeof (CubismPhysics3Json_t875977367), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2029[3] = 
{
	CubismPhysics3Json_t875977367::get_offset_of_Version_0(),
	CubismPhysics3Json_t875977367::get_offset_of_Meta_1(),
	CubismPhysics3Json_t875977367::get_offset_of_PhysicsSettings_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2030 = { sizeof (SerializableVector2_t65906335)+ sizeof (RuntimeObject), sizeof(SerializableVector2_t65906335 ), 0, 0 };
extern const int32_t g_FieldOffsetTable2030[2] = 
{
	SerializableVector2_t65906335::get_offset_of_X_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	SerializableVector2_t65906335::get_offset_of_Y_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2031 = { sizeof (SerializableNormalizationValue_t1779412570)+ sizeof (RuntimeObject), sizeof(SerializableNormalizationValue_t1779412570 ), 0, 0 };
extern const int32_t g_FieldOffsetTable2031[3] = 
{
	SerializableNormalizationValue_t1779412570::get_offset_of_Minimum_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	SerializableNormalizationValue_t1779412570::get_offset_of_Default_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	SerializableNormalizationValue_t1779412570::get_offset_of_Maximum_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2032 = { sizeof (SerializableParameter_t2924283429)+ sizeof (RuntimeObject), sizeof(SerializableParameter_t2924283429_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable2032[2] = 
{
	SerializableParameter_t2924283429::get_offset_of_Target_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	SerializableParameter_t2924283429::get_offset_of_Id_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2033 = { sizeof (SerializableInput_t1668668964)+ sizeof (RuntimeObject), sizeof(SerializableInput_t1668668964_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable2033[4] = 
{
	SerializableInput_t1668668964::get_offset_of_Source_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	SerializableInput_t1668668964::get_offset_of_Weight_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	SerializableInput_t1668668964::get_offset_of_Type_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	SerializableInput_t1668668964::get_offset_of_Reflect_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2034 = { sizeof (SerializableOutput_t2803511543)+ sizeof (RuntimeObject), sizeof(SerializableOutput_t2803511543_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable2034[6] = 
{
	SerializableOutput_t2803511543::get_offset_of_Destination_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	SerializableOutput_t2803511543::get_offset_of_VertexIndex_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	SerializableOutput_t2803511543::get_offset_of_Scale_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	SerializableOutput_t2803511543::get_offset_of_Weight_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
	SerializableOutput_t2803511543::get_offset_of_Type_4() + static_cast<int32_t>(sizeof(RuntimeObject)),
	SerializableOutput_t2803511543::get_offset_of_Reflect_5() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2035 = { sizeof (SerializableVertex_t1882814154)+ sizeof (RuntimeObject), sizeof(SerializableVertex_t1882814154 ), 0, 0 };
extern const int32_t g_FieldOffsetTable2035[5] = 
{
	SerializableVertex_t1882814154::get_offset_of_Position_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	SerializableVertex_t1882814154::get_offset_of_Mobility_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	SerializableVertex_t1882814154::get_offset_of_Delay_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	SerializableVertex_t1882814154::get_offset_of_Acceleration_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
	SerializableVertex_t1882814154::get_offset_of_Radius_4() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2036 = { sizeof (SerializableNormalization_t793291037)+ sizeof (RuntimeObject), sizeof(SerializableNormalization_t793291037 ), 0, 0 };
extern const int32_t g_FieldOffsetTable2036[2] = 
{
	SerializableNormalization_t793291037::get_offset_of_Position_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	SerializableNormalization_t793291037::get_offset_of_Angle_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2037 = { sizeof (SerializablePhysicsSettings_t496799432)+ sizeof (RuntimeObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2037[5] = 
{
	SerializablePhysicsSettings_t496799432::get_offset_of_Id_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	SerializablePhysicsSettings_t496799432::get_offset_of_Input_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	SerializablePhysicsSettings_t496799432::get_offset_of_Output_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	SerializablePhysicsSettings_t496799432::get_offset_of_Vertices_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
	SerializablePhysicsSettings_t496799432::get_offset_of_Normalization_4() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2038 = { sizeof (SerializableMeta_t1007752097)+ sizeof (RuntimeObject), sizeof(SerializableMeta_t1007752097 ), 0, 0 };
extern const int32_t g_FieldOffsetTable2038[5] = 
{
	SerializableMeta_t1007752097::get_offset_of_PhysicsSettingCount_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	SerializableMeta_t1007752097::get_offset_of_TotalInputCount_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	SerializableMeta_t1007752097::get_offset_of_TotalOutputCount_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	SerializableMeta_t1007752097::get_offset_of_TotalVertexCount_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
	SerializableMeta_t1007752097::get_offset_of_EffectiveForces_4() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2039 = { sizeof (SerializableEffectiveForces_t82278831)+ sizeof (RuntimeObject), sizeof(SerializableEffectiveForces_t82278831 ), 0, 0 };
extern const int32_t g_FieldOffsetTable2039[2] = 
{
	SerializableEffectiveForces_t82278831::get_offset_of_Gravity_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	SerializableEffectiveForces_t82278831::get_offset_of_Wind_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2040 = { sizeof (CubismLookAxis_t3392553031)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2040[4] = 
{
	CubismLookAxis_t3392553031::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2041 = { sizeof (CubismLookController_t1393487782), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2041[10] = 
{
	CubismLookController_t1393487782::get_offset_of_BlendMode_2(),
	CubismLookController_t1393487782::get_offset_of__target_3(),
	CubismLookController_t1393487782::get_offset_of__targetInterface_4(),
	CubismLookController_t1393487782::get_offset_of_Center_5(),
	CubismLookController_t1393487782::get_offset_of_Damping_6(),
	CubismLookController_t1393487782::get_offset_of_U3CSourcesU3Ek__BackingField_7(),
	CubismLookController_t1393487782::get_offset_of_U3CDestinationsU3Ek__BackingField_8(),
	CubismLookController_t1393487782::get_offset_of_U3CLastPositionU3Ek__BackingField_9(),
	CubismLookController_t1393487782::get_offset_of_U3CGoalPositionU3Ek__BackingField_10(),
	CubismLookController_t1393487782::get_offset_of_VelocityBuffer_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2042 = { sizeof (CubismLookParameter_t3070566835), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2042[2] = 
{
	CubismLookParameter_t3070566835::get_offset_of_Axis_2(),
	CubismLookParameter_t3070566835::get_offset_of_Factor_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2043 = { sizeof (CubismLookTargetBehaviour_t3218231626), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2044 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2045 = { sizeof (CubismAudioMouthInput_t1379032486), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2045[8] = 
{
	CubismAudioMouthInput_t1379032486::get_offset_of_AudioInput_2(),
	CubismAudioMouthInput_t1379032486::get_offset_of_SamplingQuality_3(),
	CubismAudioMouthInput_t1379032486::get_offset_of_Gain_4(),
	CubismAudioMouthInput_t1379032486::get_offset_of_Smoothing_5(),
	CubismAudioMouthInput_t1379032486::get_offset_of_U3CSamplesU3Ek__BackingField_6(),
	CubismAudioMouthInput_t1379032486::get_offset_of_U3CLastRmsU3Ek__BackingField_7(),
	CubismAudioMouthInput_t1379032486::get_offset_of_VelocityBuffer_8(),
	CubismAudioMouthInput_t1379032486::get_offset_of_U3CTargetU3Ek__BackingField_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2046 = { sizeof (CubismAudioSamplingQuality_t1105222369)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2046[4] = 
{
	CubismAudioSamplingQuality_t1105222369::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2047 = { sizeof (CubismAutoMouthInput_t1722567851), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2047[3] = 
{
	CubismAutoMouthInput_t1722567851::get_offset_of_Timescale_2(),
	CubismAutoMouthInput_t1722567851::get_offset_of_U3CControllerU3Ek__BackingField_3(),
	CubismAutoMouthInput_t1722567851::get_offset_of_U3CTU3Ek__BackingField_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2048 = { sizeof (CubismMouthController_t3330055306), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2048[3] = 
{
	CubismMouthController_t3330055306::get_offset_of_BlendMode_2(),
	CubismMouthController_t3330055306::get_offset_of_MouthOpening_3(),
	CubismMouthController_t3330055306::get_offset_of_U3CDestinationsU3Ek__BackingField_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2049 = { sizeof (CubismMouthParameter_t2844692355), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2050 = { sizeof (ObjectExtensionMethods_t2018374342), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2051 = { sizeof (CubismPhysics_t1257910384), -1, sizeof(CubismPhysics_t1257910384_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2051[6] = 
{
	CubismPhysics_t1257910384_StaticFields::get_offset_of_Wind_0(),
	CubismPhysics_t1257910384_StaticFields::get_offset_of_AirResistance_1(),
	CubismPhysics_t1257910384_StaticFields::get_offset_of_MaximumWeight_2(),
	CubismPhysics_t1257910384_StaticFields::get_offset_of_UseFixedDeltaTime_3(),
	CubismPhysics_t1257910384_StaticFields::get_offset_of_UseAngleCorrection_4(),
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2052 = { sizeof (CubismPhysicsController_t2863219344), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2052[2] = 
{
	CubismPhysicsController_t2863219344::get_offset_of__rig_2(),
	CubismPhysicsController_t2863219344::get_offset_of_U3CParametersU3Ek__BackingField_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2053 = { sizeof (CubismPhysicsInput_t2391170328)+ sizeof (RuntimeObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2053[8] = 
{
	CubismPhysicsInput_t2391170328::get_offset_of_SourceId_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	CubismPhysicsInput_t2391170328::get_offset_of_ScaleOfTranslation_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	CubismPhysicsInput_t2391170328::get_offset_of_AngleScale_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	CubismPhysicsInput_t2391170328::get_offset_of_Weight_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
	CubismPhysicsInput_t2391170328::get_offset_of_SourceComponent_4() + static_cast<int32_t>(sizeof(RuntimeObject)),
	CubismPhysicsInput_t2391170328::get_offset_of_IsInverted_5() + static_cast<int32_t>(sizeof(RuntimeObject)),
	CubismPhysicsInput_t2391170328::get_offset_of_Source_6() + static_cast<int32_t>(sizeof(RuntimeObject)),
	CubismPhysicsInput_t2391170328::get_offset_of_GetNormalizedParameterValue_7() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2054 = { sizeof (NormalizedParameterValueGetter_t2024973737), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2055 = { sizeof (CubismPhysicsMath_t1623149510), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2056 = { sizeof (CubismPhysicsNormalizationTuplet_t3360392585)+ sizeof (RuntimeObject), sizeof(CubismPhysicsNormalizationTuplet_t3360392585 ), 0, 0 };
extern const int32_t g_FieldOffsetTable2056[3] = 
{
	CubismPhysicsNormalizationTuplet_t3360392585::get_offset_of_Maximum_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	CubismPhysicsNormalizationTuplet_t3360392585::get_offset_of_Minimum_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	CubismPhysicsNormalizationTuplet_t3360392585::get_offset_of_Default_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2057 = { sizeof (CubismPhysicsNormalization_t2601092345)+ sizeof (RuntimeObject), sizeof(CubismPhysicsNormalization_t2601092345 ), 0, 0 };
extern const int32_t g_FieldOffsetTable2057[2] = 
{
	CubismPhysicsNormalization_t2601092345::get_offset_of_Position_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	CubismPhysicsNormalization_t2601092345::get_offset_of_Angle_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2058 = { sizeof (CubismPhysicsOutput_t1090441091)+ sizeof (RuntimeObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2058[12] = 
{
	CubismPhysicsOutput_t1090441091::get_offset_of_DestinationId_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	CubismPhysicsOutput_t1090441091::get_offset_of_ParticleIndex_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	CubismPhysicsOutput_t1090441091::get_offset_of_TranslationScale_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	CubismPhysicsOutput_t1090441091::get_offset_of_AngleScale_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
	CubismPhysicsOutput_t1090441091::get_offset_of_Weight_4() + static_cast<int32_t>(sizeof(RuntimeObject)),
	CubismPhysicsOutput_t1090441091::get_offset_of_SourceComponent_5() + static_cast<int32_t>(sizeof(RuntimeObject)),
	CubismPhysicsOutput_t1090441091::get_offset_of_IsInverted_6() + static_cast<int32_t>(sizeof(RuntimeObject)),
	CubismPhysicsOutput_t1090441091::get_offset_of_ValueBelowMinimum_7() + static_cast<int32_t>(sizeof(RuntimeObject)),
	CubismPhysicsOutput_t1090441091::get_offset_of_ValueExceededMaximum_8() + static_cast<int32_t>(sizeof(RuntimeObject)),
	CubismPhysicsOutput_t1090441091::get_offset_of_Destination_9() + static_cast<int32_t>(sizeof(RuntimeObject)),
	CubismPhysicsOutput_t1090441091::get_offset_of_GetValue_10() + static_cast<int32_t>(sizeof(RuntimeObject)),
	CubismPhysicsOutput_t1090441091::get_offset_of_GetScale_11() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2059 = { sizeof (ValueGetter_t1451636242), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2060 = { sizeof (ScaleGetter_t3351733751), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2061 = { sizeof (CubismPhysicsParticle_t2883958254)+ sizeof (RuntimeObject), sizeof(CubismPhysicsParticle_t2883958254 ), 0, 0 };
extern const int32_t g_FieldOffsetTable2061[10] = 
{
	CubismPhysicsParticle_t2883958254::get_offset_of_InitialPosition_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	CubismPhysicsParticle_t2883958254::get_offset_of_Mobility_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	CubismPhysicsParticle_t2883958254::get_offset_of_Delay_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	CubismPhysicsParticle_t2883958254::get_offset_of_Acceleration_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
	CubismPhysicsParticle_t2883958254::get_offset_of_Radius_4() + static_cast<int32_t>(sizeof(RuntimeObject)),
	CubismPhysicsParticle_t2883958254::get_offset_of_Position_5() + static_cast<int32_t>(sizeof(RuntimeObject)),
	CubismPhysicsParticle_t2883958254::get_offset_of_LastPosition_6() + static_cast<int32_t>(sizeof(RuntimeObject)),
	CubismPhysicsParticle_t2883958254::get_offset_of_LastGravity_7() + static_cast<int32_t>(sizeof(RuntimeObject)),
	CubismPhysicsParticle_t2883958254::get_offset_of_Force_8() + static_cast<int32_t>(sizeof(RuntimeObject)),
	CubismPhysicsParticle_t2883958254::get_offset_of_Velocity_9() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2062 = { sizeof (CubismPhysicsRig_t856779234), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2062[2] = 
{
	CubismPhysicsRig_t856779234::get_offset_of_SubRigs_0(),
	CubismPhysicsRig_t856779234::get_offset_of_U3CControllerU3Ek__BackingField_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2063 = { sizeof (CubismPhysicsSourceComponent_t1993572628)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2063[4] = 
{
	CubismPhysicsSourceComponent_t1993572628::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2064 = { sizeof (CubismPhysicsSubRig_t1824845752), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2064[5] = 
{
	CubismPhysicsSubRig_t1824845752::get_offset_of_Input_0(),
	CubismPhysicsSubRig_t1824845752::get_offset_of_Output_1(),
	CubismPhysicsSubRig_t1824845752::get_offset_of_Particles_2(),
	CubismPhysicsSubRig_t1824845752::get_offset_of_Normalization_3(),
	CubismPhysicsSubRig_t1824845752::get_offset_of__rig_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2065 = { sizeof (CubismRaycastable_t2586420030), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2065[1] = 
{
	CubismRaycastable_t2586420030::get_offset_of_Precision_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2066 = { sizeof (CubismRaycastablePrecision_t2374123498)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2066[3] = 
{
	CubismRaycastablePrecision_t2374123498::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2067 = { sizeof (CubismRaycaster_t3507460997), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2067[1] = 
{
	CubismRaycaster_t3507460997::get_offset_of_U3CRaycastablesU3Ek__BackingField_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2068 = { sizeof (CubismRaycastHit_t1208447795)+ sizeof (RuntimeObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2068[4] = 
{
	CubismRaycastHit_t1208447795::get_offset_of_Drawable_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	CubismRaycastHit_t1208447795::get_offset_of_Distance_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	CubismRaycastHit_t1208447795::get_offset_of_LocalPosition_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	CubismRaycastHit_t1208447795::get_offset_of_WorldPosition_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2069 = { sizeof (CubismBuiltinAsyncTaskHandler_t2731907693), -1, sizeof(CubismBuiltinAsyncTaskHandler_t2731907693_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2069[9] = 
{
	CubismBuiltinAsyncTaskHandler_t2731907693_StaticFields::get_offset_of_U3CTasksU3Ek__BackingField_0(),
	CubismBuiltinAsyncTaskHandler_t2731907693_StaticFields::get_offset_of_U3CWorkerU3Ek__BackingField_1(),
	CubismBuiltinAsyncTaskHandler_t2731907693_StaticFields::get_offset_of_U3CLockU3Ek__BackingField_2(),
	CubismBuiltinAsyncTaskHandler_t2731907693_StaticFields::get_offset_of_U3CSignalU3Ek__BackingField_3(),
	CubismBuiltinAsyncTaskHandler_t2731907693_StaticFields::get_offset_of__callItADay_4(),
	CubismBuiltinAsyncTaskHandler_t2731907693_StaticFields::get_offset_of_U3CU3Ef__mgU24cache0_5(),
	CubismBuiltinAsyncTaskHandler_t2731907693_StaticFields::get_offset_of_U3CU3Ef__mgU24cache1_6(),
	CubismBuiltinAsyncTaskHandler_t2731907693_StaticFields::get_offset_of_U3CU3Ef__mgU24cache2_7(),
	CubismBuiltinAsyncTaskHandler_t2731907693_StaticFields::get_offset_of_U3CU3Ef__mgU24cache3_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2070 = { sizeof (ArrayExtensionMethods_t2010526758), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2071 = { sizeof (CubismBuiltinMaterials_t3009607876), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2071[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2072 = { sizeof (CubismBuiltinShaders_t2033756950), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2073 = { sizeof (CubismRenderController_t3290083685), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2073[13] = 
{
	CubismRenderController_t3290083685::get_offset_of_Opacity_2(),
	CubismRenderController_t3290083685::get_offset_of__lastOpacity_3(),
	CubismRenderController_t3290083685::get_offset_of__sortingLayerId_4(),
	CubismRenderController_t3290083685::get_offset_of__sortingMode_5(),
	CubismRenderController_t3290083685::get_offset_of__sortingOrder_6(),
	CubismRenderController_t3290083685::get_offset_of_CameraToFace_7(),
	CubismRenderController_t3290083685::get_offset_of__drawOrderHandler_8(),
	CubismRenderController_t3290083685::get_offset_of__drawOrderHandlerInterface_9(),
	CubismRenderController_t3290083685::get_offset_of__opacityHandler_10(),
	CubismRenderController_t3290083685::get_offset_of__opacityHandlerInterface_11(),
	CubismRenderController_t3290083685::get_offset_of__depthOffset_12(),
	CubismRenderController_t3290083685::get_offset_of__drawablesRootTransform_13(),
	CubismRenderController_t3290083685::get_offset_of__renderers_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2074 = { sizeof (CubismRenderer_t1960351136), -1, sizeof(CubismRenderer_t1960351136_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2074[17] = 
{
	CubismRenderer_t1960351136::get_offset_of__localSortingOrder_2(),
	CubismRenderer_t1960351136::get_offset_of__color_3(),
	CubismRenderer_t1960351136::get_offset_of__mainTexture_4(),
	CubismRenderer_t1960351136::get_offset_of_U3CMeshesU3Ek__BackingField_5(),
	CubismRenderer_t1960351136::get_offset_of_U3CFrontMeshU3Ek__BackingField_6(),
	CubismRenderer_t1960351136::get_offset_of_U3CBackMeshU3Ek__BackingField_7(),
	CubismRenderer_t1960351136::get_offset_of__meshFilter_8(),
	CubismRenderer_t1960351136::get_offset_of__meshRenderer_9(),
	CubismRenderer_t1960351136::get_offset_of__sortingMode_10(),
	CubismRenderer_t1960351136::get_offset_of__sortingOrder_11(),
	CubismRenderer_t1960351136::get_offset_of__renderOrder_12(),
	CubismRenderer_t1960351136::get_offset_of__depthOffset_13(),
	CubismRenderer_t1960351136::get_offset_of__opacity_14(),
	CubismRenderer_t1960351136::get_offset_of_U3CVertexColorsU3Ek__BackingField_15(),
	CubismRenderer_t1960351136::get_offset_of_U3CLastSwapU3Ek__BackingField_16(),
	CubismRenderer_t1960351136::get_offset_of_U3CThisSwapU3Ek__BackingField_17(),
	CubismRenderer_t1960351136_StaticFields::get_offset_of__sharedPropertyBlock_18(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2075 = { sizeof (SwapInfo_t4238099863)+ sizeof (RuntimeObject), sizeof(SwapInfo_t4238099863_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable2075[3] = 
{
	SwapInfo_t4238099863::get_offset_of_U3CNewVertexPositionsU3Ek__BackingField_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	SwapInfo_t4238099863::get_offset_of_U3CNewVertexColorsU3Ek__BackingField_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	SwapInfo_t4238099863::get_offset_of_U3CDidBecomeVisibleU3Ek__BackingField_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2076 = { sizeof (CubismShaderVariables_t4252307807), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2076[5] = 
{
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2077 = { sizeof (CubismSortingMode_t663667736)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2077[5] = 
{
	CubismSortingMode_t663667736::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2078 = { sizeof (CubismSortingModeExtensionMethods_t3847127943), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2079 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2080 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2081 = { sizeof (CubismMaskCommandBuffer_t876406890), -1, sizeof(CubismMaskCommandBuffer_t876406890_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2081[2] = 
{
	CubismMaskCommandBuffer_t876406890_StaticFields::get_offset_of_U3CSourcesU3Ek__BackingField_2(),
	CubismMaskCommandBuffer_t876406890_StaticFields::get_offset_of_U3CBufferU3Ek__BackingField_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2082 = { sizeof (U3CRemoveSourceU3Ec__AnonStorey0_t1891205932), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2082[1] = 
{
	U3CRemoveSourceU3Ec__AnonStorey0_t1891205932::get_offset_of_source_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2083 = { sizeof (CubismMaskController_t3462095181), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2083[2] = 
{
	CubismMaskController_t3462095181::get_offset_of__maskTexture_2(),
	CubismMaskController_t3462095181::get_offset_of_U3CJunctionsU3Ek__BackingField_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2084 = { sizeof (MasksMaskedsPair_t1105140970)+ sizeof (RuntimeObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2084[2] = 
{
	MasksMaskedsPair_t1105140970::get_offset_of_Masks_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	MasksMaskedsPair_t1105140970::get_offset_of_Maskeds_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2085 = { sizeof (MasksMaskedsPairs_t4062112651), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2085[1] = 
{
	MasksMaskedsPairs_t4062112651::get_offset_of_Entries_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2086 = { sizeof (CubismMaskMaskedJunction_t3351726490), -1, sizeof(CubismMaskMaskedJunction_t3351726490_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2086[6] = 
{
	CubismMaskMaskedJunction_t3351726490_StaticFields::get_offset_of_U3CSharedMaskPropertiesU3Ek__BackingField_0(),
	CubismMaskMaskedJunction_t3351726490::get_offset_of_U3CMasksU3Ek__BackingField_1(),
	CubismMaskMaskedJunction_t3351726490::get_offset_of_U3CMaskedsU3Ek__BackingField_2(),
	CubismMaskMaskedJunction_t3351726490::get_offset_of_U3CMaskTextureU3Ek__BackingField_3(),
	CubismMaskMaskedJunction_t3351726490::get_offset_of_U3CMaskTileU3Ek__BackingField_4(),
	CubismMaskMaskedJunction_t3351726490::get_offset_of_U3CMaskTransformU3Ek__BackingField_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2087 = { sizeof (CubismMaskProperties_t4215298192), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2087[3] = 
{
	CubismMaskProperties_t4215298192::get_offset_of_Texture_0(),
	CubismMaskProperties_t4215298192::get_offset_of_Tile_1(),
	CubismMaskProperties_t4215298192::get_offset_of_Transform_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2088 = { sizeof (CubismMaskRenderer_t2642533120), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2088[3] = 
{
	CubismMaskRenderer_t2642533120::get_offset_of_U3CMaskPropertiesU3Ek__BackingField_0(),
	CubismMaskRenderer_t2642533120::get_offset_of_U3CMainRendererU3Ek__BackingField_1(),
	CubismMaskRenderer_t2642533120::get_offset_of_U3CMaskMaterialU3Ek__BackingField_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2089 = { sizeof (CubismMaskRendererExtensionMethods_t1294840099), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2090 = { sizeof (CubismMaskTexture_t949515734), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2090[5] = 
{
	CubismMaskTexture_t949515734::get_offset_of__size_2(),
	CubismMaskTexture_t949515734::get_offset_of__subdivisions_3(),
	CubismMaskTexture_t949515734::get_offset_of_U3CTilePoolU3Ek__BackingField_4(),
	CubismMaskTexture_t949515734::get_offset_of__renderTexture_5(),
	CubismMaskTexture_t949515734::get_offset_of_U3CSourcesU3Ek__BackingField_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2091 = { sizeof (SourcesItem_t2559393707)+ sizeof (RuntimeObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2091[2] = 
{
	SourcesItem_t2559393707::get_offset_of_Source_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	SourcesItem_t2559393707::get_offset_of_Tiles_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2092 = { sizeof (U3CAddSourceU3Ec__AnonStorey0_t3314713517), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2092[1] = 
{
	U3CAddSourceU3Ec__AnonStorey0_t3314713517::get_offset_of_source_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2093 = { sizeof (U3CRemoveSourceU3Ec__AnonStorey1_t4159041179), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2093[1] = 
{
	U3CRemoveSourceU3Ec__AnonStorey1_t4159041179::get_offset_of_source_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2094 = { sizeof (CubismMaskTile_t3996512637)+ sizeof (RuntimeObject), sizeof(CubismMaskTile_t3996512637 ), 0, 0 };
extern const int32_t g_FieldOffsetTable2094[4] = 
{
	CubismMaskTile_t3996512637::get_offset_of_Channel_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	CubismMaskTile_t3996512637::get_offset_of_Column_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	CubismMaskTile_t3996512637::get_offset_of_Row_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	CubismMaskTile_t3996512637::get_offset_of_Size_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2095 = { sizeof (CubismMaskTilePool_t2959163307), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2095[2] = 
{
	CubismMaskTilePool_t2959163307::get_offset_of_U3CSubdivisionsU3Ek__BackingField_0(),
	CubismMaskTilePool_t2959163307::get_offset_of_U3CSlotsU3Ek__BackingField_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2096 = { sizeof (CubismMaskTransform_t2215722789)+ sizeof (RuntimeObject), sizeof(CubismMaskTransform_t2215722789 ), sizeof(CubismMaskTransform_t2215722789_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2096[3] = 
{
	CubismMaskTransform_t2215722789_StaticFields::get_offset_of__uniqueId_0(),
	CubismMaskTransform_t2215722789::get_offset_of_Offset_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	CubismMaskTransform_t2215722789::get_offset_of_Scale_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2097 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2098 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2099 = { sizeof (IntExtensionMethods_t515585024), -1, 0, 0 };
#ifdef __clang__
#pragma clang diagnostic pop
#endif
