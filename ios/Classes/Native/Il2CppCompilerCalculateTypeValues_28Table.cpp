﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "object-internals.h"

// System.String
struct String_t;
// UnityEngine.UI.Toggle
struct Toggle_t3976754468;
// Utage.UguiToggleGroupIndexed
struct UguiToggleGroupIndexed_t999166452;
// Utage.ITransition[]
struct ITransitionU5BU5D_t1588618539;
// Utage.UguiView
struct UguiView_t2174908005;
// System.Predicate`1<Utage.ITransition>
struct Predicate_1_t3407471265;
// UnityEngine.CanvasGroup
struct CanvasGroup_t3296560743;
// Utage.UguiViewTransitionCrossFade
struct UguiViewTransitionCrossFade_t1724059380;
// Utage.EyeBlinkBase
struct EyeBlinkBase_t1662968566;
// UnityEngine.Events.InvokableCallList
struct InvokableCallList_t2295673753;
// UnityEngine.Events.PersistentCallGroup
struct PersistentCallGroup_t339478082;
// Utage.UguiNovelTextCharacter
struct UguiNovelTextCharacter_t1244198124;
// UnityEngine.RectTransform
struct RectTransform_t3349966182;
// System.Collections.Generic.List`1<Utage.UguiNovelTextGeneratorAdditionalRuby>
struct List_1_t4289814645;
// System.Collections.Generic.List`1<Utage.UguiNovelTextGeneratorAdditionalLine>
struct List_1_t1237941671;
// System.Collections.Generic.List`1<Utage.UguiNovelTextCharacter>
struct List_1_t613319256;
// Utage.UguiNovelTextLine
struct UguiNovelTextLine_t2439763533;
// System.Action`1<Utage.UguiCategoryGridPage>
struct Action_1_t475622313;
// Utage.UguiCategoryGridPage
struct UguiCategoryGridPage_t673822931;
// UnityEngine.Texture
struct Texture_t2243626319;
// UnityEngine.UI.RawImage
struct RawImage_t2749640213;
// Utage.UguiFadeTextureStream/FadeTextureInfo[]
struct FadeTextureInfoU5BU5D_t3972777596;
// Utage.UguiFadeTextureStream/FadeTextureInfo
struct FadeTextureInfo_t2015309025;
// Utage.UguiFadeTextureStream
struct UguiFadeTextureStream_t4272889985;
// UnityEngine.Font
struct Font_t4239498691;
// System.Collections.Generic.List`1<System.String>
struct List_1_t1398341365;
// System.Action
struct Action_t3226471752;
// Utage.DicingAnimation
struct DicingAnimation_t3770383148;
// System.Collections.Generic.List`1<System.Int32>
struct List_1_t1440998580;
// System.Collections.Generic.List`1<Utage.DicingTextureData/QuadVerts>
struct List_1_t4123338219;
// Utage.DicingTextureData/<ForeachVertexList>c__AnonStorey1
struct U3CForeachVertexListU3Ec__AnonStorey1_t1404970980;
// System.Object[]
struct ObjectU5BU5D_t3614634134;
// System.Char[]
struct CharU5BU5D_t1328083999;
// System.Collections.Generic.List`1<Utage.MiniAnimationData/Data>
struct List_1_t4284043194;
// Utage.MiniAnimationData/Data
struct Data_t619954766;
// System.Void
struct Void_t1841601450;
// System.Collections.Generic.Dictionary`2<System.String,System.Int32>
struct Dictionary_2_t3986656710;
// Utage.LipSynchDicing
struct LipSynchDicing_t1869722688;
// Utage.LipSynchAvatar
struct LipSynchAvatar_t872933325;
// UnityEngine.UI.VertexHelper
struct VertexHelper_t385374196;
// Utage.EyeBlinkDicing
struct EyeBlinkDicing_t891209267;
// System.Action`2<UnityEngine.Rect,UnityEngine.Rect>
struct Action_2_t1249439937;
// Utage.DicingTextureData
struct DicingTextureData_t752335795;
// Utage.UguiNovelText
struct UguiNovelText_t4135744055;
// System.Collections.Generic.List`1<UnityEngine.Rect>
struct List_1_t3050876758;
// System.Collections.Generic.List`1<UnityEngine.Texture2D>
struct List_1_t2912116861;
// System.Collections.Generic.List`1<Utage.DicingTextureData>
struct List_1_t121456927;
// Utage.CharData
struct CharData_t4178071270;
// UnityEngine.UIVertex[]
struct UIVertexU5BU5D_t3048644023;
// System.Collections.Generic.List`1<UnityEngine.Sprite>
struct List_1_t3973682211;
// System.Collections.Generic.Dictionary`2<System.Char,UnityEngine.Sprite>
struct Dictionary_2_t1910071072;
// System.Collections.Generic.Dictionary`2<System.String,UnityEngine.Sprite>
struct Dictionary_2_t2224373045;
// Utage.TextData
struct TextData_t603454315;
// Utage.UguiNovelTextSettings
struct UguiNovelTextSettings_t4201484304;
// Utage.UguiNovelTextEmojiData
struct UguiNovelTextEmojiData_t3935078235;
// System.Collections.Generic.List`1<Utage.UguiNovelTextLine>
struct List_1_t1808884665;
// Utage.UguiNovelTextGeneratorAdditional
struct UguiNovelTextGeneratorAdditional_t151164365;
// System.Collections.Generic.List`1<Utage.UguiNovelTextGenerator/GraphicObject>
struct List_1_t502578610;
// System.Collections.Generic.List`1<Utage.UguiNovelTextHitArea>
struct List_1_t948532665;
// Utage.UguiNovelTextGenerator
struct UguiNovelTextGenerator_t4216084310;
// Utage.OnClickLinkEvent
struct OnClickLinkEvent_t134619553;
// Utage.UguiNovelTextHitArea
struct UguiNovelTextHitArea_t1579411533;
// Utage.DicingImage
struct DicingImage_t2721298607;
// Utage.MinMaxFloat
struct MinMaxFloat_t1425080750;
// Utage.MiniAnimationData
struct MiniAnimationData_t521227391;
// Utage.UguiBackgroundRaycaster
struct UguiBackgroundRaycaster_t588614668;
// UnityEngine.UI.Selectable
struct Selectable_t1490392188;
// UnityEngine.AudioClip
struct AudioClip_t1932558630;
// System.Collections.Generic.List`1<UnityEngine.UI.Toggle>
struct List_1_t3345875600;
// UnityEngine.UI.Button
struct Button_t2872111280;
// Utage.UguiToggleGroupIndexed/UguiTabButtonGroupEvent
struct UguiTabButtonGroupEvent_t4293552753;
// Utage.UguiGridPage
struct UguiGridPage_t1812803607;
// Utage.UguiAlignGroup
struct UguiAlignGroup_t4030345508;
// UnityEngine.GameObject
struct GameObject_t1756533147;
// System.String[]
struct StringU5BU5D_t1642385972;
// UnityEngine.UI.ScrollRect
struct ScrollRect_t1199013257;
// System.Collections.Generic.List`1<UnityEngine.GameObject>
struct List_1_t1125654279;
// UnityEngine.UI.GridLayoutGroup
struct GridLayoutGroup_t1515633077;
// System.Action`2<UnityEngine.GameObject,System.Int32>
struct Action_2_t1397798026;
// UnityEngine.Events.UnityEvent
struct UnityEvent_t408735097;
// Utage.LipSynchEvent
struct LipSynchEvent_t825925384;
// UnityEngine.Material
struct Material_t193706927;
// UnityEngine.Texture2D
struct Texture2D_t3542995729;
// UnityEngine.CanvasRenderer
struct CanvasRenderer_t261436805;
// UnityEngine.Canvas
struct Canvas_t209405766;
// UnityEngine.Events.UnityAction
struct UnityAction_t4025899511;
// UnityEngine.Mesh
struct Mesh_t1356156583;
// UnityEngine.UI.CoroutineTween.TweenRunner`1<UnityEngine.UI.CoroutineTween.ColorTween>
struct TweenRunner_1_t3177091249;
// System.Collections.Generic.List`1<Utage.UguiLocalizeTextSetting/Setting>
struct List_1_t2913322537;
// Utage.UguiLocalizeTextSetting/Setting
struct Setting_t3544201405;
// UnityEngine.UI.Text
struct Text_t356221433;
// UnityEngine.Coroutine
struct Coroutine_t2299508840;
// Utage.LetterBoxCamera
struct LetterBoxCamera_t3507617684;
// System.Collections.Generic.List`1<Utage.UguiLocalizeRectTransform/Setting>
struct List_1_t1739457176;
// Utage.UguiLocalizeRectTransform/Setting
struct Setting_t2370336044;
// UnityEngine.Camera
struct Camera_t189460977;
// Utage.AvatarImage
struct AvatarImage_t1946614104;
// UnityEngine.UI.RectMask2D
struct RectMask2D_t1156185964;
// UnityEngine.UI.MaskableGraphic/CullStateChangedEvent
struct CullStateChangedEvent_t3778758259;
// UnityEngine.Vector3[]
struct Vector3U5BU5D_t1172311765;
// Utage.DicingTextures
struct DicingTextures_t1684855170;
// System.Collections.Generic.Dictionary`2<System.String,System.String>
struct Dictionary_2_t3943999495;
// UnityEngine.UI.FontData
struct FontData_t2614388407;
// UnityEngine.TextGenerator
struct TextGenerator_t647235000;




#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
#ifndef EXTENSIONUTIL_T1676251555_H
#define EXTENSIONUTIL_T1676251555_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.ExtensionUtil
struct  ExtensionUtil_t1676251555  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EXTENSIONUTIL_T1676251555_H
#ifndef U3CADDU3EC__ANONSTOREY2_T2433847059_H
#define U3CADDU3EC__ANONSTOREY2_T2433847059_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.UguiToggleGroupIndexed/<Add>c__AnonStorey2
struct  U3CAddU3Ec__AnonStorey2_t2433847059  : public RuntimeObject
{
public:
	// UnityEngine.UI.Toggle Utage.UguiToggleGroupIndexed/<Add>c__AnonStorey2::toggle
	Toggle_t3976754468 * ___toggle_0;
	// Utage.UguiToggleGroupIndexed Utage.UguiToggleGroupIndexed/<Add>c__AnonStorey2::$this
	UguiToggleGroupIndexed_t999166452 * ___U24this_1;

public:
	inline static int32_t get_offset_of_toggle_0() { return static_cast<int32_t>(offsetof(U3CAddU3Ec__AnonStorey2_t2433847059, ___toggle_0)); }
	inline Toggle_t3976754468 * get_toggle_0() const { return ___toggle_0; }
	inline Toggle_t3976754468 ** get_address_of_toggle_0() { return &___toggle_0; }
	inline void set_toggle_0(Toggle_t3976754468 * value)
	{
		___toggle_0 = value;
		Il2CppCodeGenWriteBarrier((&___toggle_0), value);
	}

	inline static int32_t get_offset_of_U24this_1() { return static_cast<int32_t>(offsetof(U3CAddU3Ec__AnonStorey2_t2433847059, ___U24this_1)); }
	inline UguiToggleGroupIndexed_t999166452 * get_U24this_1() const { return ___U24this_1; }
	inline UguiToggleGroupIndexed_t999166452 ** get_address_of_U24this_1() { return &___U24this_1; }
	inline void set_U24this_1(UguiToggleGroupIndexed_t999166452 * value)
	{
		___U24this_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CADDU3EC__ANONSTOREY2_T2433847059_H
#ifndef U3CCOOPENINGU3EC__ITERATOR0_T101420245_H
#define U3CCOOPENINGU3EC__ITERATOR0_T101420245_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.UguiView/<CoOpening>c__Iterator0
struct  U3CCoOpeningU3Ec__Iterator0_t101420245  : public RuntimeObject
{
public:
	// Utage.ITransition[] Utage.UguiView/<CoOpening>c__Iterator0::<transitions>__0
	ITransitionU5BU5D_t1588618539* ___U3CtransitionsU3E__0_0;
	// Utage.ITransition[] Utage.UguiView/<CoOpening>c__Iterator0::$locvar0
	ITransitionU5BU5D_t1588618539* ___U24locvar0_1;
	// System.Int32 Utage.UguiView/<CoOpening>c__Iterator0::$locvar1
	int32_t ___U24locvar1_2;
	// Utage.UguiView Utage.UguiView/<CoOpening>c__Iterator0::$this
	UguiView_t2174908005 * ___U24this_3;
	// System.Object Utage.UguiView/<CoOpening>c__Iterator0::$current
	RuntimeObject * ___U24current_4;
	// System.Boolean Utage.UguiView/<CoOpening>c__Iterator0::$disposing
	bool ___U24disposing_5;
	// System.Int32 Utage.UguiView/<CoOpening>c__Iterator0::$PC
	int32_t ___U24PC_6;

public:
	inline static int32_t get_offset_of_U3CtransitionsU3E__0_0() { return static_cast<int32_t>(offsetof(U3CCoOpeningU3Ec__Iterator0_t101420245, ___U3CtransitionsU3E__0_0)); }
	inline ITransitionU5BU5D_t1588618539* get_U3CtransitionsU3E__0_0() const { return ___U3CtransitionsU3E__0_0; }
	inline ITransitionU5BU5D_t1588618539** get_address_of_U3CtransitionsU3E__0_0() { return &___U3CtransitionsU3E__0_0; }
	inline void set_U3CtransitionsU3E__0_0(ITransitionU5BU5D_t1588618539* value)
	{
		___U3CtransitionsU3E__0_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CtransitionsU3E__0_0), value);
	}

	inline static int32_t get_offset_of_U24locvar0_1() { return static_cast<int32_t>(offsetof(U3CCoOpeningU3Ec__Iterator0_t101420245, ___U24locvar0_1)); }
	inline ITransitionU5BU5D_t1588618539* get_U24locvar0_1() const { return ___U24locvar0_1; }
	inline ITransitionU5BU5D_t1588618539** get_address_of_U24locvar0_1() { return &___U24locvar0_1; }
	inline void set_U24locvar0_1(ITransitionU5BU5D_t1588618539* value)
	{
		___U24locvar0_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24locvar0_1), value);
	}

	inline static int32_t get_offset_of_U24locvar1_2() { return static_cast<int32_t>(offsetof(U3CCoOpeningU3Ec__Iterator0_t101420245, ___U24locvar1_2)); }
	inline int32_t get_U24locvar1_2() const { return ___U24locvar1_2; }
	inline int32_t* get_address_of_U24locvar1_2() { return &___U24locvar1_2; }
	inline void set_U24locvar1_2(int32_t value)
	{
		___U24locvar1_2 = value;
	}

	inline static int32_t get_offset_of_U24this_3() { return static_cast<int32_t>(offsetof(U3CCoOpeningU3Ec__Iterator0_t101420245, ___U24this_3)); }
	inline UguiView_t2174908005 * get_U24this_3() const { return ___U24this_3; }
	inline UguiView_t2174908005 ** get_address_of_U24this_3() { return &___U24this_3; }
	inline void set_U24this_3(UguiView_t2174908005 * value)
	{
		___U24this_3 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_3), value);
	}

	inline static int32_t get_offset_of_U24current_4() { return static_cast<int32_t>(offsetof(U3CCoOpeningU3Ec__Iterator0_t101420245, ___U24current_4)); }
	inline RuntimeObject * get_U24current_4() const { return ___U24current_4; }
	inline RuntimeObject ** get_address_of_U24current_4() { return &___U24current_4; }
	inline void set_U24current_4(RuntimeObject * value)
	{
		___U24current_4 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_4), value);
	}

	inline static int32_t get_offset_of_U24disposing_5() { return static_cast<int32_t>(offsetof(U3CCoOpeningU3Ec__Iterator0_t101420245, ___U24disposing_5)); }
	inline bool get_U24disposing_5() const { return ___U24disposing_5; }
	inline bool* get_address_of_U24disposing_5() { return &___U24disposing_5; }
	inline void set_U24disposing_5(bool value)
	{
		___U24disposing_5 = value;
	}

	inline static int32_t get_offset_of_U24PC_6() { return static_cast<int32_t>(offsetof(U3CCoOpeningU3Ec__Iterator0_t101420245, ___U24PC_6)); }
	inline int32_t get_U24PC_6() const { return ___U24PC_6; }
	inline int32_t* get_address_of_U24PC_6() { return &___U24PC_6; }
	inline void set_U24PC_6(int32_t value)
	{
		___U24PC_6 = value;
	}
};

struct U3CCoOpeningU3Ec__Iterator0_t101420245_StaticFields
{
public:
	// System.Predicate`1<Utage.ITransition> Utage.UguiView/<CoOpening>c__Iterator0::<>f__am$cache0
	Predicate_1_t3407471265 * ___U3CU3Ef__amU24cache0_7;

public:
	inline static int32_t get_offset_of_U3CU3Ef__amU24cache0_7() { return static_cast<int32_t>(offsetof(U3CCoOpeningU3Ec__Iterator0_t101420245_StaticFields, ___U3CU3Ef__amU24cache0_7)); }
	inline Predicate_1_t3407471265 * get_U3CU3Ef__amU24cache0_7() const { return ___U3CU3Ef__amU24cache0_7; }
	inline Predicate_1_t3407471265 ** get_address_of_U3CU3Ef__amU24cache0_7() { return &___U3CU3Ef__amU24cache0_7; }
	inline void set_U3CU3Ef__amU24cache0_7(Predicate_1_t3407471265 * value)
	{
		___U3CU3Ef__amU24cache0_7 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache0_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CCOOPENINGU3EC__ITERATOR0_T101420245_H
#ifndef U3CCOCLOSINGU3EC__ITERATOR1_T587244119_H
#define U3CCOCLOSINGU3EC__ITERATOR1_T587244119_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.UguiView/<CoClosing>c__Iterator1
struct  U3CCoClosingU3Ec__Iterator1_t587244119  : public RuntimeObject
{
public:
	// Utage.ITransition[] Utage.UguiView/<CoClosing>c__Iterator1::<transitions>__0
	ITransitionU5BU5D_t1588618539* ___U3CtransitionsU3E__0_0;
	// Utage.ITransition[] Utage.UguiView/<CoClosing>c__Iterator1::$locvar0
	ITransitionU5BU5D_t1588618539* ___U24locvar0_1;
	// System.Int32 Utage.UguiView/<CoClosing>c__Iterator1::$locvar1
	int32_t ___U24locvar1_2;
	// Utage.UguiView Utage.UguiView/<CoClosing>c__Iterator1::$this
	UguiView_t2174908005 * ___U24this_3;
	// System.Object Utage.UguiView/<CoClosing>c__Iterator1::$current
	RuntimeObject * ___U24current_4;
	// System.Boolean Utage.UguiView/<CoClosing>c__Iterator1::$disposing
	bool ___U24disposing_5;
	// System.Int32 Utage.UguiView/<CoClosing>c__Iterator1::$PC
	int32_t ___U24PC_6;

public:
	inline static int32_t get_offset_of_U3CtransitionsU3E__0_0() { return static_cast<int32_t>(offsetof(U3CCoClosingU3Ec__Iterator1_t587244119, ___U3CtransitionsU3E__0_0)); }
	inline ITransitionU5BU5D_t1588618539* get_U3CtransitionsU3E__0_0() const { return ___U3CtransitionsU3E__0_0; }
	inline ITransitionU5BU5D_t1588618539** get_address_of_U3CtransitionsU3E__0_0() { return &___U3CtransitionsU3E__0_0; }
	inline void set_U3CtransitionsU3E__0_0(ITransitionU5BU5D_t1588618539* value)
	{
		___U3CtransitionsU3E__0_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CtransitionsU3E__0_0), value);
	}

	inline static int32_t get_offset_of_U24locvar0_1() { return static_cast<int32_t>(offsetof(U3CCoClosingU3Ec__Iterator1_t587244119, ___U24locvar0_1)); }
	inline ITransitionU5BU5D_t1588618539* get_U24locvar0_1() const { return ___U24locvar0_1; }
	inline ITransitionU5BU5D_t1588618539** get_address_of_U24locvar0_1() { return &___U24locvar0_1; }
	inline void set_U24locvar0_1(ITransitionU5BU5D_t1588618539* value)
	{
		___U24locvar0_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24locvar0_1), value);
	}

	inline static int32_t get_offset_of_U24locvar1_2() { return static_cast<int32_t>(offsetof(U3CCoClosingU3Ec__Iterator1_t587244119, ___U24locvar1_2)); }
	inline int32_t get_U24locvar1_2() const { return ___U24locvar1_2; }
	inline int32_t* get_address_of_U24locvar1_2() { return &___U24locvar1_2; }
	inline void set_U24locvar1_2(int32_t value)
	{
		___U24locvar1_2 = value;
	}

	inline static int32_t get_offset_of_U24this_3() { return static_cast<int32_t>(offsetof(U3CCoClosingU3Ec__Iterator1_t587244119, ___U24this_3)); }
	inline UguiView_t2174908005 * get_U24this_3() const { return ___U24this_3; }
	inline UguiView_t2174908005 ** get_address_of_U24this_3() { return &___U24this_3; }
	inline void set_U24this_3(UguiView_t2174908005 * value)
	{
		___U24this_3 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_3), value);
	}

	inline static int32_t get_offset_of_U24current_4() { return static_cast<int32_t>(offsetof(U3CCoClosingU3Ec__Iterator1_t587244119, ___U24current_4)); }
	inline RuntimeObject * get_U24current_4() const { return ___U24current_4; }
	inline RuntimeObject ** get_address_of_U24current_4() { return &___U24current_4; }
	inline void set_U24current_4(RuntimeObject * value)
	{
		___U24current_4 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_4), value);
	}

	inline static int32_t get_offset_of_U24disposing_5() { return static_cast<int32_t>(offsetof(U3CCoClosingU3Ec__Iterator1_t587244119, ___U24disposing_5)); }
	inline bool get_U24disposing_5() const { return ___U24disposing_5; }
	inline bool* get_address_of_U24disposing_5() { return &___U24disposing_5; }
	inline void set_U24disposing_5(bool value)
	{
		___U24disposing_5 = value;
	}

	inline static int32_t get_offset_of_U24PC_6() { return static_cast<int32_t>(offsetof(U3CCoClosingU3Ec__Iterator1_t587244119, ___U24PC_6)); }
	inline int32_t get_U24PC_6() const { return ___U24PC_6; }
	inline int32_t* get_address_of_U24PC_6() { return &___U24PC_6; }
	inline void set_U24PC_6(int32_t value)
	{
		___U24PC_6 = value;
	}
};

struct U3CCoClosingU3Ec__Iterator1_t587244119_StaticFields
{
public:
	// System.Predicate`1<Utage.ITransition> Utage.UguiView/<CoClosing>c__Iterator1::<>f__am$cache0
	Predicate_1_t3407471265 * ___U3CU3Ef__amU24cache0_7;

public:
	inline static int32_t get_offset_of_U3CU3Ef__amU24cache0_7() { return static_cast<int32_t>(offsetof(U3CCoClosingU3Ec__Iterator1_t587244119_StaticFields, ___U3CU3Ef__amU24cache0_7)); }
	inline Predicate_1_t3407471265 * get_U3CU3Ef__amU24cache0_7() const { return ___U3CU3Ef__amU24cache0_7; }
	inline Predicate_1_t3407471265 ** get_address_of_U3CU3Ef__amU24cache0_7() { return &___U3CU3Ef__amU24cache0_7; }
	inline void set_U3CU3Ef__amU24cache0_7(Predicate_1_t3407471265 * value)
	{
		___U3CU3Ef__amU24cache0_7 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache0_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CCOCLOSINGU3EC__ITERATOR1_T587244119_H
#ifndef U3CCOOPENU3EC__ITERATOR0_T4185887894_H
#define U3CCOOPENU3EC__ITERATOR0_T4185887894_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.UguiViewTransitionCrossFade/<CoOpen>c__Iterator0
struct  U3CCoOpenU3Ec__Iterator0_t4185887894  : public RuntimeObject
{
public:
	// UnityEngine.CanvasGroup Utage.UguiViewTransitionCrossFade/<CoOpen>c__Iterator0::<canvasGroup>__0
	CanvasGroup_t3296560743 * ___U3CcanvasGroupU3E__0_0;
	// System.Single Utage.UguiViewTransitionCrossFade/<CoOpen>c__Iterator0::<currentTime>__0
	float ___U3CcurrentTimeU3E__0_1;
	// Utage.UguiViewTransitionCrossFade Utage.UguiViewTransitionCrossFade/<CoOpen>c__Iterator0::$this
	UguiViewTransitionCrossFade_t1724059380 * ___U24this_2;
	// System.Object Utage.UguiViewTransitionCrossFade/<CoOpen>c__Iterator0::$current
	RuntimeObject * ___U24current_3;
	// System.Boolean Utage.UguiViewTransitionCrossFade/<CoOpen>c__Iterator0::$disposing
	bool ___U24disposing_4;
	// System.Int32 Utage.UguiViewTransitionCrossFade/<CoOpen>c__Iterator0::$PC
	int32_t ___U24PC_5;

public:
	inline static int32_t get_offset_of_U3CcanvasGroupU3E__0_0() { return static_cast<int32_t>(offsetof(U3CCoOpenU3Ec__Iterator0_t4185887894, ___U3CcanvasGroupU3E__0_0)); }
	inline CanvasGroup_t3296560743 * get_U3CcanvasGroupU3E__0_0() const { return ___U3CcanvasGroupU3E__0_0; }
	inline CanvasGroup_t3296560743 ** get_address_of_U3CcanvasGroupU3E__0_0() { return &___U3CcanvasGroupU3E__0_0; }
	inline void set_U3CcanvasGroupU3E__0_0(CanvasGroup_t3296560743 * value)
	{
		___U3CcanvasGroupU3E__0_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CcanvasGroupU3E__0_0), value);
	}

	inline static int32_t get_offset_of_U3CcurrentTimeU3E__0_1() { return static_cast<int32_t>(offsetof(U3CCoOpenU3Ec__Iterator0_t4185887894, ___U3CcurrentTimeU3E__0_1)); }
	inline float get_U3CcurrentTimeU3E__0_1() const { return ___U3CcurrentTimeU3E__0_1; }
	inline float* get_address_of_U3CcurrentTimeU3E__0_1() { return &___U3CcurrentTimeU3E__0_1; }
	inline void set_U3CcurrentTimeU3E__0_1(float value)
	{
		___U3CcurrentTimeU3E__0_1 = value;
	}

	inline static int32_t get_offset_of_U24this_2() { return static_cast<int32_t>(offsetof(U3CCoOpenU3Ec__Iterator0_t4185887894, ___U24this_2)); }
	inline UguiViewTransitionCrossFade_t1724059380 * get_U24this_2() const { return ___U24this_2; }
	inline UguiViewTransitionCrossFade_t1724059380 ** get_address_of_U24this_2() { return &___U24this_2; }
	inline void set_U24this_2(UguiViewTransitionCrossFade_t1724059380 * value)
	{
		___U24this_2 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_2), value);
	}

	inline static int32_t get_offset_of_U24current_3() { return static_cast<int32_t>(offsetof(U3CCoOpenU3Ec__Iterator0_t4185887894, ___U24current_3)); }
	inline RuntimeObject * get_U24current_3() const { return ___U24current_3; }
	inline RuntimeObject ** get_address_of_U24current_3() { return &___U24current_3; }
	inline void set_U24current_3(RuntimeObject * value)
	{
		___U24current_3 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_3), value);
	}

	inline static int32_t get_offset_of_U24disposing_4() { return static_cast<int32_t>(offsetof(U3CCoOpenU3Ec__Iterator0_t4185887894, ___U24disposing_4)); }
	inline bool get_U24disposing_4() const { return ___U24disposing_4; }
	inline bool* get_address_of_U24disposing_4() { return &___U24disposing_4; }
	inline void set_U24disposing_4(bool value)
	{
		___U24disposing_4 = value;
	}

	inline static int32_t get_offset_of_U24PC_5() { return static_cast<int32_t>(offsetof(U3CCoOpenU3Ec__Iterator0_t4185887894, ___U24PC_5)); }
	inline int32_t get_U24PC_5() const { return ___U24PC_5; }
	inline int32_t* get_address_of_U24PC_5() { return &___U24PC_5; }
	inline void set_U24PC_5(int32_t value)
	{
		___U24PC_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CCOOPENU3EC__ITERATOR0_T4185887894_H
#ifndef U3CCOCLOSEU3EC__ITERATOR1_T1558610921_H
#define U3CCOCLOSEU3EC__ITERATOR1_T1558610921_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.UguiViewTransitionCrossFade/<CoClose>c__Iterator1
struct  U3CCoCloseU3Ec__Iterator1_t1558610921  : public RuntimeObject
{
public:
	// UnityEngine.CanvasGroup Utage.UguiViewTransitionCrossFade/<CoClose>c__Iterator1::<canvasGroup>__0
	CanvasGroup_t3296560743 * ___U3CcanvasGroupU3E__0_0;
	// System.Single Utage.UguiViewTransitionCrossFade/<CoClose>c__Iterator1::<currentTime>__0
	float ___U3CcurrentTimeU3E__0_1;
	// Utage.UguiViewTransitionCrossFade Utage.UguiViewTransitionCrossFade/<CoClose>c__Iterator1::$this
	UguiViewTransitionCrossFade_t1724059380 * ___U24this_2;
	// System.Object Utage.UguiViewTransitionCrossFade/<CoClose>c__Iterator1::$current
	RuntimeObject * ___U24current_3;
	// System.Boolean Utage.UguiViewTransitionCrossFade/<CoClose>c__Iterator1::$disposing
	bool ___U24disposing_4;
	// System.Int32 Utage.UguiViewTransitionCrossFade/<CoClose>c__Iterator1::$PC
	int32_t ___U24PC_5;

public:
	inline static int32_t get_offset_of_U3CcanvasGroupU3E__0_0() { return static_cast<int32_t>(offsetof(U3CCoCloseU3Ec__Iterator1_t1558610921, ___U3CcanvasGroupU3E__0_0)); }
	inline CanvasGroup_t3296560743 * get_U3CcanvasGroupU3E__0_0() const { return ___U3CcanvasGroupU3E__0_0; }
	inline CanvasGroup_t3296560743 ** get_address_of_U3CcanvasGroupU3E__0_0() { return &___U3CcanvasGroupU3E__0_0; }
	inline void set_U3CcanvasGroupU3E__0_0(CanvasGroup_t3296560743 * value)
	{
		___U3CcanvasGroupU3E__0_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CcanvasGroupU3E__0_0), value);
	}

	inline static int32_t get_offset_of_U3CcurrentTimeU3E__0_1() { return static_cast<int32_t>(offsetof(U3CCoCloseU3Ec__Iterator1_t1558610921, ___U3CcurrentTimeU3E__0_1)); }
	inline float get_U3CcurrentTimeU3E__0_1() const { return ___U3CcurrentTimeU3E__0_1; }
	inline float* get_address_of_U3CcurrentTimeU3E__0_1() { return &___U3CcurrentTimeU3E__0_1; }
	inline void set_U3CcurrentTimeU3E__0_1(float value)
	{
		___U3CcurrentTimeU3E__0_1 = value;
	}

	inline static int32_t get_offset_of_U24this_2() { return static_cast<int32_t>(offsetof(U3CCoCloseU3Ec__Iterator1_t1558610921, ___U24this_2)); }
	inline UguiViewTransitionCrossFade_t1724059380 * get_U24this_2() const { return ___U24this_2; }
	inline UguiViewTransitionCrossFade_t1724059380 ** get_address_of_U24this_2() { return &___U24this_2; }
	inline void set_U24this_2(UguiViewTransitionCrossFade_t1724059380 * value)
	{
		___U24this_2 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_2), value);
	}

	inline static int32_t get_offset_of_U24current_3() { return static_cast<int32_t>(offsetof(U3CCoCloseU3Ec__Iterator1_t1558610921, ___U24current_3)); }
	inline RuntimeObject * get_U24current_3() const { return ___U24current_3; }
	inline RuntimeObject ** get_address_of_U24current_3() { return &___U24current_3; }
	inline void set_U24current_3(RuntimeObject * value)
	{
		___U24current_3 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_3), value);
	}

	inline static int32_t get_offset_of_U24disposing_4() { return static_cast<int32_t>(offsetof(U3CCoCloseU3Ec__Iterator1_t1558610921, ___U24disposing_4)); }
	inline bool get_U24disposing_4() const { return ___U24disposing_4; }
	inline bool* get_address_of_U24disposing_4() { return &___U24disposing_4; }
	inline void set_U24disposing_4(bool value)
	{
		___U24disposing_4 = value;
	}

	inline static int32_t get_offset_of_U24PC_5() { return static_cast<int32_t>(offsetof(U3CCoCloseU3Ec__Iterator1_t1558610921, ___U24PC_5)); }
	inline int32_t get_U24PC_5() const { return ___U24PC_5; }
	inline int32_t* get_address_of_U24PC_5() { return &___U24PC_5; }
	inline void set_U24PC_5(int32_t value)
	{
		___U24PC_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CCOCLOSEU3EC__ITERATOR1_T1558610921_H
#ifndef ALIGNMENTUTIL_T2144597389_H
#define ALIGNMENTUTIL_T2144597389_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.AlignmentUtil
struct  AlignmentUtil_t2144597389  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ALIGNMENTUTIL_T2144597389_H
#ifndef U3CCODOUBLEEYEBLINKU3EC__ITERATOR1_T2312577335_H
#define U3CCODOUBLEEYEBLINKU3EC__ITERATOR1_T2312577335_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.EyeBlinkBase/<CoDoubleEyeBlink>c__Iterator1
struct  U3CCoDoubleEyeBlinkU3Ec__Iterator1_t2312577335  : public RuntimeObject
{
public:
	// Utage.EyeBlinkBase Utage.EyeBlinkBase/<CoDoubleEyeBlink>c__Iterator1::$this
	EyeBlinkBase_t1662968566 * ___U24this_0;
	// System.Object Utage.EyeBlinkBase/<CoDoubleEyeBlink>c__Iterator1::$current
	RuntimeObject * ___U24current_1;
	// System.Boolean Utage.EyeBlinkBase/<CoDoubleEyeBlink>c__Iterator1::$disposing
	bool ___U24disposing_2;
	// System.Int32 Utage.EyeBlinkBase/<CoDoubleEyeBlink>c__Iterator1::$PC
	int32_t ___U24PC_3;

public:
	inline static int32_t get_offset_of_U24this_0() { return static_cast<int32_t>(offsetof(U3CCoDoubleEyeBlinkU3Ec__Iterator1_t2312577335, ___U24this_0)); }
	inline EyeBlinkBase_t1662968566 * get_U24this_0() const { return ___U24this_0; }
	inline EyeBlinkBase_t1662968566 ** get_address_of_U24this_0() { return &___U24this_0; }
	inline void set_U24this_0(EyeBlinkBase_t1662968566 * value)
	{
		___U24this_0 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_0), value);
	}

	inline static int32_t get_offset_of_U24current_1() { return static_cast<int32_t>(offsetof(U3CCoDoubleEyeBlinkU3Ec__Iterator1_t2312577335, ___U24current_1)); }
	inline RuntimeObject * get_U24current_1() const { return ___U24current_1; }
	inline RuntimeObject ** get_address_of_U24current_1() { return &___U24current_1; }
	inline void set_U24current_1(RuntimeObject * value)
	{
		___U24current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_1), value);
	}

	inline static int32_t get_offset_of_U24disposing_2() { return static_cast<int32_t>(offsetof(U3CCoDoubleEyeBlinkU3Ec__Iterator1_t2312577335, ___U24disposing_2)); }
	inline bool get_U24disposing_2() const { return ___U24disposing_2; }
	inline bool* get_address_of_U24disposing_2() { return &___U24disposing_2; }
	inline void set_U24disposing_2(bool value)
	{
		___U24disposing_2 = value;
	}

	inline static int32_t get_offset_of_U24PC_3() { return static_cast<int32_t>(offsetof(U3CCoDoubleEyeBlinkU3Ec__Iterator1_t2312577335, ___U24PC_3)); }
	inline int32_t get_U24PC_3() const { return ___U24PC_3; }
	inline int32_t* get_address_of_U24PC_3() { return &___U24PC_3; }
	inline void set_U24PC_3(int32_t value)
	{
		___U24PC_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CCODOUBLEEYEBLINKU3EC__ITERATOR1_T2312577335_H
#ifndef U3CCOUPATEWAITINGU3EC__ITERATOR0_T2118272722_H
#define U3CCOUPATEWAITINGU3EC__ITERATOR0_T2118272722_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.EyeBlinkBase/<CoUpateWaiting>c__Iterator0
struct  U3CCoUpateWaitingU3Ec__Iterator0_t2118272722  : public RuntimeObject
{
public:
	// System.Single Utage.EyeBlinkBase/<CoUpateWaiting>c__Iterator0::waitTime
	float ___waitTime_0;
	// Utage.EyeBlinkBase Utage.EyeBlinkBase/<CoUpateWaiting>c__Iterator0::$this
	EyeBlinkBase_t1662968566 * ___U24this_1;
	// System.Object Utage.EyeBlinkBase/<CoUpateWaiting>c__Iterator0::$current
	RuntimeObject * ___U24current_2;
	// System.Boolean Utage.EyeBlinkBase/<CoUpateWaiting>c__Iterator0::$disposing
	bool ___U24disposing_3;
	// System.Int32 Utage.EyeBlinkBase/<CoUpateWaiting>c__Iterator0::$PC
	int32_t ___U24PC_4;

public:
	inline static int32_t get_offset_of_waitTime_0() { return static_cast<int32_t>(offsetof(U3CCoUpateWaitingU3Ec__Iterator0_t2118272722, ___waitTime_0)); }
	inline float get_waitTime_0() const { return ___waitTime_0; }
	inline float* get_address_of_waitTime_0() { return &___waitTime_0; }
	inline void set_waitTime_0(float value)
	{
		___waitTime_0 = value;
	}

	inline static int32_t get_offset_of_U24this_1() { return static_cast<int32_t>(offsetof(U3CCoUpateWaitingU3Ec__Iterator0_t2118272722, ___U24this_1)); }
	inline EyeBlinkBase_t1662968566 * get_U24this_1() const { return ___U24this_1; }
	inline EyeBlinkBase_t1662968566 ** get_address_of_U24this_1() { return &___U24this_1; }
	inline void set_U24this_1(EyeBlinkBase_t1662968566 * value)
	{
		___U24this_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_1), value);
	}

	inline static int32_t get_offset_of_U24current_2() { return static_cast<int32_t>(offsetof(U3CCoUpateWaitingU3Ec__Iterator0_t2118272722, ___U24current_2)); }
	inline RuntimeObject * get_U24current_2() const { return ___U24current_2; }
	inline RuntimeObject ** get_address_of_U24current_2() { return &___U24current_2; }
	inline void set_U24current_2(RuntimeObject * value)
	{
		___U24current_2 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_2), value);
	}

	inline static int32_t get_offset_of_U24disposing_3() { return static_cast<int32_t>(offsetof(U3CCoUpateWaitingU3Ec__Iterator0_t2118272722, ___U24disposing_3)); }
	inline bool get_U24disposing_3() const { return ___U24disposing_3; }
	inline bool* get_address_of_U24disposing_3() { return &___U24disposing_3; }
	inline void set_U24disposing_3(bool value)
	{
		___U24disposing_3 = value;
	}

	inline static int32_t get_offset_of_U24PC_4() { return static_cast<int32_t>(offsetof(U3CCoUpateWaitingU3Ec__Iterator0_t2118272722, ___U24PC_4)); }
	inline int32_t get_U24PC_4() const { return ___U24PC_4; }
	inline int32_t* get_address_of_U24PC_4() { return &___U24PC_4; }
	inline void set_U24PC_4(int32_t value)
	{
		___U24PC_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CCOUPATEWAITINGU3EC__ITERATOR0_T2118272722_H
#ifndef UNITYEVENTBASE_T828812576_H
#define UNITYEVENTBASE_T828812576_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Events.UnityEventBase
struct  UnityEventBase_t828812576  : public RuntimeObject
{
public:
	// UnityEngine.Events.InvokableCallList UnityEngine.Events.UnityEventBase::m_Calls
	InvokableCallList_t2295673753 * ___m_Calls_0;
	// UnityEngine.Events.PersistentCallGroup UnityEngine.Events.UnityEventBase::m_PersistentCalls
	PersistentCallGroup_t339478082 * ___m_PersistentCalls_1;
	// System.String UnityEngine.Events.UnityEventBase::m_TypeName
	String_t* ___m_TypeName_2;
	// System.Boolean UnityEngine.Events.UnityEventBase::m_CallsDirty
	bool ___m_CallsDirty_3;

public:
	inline static int32_t get_offset_of_m_Calls_0() { return static_cast<int32_t>(offsetof(UnityEventBase_t828812576, ___m_Calls_0)); }
	inline InvokableCallList_t2295673753 * get_m_Calls_0() const { return ___m_Calls_0; }
	inline InvokableCallList_t2295673753 ** get_address_of_m_Calls_0() { return &___m_Calls_0; }
	inline void set_m_Calls_0(InvokableCallList_t2295673753 * value)
	{
		___m_Calls_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_Calls_0), value);
	}

	inline static int32_t get_offset_of_m_PersistentCalls_1() { return static_cast<int32_t>(offsetof(UnityEventBase_t828812576, ___m_PersistentCalls_1)); }
	inline PersistentCallGroup_t339478082 * get_m_PersistentCalls_1() const { return ___m_PersistentCalls_1; }
	inline PersistentCallGroup_t339478082 ** get_address_of_m_PersistentCalls_1() { return &___m_PersistentCalls_1; }
	inline void set_m_PersistentCalls_1(PersistentCallGroup_t339478082 * value)
	{
		___m_PersistentCalls_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_PersistentCalls_1), value);
	}

	inline static int32_t get_offset_of_m_TypeName_2() { return static_cast<int32_t>(offsetof(UnityEventBase_t828812576, ___m_TypeName_2)); }
	inline String_t* get_m_TypeName_2() const { return ___m_TypeName_2; }
	inline String_t** get_address_of_m_TypeName_2() { return &___m_TypeName_2; }
	inline void set_m_TypeName_2(String_t* value)
	{
		___m_TypeName_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_TypeName_2), value);
	}

	inline static int32_t get_offset_of_m_CallsDirty_3() { return static_cast<int32_t>(offsetof(UnityEventBase_t828812576, ___m_CallsDirty_3)); }
	inline bool get_m_CallsDirty_3() const { return ___m_CallsDirty_3; }
	inline bool* get_address_of_m_CallsDirty_3() { return &___m_CallsDirty_3; }
	inline void set_m_CallsDirty_3(bool value)
	{
		___m_CallsDirty_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYEVENTBASE_T828812576_H
#ifndef VALUETYPE_T3507792607_H
#define VALUETYPE_T3507792607_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ValueType
struct  ValueType_t3507792607  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t3507792607_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t3507792607_marshaled_com
{
};
#endif // VALUETYPE_T3507792607_H
#ifndef GRAPHICOBJECT_T1133457478_H
#define GRAPHICOBJECT_T1133457478_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.UguiNovelTextGenerator/GraphicObject
struct  GraphicObject_t1133457478  : public RuntimeObject
{
public:
	// Utage.UguiNovelTextCharacter Utage.UguiNovelTextGenerator/GraphicObject::character
	UguiNovelTextCharacter_t1244198124 * ___character_0;
	// UnityEngine.RectTransform Utage.UguiNovelTextGenerator/GraphicObject::graphic
	RectTransform_t3349966182 * ___graphic_1;

public:
	inline static int32_t get_offset_of_character_0() { return static_cast<int32_t>(offsetof(GraphicObject_t1133457478, ___character_0)); }
	inline UguiNovelTextCharacter_t1244198124 * get_character_0() const { return ___character_0; }
	inline UguiNovelTextCharacter_t1244198124 ** get_address_of_character_0() { return &___character_0; }
	inline void set_character_0(UguiNovelTextCharacter_t1244198124 * value)
	{
		___character_0 = value;
		Il2CppCodeGenWriteBarrier((&___character_0), value);
	}

	inline static int32_t get_offset_of_graphic_1() { return static_cast<int32_t>(offsetof(GraphicObject_t1133457478, ___graphic_1)); }
	inline RectTransform_t3349966182 * get_graphic_1() const { return ___graphic_1; }
	inline RectTransform_t3349966182 ** get_address_of_graphic_1() { return &___graphic_1; }
	inline void set_graphic_1(RectTransform_t3349966182 * value)
	{
		___graphic_1 = value;
		Il2CppCodeGenWriteBarrier((&___graphic_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GRAPHICOBJECT_T1133457478_H
#ifndef UGUINOVELTEXTGENERATORADDITIONAL_T151164365_H
#define UGUINOVELTEXTGENERATORADDITIONAL_T151164365_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.UguiNovelTextGeneratorAdditional
struct  UguiNovelTextGeneratorAdditional_t151164365  : public RuntimeObject
{
public:
	// System.Collections.Generic.List`1<Utage.UguiNovelTextGeneratorAdditionalRuby> Utage.UguiNovelTextGeneratorAdditional::rubyList
	List_1_t4289814645 * ___rubyList_0;
	// System.Collections.Generic.List`1<Utage.UguiNovelTextGeneratorAdditionalLine> Utage.UguiNovelTextGeneratorAdditional::lineList
	List_1_t1237941671 * ___lineList_1;

public:
	inline static int32_t get_offset_of_rubyList_0() { return static_cast<int32_t>(offsetof(UguiNovelTextGeneratorAdditional_t151164365, ___rubyList_0)); }
	inline List_1_t4289814645 * get_rubyList_0() const { return ___rubyList_0; }
	inline List_1_t4289814645 ** get_address_of_rubyList_0() { return &___rubyList_0; }
	inline void set_rubyList_0(List_1_t4289814645 * value)
	{
		___rubyList_0 = value;
		Il2CppCodeGenWriteBarrier((&___rubyList_0), value);
	}

	inline static int32_t get_offset_of_lineList_1() { return static_cast<int32_t>(offsetof(UguiNovelTextGeneratorAdditional_t151164365, ___lineList_1)); }
	inline List_1_t1237941671 * get_lineList_1() const { return ___lineList_1; }
	inline List_1_t1237941671 ** get_address_of_lineList_1() { return &___lineList_1; }
	inline void set_lineList_1(List_1_t1237941671 * value)
	{
		___lineList_1 = value;
		Il2CppCodeGenWriteBarrier((&___lineList_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UGUINOVELTEXTGENERATORADDITIONAL_T151164365_H
#ifndef UGUINOVELTEXTGENERATORADDITIONALRUBY_T625726217_H
#define UGUINOVELTEXTGENERATORADDITIONALRUBY_T625726217_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.UguiNovelTextGeneratorAdditionalRuby
struct  UguiNovelTextGeneratorAdditionalRuby_t625726217  : public RuntimeObject
{
public:
	// System.Collections.Generic.List`1<Utage.UguiNovelTextCharacter> Utage.UguiNovelTextGeneratorAdditionalRuby::rubyList
	List_1_t613319256 * ___rubyList_0;
	// System.Collections.Generic.List`1<Utage.UguiNovelTextCharacter> Utage.UguiNovelTextGeneratorAdditionalRuby::stringData
	List_1_t613319256 * ___stringData_1;
	// System.Single Utage.UguiNovelTextGeneratorAdditionalRuby::rubyWidth
	float ___rubyWidth_2;
	// System.Single Utage.UguiNovelTextGeneratorAdditionalRuby::stringWidth
	float ___stringWidth_3;
	// Utage.UguiNovelTextLine Utage.UguiNovelTextGeneratorAdditionalRuby::textLine
	UguiNovelTextLine_t2439763533 * ___textLine_4;

public:
	inline static int32_t get_offset_of_rubyList_0() { return static_cast<int32_t>(offsetof(UguiNovelTextGeneratorAdditionalRuby_t625726217, ___rubyList_0)); }
	inline List_1_t613319256 * get_rubyList_0() const { return ___rubyList_0; }
	inline List_1_t613319256 ** get_address_of_rubyList_0() { return &___rubyList_0; }
	inline void set_rubyList_0(List_1_t613319256 * value)
	{
		___rubyList_0 = value;
		Il2CppCodeGenWriteBarrier((&___rubyList_0), value);
	}

	inline static int32_t get_offset_of_stringData_1() { return static_cast<int32_t>(offsetof(UguiNovelTextGeneratorAdditionalRuby_t625726217, ___stringData_1)); }
	inline List_1_t613319256 * get_stringData_1() const { return ___stringData_1; }
	inline List_1_t613319256 ** get_address_of_stringData_1() { return &___stringData_1; }
	inline void set_stringData_1(List_1_t613319256 * value)
	{
		___stringData_1 = value;
		Il2CppCodeGenWriteBarrier((&___stringData_1), value);
	}

	inline static int32_t get_offset_of_rubyWidth_2() { return static_cast<int32_t>(offsetof(UguiNovelTextGeneratorAdditionalRuby_t625726217, ___rubyWidth_2)); }
	inline float get_rubyWidth_2() const { return ___rubyWidth_2; }
	inline float* get_address_of_rubyWidth_2() { return &___rubyWidth_2; }
	inline void set_rubyWidth_2(float value)
	{
		___rubyWidth_2 = value;
	}

	inline static int32_t get_offset_of_stringWidth_3() { return static_cast<int32_t>(offsetof(UguiNovelTextGeneratorAdditionalRuby_t625726217, ___stringWidth_3)); }
	inline float get_stringWidth_3() const { return ___stringWidth_3; }
	inline float* get_address_of_stringWidth_3() { return &___stringWidth_3; }
	inline void set_stringWidth_3(float value)
	{
		___stringWidth_3 = value;
	}

	inline static int32_t get_offset_of_textLine_4() { return static_cast<int32_t>(offsetof(UguiNovelTextGeneratorAdditionalRuby_t625726217, ___textLine_4)); }
	inline UguiNovelTextLine_t2439763533 * get_textLine_4() const { return ___textLine_4; }
	inline UguiNovelTextLine_t2439763533 ** get_address_of_textLine_4() { return &___textLine_4; }
	inline void set_textLine_4(UguiNovelTextLine_t2439763533 * value)
	{
		___textLine_4 = value;
		Il2CppCodeGenWriteBarrier((&___textLine_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UGUINOVELTEXTGENERATORADDITIONALRUBY_T625726217_H
#ifndef UGUINOVELTEXTLINE_T2439763533_H
#define UGUINOVELTEXTLINE_T2439763533_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.UguiNovelTextLine
struct  UguiNovelTextLine_t2439763533  : public RuntimeObject
{
public:
	// System.Collections.Generic.List`1<Utage.UguiNovelTextCharacter> Utage.UguiNovelTextLine::characters
	List_1_t613319256 * ___characters_0;
	// System.Collections.Generic.List`1<Utage.UguiNovelTextCharacter> Utage.UguiNovelTextLine::rubyCharacters
	List_1_t613319256 * ___rubyCharacters_1;
	// System.Boolean Utage.UguiNovelTextLine::isOver
	bool ___isOver_2;
	// System.Int32 Utage.UguiNovelTextLine::maxFontSize
	int32_t ___maxFontSize_3;
	// System.Single Utage.UguiNovelTextLine::width
	float ___width_4;
	// System.Single Utage.UguiNovelTextLine::totalHeight
	float ___totalHeight_5;
	// System.Single Utage.UguiNovelTextLine::<Y0>k__BackingField
	float ___U3CY0U3Ek__BackingField_6;

public:
	inline static int32_t get_offset_of_characters_0() { return static_cast<int32_t>(offsetof(UguiNovelTextLine_t2439763533, ___characters_0)); }
	inline List_1_t613319256 * get_characters_0() const { return ___characters_0; }
	inline List_1_t613319256 ** get_address_of_characters_0() { return &___characters_0; }
	inline void set_characters_0(List_1_t613319256 * value)
	{
		___characters_0 = value;
		Il2CppCodeGenWriteBarrier((&___characters_0), value);
	}

	inline static int32_t get_offset_of_rubyCharacters_1() { return static_cast<int32_t>(offsetof(UguiNovelTextLine_t2439763533, ___rubyCharacters_1)); }
	inline List_1_t613319256 * get_rubyCharacters_1() const { return ___rubyCharacters_1; }
	inline List_1_t613319256 ** get_address_of_rubyCharacters_1() { return &___rubyCharacters_1; }
	inline void set_rubyCharacters_1(List_1_t613319256 * value)
	{
		___rubyCharacters_1 = value;
		Il2CppCodeGenWriteBarrier((&___rubyCharacters_1), value);
	}

	inline static int32_t get_offset_of_isOver_2() { return static_cast<int32_t>(offsetof(UguiNovelTextLine_t2439763533, ___isOver_2)); }
	inline bool get_isOver_2() const { return ___isOver_2; }
	inline bool* get_address_of_isOver_2() { return &___isOver_2; }
	inline void set_isOver_2(bool value)
	{
		___isOver_2 = value;
	}

	inline static int32_t get_offset_of_maxFontSize_3() { return static_cast<int32_t>(offsetof(UguiNovelTextLine_t2439763533, ___maxFontSize_3)); }
	inline int32_t get_maxFontSize_3() const { return ___maxFontSize_3; }
	inline int32_t* get_address_of_maxFontSize_3() { return &___maxFontSize_3; }
	inline void set_maxFontSize_3(int32_t value)
	{
		___maxFontSize_3 = value;
	}

	inline static int32_t get_offset_of_width_4() { return static_cast<int32_t>(offsetof(UguiNovelTextLine_t2439763533, ___width_4)); }
	inline float get_width_4() const { return ___width_4; }
	inline float* get_address_of_width_4() { return &___width_4; }
	inline void set_width_4(float value)
	{
		___width_4 = value;
	}

	inline static int32_t get_offset_of_totalHeight_5() { return static_cast<int32_t>(offsetof(UguiNovelTextLine_t2439763533, ___totalHeight_5)); }
	inline float get_totalHeight_5() const { return ___totalHeight_5; }
	inline float* get_address_of_totalHeight_5() { return &___totalHeight_5; }
	inline void set_totalHeight_5(float value)
	{
		___totalHeight_5 = value;
	}

	inline static int32_t get_offset_of_U3CY0U3Ek__BackingField_6() { return static_cast<int32_t>(offsetof(UguiNovelTextLine_t2439763533, ___U3CY0U3Ek__BackingField_6)); }
	inline float get_U3CY0U3Ek__BackingField_6() const { return ___U3CY0U3Ek__BackingField_6; }
	inline float* get_address_of_U3CY0U3Ek__BackingField_6() { return &___U3CY0U3Ek__BackingField_6; }
	inline void set_U3CY0U3Ek__BackingField_6(float value)
	{
		___U3CY0U3Ek__BackingField_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UGUINOVELTEXTLINE_T2439763533_H
#ifndef U3CINITU3EC__ANONSTOREY0_T2630862191_H
#define U3CINITU3EC__ANONSTOREY0_T2630862191_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.UguiCategoryGridPage/<Init>c__AnonStorey0
struct  U3CInitU3Ec__AnonStorey0_t2630862191  : public RuntimeObject
{
public:
	// System.Action`1<Utage.UguiCategoryGridPage> Utage.UguiCategoryGridPage/<Init>c__AnonStorey0::OpenCurrentCategory
	Action_1_t475622313 * ___OpenCurrentCategory_0;
	// Utage.UguiCategoryGridPage Utage.UguiCategoryGridPage/<Init>c__AnonStorey0::$this
	UguiCategoryGridPage_t673822931 * ___U24this_1;

public:
	inline static int32_t get_offset_of_OpenCurrentCategory_0() { return static_cast<int32_t>(offsetof(U3CInitU3Ec__AnonStorey0_t2630862191, ___OpenCurrentCategory_0)); }
	inline Action_1_t475622313 * get_OpenCurrentCategory_0() const { return ___OpenCurrentCategory_0; }
	inline Action_1_t475622313 ** get_address_of_OpenCurrentCategory_0() { return &___OpenCurrentCategory_0; }
	inline void set_OpenCurrentCategory_0(Action_1_t475622313 * value)
	{
		___OpenCurrentCategory_0 = value;
		Il2CppCodeGenWriteBarrier((&___OpenCurrentCategory_0), value);
	}

	inline static int32_t get_offset_of_U24this_1() { return static_cast<int32_t>(offsetof(U3CInitU3Ec__AnonStorey0_t2630862191, ___U24this_1)); }
	inline UguiCategoryGridPage_t673822931 * get_U24this_1() const { return ___U24this_1; }
	inline UguiCategoryGridPage_t673822931 ** get_address_of_U24this_1() { return &___U24this_1; }
	inline void set_U24this_1(UguiCategoryGridPage_t673822931 * value)
	{
		___U24this_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CINITU3EC__ANONSTOREY0_T2630862191_H
#ifndef FADETEXTUREINFO_T2015309025_H
#define FADETEXTUREINFO_T2015309025_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.UguiFadeTextureStream/FadeTextureInfo
struct  FadeTextureInfo_t2015309025  : public RuntimeObject
{
public:
	// UnityEngine.Texture Utage.UguiFadeTextureStream/FadeTextureInfo::texture
	Texture_t2243626319 * ___texture_0;
	// System.String Utage.UguiFadeTextureStream/FadeTextureInfo::moviePath
	String_t* ___moviePath_1;
	// System.Single Utage.UguiFadeTextureStream/FadeTextureInfo::fadeInTime
	float ___fadeInTime_2;
	// System.Single Utage.UguiFadeTextureStream/FadeTextureInfo::duration
	float ___duration_3;
	// System.Single Utage.UguiFadeTextureStream/FadeTextureInfo::fadeOutTime
	float ___fadeOutTime_4;
	// System.Boolean Utage.UguiFadeTextureStream/FadeTextureInfo::allowSkip
	bool ___allowSkip_5;

public:
	inline static int32_t get_offset_of_texture_0() { return static_cast<int32_t>(offsetof(FadeTextureInfo_t2015309025, ___texture_0)); }
	inline Texture_t2243626319 * get_texture_0() const { return ___texture_0; }
	inline Texture_t2243626319 ** get_address_of_texture_0() { return &___texture_0; }
	inline void set_texture_0(Texture_t2243626319 * value)
	{
		___texture_0 = value;
		Il2CppCodeGenWriteBarrier((&___texture_0), value);
	}

	inline static int32_t get_offset_of_moviePath_1() { return static_cast<int32_t>(offsetof(FadeTextureInfo_t2015309025, ___moviePath_1)); }
	inline String_t* get_moviePath_1() const { return ___moviePath_1; }
	inline String_t** get_address_of_moviePath_1() { return &___moviePath_1; }
	inline void set_moviePath_1(String_t* value)
	{
		___moviePath_1 = value;
		Il2CppCodeGenWriteBarrier((&___moviePath_1), value);
	}

	inline static int32_t get_offset_of_fadeInTime_2() { return static_cast<int32_t>(offsetof(FadeTextureInfo_t2015309025, ___fadeInTime_2)); }
	inline float get_fadeInTime_2() const { return ___fadeInTime_2; }
	inline float* get_address_of_fadeInTime_2() { return &___fadeInTime_2; }
	inline void set_fadeInTime_2(float value)
	{
		___fadeInTime_2 = value;
	}

	inline static int32_t get_offset_of_duration_3() { return static_cast<int32_t>(offsetof(FadeTextureInfo_t2015309025, ___duration_3)); }
	inline float get_duration_3() const { return ___duration_3; }
	inline float* get_address_of_duration_3() { return &___duration_3; }
	inline void set_duration_3(float value)
	{
		___duration_3 = value;
	}

	inline static int32_t get_offset_of_fadeOutTime_4() { return static_cast<int32_t>(offsetof(FadeTextureInfo_t2015309025, ___fadeOutTime_4)); }
	inline float get_fadeOutTime_4() const { return ___fadeOutTime_4; }
	inline float* get_address_of_fadeOutTime_4() { return &___fadeOutTime_4; }
	inline void set_fadeOutTime_4(float value)
	{
		___fadeOutTime_4 = value;
	}

	inline static int32_t get_offset_of_allowSkip_5() { return static_cast<int32_t>(offsetof(FadeTextureInfo_t2015309025, ___allowSkip_5)); }
	inline bool get_allowSkip_5() const { return ___allowSkip_5; }
	inline bool* get_address_of_allowSkip_5() { return &___allowSkip_5; }
	inline void set_allowSkip_5(bool value)
	{
		___allowSkip_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FADETEXTUREINFO_T2015309025_H
#ifndef U3CCOPLAYU3EC__ITERATOR0_T252162737_H
#define U3CCOPLAYU3EC__ITERATOR0_T252162737_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.UguiFadeTextureStream/<CoPlay>c__Iterator0
struct  U3CCoPlayU3Ec__Iterator0_t252162737  : public RuntimeObject
{
public:
	// UnityEngine.UI.RawImage Utage.UguiFadeTextureStream/<CoPlay>c__Iterator0::<rawImage>__0
	RawImage_t2749640213 * ___U3CrawImageU3E__0_0;
	// Utage.UguiFadeTextureStream/FadeTextureInfo[] Utage.UguiFadeTextureStream/<CoPlay>c__Iterator0::$locvar0
	FadeTextureInfoU5BU5D_t3972777596* ___U24locvar0_1;
	// System.Int32 Utage.UguiFadeTextureStream/<CoPlay>c__Iterator0::$locvar1
	int32_t ___U24locvar1_2;
	// Utage.UguiFadeTextureStream/FadeTextureInfo Utage.UguiFadeTextureStream/<CoPlay>c__Iterator0::<info>__1
	FadeTextureInfo_t2015309025 * ___U3CinfoU3E__1_3;
	// System.Boolean Utage.UguiFadeTextureStream/<CoPlay>c__Iterator0::<allSkip>__2
	bool ___U3CallSkipU3E__2_4;
	// System.Single Utage.UguiFadeTextureStream/<CoPlay>c__Iterator0::<time>__3
	float ___U3CtimeU3E__3_5;
	// Utage.UguiFadeTextureStream Utage.UguiFadeTextureStream/<CoPlay>c__Iterator0::$this
	UguiFadeTextureStream_t4272889985 * ___U24this_6;
	// System.Object Utage.UguiFadeTextureStream/<CoPlay>c__Iterator0::$current
	RuntimeObject * ___U24current_7;
	// System.Boolean Utage.UguiFadeTextureStream/<CoPlay>c__Iterator0::$disposing
	bool ___U24disposing_8;
	// System.Int32 Utage.UguiFadeTextureStream/<CoPlay>c__Iterator0::$PC
	int32_t ___U24PC_9;

public:
	inline static int32_t get_offset_of_U3CrawImageU3E__0_0() { return static_cast<int32_t>(offsetof(U3CCoPlayU3Ec__Iterator0_t252162737, ___U3CrawImageU3E__0_0)); }
	inline RawImage_t2749640213 * get_U3CrawImageU3E__0_0() const { return ___U3CrawImageU3E__0_0; }
	inline RawImage_t2749640213 ** get_address_of_U3CrawImageU3E__0_0() { return &___U3CrawImageU3E__0_0; }
	inline void set_U3CrawImageU3E__0_0(RawImage_t2749640213 * value)
	{
		___U3CrawImageU3E__0_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CrawImageU3E__0_0), value);
	}

	inline static int32_t get_offset_of_U24locvar0_1() { return static_cast<int32_t>(offsetof(U3CCoPlayU3Ec__Iterator0_t252162737, ___U24locvar0_1)); }
	inline FadeTextureInfoU5BU5D_t3972777596* get_U24locvar0_1() const { return ___U24locvar0_1; }
	inline FadeTextureInfoU5BU5D_t3972777596** get_address_of_U24locvar0_1() { return &___U24locvar0_1; }
	inline void set_U24locvar0_1(FadeTextureInfoU5BU5D_t3972777596* value)
	{
		___U24locvar0_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24locvar0_1), value);
	}

	inline static int32_t get_offset_of_U24locvar1_2() { return static_cast<int32_t>(offsetof(U3CCoPlayU3Ec__Iterator0_t252162737, ___U24locvar1_2)); }
	inline int32_t get_U24locvar1_2() const { return ___U24locvar1_2; }
	inline int32_t* get_address_of_U24locvar1_2() { return &___U24locvar1_2; }
	inline void set_U24locvar1_2(int32_t value)
	{
		___U24locvar1_2 = value;
	}

	inline static int32_t get_offset_of_U3CinfoU3E__1_3() { return static_cast<int32_t>(offsetof(U3CCoPlayU3Ec__Iterator0_t252162737, ___U3CinfoU3E__1_3)); }
	inline FadeTextureInfo_t2015309025 * get_U3CinfoU3E__1_3() const { return ___U3CinfoU3E__1_3; }
	inline FadeTextureInfo_t2015309025 ** get_address_of_U3CinfoU3E__1_3() { return &___U3CinfoU3E__1_3; }
	inline void set_U3CinfoU3E__1_3(FadeTextureInfo_t2015309025 * value)
	{
		___U3CinfoU3E__1_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CinfoU3E__1_3), value);
	}

	inline static int32_t get_offset_of_U3CallSkipU3E__2_4() { return static_cast<int32_t>(offsetof(U3CCoPlayU3Ec__Iterator0_t252162737, ___U3CallSkipU3E__2_4)); }
	inline bool get_U3CallSkipU3E__2_4() const { return ___U3CallSkipU3E__2_4; }
	inline bool* get_address_of_U3CallSkipU3E__2_4() { return &___U3CallSkipU3E__2_4; }
	inline void set_U3CallSkipU3E__2_4(bool value)
	{
		___U3CallSkipU3E__2_4 = value;
	}

	inline static int32_t get_offset_of_U3CtimeU3E__3_5() { return static_cast<int32_t>(offsetof(U3CCoPlayU3Ec__Iterator0_t252162737, ___U3CtimeU3E__3_5)); }
	inline float get_U3CtimeU3E__3_5() const { return ___U3CtimeU3E__3_5; }
	inline float* get_address_of_U3CtimeU3E__3_5() { return &___U3CtimeU3E__3_5; }
	inline void set_U3CtimeU3E__3_5(float value)
	{
		___U3CtimeU3E__3_5 = value;
	}

	inline static int32_t get_offset_of_U24this_6() { return static_cast<int32_t>(offsetof(U3CCoPlayU3Ec__Iterator0_t252162737, ___U24this_6)); }
	inline UguiFadeTextureStream_t4272889985 * get_U24this_6() const { return ___U24this_6; }
	inline UguiFadeTextureStream_t4272889985 ** get_address_of_U24this_6() { return &___U24this_6; }
	inline void set_U24this_6(UguiFadeTextureStream_t4272889985 * value)
	{
		___U24this_6 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_6), value);
	}

	inline static int32_t get_offset_of_U24current_7() { return static_cast<int32_t>(offsetof(U3CCoPlayU3Ec__Iterator0_t252162737, ___U24current_7)); }
	inline RuntimeObject * get_U24current_7() const { return ___U24current_7; }
	inline RuntimeObject ** get_address_of_U24current_7() { return &___U24current_7; }
	inline void set_U24current_7(RuntimeObject * value)
	{
		___U24current_7 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_7), value);
	}

	inline static int32_t get_offset_of_U24disposing_8() { return static_cast<int32_t>(offsetof(U3CCoPlayU3Ec__Iterator0_t252162737, ___U24disposing_8)); }
	inline bool get_U24disposing_8() const { return ___U24disposing_8; }
	inline bool* get_address_of_U24disposing_8() { return &___U24disposing_8; }
	inline void set_U24disposing_8(bool value)
	{
		___U24disposing_8 = value;
	}

	inline static int32_t get_offset_of_U24PC_9() { return static_cast<int32_t>(offsetof(U3CCoPlayU3Ec__Iterator0_t252162737, ___U24PC_9)); }
	inline int32_t get_U24PC_9() const { return ___U24PC_9; }
	inline int32_t* get_address_of_U24PC_9() { return &___U24PC_9; }
	inline void set_U24PC_9(int32_t value)
	{
		___U24PC_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CCOPLAYU3EC__ITERATOR0_T252162737_H
#ifndef U3CAWAKEU3EC__ANONSTOREY0_T3297509173_H
#define U3CAWAKEU3EC__ANONSTOREY0_T3297509173_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.UguiToggleGroupIndexed/<Awake>c__AnonStorey0
struct  U3CAwakeU3Ec__AnonStorey0_t3297509173  : public RuntimeObject
{
public:
	// UnityEngine.UI.Toggle Utage.UguiToggleGroupIndexed/<Awake>c__AnonStorey0::toggle
	Toggle_t3976754468 * ___toggle_0;
	// Utage.UguiToggleGroupIndexed Utage.UguiToggleGroupIndexed/<Awake>c__AnonStorey0::$this
	UguiToggleGroupIndexed_t999166452 * ___U24this_1;

public:
	inline static int32_t get_offset_of_toggle_0() { return static_cast<int32_t>(offsetof(U3CAwakeU3Ec__AnonStorey0_t3297509173, ___toggle_0)); }
	inline Toggle_t3976754468 * get_toggle_0() const { return ___toggle_0; }
	inline Toggle_t3976754468 ** get_address_of_toggle_0() { return &___toggle_0; }
	inline void set_toggle_0(Toggle_t3976754468 * value)
	{
		___toggle_0 = value;
		Il2CppCodeGenWriteBarrier((&___toggle_0), value);
	}

	inline static int32_t get_offset_of_U24this_1() { return static_cast<int32_t>(offsetof(U3CAwakeU3Ec__AnonStorey0_t3297509173, ___U24this_1)); }
	inline UguiToggleGroupIndexed_t999166452 * get_U24this_1() const { return ___U24this_1; }
	inline UguiToggleGroupIndexed_t999166452 ** get_address_of_U24this_1() { return &___U24this_1; }
	inline void set_U24this_1(UguiToggleGroupIndexed_t999166452 * value)
	{
		___U24this_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CAWAKEU3EC__ANONSTOREY0_T3297509173_H
#ifndef U3CONTOGGLEVALUECHANGEDU3EC__ANONSTOREY1_T907687639_H
#define U3CONTOGGLEVALUECHANGEDU3EC__ANONSTOREY1_T907687639_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.UguiToggleGroupIndexed/<OnToggleValueChanged>c__AnonStorey1
struct  U3COnToggleValueChangedU3Ec__AnonStorey1_t907687639  : public RuntimeObject
{
public:
	// UnityEngine.UI.Toggle Utage.UguiToggleGroupIndexed/<OnToggleValueChanged>c__AnonStorey1::toggle
	Toggle_t3976754468 * ___toggle_0;

public:
	inline static int32_t get_offset_of_toggle_0() { return static_cast<int32_t>(offsetof(U3COnToggleValueChangedU3Ec__AnonStorey1_t907687639, ___toggle_0)); }
	inline Toggle_t3976754468 * get_toggle_0() const { return ___toggle_0; }
	inline Toggle_t3976754468 ** get_address_of_toggle_0() { return &___toggle_0; }
	inline void set_toggle_0(Toggle_t3976754468 * value)
	{
		___toggle_0 = value;
		Il2CppCodeGenWriteBarrier((&___toggle_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CONTOGGLEVALUECHANGEDU3EC__ANONSTOREY1_T907687639_H
#ifndef SETTING_T3544201405_H
#define SETTING_T3544201405_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.UguiLocalizeTextSetting/Setting
struct  Setting_t3544201405  : public RuntimeObject
{
public:
	// System.String Utage.UguiLocalizeTextSetting/Setting::language
	String_t* ___language_0;
	// UnityEngine.Font Utage.UguiLocalizeTextSetting/Setting::font
	Font_t4239498691 * ___font_1;
	// System.Int32 Utage.UguiLocalizeTextSetting/Setting::fontSize
	int32_t ___fontSize_2;
	// System.Single Utage.UguiLocalizeTextSetting/Setting::lineSpacing
	float ___lineSpacing_3;

public:
	inline static int32_t get_offset_of_language_0() { return static_cast<int32_t>(offsetof(Setting_t3544201405, ___language_0)); }
	inline String_t* get_language_0() const { return ___language_0; }
	inline String_t** get_address_of_language_0() { return &___language_0; }
	inline void set_language_0(String_t* value)
	{
		___language_0 = value;
		Il2CppCodeGenWriteBarrier((&___language_0), value);
	}

	inline static int32_t get_offset_of_font_1() { return static_cast<int32_t>(offsetof(Setting_t3544201405, ___font_1)); }
	inline Font_t4239498691 * get_font_1() const { return ___font_1; }
	inline Font_t4239498691 ** get_address_of_font_1() { return &___font_1; }
	inline void set_font_1(Font_t4239498691 * value)
	{
		___font_1 = value;
		Il2CppCodeGenWriteBarrier((&___font_1), value);
	}

	inline static int32_t get_offset_of_fontSize_2() { return static_cast<int32_t>(offsetof(Setting_t3544201405, ___fontSize_2)); }
	inline int32_t get_fontSize_2() const { return ___fontSize_2; }
	inline int32_t* get_address_of_fontSize_2() { return &___fontSize_2; }
	inline void set_fontSize_2(int32_t value)
	{
		___fontSize_2 = value;
	}

	inline static int32_t get_offset_of_lineSpacing_3() { return static_cast<int32_t>(offsetof(Setting_t3544201405, ___lineSpacing_3)); }
	inline float get_lineSpacing_3() const { return ___lineSpacing_3; }
	inline float* get_address_of_lineSpacing_3() { return &___lineSpacing_3; }
	inline void set_lineSpacing_3(float value)
	{
		___lineSpacing_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SETTING_T3544201405_H
#ifndef U3CCOPLAYU3EC__ITERATOR0_T563534176_H
#define U3CCOPLAYU3EC__ITERATOR0_T563534176_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.DicingAnimation/<CoPlay>c__Iterator0
struct  U3CCoPlayU3Ec__Iterator0_t563534176  : public RuntimeObject
{
public:
	// System.Collections.Generic.List`1<System.String> Utage.DicingAnimation/<CoPlay>c__Iterator0::<list>__0
	List_1_t1398341365 * ___U3ClistU3E__0_0;
	// System.Boolean Utage.DicingAnimation/<CoPlay>c__Iterator0::<isEnd>__1
	bool ___U3CisEndU3E__1_1;
	// System.Action Utage.DicingAnimation/<CoPlay>c__Iterator0::onComplete
	Action_t3226471752 * ___onComplete_2;
	// Utage.DicingAnimation Utage.DicingAnimation/<CoPlay>c__Iterator0::$this
	DicingAnimation_t3770383148 * ___U24this_3;
	// System.Object Utage.DicingAnimation/<CoPlay>c__Iterator0::$current
	RuntimeObject * ___U24current_4;
	// System.Boolean Utage.DicingAnimation/<CoPlay>c__Iterator0::$disposing
	bool ___U24disposing_5;
	// System.Int32 Utage.DicingAnimation/<CoPlay>c__Iterator0::$PC
	int32_t ___U24PC_6;

public:
	inline static int32_t get_offset_of_U3ClistU3E__0_0() { return static_cast<int32_t>(offsetof(U3CCoPlayU3Ec__Iterator0_t563534176, ___U3ClistU3E__0_0)); }
	inline List_1_t1398341365 * get_U3ClistU3E__0_0() const { return ___U3ClistU3E__0_0; }
	inline List_1_t1398341365 ** get_address_of_U3ClistU3E__0_0() { return &___U3ClistU3E__0_0; }
	inline void set_U3ClistU3E__0_0(List_1_t1398341365 * value)
	{
		___U3ClistU3E__0_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3ClistU3E__0_0), value);
	}

	inline static int32_t get_offset_of_U3CisEndU3E__1_1() { return static_cast<int32_t>(offsetof(U3CCoPlayU3Ec__Iterator0_t563534176, ___U3CisEndU3E__1_1)); }
	inline bool get_U3CisEndU3E__1_1() const { return ___U3CisEndU3E__1_1; }
	inline bool* get_address_of_U3CisEndU3E__1_1() { return &___U3CisEndU3E__1_1; }
	inline void set_U3CisEndU3E__1_1(bool value)
	{
		___U3CisEndU3E__1_1 = value;
	}

	inline static int32_t get_offset_of_onComplete_2() { return static_cast<int32_t>(offsetof(U3CCoPlayU3Ec__Iterator0_t563534176, ___onComplete_2)); }
	inline Action_t3226471752 * get_onComplete_2() const { return ___onComplete_2; }
	inline Action_t3226471752 ** get_address_of_onComplete_2() { return &___onComplete_2; }
	inline void set_onComplete_2(Action_t3226471752 * value)
	{
		___onComplete_2 = value;
		Il2CppCodeGenWriteBarrier((&___onComplete_2), value);
	}

	inline static int32_t get_offset_of_U24this_3() { return static_cast<int32_t>(offsetof(U3CCoPlayU3Ec__Iterator0_t563534176, ___U24this_3)); }
	inline DicingAnimation_t3770383148 * get_U24this_3() const { return ___U24this_3; }
	inline DicingAnimation_t3770383148 ** get_address_of_U24this_3() { return &___U24this_3; }
	inline void set_U24this_3(DicingAnimation_t3770383148 * value)
	{
		___U24this_3 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_3), value);
	}

	inline static int32_t get_offset_of_U24current_4() { return static_cast<int32_t>(offsetof(U3CCoPlayU3Ec__Iterator0_t563534176, ___U24current_4)); }
	inline RuntimeObject * get_U24current_4() const { return ___U24current_4; }
	inline RuntimeObject ** get_address_of_U24current_4() { return &___U24current_4; }
	inline void set_U24current_4(RuntimeObject * value)
	{
		___U24current_4 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_4), value);
	}

	inline static int32_t get_offset_of_U24disposing_5() { return static_cast<int32_t>(offsetof(U3CCoPlayU3Ec__Iterator0_t563534176, ___U24disposing_5)); }
	inline bool get_U24disposing_5() const { return ___U24disposing_5; }
	inline bool* get_address_of_U24disposing_5() { return &___U24disposing_5; }
	inline void set_U24disposing_5(bool value)
	{
		___U24disposing_5 = value;
	}

	inline static int32_t get_offset_of_U24PC_6() { return static_cast<int32_t>(offsetof(U3CCoPlayU3Ec__Iterator0_t563534176, ___U24PC_6)); }
	inline int32_t get_U24PC_6() const { return ___U24PC_6; }
	inline int32_t* get_address_of_U24PC_6() { return &___U24PC_6; }
	inline void set_U24PC_6(int32_t value)
	{
		___U24PC_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CCOPLAYU3EC__ITERATOR0_T563534176_H
#ifndef DICINGTEXTUREDATA_T752335795_H
#define DICINGTEXTUREDATA_T752335795_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.DicingTextureData
struct  DicingTextureData_t752335795  : public RuntimeObject
{
public:
	// System.String Utage.DicingTextureData::name
	String_t* ___name_0;
	// System.String Utage.DicingTextureData::atlasName
	String_t* ___atlasName_1;
	// System.Int32 Utage.DicingTextureData::width
	int32_t ___width_2;
	// System.Int32 Utage.DicingTextureData::height
	int32_t ___height_3;
	// System.Collections.Generic.List`1<System.Int32> Utage.DicingTextureData::cellIndexList
	List_1_t1440998580 * ___cellIndexList_4;
	// System.Int32 Utage.DicingTextureData::transparentIndex
	int32_t ___transparentIndex_5;
	// System.Collections.Generic.List`1<Utage.DicingTextureData/QuadVerts> Utage.DicingTextureData::verts
	List_1_t4123338219 * ___verts_6;

public:
	inline static int32_t get_offset_of_name_0() { return static_cast<int32_t>(offsetof(DicingTextureData_t752335795, ___name_0)); }
	inline String_t* get_name_0() const { return ___name_0; }
	inline String_t** get_address_of_name_0() { return &___name_0; }
	inline void set_name_0(String_t* value)
	{
		___name_0 = value;
		Il2CppCodeGenWriteBarrier((&___name_0), value);
	}

	inline static int32_t get_offset_of_atlasName_1() { return static_cast<int32_t>(offsetof(DicingTextureData_t752335795, ___atlasName_1)); }
	inline String_t* get_atlasName_1() const { return ___atlasName_1; }
	inline String_t** get_address_of_atlasName_1() { return &___atlasName_1; }
	inline void set_atlasName_1(String_t* value)
	{
		___atlasName_1 = value;
		Il2CppCodeGenWriteBarrier((&___atlasName_1), value);
	}

	inline static int32_t get_offset_of_width_2() { return static_cast<int32_t>(offsetof(DicingTextureData_t752335795, ___width_2)); }
	inline int32_t get_width_2() const { return ___width_2; }
	inline int32_t* get_address_of_width_2() { return &___width_2; }
	inline void set_width_2(int32_t value)
	{
		___width_2 = value;
	}

	inline static int32_t get_offset_of_height_3() { return static_cast<int32_t>(offsetof(DicingTextureData_t752335795, ___height_3)); }
	inline int32_t get_height_3() const { return ___height_3; }
	inline int32_t* get_address_of_height_3() { return &___height_3; }
	inline void set_height_3(int32_t value)
	{
		___height_3 = value;
	}

	inline static int32_t get_offset_of_cellIndexList_4() { return static_cast<int32_t>(offsetof(DicingTextureData_t752335795, ___cellIndexList_4)); }
	inline List_1_t1440998580 * get_cellIndexList_4() const { return ___cellIndexList_4; }
	inline List_1_t1440998580 ** get_address_of_cellIndexList_4() { return &___cellIndexList_4; }
	inline void set_cellIndexList_4(List_1_t1440998580 * value)
	{
		___cellIndexList_4 = value;
		Il2CppCodeGenWriteBarrier((&___cellIndexList_4), value);
	}

	inline static int32_t get_offset_of_transparentIndex_5() { return static_cast<int32_t>(offsetof(DicingTextureData_t752335795, ___transparentIndex_5)); }
	inline int32_t get_transparentIndex_5() const { return ___transparentIndex_5; }
	inline int32_t* get_address_of_transparentIndex_5() { return &___transparentIndex_5; }
	inline void set_transparentIndex_5(int32_t value)
	{
		___transparentIndex_5 = value;
	}

	inline static int32_t get_offset_of_verts_6() { return static_cast<int32_t>(offsetof(DicingTextureData_t752335795, ___verts_6)); }
	inline List_1_t4123338219 * get_verts_6() const { return ___verts_6; }
	inline List_1_t4123338219 ** get_address_of_verts_6() { return &___verts_6; }
	inline void set_verts_6(List_1_t4123338219 * value)
	{
		___verts_6 = value;
		Il2CppCodeGenWriteBarrier((&___verts_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DICINGTEXTUREDATA_T752335795_H
#ifndef U3CFOREACHVERTEXLISTU3EC__ANONSTOREY2_T1404970981_H
#define U3CFOREACHVERTEXLISTU3EC__ANONSTOREY2_T1404970981_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.DicingTextureData/<ForeachVertexList>c__AnonStorey2
struct  U3CForeachVertexListU3Ec__AnonStorey2_t1404970981  : public RuntimeObject
{
public:
	// System.Single Utage.DicingTextureData/<ForeachVertexList>c__AnonStorey2::offsetX
	float ___offsetX_0;
	// Utage.DicingTextureData/<ForeachVertexList>c__AnonStorey1 Utage.DicingTextureData/<ForeachVertexList>c__AnonStorey2::<>f__ref$1
	U3CForeachVertexListU3Ec__AnonStorey1_t1404970980 * ___U3CU3Ef__refU241_1;

public:
	inline static int32_t get_offset_of_offsetX_0() { return static_cast<int32_t>(offsetof(U3CForeachVertexListU3Ec__AnonStorey2_t1404970981, ___offsetX_0)); }
	inline float get_offsetX_0() const { return ___offsetX_0; }
	inline float* get_address_of_offsetX_0() { return &___offsetX_0; }
	inline void set_offsetX_0(float value)
	{
		___offsetX_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3Ef__refU241_1() { return static_cast<int32_t>(offsetof(U3CForeachVertexListU3Ec__AnonStorey2_t1404970981, ___U3CU3Ef__refU241_1)); }
	inline U3CForeachVertexListU3Ec__AnonStorey1_t1404970980 * get_U3CU3Ef__refU241_1() const { return ___U3CU3Ef__refU241_1; }
	inline U3CForeachVertexListU3Ec__AnonStorey1_t1404970980 ** get_address_of_U3CU3Ef__refU241_1() { return &___U3CU3Ef__refU241_1; }
	inline void set_U3CU3Ef__refU241_1(U3CForeachVertexListU3Ec__AnonStorey1_t1404970980 * value)
	{
		___U3CU3Ef__refU241_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__refU241_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CFOREACHVERTEXLISTU3EC__ANONSTOREY2_T1404970981_H
#ifndef U3CEXISTSU3EC__ANONSTOREY0_T771376738_H
#define U3CEXISTSU3EC__ANONSTOREY0_T771376738_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.DicingTextures/<Exists>c__AnonStorey0
struct  U3CExistsU3Ec__AnonStorey0_t771376738  : public RuntimeObject
{
public:
	// System.String Utage.DicingTextures/<Exists>c__AnonStorey0::pattern
	String_t* ___pattern_0;

public:
	inline static int32_t get_offset_of_pattern_0() { return static_cast<int32_t>(offsetof(U3CExistsU3Ec__AnonStorey0_t771376738, ___pattern_0)); }
	inline String_t* get_pattern_0() const { return ___pattern_0; }
	inline String_t** get_address_of_pattern_0() { return &___pattern_0; }
	inline void set_pattern_0(String_t* value)
	{
		___pattern_0 = value;
		Il2CppCodeGenWriteBarrier((&___pattern_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CEXISTSU3EC__ANONSTOREY0_T771376738_H
#ifndef U3CGETTEXTUREU3EC__ANONSTOREY1_T379105648_H
#define U3CGETTEXTUREU3EC__ANONSTOREY1_T379105648_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.DicingTextures/<GetTexture>c__AnonStorey1
struct  U3CGetTextureU3Ec__AnonStorey1_t379105648  : public RuntimeObject
{
public:
	// System.String Utage.DicingTextures/<GetTexture>c__AnonStorey1::name
	String_t* ___name_0;

public:
	inline static int32_t get_offset_of_name_0() { return static_cast<int32_t>(offsetof(U3CGetTextureU3Ec__AnonStorey1_t379105648, ___name_0)); }
	inline String_t* get_name_0() const { return ___name_0; }
	inline String_t** get_address_of_name_0() { return &___name_0; }
	inline void set_name_0(String_t* value)
	{
		___name_0 = value;
		Il2CppCodeGenWriteBarrier((&___name_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CGETTEXTUREU3EC__ANONSTOREY1_T379105648_H
#ifndef PARSERUTIL_T1878827749_H
#define PARSERUTIL_T1878827749_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.ParserUtil
struct  ParserUtil_t1878827749  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PARSERUTIL_T1878827749_H
#ifndef LINEARVALUE_T662057272_H
#define LINEARVALUE_T662057272_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.LinearValue
struct  LinearValue_t662057272  : public RuntimeObject
{
public:
	// System.Single Utage.LinearValue::time
	float ___time_0;
	// System.Single Utage.LinearValue::timeCurrent
	float ___timeCurrent_1;
	// System.Single Utage.LinearValue::valueBegin
	float ___valueBegin_2;
	// System.Single Utage.LinearValue::valueEnd
	float ___valueEnd_3;
	// System.Single Utage.LinearValue::valueCurrent
	float ___valueCurrent_4;

public:
	inline static int32_t get_offset_of_time_0() { return static_cast<int32_t>(offsetof(LinearValue_t662057272, ___time_0)); }
	inline float get_time_0() const { return ___time_0; }
	inline float* get_address_of_time_0() { return &___time_0; }
	inline void set_time_0(float value)
	{
		___time_0 = value;
	}

	inline static int32_t get_offset_of_timeCurrent_1() { return static_cast<int32_t>(offsetof(LinearValue_t662057272, ___timeCurrent_1)); }
	inline float get_timeCurrent_1() const { return ___timeCurrent_1; }
	inline float* get_address_of_timeCurrent_1() { return &___timeCurrent_1; }
	inline void set_timeCurrent_1(float value)
	{
		___timeCurrent_1 = value;
	}

	inline static int32_t get_offset_of_valueBegin_2() { return static_cast<int32_t>(offsetof(LinearValue_t662057272, ___valueBegin_2)); }
	inline float get_valueBegin_2() const { return ___valueBegin_2; }
	inline float* get_address_of_valueBegin_2() { return &___valueBegin_2; }
	inline void set_valueBegin_2(float value)
	{
		___valueBegin_2 = value;
	}

	inline static int32_t get_offset_of_valueEnd_3() { return static_cast<int32_t>(offsetof(LinearValue_t662057272, ___valueEnd_3)); }
	inline float get_valueEnd_3() const { return ___valueEnd_3; }
	inline float* get_address_of_valueEnd_3() { return &___valueEnd_3; }
	inline void set_valueEnd_3(float value)
	{
		___valueEnd_3 = value;
	}

	inline static int32_t get_offset_of_valueCurrent_4() { return static_cast<int32_t>(offsetof(LinearValue_t662057272, ___valueCurrent_4)); }
	inline float get_valueCurrent_4() const { return ___valueCurrent_4; }
	inline float* get_address_of_valueCurrent_4() { return &___valueCurrent_4; }
	inline void set_valueCurrent_4(float value)
	{
		___valueCurrent_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LINEARVALUE_T662057272_H
#ifndef FLAGSUTIL_T2831559481_H
#define FLAGSUTIL_T2831559481_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.FlagsUtil
struct  FlagsUtil_t2831559481  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FLAGSUTIL_T2831559481_H
#ifndef INPUTUTIL_T3545718610_H
#define INPUTUTIL_T3545718610_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.InputUtil
struct  InputUtil_t3545718610  : public RuntimeObject
{
public:

public:
};

struct InputUtil_t3545718610_StaticFields
{
public:
	// System.Single Utage.InputUtil::wheelSensitive
	float ___wheelSensitive_0;

public:
	inline static int32_t get_offset_of_wheelSensitive_0() { return static_cast<int32_t>(offsetof(InputUtil_t3545718610_StaticFields, ___wheelSensitive_0)); }
	inline float get_wheelSensitive_0() const { return ___wheelSensitive_0; }
	inline float* get_address_of_wheelSensitive_0() { return &___wheelSensitive_0; }
	inline void set_wheelSensitive_0(float value)
	{
		___wheelSensitive_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INPUTUTIL_T3545718610_H
#ifndef UNITYEVENT_1_T2414510360_H
#define UNITYEVENT_1_T2414510360_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Events.UnityEvent`1<Utage.LipSynchBase>
struct  UnityEvent_1_t2414510360  : public UnityEventBase_t828812576
{
public:
	// System.Object[] UnityEngine.Events.UnityEvent`1::m_InvokeArray
	ObjectU5BU5D_t3614634134* ___m_InvokeArray_4;

public:
	inline static int32_t get_offset_of_m_InvokeArray_4() { return static_cast<int32_t>(offsetof(UnityEvent_1_t2414510360, ___m_InvokeArray_4)); }
	inline ObjectU5BU5D_t3614634134* get_m_InvokeArray_4() const { return ___m_InvokeArray_4; }
	inline ObjectU5BU5D_t3614634134** get_address_of_m_InvokeArray_4() { return &___m_InvokeArray_4; }
	inline void set_m_InvokeArray_4(ObjectU5BU5D_t3614634134* value)
	{
		___m_InvokeArray_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_InvokeArray_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYEVENT_1_T2414510360_H
#ifndef VECTOR3_T2243707580_H
#define VECTOR3_T2243707580_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector3
struct  Vector3_t2243707580 
{
public:
	// System.Single UnityEngine.Vector3::x
	float ___x_1;
	// System.Single UnityEngine.Vector3::y
	float ___y_2;
	// System.Single UnityEngine.Vector3::z
	float ___z_3;

public:
	inline static int32_t get_offset_of_x_1() { return static_cast<int32_t>(offsetof(Vector3_t2243707580, ___x_1)); }
	inline float get_x_1() const { return ___x_1; }
	inline float* get_address_of_x_1() { return &___x_1; }
	inline void set_x_1(float value)
	{
		___x_1 = value;
	}

	inline static int32_t get_offset_of_y_2() { return static_cast<int32_t>(offsetof(Vector3_t2243707580, ___y_2)); }
	inline float get_y_2() const { return ___y_2; }
	inline float* get_address_of_y_2() { return &___y_2; }
	inline void set_y_2(float value)
	{
		___y_2 = value;
	}

	inline static int32_t get_offset_of_z_3() { return static_cast<int32_t>(offsetof(Vector3_t2243707580, ___z_3)); }
	inline float get_z_3() const { return ___z_3; }
	inline float* get_address_of_z_3() { return &___z_3; }
	inline void set_z_3(float value)
	{
		___z_3 = value;
	}
};

struct Vector3_t2243707580_StaticFields
{
public:
	// UnityEngine.Vector3 UnityEngine.Vector3::zeroVector
	Vector3_t2243707580  ___zeroVector_4;
	// UnityEngine.Vector3 UnityEngine.Vector3::oneVector
	Vector3_t2243707580  ___oneVector_5;
	// UnityEngine.Vector3 UnityEngine.Vector3::upVector
	Vector3_t2243707580  ___upVector_6;
	// UnityEngine.Vector3 UnityEngine.Vector3::downVector
	Vector3_t2243707580  ___downVector_7;
	// UnityEngine.Vector3 UnityEngine.Vector3::leftVector
	Vector3_t2243707580  ___leftVector_8;
	// UnityEngine.Vector3 UnityEngine.Vector3::rightVector
	Vector3_t2243707580  ___rightVector_9;
	// UnityEngine.Vector3 UnityEngine.Vector3::forwardVector
	Vector3_t2243707580  ___forwardVector_10;
	// UnityEngine.Vector3 UnityEngine.Vector3::backVector
	Vector3_t2243707580  ___backVector_11;
	// UnityEngine.Vector3 UnityEngine.Vector3::positiveInfinityVector
	Vector3_t2243707580  ___positiveInfinityVector_12;
	// UnityEngine.Vector3 UnityEngine.Vector3::negativeInfinityVector
	Vector3_t2243707580  ___negativeInfinityVector_13;

public:
	inline static int32_t get_offset_of_zeroVector_4() { return static_cast<int32_t>(offsetof(Vector3_t2243707580_StaticFields, ___zeroVector_4)); }
	inline Vector3_t2243707580  get_zeroVector_4() const { return ___zeroVector_4; }
	inline Vector3_t2243707580 * get_address_of_zeroVector_4() { return &___zeroVector_4; }
	inline void set_zeroVector_4(Vector3_t2243707580  value)
	{
		___zeroVector_4 = value;
	}

	inline static int32_t get_offset_of_oneVector_5() { return static_cast<int32_t>(offsetof(Vector3_t2243707580_StaticFields, ___oneVector_5)); }
	inline Vector3_t2243707580  get_oneVector_5() const { return ___oneVector_5; }
	inline Vector3_t2243707580 * get_address_of_oneVector_5() { return &___oneVector_5; }
	inline void set_oneVector_5(Vector3_t2243707580  value)
	{
		___oneVector_5 = value;
	}

	inline static int32_t get_offset_of_upVector_6() { return static_cast<int32_t>(offsetof(Vector3_t2243707580_StaticFields, ___upVector_6)); }
	inline Vector3_t2243707580  get_upVector_6() const { return ___upVector_6; }
	inline Vector3_t2243707580 * get_address_of_upVector_6() { return &___upVector_6; }
	inline void set_upVector_6(Vector3_t2243707580  value)
	{
		___upVector_6 = value;
	}

	inline static int32_t get_offset_of_downVector_7() { return static_cast<int32_t>(offsetof(Vector3_t2243707580_StaticFields, ___downVector_7)); }
	inline Vector3_t2243707580  get_downVector_7() const { return ___downVector_7; }
	inline Vector3_t2243707580 * get_address_of_downVector_7() { return &___downVector_7; }
	inline void set_downVector_7(Vector3_t2243707580  value)
	{
		___downVector_7 = value;
	}

	inline static int32_t get_offset_of_leftVector_8() { return static_cast<int32_t>(offsetof(Vector3_t2243707580_StaticFields, ___leftVector_8)); }
	inline Vector3_t2243707580  get_leftVector_8() const { return ___leftVector_8; }
	inline Vector3_t2243707580 * get_address_of_leftVector_8() { return &___leftVector_8; }
	inline void set_leftVector_8(Vector3_t2243707580  value)
	{
		___leftVector_8 = value;
	}

	inline static int32_t get_offset_of_rightVector_9() { return static_cast<int32_t>(offsetof(Vector3_t2243707580_StaticFields, ___rightVector_9)); }
	inline Vector3_t2243707580  get_rightVector_9() const { return ___rightVector_9; }
	inline Vector3_t2243707580 * get_address_of_rightVector_9() { return &___rightVector_9; }
	inline void set_rightVector_9(Vector3_t2243707580  value)
	{
		___rightVector_9 = value;
	}

	inline static int32_t get_offset_of_forwardVector_10() { return static_cast<int32_t>(offsetof(Vector3_t2243707580_StaticFields, ___forwardVector_10)); }
	inline Vector3_t2243707580  get_forwardVector_10() const { return ___forwardVector_10; }
	inline Vector3_t2243707580 * get_address_of_forwardVector_10() { return &___forwardVector_10; }
	inline void set_forwardVector_10(Vector3_t2243707580  value)
	{
		___forwardVector_10 = value;
	}

	inline static int32_t get_offset_of_backVector_11() { return static_cast<int32_t>(offsetof(Vector3_t2243707580_StaticFields, ___backVector_11)); }
	inline Vector3_t2243707580  get_backVector_11() const { return ___backVector_11; }
	inline Vector3_t2243707580 * get_address_of_backVector_11() { return &___backVector_11; }
	inline void set_backVector_11(Vector3_t2243707580  value)
	{
		___backVector_11 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_12() { return static_cast<int32_t>(offsetof(Vector3_t2243707580_StaticFields, ___positiveInfinityVector_12)); }
	inline Vector3_t2243707580  get_positiveInfinityVector_12() const { return ___positiveInfinityVector_12; }
	inline Vector3_t2243707580 * get_address_of_positiveInfinityVector_12() { return &___positiveInfinityVector_12; }
	inline void set_positiveInfinityVector_12(Vector3_t2243707580  value)
	{
		___positiveInfinityVector_12 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_13() { return static_cast<int32_t>(offsetof(Vector3_t2243707580_StaticFields, ___negativeInfinityVector_13)); }
	inline Vector3_t2243707580  get_negativeInfinityVector_13() const { return ___negativeInfinityVector_13; }
	inline Vector3_t2243707580 * get_address_of_negativeInfinityVector_13() { return &___negativeInfinityVector_13; }
	inline void set_negativeInfinityVector_13(Vector3_t2243707580  value)
	{
		___negativeInfinityVector_13 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR3_T2243707580_H
#ifndef DRIVENRECTTRANSFORMTRACKER_T154385424_H
#define DRIVENRECTTRANSFORMTRACKER_T154385424_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.DrivenRectTransformTracker
struct  DrivenRectTransformTracker_t154385424 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DRIVENRECTTRANSFORMTRACKER_T154385424_H
#ifndef UNITYEVENT_1_T2110227463_H
#define UNITYEVENT_1_T2110227463_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Events.UnityEvent`1<System.Int32>
struct  UnityEvent_1_t2110227463  : public UnityEventBase_t828812576
{
public:
	// System.Object[] UnityEngine.Events.UnityEvent`1::m_InvokeArray
	ObjectU5BU5D_t3614634134* ___m_InvokeArray_4;

public:
	inline static int32_t get_offset_of_m_InvokeArray_4() { return static_cast<int32_t>(offsetof(UnityEvent_1_t2110227463, ___m_InvokeArray_4)); }
	inline ObjectU5BU5D_t3614634134* get_m_InvokeArray_4() const { return ___m_InvokeArray_4; }
	inline ObjectU5BU5D_t3614634134** get_address_of_m_InvokeArray_4() { return &___m_InvokeArray_4; }
	inline void set_m_InvokeArray_4(ObjectU5BU5D_t3614634134* value)
	{
		___m_InvokeArray_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_InvokeArray_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYEVENT_1_T2110227463_H
#ifndef UNITYEVENT_1_T1617761548_H
#define UNITYEVENT_1_T1617761548_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Events.UnityEvent`1<Utage.UguiNovelTextHitArea>
struct  UnityEvent_1_t1617761548  : public UnityEventBase_t828812576
{
public:
	// System.Object[] UnityEngine.Events.UnityEvent`1::m_InvokeArray
	ObjectU5BU5D_t3614634134* ___m_InvokeArray_4;

public:
	inline static int32_t get_offset_of_m_InvokeArray_4() { return static_cast<int32_t>(offsetof(UnityEvent_1_t1617761548, ___m_InvokeArray_4)); }
	inline ObjectU5BU5D_t3614634134* get_m_InvokeArray_4() const { return ___m_InvokeArray_4; }
	inline ObjectU5BU5D_t3614634134** get_address_of_m_InvokeArray_4() { return &___m_InvokeArray_4; }
	inline void set_m_InvokeArray_4(ObjectU5BU5D_t3614634134* value)
	{
		___m_InvokeArray_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_InvokeArray_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYEVENT_1_T1617761548_H
#ifndef ENUM_T2459695545_H
#define ENUM_T2459695545_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Enum
struct  Enum_t2459695545  : public ValueType_t3507792607
{
public:

public:
};

struct Enum_t2459695545_StaticFields
{
public:
	// System.Char[] System.Enum::split_char
	CharU5BU5D_t1328083999* ___split_char_0;

public:
	inline static int32_t get_offset_of_split_char_0() { return static_cast<int32_t>(offsetof(Enum_t2459695545_StaticFields, ___split_char_0)); }
	inline CharU5BU5D_t1328083999* get_split_char_0() const { return ___split_char_0; }
	inline CharU5BU5D_t1328083999** get_address_of_split_char_0() { return &___split_char_0; }
	inline void set_split_char_0(CharU5BU5D_t1328083999* value)
	{
		___split_char_0 = value;
		Il2CppCodeGenWriteBarrier((&___split_char_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t2459695545_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t2459695545_marshaled_com
{
};
#endif // ENUM_T2459695545_H
#ifndef ENUMERATOR_T3818772868_H
#define ENUMERATOR_T3818772868_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.List`1/Enumerator<Utage.MiniAnimationData/Data>
struct  Enumerator_t3818772868 
{
public:
	// System.Collections.Generic.List`1<T> System.Collections.Generic.List`1/Enumerator::l
	List_1_t4284043194 * ___l_0;
	// System.Int32 System.Collections.Generic.List`1/Enumerator::next
	int32_t ___next_1;
	// System.Int32 System.Collections.Generic.List`1/Enumerator::ver
	int32_t ___ver_2;
	// T System.Collections.Generic.List`1/Enumerator::current
	Data_t619954766 * ___current_3;

public:
	inline static int32_t get_offset_of_l_0() { return static_cast<int32_t>(offsetof(Enumerator_t3818772868, ___l_0)); }
	inline List_1_t4284043194 * get_l_0() const { return ___l_0; }
	inline List_1_t4284043194 ** get_address_of_l_0() { return &___l_0; }
	inline void set_l_0(List_1_t4284043194 * value)
	{
		___l_0 = value;
		Il2CppCodeGenWriteBarrier((&___l_0), value);
	}

	inline static int32_t get_offset_of_next_1() { return static_cast<int32_t>(offsetof(Enumerator_t3818772868, ___next_1)); }
	inline int32_t get_next_1() const { return ___next_1; }
	inline int32_t* get_address_of_next_1() { return &___next_1; }
	inline void set_next_1(int32_t value)
	{
		___next_1 = value;
	}

	inline static int32_t get_offset_of_ver_2() { return static_cast<int32_t>(offsetof(Enumerator_t3818772868, ___ver_2)); }
	inline int32_t get_ver_2() const { return ___ver_2; }
	inline int32_t* get_address_of_ver_2() { return &___ver_2; }
	inline void set_ver_2(int32_t value)
	{
		___ver_2 = value;
	}

	inline static int32_t get_offset_of_current_3() { return static_cast<int32_t>(offsetof(Enumerator_t3818772868, ___current_3)); }
	inline Data_t619954766 * get_current_3() const { return ___current_3; }
	inline Data_t619954766 ** get_address_of_current_3() { return &___current_3; }
	inline void set_current_3(Data_t619954766 * value)
	{
		___current_3 = value;
		Il2CppCodeGenWriteBarrier((&___current_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENUMERATOR_T3818772868_H
#ifndef ENUMERATOR_T933071039_H
#define ENUMERATOR_T933071039_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.List`1/Enumerator<System.String>
struct  Enumerator_t933071039 
{
public:
	// System.Collections.Generic.List`1<T> System.Collections.Generic.List`1/Enumerator::l
	List_1_t1398341365 * ___l_0;
	// System.Int32 System.Collections.Generic.List`1/Enumerator::next
	int32_t ___next_1;
	// System.Int32 System.Collections.Generic.List`1/Enumerator::ver
	int32_t ___ver_2;
	// T System.Collections.Generic.List`1/Enumerator::current
	String_t* ___current_3;

public:
	inline static int32_t get_offset_of_l_0() { return static_cast<int32_t>(offsetof(Enumerator_t933071039, ___l_0)); }
	inline List_1_t1398341365 * get_l_0() const { return ___l_0; }
	inline List_1_t1398341365 ** get_address_of_l_0() { return &___l_0; }
	inline void set_l_0(List_1_t1398341365 * value)
	{
		___l_0 = value;
		Il2CppCodeGenWriteBarrier((&___l_0), value);
	}

	inline static int32_t get_offset_of_next_1() { return static_cast<int32_t>(offsetof(Enumerator_t933071039, ___next_1)); }
	inline int32_t get_next_1() const { return ___next_1; }
	inline int32_t* get_address_of_next_1() { return &___next_1; }
	inline void set_next_1(int32_t value)
	{
		___next_1 = value;
	}

	inline static int32_t get_offset_of_ver_2() { return static_cast<int32_t>(offsetof(Enumerator_t933071039, ___ver_2)); }
	inline int32_t get_ver_2() const { return ___ver_2; }
	inline int32_t* get_address_of_ver_2() { return &___ver_2; }
	inline void set_ver_2(int32_t value)
	{
		___ver_2 = value;
	}

	inline static int32_t get_offset_of_current_3() { return static_cast<int32_t>(offsetof(Enumerator_t933071039, ___current_3)); }
	inline String_t* get_current_3() const { return ___current_3; }
	inline String_t** get_address_of_current_3() { return &___current_3; }
	inline void set_current_3(String_t* value)
	{
		___current_3 = value;
		Il2CppCodeGenWriteBarrier((&___current_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENUMERATOR_T933071039_H
#ifndef VECTOR4_T2243707581_H
#define VECTOR4_T2243707581_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector4
struct  Vector4_t2243707581 
{
public:
	// System.Single UnityEngine.Vector4::x
	float ___x_1;
	// System.Single UnityEngine.Vector4::y
	float ___y_2;
	// System.Single UnityEngine.Vector4::z
	float ___z_3;
	// System.Single UnityEngine.Vector4::w
	float ___w_4;

public:
	inline static int32_t get_offset_of_x_1() { return static_cast<int32_t>(offsetof(Vector4_t2243707581, ___x_1)); }
	inline float get_x_1() const { return ___x_1; }
	inline float* get_address_of_x_1() { return &___x_1; }
	inline void set_x_1(float value)
	{
		___x_1 = value;
	}

	inline static int32_t get_offset_of_y_2() { return static_cast<int32_t>(offsetof(Vector4_t2243707581, ___y_2)); }
	inline float get_y_2() const { return ___y_2; }
	inline float* get_address_of_y_2() { return &___y_2; }
	inline void set_y_2(float value)
	{
		___y_2 = value;
	}

	inline static int32_t get_offset_of_z_3() { return static_cast<int32_t>(offsetof(Vector4_t2243707581, ___z_3)); }
	inline float get_z_3() const { return ___z_3; }
	inline float* get_address_of_z_3() { return &___z_3; }
	inline void set_z_3(float value)
	{
		___z_3 = value;
	}

	inline static int32_t get_offset_of_w_4() { return static_cast<int32_t>(offsetof(Vector4_t2243707581, ___w_4)); }
	inline float get_w_4() const { return ___w_4; }
	inline float* get_address_of_w_4() { return &___w_4; }
	inline void set_w_4(float value)
	{
		___w_4 = value;
	}
};

struct Vector4_t2243707581_StaticFields
{
public:
	// UnityEngine.Vector4 UnityEngine.Vector4::zeroVector
	Vector4_t2243707581  ___zeroVector_5;
	// UnityEngine.Vector4 UnityEngine.Vector4::oneVector
	Vector4_t2243707581  ___oneVector_6;
	// UnityEngine.Vector4 UnityEngine.Vector4::positiveInfinityVector
	Vector4_t2243707581  ___positiveInfinityVector_7;
	// UnityEngine.Vector4 UnityEngine.Vector4::negativeInfinityVector
	Vector4_t2243707581  ___negativeInfinityVector_8;

public:
	inline static int32_t get_offset_of_zeroVector_5() { return static_cast<int32_t>(offsetof(Vector4_t2243707581_StaticFields, ___zeroVector_5)); }
	inline Vector4_t2243707581  get_zeroVector_5() const { return ___zeroVector_5; }
	inline Vector4_t2243707581 * get_address_of_zeroVector_5() { return &___zeroVector_5; }
	inline void set_zeroVector_5(Vector4_t2243707581  value)
	{
		___zeroVector_5 = value;
	}

	inline static int32_t get_offset_of_oneVector_6() { return static_cast<int32_t>(offsetof(Vector4_t2243707581_StaticFields, ___oneVector_6)); }
	inline Vector4_t2243707581  get_oneVector_6() const { return ___oneVector_6; }
	inline Vector4_t2243707581 * get_address_of_oneVector_6() { return &___oneVector_6; }
	inline void set_oneVector_6(Vector4_t2243707581  value)
	{
		___oneVector_6 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_7() { return static_cast<int32_t>(offsetof(Vector4_t2243707581_StaticFields, ___positiveInfinityVector_7)); }
	inline Vector4_t2243707581  get_positiveInfinityVector_7() const { return ___positiveInfinityVector_7; }
	inline Vector4_t2243707581 * get_address_of_positiveInfinityVector_7() { return &___positiveInfinityVector_7; }
	inline void set_positiveInfinityVector_7(Vector4_t2243707581  value)
	{
		___positiveInfinityVector_7 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_8() { return static_cast<int32_t>(offsetof(Vector4_t2243707581_StaticFields, ___negativeInfinityVector_8)); }
	inline Vector4_t2243707581  get_negativeInfinityVector_8() const { return ___negativeInfinityVector_8; }
	inline Vector4_t2243707581 * get_address_of_negativeInfinityVector_8() { return &___negativeInfinityVector_8; }
	inline void set_negativeInfinityVector_8(Vector4_t2243707581  value)
	{
		___negativeInfinityVector_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR4_T2243707581_H
#ifndef RECT_T3681755626_H
#define RECT_T3681755626_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Rect
struct  Rect_t3681755626 
{
public:
	// System.Single UnityEngine.Rect::m_XMin
	float ___m_XMin_0;
	// System.Single UnityEngine.Rect::m_YMin
	float ___m_YMin_1;
	// System.Single UnityEngine.Rect::m_Width
	float ___m_Width_2;
	// System.Single UnityEngine.Rect::m_Height
	float ___m_Height_3;

public:
	inline static int32_t get_offset_of_m_XMin_0() { return static_cast<int32_t>(offsetof(Rect_t3681755626, ___m_XMin_0)); }
	inline float get_m_XMin_0() const { return ___m_XMin_0; }
	inline float* get_address_of_m_XMin_0() { return &___m_XMin_0; }
	inline void set_m_XMin_0(float value)
	{
		___m_XMin_0 = value;
	}

	inline static int32_t get_offset_of_m_YMin_1() { return static_cast<int32_t>(offsetof(Rect_t3681755626, ___m_YMin_1)); }
	inline float get_m_YMin_1() const { return ___m_YMin_1; }
	inline float* get_address_of_m_YMin_1() { return &___m_YMin_1; }
	inline void set_m_YMin_1(float value)
	{
		___m_YMin_1 = value;
	}

	inline static int32_t get_offset_of_m_Width_2() { return static_cast<int32_t>(offsetof(Rect_t3681755626, ___m_Width_2)); }
	inline float get_m_Width_2() const { return ___m_Width_2; }
	inline float* get_address_of_m_Width_2() { return &___m_Width_2; }
	inline void set_m_Width_2(float value)
	{
		___m_Width_2 = value;
	}

	inline static int32_t get_offset_of_m_Height_3() { return static_cast<int32_t>(offsetof(Rect_t3681755626, ___m_Height_3)); }
	inline float get_m_Height_3() const { return ___m_Height_3; }
	inline float* get_address_of_m_Height_3() { return &___m_Height_3; }
	inline void set_m_Height_3(float value)
	{
		___m_Height_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RECT_T3681755626_H
#ifndef COLOR_T2020392075_H
#define COLOR_T2020392075_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Color
struct  Color_t2020392075 
{
public:
	// System.Single UnityEngine.Color::r
	float ___r_0;
	// System.Single UnityEngine.Color::g
	float ___g_1;
	// System.Single UnityEngine.Color::b
	float ___b_2;
	// System.Single UnityEngine.Color::a
	float ___a_3;

public:
	inline static int32_t get_offset_of_r_0() { return static_cast<int32_t>(offsetof(Color_t2020392075, ___r_0)); }
	inline float get_r_0() const { return ___r_0; }
	inline float* get_address_of_r_0() { return &___r_0; }
	inline void set_r_0(float value)
	{
		___r_0 = value;
	}

	inline static int32_t get_offset_of_g_1() { return static_cast<int32_t>(offsetof(Color_t2020392075, ___g_1)); }
	inline float get_g_1() const { return ___g_1; }
	inline float* get_address_of_g_1() { return &___g_1; }
	inline void set_g_1(float value)
	{
		___g_1 = value;
	}

	inline static int32_t get_offset_of_b_2() { return static_cast<int32_t>(offsetof(Color_t2020392075, ___b_2)); }
	inline float get_b_2() const { return ___b_2; }
	inline float* get_address_of_b_2() { return &___b_2; }
	inline void set_b_2(float value)
	{
		___b_2 = value;
	}

	inline static int32_t get_offset_of_a_3() { return static_cast<int32_t>(offsetof(Color_t2020392075, ___a_3)); }
	inline float get_a_3() const { return ___a_3; }
	inline float* get_address_of_a_3() { return &___a_3; }
	inline void set_a_3(float value)
	{
		___a_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLOR_T2020392075_H
#ifndef VECTOR2_T2243707579_H
#define VECTOR2_T2243707579_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector2
struct  Vector2_t2243707579 
{
public:
	// System.Single UnityEngine.Vector2::x
	float ___x_0;
	// System.Single UnityEngine.Vector2::y
	float ___y_1;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Vector2_t2243707579, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Vector2_t2243707579, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}
};

struct Vector2_t2243707579_StaticFields
{
public:
	// UnityEngine.Vector2 UnityEngine.Vector2::zeroVector
	Vector2_t2243707579  ___zeroVector_2;
	// UnityEngine.Vector2 UnityEngine.Vector2::oneVector
	Vector2_t2243707579  ___oneVector_3;
	// UnityEngine.Vector2 UnityEngine.Vector2::upVector
	Vector2_t2243707579  ___upVector_4;
	// UnityEngine.Vector2 UnityEngine.Vector2::downVector
	Vector2_t2243707579  ___downVector_5;
	// UnityEngine.Vector2 UnityEngine.Vector2::leftVector
	Vector2_t2243707579  ___leftVector_6;
	// UnityEngine.Vector2 UnityEngine.Vector2::rightVector
	Vector2_t2243707579  ___rightVector_7;
	// UnityEngine.Vector2 UnityEngine.Vector2::positiveInfinityVector
	Vector2_t2243707579  ___positiveInfinityVector_8;
	// UnityEngine.Vector2 UnityEngine.Vector2::negativeInfinityVector
	Vector2_t2243707579  ___negativeInfinityVector_9;

public:
	inline static int32_t get_offset_of_zeroVector_2() { return static_cast<int32_t>(offsetof(Vector2_t2243707579_StaticFields, ___zeroVector_2)); }
	inline Vector2_t2243707579  get_zeroVector_2() const { return ___zeroVector_2; }
	inline Vector2_t2243707579 * get_address_of_zeroVector_2() { return &___zeroVector_2; }
	inline void set_zeroVector_2(Vector2_t2243707579  value)
	{
		___zeroVector_2 = value;
	}

	inline static int32_t get_offset_of_oneVector_3() { return static_cast<int32_t>(offsetof(Vector2_t2243707579_StaticFields, ___oneVector_3)); }
	inline Vector2_t2243707579  get_oneVector_3() const { return ___oneVector_3; }
	inline Vector2_t2243707579 * get_address_of_oneVector_3() { return &___oneVector_3; }
	inline void set_oneVector_3(Vector2_t2243707579  value)
	{
		___oneVector_3 = value;
	}

	inline static int32_t get_offset_of_upVector_4() { return static_cast<int32_t>(offsetof(Vector2_t2243707579_StaticFields, ___upVector_4)); }
	inline Vector2_t2243707579  get_upVector_4() const { return ___upVector_4; }
	inline Vector2_t2243707579 * get_address_of_upVector_4() { return &___upVector_4; }
	inline void set_upVector_4(Vector2_t2243707579  value)
	{
		___upVector_4 = value;
	}

	inline static int32_t get_offset_of_downVector_5() { return static_cast<int32_t>(offsetof(Vector2_t2243707579_StaticFields, ___downVector_5)); }
	inline Vector2_t2243707579  get_downVector_5() const { return ___downVector_5; }
	inline Vector2_t2243707579 * get_address_of_downVector_5() { return &___downVector_5; }
	inline void set_downVector_5(Vector2_t2243707579  value)
	{
		___downVector_5 = value;
	}

	inline static int32_t get_offset_of_leftVector_6() { return static_cast<int32_t>(offsetof(Vector2_t2243707579_StaticFields, ___leftVector_6)); }
	inline Vector2_t2243707579  get_leftVector_6() const { return ___leftVector_6; }
	inline Vector2_t2243707579 * get_address_of_leftVector_6() { return &___leftVector_6; }
	inline void set_leftVector_6(Vector2_t2243707579  value)
	{
		___leftVector_6 = value;
	}

	inline static int32_t get_offset_of_rightVector_7() { return static_cast<int32_t>(offsetof(Vector2_t2243707579_StaticFields, ___rightVector_7)); }
	inline Vector2_t2243707579  get_rightVector_7() const { return ___rightVector_7; }
	inline Vector2_t2243707579 * get_address_of_rightVector_7() { return &___rightVector_7; }
	inline void set_rightVector_7(Vector2_t2243707579  value)
	{
		___rightVector_7 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_8() { return static_cast<int32_t>(offsetof(Vector2_t2243707579_StaticFields, ___positiveInfinityVector_8)); }
	inline Vector2_t2243707579  get_positiveInfinityVector_8() const { return ___positiveInfinityVector_8; }
	inline Vector2_t2243707579 * get_address_of_positiveInfinityVector_8() { return &___positiveInfinityVector_8; }
	inline void set_positiveInfinityVector_8(Vector2_t2243707579  value)
	{
		___positiveInfinityVector_8 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_9() { return static_cast<int32_t>(offsetof(Vector2_t2243707579_StaticFields, ___negativeInfinityVector_9)); }
	inline Vector2_t2243707579  get_negativeInfinityVector_9() const { return ___negativeInfinityVector_9; }
	inline Vector2_t2243707579 * get_address_of_negativeInfinityVector_9() { return &___negativeInfinityVector_9; }
	inline void set_negativeInfinityVector_9(Vector2_t2243707579  value)
	{
		___negativeInfinityVector_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR2_T2243707579_H
#ifndef INTPTR_T_H
#define INTPTR_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTPTR_T_H
#ifndef COLORUTIL_T1432289115_H
#define COLORUTIL_T1432289115_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.ColorUtil
struct  ColorUtil_t1432289115  : public RuntimeObject
{
public:

public:
};

struct ColorUtil_t1432289115_StaticFields
{
public:
	// UnityEngine.Color Utage.ColorUtil::Aqua
	Color_t2020392075  ___Aqua_0;
	// UnityEngine.Color Utage.ColorUtil::Black
	Color_t2020392075  ___Black_1;
	// UnityEngine.Color Utage.ColorUtil::Blue
	Color_t2020392075  ___Blue_2;
	// UnityEngine.Color Utage.ColorUtil::Brown
	Color_t2020392075  ___Brown_3;
	// UnityEngine.Color Utage.ColorUtil::Cyan
	Color_t2020392075  ___Cyan_4;
	// UnityEngine.Color Utage.ColorUtil::Darkblue
	Color_t2020392075  ___Darkblue_5;
	// UnityEngine.Color Utage.ColorUtil::Fuchsia
	Color_t2020392075  ___Fuchsia_6;
	// UnityEngine.Color Utage.ColorUtil::Green
	Color_t2020392075  ___Green_7;
	// UnityEngine.Color Utage.ColorUtil::Grey
	Color_t2020392075  ___Grey_8;
	// UnityEngine.Color Utage.ColorUtil::Lightblue
	Color_t2020392075  ___Lightblue_9;
	// UnityEngine.Color Utage.ColorUtil::Lime
	Color_t2020392075  ___Lime_10;
	// UnityEngine.Color Utage.ColorUtil::Magenta
	Color_t2020392075  ___Magenta_11;
	// UnityEngine.Color Utage.ColorUtil::Maroon
	Color_t2020392075  ___Maroon_12;
	// UnityEngine.Color Utage.ColorUtil::Navy
	Color_t2020392075  ___Navy_13;
	// UnityEngine.Color Utage.ColorUtil::Olive
	Color_t2020392075  ___Olive_14;
	// UnityEngine.Color Utage.ColorUtil::Orange
	Color_t2020392075  ___Orange_15;
	// UnityEngine.Color Utage.ColorUtil::Purple
	Color_t2020392075  ___Purple_16;
	// UnityEngine.Color Utage.ColorUtil::Red
	Color_t2020392075  ___Red_17;
	// UnityEngine.Color Utage.ColorUtil::Silver
	Color_t2020392075  ___Silver_18;
	// UnityEngine.Color Utage.ColorUtil::Teal
	Color_t2020392075  ___Teal_19;
	// UnityEngine.Color Utage.ColorUtil::White
	Color_t2020392075  ___White_20;
	// UnityEngine.Color Utage.ColorUtil::Yellow
	Color_t2020392075  ___Yellow_21;
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> Utage.ColorUtil::<>f__switch$mapB
	Dictionary_2_t3986656710 * ___U3CU3Ef__switchU24mapB_22;

public:
	inline static int32_t get_offset_of_Aqua_0() { return static_cast<int32_t>(offsetof(ColorUtil_t1432289115_StaticFields, ___Aqua_0)); }
	inline Color_t2020392075  get_Aqua_0() const { return ___Aqua_0; }
	inline Color_t2020392075 * get_address_of_Aqua_0() { return &___Aqua_0; }
	inline void set_Aqua_0(Color_t2020392075  value)
	{
		___Aqua_0 = value;
	}

	inline static int32_t get_offset_of_Black_1() { return static_cast<int32_t>(offsetof(ColorUtil_t1432289115_StaticFields, ___Black_1)); }
	inline Color_t2020392075  get_Black_1() const { return ___Black_1; }
	inline Color_t2020392075 * get_address_of_Black_1() { return &___Black_1; }
	inline void set_Black_1(Color_t2020392075  value)
	{
		___Black_1 = value;
	}

	inline static int32_t get_offset_of_Blue_2() { return static_cast<int32_t>(offsetof(ColorUtil_t1432289115_StaticFields, ___Blue_2)); }
	inline Color_t2020392075  get_Blue_2() const { return ___Blue_2; }
	inline Color_t2020392075 * get_address_of_Blue_2() { return &___Blue_2; }
	inline void set_Blue_2(Color_t2020392075  value)
	{
		___Blue_2 = value;
	}

	inline static int32_t get_offset_of_Brown_3() { return static_cast<int32_t>(offsetof(ColorUtil_t1432289115_StaticFields, ___Brown_3)); }
	inline Color_t2020392075  get_Brown_3() const { return ___Brown_3; }
	inline Color_t2020392075 * get_address_of_Brown_3() { return &___Brown_3; }
	inline void set_Brown_3(Color_t2020392075  value)
	{
		___Brown_3 = value;
	}

	inline static int32_t get_offset_of_Cyan_4() { return static_cast<int32_t>(offsetof(ColorUtil_t1432289115_StaticFields, ___Cyan_4)); }
	inline Color_t2020392075  get_Cyan_4() const { return ___Cyan_4; }
	inline Color_t2020392075 * get_address_of_Cyan_4() { return &___Cyan_4; }
	inline void set_Cyan_4(Color_t2020392075  value)
	{
		___Cyan_4 = value;
	}

	inline static int32_t get_offset_of_Darkblue_5() { return static_cast<int32_t>(offsetof(ColorUtil_t1432289115_StaticFields, ___Darkblue_5)); }
	inline Color_t2020392075  get_Darkblue_5() const { return ___Darkblue_5; }
	inline Color_t2020392075 * get_address_of_Darkblue_5() { return &___Darkblue_5; }
	inline void set_Darkblue_5(Color_t2020392075  value)
	{
		___Darkblue_5 = value;
	}

	inline static int32_t get_offset_of_Fuchsia_6() { return static_cast<int32_t>(offsetof(ColorUtil_t1432289115_StaticFields, ___Fuchsia_6)); }
	inline Color_t2020392075  get_Fuchsia_6() const { return ___Fuchsia_6; }
	inline Color_t2020392075 * get_address_of_Fuchsia_6() { return &___Fuchsia_6; }
	inline void set_Fuchsia_6(Color_t2020392075  value)
	{
		___Fuchsia_6 = value;
	}

	inline static int32_t get_offset_of_Green_7() { return static_cast<int32_t>(offsetof(ColorUtil_t1432289115_StaticFields, ___Green_7)); }
	inline Color_t2020392075  get_Green_7() const { return ___Green_7; }
	inline Color_t2020392075 * get_address_of_Green_7() { return &___Green_7; }
	inline void set_Green_7(Color_t2020392075  value)
	{
		___Green_7 = value;
	}

	inline static int32_t get_offset_of_Grey_8() { return static_cast<int32_t>(offsetof(ColorUtil_t1432289115_StaticFields, ___Grey_8)); }
	inline Color_t2020392075  get_Grey_8() const { return ___Grey_8; }
	inline Color_t2020392075 * get_address_of_Grey_8() { return &___Grey_8; }
	inline void set_Grey_8(Color_t2020392075  value)
	{
		___Grey_8 = value;
	}

	inline static int32_t get_offset_of_Lightblue_9() { return static_cast<int32_t>(offsetof(ColorUtil_t1432289115_StaticFields, ___Lightblue_9)); }
	inline Color_t2020392075  get_Lightblue_9() const { return ___Lightblue_9; }
	inline Color_t2020392075 * get_address_of_Lightblue_9() { return &___Lightblue_9; }
	inline void set_Lightblue_9(Color_t2020392075  value)
	{
		___Lightblue_9 = value;
	}

	inline static int32_t get_offset_of_Lime_10() { return static_cast<int32_t>(offsetof(ColorUtil_t1432289115_StaticFields, ___Lime_10)); }
	inline Color_t2020392075  get_Lime_10() const { return ___Lime_10; }
	inline Color_t2020392075 * get_address_of_Lime_10() { return &___Lime_10; }
	inline void set_Lime_10(Color_t2020392075  value)
	{
		___Lime_10 = value;
	}

	inline static int32_t get_offset_of_Magenta_11() { return static_cast<int32_t>(offsetof(ColorUtil_t1432289115_StaticFields, ___Magenta_11)); }
	inline Color_t2020392075  get_Magenta_11() const { return ___Magenta_11; }
	inline Color_t2020392075 * get_address_of_Magenta_11() { return &___Magenta_11; }
	inline void set_Magenta_11(Color_t2020392075  value)
	{
		___Magenta_11 = value;
	}

	inline static int32_t get_offset_of_Maroon_12() { return static_cast<int32_t>(offsetof(ColorUtil_t1432289115_StaticFields, ___Maroon_12)); }
	inline Color_t2020392075  get_Maroon_12() const { return ___Maroon_12; }
	inline Color_t2020392075 * get_address_of_Maroon_12() { return &___Maroon_12; }
	inline void set_Maroon_12(Color_t2020392075  value)
	{
		___Maroon_12 = value;
	}

	inline static int32_t get_offset_of_Navy_13() { return static_cast<int32_t>(offsetof(ColorUtil_t1432289115_StaticFields, ___Navy_13)); }
	inline Color_t2020392075  get_Navy_13() const { return ___Navy_13; }
	inline Color_t2020392075 * get_address_of_Navy_13() { return &___Navy_13; }
	inline void set_Navy_13(Color_t2020392075  value)
	{
		___Navy_13 = value;
	}

	inline static int32_t get_offset_of_Olive_14() { return static_cast<int32_t>(offsetof(ColorUtil_t1432289115_StaticFields, ___Olive_14)); }
	inline Color_t2020392075  get_Olive_14() const { return ___Olive_14; }
	inline Color_t2020392075 * get_address_of_Olive_14() { return &___Olive_14; }
	inline void set_Olive_14(Color_t2020392075  value)
	{
		___Olive_14 = value;
	}

	inline static int32_t get_offset_of_Orange_15() { return static_cast<int32_t>(offsetof(ColorUtil_t1432289115_StaticFields, ___Orange_15)); }
	inline Color_t2020392075  get_Orange_15() const { return ___Orange_15; }
	inline Color_t2020392075 * get_address_of_Orange_15() { return &___Orange_15; }
	inline void set_Orange_15(Color_t2020392075  value)
	{
		___Orange_15 = value;
	}

	inline static int32_t get_offset_of_Purple_16() { return static_cast<int32_t>(offsetof(ColorUtil_t1432289115_StaticFields, ___Purple_16)); }
	inline Color_t2020392075  get_Purple_16() const { return ___Purple_16; }
	inline Color_t2020392075 * get_address_of_Purple_16() { return &___Purple_16; }
	inline void set_Purple_16(Color_t2020392075  value)
	{
		___Purple_16 = value;
	}

	inline static int32_t get_offset_of_Red_17() { return static_cast<int32_t>(offsetof(ColorUtil_t1432289115_StaticFields, ___Red_17)); }
	inline Color_t2020392075  get_Red_17() const { return ___Red_17; }
	inline Color_t2020392075 * get_address_of_Red_17() { return &___Red_17; }
	inline void set_Red_17(Color_t2020392075  value)
	{
		___Red_17 = value;
	}

	inline static int32_t get_offset_of_Silver_18() { return static_cast<int32_t>(offsetof(ColorUtil_t1432289115_StaticFields, ___Silver_18)); }
	inline Color_t2020392075  get_Silver_18() const { return ___Silver_18; }
	inline Color_t2020392075 * get_address_of_Silver_18() { return &___Silver_18; }
	inline void set_Silver_18(Color_t2020392075  value)
	{
		___Silver_18 = value;
	}

	inline static int32_t get_offset_of_Teal_19() { return static_cast<int32_t>(offsetof(ColorUtil_t1432289115_StaticFields, ___Teal_19)); }
	inline Color_t2020392075  get_Teal_19() const { return ___Teal_19; }
	inline Color_t2020392075 * get_address_of_Teal_19() { return &___Teal_19; }
	inline void set_Teal_19(Color_t2020392075  value)
	{
		___Teal_19 = value;
	}

	inline static int32_t get_offset_of_White_20() { return static_cast<int32_t>(offsetof(ColorUtil_t1432289115_StaticFields, ___White_20)); }
	inline Color_t2020392075  get_White_20() const { return ___White_20; }
	inline Color_t2020392075 * get_address_of_White_20() { return &___White_20; }
	inline void set_White_20(Color_t2020392075  value)
	{
		___White_20 = value;
	}

	inline static int32_t get_offset_of_Yellow_21() { return static_cast<int32_t>(offsetof(ColorUtil_t1432289115_StaticFields, ___Yellow_21)); }
	inline Color_t2020392075  get_Yellow_21() const { return ___Yellow_21; }
	inline Color_t2020392075 * get_address_of_Yellow_21() { return &___Yellow_21; }
	inline void set_Yellow_21(Color_t2020392075  value)
	{
		___Yellow_21 = value;
	}

	inline static int32_t get_offset_of_U3CU3Ef__switchU24mapB_22() { return static_cast<int32_t>(offsetof(ColorUtil_t1432289115_StaticFields, ___U3CU3Ef__switchU24mapB_22)); }
	inline Dictionary_2_t3986656710 * get_U3CU3Ef__switchU24mapB_22() const { return ___U3CU3Ef__switchU24mapB_22; }
	inline Dictionary_2_t3986656710 ** get_address_of_U3CU3Ef__switchU24mapB_22() { return &___U3CU3Ef__switchU24mapB_22; }
	inline void set_U3CU3Ef__switchU24mapB_22(Dictionary_2_t3986656710 * value)
	{
		___U3CU3Ef__switchU24mapB_22 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__switchU24mapB_22), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLORUTIL_T1432289115_H
#ifndef ALIGNMENT_T3342318601_H
#define ALIGNMENT_T3342318601_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.Alignment
struct  Alignment_t3342318601 
{
public:
	// System.Int32 Utage.Alignment::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(Alignment_t3342318601, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ALIGNMENT_T3342318601_H
#ifndef LIPSYNCHTYPE_T1469092900_H
#define LIPSYNCHTYPE_T1469092900_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.LipSynchType
struct  LipSynchType_t1469092900 
{
public:
	// System.Int32 Utage.LipSynchType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(LipSynchType_t1469092900, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LIPSYNCHTYPE_T1469092900_H
#ifndef LIPSYNCHMODE_T1462187095_H
#define LIPSYNCHMODE_T1462187095_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.LipSynchMode
struct  LipSynchMode_t1462187095 
{
public:
	// System.Int32 Utage.LipSynchMode::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(LipSynchMode_t1462187095, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LIPSYNCHMODE_T1462187095_H
#ifndef LIPSYNCHEVENT_T825925384_H
#define LIPSYNCHEVENT_T825925384_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.LipSynchEvent
struct  LipSynchEvent_t825925384  : public UnityEvent_1_t2414510360
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LIPSYNCHEVENT_T825925384_H
#ifndef STATUS_T2577383376_H
#define STATUS_T2577383376_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.UguiView/Status
struct  Status_t2577383376 
{
public:
	// System.Int32 Utage.UguiView/Status::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(Status_t2577383376, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STATUS_T2577383376_H
#ifndef ALIGNDIRECTION_T4010138607_H
#define ALIGNDIRECTION_T4010138607_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.UguiHorizontalAlignGroup/AlignDirection
struct  AlignDirection_t4010138607 
{
public:
	// System.Int32 Utage.UguiHorizontalAlignGroup/AlignDirection::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(AlignDirection_t4010138607, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ALIGNDIRECTION_T4010138607_H
#ifndef OBJECT_T1021602117_H
#define OBJECT_T1021602117_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Object
struct  Object_t1021602117  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Object::m_CachedPtr
	intptr_t ___m_CachedPtr_0;

public:
	inline static int32_t get_offset_of_m_CachedPtr_0() { return static_cast<int32_t>(offsetof(Object_t1021602117, ___m_CachedPtr_0)); }
	inline intptr_t get_m_CachedPtr_0() const { return ___m_CachedPtr_0; }
	inline intptr_t* get_address_of_m_CachedPtr_0() { return &___m_CachedPtr_0; }
	inline void set_m_CachedPtr_0(intptr_t value)
	{
		___m_CachedPtr_0 = value;
	}
};

struct Object_t1021602117_StaticFields
{
public:
	// System.Int32 UnityEngine.Object::OffsetOfInstanceIDInCPlusPlusObject
	int32_t ___OffsetOfInstanceIDInCPlusPlusObject_1;

public:
	inline static int32_t get_offset_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return static_cast<int32_t>(offsetof(Object_t1021602117_StaticFields, ___OffsetOfInstanceIDInCPlusPlusObject_1)); }
	inline int32_t get_OffsetOfInstanceIDInCPlusPlusObject_1() const { return ___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline int32_t* get_address_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return &___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline void set_OffsetOfInstanceIDInCPlusPlusObject_1(int32_t value)
	{
		___OffsetOfInstanceIDInCPlusPlusObject_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.Object
struct Object_t1021602117_marshaled_pinvoke
{
	intptr_t ___m_CachedPtr_0;
};
// Native definition for COM marshalling of UnityEngine.Object
struct Object_t1021602117_marshaled_com
{
	intptr_t ___m_CachedPtr_0;
};
#endif // OBJECT_T1021602117_H
#ifndef UGUITABBUTTONGROUPEVENT_T4293552753_H
#define UGUITABBUTTONGROUPEVENT_T4293552753_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.UguiToggleGroupIndexed/UguiTabButtonGroupEvent
struct  UguiTabButtonGroupEvent_t4293552753  : public UnityEvent_1_t2110227463
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UGUITABBUTTONGROUPEVENT_T4293552753_H
#ifndef TYPE_T1945770602_H
#define TYPE_T1945770602_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.UguiListView/Type
struct  Type_t1945770602 
{
public:
	// System.Int32 Utage.UguiListView/Type::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(Type_t1945770602, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TYPE_T1945770602_H
#ifndef ALIGNDIRECTION_T1523984715_H
#define ALIGNDIRECTION_T1523984715_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.UguiVerticalAlignGroup/AlignDirection
struct  AlignDirection_t1523984715 
{
public:
	// System.Int32 Utage.UguiVerticalAlignGroup/AlignDirection::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(AlignDirection_t1523984715, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ALIGNDIRECTION_T1523984715_H
#ifndef U3CCOUPDATELIPSYNCU3EC__ITERATOR0_T137802369_H
#define U3CCOUPDATELIPSYNCU3EC__ITERATOR0_T137802369_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.LipSynchDicing/<CoUpdateLipSync>c__Iterator0
struct  U3CCoUpdateLipSyncU3Ec__Iterator0_t137802369  : public RuntimeObject
{
public:
	// System.String Utage.LipSynchDicing/<CoUpdateLipSync>c__Iterator0::<pattern>__1
	String_t* ___U3CpatternU3E__1_0;
	// System.Collections.Generic.List`1/Enumerator<Utage.MiniAnimationData/Data> Utage.LipSynchDicing/<CoUpdateLipSync>c__Iterator0::$locvar0
	Enumerator_t3818772868  ___U24locvar0_1;
	// Utage.MiniAnimationData/Data Utage.LipSynchDicing/<CoUpdateLipSync>c__Iterator0::<data>__2
	Data_t619954766 * ___U3CdataU3E__2_2;
	// Utage.LipSynchDicing Utage.LipSynchDicing/<CoUpdateLipSync>c__Iterator0::$this
	LipSynchDicing_t1869722688 * ___U24this_3;
	// System.Object Utage.LipSynchDicing/<CoUpdateLipSync>c__Iterator0::$current
	RuntimeObject * ___U24current_4;
	// System.Boolean Utage.LipSynchDicing/<CoUpdateLipSync>c__Iterator0::$disposing
	bool ___U24disposing_5;
	// System.Int32 Utage.LipSynchDicing/<CoUpdateLipSync>c__Iterator0::$PC
	int32_t ___U24PC_6;

public:
	inline static int32_t get_offset_of_U3CpatternU3E__1_0() { return static_cast<int32_t>(offsetof(U3CCoUpdateLipSyncU3Ec__Iterator0_t137802369, ___U3CpatternU3E__1_0)); }
	inline String_t* get_U3CpatternU3E__1_0() const { return ___U3CpatternU3E__1_0; }
	inline String_t** get_address_of_U3CpatternU3E__1_0() { return &___U3CpatternU3E__1_0; }
	inline void set_U3CpatternU3E__1_0(String_t* value)
	{
		___U3CpatternU3E__1_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CpatternU3E__1_0), value);
	}

	inline static int32_t get_offset_of_U24locvar0_1() { return static_cast<int32_t>(offsetof(U3CCoUpdateLipSyncU3Ec__Iterator0_t137802369, ___U24locvar0_1)); }
	inline Enumerator_t3818772868  get_U24locvar0_1() const { return ___U24locvar0_1; }
	inline Enumerator_t3818772868 * get_address_of_U24locvar0_1() { return &___U24locvar0_1; }
	inline void set_U24locvar0_1(Enumerator_t3818772868  value)
	{
		___U24locvar0_1 = value;
	}

	inline static int32_t get_offset_of_U3CdataU3E__2_2() { return static_cast<int32_t>(offsetof(U3CCoUpdateLipSyncU3Ec__Iterator0_t137802369, ___U3CdataU3E__2_2)); }
	inline Data_t619954766 * get_U3CdataU3E__2_2() const { return ___U3CdataU3E__2_2; }
	inline Data_t619954766 ** get_address_of_U3CdataU3E__2_2() { return &___U3CdataU3E__2_2; }
	inline void set_U3CdataU3E__2_2(Data_t619954766 * value)
	{
		___U3CdataU3E__2_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CdataU3E__2_2), value);
	}

	inline static int32_t get_offset_of_U24this_3() { return static_cast<int32_t>(offsetof(U3CCoUpdateLipSyncU3Ec__Iterator0_t137802369, ___U24this_3)); }
	inline LipSynchDicing_t1869722688 * get_U24this_3() const { return ___U24this_3; }
	inline LipSynchDicing_t1869722688 ** get_address_of_U24this_3() { return &___U24this_3; }
	inline void set_U24this_3(LipSynchDicing_t1869722688 * value)
	{
		___U24this_3 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_3), value);
	}

	inline static int32_t get_offset_of_U24current_4() { return static_cast<int32_t>(offsetof(U3CCoUpdateLipSyncU3Ec__Iterator0_t137802369, ___U24current_4)); }
	inline RuntimeObject * get_U24current_4() const { return ___U24current_4; }
	inline RuntimeObject ** get_address_of_U24current_4() { return &___U24current_4; }
	inline void set_U24current_4(RuntimeObject * value)
	{
		___U24current_4 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_4), value);
	}

	inline static int32_t get_offset_of_U24disposing_5() { return static_cast<int32_t>(offsetof(U3CCoUpdateLipSyncU3Ec__Iterator0_t137802369, ___U24disposing_5)); }
	inline bool get_U24disposing_5() const { return ___U24disposing_5; }
	inline bool* get_address_of_U24disposing_5() { return &___U24disposing_5; }
	inline void set_U24disposing_5(bool value)
	{
		___U24disposing_5 = value;
	}

	inline static int32_t get_offset_of_U24PC_6() { return static_cast<int32_t>(offsetof(U3CCoUpdateLipSyncU3Ec__Iterator0_t137802369, ___U24PC_6)); }
	inline int32_t get_U24PC_6() const { return ___U24PC_6; }
	inline int32_t* get_address_of_U24PC_6() { return &___U24PC_6; }
	inline void set_U24PC_6(int32_t value)
	{
		___U24PC_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CCOUPDATELIPSYNCU3EC__ITERATOR0_T137802369_H
#ifndef U3CCOUPDATELIPSYNCU3EC__ITERATOR0_T1939142126_H
#define U3CCOUPDATELIPSYNCU3EC__ITERATOR0_T1939142126_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.LipSynchAvatar/<CoUpdateLipSync>c__Iterator0
struct  U3CCoUpdateLipSyncU3Ec__Iterator0_t1939142126  : public RuntimeObject
{
public:
	// System.String Utage.LipSynchAvatar/<CoUpdateLipSync>c__Iterator0::<pattern>__1
	String_t* ___U3CpatternU3E__1_0;
	// System.Collections.Generic.List`1/Enumerator<Utage.MiniAnimationData/Data> Utage.LipSynchAvatar/<CoUpdateLipSync>c__Iterator0::$locvar0
	Enumerator_t3818772868  ___U24locvar0_1;
	// Utage.MiniAnimationData/Data Utage.LipSynchAvatar/<CoUpdateLipSync>c__Iterator0::<data>__2
	Data_t619954766 * ___U3CdataU3E__2_2;
	// Utage.LipSynchAvatar Utage.LipSynchAvatar/<CoUpdateLipSync>c__Iterator0::$this
	LipSynchAvatar_t872933325 * ___U24this_3;
	// System.Object Utage.LipSynchAvatar/<CoUpdateLipSync>c__Iterator0::$current
	RuntimeObject * ___U24current_4;
	// System.Boolean Utage.LipSynchAvatar/<CoUpdateLipSync>c__Iterator0::$disposing
	bool ___U24disposing_5;
	// System.Int32 Utage.LipSynchAvatar/<CoUpdateLipSync>c__Iterator0::$PC
	int32_t ___U24PC_6;

public:
	inline static int32_t get_offset_of_U3CpatternU3E__1_0() { return static_cast<int32_t>(offsetof(U3CCoUpdateLipSyncU3Ec__Iterator0_t1939142126, ___U3CpatternU3E__1_0)); }
	inline String_t* get_U3CpatternU3E__1_0() const { return ___U3CpatternU3E__1_0; }
	inline String_t** get_address_of_U3CpatternU3E__1_0() { return &___U3CpatternU3E__1_0; }
	inline void set_U3CpatternU3E__1_0(String_t* value)
	{
		___U3CpatternU3E__1_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CpatternU3E__1_0), value);
	}

	inline static int32_t get_offset_of_U24locvar0_1() { return static_cast<int32_t>(offsetof(U3CCoUpdateLipSyncU3Ec__Iterator0_t1939142126, ___U24locvar0_1)); }
	inline Enumerator_t3818772868  get_U24locvar0_1() const { return ___U24locvar0_1; }
	inline Enumerator_t3818772868 * get_address_of_U24locvar0_1() { return &___U24locvar0_1; }
	inline void set_U24locvar0_1(Enumerator_t3818772868  value)
	{
		___U24locvar0_1 = value;
	}

	inline static int32_t get_offset_of_U3CdataU3E__2_2() { return static_cast<int32_t>(offsetof(U3CCoUpdateLipSyncU3Ec__Iterator0_t1939142126, ___U3CdataU3E__2_2)); }
	inline Data_t619954766 * get_U3CdataU3E__2_2() const { return ___U3CdataU3E__2_2; }
	inline Data_t619954766 ** get_address_of_U3CdataU3E__2_2() { return &___U3CdataU3E__2_2; }
	inline void set_U3CdataU3E__2_2(Data_t619954766 * value)
	{
		___U3CdataU3E__2_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CdataU3E__2_2), value);
	}

	inline static int32_t get_offset_of_U24this_3() { return static_cast<int32_t>(offsetof(U3CCoUpdateLipSyncU3Ec__Iterator0_t1939142126, ___U24this_3)); }
	inline LipSynchAvatar_t872933325 * get_U24this_3() const { return ___U24this_3; }
	inline LipSynchAvatar_t872933325 ** get_address_of_U24this_3() { return &___U24this_3; }
	inline void set_U24this_3(LipSynchAvatar_t872933325 * value)
	{
		___U24this_3 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_3), value);
	}

	inline static int32_t get_offset_of_U24current_4() { return static_cast<int32_t>(offsetof(U3CCoUpdateLipSyncU3Ec__Iterator0_t1939142126, ___U24current_4)); }
	inline RuntimeObject * get_U24current_4() const { return ___U24current_4; }
	inline RuntimeObject ** get_address_of_U24current_4() { return &___U24current_4; }
	inline void set_U24current_4(RuntimeObject * value)
	{
		___U24current_4 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_4), value);
	}

	inline static int32_t get_offset_of_U24disposing_5() { return static_cast<int32_t>(offsetof(U3CCoUpdateLipSyncU3Ec__Iterator0_t1939142126, ___U24disposing_5)); }
	inline bool get_U24disposing_5() const { return ___U24disposing_5; }
	inline bool* get_address_of_U24disposing_5() { return &___U24disposing_5; }
	inline void set_U24disposing_5(bool value)
	{
		___U24disposing_5 = value;
	}

	inline static int32_t get_offset_of_U24PC_6() { return static_cast<int32_t>(offsetof(U3CCoUpdateLipSyncU3Ec__Iterator0_t1939142126, ___U24PC_6)); }
	inline int32_t get_U24PC_6() const { return ___U24PC_6; }
	inline int32_t* get_address_of_U24PC_6() { return &___U24PC_6; }
	inline void set_U24PC_6(int32_t value)
	{
		___U24PC_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CCOUPDATELIPSYNCU3EC__ITERATOR0_T1939142126_H
#ifndef FONTSTYLE_T2764949590_H
#define FONTSTYLE_T2764949590_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.FontStyle
struct  FontStyle_t2764949590 
{
public:
	// System.Int32 UnityEngine.FontStyle::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(FontStyle_t2764949590, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FONTSTYLE_T2764949590_H
#ifndef HITEVENTTYPE_T2243784412_H
#define HITEVENTTYPE_T2243784412_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.CharData/HitEventType
struct  HitEventType_t2243784412 
{
public:
	// System.Int32 Utage.CharData/HitEventType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(HitEventType_t2243784412, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HITEVENTTYPE_T2243784412_H
#ifndef U3CCOPLAYONCEU3EC__ITERATOR1_T201519776_H
#define U3CCOPLAYONCEU3EC__ITERATOR1_T201519776_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.DicingAnimation/<CoPlayOnce>c__Iterator1
struct  U3CCoPlayOnceU3Ec__Iterator1_t201519776  : public RuntimeObject
{
public:
	// System.Collections.Generic.List`1<System.String> Utage.DicingAnimation/<CoPlayOnce>c__Iterator1::patternList
	List_1_t1398341365 * ___patternList_0;
	// System.Collections.Generic.List`1/Enumerator<System.String> Utage.DicingAnimation/<CoPlayOnce>c__Iterator1::$locvar0
	Enumerator_t933071039  ___U24locvar0_1;
	// System.String Utage.DicingAnimation/<CoPlayOnce>c__Iterator1::<pattern>__1
	String_t* ___U3CpatternU3E__1_2;
	// Utage.DicingAnimation Utage.DicingAnimation/<CoPlayOnce>c__Iterator1::$this
	DicingAnimation_t3770383148 * ___U24this_3;
	// System.Object Utage.DicingAnimation/<CoPlayOnce>c__Iterator1::$current
	RuntimeObject * ___U24current_4;
	// System.Boolean Utage.DicingAnimation/<CoPlayOnce>c__Iterator1::$disposing
	bool ___U24disposing_5;
	// System.Int32 Utage.DicingAnimation/<CoPlayOnce>c__Iterator1::$PC
	int32_t ___U24PC_6;

public:
	inline static int32_t get_offset_of_patternList_0() { return static_cast<int32_t>(offsetof(U3CCoPlayOnceU3Ec__Iterator1_t201519776, ___patternList_0)); }
	inline List_1_t1398341365 * get_patternList_0() const { return ___patternList_0; }
	inline List_1_t1398341365 ** get_address_of_patternList_0() { return &___patternList_0; }
	inline void set_patternList_0(List_1_t1398341365 * value)
	{
		___patternList_0 = value;
		Il2CppCodeGenWriteBarrier((&___patternList_0), value);
	}

	inline static int32_t get_offset_of_U24locvar0_1() { return static_cast<int32_t>(offsetof(U3CCoPlayOnceU3Ec__Iterator1_t201519776, ___U24locvar0_1)); }
	inline Enumerator_t933071039  get_U24locvar0_1() const { return ___U24locvar0_1; }
	inline Enumerator_t933071039 * get_address_of_U24locvar0_1() { return &___U24locvar0_1; }
	inline void set_U24locvar0_1(Enumerator_t933071039  value)
	{
		___U24locvar0_1 = value;
	}

	inline static int32_t get_offset_of_U3CpatternU3E__1_2() { return static_cast<int32_t>(offsetof(U3CCoPlayOnceU3Ec__Iterator1_t201519776, ___U3CpatternU3E__1_2)); }
	inline String_t* get_U3CpatternU3E__1_2() const { return ___U3CpatternU3E__1_2; }
	inline String_t** get_address_of_U3CpatternU3E__1_2() { return &___U3CpatternU3E__1_2; }
	inline void set_U3CpatternU3E__1_2(String_t* value)
	{
		___U3CpatternU3E__1_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CpatternU3E__1_2), value);
	}

	inline static int32_t get_offset_of_U24this_3() { return static_cast<int32_t>(offsetof(U3CCoPlayOnceU3Ec__Iterator1_t201519776, ___U24this_3)); }
	inline DicingAnimation_t3770383148 * get_U24this_3() const { return ___U24this_3; }
	inline DicingAnimation_t3770383148 ** get_address_of_U24this_3() { return &___U24this_3; }
	inline void set_U24this_3(DicingAnimation_t3770383148 * value)
	{
		___U24this_3 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_3), value);
	}

	inline static int32_t get_offset_of_U24current_4() { return static_cast<int32_t>(offsetof(U3CCoPlayOnceU3Ec__Iterator1_t201519776, ___U24current_4)); }
	inline RuntimeObject * get_U24current_4() const { return ___U24current_4; }
	inline RuntimeObject ** get_address_of_U24current_4() { return &___U24current_4; }
	inline void set_U24current_4(RuntimeObject * value)
	{
		___U24current_4 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_4), value);
	}

	inline static int32_t get_offset_of_U24disposing_5() { return static_cast<int32_t>(offsetof(U3CCoPlayOnceU3Ec__Iterator1_t201519776, ___U24disposing_5)); }
	inline bool get_U24disposing_5() const { return ___U24disposing_5; }
	inline bool* get_address_of_U24disposing_5() { return &___U24disposing_5; }
	inline void set_U24disposing_5(bool value)
	{
		___U24disposing_5 = value;
	}

	inline static int32_t get_offset_of_U24PC_6() { return static_cast<int32_t>(offsetof(U3CCoPlayOnceU3Ec__Iterator1_t201519776, ___U24PC_6)); }
	inline int32_t get_U24PC_6() const { return ___U24PC_6; }
	inline int32_t* get_address_of_U24PC_6() { return &___U24PC_6; }
	inline void set_U24PC_6(int32_t value)
	{
		___U24PC_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CCOPLAYONCEU3EC__ITERATOR1_T201519776_H
#ifndef UIQUAD_T2460659847_H
#define UIQUAD_T2460659847_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.UIQuad
struct  UIQuad_t2460659847  : public RuntimeObject
{
public:
	// UnityEngine.Vector4 Utage.UIQuad::v
	Vector4_t2243707581  ___v_0;
	// UnityEngine.Rect Utage.UIQuad::uv
	Rect_t3681755626  ___uv_1;

public:
	inline static int32_t get_offset_of_v_0() { return static_cast<int32_t>(offsetof(UIQuad_t2460659847, ___v_0)); }
	inline Vector4_t2243707581  get_v_0() const { return ___v_0; }
	inline Vector4_t2243707581 * get_address_of_v_0() { return &___v_0; }
	inline void set_v_0(Vector4_t2243707581  value)
	{
		___v_0 = value;
	}

	inline static int32_t get_offset_of_uv_1() { return static_cast<int32_t>(offsetof(UIQuad_t2460659847, ___uv_1)); }
	inline Rect_t3681755626  get_uv_1() const { return ___uv_1; }
	inline Rect_t3681755626 * get_address_of_uv_1() { return &___uv_1; }
	inline void set_uv_1(Rect_t3681755626  value)
	{
		___uv_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UIQUAD_T2460659847_H
#ifndef U3CONPOPULATEMESHU3EC__ANONSTOREY0_T1522063813_H
#define U3CONPOPULATEMESHU3EC__ANONSTOREY0_T1522063813_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.DicingImage/<OnPopulateMesh>c__AnonStorey0
struct  U3COnPopulateMeshU3Ec__AnonStorey0_t1522063813  : public RuntimeObject
{
public:
	// UnityEngine.UI.VertexHelper Utage.DicingImage/<OnPopulateMesh>c__AnonStorey0::vh
	VertexHelper_t385374196 * ___vh_0;
	// UnityEngine.Color Utage.DicingImage/<OnPopulateMesh>c__AnonStorey0::color32
	Color_t2020392075  ___color32_1;
	// System.Int32 Utage.DicingImage/<OnPopulateMesh>c__AnonStorey0::index
	int32_t ___index_2;

public:
	inline static int32_t get_offset_of_vh_0() { return static_cast<int32_t>(offsetof(U3COnPopulateMeshU3Ec__AnonStorey0_t1522063813, ___vh_0)); }
	inline VertexHelper_t385374196 * get_vh_0() const { return ___vh_0; }
	inline VertexHelper_t385374196 ** get_address_of_vh_0() { return &___vh_0; }
	inline void set_vh_0(VertexHelper_t385374196 * value)
	{
		___vh_0 = value;
		Il2CppCodeGenWriteBarrier((&___vh_0), value);
	}

	inline static int32_t get_offset_of_color32_1() { return static_cast<int32_t>(offsetof(U3COnPopulateMeshU3Ec__AnonStorey0_t1522063813, ___color32_1)); }
	inline Color_t2020392075  get_color32_1() const { return ___color32_1; }
	inline Color_t2020392075 * get_address_of_color32_1() { return &___color32_1; }
	inline void set_color32_1(Color_t2020392075  value)
	{
		___color32_1 = value;
	}

	inline static int32_t get_offset_of_index_2() { return static_cast<int32_t>(offsetof(U3COnPopulateMeshU3Ec__AnonStorey0_t1522063813, ___index_2)); }
	inline int32_t get_index_2() const { return ___index_2; }
	inline int32_t* get_address_of_index_2() { return &___index_2; }
	inline void set_index_2(int32_t value)
	{
		___index_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CONPOPULATEMESHU3EC__ANONSTOREY0_T1522063813_H
#ifndef U3CCOEYEBLINKU3EC__ITERATOR0_T2219955804_H
#define U3CCOEYEBLINKU3EC__ITERATOR0_T2219955804_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.EyeBlinkDicing/<CoEyeBlink>c__Iterator0
struct  U3CCoEyeBlinkU3Ec__Iterator0_t2219955804  : public RuntimeObject
{
public:
	// System.Collections.Generic.List`1/Enumerator<Utage.MiniAnimationData/Data> Utage.EyeBlinkDicing/<CoEyeBlink>c__Iterator0::$locvar0
	Enumerator_t3818772868  ___U24locvar0_0;
	// Utage.MiniAnimationData/Data Utage.EyeBlinkDicing/<CoEyeBlink>c__Iterator0::<data>__1
	Data_t619954766 * ___U3CdataU3E__1_1;
	// System.Action Utage.EyeBlinkDicing/<CoEyeBlink>c__Iterator0::onComplete
	Action_t3226471752 * ___onComplete_2;
	// Utage.EyeBlinkDicing Utage.EyeBlinkDicing/<CoEyeBlink>c__Iterator0::$this
	EyeBlinkDicing_t891209267 * ___U24this_3;
	// System.Object Utage.EyeBlinkDicing/<CoEyeBlink>c__Iterator0::$current
	RuntimeObject * ___U24current_4;
	// System.Boolean Utage.EyeBlinkDicing/<CoEyeBlink>c__Iterator0::$disposing
	bool ___U24disposing_5;
	// System.Int32 Utage.EyeBlinkDicing/<CoEyeBlink>c__Iterator0::$PC
	int32_t ___U24PC_6;

public:
	inline static int32_t get_offset_of_U24locvar0_0() { return static_cast<int32_t>(offsetof(U3CCoEyeBlinkU3Ec__Iterator0_t2219955804, ___U24locvar0_0)); }
	inline Enumerator_t3818772868  get_U24locvar0_0() const { return ___U24locvar0_0; }
	inline Enumerator_t3818772868 * get_address_of_U24locvar0_0() { return &___U24locvar0_0; }
	inline void set_U24locvar0_0(Enumerator_t3818772868  value)
	{
		___U24locvar0_0 = value;
	}

	inline static int32_t get_offset_of_U3CdataU3E__1_1() { return static_cast<int32_t>(offsetof(U3CCoEyeBlinkU3Ec__Iterator0_t2219955804, ___U3CdataU3E__1_1)); }
	inline Data_t619954766 * get_U3CdataU3E__1_1() const { return ___U3CdataU3E__1_1; }
	inline Data_t619954766 ** get_address_of_U3CdataU3E__1_1() { return &___U3CdataU3E__1_1; }
	inline void set_U3CdataU3E__1_1(Data_t619954766 * value)
	{
		___U3CdataU3E__1_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CdataU3E__1_1), value);
	}

	inline static int32_t get_offset_of_onComplete_2() { return static_cast<int32_t>(offsetof(U3CCoEyeBlinkU3Ec__Iterator0_t2219955804, ___onComplete_2)); }
	inline Action_t3226471752 * get_onComplete_2() const { return ___onComplete_2; }
	inline Action_t3226471752 ** get_address_of_onComplete_2() { return &___onComplete_2; }
	inline void set_onComplete_2(Action_t3226471752 * value)
	{
		___onComplete_2 = value;
		Il2CppCodeGenWriteBarrier((&___onComplete_2), value);
	}

	inline static int32_t get_offset_of_U24this_3() { return static_cast<int32_t>(offsetof(U3CCoEyeBlinkU3Ec__Iterator0_t2219955804, ___U24this_3)); }
	inline EyeBlinkDicing_t891209267 * get_U24this_3() const { return ___U24this_3; }
	inline EyeBlinkDicing_t891209267 ** get_address_of_U24this_3() { return &___U24this_3; }
	inline void set_U24this_3(EyeBlinkDicing_t891209267 * value)
	{
		___U24this_3 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_3), value);
	}

	inline static int32_t get_offset_of_U24current_4() { return static_cast<int32_t>(offsetof(U3CCoEyeBlinkU3Ec__Iterator0_t2219955804, ___U24current_4)); }
	inline RuntimeObject * get_U24current_4() const { return ___U24current_4; }
	inline RuntimeObject ** get_address_of_U24current_4() { return &___U24current_4; }
	inline void set_U24current_4(RuntimeObject * value)
	{
		___U24current_4 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_4), value);
	}

	inline static int32_t get_offset_of_U24disposing_5() { return static_cast<int32_t>(offsetof(U3CCoEyeBlinkU3Ec__Iterator0_t2219955804, ___U24disposing_5)); }
	inline bool get_U24disposing_5() const { return ___U24disposing_5; }
	inline bool* get_address_of_U24disposing_5() { return &___U24disposing_5; }
	inline void set_U24disposing_5(bool value)
	{
		___U24disposing_5 = value;
	}

	inline static int32_t get_offset_of_U24PC_6() { return static_cast<int32_t>(offsetof(U3CCoEyeBlinkU3Ec__Iterator0_t2219955804, ___U24PC_6)); }
	inline int32_t get_U24PC_6() const { return ___U24PC_6; }
	inline int32_t* get_address_of_U24PC_6() { return &___U24PC_6; }
	inline void set_U24PC_6(int32_t value)
	{
		___U24PC_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CCOEYEBLINKU3EC__ITERATOR0_T2219955804_H
#ifndef U3CFOREACHVERTEXLISTU3EC__ANONSTOREY1_T1404970980_H
#define U3CFOREACHVERTEXLISTU3EC__ANONSTOREY1_T1404970980_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.DicingTextureData/<ForeachVertexList>c__AnonStorey1
struct  U3CForeachVertexListU3Ec__AnonStorey1_t1404970980  : public RuntimeObject
{
public:
	// System.Single Utage.DicingTextureData/<ForeachVertexList>c__AnonStorey1::scaleX
	float ___scaleX_0;
	// UnityEngine.Rect Utage.DicingTextureData/<ForeachVertexList>c__AnonStorey1::rect
	Rect_t3681755626  ___rect_1;
	// System.Single Utage.DicingTextureData/<ForeachVertexList>c__AnonStorey1::fipOffsetX
	float ___fipOffsetX_2;
	// System.Single Utage.DicingTextureData/<ForeachVertexList>c__AnonStorey1::scaleY
	float ___scaleY_3;
	// System.Single Utage.DicingTextureData/<ForeachVertexList>c__AnonStorey1::offsetY
	float ___offsetY_4;
	// System.Single Utage.DicingTextureData/<ForeachVertexList>c__AnonStorey1::fipOffsetY
	float ___fipOffsetY_5;
	// System.Action`2<UnityEngine.Rect,UnityEngine.Rect> Utage.DicingTextureData/<ForeachVertexList>c__AnonStorey1::function
	Action_2_t1249439937 * ___function_6;
	// Utage.DicingTextureData Utage.DicingTextureData/<ForeachVertexList>c__AnonStorey1::$this
	DicingTextureData_t752335795 * ___U24this_7;

public:
	inline static int32_t get_offset_of_scaleX_0() { return static_cast<int32_t>(offsetof(U3CForeachVertexListU3Ec__AnonStorey1_t1404970980, ___scaleX_0)); }
	inline float get_scaleX_0() const { return ___scaleX_0; }
	inline float* get_address_of_scaleX_0() { return &___scaleX_0; }
	inline void set_scaleX_0(float value)
	{
		___scaleX_0 = value;
	}

	inline static int32_t get_offset_of_rect_1() { return static_cast<int32_t>(offsetof(U3CForeachVertexListU3Ec__AnonStorey1_t1404970980, ___rect_1)); }
	inline Rect_t3681755626  get_rect_1() const { return ___rect_1; }
	inline Rect_t3681755626 * get_address_of_rect_1() { return &___rect_1; }
	inline void set_rect_1(Rect_t3681755626  value)
	{
		___rect_1 = value;
	}

	inline static int32_t get_offset_of_fipOffsetX_2() { return static_cast<int32_t>(offsetof(U3CForeachVertexListU3Ec__AnonStorey1_t1404970980, ___fipOffsetX_2)); }
	inline float get_fipOffsetX_2() const { return ___fipOffsetX_2; }
	inline float* get_address_of_fipOffsetX_2() { return &___fipOffsetX_2; }
	inline void set_fipOffsetX_2(float value)
	{
		___fipOffsetX_2 = value;
	}

	inline static int32_t get_offset_of_scaleY_3() { return static_cast<int32_t>(offsetof(U3CForeachVertexListU3Ec__AnonStorey1_t1404970980, ___scaleY_3)); }
	inline float get_scaleY_3() const { return ___scaleY_3; }
	inline float* get_address_of_scaleY_3() { return &___scaleY_3; }
	inline void set_scaleY_3(float value)
	{
		___scaleY_3 = value;
	}

	inline static int32_t get_offset_of_offsetY_4() { return static_cast<int32_t>(offsetof(U3CForeachVertexListU3Ec__AnonStorey1_t1404970980, ___offsetY_4)); }
	inline float get_offsetY_4() const { return ___offsetY_4; }
	inline float* get_address_of_offsetY_4() { return &___offsetY_4; }
	inline void set_offsetY_4(float value)
	{
		___offsetY_4 = value;
	}

	inline static int32_t get_offset_of_fipOffsetY_5() { return static_cast<int32_t>(offsetof(U3CForeachVertexListU3Ec__AnonStorey1_t1404970980, ___fipOffsetY_5)); }
	inline float get_fipOffsetY_5() const { return ___fipOffsetY_5; }
	inline float* get_address_of_fipOffsetY_5() { return &___fipOffsetY_5; }
	inline void set_fipOffsetY_5(float value)
	{
		___fipOffsetY_5 = value;
	}

	inline static int32_t get_offset_of_function_6() { return static_cast<int32_t>(offsetof(U3CForeachVertexListU3Ec__AnonStorey1_t1404970980, ___function_6)); }
	inline Action_2_t1249439937 * get_function_6() const { return ___function_6; }
	inline Action_2_t1249439937 ** get_address_of_function_6() { return &___function_6; }
	inline void set_function_6(Action_2_t1249439937 * value)
	{
		___function_6 = value;
		Il2CppCodeGenWriteBarrier((&___function_6), value);
	}

	inline static int32_t get_offset_of_U24this_7() { return static_cast<int32_t>(offsetof(U3CForeachVertexListU3Ec__AnonStorey1_t1404970980, ___U24this_7)); }
	inline DicingTextureData_t752335795 * get_U24this_7() const { return ___U24this_7; }
	inline DicingTextureData_t752335795 ** get_address_of_U24this_7() { return &___U24this_7; }
	inline void set_U24this_7(DicingTextureData_t752335795 * value)
	{
		___U24this_7 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CFOREACHVERTEXLISTU3EC__ANONSTOREY1_T1404970980_H
#ifndef MOTIONPLAYTYPE_T785574570_H
#define MOTIONPLAYTYPE_T785574570_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.MotionPlayType
struct  MotionPlayType_t785574570 
{
public:
	// System.Int32 Utage.MotionPlayType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(MotionPlayType_t785574570, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MOTIONPLAYTYPE_T785574570_H
#ifndef U3CFOREACHVERTEXLISTU3EC__ANONSTOREY0_T1404970979_H
#define U3CFOREACHVERTEXLISTU3EC__ANONSTOREY0_T1404970979_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.DicingTextureData/<ForeachVertexList>c__AnonStorey0
struct  U3CForeachVertexListU3Ec__AnonStorey0_t1404970979  : public RuntimeObject
{
public:
	// UnityEngine.Vector2 Utage.DicingTextureData/<ForeachVertexList>c__AnonStorey0::scale
	Vector2_t2243707579  ___scale_0;
	// UnityEngine.Rect Utage.DicingTextureData/<ForeachVertexList>c__AnonStorey0::position
	Rect_t3681755626  ___position_1;
	// System.Action`2<UnityEngine.Rect,UnityEngine.Rect> Utage.DicingTextureData/<ForeachVertexList>c__AnonStorey0::function
	Action_2_t1249439937 * ___function_2;

public:
	inline static int32_t get_offset_of_scale_0() { return static_cast<int32_t>(offsetof(U3CForeachVertexListU3Ec__AnonStorey0_t1404970979, ___scale_0)); }
	inline Vector2_t2243707579  get_scale_0() const { return ___scale_0; }
	inline Vector2_t2243707579 * get_address_of_scale_0() { return &___scale_0; }
	inline void set_scale_0(Vector2_t2243707579  value)
	{
		___scale_0 = value;
	}

	inline static int32_t get_offset_of_position_1() { return static_cast<int32_t>(offsetof(U3CForeachVertexListU3Ec__AnonStorey0_t1404970979, ___position_1)); }
	inline Rect_t3681755626  get_position_1() const { return ___position_1; }
	inline Rect_t3681755626 * get_address_of_position_1() { return &___position_1; }
	inline void set_position_1(Rect_t3681755626  value)
	{
		___position_1 = value;
	}

	inline static int32_t get_offset_of_function_2() { return static_cast<int32_t>(offsetof(U3CForeachVertexListU3Ec__AnonStorey0_t1404970979, ___function_2)); }
	inline Action_2_t1249439937 * get_function_2() const { return ___function_2; }
	inline Action_2_t1249439937 ** get_address_of_function_2() { return &___function_2; }
	inline void set_function_2(Action_2_t1249439937 * value)
	{
		___function_2 = value;
		Il2CppCodeGenWriteBarrier((&___function_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CFOREACHVERTEXLISTU3EC__ANONSTOREY0_T1404970979_H
#ifndef QUADVERTS_T459249791_H
#define QUADVERTS_T459249791_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.DicingTextureData/QuadVerts
struct  QuadVerts_t459249791  : public RuntimeObject
{
public:
	// UnityEngine.Vector4 Utage.DicingTextureData/QuadVerts::v
	Vector4_t2243707581  ___v_0;
	// UnityEngine.Rect Utage.DicingTextureData/QuadVerts::uvRect
	Rect_t3681755626  ___uvRect_1;
	// System.Boolean Utage.DicingTextureData/QuadVerts::isAllTransparent
	bool ___isAllTransparent_2;

public:
	inline static int32_t get_offset_of_v_0() { return static_cast<int32_t>(offsetof(QuadVerts_t459249791, ___v_0)); }
	inline Vector4_t2243707581  get_v_0() const { return ___v_0; }
	inline Vector4_t2243707581 * get_address_of_v_0() { return &___v_0; }
	inline void set_v_0(Vector4_t2243707581  value)
	{
		___v_0 = value;
	}

	inline static int32_t get_offset_of_uvRect_1() { return static_cast<int32_t>(offsetof(QuadVerts_t459249791, ___uvRect_1)); }
	inline Rect_t3681755626  get_uvRect_1() const { return ___uvRect_1; }
	inline Rect_t3681755626 * get_address_of_uvRect_1() { return &___uvRect_1; }
	inline void set_uvRect_1(Rect_t3681755626  value)
	{
		___uvRect_1 = value;
	}

	inline static int32_t get_offset_of_isAllTransparent_2() { return static_cast<int32_t>(offsetof(QuadVerts_t459249791, ___isAllTransparent_2)); }
	inline bool get_isAllTransparent_2() const { return ___isAllTransparent_2; }
	inline bool* get_address_of_isAllTransparent_2() { return &___isAllTransparent_2; }
	inline void set_isAllTransparent_2(bool value)
	{
		___isAllTransparent_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // QUADVERTS_T459249791_H
#ifndef U3CHITTESTU3EC__ANONSTOREY1_T3427657523_H
#define U3CHITTESTU3EC__ANONSTOREY1_T3427657523_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.DicingImage/<HitTest>c__AnonStorey1
struct  U3CHitTestU3Ec__AnonStorey1_t3427657523  : public RuntimeObject
{
public:
	// UnityEngine.Vector2 Utage.DicingImage/<HitTest>c__AnonStorey1::localPosition
	Vector2_t2243707579  ___localPosition_0;
	// System.Boolean Utage.DicingImage/<HitTest>c__AnonStorey1::isHit
	bool ___isHit_1;

public:
	inline static int32_t get_offset_of_localPosition_0() { return static_cast<int32_t>(offsetof(U3CHitTestU3Ec__AnonStorey1_t3427657523, ___localPosition_0)); }
	inline Vector2_t2243707579  get_localPosition_0() const { return ___localPosition_0; }
	inline Vector2_t2243707579 * get_address_of_localPosition_0() { return &___localPosition_0; }
	inline void set_localPosition_0(Vector2_t2243707579  value)
	{
		___localPosition_0 = value;
	}

	inline static int32_t get_offset_of_isHit_1() { return static_cast<int32_t>(offsetof(U3CHitTestU3Ec__AnonStorey1_t3427657523, ___isHit_1)); }
	inline bool get_isHit_1() const { return ___isHit_1; }
	inline bool* get_address_of_isHit_1() { return &___isHit_1; }
	inline void set_isHit_1(bool value)
	{
		___isHit_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CHITTESTU3EC__ANONSTOREY1_T3427657523_H
#ifndef TYPE_T2522374058_H
#define TYPE_T2522374058_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.UguiNovelTextGeneratorAdditionalLine/Type
struct  Type_t2522374058 
{
public:
	// System.Int32 Utage.UguiNovelTextGeneratorAdditionalLine/Type::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(Type_t2522374058, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TYPE_T2522374058_H
#ifndef SOUNDPLAYMODE_T4141625668_H
#define SOUNDPLAYMODE_T4141625668_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.SoundPlayMode
struct  SoundPlayMode_t4141625668 
{
public:
	// System.Int32 Utage.SoundPlayMode::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(SoundPlayMode_t4141625668, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SOUNDPLAYMODE_T4141625668_H
#ifndef CHAGNETYPE_T192839657_H
#define CHAGNETYPE_T192839657_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.UguiNovelTextGenerator/ChagneType
struct  ChagneType_t192839657 
{
public:
	// System.Int32 Utage.UguiNovelTextGenerator/ChagneType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(ChagneType_t192839657, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CHAGNETYPE_T192839657_H
#ifndef ONCLICKLINKEVENT_T134619553_H
#define ONCLICKLINKEVENT_T134619553_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.OnClickLinkEvent
struct  OnClickLinkEvent_t134619553  : public UnityEvent_1_t1617761548
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ONCLICKLINKEVENT_T134619553_H
#ifndef WORDWRAP_T2124153_H
#define WORDWRAP_T2124153_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.UguiNovelTextGenerator/WordWrap
struct  WordWrap_t2124153 
{
public:
	// System.Int32 Utage.UguiNovelTextGenerator/WordWrap::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(WordWrap_t2124153, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WORDWRAP_T2124153_H
#ifndef TYPE_T520746208_H
#define TYPE_T520746208_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.UguiNovelTextHitArea/Type
struct  Type_t520746208 
{
public:
	// System.Int32 Utage.UguiNovelTextHitArea/Type::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(Type_t520746208, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TYPE_T520746208_H
#ifndef SETTING_T2370336044_H
#define SETTING_T2370336044_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.UguiLocalizeRectTransform/Setting
struct  Setting_t2370336044  : public RuntimeObject
{
public:
	// System.String Utage.UguiLocalizeRectTransform/Setting::language
	String_t* ___language_0;
	// UnityEngine.Vector2 Utage.UguiLocalizeRectTransform/Setting::anchoredPosition
	Vector2_t2243707579  ___anchoredPosition_1;
	// UnityEngine.Vector2 Utage.UguiLocalizeRectTransform/Setting::size
	Vector2_t2243707579  ___size_2;

public:
	inline static int32_t get_offset_of_language_0() { return static_cast<int32_t>(offsetof(Setting_t2370336044, ___language_0)); }
	inline String_t* get_language_0() const { return ___language_0; }
	inline String_t** get_address_of_language_0() { return &___language_0; }
	inline void set_language_0(String_t* value)
	{
		___language_0 = value;
		Il2CppCodeGenWriteBarrier((&___language_0), value);
	}

	inline static int32_t get_offset_of_anchoredPosition_1() { return static_cast<int32_t>(offsetof(Setting_t2370336044, ___anchoredPosition_1)); }
	inline Vector2_t2243707579  get_anchoredPosition_1() const { return ___anchoredPosition_1; }
	inline Vector2_t2243707579 * get_address_of_anchoredPosition_1() { return &___anchoredPosition_1; }
	inline void set_anchoredPosition_1(Vector2_t2243707579  value)
	{
		___anchoredPosition_1 = value;
	}

	inline static int32_t get_offset_of_size_2() { return static_cast<int32_t>(offsetof(Setting_t2370336044, ___size_2)); }
	inline Vector2_t2243707579  get_size_2() const { return ___size_2; }
	inline Vector2_t2243707579 * get_address_of_size_2() { return &___size_2; }
	inline void set_size_2(Vector2_t2243707579  value)
	{
		___size_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SETTING_T2370336044_H
#ifndef UGUINOVELTEXTGENERATORADDITIONALLINE_T1868820539_H
#define UGUINOVELTEXTGENERATORADDITIONALLINE_T1868820539_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.UguiNovelTextGeneratorAdditionalLine
struct  UguiNovelTextGeneratorAdditionalLine_t1868820539  : public RuntimeObject
{
public:
	// Utage.UguiNovelTextGeneratorAdditionalLine/Type Utage.UguiNovelTextGeneratorAdditionalLine::type
	int32_t ___type_0;
	// Utage.UguiNovelTextCharacter Utage.UguiNovelTextGeneratorAdditionalLine::characteData
	UguiNovelTextCharacter_t1244198124 * ___characteData_1;
	// System.Collections.Generic.List`1<Utage.UguiNovelTextCharacter> Utage.UguiNovelTextGeneratorAdditionalLine::stringData
	List_1_t613319256 * ___stringData_2;
	// Utage.UguiNovelTextLine Utage.UguiNovelTextGeneratorAdditionalLine::textLine
	UguiNovelTextLine_t2439763533 * ___textLine_3;

public:
	inline static int32_t get_offset_of_type_0() { return static_cast<int32_t>(offsetof(UguiNovelTextGeneratorAdditionalLine_t1868820539, ___type_0)); }
	inline int32_t get_type_0() const { return ___type_0; }
	inline int32_t* get_address_of_type_0() { return &___type_0; }
	inline void set_type_0(int32_t value)
	{
		___type_0 = value;
	}

	inline static int32_t get_offset_of_characteData_1() { return static_cast<int32_t>(offsetof(UguiNovelTextGeneratorAdditionalLine_t1868820539, ___characteData_1)); }
	inline UguiNovelTextCharacter_t1244198124 * get_characteData_1() const { return ___characteData_1; }
	inline UguiNovelTextCharacter_t1244198124 ** get_address_of_characteData_1() { return &___characteData_1; }
	inline void set_characteData_1(UguiNovelTextCharacter_t1244198124 * value)
	{
		___characteData_1 = value;
		Il2CppCodeGenWriteBarrier((&___characteData_1), value);
	}

	inline static int32_t get_offset_of_stringData_2() { return static_cast<int32_t>(offsetof(UguiNovelTextGeneratorAdditionalLine_t1868820539, ___stringData_2)); }
	inline List_1_t613319256 * get_stringData_2() const { return ___stringData_2; }
	inline List_1_t613319256 ** get_address_of_stringData_2() { return &___stringData_2; }
	inline void set_stringData_2(List_1_t613319256 * value)
	{
		___stringData_2 = value;
		Il2CppCodeGenWriteBarrier((&___stringData_2), value);
	}

	inline static int32_t get_offset_of_textLine_3() { return static_cast<int32_t>(offsetof(UguiNovelTextGeneratorAdditionalLine_t1868820539, ___textLine_3)); }
	inline UguiNovelTextLine_t2439763533 * get_textLine_3() const { return ___textLine_3; }
	inline UguiNovelTextLine_t2439763533 ** get_address_of_textLine_3() { return &___textLine_3; }
	inline void set_textLine_3(UguiNovelTextLine_t2439763533 * value)
	{
		___textLine_3 = value;
		Il2CppCodeGenWriteBarrier((&___textLine_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UGUINOVELTEXTGENERATORADDITIONALLINE_T1868820539_H
#ifndef UGUINOVELTEXTHITAREA_T1579411533_H
#define UGUINOVELTEXTHITAREA_T1579411533_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.UguiNovelTextHitArea
struct  UguiNovelTextHitArea_t1579411533  : public RuntimeObject
{
public:
	// Utage.UguiNovelText Utage.UguiNovelTextHitArea::novelText
	UguiNovelText_t4135744055 * ___novelText_0;
	// Utage.CharData/HitEventType Utage.UguiNovelTextHitArea::<HitEventType>k__BackingField
	int32_t ___U3CHitEventTypeU3Ek__BackingField_1;
	// System.String Utage.UguiNovelTextHitArea::<Arg>k__BackingField
	String_t* ___U3CArgU3Ek__BackingField_2;
	// System.Collections.Generic.List`1<Utage.UguiNovelTextCharacter> Utage.UguiNovelTextHitArea::characters
	List_1_t613319256 * ___characters_3;
	// System.Collections.Generic.List`1<UnityEngine.Rect> Utage.UguiNovelTextHitArea::hitAreaList
	List_1_t3050876758 * ___hitAreaList_4;

public:
	inline static int32_t get_offset_of_novelText_0() { return static_cast<int32_t>(offsetof(UguiNovelTextHitArea_t1579411533, ___novelText_0)); }
	inline UguiNovelText_t4135744055 * get_novelText_0() const { return ___novelText_0; }
	inline UguiNovelText_t4135744055 ** get_address_of_novelText_0() { return &___novelText_0; }
	inline void set_novelText_0(UguiNovelText_t4135744055 * value)
	{
		___novelText_0 = value;
		Il2CppCodeGenWriteBarrier((&___novelText_0), value);
	}

	inline static int32_t get_offset_of_U3CHitEventTypeU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(UguiNovelTextHitArea_t1579411533, ___U3CHitEventTypeU3Ek__BackingField_1)); }
	inline int32_t get_U3CHitEventTypeU3Ek__BackingField_1() const { return ___U3CHitEventTypeU3Ek__BackingField_1; }
	inline int32_t* get_address_of_U3CHitEventTypeU3Ek__BackingField_1() { return &___U3CHitEventTypeU3Ek__BackingField_1; }
	inline void set_U3CHitEventTypeU3Ek__BackingField_1(int32_t value)
	{
		___U3CHitEventTypeU3Ek__BackingField_1 = value;
	}

	inline static int32_t get_offset_of_U3CArgU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(UguiNovelTextHitArea_t1579411533, ___U3CArgU3Ek__BackingField_2)); }
	inline String_t* get_U3CArgU3Ek__BackingField_2() const { return ___U3CArgU3Ek__BackingField_2; }
	inline String_t** get_address_of_U3CArgU3Ek__BackingField_2() { return &___U3CArgU3Ek__BackingField_2; }
	inline void set_U3CArgU3Ek__BackingField_2(String_t* value)
	{
		___U3CArgU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CArgU3Ek__BackingField_2), value);
	}

	inline static int32_t get_offset_of_characters_3() { return static_cast<int32_t>(offsetof(UguiNovelTextHitArea_t1579411533, ___characters_3)); }
	inline List_1_t613319256 * get_characters_3() const { return ___characters_3; }
	inline List_1_t613319256 ** get_address_of_characters_3() { return &___characters_3; }
	inline void set_characters_3(List_1_t613319256 * value)
	{
		___characters_3 = value;
		Il2CppCodeGenWriteBarrier((&___characters_3), value);
	}

	inline static int32_t get_offset_of_hitAreaList_4() { return static_cast<int32_t>(offsetof(UguiNovelTextHitArea_t1579411533, ___hitAreaList_4)); }
	inline List_1_t3050876758 * get_hitAreaList_4() const { return ___hitAreaList_4; }
	inline List_1_t3050876758 ** get_address_of_hitAreaList_4() { return &___hitAreaList_4; }
	inline void set_hitAreaList_4(List_1_t3050876758 * value)
	{
		___hitAreaList_4 = value;
		Il2CppCodeGenWriteBarrier((&___hitAreaList_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UGUINOVELTEXTHITAREA_T1579411533_H
#ifndef COMPONENT_T3819376471_H
#define COMPONENT_T3819376471_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Component
struct  Component_t3819376471  : public Object_t1021602117
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPONENT_T3819376471_H
#ifndef SCRIPTABLEOBJECT_T1975622470_H
#define SCRIPTABLEOBJECT_T1975622470_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.ScriptableObject
struct  ScriptableObject_t1975622470  : public Object_t1021602117
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.ScriptableObject
struct ScriptableObject_t1975622470_marshaled_pinvoke : public Object_t1021602117_marshaled_pinvoke
{
};
// Native definition for COM marshalling of UnityEngine.ScriptableObject
struct ScriptableObject_t1975622470_marshaled_com : public Object_t1021602117_marshaled_com
{
};
#endif // SCRIPTABLEOBJECT_T1975622470_H
#ifndef CHARACTERINFO_T3919092135_H
#define CHARACTERINFO_T3919092135_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.CharacterInfo
struct  CharacterInfo_t3919092135 
{
public:
	// System.Int32 UnityEngine.CharacterInfo::index
	int32_t ___index_0;
	// UnityEngine.Rect UnityEngine.CharacterInfo::uv
	Rect_t3681755626  ___uv_1;
	// UnityEngine.Rect UnityEngine.CharacterInfo::vert
	Rect_t3681755626  ___vert_2;
	// System.Single UnityEngine.CharacterInfo::width
	float ___width_3;
	// System.Int32 UnityEngine.CharacterInfo::size
	int32_t ___size_4;
	// UnityEngine.FontStyle UnityEngine.CharacterInfo::style
	int32_t ___style_5;
	// System.Boolean UnityEngine.CharacterInfo::flipped
	bool ___flipped_6;

public:
	inline static int32_t get_offset_of_index_0() { return static_cast<int32_t>(offsetof(CharacterInfo_t3919092135, ___index_0)); }
	inline int32_t get_index_0() const { return ___index_0; }
	inline int32_t* get_address_of_index_0() { return &___index_0; }
	inline void set_index_0(int32_t value)
	{
		___index_0 = value;
	}

	inline static int32_t get_offset_of_uv_1() { return static_cast<int32_t>(offsetof(CharacterInfo_t3919092135, ___uv_1)); }
	inline Rect_t3681755626  get_uv_1() const { return ___uv_1; }
	inline Rect_t3681755626 * get_address_of_uv_1() { return &___uv_1; }
	inline void set_uv_1(Rect_t3681755626  value)
	{
		___uv_1 = value;
	}

	inline static int32_t get_offset_of_vert_2() { return static_cast<int32_t>(offsetof(CharacterInfo_t3919092135, ___vert_2)); }
	inline Rect_t3681755626  get_vert_2() const { return ___vert_2; }
	inline Rect_t3681755626 * get_address_of_vert_2() { return &___vert_2; }
	inline void set_vert_2(Rect_t3681755626  value)
	{
		___vert_2 = value;
	}

	inline static int32_t get_offset_of_width_3() { return static_cast<int32_t>(offsetof(CharacterInfo_t3919092135, ___width_3)); }
	inline float get_width_3() const { return ___width_3; }
	inline float* get_address_of_width_3() { return &___width_3; }
	inline void set_width_3(float value)
	{
		___width_3 = value;
	}

	inline static int32_t get_offset_of_size_4() { return static_cast<int32_t>(offsetof(CharacterInfo_t3919092135, ___size_4)); }
	inline int32_t get_size_4() const { return ___size_4; }
	inline int32_t* get_address_of_size_4() { return &___size_4; }
	inline void set_size_4(int32_t value)
	{
		___size_4 = value;
	}

	inline static int32_t get_offset_of_style_5() { return static_cast<int32_t>(offsetof(CharacterInfo_t3919092135, ___style_5)); }
	inline int32_t get_style_5() const { return ___style_5; }
	inline int32_t* get_address_of_style_5() { return &___style_5; }
	inline void set_style_5(int32_t value)
	{
		___style_5 = value;
	}

	inline static int32_t get_offset_of_flipped_6() { return static_cast<int32_t>(offsetof(CharacterInfo_t3919092135, ___flipped_6)); }
	inline bool get_flipped_6() const { return ___flipped_6; }
	inline bool* get_address_of_flipped_6() { return &___flipped_6; }
	inline void set_flipped_6(bool value)
	{
		___flipped_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.CharacterInfo
struct CharacterInfo_t3919092135_marshaled_pinvoke
{
	int32_t ___index_0;
	Rect_t3681755626  ___uv_1;
	Rect_t3681755626  ___vert_2;
	float ___width_3;
	int32_t ___size_4;
	int32_t ___style_5;
	int32_t ___flipped_6;
};
// Native definition for COM marshalling of UnityEngine.CharacterInfo
struct CharacterInfo_t3919092135_marshaled_com
{
	int32_t ___index_0;
	Rect_t3681755626  ___uv_1;
	Rect_t3681755626  ___vert_2;
	float ___width_3;
	int32_t ___size_4;
	int32_t ___style_5;
	int32_t ___flipped_6;
};
#endif // CHARACTERINFO_T3919092135_H
#ifndef REQUESTCHARACTERSINFO_T3077364824_H
#define REQUESTCHARACTERSINFO_T3077364824_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.UguiNovelTextGenerator/RequestCharactersInfo
struct  RequestCharactersInfo_t3077364824  : public RuntimeObject
{
public:
	// System.String Utage.UguiNovelTextGenerator/RequestCharactersInfo::characters
	String_t* ___characters_0;
	// System.Int32 Utage.UguiNovelTextGenerator/RequestCharactersInfo::size
	int32_t ___size_1;
	// UnityEngine.FontStyle Utage.UguiNovelTextGenerator/RequestCharactersInfo::style
	int32_t ___style_2;

public:
	inline static int32_t get_offset_of_characters_0() { return static_cast<int32_t>(offsetof(RequestCharactersInfo_t3077364824, ___characters_0)); }
	inline String_t* get_characters_0() const { return ___characters_0; }
	inline String_t** get_address_of_characters_0() { return &___characters_0; }
	inline void set_characters_0(String_t* value)
	{
		___characters_0 = value;
		Il2CppCodeGenWriteBarrier((&___characters_0), value);
	}

	inline static int32_t get_offset_of_size_1() { return static_cast<int32_t>(offsetof(RequestCharactersInfo_t3077364824, ___size_1)); }
	inline int32_t get_size_1() const { return ___size_1; }
	inline int32_t* get_address_of_size_1() { return &___size_1; }
	inline void set_size_1(int32_t value)
	{
		___size_1 = value;
	}

	inline static int32_t get_offset_of_style_2() { return static_cast<int32_t>(offsetof(RequestCharactersInfo_t3077364824, ___style_2)); }
	inline int32_t get_style_2() const { return ___style_2; }
	inline int32_t* get_address_of_style_2() { return &___style_2; }
	inline void set_style_2(int32_t value)
	{
		___style_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // REQUESTCHARACTERSINFO_T3077364824_H
#ifndef UGUINOVELTEXTSETTINGS_T4201484304_H
#define UGUINOVELTEXTSETTINGS_T4201484304_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.UguiNovelTextSettings
struct  UguiNovelTextSettings_t4201484304  : public ScriptableObject_t1975622470
{
public:
	// System.String Utage.UguiNovelTextSettings::wordWrapSeparator
	String_t* ___wordWrapSeparator_2;
	// System.String Utage.UguiNovelTextSettings::kinsokuTop
	String_t* ___kinsokuTop_3;
	// System.String Utage.UguiNovelTextSettings::kinsokuEnd
	String_t* ___kinsokuEnd_4;
	// System.String Utage.UguiNovelTextSettings::ignoreLetterSpace
	String_t* ___ignoreLetterSpace_5;
	// System.String Utage.UguiNovelTextSettings::autoIndentation
	String_t* ___autoIndentation_6;
	// System.Boolean Utage.UguiNovelTextSettings::forceIgonreJapaneseKinsoku
	bool ___forceIgonreJapaneseKinsoku_7;

public:
	inline static int32_t get_offset_of_wordWrapSeparator_2() { return static_cast<int32_t>(offsetof(UguiNovelTextSettings_t4201484304, ___wordWrapSeparator_2)); }
	inline String_t* get_wordWrapSeparator_2() const { return ___wordWrapSeparator_2; }
	inline String_t** get_address_of_wordWrapSeparator_2() { return &___wordWrapSeparator_2; }
	inline void set_wordWrapSeparator_2(String_t* value)
	{
		___wordWrapSeparator_2 = value;
		Il2CppCodeGenWriteBarrier((&___wordWrapSeparator_2), value);
	}

	inline static int32_t get_offset_of_kinsokuTop_3() { return static_cast<int32_t>(offsetof(UguiNovelTextSettings_t4201484304, ___kinsokuTop_3)); }
	inline String_t* get_kinsokuTop_3() const { return ___kinsokuTop_3; }
	inline String_t** get_address_of_kinsokuTop_3() { return &___kinsokuTop_3; }
	inline void set_kinsokuTop_3(String_t* value)
	{
		___kinsokuTop_3 = value;
		Il2CppCodeGenWriteBarrier((&___kinsokuTop_3), value);
	}

	inline static int32_t get_offset_of_kinsokuEnd_4() { return static_cast<int32_t>(offsetof(UguiNovelTextSettings_t4201484304, ___kinsokuEnd_4)); }
	inline String_t* get_kinsokuEnd_4() const { return ___kinsokuEnd_4; }
	inline String_t** get_address_of_kinsokuEnd_4() { return &___kinsokuEnd_4; }
	inline void set_kinsokuEnd_4(String_t* value)
	{
		___kinsokuEnd_4 = value;
		Il2CppCodeGenWriteBarrier((&___kinsokuEnd_4), value);
	}

	inline static int32_t get_offset_of_ignoreLetterSpace_5() { return static_cast<int32_t>(offsetof(UguiNovelTextSettings_t4201484304, ___ignoreLetterSpace_5)); }
	inline String_t* get_ignoreLetterSpace_5() const { return ___ignoreLetterSpace_5; }
	inline String_t** get_address_of_ignoreLetterSpace_5() { return &___ignoreLetterSpace_5; }
	inline void set_ignoreLetterSpace_5(String_t* value)
	{
		___ignoreLetterSpace_5 = value;
		Il2CppCodeGenWriteBarrier((&___ignoreLetterSpace_5), value);
	}

	inline static int32_t get_offset_of_autoIndentation_6() { return static_cast<int32_t>(offsetof(UguiNovelTextSettings_t4201484304, ___autoIndentation_6)); }
	inline String_t* get_autoIndentation_6() const { return ___autoIndentation_6; }
	inline String_t** get_address_of_autoIndentation_6() { return &___autoIndentation_6; }
	inline void set_autoIndentation_6(String_t* value)
	{
		___autoIndentation_6 = value;
		Il2CppCodeGenWriteBarrier((&___autoIndentation_6), value);
	}

	inline static int32_t get_offset_of_forceIgonreJapaneseKinsoku_7() { return static_cast<int32_t>(offsetof(UguiNovelTextSettings_t4201484304, ___forceIgonreJapaneseKinsoku_7)); }
	inline bool get_forceIgonreJapaneseKinsoku_7() const { return ___forceIgonreJapaneseKinsoku_7; }
	inline bool* get_address_of_forceIgonreJapaneseKinsoku_7() { return &___forceIgonreJapaneseKinsoku_7; }
	inline void set_forceIgonreJapaneseKinsoku_7(bool value)
	{
		___forceIgonreJapaneseKinsoku_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UGUINOVELTEXTSETTINGS_T4201484304_H
#ifndef BEHAVIOUR_T955675639_H
#define BEHAVIOUR_T955675639_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Behaviour
struct  Behaviour_t955675639  : public Component_t3819376471
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BEHAVIOUR_T955675639_H
#ifndef DICINGTEXTURES_T1684855170_H
#define DICINGTEXTURES_T1684855170_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.DicingTextures
struct  DicingTextures_t1684855170  : public ScriptableObject_t1975622470
{
public:
	// System.Int32 Utage.DicingTextures::cellSize
	int32_t ___cellSize_2;
	// System.Int32 Utage.DicingTextures::padding
	int32_t ___padding_3;
	// System.Collections.Generic.List`1<UnityEngine.Texture2D> Utage.DicingTextures::atlasTextures
	List_1_t2912116861 * ___atlasTextures_4;
	// System.Collections.Generic.List`1<Utage.DicingTextureData> Utage.DicingTextures::textureDataList
	List_1_t121456927 * ___textureDataList_5;

public:
	inline static int32_t get_offset_of_cellSize_2() { return static_cast<int32_t>(offsetof(DicingTextures_t1684855170, ___cellSize_2)); }
	inline int32_t get_cellSize_2() const { return ___cellSize_2; }
	inline int32_t* get_address_of_cellSize_2() { return &___cellSize_2; }
	inline void set_cellSize_2(int32_t value)
	{
		___cellSize_2 = value;
	}

	inline static int32_t get_offset_of_padding_3() { return static_cast<int32_t>(offsetof(DicingTextures_t1684855170, ___padding_3)); }
	inline int32_t get_padding_3() const { return ___padding_3; }
	inline int32_t* get_address_of_padding_3() { return &___padding_3; }
	inline void set_padding_3(int32_t value)
	{
		___padding_3 = value;
	}

	inline static int32_t get_offset_of_atlasTextures_4() { return static_cast<int32_t>(offsetof(DicingTextures_t1684855170, ___atlasTextures_4)); }
	inline List_1_t2912116861 * get_atlasTextures_4() const { return ___atlasTextures_4; }
	inline List_1_t2912116861 ** get_address_of_atlasTextures_4() { return &___atlasTextures_4; }
	inline void set_atlasTextures_4(List_1_t2912116861 * value)
	{
		___atlasTextures_4 = value;
		Il2CppCodeGenWriteBarrier((&___atlasTextures_4), value);
	}

	inline static int32_t get_offset_of_textureDataList_5() { return static_cast<int32_t>(offsetof(DicingTextures_t1684855170, ___textureDataList_5)); }
	inline List_1_t121456927 * get_textureDataList_5() const { return ___textureDataList_5; }
	inline List_1_t121456927 ** get_address_of_textureDataList_5() { return &___textureDataList_5; }
	inline void set_textureDataList_5(List_1_t121456927 * value)
	{
		___textureDataList_5 = value;
		Il2CppCodeGenWriteBarrier((&___textureDataList_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DICINGTEXTURES_T1684855170_H
#ifndef UGUINOVELTEXTCHARACTER_T1244198124_H
#define UGUINOVELTEXTCHARACTER_T1244198124_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.UguiNovelTextCharacter
struct  UguiNovelTextCharacter_t1244198124  : public RuntimeObject
{
public:
	// System.Boolean Utage.UguiNovelTextCharacter::isAutoLineBreak
	bool ___isAutoLineBreak_0;
	// System.Boolean Utage.UguiNovelTextCharacter::isKinsokuBurasage
	bool ___isKinsokuBurasage_1;
	// Utage.CharData Utage.UguiNovelTextCharacter::charData
	CharData_t4178071270 * ___charData_2;
	// UnityEngine.CharacterInfo Utage.UguiNovelTextCharacter::charInfo
	CharacterInfo_t3919092135  ___charInfo_3;
	// System.Single Utage.UguiNovelTextCharacter::width
	float ___width_4;
	// System.Int32 Utage.UguiNovelTextCharacter::fontSize
	int32_t ___fontSize_5;
	// System.Int32 Utage.UguiNovelTextCharacter::defaultFontSize
	int32_t ___defaultFontSize_6;
	// UnityEngine.FontStyle Utage.UguiNovelTextCharacter::fontStyle
	int32_t ___fontStyle_7;
	// UnityEngine.UIVertex[] Utage.UguiNovelTextCharacter::verts
	UIVertexU5BU5D_t3048644023* ___verts_8;
	// System.Single Utage.UguiNovelTextCharacter::<X0>k__BackingField
	float ___U3CX0U3Ek__BackingField_9;
	// System.Single Utage.UguiNovelTextCharacter::<Y0>k__BackingField
	float ___U3CY0U3Ek__BackingField_10;
	// System.Single Utage.UguiNovelTextCharacter::<OffsetX>k__BackingField
	float ___U3COffsetXU3Ek__BackingField_11;
	// System.Single Utage.UguiNovelTextCharacter::<OffsetY>k__BackingField
	float ___U3COffsetYU3Ek__BackingField_12;
	// System.Boolean Utage.UguiNovelTextCharacter::isError
	bool ___isError_13;
	// System.Boolean Utage.UguiNovelTextCharacter::<CustomSpace>k__BackingField
	bool ___U3CCustomSpaceU3Ek__BackingField_14;
	// System.Boolean Utage.UguiNovelTextCharacter::isSprite
	bool ___isSprite_15;
	// System.Single Utage.UguiNovelTextCharacter::<RubySpaceSize>k__BackingField
	float ___U3CRubySpaceSizeU3Ek__BackingField_16;
	// System.Boolean Utage.UguiNovelTextCharacter::<IsVisible>k__BackingField
	bool ___U3CIsVisibleU3Ek__BackingField_17;
	// UnityEngine.Color Utage.UguiNovelTextCharacter::effectColor
	Color_t2020392075  ___effectColor_18;
	// System.Single Utage.UguiNovelTextCharacter::<BmpFontScale>k__BackingField
	float ___U3CBmpFontScaleU3Ek__BackingField_19;

public:
	inline static int32_t get_offset_of_isAutoLineBreak_0() { return static_cast<int32_t>(offsetof(UguiNovelTextCharacter_t1244198124, ___isAutoLineBreak_0)); }
	inline bool get_isAutoLineBreak_0() const { return ___isAutoLineBreak_0; }
	inline bool* get_address_of_isAutoLineBreak_0() { return &___isAutoLineBreak_0; }
	inline void set_isAutoLineBreak_0(bool value)
	{
		___isAutoLineBreak_0 = value;
	}

	inline static int32_t get_offset_of_isKinsokuBurasage_1() { return static_cast<int32_t>(offsetof(UguiNovelTextCharacter_t1244198124, ___isKinsokuBurasage_1)); }
	inline bool get_isKinsokuBurasage_1() const { return ___isKinsokuBurasage_1; }
	inline bool* get_address_of_isKinsokuBurasage_1() { return &___isKinsokuBurasage_1; }
	inline void set_isKinsokuBurasage_1(bool value)
	{
		___isKinsokuBurasage_1 = value;
	}

	inline static int32_t get_offset_of_charData_2() { return static_cast<int32_t>(offsetof(UguiNovelTextCharacter_t1244198124, ___charData_2)); }
	inline CharData_t4178071270 * get_charData_2() const { return ___charData_2; }
	inline CharData_t4178071270 ** get_address_of_charData_2() { return &___charData_2; }
	inline void set_charData_2(CharData_t4178071270 * value)
	{
		___charData_2 = value;
		Il2CppCodeGenWriteBarrier((&___charData_2), value);
	}

	inline static int32_t get_offset_of_charInfo_3() { return static_cast<int32_t>(offsetof(UguiNovelTextCharacter_t1244198124, ___charInfo_3)); }
	inline CharacterInfo_t3919092135  get_charInfo_3() const { return ___charInfo_3; }
	inline CharacterInfo_t3919092135 * get_address_of_charInfo_3() { return &___charInfo_3; }
	inline void set_charInfo_3(CharacterInfo_t3919092135  value)
	{
		___charInfo_3 = value;
	}

	inline static int32_t get_offset_of_width_4() { return static_cast<int32_t>(offsetof(UguiNovelTextCharacter_t1244198124, ___width_4)); }
	inline float get_width_4() const { return ___width_4; }
	inline float* get_address_of_width_4() { return &___width_4; }
	inline void set_width_4(float value)
	{
		___width_4 = value;
	}

	inline static int32_t get_offset_of_fontSize_5() { return static_cast<int32_t>(offsetof(UguiNovelTextCharacter_t1244198124, ___fontSize_5)); }
	inline int32_t get_fontSize_5() const { return ___fontSize_5; }
	inline int32_t* get_address_of_fontSize_5() { return &___fontSize_5; }
	inline void set_fontSize_5(int32_t value)
	{
		___fontSize_5 = value;
	}

	inline static int32_t get_offset_of_defaultFontSize_6() { return static_cast<int32_t>(offsetof(UguiNovelTextCharacter_t1244198124, ___defaultFontSize_6)); }
	inline int32_t get_defaultFontSize_6() const { return ___defaultFontSize_6; }
	inline int32_t* get_address_of_defaultFontSize_6() { return &___defaultFontSize_6; }
	inline void set_defaultFontSize_6(int32_t value)
	{
		___defaultFontSize_6 = value;
	}

	inline static int32_t get_offset_of_fontStyle_7() { return static_cast<int32_t>(offsetof(UguiNovelTextCharacter_t1244198124, ___fontStyle_7)); }
	inline int32_t get_fontStyle_7() const { return ___fontStyle_7; }
	inline int32_t* get_address_of_fontStyle_7() { return &___fontStyle_7; }
	inline void set_fontStyle_7(int32_t value)
	{
		___fontStyle_7 = value;
	}

	inline static int32_t get_offset_of_verts_8() { return static_cast<int32_t>(offsetof(UguiNovelTextCharacter_t1244198124, ___verts_8)); }
	inline UIVertexU5BU5D_t3048644023* get_verts_8() const { return ___verts_8; }
	inline UIVertexU5BU5D_t3048644023** get_address_of_verts_8() { return &___verts_8; }
	inline void set_verts_8(UIVertexU5BU5D_t3048644023* value)
	{
		___verts_8 = value;
		Il2CppCodeGenWriteBarrier((&___verts_8), value);
	}

	inline static int32_t get_offset_of_U3CX0U3Ek__BackingField_9() { return static_cast<int32_t>(offsetof(UguiNovelTextCharacter_t1244198124, ___U3CX0U3Ek__BackingField_9)); }
	inline float get_U3CX0U3Ek__BackingField_9() const { return ___U3CX0U3Ek__BackingField_9; }
	inline float* get_address_of_U3CX0U3Ek__BackingField_9() { return &___U3CX0U3Ek__BackingField_9; }
	inline void set_U3CX0U3Ek__BackingField_9(float value)
	{
		___U3CX0U3Ek__BackingField_9 = value;
	}

	inline static int32_t get_offset_of_U3CY0U3Ek__BackingField_10() { return static_cast<int32_t>(offsetof(UguiNovelTextCharacter_t1244198124, ___U3CY0U3Ek__BackingField_10)); }
	inline float get_U3CY0U3Ek__BackingField_10() const { return ___U3CY0U3Ek__BackingField_10; }
	inline float* get_address_of_U3CY0U3Ek__BackingField_10() { return &___U3CY0U3Ek__BackingField_10; }
	inline void set_U3CY0U3Ek__BackingField_10(float value)
	{
		___U3CY0U3Ek__BackingField_10 = value;
	}

	inline static int32_t get_offset_of_U3COffsetXU3Ek__BackingField_11() { return static_cast<int32_t>(offsetof(UguiNovelTextCharacter_t1244198124, ___U3COffsetXU3Ek__BackingField_11)); }
	inline float get_U3COffsetXU3Ek__BackingField_11() const { return ___U3COffsetXU3Ek__BackingField_11; }
	inline float* get_address_of_U3COffsetXU3Ek__BackingField_11() { return &___U3COffsetXU3Ek__BackingField_11; }
	inline void set_U3COffsetXU3Ek__BackingField_11(float value)
	{
		___U3COffsetXU3Ek__BackingField_11 = value;
	}

	inline static int32_t get_offset_of_U3COffsetYU3Ek__BackingField_12() { return static_cast<int32_t>(offsetof(UguiNovelTextCharacter_t1244198124, ___U3COffsetYU3Ek__BackingField_12)); }
	inline float get_U3COffsetYU3Ek__BackingField_12() const { return ___U3COffsetYU3Ek__BackingField_12; }
	inline float* get_address_of_U3COffsetYU3Ek__BackingField_12() { return &___U3COffsetYU3Ek__BackingField_12; }
	inline void set_U3COffsetYU3Ek__BackingField_12(float value)
	{
		___U3COffsetYU3Ek__BackingField_12 = value;
	}

	inline static int32_t get_offset_of_isError_13() { return static_cast<int32_t>(offsetof(UguiNovelTextCharacter_t1244198124, ___isError_13)); }
	inline bool get_isError_13() const { return ___isError_13; }
	inline bool* get_address_of_isError_13() { return &___isError_13; }
	inline void set_isError_13(bool value)
	{
		___isError_13 = value;
	}

	inline static int32_t get_offset_of_U3CCustomSpaceU3Ek__BackingField_14() { return static_cast<int32_t>(offsetof(UguiNovelTextCharacter_t1244198124, ___U3CCustomSpaceU3Ek__BackingField_14)); }
	inline bool get_U3CCustomSpaceU3Ek__BackingField_14() const { return ___U3CCustomSpaceU3Ek__BackingField_14; }
	inline bool* get_address_of_U3CCustomSpaceU3Ek__BackingField_14() { return &___U3CCustomSpaceU3Ek__BackingField_14; }
	inline void set_U3CCustomSpaceU3Ek__BackingField_14(bool value)
	{
		___U3CCustomSpaceU3Ek__BackingField_14 = value;
	}

	inline static int32_t get_offset_of_isSprite_15() { return static_cast<int32_t>(offsetof(UguiNovelTextCharacter_t1244198124, ___isSprite_15)); }
	inline bool get_isSprite_15() const { return ___isSprite_15; }
	inline bool* get_address_of_isSprite_15() { return &___isSprite_15; }
	inline void set_isSprite_15(bool value)
	{
		___isSprite_15 = value;
	}

	inline static int32_t get_offset_of_U3CRubySpaceSizeU3Ek__BackingField_16() { return static_cast<int32_t>(offsetof(UguiNovelTextCharacter_t1244198124, ___U3CRubySpaceSizeU3Ek__BackingField_16)); }
	inline float get_U3CRubySpaceSizeU3Ek__BackingField_16() const { return ___U3CRubySpaceSizeU3Ek__BackingField_16; }
	inline float* get_address_of_U3CRubySpaceSizeU3Ek__BackingField_16() { return &___U3CRubySpaceSizeU3Ek__BackingField_16; }
	inline void set_U3CRubySpaceSizeU3Ek__BackingField_16(float value)
	{
		___U3CRubySpaceSizeU3Ek__BackingField_16 = value;
	}

	inline static int32_t get_offset_of_U3CIsVisibleU3Ek__BackingField_17() { return static_cast<int32_t>(offsetof(UguiNovelTextCharacter_t1244198124, ___U3CIsVisibleU3Ek__BackingField_17)); }
	inline bool get_U3CIsVisibleU3Ek__BackingField_17() const { return ___U3CIsVisibleU3Ek__BackingField_17; }
	inline bool* get_address_of_U3CIsVisibleU3Ek__BackingField_17() { return &___U3CIsVisibleU3Ek__BackingField_17; }
	inline void set_U3CIsVisibleU3Ek__BackingField_17(bool value)
	{
		___U3CIsVisibleU3Ek__BackingField_17 = value;
	}

	inline static int32_t get_offset_of_effectColor_18() { return static_cast<int32_t>(offsetof(UguiNovelTextCharacter_t1244198124, ___effectColor_18)); }
	inline Color_t2020392075  get_effectColor_18() const { return ___effectColor_18; }
	inline Color_t2020392075 * get_address_of_effectColor_18() { return &___effectColor_18; }
	inline void set_effectColor_18(Color_t2020392075  value)
	{
		___effectColor_18 = value;
	}

	inline static int32_t get_offset_of_U3CBmpFontScaleU3Ek__BackingField_19() { return static_cast<int32_t>(offsetof(UguiNovelTextCharacter_t1244198124, ___U3CBmpFontScaleU3Ek__BackingField_19)); }
	inline float get_U3CBmpFontScaleU3Ek__BackingField_19() const { return ___U3CBmpFontScaleU3Ek__BackingField_19; }
	inline float* get_address_of_U3CBmpFontScaleU3Ek__BackingField_19() { return &___U3CBmpFontScaleU3Ek__BackingField_19; }
	inline void set_U3CBmpFontScaleU3Ek__BackingField_19(float value)
	{
		___U3CBmpFontScaleU3Ek__BackingField_19 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UGUINOVELTEXTCHARACTER_T1244198124_H
#ifndef UGUINOVELTEXTEMOJIDATA_T3935078235_H
#define UGUINOVELTEXTEMOJIDATA_T3935078235_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.UguiNovelTextEmojiData
struct  UguiNovelTextEmojiData_t3935078235  : public ScriptableObject_t1975622470
{
public:
	// System.Int32 Utage.UguiNovelTextEmojiData::size
	int32_t ___size_2;
	// System.Collections.Generic.List`1<UnityEngine.Sprite> Utage.UguiNovelTextEmojiData::spriteTbl
	List_1_t3973682211 * ___spriteTbl_3;
	// System.Collections.Generic.Dictionary`2<System.Char,UnityEngine.Sprite> Utage.UguiNovelTextEmojiData::spriteDictionary
	Dictionary_2_t1910071072 * ___spriteDictionary_4;
	// System.Collections.Generic.Dictionary`2<System.String,UnityEngine.Sprite> Utage.UguiNovelTextEmojiData::spriteDictionaryStringKey
	Dictionary_2_t2224373045 * ___spriteDictionaryStringKey_5;

public:
	inline static int32_t get_offset_of_size_2() { return static_cast<int32_t>(offsetof(UguiNovelTextEmojiData_t3935078235, ___size_2)); }
	inline int32_t get_size_2() const { return ___size_2; }
	inline int32_t* get_address_of_size_2() { return &___size_2; }
	inline void set_size_2(int32_t value)
	{
		___size_2 = value;
	}

	inline static int32_t get_offset_of_spriteTbl_3() { return static_cast<int32_t>(offsetof(UguiNovelTextEmojiData_t3935078235, ___spriteTbl_3)); }
	inline List_1_t3973682211 * get_spriteTbl_3() const { return ___spriteTbl_3; }
	inline List_1_t3973682211 ** get_address_of_spriteTbl_3() { return &___spriteTbl_3; }
	inline void set_spriteTbl_3(List_1_t3973682211 * value)
	{
		___spriteTbl_3 = value;
		Il2CppCodeGenWriteBarrier((&___spriteTbl_3), value);
	}

	inline static int32_t get_offset_of_spriteDictionary_4() { return static_cast<int32_t>(offsetof(UguiNovelTextEmojiData_t3935078235, ___spriteDictionary_4)); }
	inline Dictionary_2_t1910071072 * get_spriteDictionary_4() const { return ___spriteDictionary_4; }
	inline Dictionary_2_t1910071072 ** get_address_of_spriteDictionary_4() { return &___spriteDictionary_4; }
	inline void set_spriteDictionary_4(Dictionary_2_t1910071072 * value)
	{
		___spriteDictionary_4 = value;
		Il2CppCodeGenWriteBarrier((&___spriteDictionary_4), value);
	}

	inline static int32_t get_offset_of_spriteDictionaryStringKey_5() { return static_cast<int32_t>(offsetof(UguiNovelTextEmojiData_t3935078235, ___spriteDictionaryStringKey_5)); }
	inline Dictionary_2_t2224373045 * get_spriteDictionaryStringKey_5() const { return ___spriteDictionaryStringKey_5; }
	inline Dictionary_2_t2224373045 ** get_address_of_spriteDictionaryStringKey_5() { return &___spriteDictionaryStringKey_5; }
	inline void set_spriteDictionaryStringKey_5(Dictionary_2_t2224373045 * value)
	{
		___spriteDictionaryStringKey_5 = value;
		Il2CppCodeGenWriteBarrier((&___spriteDictionaryStringKey_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UGUINOVELTEXTEMOJIDATA_T3935078235_H
#ifndef MONOBEHAVIOUR_T1158329972_H
#define MONOBEHAVIOUR_T1158329972_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.MonoBehaviour
struct  MonoBehaviour_t1158329972  : public Behaviour_t955675639
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MONOBEHAVIOUR_T1158329972_H
#ifndef UGUILOCALIZEBASE_T685439682_H
#define UGUILOCALIZEBASE_T685439682_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.UguiLocalizeBase
struct  UguiLocalizeBase_t685439682  : public MonoBehaviour_t1158329972
{
public:
	// System.String Utage.UguiLocalizeBase::currentLanguage
	String_t* ___currentLanguage_2;
	// System.Boolean Utage.UguiLocalizeBase::isInit
	bool ___isInit_3;

public:
	inline static int32_t get_offset_of_currentLanguage_2() { return static_cast<int32_t>(offsetof(UguiLocalizeBase_t685439682, ___currentLanguage_2)); }
	inline String_t* get_currentLanguage_2() const { return ___currentLanguage_2; }
	inline String_t** get_address_of_currentLanguage_2() { return &___currentLanguage_2; }
	inline void set_currentLanguage_2(String_t* value)
	{
		___currentLanguage_2 = value;
		Il2CppCodeGenWriteBarrier((&___currentLanguage_2), value);
	}

	inline static int32_t get_offset_of_isInit_3() { return static_cast<int32_t>(offsetof(UguiLocalizeBase_t685439682, ___isInit_3)); }
	inline bool get_isInit_3() const { return ___isInit_3; }
	inline bool* get_address_of_isInit_3() { return &___isInit_3; }
	inline void set_isInit_3(bool value)
	{
		___isInit_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UGUILOCALIZEBASE_T685439682_H
#ifndef UIBEHAVIOUR_T3960014691_H
#define UIBEHAVIOUR_T3960014691_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.EventSystems.UIBehaviour
struct  UIBehaviour_t3960014691  : public MonoBehaviour_t1158329972
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UIBEHAVIOUR_T3960014691_H
#ifndef UGUINOVELTEXTGENERATOR_T4216084310_H
#define UGUINOVELTEXTGENERATOR_T4216084310_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.UguiNovelTextGenerator
struct  UguiNovelTextGenerator_t4216084310  : public MonoBehaviour_t1158329972
{
public:
	// Utage.UguiNovelText Utage.UguiNovelTextGenerator::novelText
	UguiNovelText_t4135744055 * ___novelText_2;
	// Utage.TextData Utage.UguiNovelTextGenerator::textData
	TextData_t603454315 * ___textData_3;
	// System.Single Utage.UguiNovelTextGenerator::<MaxWidth>k__BackingField
	float ___U3CMaxWidthU3Ek__BackingField_4;
	// System.Single Utage.UguiNovelTextGenerator::<MaxHeight>k__BackingField
	float ___U3CMaxHeightU3Ek__BackingField_5;
	// System.Single Utage.UguiNovelTextGenerator::preferredHeight
	float ___preferredHeight_6;
	// System.Single Utage.UguiNovelTextGenerator::preferredWidth
	float ___preferredWidth_7;
	// System.Single Utage.UguiNovelTextGenerator::height
	float ___height_8;
	// System.Single Utage.UguiNovelTextGenerator::space
	float ___space_9;
	// System.Single Utage.UguiNovelTextGenerator::letterSpaceSize
	float ___letterSpaceSize_10;
	// Utage.UguiNovelTextGenerator/WordWrap Utage.UguiNovelTextGenerator::wordWrap
	int32_t ___wordWrap_11;
	// System.Int32 Utage.UguiNovelTextGenerator::lengthOfView
	int32_t ___lengthOfView_12;
	// Utage.UguiNovelTextSettings Utage.UguiNovelTextGenerator::textSettings
	UguiNovelTextSettings_t4201484304 * ___textSettings_13;
	// System.Single Utage.UguiNovelTextGenerator::rubySizeScale
	float ___rubySizeScale_14;
	// System.Single Utage.UguiNovelTextGenerator::supOrSubSizeScale
	float ___supOrSubSizeScale_15;
	// Utage.UguiNovelTextEmojiData Utage.UguiNovelTextGenerator::emojiData
	UguiNovelTextEmojiData_t3935078235 * ___emojiData_16;
	// System.Char Utage.UguiNovelTextGenerator::dashChar
	Il2CppChar ___dashChar_17;
	// System.Int32 Utage.UguiNovelTextGenerator::bmpFontSize
	int32_t ___bmpFontSize_18;
	// System.Boolean Utage.UguiNovelTextGenerator::isUnicodeFont
	bool ___isUnicodeFont_19;
	// UnityEngine.RectTransform Utage.UguiNovelTextGenerator::cachedRectTransform
	RectTransform_t3349966182 * ___cachedRectTransform_20;
	// System.Collections.Generic.List`1<Utage.UguiNovelTextCharacter> Utage.UguiNovelTextGenerator::characterDataList
	List_1_t613319256 * ___characterDataList_21;
	// System.Collections.Generic.List`1<Utage.UguiNovelTextLine> Utage.UguiNovelTextGenerator::lineDataList
	List_1_t1808884665 * ___lineDataList_22;
	// UnityEngine.Vector3 Utage.UguiNovelTextGenerator::endPosition
	Vector3_t2243707580  ___endPosition_23;
	// Utage.UguiNovelTextGeneratorAdditional Utage.UguiNovelTextGenerator::additional
	UguiNovelTextGeneratorAdditional_t151164365 * ___additional_24;
	// System.Collections.Generic.List`1<Utage.UguiNovelTextGenerator/GraphicObject> Utage.UguiNovelTextGenerator::graphicObjectList
	List_1_t502578610 * ___graphicObjectList_25;
	// System.Boolean Utage.UguiNovelTextGenerator::isInitGraphicObjectList
	bool ___isInitGraphicObjectList_26;
	// System.Collections.Generic.List`1<Utage.UguiNovelTextHitArea> Utage.UguiNovelTextGenerator::hitGroupLists
	List_1_t948532665 * ___hitGroupLists_27;
	// System.Boolean Utage.UguiNovelTextGenerator::isDebugLog
	bool ___isDebugLog_28;
	// Utage.UguiNovelTextGenerator/ChagneType Utage.UguiNovelTextGenerator::<CurrentChangeType>k__BackingField
	int32_t ___U3CCurrentChangeTypeU3Ek__BackingField_29;
	// System.Boolean Utage.UguiNovelTextGenerator::<IsRebuidFont>k__BackingField
	bool ___U3CIsRebuidFontU3Ek__BackingField_30;
	// System.Boolean Utage.UguiNovelTextGenerator::isRequestingCharactersInTexture
	bool ___isRequestingCharactersInTexture_31;

public:
	inline static int32_t get_offset_of_novelText_2() { return static_cast<int32_t>(offsetof(UguiNovelTextGenerator_t4216084310, ___novelText_2)); }
	inline UguiNovelText_t4135744055 * get_novelText_2() const { return ___novelText_2; }
	inline UguiNovelText_t4135744055 ** get_address_of_novelText_2() { return &___novelText_2; }
	inline void set_novelText_2(UguiNovelText_t4135744055 * value)
	{
		___novelText_2 = value;
		Il2CppCodeGenWriteBarrier((&___novelText_2), value);
	}

	inline static int32_t get_offset_of_textData_3() { return static_cast<int32_t>(offsetof(UguiNovelTextGenerator_t4216084310, ___textData_3)); }
	inline TextData_t603454315 * get_textData_3() const { return ___textData_3; }
	inline TextData_t603454315 ** get_address_of_textData_3() { return &___textData_3; }
	inline void set_textData_3(TextData_t603454315 * value)
	{
		___textData_3 = value;
		Il2CppCodeGenWriteBarrier((&___textData_3), value);
	}

	inline static int32_t get_offset_of_U3CMaxWidthU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(UguiNovelTextGenerator_t4216084310, ___U3CMaxWidthU3Ek__BackingField_4)); }
	inline float get_U3CMaxWidthU3Ek__BackingField_4() const { return ___U3CMaxWidthU3Ek__BackingField_4; }
	inline float* get_address_of_U3CMaxWidthU3Ek__BackingField_4() { return &___U3CMaxWidthU3Ek__BackingField_4; }
	inline void set_U3CMaxWidthU3Ek__BackingField_4(float value)
	{
		___U3CMaxWidthU3Ek__BackingField_4 = value;
	}

	inline static int32_t get_offset_of_U3CMaxHeightU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(UguiNovelTextGenerator_t4216084310, ___U3CMaxHeightU3Ek__BackingField_5)); }
	inline float get_U3CMaxHeightU3Ek__BackingField_5() const { return ___U3CMaxHeightU3Ek__BackingField_5; }
	inline float* get_address_of_U3CMaxHeightU3Ek__BackingField_5() { return &___U3CMaxHeightU3Ek__BackingField_5; }
	inline void set_U3CMaxHeightU3Ek__BackingField_5(float value)
	{
		___U3CMaxHeightU3Ek__BackingField_5 = value;
	}

	inline static int32_t get_offset_of_preferredHeight_6() { return static_cast<int32_t>(offsetof(UguiNovelTextGenerator_t4216084310, ___preferredHeight_6)); }
	inline float get_preferredHeight_6() const { return ___preferredHeight_6; }
	inline float* get_address_of_preferredHeight_6() { return &___preferredHeight_6; }
	inline void set_preferredHeight_6(float value)
	{
		___preferredHeight_6 = value;
	}

	inline static int32_t get_offset_of_preferredWidth_7() { return static_cast<int32_t>(offsetof(UguiNovelTextGenerator_t4216084310, ___preferredWidth_7)); }
	inline float get_preferredWidth_7() const { return ___preferredWidth_7; }
	inline float* get_address_of_preferredWidth_7() { return &___preferredWidth_7; }
	inline void set_preferredWidth_7(float value)
	{
		___preferredWidth_7 = value;
	}

	inline static int32_t get_offset_of_height_8() { return static_cast<int32_t>(offsetof(UguiNovelTextGenerator_t4216084310, ___height_8)); }
	inline float get_height_8() const { return ___height_8; }
	inline float* get_address_of_height_8() { return &___height_8; }
	inline void set_height_8(float value)
	{
		___height_8 = value;
	}

	inline static int32_t get_offset_of_space_9() { return static_cast<int32_t>(offsetof(UguiNovelTextGenerator_t4216084310, ___space_9)); }
	inline float get_space_9() const { return ___space_9; }
	inline float* get_address_of_space_9() { return &___space_9; }
	inline void set_space_9(float value)
	{
		___space_9 = value;
	}

	inline static int32_t get_offset_of_letterSpaceSize_10() { return static_cast<int32_t>(offsetof(UguiNovelTextGenerator_t4216084310, ___letterSpaceSize_10)); }
	inline float get_letterSpaceSize_10() const { return ___letterSpaceSize_10; }
	inline float* get_address_of_letterSpaceSize_10() { return &___letterSpaceSize_10; }
	inline void set_letterSpaceSize_10(float value)
	{
		___letterSpaceSize_10 = value;
	}

	inline static int32_t get_offset_of_wordWrap_11() { return static_cast<int32_t>(offsetof(UguiNovelTextGenerator_t4216084310, ___wordWrap_11)); }
	inline int32_t get_wordWrap_11() const { return ___wordWrap_11; }
	inline int32_t* get_address_of_wordWrap_11() { return &___wordWrap_11; }
	inline void set_wordWrap_11(int32_t value)
	{
		___wordWrap_11 = value;
	}

	inline static int32_t get_offset_of_lengthOfView_12() { return static_cast<int32_t>(offsetof(UguiNovelTextGenerator_t4216084310, ___lengthOfView_12)); }
	inline int32_t get_lengthOfView_12() const { return ___lengthOfView_12; }
	inline int32_t* get_address_of_lengthOfView_12() { return &___lengthOfView_12; }
	inline void set_lengthOfView_12(int32_t value)
	{
		___lengthOfView_12 = value;
	}

	inline static int32_t get_offset_of_textSettings_13() { return static_cast<int32_t>(offsetof(UguiNovelTextGenerator_t4216084310, ___textSettings_13)); }
	inline UguiNovelTextSettings_t4201484304 * get_textSettings_13() const { return ___textSettings_13; }
	inline UguiNovelTextSettings_t4201484304 ** get_address_of_textSettings_13() { return &___textSettings_13; }
	inline void set_textSettings_13(UguiNovelTextSettings_t4201484304 * value)
	{
		___textSettings_13 = value;
		Il2CppCodeGenWriteBarrier((&___textSettings_13), value);
	}

	inline static int32_t get_offset_of_rubySizeScale_14() { return static_cast<int32_t>(offsetof(UguiNovelTextGenerator_t4216084310, ___rubySizeScale_14)); }
	inline float get_rubySizeScale_14() const { return ___rubySizeScale_14; }
	inline float* get_address_of_rubySizeScale_14() { return &___rubySizeScale_14; }
	inline void set_rubySizeScale_14(float value)
	{
		___rubySizeScale_14 = value;
	}

	inline static int32_t get_offset_of_supOrSubSizeScale_15() { return static_cast<int32_t>(offsetof(UguiNovelTextGenerator_t4216084310, ___supOrSubSizeScale_15)); }
	inline float get_supOrSubSizeScale_15() const { return ___supOrSubSizeScale_15; }
	inline float* get_address_of_supOrSubSizeScale_15() { return &___supOrSubSizeScale_15; }
	inline void set_supOrSubSizeScale_15(float value)
	{
		___supOrSubSizeScale_15 = value;
	}

	inline static int32_t get_offset_of_emojiData_16() { return static_cast<int32_t>(offsetof(UguiNovelTextGenerator_t4216084310, ___emojiData_16)); }
	inline UguiNovelTextEmojiData_t3935078235 * get_emojiData_16() const { return ___emojiData_16; }
	inline UguiNovelTextEmojiData_t3935078235 ** get_address_of_emojiData_16() { return &___emojiData_16; }
	inline void set_emojiData_16(UguiNovelTextEmojiData_t3935078235 * value)
	{
		___emojiData_16 = value;
		Il2CppCodeGenWriteBarrier((&___emojiData_16), value);
	}

	inline static int32_t get_offset_of_dashChar_17() { return static_cast<int32_t>(offsetof(UguiNovelTextGenerator_t4216084310, ___dashChar_17)); }
	inline Il2CppChar get_dashChar_17() const { return ___dashChar_17; }
	inline Il2CppChar* get_address_of_dashChar_17() { return &___dashChar_17; }
	inline void set_dashChar_17(Il2CppChar value)
	{
		___dashChar_17 = value;
	}

	inline static int32_t get_offset_of_bmpFontSize_18() { return static_cast<int32_t>(offsetof(UguiNovelTextGenerator_t4216084310, ___bmpFontSize_18)); }
	inline int32_t get_bmpFontSize_18() const { return ___bmpFontSize_18; }
	inline int32_t* get_address_of_bmpFontSize_18() { return &___bmpFontSize_18; }
	inline void set_bmpFontSize_18(int32_t value)
	{
		___bmpFontSize_18 = value;
	}

	inline static int32_t get_offset_of_isUnicodeFont_19() { return static_cast<int32_t>(offsetof(UguiNovelTextGenerator_t4216084310, ___isUnicodeFont_19)); }
	inline bool get_isUnicodeFont_19() const { return ___isUnicodeFont_19; }
	inline bool* get_address_of_isUnicodeFont_19() { return &___isUnicodeFont_19; }
	inline void set_isUnicodeFont_19(bool value)
	{
		___isUnicodeFont_19 = value;
	}

	inline static int32_t get_offset_of_cachedRectTransform_20() { return static_cast<int32_t>(offsetof(UguiNovelTextGenerator_t4216084310, ___cachedRectTransform_20)); }
	inline RectTransform_t3349966182 * get_cachedRectTransform_20() const { return ___cachedRectTransform_20; }
	inline RectTransform_t3349966182 ** get_address_of_cachedRectTransform_20() { return &___cachedRectTransform_20; }
	inline void set_cachedRectTransform_20(RectTransform_t3349966182 * value)
	{
		___cachedRectTransform_20 = value;
		Il2CppCodeGenWriteBarrier((&___cachedRectTransform_20), value);
	}

	inline static int32_t get_offset_of_characterDataList_21() { return static_cast<int32_t>(offsetof(UguiNovelTextGenerator_t4216084310, ___characterDataList_21)); }
	inline List_1_t613319256 * get_characterDataList_21() const { return ___characterDataList_21; }
	inline List_1_t613319256 ** get_address_of_characterDataList_21() { return &___characterDataList_21; }
	inline void set_characterDataList_21(List_1_t613319256 * value)
	{
		___characterDataList_21 = value;
		Il2CppCodeGenWriteBarrier((&___characterDataList_21), value);
	}

	inline static int32_t get_offset_of_lineDataList_22() { return static_cast<int32_t>(offsetof(UguiNovelTextGenerator_t4216084310, ___lineDataList_22)); }
	inline List_1_t1808884665 * get_lineDataList_22() const { return ___lineDataList_22; }
	inline List_1_t1808884665 ** get_address_of_lineDataList_22() { return &___lineDataList_22; }
	inline void set_lineDataList_22(List_1_t1808884665 * value)
	{
		___lineDataList_22 = value;
		Il2CppCodeGenWriteBarrier((&___lineDataList_22), value);
	}

	inline static int32_t get_offset_of_endPosition_23() { return static_cast<int32_t>(offsetof(UguiNovelTextGenerator_t4216084310, ___endPosition_23)); }
	inline Vector3_t2243707580  get_endPosition_23() const { return ___endPosition_23; }
	inline Vector3_t2243707580 * get_address_of_endPosition_23() { return &___endPosition_23; }
	inline void set_endPosition_23(Vector3_t2243707580  value)
	{
		___endPosition_23 = value;
	}

	inline static int32_t get_offset_of_additional_24() { return static_cast<int32_t>(offsetof(UguiNovelTextGenerator_t4216084310, ___additional_24)); }
	inline UguiNovelTextGeneratorAdditional_t151164365 * get_additional_24() const { return ___additional_24; }
	inline UguiNovelTextGeneratorAdditional_t151164365 ** get_address_of_additional_24() { return &___additional_24; }
	inline void set_additional_24(UguiNovelTextGeneratorAdditional_t151164365 * value)
	{
		___additional_24 = value;
		Il2CppCodeGenWriteBarrier((&___additional_24), value);
	}

	inline static int32_t get_offset_of_graphicObjectList_25() { return static_cast<int32_t>(offsetof(UguiNovelTextGenerator_t4216084310, ___graphicObjectList_25)); }
	inline List_1_t502578610 * get_graphicObjectList_25() const { return ___graphicObjectList_25; }
	inline List_1_t502578610 ** get_address_of_graphicObjectList_25() { return &___graphicObjectList_25; }
	inline void set_graphicObjectList_25(List_1_t502578610 * value)
	{
		___graphicObjectList_25 = value;
		Il2CppCodeGenWriteBarrier((&___graphicObjectList_25), value);
	}

	inline static int32_t get_offset_of_isInitGraphicObjectList_26() { return static_cast<int32_t>(offsetof(UguiNovelTextGenerator_t4216084310, ___isInitGraphicObjectList_26)); }
	inline bool get_isInitGraphicObjectList_26() const { return ___isInitGraphicObjectList_26; }
	inline bool* get_address_of_isInitGraphicObjectList_26() { return &___isInitGraphicObjectList_26; }
	inline void set_isInitGraphicObjectList_26(bool value)
	{
		___isInitGraphicObjectList_26 = value;
	}

	inline static int32_t get_offset_of_hitGroupLists_27() { return static_cast<int32_t>(offsetof(UguiNovelTextGenerator_t4216084310, ___hitGroupLists_27)); }
	inline List_1_t948532665 * get_hitGroupLists_27() const { return ___hitGroupLists_27; }
	inline List_1_t948532665 ** get_address_of_hitGroupLists_27() { return &___hitGroupLists_27; }
	inline void set_hitGroupLists_27(List_1_t948532665 * value)
	{
		___hitGroupLists_27 = value;
		Il2CppCodeGenWriteBarrier((&___hitGroupLists_27), value);
	}

	inline static int32_t get_offset_of_isDebugLog_28() { return static_cast<int32_t>(offsetof(UguiNovelTextGenerator_t4216084310, ___isDebugLog_28)); }
	inline bool get_isDebugLog_28() const { return ___isDebugLog_28; }
	inline bool* get_address_of_isDebugLog_28() { return &___isDebugLog_28; }
	inline void set_isDebugLog_28(bool value)
	{
		___isDebugLog_28 = value;
	}

	inline static int32_t get_offset_of_U3CCurrentChangeTypeU3Ek__BackingField_29() { return static_cast<int32_t>(offsetof(UguiNovelTextGenerator_t4216084310, ___U3CCurrentChangeTypeU3Ek__BackingField_29)); }
	inline int32_t get_U3CCurrentChangeTypeU3Ek__BackingField_29() const { return ___U3CCurrentChangeTypeU3Ek__BackingField_29; }
	inline int32_t* get_address_of_U3CCurrentChangeTypeU3Ek__BackingField_29() { return &___U3CCurrentChangeTypeU3Ek__BackingField_29; }
	inline void set_U3CCurrentChangeTypeU3Ek__BackingField_29(int32_t value)
	{
		___U3CCurrentChangeTypeU3Ek__BackingField_29 = value;
	}

	inline static int32_t get_offset_of_U3CIsRebuidFontU3Ek__BackingField_30() { return static_cast<int32_t>(offsetof(UguiNovelTextGenerator_t4216084310, ___U3CIsRebuidFontU3Ek__BackingField_30)); }
	inline bool get_U3CIsRebuidFontU3Ek__BackingField_30() const { return ___U3CIsRebuidFontU3Ek__BackingField_30; }
	inline bool* get_address_of_U3CIsRebuidFontU3Ek__BackingField_30() { return &___U3CIsRebuidFontU3Ek__BackingField_30; }
	inline void set_U3CIsRebuidFontU3Ek__BackingField_30(bool value)
	{
		___U3CIsRebuidFontU3Ek__BackingField_30 = value;
	}

	inline static int32_t get_offset_of_isRequestingCharactersInTexture_31() { return static_cast<int32_t>(offsetof(UguiNovelTextGenerator_t4216084310, ___isRequestingCharactersInTexture_31)); }
	inline bool get_isRequestingCharactersInTexture_31() const { return ___isRequestingCharactersInTexture_31; }
	inline bool* get_address_of_isRequestingCharactersInTexture_31() { return &___isRequestingCharactersInTexture_31; }
	inline void set_isRequestingCharactersInTexture_31(bool value)
	{
		___isRequestingCharactersInTexture_31 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UGUINOVELTEXTGENERATOR_T4216084310_H
#ifndef UGUINOVELTEXTEVENTTRIGGER_T710914779_H
#define UGUINOVELTEXTEVENTTRIGGER_T710914779_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.UguiNovelTextEventTrigger
struct  UguiNovelTextEventTrigger_t710914779  : public MonoBehaviour_t1158329972
{
public:
	// Utage.UguiNovelTextGenerator Utage.UguiNovelTextEventTrigger::generator
	UguiNovelTextGenerator_t4216084310 * ___generator_2;
	// Utage.UguiNovelText Utage.UguiNovelTextEventTrigger::novelText
	UguiNovelText_t4135744055 * ___novelText_3;
	// UnityEngine.RectTransform Utage.UguiNovelTextEventTrigger::cachedRectTransform
	RectTransform_t3349966182 * ___cachedRectTransform_4;
	// Utage.OnClickLinkEvent Utage.UguiNovelTextEventTrigger::OnClick
	OnClickLinkEvent_t134619553 * ___OnClick_5;
	// UnityEngine.Color Utage.UguiNovelTextEventTrigger::hoverColor
	Color_t2020392075  ___hoverColor_6;
	// Utage.UguiNovelTextHitArea Utage.UguiNovelTextEventTrigger::currentTarget
	UguiNovelTextHitArea_t1579411533 * ___currentTarget_7;
	// System.Boolean Utage.UguiNovelTextEventTrigger::isEntered
	bool ___isEntered_8;

public:
	inline static int32_t get_offset_of_generator_2() { return static_cast<int32_t>(offsetof(UguiNovelTextEventTrigger_t710914779, ___generator_2)); }
	inline UguiNovelTextGenerator_t4216084310 * get_generator_2() const { return ___generator_2; }
	inline UguiNovelTextGenerator_t4216084310 ** get_address_of_generator_2() { return &___generator_2; }
	inline void set_generator_2(UguiNovelTextGenerator_t4216084310 * value)
	{
		___generator_2 = value;
		Il2CppCodeGenWriteBarrier((&___generator_2), value);
	}

	inline static int32_t get_offset_of_novelText_3() { return static_cast<int32_t>(offsetof(UguiNovelTextEventTrigger_t710914779, ___novelText_3)); }
	inline UguiNovelText_t4135744055 * get_novelText_3() const { return ___novelText_3; }
	inline UguiNovelText_t4135744055 ** get_address_of_novelText_3() { return &___novelText_3; }
	inline void set_novelText_3(UguiNovelText_t4135744055 * value)
	{
		___novelText_3 = value;
		Il2CppCodeGenWriteBarrier((&___novelText_3), value);
	}

	inline static int32_t get_offset_of_cachedRectTransform_4() { return static_cast<int32_t>(offsetof(UguiNovelTextEventTrigger_t710914779, ___cachedRectTransform_4)); }
	inline RectTransform_t3349966182 * get_cachedRectTransform_4() const { return ___cachedRectTransform_4; }
	inline RectTransform_t3349966182 ** get_address_of_cachedRectTransform_4() { return &___cachedRectTransform_4; }
	inline void set_cachedRectTransform_4(RectTransform_t3349966182 * value)
	{
		___cachedRectTransform_4 = value;
		Il2CppCodeGenWriteBarrier((&___cachedRectTransform_4), value);
	}

	inline static int32_t get_offset_of_OnClick_5() { return static_cast<int32_t>(offsetof(UguiNovelTextEventTrigger_t710914779, ___OnClick_5)); }
	inline OnClickLinkEvent_t134619553 * get_OnClick_5() const { return ___OnClick_5; }
	inline OnClickLinkEvent_t134619553 ** get_address_of_OnClick_5() { return &___OnClick_5; }
	inline void set_OnClick_5(OnClickLinkEvent_t134619553 * value)
	{
		___OnClick_5 = value;
		Il2CppCodeGenWriteBarrier((&___OnClick_5), value);
	}

	inline static int32_t get_offset_of_hoverColor_6() { return static_cast<int32_t>(offsetof(UguiNovelTextEventTrigger_t710914779, ___hoverColor_6)); }
	inline Color_t2020392075  get_hoverColor_6() const { return ___hoverColor_6; }
	inline Color_t2020392075 * get_address_of_hoverColor_6() { return &___hoverColor_6; }
	inline void set_hoverColor_6(Color_t2020392075  value)
	{
		___hoverColor_6 = value;
	}

	inline static int32_t get_offset_of_currentTarget_7() { return static_cast<int32_t>(offsetof(UguiNovelTextEventTrigger_t710914779, ___currentTarget_7)); }
	inline UguiNovelTextHitArea_t1579411533 * get_currentTarget_7() const { return ___currentTarget_7; }
	inline UguiNovelTextHitArea_t1579411533 ** get_address_of_currentTarget_7() { return &___currentTarget_7; }
	inline void set_currentTarget_7(UguiNovelTextHitArea_t1579411533 * value)
	{
		___currentTarget_7 = value;
		Il2CppCodeGenWriteBarrier((&___currentTarget_7), value);
	}

	inline static int32_t get_offset_of_isEntered_8() { return static_cast<int32_t>(offsetof(UguiNovelTextEventTrigger_t710914779, ___isEntered_8)); }
	inline bool get_isEntered_8() const { return ___isEntered_8; }
	inline bool* get_address_of_isEntered_8() { return &___isEntered_8; }
	inline void set_isEntered_8(bool value)
	{
		___isEntered_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UGUINOVELTEXTEVENTTRIGGER_T710914779_H
#ifndef DICINGANIMATION_T3770383148_H
#define DICINGANIMATION_T3770383148_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.DicingAnimation
struct  DicingAnimation_t3770383148  : public MonoBehaviour_t1158329972
{
public:
	// System.Boolean Utage.DicingAnimation::playOnAwake
	bool ___playOnAwake_2;
	// Utage.MotionPlayType Utage.DicingAnimation::wrapMode
	int32_t ___wrapMode_3;
	// System.Boolean Utage.DicingAnimation::reverse
	bool ___reverse_4;
	// System.Single Utage.DicingAnimation::frameRate
	float ___frameRate_5;
	// Utage.DicingImage Utage.DicingAnimation::dicing
	DicingImage_t2721298607 * ___dicing_6;

public:
	inline static int32_t get_offset_of_playOnAwake_2() { return static_cast<int32_t>(offsetof(DicingAnimation_t3770383148, ___playOnAwake_2)); }
	inline bool get_playOnAwake_2() const { return ___playOnAwake_2; }
	inline bool* get_address_of_playOnAwake_2() { return &___playOnAwake_2; }
	inline void set_playOnAwake_2(bool value)
	{
		___playOnAwake_2 = value;
	}

	inline static int32_t get_offset_of_wrapMode_3() { return static_cast<int32_t>(offsetof(DicingAnimation_t3770383148, ___wrapMode_3)); }
	inline int32_t get_wrapMode_3() const { return ___wrapMode_3; }
	inline int32_t* get_address_of_wrapMode_3() { return &___wrapMode_3; }
	inline void set_wrapMode_3(int32_t value)
	{
		___wrapMode_3 = value;
	}

	inline static int32_t get_offset_of_reverse_4() { return static_cast<int32_t>(offsetof(DicingAnimation_t3770383148, ___reverse_4)); }
	inline bool get_reverse_4() const { return ___reverse_4; }
	inline bool* get_address_of_reverse_4() { return &___reverse_4; }
	inline void set_reverse_4(bool value)
	{
		___reverse_4 = value;
	}

	inline static int32_t get_offset_of_frameRate_5() { return static_cast<int32_t>(offsetof(DicingAnimation_t3770383148, ___frameRate_5)); }
	inline float get_frameRate_5() const { return ___frameRate_5; }
	inline float* get_address_of_frameRate_5() { return &___frameRate_5; }
	inline void set_frameRate_5(float value)
	{
		___frameRate_5 = value;
	}

	inline static int32_t get_offset_of_dicing_6() { return static_cast<int32_t>(offsetof(DicingAnimation_t3770383148, ___dicing_6)); }
	inline DicingImage_t2721298607 * get_dicing_6() const { return ___dicing_6; }
	inline DicingImage_t2721298607 ** get_address_of_dicing_6() { return &___dicing_6; }
	inline void set_dicing_6(DicingImage_t2721298607 * value)
	{
		___dicing_6 = value;
		Il2CppCodeGenWriteBarrier((&___dicing_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DICINGANIMATION_T3770383148_H
#ifndef EYEBLINKBASE_T1662968566_H
#define EYEBLINKBASE_T1662968566_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.EyeBlinkBase
struct  EyeBlinkBase_t1662968566  : public MonoBehaviour_t1158329972
{
public:
	// Utage.MinMaxFloat Utage.EyeBlinkBase::intervalTime
	MinMaxFloat_t1425080750 * ___intervalTime_2;
	// System.Single Utage.EyeBlinkBase::randomDoubleEyeBlink
	float ___randomDoubleEyeBlink_3;
	// System.Single Utage.EyeBlinkBase::intervalDoubleEyeBlink
	float ___intervalDoubleEyeBlink_4;
	// System.String Utage.EyeBlinkBase::eyeTag
	String_t* ___eyeTag_5;
	// Utage.MiniAnimationData Utage.EyeBlinkBase::animationData
	MiniAnimationData_t521227391 * ___animationData_6;

public:
	inline static int32_t get_offset_of_intervalTime_2() { return static_cast<int32_t>(offsetof(EyeBlinkBase_t1662968566, ___intervalTime_2)); }
	inline MinMaxFloat_t1425080750 * get_intervalTime_2() const { return ___intervalTime_2; }
	inline MinMaxFloat_t1425080750 ** get_address_of_intervalTime_2() { return &___intervalTime_2; }
	inline void set_intervalTime_2(MinMaxFloat_t1425080750 * value)
	{
		___intervalTime_2 = value;
		Il2CppCodeGenWriteBarrier((&___intervalTime_2), value);
	}

	inline static int32_t get_offset_of_randomDoubleEyeBlink_3() { return static_cast<int32_t>(offsetof(EyeBlinkBase_t1662968566, ___randomDoubleEyeBlink_3)); }
	inline float get_randomDoubleEyeBlink_3() const { return ___randomDoubleEyeBlink_3; }
	inline float* get_address_of_randomDoubleEyeBlink_3() { return &___randomDoubleEyeBlink_3; }
	inline void set_randomDoubleEyeBlink_3(float value)
	{
		___randomDoubleEyeBlink_3 = value;
	}

	inline static int32_t get_offset_of_intervalDoubleEyeBlink_4() { return static_cast<int32_t>(offsetof(EyeBlinkBase_t1662968566, ___intervalDoubleEyeBlink_4)); }
	inline float get_intervalDoubleEyeBlink_4() const { return ___intervalDoubleEyeBlink_4; }
	inline float* get_address_of_intervalDoubleEyeBlink_4() { return &___intervalDoubleEyeBlink_4; }
	inline void set_intervalDoubleEyeBlink_4(float value)
	{
		___intervalDoubleEyeBlink_4 = value;
	}

	inline static int32_t get_offset_of_eyeTag_5() { return static_cast<int32_t>(offsetof(EyeBlinkBase_t1662968566, ___eyeTag_5)); }
	inline String_t* get_eyeTag_5() const { return ___eyeTag_5; }
	inline String_t** get_address_of_eyeTag_5() { return &___eyeTag_5; }
	inline void set_eyeTag_5(String_t* value)
	{
		___eyeTag_5 = value;
		Il2CppCodeGenWriteBarrier((&___eyeTag_5), value);
	}

	inline static int32_t get_offset_of_animationData_6() { return static_cast<int32_t>(offsetof(EyeBlinkBase_t1662968566, ___animationData_6)); }
	inline MiniAnimationData_t521227391 * get_animationData_6() const { return ___animationData_6; }
	inline MiniAnimationData_t521227391 ** get_address_of_animationData_6() { return &___animationData_6; }
	inline void set_animationData_6(MiniAnimationData_t521227391 * value)
	{
		___animationData_6 = value;
		Il2CppCodeGenWriteBarrier((&___animationData_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EYEBLINKBASE_T1662968566_H
#ifndef DONTDESTORYONLOAD_T3157743482_H
#define DONTDESTORYONLOAD_T3157743482_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.DontDestoryOnLoad
struct  DontDestoryOnLoad_t3157743482  : public MonoBehaviour_t1158329972
{
public:
	// System.Boolean Utage.DontDestoryOnLoad::dontDestoryOnLoad
	bool ___dontDestoryOnLoad_2;

public:
	inline static int32_t get_offset_of_dontDestoryOnLoad_2() { return static_cast<int32_t>(offsetof(DontDestoryOnLoad_t3157743482, ___dontDestoryOnLoad_2)); }
	inline bool get_dontDestoryOnLoad_2() const { return ___dontDestoryOnLoad_2; }
	inline bool* get_address_of_dontDestoryOnLoad_2() { return &___dontDestoryOnLoad_2; }
	inline void set_dontDestoryOnLoad_2(bool value)
	{
		___dontDestoryOnLoad_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DONTDESTORYONLOAD_T3157743482_H
#ifndef UGUIBACKGROUNDRAYCASTRECIEVER_T733380310_H
#define UGUIBACKGROUNDRAYCASTRECIEVER_T733380310_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.UguiBackgroundRaycastReciever
struct  UguiBackgroundRaycastReciever_t733380310  : public MonoBehaviour_t1158329972
{
public:
	// Utage.UguiBackgroundRaycaster Utage.UguiBackgroundRaycastReciever::raycaster
	UguiBackgroundRaycaster_t588614668 * ___raycaster_2;

public:
	inline static int32_t get_offset_of_raycaster_2() { return static_cast<int32_t>(offsetof(UguiBackgroundRaycastReciever_t733380310, ___raycaster_2)); }
	inline UguiBackgroundRaycaster_t588614668 * get_raycaster_2() const { return ___raycaster_2; }
	inline UguiBackgroundRaycaster_t588614668 ** get_address_of_raycaster_2() { return &___raycaster_2; }
	inline void set_raycaster_2(UguiBackgroundRaycaster_t588614668 * value)
	{
		___raycaster_2 = value;
		Il2CppCodeGenWriteBarrier((&___raycaster_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UGUIBACKGROUNDRAYCASTRECIEVER_T733380310_H
#ifndef UGUIBUTTONSE_T466015712_H
#define UGUIBUTTONSE_T466015712_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.UguiButtonSe
struct  UguiButtonSe_t466015712  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.UI.Selectable Utage.UguiButtonSe::selectable
	Selectable_t1490392188 * ___selectable_2;
	// UnityEngine.AudioClip Utage.UguiButtonSe::clicked
	AudioClip_t1932558630 * ___clicked_3;
	// UnityEngine.AudioClip Utage.UguiButtonSe::highlited
	AudioClip_t1932558630 * ___highlited_4;
	// Utage.SoundPlayMode Utage.UguiButtonSe::clickedPlayMode
	int32_t ___clickedPlayMode_5;
	// Utage.SoundPlayMode Utage.UguiButtonSe::highlitedPlayMode
	int32_t ___highlitedPlayMode_6;

public:
	inline static int32_t get_offset_of_selectable_2() { return static_cast<int32_t>(offsetof(UguiButtonSe_t466015712, ___selectable_2)); }
	inline Selectable_t1490392188 * get_selectable_2() const { return ___selectable_2; }
	inline Selectable_t1490392188 ** get_address_of_selectable_2() { return &___selectable_2; }
	inline void set_selectable_2(Selectable_t1490392188 * value)
	{
		___selectable_2 = value;
		Il2CppCodeGenWriteBarrier((&___selectable_2), value);
	}

	inline static int32_t get_offset_of_clicked_3() { return static_cast<int32_t>(offsetof(UguiButtonSe_t466015712, ___clicked_3)); }
	inline AudioClip_t1932558630 * get_clicked_3() const { return ___clicked_3; }
	inline AudioClip_t1932558630 ** get_address_of_clicked_3() { return &___clicked_3; }
	inline void set_clicked_3(AudioClip_t1932558630 * value)
	{
		___clicked_3 = value;
		Il2CppCodeGenWriteBarrier((&___clicked_3), value);
	}

	inline static int32_t get_offset_of_highlited_4() { return static_cast<int32_t>(offsetof(UguiButtonSe_t466015712, ___highlited_4)); }
	inline AudioClip_t1932558630 * get_highlited_4() const { return ___highlited_4; }
	inline AudioClip_t1932558630 ** get_address_of_highlited_4() { return &___highlited_4; }
	inline void set_highlited_4(AudioClip_t1932558630 * value)
	{
		___highlited_4 = value;
		Il2CppCodeGenWriteBarrier((&___highlited_4), value);
	}

	inline static int32_t get_offset_of_clickedPlayMode_5() { return static_cast<int32_t>(offsetof(UguiButtonSe_t466015712, ___clickedPlayMode_5)); }
	inline int32_t get_clickedPlayMode_5() const { return ___clickedPlayMode_5; }
	inline int32_t* get_address_of_clickedPlayMode_5() { return &___clickedPlayMode_5; }
	inline void set_clickedPlayMode_5(int32_t value)
	{
		___clickedPlayMode_5 = value;
	}

	inline static int32_t get_offset_of_highlitedPlayMode_6() { return static_cast<int32_t>(offsetof(UguiButtonSe_t466015712, ___highlitedPlayMode_6)); }
	inline int32_t get_highlitedPlayMode_6() const { return ___highlitedPlayMode_6; }
	inline int32_t* get_address_of_highlitedPlayMode_6() { return &___highlitedPlayMode_6; }
	inline void set_highlitedPlayMode_6(int32_t value)
	{
		___highlitedPlayMode_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UGUIBUTTONSE_T466015712_H
#ifndef UGUITOGGLEGROUPINDEXED_T999166452_H
#define UGUITOGGLEGROUPINDEXED_T999166452_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.UguiToggleGroupIndexed
struct  UguiToggleGroupIndexed_t999166452  : public MonoBehaviour_t1158329972
{
public:
	// System.Collections.Generic.List`1<UnityEngine.UI.Toggle> Utage.UguiToggleGroupIndexed::toggles
	List_1_t3345875600 * ___toggles_2;
	// System.Int32 Utage.UguiToggleGroupIndexed::firstIndexOnAwake
	int32_t ___firstIndexOnAwake_3;
	// System.Boolean Utage.UguiToggleGroupIndexed::ignoreValueChangeOnAwake
	bool ___ignoreValueChangeOnAwake_4;
	// System.Boolean Utage.UguiToggleGroupIndexed::autoToggleInteractiveOff
	bool ___autoToggleInteractiveOff_5;
	// System.Boolean Utage.UguiToggleGroupIndexed::isLoopShift
	bool ___isLoopShift_6;
	// UnityEngine.UI.Button Utage.UguiToggleGroupIndexed::shiftLeftButton
	Button_t2872111280 * ___shiftLeftButton_7;
	// UnityEngine.UI.Button Utage.UguiToggleGroupIndexed::shiftRightButton
	Button_t2872111280 * ___shiftRightButton_8;
	// UnityEngine.UI.Button Utage.UguiToggleGroupIndexed::jumpLeftEdgeButton
	Button_t2872111280 * ___jumpLeftEdgeButton_9;
	// UnityEngine.UI.Button Utage.UguiToggleGroupIndexed::jumpRightEdgeButton
	Button_t2872111280 * ___jumpRightEdgeButton_10;
	// System.Int32 Utage.UguiToggleGroupIndexed::currentIndex
	int32_t ___currentIndex_11;
	// Utage.UguiToggleGroupIndexed/UguiTabButtonGroupEvent Utage.UguiToggleGroupIndexed::OnValueChanged
	UguiTabButtonGroupEvent_t4293552753 * ___OnValueChanged_12;
	// System.Boolean Utage.UguiToggleGroupIndexed::isIgnoreValueChange
	bool ___isIgnoreValueChange_13;

public:
	inline static int32_t get_offset_of_toggles_2() { return static_cast<int32_t>(offsetof(UguiToggleGroupIndexed_t999166452, ___toggles_2)); }
	inline List_1_t3345875600 * get_toggles_2() const { return ___toggles_2; }
	inline List_1_t3345875600 ** get_address_of_toggles_2() { return &___toggles_2; }
	inline void set_toggles_2(List_1_t3345875600 * value)
	{
		___toggles_2 = value;
		Il2CppCodeGenWriteBarrier((&___toggles_2), value);
	}

	inline static int32_t get_offset_of_firstIndexOnAwake_3() { return static_cast<int32_t>(offsetof(UguiToggleGroupIndexed_t999166452, ___firstIndexOnAwake_3)); }
	inline int32_t get_firstIndexOnAwake_3() const { return ___firstIndexOnAwake_3; }
	inline int32_t* get_address_of_firstIndexOnAwake_3() { return &___firstIndexOnAwake_3; }
	inline void set_firstIndexOnAwake_3(int32_t value)
	{
		___firstIndexOnAwake_3 = value;
	}

	inline static int32_t get_offset_of_ignoreValueChangeOnAwake_4() { return static_cast<int32_t>(offsetof(UguiToggleGroupIndexed_t999166452, ___ignoreValueChangeOnAwake_4)); }
	inline bool get_ignoreValueChangeOnAwake_4() const { return ___ignoreValueChangeOnAwake_4; }
	inline bool* get_address_of_ignoreValueChangeOnAwake_4() { return &___ignoreValueChangeOnAwake_4; }
	inline void set_ignoreValueChangeOnAwake_4(bool value)
	{
		___ignoreValueChangeOnAwake_4 = value;
	}

	inline static int32_t get_offset_of_autoToggleInteractiveOff_5() { return static_cast<int32_t>(offsetof(UguiToggleGroupIndexed_t999166452, ___autoToggleInteractiveOff_5)); }
	inline bool get_autoToggleInteractiveOff_5() const { return ___autoToggleInteractiveOff_5; }
	inline bool* get_address_of_autoToggleInteractiveOff_5() { return &___autoToggleInteractiveOff_5; }
	inline void set_autoToggleInteractiveOff_5(bool value)
	{
		___autoToggleInteractiveOff_5 = value;
	}

	inline static int32_t get_offset_of_isLoopShift_6() { return static_cast<int32_t>(offsetof(UguiToggleGroupIndexed_t999166452, ___isLoopShift_6)); }
	inline bool get_isLoopShift_6() const { return ___isLoopShift_6; }
	inline bool* get_address_of_isLoopShift_6() { return &___isLoopShift_6; }
	inline void set_isLoopShift_6(bool value)
	{
		___isLoopShift_6 = value;
	}

	inline static int32_t get_offset_of_shiftLeftButton_7() { return static_cast<int32_t>(offsetof(UguiToggleGroupIndexed_t999166452, ___shiftLeftButton_7)); }
	inline Button_t2872111280 * get_shiftLeftButton_7() const { return ___shiftLeftButton_7; }
	inline Button_t2872111280 ** get_address_of_shiftLeftButton_7() { return &___shiftLeftButton_7; }
	inline void set_shiftLeftButton_7(Button_t2872111280 * value)
	{
		___shiftLeftButton_7 = value;
		Il2CppCodeGenWriteBarrier((&___shiftLeftButton_7), value);
	}

	inline static int32_t get_offset_of_shiftRightButton_8() { return static_cast<int32_t>(offsetof(UguiToggleGroupIndexed_t999166452, ___shiftRightButton_8)); }
	inline Button_t2872111280 * get_shiftRightButton_8() const { return ___shiftRightButton_8; }
	inline Button_t2872111280 ** get_address_of_shiftRightButton_8() { return &___shiftRightButton_8; }
	inline void set_shiftRightButton_8(Button_t2872111280 * value)
	{
		___shiftRightButton_8 = value;
		Il2CppCodeGenWriteBarrier((&___shiftRightButton_8), value);
	}

	inline static int32_t get_offset_of_jumpLeftEdgeButton_9() { return static_cast<int32_t>(offsetof(UguiToggleGroupIndexed_t999166452, ___jumpLeftEdgeButton_9)); }
	inline Button_t2872111280 * get_jumpLeftEdgeButton_9() const { return ___jumpLeftEdgeButton_9; }
	inline Button_t2872111280 ** get_address_of_jumpLeftEdgeButton_9() { return &___jumpLeftEdgeButton_9; }
	inline void set_jumpLeftEdgeButton_9(Button_t2872111280 * value)
	{
		___jumpLeftEdgeButton_9 = value;
		Il2CppCodeGenWriteBarrier((&___jumpLeftEdgeButton_9), value);
	}

	inline static int32_t get_offset_of_jumpRightEdgeButton_10() { return static_cast<int32_t>(offsetof(UguiToggleGroupIndexed_t999166452, ___jumpRightEdgeButton_10)); }
	inline Button_t2872111280 * get_jumpRightEdgeButton_10() const { return ___jumpRightEdgeButton_10; }
	inline Button_t2872111280 ** get_address_of_jumpRightEdgeButton_10() { return &___jumpRightEdgeButton_10; }
	inline void set_jumpRightEdgeButton_10(Button_t2872111280 * value)
	{
		___jumpRightEdgeButton_10 = value;
		Il2CppCodeGenWriteBarrier((&___jumpRightEdgeButton_10), value);
	}

	inline static int32_t get_offset_of_currentIndex_11() { return static_cast<int32_t>(offsetof(UguiToggleGroupIndexed_t999166452, ___currentIndex_11)); }
	inline int32_t get_currentIndex_11() const { return ___currentIndex_11; }
	inline int32_t* get_address_of_currentIndex_11() { return &___currentIndex_11; }
	inline void set_currentIndex_11(int32_t value)
	{
		___currentIndex_11 = value;
	}

	inline static int32_t get_offset_of_OnValueChanged_12() { return static_cast<int32_t>(offsetof(UguiToggleGroupIndexed_t999166452, ___OnValueChanged_12)); }
	inline UguiTabButtonGroupEvent_t4293552753 * get_OnValueChanged_12() const { return ___OnValueChanged_12; }
	inline UguiTabButtonGroupEvent_t4293552753 ** get_address_of_OnValueChanged_12() { return &___OnValueChanged_12; }
	inline void set_OnValueChanged_12(UguiTabButtonGroupEvent_t4293552753 * value)
	{
		___OnValueChanged_12 = value;
		Il2CppCodeGenWriteBarrier((&___OnValueChanged_12), value);
	}

	inline static int32_t get_offset_of_isIgnoreValueChange_13() { return static_cast<int32_t>(offsetof(UguiToggleGroupIndexed_t999166452, ___isIgnoreValueChange_13)); }
	inline bool get_isIgnoreValueChange_13() const { return ___isIgnoreValueChange_13; }
	inline bool* get_address_of_isIgnoreValueChange_13() { return &___isIgnoreValueChange_13; }
	inline void set_isIgnoreValueChange_13(bool value)
	{
		___isIgnoreValueChange_13 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UGUITOGGLEGROUPINDEXED_T999166452_H
#ifndef UGUICATEGORYGRIDPAGE_T673822931_H
#define UGUICATEGORYGRIDPAGE_T673822931_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.UguiCategoryGridPage
struct  UguiCategoryGridPage_t673822931  : public MonoBehaviour_t1158329972
{
public:
	// Utage.UguiGridPage Utage.UguiCategoryGridPage::gridPage
	UguiGridPage_t1812803607 * ___gridPage_2;
	// Utage.UguiToggleGroupIndexed Utage.UguiCategoryGridPage::categoryToggleGroup
	UguiToggleGroupIndexed_t999166452 * ___categoryToggleGroup_3;
	// Utage.UguiAlignGroup Utage.UguiCategoryGridPage::categoryAlignGroup
	UguiAlignGroup_t4030345508 * ___categoryAlignGroup_4;
	// UnityEngine.GameObject Utage.UguiCategoryGridPage::categoryPrefab
	GameObject_t1756533147 * ___categoryPrefab_5;
	// System.Collections.Generic.List`1<UnityEngine.Sprite> Utage.UguiCategoryGridPage::buttonSpriteList
	List_1_t3973682211 * ___buttonSpriteList_6;
	// System.String[] Utage.UguiCategoryGridPage::categoryList
	StringU5BU5D_t1642385972* ___categoryList_7;

public:
	inline static int32_t get_offset_of_gridPage_2() { return static_cast<int32_t>(offsetof(UguiCategoryGridPage_t673822931, ___gridPage_2)); }
	inline UguiGridPage_t1812803607 * get_gridPage_2() const { return ___gridPage_2; }
	inline UguiGridPage_t1812803607 ** get_address_of_gridPage_2() { return &___gridPage_2; }
	inline void set_gridPage_2(UguiGridPage_t1812803607 * value)
	{
		___gridPage_2 = value;
		Il2CppCodeGenWriteBarrier((&___gridPage_2), value);
	}

	inline static int32_t get_offset_of_categoryToggleGroup_3() { return static_cast<int32_t>(offsetof(UguiCategoryGridPage_t673822931, ___categoryToggleGroup_3)); }
	inline UguiToggleGroupIndexed_t999166452 * get_categoryToggleGroup_3() const { return ___categoryToggleGroup_3; }
	inline UguiToggleGroupIndexed_t999166452 ** get_address_of_categoryToggleGroup_3() { return &___categoryToggleGroup_3; }
	inline void set_categoryToggleGroup_3(UguiToggleGroupIndexed_t999166452 * value)
	{
		___categoryToggleGroup_3 = value;
		Il2CppCodeGenWriteBarrier((&___categoryToggleGroup_3), value);
	}

	inline static int32_t get_offset_of_categoryAlignGroup_4() { return static_cast<int32_t>(offsetof(UguiCategoryGridPage_t673822931, ___categoryAlignGroup_4)); }
	inline UguiAlignGroup_t4030345508 * get_categoryAlignGroup_4() const { return ___categoryAlignGroup_4; }
	inline UguiAlignGroup_t4030345508 ** get_address_of_categoryAlignGroup_4() { return &___categoryAlignGroup_4; }
	inline void set_categoryAlignGroup_4(UguiAlignGroup_t4030345508 * value)
	{
		___categoryAlignGroup_4 = value;
		Il2CppCodeGenWriteBarrier((&___categoryAlignGroup_4), value);
	}

	inline static int32_t get_offset_of_categoryPrefab_5() { return static_cast<int32_t>(offsetof(UguiCategoryGridPage_t673822931, ___categoryPrefab_5)); }
	inline GameObject_t1756533147 * get_categoryPrefab_5() const { return ___categoryPrefab_5; }
	inline GameObject_t1756533147 ** get_address_of_categoryPrefab_5() { return &___categoryPrefab_5; }
	inline void set_categoryPrefab_5(GameObject_t1756533147 * value)
	{
		___categoryPrefab_5 = value;
		Il2CppCodeGenWriteBarrier((&___categoryPrefab_5), value);
	}

	inline static int32_t get_offset_of_buttonSpriteList_6() { return static_cast<int32_t>(offsetof(UguiCategoryGridPage_t673822931, ___buttonSpriteList_6)); }
	inline List_1_t3973682211 * get_buttonSpriteList_6() const { return ___buttonSpriteList_6; }
	inline List_1_t3973682211 ** get_address_of_buttonSpriteList_6() { return &___buttonSpriteList_6; }
	inline void set_buttonSpriteList_6(List_1_t3973682211 * value)
	{
		___buttonSpriteList_6 = value;
		Il2CppCodeGenWriteBarrier((&___buttonSpriteList_6), value);
	}

	inline static int32_t get_offset_of_categoryList_7() { return static_cast<int32_t>(offsetof(UguiCategoryGridPage_t673822931, ___categoryList_7)); }
	inline StringU5BU5D_t1642385972* get_categoryList_7() const { return ___categoryList_7; }
	inline StringU5BU5D_t1642385972** get_address_of_categoryList_7() { return &___categoryList_7; }
	inline void set_categoryList_7(StringU5BU5D_t1642385972* value)
	{
		___categoryList_7 = value;
		Il2CppCodeGenWriteBarrier((&___categoryList_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UGUICATEGORYGRIDPAGE_T673822931_H
#ifndef UGUILISTVIEW_T169672791_H
#define UGUILISTVIEW_T169672791_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.UguiListView
struct  UguiListView_t169672791  : public MonoBehaviour_t1158329972
{
public:
	// Utage.UguiListView/Type Utage.UguiListView::scrollType
	int32_t ___scrollType_2;
	// UnityEngine.GameObject Utage.UguiListView::itemPrefab
	GameObject_t1756533147 * ___itemPrefab_3;
	// UnityEngine.RectTransform Utage.UguiListView::content
	RectTransform_t3349966182 * ___content_4;
	// System.Boolean Utage.UguiListView::isStopScroolWithAllInnner
	bool ___isStopScroolWithAllInnner_5;
	// System.Boolean Utage.UguiListView::isAutoCenteringOnRepostion
	bool ___isAutoCenteringOnRepostion_6;
	// Utage.UguiAlignGroup Utage.UguiListView::positionGroup
	UguiAlignGroup_t4030345508 * ___positionGroup_7;
	// UnityEngine.UI.ScrollRect Utage.UguiListView::scrollRect
	ScrollRect_t1199013257 * ___scrollRect_8;
	// UnityEngine.RectTransform Utage.UguiListView::scrollRectTransform
	RectTransform_t3349966182 * ___scrollRectTransform_9;
	// UnityEngine.GameObject Utage.UguiListView::minArrow
	GameObject_t1756533147 * ___minArrow_10;
	// UnityEngine.GameObject Utage.UguiListView::maxArrow
	GameObject_t1756533147 * ___maxArrow_11;
	// System.Collections.Generic.List`1<UnityEngine.GameObject> Utage.UguiListView::items
	List_1_t1125654279 * ___items_12;

public:
	inline static int32_t get_offset_of_scrollType_2() { return static_cast<int32_t>(offsetof(UguiListView_t169672791, ___scrollType_2)); }
	inline int32_t get_scrollType_2() const { return ___scrollType_2; }
	inline int32_t* get_address_of_scrollType_2() { return &___scrollType_2; }
	inline void set_scrollType_2(int32_t value)
	{
		___scrollType_2 = value;
	}

	inline static int32_t get_offset_of_itemPrefab_3() { return static_cast<int32_t>(offsetof(UguiListView_t169672791, ___itemPrefab_3)); }
	inline GameObject_t1756533147 * get_itemPrefab_3() const { return ___itemPrefab_3; }
	inline GameObject_t1756533147 ** get_address_of_itemPrefab_3() { return &___itemPrefab_3; }
	inline void set_itemPrefab_3(GameObject_t1756533147 * value)
	{
		___itemPrefab_3 = value;
		Il2CppCodeGenWriteBarrier((&___itemPrefab_3), value);
	}

	inline static int32_t get_offset_of_content_4() { return static_cast<int32_t>(offsetof(UguiListView_t169672791, ___content_4)); }
	inline RectTransform_t3349966182 * get_content_4() const { return ___content_4; }
	inline RectTransform_t3349966182 ** get_address_of_content_4() { return &___content_4; }
	inline void set_content_4(RectTransform_t3349966182 * value)
	{
		___content_4 = value;
		Il2CppCodeGenWriteBarrier((&___content_4), value);
	}

	inline static int32_t get_offset_of_isStopScroolWithAllInnner_5() { return static_cast<int32_t>(offsetof(UguiListView_t169672791, ___isStopScroolWithAllInnner_5)); }
	inline bool get_isStopScroolWithAllInnner_5() const { return ___isStopScroolWithAllInnner_5; }
	inline bool* get_address_of_isStopScroolWithAllInnner_5() { return &___isStopScroolWithAllInnner_5; }
	inline void set_isStopScroolWithAllInnner_5(bool value)
	{
		___isStopScroolWithAllInnner_5 = value;
	}

	inline static int32_t get_offset_of_isAutoCenteringOnRepostion_6() { return static_cast<int32_t>(offsetof(UguiListView_t169672791, ___isAutoCenteringOnRepostion_6)); }
	inline bool get_isAutoCenteringOnRepostion_6() const { return ___isAutoCenteringOnRepostion_6; }
	inline bool* get_address_of_isAutoCenteringOnRepostion_6() { return &___isAutoCenteringOnRepostion_6; }
	inline void set_isAutoCenteringOnRepostion_6(bool value)
	{
		___isAutoCenteringOnRepostion_6 = value;
	}

	inline static int32_t get_offset_of_positionGroup_7() { return static_cast<int32_t>(offsetof(UguiListView_t169672791, ___positionGroup_7)); }
	inline UguiAlignGroup_t4030345508 * get_positionGroup_7() const { return ___positionGroup_7; }
	inline UguiAlignGroup_t4030345508 ** get_address_of_positionGroup_7() { return &___positionGroup_7; }
	inline void set_positionGroup_7(UguiAlignGroup_t4030345508 * value)
	{
		___positionGroup_7 = value;
		Il2CppCodeGenWriteBarrier((&___positionGroup_7), value);
	}

	inline static int32_t get_offset_of_scrollRect_8() { return static_cast<int32_t>(offsetof(UguiListView_t169672791, ___scrollRect_8)); }
	inline ScrollRect_t1199013257 * get_scrollRect_8() const { return ___scrollRect_8; }
	inline ScrollRect_t1199013257 ** get_address_of_scrollRect_8() { return &___scrollRect_8; }
	inline void set_scrollRect_8(ScrollRect_t1199013257 * value)
	{
		___scrollRect_8 = value;
		Il2CppCodeGenWriteBarrier((&___scrollRect_8), value);
	}

	inline static int32_t get_offset_of_scrollRectTransform_9() { return static_cast<int32_t>(offsetof(UguiListView_t169672791, ___scrollRectTransform_9)); }
	inline RectTransform_t3349966182 * get_scrollRectTransform_9() const { return ___scrollRectTransform_9; }
	inline RectTransform_t3349966182 ** get_address_of_scrollRectTransform_9() { return &___scrollRectTransform_9; }
	inline void set_scrollRectTransform_9(RectTransform_t3349966182 * value)
	{
		___scrollRectTransform_9 = value;
		Il2CppCodeGenWriteBarrier((&___scrollRectTransform_9), value);
	}

	inline static int32_t get_offset_of_minArrow_10() { return static_cast<int32_t>(offsetof(UguiListView_t169672791, ___minArrow_10)); }
	inline GameObject_t1756533147 * get_minArrow_10() const { return ___minArrow_10; }
	inline GameObject_t1756533147 ** get_address_of_minArrow_10() { return &___minArrow_10; }
	inline void set_minArrow_10(GameObject_t1756533147 * value)
	{
		___minArrow_10 = value;
		Il2CppCodeGenWriteBarrier((&___minArrow_10), value);
	}

	inline static int32_t get_offset_of_maxArrow_11() { return static_cast<int32_t>(offsetof(UguiListView_t169672791, ___maxArrow_11)); }
	inline GameObject_t1756533147 * get_maxArrow_11() const { return ___maxArrow_11; }
	inline GameObject_t1756533147 ** get_address_of_maxArrow_11() { return &___maxArrow_11; }
	inline void set_maxArrow_11(GameObject_t1756533147 * value)
	{
		___maxArrow_11 = value;
		Il2CppCodeGenWriteBarrier((&___maxArrow_11), value);
	}

	inline static int32_t get_offset_of_items_12() { return static_cast<int32_t>(offsetof(UguiListView_t169672791, ___items_12)); }
	inline List_1_t1125654279 * get_items_12() const { return ___items_12; }
	inline List_1_t1125654279 ** get_address_of_items_12() { return &___items_12; }
	inline void set_items_12(List_1_t1125654279 * value)
	{
		___items_12 = value;
		Il2CppCodeGenWriteBarrier((&___items_12), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UGUILISTVIEW_T169672791_H
#ifndef UGUIIGNORERAYCASTER_T1854879086_H
#define UGUIIGNORERAYCASTER_T1854879086_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.UguiIgnoreRaycaster
struct  UguiIgnoreRaycaster_t1854879086  : public MonoBehaviour_t1158329972
{
public:
	// System.Boolean Utage.UguiIgnoreRaycaster::ignoreRaycaster
	bool ___ignoreRaycaster_2;

public:
	inline static int32_t get_offset_of_ignoreRaycaster_2() { return static_cast<int32_t>(offsetof(UguiIgnoreRaycaster_t1854879086, ___ignoreRaycaster_2)); }
	inline bool get_ignoreRaycaster_2() const { return ___ignoreRaycaster_2; }
	inline bool* get_address_of_ignoreRaycaster_2() { return &___ignoreRaycaster_2; }
	inline void set_ignoreRaycaster_2(bool value)
	{
		___ignoreRaycaster_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UGUIIGNORERAYCASTER_T1854879086_H
#ifndef UGUIGRIDPAGE_T1812803607_H
#define UGUIGRIDPAGE_T1812803607_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.UguiGridPage
struct  UguiGridPage_t1812803607  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.UI.GridLayoutGroup Utage.UguiGridPage::grid
	GridLayoutGroup_t1515633077 * ___grid_2;
	// UnityEngine.GameObject Utage.UguiGridPage::itemPrefab
	GameObject_t1756533147 * ___itemPrefab_3;
	// Utage.UguiToggleGroupIndexed Utage.UguiGridPage::pageCarouselToggles
	UguiToggleGroupIndexed_t999166452 * ___pageCarouselToggles_4;
	// Utage.UguiAlignGroup Utage.UguiGridPage::pageCarouselAlignGroup
	UguiAlignGroup_t4030345508 * ___pageCarouselAlignGroup_5;
	// UnityEngine.GameObject Utage.UguiGridPage::pageCarouselPrefab
	GameObject_t1756533147 * ___pageCarouselPrefab_6;
	// UnityEngine.RectTransform Utage.UguiGridPage::cachedRectTransform
	RectTransform_t3349966182 * ___cachedRectTransform_7;
	// System.Int32 Utage.UguiGridPage::maxItemPerPage
	int32_t ___maxItemPerPage_8;
	// System.Int32 Utage.UguiGridPage::maxItemNum
	int32_t ___maxItemNum_9;
	// System.Int32 Utage.UguiGridPage::currentPage
	int32_t ___currentPage_10;
	// System.Collections.Generic.List`1<UnityEngine.GameObject> Utage.UguiGridPage::items
	List_1_t1125654279 * ___items_11;
	// System.Action`2<UnityEngine.GameObject,System.Int32> Utage.UguiGridPage::CallbackCreateItem
	Action_2_t1397798026 * ___CallbackCreateItem_12;

public:
	inline static int32_t get_offset_of_grid_2() { return static_cast<int32_t>(offsetof(UguiGridPage_t1812803607, ___grid_2)); }
	inline GridLayoutGroup_t1515633077 * get_grid_2() const { return ___grid_2; }
	inline GridLayoutGroup_t1515633077 ** get_address_of_grid_2() { return &___grid_2; }
	inline void set_grid_2(GridLayoutGroup_t1515633077 * value)
	{
		___grid_2 = value;
		Il2CppCodeGenWriteBarrier((&___grid_2), value);
	}

	inline static int32_t get_offset_of_itemPrefab_3() { return static_cast<int32_t>(offsetof(UguiGridPage_t1812803607, ___itemPrefab_3)); }
	inline GameObject_t1756533147 * get_itemPrefab_3() const { return ___itemPrefab_3; }
	inline GameObject_t1756533147 ** get_address_of_itemPrefab_3() { return &___itemPrefab_3; }
	inline void set_itemPrefab_3(GameObject_t1756533147 * value)
	{
		___itemPrefab_3 = value;
		Il2CppCodeGenWriteBarrier((&___itemPrefab_3), value);
	}

	inline static int32_t get_offset_of_pageCarouselToggles_4() { return static_cast<int32_t>(offsetof(UguiGridPage_t1812803607, ___pageCarouselToggles_4)); }
	inline UguiToggleGroupIndexed_t999166452 * get_pageCarouselToggles_4() const { return ___pageCarouselToggles_4; }
	inline UguiToggleGroupIndexed_t999166452 ** get_address_of_pageCarouselToggles_4() { return &___pageCarouselToggles_4; }
	inline void set_pageCarouselToggles_4(UguiToggleGroupIndexed_t999166452 * value)
	{
		___pageCarouselToggles_4 = value;
		Il2CppCodeGenWriteBarrier((&___pageCarouselToggles_4), value);
	}

	inline static int32_t get_offset_of_pageCarouselAlignGroup_5() { return static_cast<int32_t>(offsetof(UguiGridPage_t1812803607, ___pageCarouselAlignGroup_5)); }
	inline UguiAlignGroup_t4030345508 * get_pageCarouselAlignGroup_5() const { return ___pageCarouselAlignGroup_5; }
	inline UguiAlignGroup_t4030345508 ** get_address_of_pageCarouselAlignGroup_5() { return &___pageCarouselAlignGroup_5; }
	inline void set_pageCarouselAlignGroup_5(UguiAlignGroup_t4030345508 * value)
	{
		___pageCarouselAlignGroup_5 = value;
		Il2CppCodeGenWriteBarrier((&___pageCarouselAlignGroup_5), value);
	}

	inline static int32_t get_offset_of_pageCarouselPrefab_6() { return static_cast<int32_t>(offsetof(UguiGridPage_t1812803607, ___pageCarouselPrefab_6)); }
	inline GameObject_t1756533147 * get_pageCarouselPrefab_6() const { return ___pageCarouselPrefab_6; }
	inline GameObject_t1756533147 ** get_address_of_pageCarouselPrefab_6() { return &___pageCarouselPrefab_6; }
	inline void set_pageCarouselPrefab_6(GameObject_t1756533147 * value)
	{
		___pageCarouselPrefab_6 = value;
		Il2CppCodeGenWriteBarrier((&___pageCarouselPrefab_6), value);
	}

	inline static int32_t get_offset_of_cachedRectTransform_7() { return static_cast<int32_t>(offsetof(UguiGridPage_t1812803607, ___cachedRectTransform_7)); }
	inline RectTransform_t3349966182 * get_cachedRectTransform_7() const { return ___cachedRectTransform_7; }
	inline RectTransform_t3349966182 ** get_address_of_cachedRectTransform_7() { return &___cachedRectTransform_7; }
	inline void set_cachedRectTransform_7(RectTransform_t3349966182 * value)
	{
		___cachedRectTransform_7 = value;
		Il2CppCodeGenWriteBarrier((&___cachedRectTransform_7), value);
	}

	inline static int32_t get_offset_of_maxItemPerPage_8() { return static_cast<int32_t>(offsetof(UguiGridPage_t1812803607, ___maxItemPerPage_8)); }
	inline int32_t get_maxItemPerPage_8() const { return ___maxItemPerPage_8; }
	inline int32_t* get_address_of_maxItemPerPage_8() { return &___maxItemPerPage_8; }
	inline void set_maxItemPerPage_8(int32_t value)
	{
		___maxItemPerPage_8 = value;
	}

	inline static int32_t get_offset_of_maxItemNum_9() { return static_cast<int32_t>(offsetof(UguiGridPage_t1812803607, ___maxItemNum_9)); }
	inline int32_t get_maxItemNum_9() const { return ___maxItemNum_9; }
	inline int32_t* get_address_of_maxItemNum_9() { return &___maxItemNum_9; }
	inline void set_maxItemNum_9(int32_t value)
	{
		___maxItemNum_9 = value;
	}

	inline static int32_t get_offset_of_currentPage_10() { return static_cast<int32_t>(offsetof(UguiGridPage_t1812803607, ___currentPage_10)); }
	inline int32_t get_currentPage_10() const { return ___currentPage_10; }
	inline int32_t* get_address_of_currentPage_10() { return &___currentPage_10; }
	inline void set_currentPage_10(int32_t value)
	{
		___currentPage_10 = value;
	}

	inline static int32_t get_offset_of_items_11() { return static_cast<int32_t>(offsetof(UguiGridPage_t1812803607, ___items_11)); }
	inline List_1_t1125654279 * get_items_11() const { return ___items_11; }
	inline List_1_t1125654279 ** get_address_of_items_11() { return &___items_11; }
	inline void set_items_11(List_1_t1125654279 * value)
	{
		___items_11 = value;
		Il2CppCodeGenWriteBarrier((&___items_11), value);
	}

	inline static int32_t get_offset_of_CallbackCreateItem_12() { return static_cast<int32_t>(offsetof(UguiGridPage_t1812803607, ___CallbackCreateItem_12)); }
	inline Action_2_t1397798026 * get_CallbackCreateItem_12() const { return ___CallbackCreateItem_12; }
	inline Action_2_t1397798026 ** get_address_of_CallbackCreateItem_12() { return &___CallbackCreateItem_12; }
	inline void set_CallbackCreateItem_12(Action_2_t1397798026 * value)
	{
		___CallbackCreateItem_12 = value;
		Il2CppCodeGenWriteBarrier((&___CallbackCreateItem_12), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UGUIGRIDPAGE_T1812803607_H
#ifndef UGUIVIEW_T2174908005_H
#define UGUIVIEW_T2174908005_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.UguiView
struct  UguiView_t2174908005  : public MonoBehaviour_t1158329972
{
public:
	// Utage.UguiView Utage.UguiView::prevView
	UguiView_t2174908005 * ___prevView_2;
	// UnityEngine.AudioClip Utage.UguiView::bgm
	AudioClip_t1932558630 * ___bgm_3;
	// System.Boolean Utage.UguiView::isStopBgmIfNoneBgm
	bool ___isStopBgmIfNoneBgm_4;
	// UnityEngine.Events.UnityEvent Utage.UguiView::onOpen
	UnityEvent_t408735097 * ___onOpen_5;
	// UnityEngine.Events.UnityEvent Utage.UguiView::onClose
	UnityEvent_t408735097 * ___onClose_6;
	// UnityEngine.CanvasGroup Utage.UguiView::canvasGroup
	CanvasGroup_t3296560743 * ___canvasGroup_7;
	// Utage.UguiView/Status Utage.UguiView::status
	int32_t ___status_8;
	// System.Boolean Utage.UguiView::storedCanvasGroupInteractable
	bool ___storedCanvasGroupInteractable_9;
	// System.Boolean Utage.UguiView::storedCanvasGroupBlocksRaycasts
	bool ___storedCanvasGroupBlocksRaycasts_10;
	// System.Boolean Utage.UguiView::isStoredCanvasGroupInfo
	bool ___isStoredCanvasGroupInfo_11;

public:
	inline static int32_t get_offset_of_prevView_2() { return static_cast<int32_t>(offsetof(UguiView_t2174908005, ___prevView_2)); }
	inline UguiView_t2174908005 * get_prevView_2() const { return ___prevView_2; }
	inline UguiView_t2174908005 ** get_address_of_prevView_2() { return &___prevView_2; }
	inline void set_prevView_2(UguiView_t2174908005 * value)
	{
		___prevView_2 = value;
		Il2CppCodeGenWriteBarrier((&___prevView_2), value);
	}

	inline static int32_t get_offset_of_bgm_3() { return static_cast<int32_t>(offsetof(UguiView_t2174908005, ___bgm_3)); }
	inline AudioClip_t1932558630 * get_bgm_3() const { return ___bgm_3; }
	inline AudioClip_t1932558630 ** get_address_of_bgm_3() { return &___bgm_3; }
	inline void set_bgm_3(AudioClip_t1932558630 * value)
	{
		___bgm_3 = value;
		Il2CppCodeGenWriteBarrier((&___bgm_3), value);
	}

	inline static int32_t get_offset_of_isStopBgmIfNoneBgm_4() { return static_cast<int32_t>(offsetof(UguiView_t2174908005, ___isStopBgmIfNoneBgm_4)); }
	inline bool get_isStopBgmIfNoneBgm_4() const { return ___isStopBgmIfNoneBgm_4; }
	inline bool* get_address_of_isStopBgmIfNoneBgm_4() { return &___isStopBgmIfNoneBgm_4; }
	inline void set_isStopBgmIfNoneBgm_4(bool value)
	{
		___isStopBgmIfNoneBgm_4 = value;
	}

	inline static int32_t get_offset_of_onOpen_5() { return static_cast<int32_t>(offsetof(UguiView_t2174908005, ___onOpen_5)); }
	inline UnityEvent_t408735097 * get_onOpen_5() const { return ___onOpen_5; }
	inline UnityEvent_t408735097 ** get_address_of_onOpen_5() { return &___onOpen_5; }
	inline void set_onOpen_5(UnityEvent_t408735097 * value)
	{
		___onOpen_5 = value;
		Il2CppCodeGenWriteBarrier((&___onOpen_5), value);
	}

	inline static int32_t get_offset_of_onClose_6() { return static_cast<int32_t>(offsetof(UguiView_t2174908005, ___onClose_6)); }
	inline UnityEvent_t408735097 * get_onClose_6() const { return ___onClose_6; }
	inline UnityEvent_t408735097 ** get_address_of_onClose_6() { return &___onClose_6; }
	inline void set_onClose_6(UnityEvent_t408735097 * value)
	{
		___onClose_6 = value;
		Il2CppCodeGenWriteBarrier((&___onClose_6), value);
	}

	inline static int32_t get_offset_of_canvasGroup_7() { return static_cast<int32_t>(offsetof(UguiView_t2174908005, ___canvasGroup_7)); }
	inline CanvasGroup_t3296560743 * get_canvasGroup_7() const { return ___canvasGroup_7; }
	inline CanvasGroup_t3296560743 ** get_address_of_canvasGroup_7() { return &___canvasGroup_7; }
	inline void set_canvasGroup_7(CanvasGroup_t3296560743 * value)
	{
		___canvasGroup_7 = value;
		Il2CppCodeGenWriteBarrier((&___canvasGroup_7), value);
	}

	inline static int32_t get_offset_of_status_8() { return static_cast<int32_t>(offsetof(UguiView_t2174908005, ___status_8)); }
	inline int32_t get_status_8() const { return ___status_8; }
	inline int32_t* get_address_of_status_8() { return &___status_8; }
	inline void set_status_8(int32_t value)
	{
		___status_8 = value;
	}

	inline static int32_t get_offset_of_storedCanvasGroupInteractable_9() { return static_cast<int32_t>(offsetof(UguiView_t2174908005, ___storedCanvasGroupInteractable_9)); }
	inline bool get_storedCanvasGroupInteractable_9() const { return ___storedCanvasGroupInteractable_9; }
	inline bool* get_address_of_storedCanvasGroupInteractable_9() { return &___storedCanvasGroupInteractable_9; }
	inline void set_storedCanvasGroupInteractable_9(bool value)
	{
		___storedCanvasGroupInteractable_9 = value;
	}

	inline static int32_t get_offset_of_storedCanvasGroupBlocksRaycasts_10() { return static_cast<int32_t>(offsetof(UguiView_t2174908005, ___storedCanvasGroupBlocksRaycasts_10)); }
	inline bool get_storedCanvasGroupBlocksRaycasts_10() const { return ___storedCanvasGroupBlocksRaycasts_10; }
	inline bool* get_address_of_storedCanvasGroupBlocksRaycasts_10() { return &___storedCanvasGroupBlocksRaycasts_10; }
	inline void set_storedCanvasGroupBlocksRaycasts_10(bool value)
	{
		___storedCanvasGroupBlocksRaycasts_10 = value;
	}

	inline static int32_t get_offset_of_isStoredCanvasGroupInfo_11() { return static_cast<int32_t>(offsetof(UguiView_t2174908005, ___isStoredCanvasGroupInfo_11)); }
	inline bool get_isStoredCanvasGroupInfo_11() const { return ___isStoredCanvasGroupInfo_11; }
	inline bool* get_address_of_isStoredCanvasGroupInfo_11() { return &___isStoredCanvasGroupInfo_11; }
	inline void set_isStoredCanvasGroupInfo_11(bool value)
	{
		___isStoredCanvasGroupInfo_11 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UGUIVIEW_T2174908005_H
#ifndef UGUIFADETEXTURESTREAM_T4272889985_H
#define UGUIFADETEXTURESTREAM_T4272889985_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.UguiFadeTextureStream
struct  UguiFadeTextureStream_t4272889985  : public MonoBehaviour_t1158329972
{
public:
	// System.Boolean Utage.UguiFadeTextureStream::allowSkip
	bool ___allowSkip_2;
	// System.Boolean Utage.UguiFadeTextureStream::allowAllSkip
	bool ___allowAllSkip_3;
	// Utage.UguiFadeTextureStream/FadeTextureInfo[] Utage.UguiFadeTextureStream::fadeTextures
	FadeTextureInfoU5BU5D_t3972777596* ___fadeTextures_4;
	// System.Boolean Utage.UguiFadeTextureStream::isInput
	bool ___isInput_5;
	// System.Boolean Utage.UguiFadeTextureStream::isPlaying
	bool ___isPlaying_6;

public:
	inline static int32_t get_offset_of_allowSkip_2() { return static_cast<int32_t>(offsetof(UguiFadeTextureStream_t4272889985, ___allowSkip_2)); }
	inline bool get_allowSkip_2() const { return ___allowSkip_2; }
	inline bool* get_address_of_allowSkip_2() { return &___allowSkip_2; }
	inline void set_allowSkip_2(bool value)
	{
		___allowSkip_2 = value;
	}

	inline static int32_t get_offset_of_allowAllSkip_3() { return static_cast<int32_t>(offsetof(UguiFadeTextureStream_t4272889985, ___allowAllSkip_3)); }
	inline bool get_allowAllSkip_3() const { return ___allowAllSkip_3; }
	inline bool* get_address_of_allowAllSkip_3() { return &___allowAllSkip_3; }
	inline void set_allowAllSkip_3(bool value)
	{
		___allowAllSkip_3 = value;
	}

	inline static int32_t get_offset_of_fadeTextures_4() { return static_cast<int32_t>(offsetof(UguiFadeTextureStream_t4272889985, ___fadeTextures_4)); }
	inline FadeTextureInfoU5BU5D_t3972777596* get_fadeTextures_4() const { return ___fadeTextures_4; }
	inline FadeTextureInfoU5BU5D_t3972777596** get_address_of_fadeTextures_4() { return &___fadeTextures_4; }
	inline void set_fadeTextures_4(FadeTextureInfoU5BU5D_t3972777596* value)
	{
		___fadeTextures_4 = value;
		Il2CppCodeGenWriteBarrier((&___fadeTextures_4), value);
	}

	inline static int32_t get_offset_of_isInput_5() { return static_cast<int32_t>(offsetof(UguiFadeTextureStream_t4272889985, ___isInput_5)); }
	inline bool get_isInput_5() const { return ___isInput_5; }
	inline bool* get_address_of_isInput_5() { return &___isInput_5; }
	inline void set_isInput_5(bool value)
	{
		___isInput_5 = value;
	}

	inline static int32_t get_offset_of_isPlaying_6() { return static_cast<int32_t>(offsetof(UguiFadeTextureStream_t4272889985, ___isPlaying_6)); }
	inline bool get_isPlaying_6() const { return ___isPlaying_6; }
	inline bool* get_address_of_isPlaying_6() { return &___isPlaying_6; }
	inline void set_isPlaying_6(bool value)
	{
		___isPlaying_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UGUIFADETEXTURESTREAM_T4272889985_H
#ifndef UGUIVIEWTRANSITIONCROSSFADE_T1724059380_H
#define UGUIVIEWTRANSITIONCROSSFADE_T1724059380_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.UguiViewTransitionCrossFade
struct  UguiViewTransitionCrossFade_t1724059380  : public MonoBehaviour_t1158329972
{
public:
	// Utage.UguiView Utage.UguiViewTransitionCrossFade::uguiView
	UguiView_t2174908005 * ___uguiView_2;
	// System.Boolean Utage.UguiViewTransitionCrossFade::isPlaying
	bool ___isPlaying_3;
	// System.Single Utage.UguiViewTransitionCrossFade::time
	float ___time_4;

public:
	inline static int32_t get_offset_of_uguiView_2() { return static_cast<int32_t>(offsetof(UguiViewTransitionCrossFade_t1724059380, ___uguiView_2)); }
	inline UguiView_t2174908005 * get_uguiView_2() const { return ___uguiView_2; }
	inline UguiView_t2174908005 ** get_address_of_uguiView_2() { return &___uguiView_2; }
	inline void set_uguiView_2(UguiView_t2174908005 * value)
	{
		___uguiView_2 = value;
		Il2CppCodeGenWriteBarrier((&___uguiView_2), value);
	}

	inline static int32_t get_offset_of_isPlaying_3() { return static_cast<int32_t>(offsetof(UguiViewTransitionCrossFade_t1724059380, ___isPlaying_3)); }
	inline bool get_isPlaying_3() const { return ___isPlaying_3; }
	inline bool* get_address_of_isPlaying_3() { return &___isPlaying_3; }
	inline void set_isPlaying_3(bool value)
	{
		___isPlaying_3 = value;
	}

	inline static int32_t get_offset_of_time_4() { return static_cast<int32_t>(offsetof(UguiViewTransitionCrossFade_t1724059380, ___time_4)); }
	inline float get_time_4() const { return ___time_4; }
	inline float* get_address_of_time_4() { return &___time_4; }
	inline void set_time_4(float value)
	{
		___time_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UGUIVIEWTRANSITIONCROSSFADE_T1724059380_H
#ifndef UGUILAYOUTCONTROLLERBASE_T1432848723_H
#define UGUILAYOUTCONTROLLERBASE_T1432848723_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.UguiLayoutControllerBase
struct  UguiLayoutControllerBase_t1432848723  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.RectTransform Utage.UguiLayoutControllerBase::cachedRectTransform
	RectTransform_t3349966182 * ___cachedRectTransform_2;
	// UnityEngine.DrivenRectTransformTracker Utage.UguiLayoutControllerBase::tracker
	DrivenRectTransformTracker_t154385424  ___tracker_3;

public:
	inline static int32_t get_offset_of_cachedRectTransform_2() { return static_cast<int32_t>(offsetof(UguiLayoutControllerBase_t1432848723, ___cachedRectTransform_2)); }
	inline RectTransform_t3349966182 * get_cachedRectTransform_2() const { return ___cachedRectTransform_2; }
	inline RectTransform_t3349966182 ** get_address_of_cachedRectTransform_2() { return &___cachedRectTransform_2; }
	inline void set_cachedRectTransform_2(RectTransform_t3349966182 * value)
	{
		___cachedRectTransform_2 = value;
		Il2CppCodeGenWriteBarrier((&___cachedRectTransform_2), value);
	}

	inline static int32_t get_offset_of_tracker_3() { return static_cast<int32_t>(offsetof(UguiLayoutControllerBase_t1432848723, ___tracker_3)); }
	inline DrivenRectTransformTracker_t154385424  get_tracker_3() const { return ___tracker_3; }
	inline DrivenRectTransformTracker_t154385424 * get_address_of_tracker_3() { return &___tracker_3; }
	inline void set_tracker_3(DrivenRectTransformTracker_t154385424  value)
	{
		___tracker_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UGUILAYOUTCONTROLLERBASE_T1432848723_H
#ifndef LIPSYNCHBASE_T2376160345_H
#define LIPSYNCHBASE_T2376160345_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.LipSynchBase
struct  LipSynchBase_t2376160345  : public MonoBehaviour_t1158329972
{
public:
	// Utage.LipSynchType Utage.LipSynchBase::type
	int32_t ___type_2;
	// System.Boolean Utage.LipSynchBase::<EnableTextLipSync>k__BackingField
	bool ___U3CEnableTextLipSyncU3Ek__BackingField_3;
	// Utage.LipSynchMode Utage.LipSynchBase::<LipSynchMode>k__BackingField
	int32_t ___U3CLipSynchModeU3Ek__BackingField_4;
	// Utage.LipSynchEvent Utage.LipSynchBase::OnCheckTextLipSync
	LipSynchEvent_t825925384 * ___OnCheckTextLipSync_5;
	// System.String Utage.LipSynchBase::characterLabel
	String_t* ___characterLabel_6;
	// System.Boolean Utage.LipSynchBase::<IsEnable>k__BackingField
	bool ___U3CIsEnableU3Ek__BackingField_7;
	// System.Boolean Utage.LipSynchBase::<IsPlaying>k__BackingField
	bool ___U3CIsPlayingU3Ek__BackingField_8;

public:
	inline static int32_t get_offset_of_type_2() { return static_cast<int32_t>(offsetof(LipSynchBase_t2376160345, ___type_2)); }
	inline int32_t get_type_2() const { return ___type_2; }
	inline int32_t* get_address_of_type_2() { return &___type_2; }
	inline void set_type_2(int32_t value)
	{
		___type_2 = value;
	}

	inline static int32_t get_offset_of_U3CEnableTextLipSyncU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(LipSynchBase_t2376160345, ___U3CEnableTextLipSyncU3Ek__BackingField_3)); }
	inline bool get_U3CEnableTextLipSyncU3Ek__BackingField_3() const { return ___U3CEnableTextLipSyncU3Ek__BackingField_3; }
	inline bool* get_address_of_U3CEnableTextLipSyncU3Ek__BackingField_3() { return &___U3CEnableTextLipSyncU3Ek__BackingField_3; }
	inline void set_U3CEnableTextLipSyncU3Ek__BackingField_3(bool value)
	{
		___U3CEnableTextLipSyncU3Ek__BackingField_3 = value;
	}

	inline static int32_t get_offset_of_U3CLipSynchModeU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(LipSynchBase_t2376160345, ___U3CLipSynchModeU3Ek__BackingField_4)); }
	inline int32_t get_U3CLipSynchModeU3Ek__BackingField_4() const { return ___U3CLipSynchModeU3Ek__BackingField_4; }
	inline int32_t* get_address_of_U3CLipSynchModeU3Ek__BackingField_4() { return &___U3CLipSynchModeU3Ek__BackingField_4; }
	inline void set_U3CLipSynchModeU3Ek__BackingField_4(int32_t value)
	{
		___U3CLipSynchModeU3Ek__BackingField_4 = value;
	}

	inline static int32_t get_offset_of_OnCheckTextLipSync_5() { return static_cast<int32_t>(offsetof(LipSynchBase_t2376160345, ___OnCheckTextLipSync_5)); }
	inline LipSynchEvent_t825925384 * get_OnCheckTextLipSync_5() const { return ___OnCheckTextLipSync_5; }
	inline LipSynchEvent_t825925384 ** get_address_of_OnCheckTextLipSync_5() { return &___OnCheckTextLipSync_5; }
	inline void set_OnCheckTextLipSync_5(LipSynchEvent_t825925384 * value)
	{
		___OnCheckTextLipSync_5 = value;
		Il2CppCodeGenWriteBarrier((&___OnCheckTextLipSync_5), value);
	}

	inline static int32_t get_offset_of_characterLabel_6() { return static_cast<int32_t>(offsetof(LipSynchBase_t2376160345, ___characterLabel_6)); }
	inline String_t* get_characterLabel_6() const { return ___characterLabel_6; }
	inline String_t** get_address_of_characterLabel_6() { return &___characterLabel_6; }
	inline void set_characterLabel_6(String_t* value)
	{
		___characterLabel_6 = value;
		Il2CppCodeGenWriteBarrier((&___characterLabel_6), value);
	}

	inline static int32_t get_offset_of_U3CIsEnableU3Ek__BackingField_7() { return static_cast<int32_t>(offsetof(LipSynchBase_t2376160345, ___U3CIsEnableU3Ek__BackingField_7)); }
	inline bool get_U3CIsEnableU3Ek__BackingField_7() const { return ___U3CIsEnableU3Ek__BackingField_7; }
	inline bool* get_address_of_U3CIsEnableU3Ek__BackingField_7() { return &___U3CIsEnableU3Ek__BackingField_7; }
	inline void set_U3CIsEnableU3Ek__BackingField_7(bool value)
	{
		___U3CIsEnableU3Ek__BackingField_7 = value;
	}

	inline static int32_t get_offset_of_U3CIsPlayingU3Ek__BackingField_8() { return static_cast<int32_t>(offsetof(LipSynchBase_t2376160345, ___U3CIsPlayingU3Ek__BackingField_8)); }
	inline bool get_U3CIsPlayingU3Ek__BackingField_8() const { return ___U3CIsPlayingU3Ek__BackingField_8; }
	inline bool* get_address_of_U3CIsPlayingU3Ek__BackingField_8() { return &___U3CIsPlayingU3Ek__BackingField_8; }
	inline void set_U3CIsPlayingU3Ek__BackingField_8(bool value)
	{
		___U3CIsPlayingU3Ek__BackingField_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LIPSYNCHBASE_T2376160345_H
#ifndef GRAPHIC_T2426225576_H
#define GRAPHIC_T2426225576_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Graphic
struct  Graphic_t2426225576  : public UIBehaviour_t3960014691
{
public:
	// UnityEngine.Material UnityEngine.UI.Graphic::m_Material
	Material_t193706927 * ___m_Material_4;
	// UnityEngine.Color UnityEngine.UI.Graphic::m_Color
	Color_t2020392075  ___m_Color_5;
	// System.Boolean UnityEngine.UI.Graphic::m_RaycastTarget
	bool ___m_RaycastTarget_6;
	// UnityEngine.RectTransform UnityEngine.UI.Graphic::m_RectTransform
	RectTransform_t3349966182 * ___m_RectTransform_7;
	// UnityEngine.CanvasRenderer UnityEngine.UI.Graphic::m_CanvasRender
	CanvasRenderer_t261436805 * ___m_CanvasRender_8;
	// UnityEngine.Canvas UnityEngine.UI.Graphic::m_Canvas
	Canvas_t209405766 * ___m_Canvas_9;
	// System.Boolean UnityEngine.UI.Graphic::m_VertsDirty
	bool ___m_VertsDirty_10;
	// System.Boolean UnityEngine.UI.Graphic::m_MaterialDirty
	bool ___m_MaterialDirty_11;
	// UnityEngine.Events.UnityAction UnityEngine.UI.Graphic::m_OnDirtyLayoutCallback
	UnityAction_t4025899511 * ___m_OnDirtyLayoutCallback_12;
	// UnityEngine.Events.UnityAction UnityEngine.UI.Graphic::m_OnDirtyVertsCallback
	UnityAction_t4025899511 * ___m_OnDirtyVertsCallback_13;
	// UnityEngine.Events.UnityAction UnityEngine.UI.Graphic::m_OnDirtyMaterialCallback
	UnityAction_t4025899511 * ___m_OnDirtyMaterialCallback_14;
	// UnityEngine.UI.CoroutineTween.TweenRunner`1<UnityEngine.UI.CoroutineTween.ColorTween> UnityEngine.UI.Graphic::m_ColorTweenRunner
	TweenRunner_1_t3177091249 * ___m_ColorTweenRunner_17;
	// System.Boolean UnityEngine.UI.Graphic::<useLegacyMeshGeneration>k__BackingField
	bool ___U3CuseLegacyMeshGenerationU3Ek__BackingField_18;

public:
	inline static int32_t get_offset_of_m_Material_4() { return static_cast<int32_t>(offsetof(Graphic_t2426225576, ___m_Material_4)); }
	inline Material_t193706927 * get_m_Material_4() const { return ___m_Material_4; }
	inline Material_t193706927 ** get_address_of_m_Material_4() { return &___m_Material_4; }
	inline void set_m_Material_4(Material_t193706927 * value)
	{
		___m_Material_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_Material_4), value);
	}

	inline static int32_t get_offset_of_m_Color_5() { return static_cast<int32_t>(offsetof(Graphic_t2426225576, ___m_Color_5)); }
	inline Color_t2020392075  get_m_Color_5() const { return ___m_Color_5; }
	inline Color_t2020392075 * get_address_of_m_Color_5() { return &___m_Color_5; }
	inline void set_m_Color_5(Color_t2020392075  value)
	{
		___m_Color_5 = value;
	}

	inline static int32_t get_offset_of_m_RaycastTarget_6() { return static_cast<int32_t>(offsetof(Graphic_t2426225576, ___m_RaycastTarget_6)); }
	inline bool get_m_RaycastTarget_6() const { return ___m_RaycastTarget_6; }
	inline bool* get_address_of_m_RaycastTarget_6() { return &___m_RaycastTarget_6; }
	inline void set_m_RaycastTarget_6(bool value)
	{
		___m_RaycastTarget_6 = value;
	}

	inline static int32_t get_offset_of_m_RectTransform_7() { return static_cast<int32_t>(offsetof(Graphic_t2426225576, ___m_RectTransform_7)); }
	inline RectTransform_t3349966182 * get_m_RectTransform_7() const { return ___m_RectTransform_7; }
	inline RectTransform_t3349966182 ** get_address_of_m_RectTransform_7() { return &___m_RectTransform_7; }
	inline void set_m_RectTransform_7(RectTransform_t3349966182 * value)
	{
		___m_RectTransform_7 = value;
		Il2CppCodeGenWriteBarrier((&___m_RectTransform_7), value);
	}

	inline static int32_t get_offset_of_m_CanvasRender_8() { return static_cast<int32_t>(offsetof(Graphic_t2426225576, ___m_CanvasRender_8)); }
	inline CanvasRenderer_t261436805 * get_m_CanvasRender_8() const { return ___m_CanvasRender_8; }
	inline CanvasRenderer_t261436805 ** get_address_of_m_CanvasRender_8() { return &___m_CanvasRender_8; }
	inline void set_m_CanvasRender_8(CanvasRenderer_t261436805 * value)
	{
		___m_CanvasRender_8 = value;
		Il2CppCodeGenWriteBarrier((&___m_CanvasRender_8), value);
	}

	inline static int32_t get_offset_of_m_Canvas_9() { return static_cast<int32_t>(offsetof(Graphic_t2426225576, ___m_Canvas_9)); }
	inline Canvas_t209405766 * get_m_Canvas_9() const { return ___m_Canvas_9; }
	inline Canvas_t209405766 ** get_address_of_m_Canvas_9() { return &___m_Canvas_9; }
	inline void set_m_Canvas_9(Canvas_t209405766 * value)
	{
		___m_Canvas_9 = value;
		Il2CppCodeGenWriteBarrier((&___m_Canvas_9), value);
	}

	inline static int32_t get_offset_of_m_VertsDirty_10() { return static_cast<int32_t>(offsetof(Graphic_t2426225576, ___m_VertsDirty_10)); }
	inline bool get_m_VertsDirty_10() const { return ___m_VertsDirty_10; }
	inline bool* get_address_of_m_VertsDirty_10() { return &___m_VertsDirty_10; }
	inline void set_m_VertsDirty_10(bool value)
	{
		___m_VertsDirty_10 = value;
	}

	inline static int32_t get_offset_of_m_MaterialDirty_11() { return static_cast<int32_t>(offsetof(Graphic_t2426225576, ___m_MaterialDirty_11)); }
	inline bool get_m_MaterialDirty_11() const { return ___m_MaterialDirty_11; }
	inline bool* get_address_of_m_MaterialDirty_11() { return &___m_MaterialDirty_11; }
	inline void set_m_MaterialDirty_11(bool value)
	{
		___m_MaterialDirty_11 = value;
	}

	inline static int32_t get_offset_of_m_OnDirtyLayoutCallback_12() { return static_cast<int32_t>(offsetof(Graphic_t2426225576, ___m_OnDirtyLayoutCallback_12)); }
	inline UnityAction_t4025899511 * get_m_OnDirtyLayoutCallback_12() const { return ___m_OnDirtyLayoutCallback_12; }
	inline UnityAction_t4025899511 ** get_address_of_m_OnDirtyLayoutCallback_12() { return &___m_OnDirtyLayoutCallback_12; }
	inline void set_m_OnDirtyLayoutCallback_12(UnityAction_t4025899511 * value)
	{
		___m_OnDirtyLayoutCallback_12 = value;
		Il2CppCodeGenWriteBarrier((&___m_OnDirtyLayoutCallback_12), value);
	}

	inline static int32_t get_offset_of_m_OnDirtyVertsCallback_13() { return static_cast<int32_t>(offsetof(Graphic_t2426225576, ___m_OnDirtyVertsCallback_13)); }
	inline UnityAction_t4025899511 * get_m_OnDirtyVertsCallback_13() const { return ___m_OnDirtyVertsCallback_13; }
	inline UnityAction_t4025899511 ** get_address_of_m_OnDirtyVertsCallback_13() { return &___m_OnDirtyVertsCallback_13; }
	inline void set_m_OnDirtyVertsCallback_13(UnityAction_t4025899511 * value)
	{
		___m_OnDirtyVertsCallback_13 = value;
		Il2CppCodeGenWriteBarrier((&___m_OnDirtyVertsCallback_13), value);
	}

	inline static int32_t get_offset_of_m_OnDirtyMaterialCallback_14() { return static_cast<int32_t>(offsetof(Graphic_t2426225576, ___m_OnDirtyMaterialCallback_14)); }
	inline UnityAction_t4025899511 * get_m_OnDirtyMaterialCallback_14() const { return ___m_OnDirtyMaterialCallback_14; }
	inline UnityAction_t4025899511 ** get_address_of_m_OnDirtyMaterialCallback_14() { return &___m_OnDirtyMaterialCallback_14; }
	inline void set_m_OnDirtyMaterialCallback_14(UnityAction_t4025899511 * value)
	{
		___m_OnDirtyMaterialCallback_14 = value;
		Il2CppCodeGenWriteBarrier((&___m_OnDirtyMaterialCallback_14), value);
	}

	inline static int32_t get_offset_of_m_ColorTweenRunner_17() { return static_cast<int32_t>(offsetof(Graphic_t2426225576, ___m_ColorTweenRunner_17)); }
	inline TweenRunner_1_t3177091249 * get_m_ColorTweenRunner_17() const { return ___m_ColorTweenRunner_17; }
	inline TweenRunner_1_t3177091249 ** get_address_of_m_ColorTweenRunner_17() { return &___m_ColorTweenRunner_17; }
	inline void set_m_ColorTweenRunner_17(TweenRunner_1_t3177091249 * value)
	{
		___m_ColorTweenRunner_17 = value;
		Il2CppCodeGenWriteBarrier((&___m_ColorTweenRunner_17), value);
	}

	inline static int32_t get_offset_of_U3CuseLegacyMeshGenerationU3Ek__BackingField_18() { return static_cast<int32_t>(offsetof(Graphic_t2426225576, ___U3CuseLegacyMeshGenerationU3Ek__BackingField_18)); }
	inline bool get_U3CuseLegacyMeshGenerationU3Ek__BackingField_18() const { return ___U3CuseLegacyMeshGenerationU3Ek__BackingField_18; }
	inline bool* get_address_of_U3CuseLegacyMeshGenerationU3Ek__BackingField_18() { return &___U3CuseLegacyMeshGenerationU3Ek__BackingField_18; }
	inline void set_U3CuseLegacyMeshGenerationU3Ek__BackingField_18(bool value)
	{
		___U3CuseLegacyMeshGenerationU3Ek__BackingField_18 = value;
	}
};

struct Graphic_t2426225576_StaticFields
{
public:
	// UnityEngine.Material UnityEngine.UI.Graphic::s_DefaultUI
	Material_t193706927 * ___s_DefaultUI_2;
	// UnityEngine.Texture2D UnityEngine.UI.Graphic::s_WhiteTexture
	Texture2D_t3542995729 * ___s_WhiteTexture_3;
	// UnityEngine.Mesh UnityEngine.UI.Graphic::s_Mesh
	Mesh_t1356156583 * ___s_Mesh_15;
	// UnityEngine.UI.VertexHelper UnityEngine.UI.Graphic::s_VertexHelper
	VertexHelper_t385374196 * ___s_VertexHelper_16;

public:
	inline static int32_t get_offset_of_s_DefaultUI_2() { return static_cast<int32_t>(offsetof(Graphic_t2426225576_StaticFields, ___s_DefaultUI_2)); }
	inline Material_t193706927 * get_s_DefaultUI_2() const { return ___s_DefaultUI_2; }
	inline Material_t193706927 ** get_address_of_s_DefaultUI_2() { return &___s_DefaultUI_2; }
	inline void set_s_DefaultUI_2(Material_t193706927 * value)
	{
		___s_DefaultUI_2 = value;
		Il2CppCodeGenWriteBarrier((&___s_DefaultUI_2), value);
	}

	inline static int32_t get_offset_of_s_WhiteTexture_3() { return static_cast<int32_t>(offsetof(Graphic_t2426225576_StaticFields, ___s_WhiteTexture_3)); }
	inline Texture2D_t3542995729 * get_s_WhiteTexture_3() const { return ___s_WhiteTexture_3; }
	inline Texture2D_t3542995729 ** get_address_of_s_WhiteTexture_3() { return &___s_WhiteTexture_3; }
	inline void set_s_WhiteTexture_3(Texture2D_t3542995729 * value)
	{
		___s_WhiteTexture_3 = value;
		Il2CppCodeGenWriteBarrier((&___s_WhiteTexture_3), value);
	}

	inline static int32_t get_offset_of_s_Mesh_15() { return static_cast<int32_t>(offsetof(Graphic_t2426225576_StaticFields, ___s_Mesh_15)); }
	inline Mesh_t1356156583 * get_s_Mesh_15() const { return ___s_Mesh_15; }
	inline Mesh_t1356156583 ** get_address_of_s_Mesh_15() { return &___s_Mesh_15; }
	inline void set_s_Mesh_15(Mesh_t1356156583 * value)
	{
		___s_Mesh_15 = value;
		Il2CppCodeGenWriteBarrier((&___s_Mesh_15), value);
	}

	inline static int32_t get_offset_of_s_VertexHelper_16() { return static_cast<int32_t>(offsetof(Graphic_t2426225576_StaticFields, ___s_VertexHelper_16)); }
	inline VertexHelper_t385374196 * get_s_VertexHelper_16() const { return ___s_VertexHelper_16; }
	inline VertexHelper_t385374196 ** get_address_of_s_VertexHelper_16() { return &___s_VertexHelper_16; }
	inline void set_s_VertexHelper_16(VertexHelper_t385374196 * value)
	{
		___s_VertexHelper_16 = value;
		Il2CppCodeGenWriteBarrier((&___s_VertexHelper_16), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GRAPHIC_T2426225576_H
#ifndef UGUILOCALIZETEXTSETTING_T2621520384_H
#define UGUILOCALIZETEXTSETTING_T2621520384_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.UguiLocalizeTextSetting
struct  UguiLocalizeTextSetting_t2621520384  : public UguiLocalizeBase_t685439682
{
public:
	// System.Collections.Generic.List`1<Utage.UguiLocalizeTextSetting/Setting> Utage.UguiLocalizeTextSetting::settingList
	List_1_t2913322537 * ___settingList_4;
	// Utage.UguiLocalizeTextSetting/Setting Utage.UguiLocalizeTextSetting::defaultSetting
	Setting_t3544201405 * ___defaultSetting_5;
	// UnityEngine.UI.Text Utage.UguiLocalizeTextSetting::cachedText
	Text_t356221433 * ___cachedText_6;

public:
	inline static int32_t get_offset_of_settingList_4() { return static_cast<int32_t>(offsetof(UguiLocalizeTextSetting_t2621520384, ___settingList_4)); }
	inline List_1_t2913322537 * get_settingList_4() const { return ___settingList_4; }
	inline List_1_t2913322537 ** get_address_of_settingList_4() { return &___settingList_4; }
	inline void set_settingList_4(List_1_t2913322537 * value)
	{
		___settingList_4 = value;
		Il2CppCodeGenWriteBarrier((&___settingList_4), value);
	}

	inline static int32_t get_offset_of_defaultSetting_5() { return static_cast<int32_t>(offsetof(UguiLocalizeTextSetting_t2621520384, ___defaultSetting_5)); }
	inline Setting_t3544201405 * get_defaultSetting_5() const { return ___defaultSetting_5; }
	inline Setting_t3544201405 ** get_address_of_defaultSetting_5() { return &___defaultSetting_5; }
	inline void set_defaultSetting_5(Setting_t3544201405 * value)
	{
		___defaultSetting_5 = value;
		Il2CppCodeGenWriteBarrier((&___defaultSetting_5), value);
	}

	inline static int32_t get_offset_of_cachedText_6() { return static_cast<int32_t>(offsetof(UguiLocalizeTextSetting_t2621520384, ___cachedText_6)); }
	inline Text_t356221433 * get_cachedText_6() const { return ___cachedText_6; }
	inline Text_t356221433 ** get_address_of_cachedText_6() { return &___cachedText_6; }
	inline void set_cachedText_6(Text_t356221433 * value)
	{
		___cachedText_6 = value;
		Il2CppCodeGenWriteBarrier((&___cachedText_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UGUILOCALIZETEXTSETTING_T2621520384_H
#ifndef UGUISIZEFITTER_T3082290857_H
#define UGUISIZEFITTER_T3082290857_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.UguiSizeFitter
struct  UguiSizeFitter_t3082290857  : public UguiLayoutControllerBase_t1432848723
{
public:
	// UnityEngine.RectTransform Utage.UguiSizeFitter::target
	RectTransform_t3349966182 * ___target_4;

public:
	inline static int32_t get_offset_of_target_4() { return static_cast<int32_t>(offsetof(UguiSizeFitter_t3082290857, ___target_4)); }
	inline RectTransform_t3349966182 * get_target_4() const { return ___target_4; }
	inline RectTransform_t3349966182 ** get_address_of_target_4() { return &___target_4; }
	inline void set_target_4(RectTransform_t3349966182 * value)
	{
		___target_4 = value;
		Il2CppCodeGenWriteBarrier((&___target_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UGUISIZEFITTER_T3082290857_H
#ifndef LIPSYNCH2D_T3152759062_H
#define LIPSYNCH2D_T3152759062_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.LipSynch2d
struct  LipSynch2d_t3152759062  : public LipSynchBase_t2376160345
{
public:
	// System.Single Utage.LipSynch2d::duration
	float ___duration_9;
	// System.Single Utage.LipSynch2d::interval
	float ___interval_10;
	// System.Single Utage.LipSynch2d::scaleVoiceVolume
	float ___scaleVoiceVolume_11;
	// System.String Utage.LipSynch2d::lipTag
	String_t* ___lipTag_12;
	// Utage.MiniAnimationData Utage.LipSynch2d::animationData
	MiniAnimationData_t521227391 * ___animationData_13;
	// System.Single Utage.LipSynch2d::<LipSyncVolume>k__BackingField
	float ___U3CLipSyncVolumeU3Ek__BackingField_14;
	// UnityEngine.GameObject Utage.LipSynch2d::target
	GameObject_t1756533147 * ___target_15;
	// UnityEngine.Coroutine Utage.LipSynch2d::coLypSync
	Coroutine_t2299508840 * ___coLypSync_16;

public:
	inline static int32_t get_offset_of_duration_9() { return static_cast<int32_t>(offsetof(LipSynch2d_t3152759062, ___duration_9)); }
	inline float get_duration_9() const { return ___duration_9; }
	inline float* get_address_of_duration_9() { return &___duration_9; }
	inline void set_duration_9(float value)
	{
		___duration_9 = value;
	}

	inline static int32_t get_offset_of_interval_10() { return static_cast<int32_t>(offsetof(LipSynch2d_t3152759062, ___interval_10)); }
	inline float get_interval_10() const { return ___interval_10; }
	inline float* get_address_of_interval_10() { return &___interval_10; }
	inline void set_interval_10(float value)
	{
		___interval_10 = value;
	}

	inline static int32_t get_offset_of_scaleVoiceVolume_11() { return static_cast<int32_t>(offsetof(LipSynch2d_t3152759062, ___scaleVoiceVolume_11)); }
	inline float get_scaleVoiceVolume_11() const { return ___scaleVoiceVolume_11; }
	inline float* get_address_of_scaleVoiceVolume_11() { return &___scaleVoiceVolume_11; }
	inline void set_scaleVoiceVolume_11(float value)
	{
		___scaleVoiceVolume_11 = value;
	}

	inline static int32_t get_offset_of_lipTag_12() { return static_cast<int32_t>(offsetof(LipSynch2d_t3152759062, ___lipTag_12)); }
	inline String_t* get_lipTag_12() const { return ___lipTag_12; }
	inline String_t** get_address_of_lipTag_12() { return &___lipTag_12; }
	inline void set_lipTag_12(String_t* value)
	{
		___lipTag_12 = value;
		Il2CppCodeGenWriteBarrier((&___lipTag_12), value);
	}

	inline static int32_t get_offset_of_animationData_13() { return static_cast<int32_t>(offsetof(LipSynch2d_t3152759062, ___animationData_13)); }
	inline MiniAnimationData_t521227391 * get_animationData_13() const { return ___animationData_13; }
	inline MiniAnimationData_t521227391 ** get_address_of_animationData_13() { return &___animationData_13; }
	inline void set_animationData_13(MiniAnimationData_t521227391 * value)
	{
		___animationData_13 = value;
		Il2CppCodeGenWriteBarrier((&___animationData_13), value);
	}

	inline static int32_t get_offset_of_U3CLipSyncVolumeU3Ek__BackingField_14() { return static_cast<int32_t>(offsetof(LipSynch2d_t3152759062, ___U3CLipSyncVolumeU3Ek__BackingField_14)); }
	inline float get_U3CLipSyncVolumeU3Ek__BackingField_14() const { return ___U3CLipSyncVolumeU3Ek__BackingField_14; }
	inline float* get_address_of_U3CLipSyncVolumeU3Ek__BackingField_14() { return &___U3CLipSyncVolumeU3Ek__BackingField_14; }
	inline void set_U3CLipSyncVolumeU3Ek__BackingField_14(float value)
	{
		___U3CLipSyncVolumeU3Ek__BackingField_14 = value;
	}

	inline static int32_t get_offset_of_target_15() { return static_cast<int32_t>(offsetof(LipSynch2d_t3152759062, ___target_15)); }
	inline GameObject_t1756533147 * get_target_15() const { return ___target_15; }
	inline GameObject_t1756533147 ** get_address_of_target_15() { return &___target_15; }
	inline void set_target_15(GameObject_t1756533147 * value)
	{
		___target_15 = value;
		Il2CppCodeGenWriteBarrier((&___target_15), value);
	}

	inline static int32_t get_offset_of_coLypSync_16() { return static_cast<int32_t>(offsetof(LipSynch2d_t3152759062, ___coLypSync_16)); }
	inline Coroutine_t2299508840 * get_coLypSync_16() const { return ___coLypSync_16; }
	inline Coroutine_t2299508840 ** get_address_of_coLypSync_16() { return &___coLypSync_16; }
	inline void set_coLypSync_16(Coroutine_t2299508840 * value)
	{
		___coLypSync_16 = value;
		Il2CppCodeGenWriteBarrier((&___coLypSync_16), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LIPSYNCH2D_T3152759062_H
#ifndef EYEBLINKDICING_T891209267_H
#define EYEBLINKDICING_T891209267_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.EyeBlinkDicing
struct  EyeBlinkDicing_t891209267  : public EyeBlinkBase_t1662968566
{
public:
	// Utage.DicingImage Utage.EyeBlinkDicing::dicing
	DicingImage_t2721298607 * ___dicing_7;

public:
	inline static int32_t get_offset_of_dicing_7() { return static_cast<int32_t>(offsetof(EyeBlinkDicing_t891209267, ___dicing_7)); }
	inline DicingImage_t2721298607 * get_dicing_7() const { return ___dicing_7; }
	inline DicingImage_t2721298607 ** get_address_of_dicing_7() { return &___dicing_7; }
	inline void set_dicing_7(DicingImage_t2721298607 * value)
	{
		___dicing_7 = value;
		Il2CppCodeGenWriteBarrier((&___dicing_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EYEBLINKDICING_T891209267_H
#ifndef UGUILOCALIZE_T1715753735_H
#define UGUILOCALIZE_T1715753735_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.UguiLocalize
struct  UguiLocalize_t1715753735  : public UguiLocalizeBase_t685439682
{
public:
	// System.String Utage.UguiLocalize::key
	String_t* ___key_4;
	// System.String Utage.UguiLocalize::defaultText
	String_t* ___defaultText_5;
	// UnityEngine.UI.Text Utage.UguiLocalize::cachedText
	Text_t356221433 * ___cachedText_6;

public:
	inline static int32_t get_offset_of_key_4() { return static_cast<int32_t>(offsetof(UguiLocalize_t1715753735, ___key_4)); }
	inline String_t* get_key_4() const { return ___key_4; }
	inline String_t** get_address_of_key_4() { return &___key_4; }
	inline void set_key_4(String_t* value)
	{
		___key_4 = value;
		Il2CppCodeGenWriteBarrier((&___key_4), value);
	}

	inline static int32_t get_offset_of_defaultText_5() { return static_cast<int32_t>(offsetof(UguiLocalize_t1715753735, ___defaultText_5)); }
	inline String_t* get_defaultText_5() const { return ___defaultText_5; }
	inline String_t** get_address_of_defaultText_5() { return &___defaultText_5; }
	inline void set_defaultText_5(String_t* value)
	{
		___defaultText_5 = value;
		Il2CppCodeGenWriteBarrier((&___defaultText_5), value);
	}

	inline static int32_t get_offset_of_cachedText_6() { return static_cast<int32_t>(offsetof(UguiLocalize_t1715753735, ___cachedText_6)); }
	inline Text_t356221433 * get_cachedText_6() const { return ___cachedText_6; }
	inline Text_t356221433 ** get_address_of_cachedText_6() { return &___cachedText_6; }
	inline void set_cachedText_6(Text_t356221433 * value)
	{
		___cachedText_6 = value;
		Il2CppCodeGenWriteBarrier((&___cachedText_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UGUILOCALIZE_T1715753735_H
#ifndef UGUILETTERBOXCANVASSCALER_T1623094149_H
#define UGUILETTERBOXCANVASSCALER_T1623094149_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.UguiLetterBoxCanvasScaler
struct  UguiLetterBoxCanvasScaler_t1623094149  : public UguiLayoutControllerBase_t1432848723
{
public:
	// UnityEngine.Canvas Utage.UguiLetterBoxCanvasScaler::canvas
	Canvas_t209405766 * ___canvas_4;
	// Utage.LetterBoxCamera Utage.UguiLetterBoxCanvasScaler::letterBoxCamera
	LetterBoxCamera_t3507617684 * ___letterBoxCamera_5;

public:
	inline static int32_t get_offset_of_canvas_4() { return static_cast<int32_t>(offsetof(UguiLetterBoxCanvasScaler_t1623094149, ___canvas_4)); }
	inline Canvas_t209405766 * get_canvas_4() const { return ___canvas_4; }
	inline Canvas_t209405766 ** get_address_of_canvas_4() { return &___canvas_4; }
	inline void set_canvas_4(Canvas_t209405766 * value)
	{
		___canvas_4 = value;
		Il2CppCodeGenWriteBarrier((&___canvas_4), value);
	}

	inline static int32_t get_offset_of_letterBoxCamera_5() { return static_cast<int32_t>(offsetof(UguiLetterBoxCanvasScaler_t1623094149, ___letterBoxCamera_5)); }
	inline LetterBoxCamera_t3507617684 * get_letterBoxCamera_5() const { return ___letterBoxCamera_5; }
	inline LetterBoxCamera_t3507617684 ** get_address_of_letterBoxCamera_5() { return &___letterBoxCamera_5; }
	inline void set_letterBoxCamera_5(LetterBoxCamera_t3507617684 * value)
	{
		___letterBoxCamera_5 = value;
		Il2CppCodeGenWriteBarrier((&___letterBoxCamera_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UGUILETTERBOXCANVASSCALER_T1623094149_H
#ifndef BASERAYCASTER_T2336171397_H
#define BASERAYCASTER_T2336171397_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.EventSystems.BaseRaycaster
struct  BaseRaycaster_t2336171397  : public UIBehaviour_t3960014691
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BASERAYCASTER_T2336171397_H
#ifndef UGUILOCALIZERECTTRANSFORM_T2564129487_H
#define UGUILOCALIZERECTTRANSFORM_T2564129487_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.UguiLocalizeRectTransform
struct  UguiLocalizeRectTransform_t2564129487  : public UguiLocalizeBase_t685439682
{
public:
	// System.Collections.Generic.List`1<Utage.UguiLocalizeRectTransform/Setting> Utage.UguiLocalizeRectTransform::settingList
	List_1_t1739457176 * ___settingList_4;
	// Utage.UguiLocalizeRectTransform/Setting Utage.UguiLocalizeRectTransform::defaultSetting
	Setting_t2370336044 * ___defaultSetting_5;
	// UnityEngine.RectTransform Utage.UguiLocalizeRectTransform::cachedRectTransform
	RectTransform_t3349966182 * ___cachedRectTransform_6;
	// System.Boolean Utage.UguiLocalizeRectTransform::<HasChanged>k__BackingField
	bool ___U3CHasChangedU3Ek__BackingField_7;

public:
	inline static int32_t get_offset_of_settingList_4() { return static_cast<int32_t>(offsetof(UguiLocalizeRectTransform_t2564129487, ___settingList_4)); }
	inline List_1_t1739457176 * get_settingList_4() const { return ___settingList_4; }
	inline List_1_t1739457176 ** get_address_of_settingList_4() { return &___settingList_4; }
	inline void set_settingList_4(List_1_t1739457176 * value)
	{
		___settingList_4 = value;
		Il2CppCodeGenWriteBarrier((&___settingList_4), value);
	}

	inline static int32_t get_offset_of_defaultSetting_5() { return static_cast<int32_t>(offsetof(UguiLocalizeRectTransform_t2564129487, ___defaultSetting_5)); }
	inline Setting_t2370336044 * get_defaultSetting_5() const { return ___defaultSetting_5; }
	inline Setting_t2370336044 ** get_address_of_defaultSetting_5() { return &___defaultSetting_5; }
	inline void set_defaultSetting_5(Setting_t2370336044 * value)
	{
		___defaultSetting_5 = value;
		Il2CppCodeGenWriteBarrier((&___defaultSetting_5), value);
	}

	inline static int32_t get_offset_of_cachedRectTransform_6() { return static_cast<int32_t>(offsetof(UguiLocalizeRectTransform_t2564129487, ___cachedRectTransform_6)); }
	inline RectTransform_t3349966182 * get_cachedRectTransform_6() const { return ___cachedRectTransform_6; }
	inline RectTransform_t3349966182 ** get_address_of_cachedRectTransform_6() { return &___cachedRectTransform_6; }
	inline void set_cachedRectTransform_6(RectTransform_t3349966182 * value)
	{
		___cachedRectTransform_6 = value;
		Il2CppCodeGenWriteBarrier((&___cachedRectTransform_6), value);
	}

	inline static int32_t get_offset_of_U3CHasChangedU3Ek__BackingField_7() { return static_cast<int32_t>(offsetof(UguiLocalizeRectTransform_t2564129487, ___U3CHasChangedU3Ek__BackingField_7)); }
	inline bool get_U3CHasChangedU3Ek__BackingField_7() const { return ___U3CHasChangedU3Ek__BackingField_7; }
	inline bool* get_address_of_U3CHasChangedU3Ek__BackingField_7() { return &___U3CHasChangedU3Ek__BackingField_7; }
	inline void set_U3CHasChangedU3Ek__BackingField_7(bool value)
	{
		___U3CHasChangedU3Ek__BackingField_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UGUILOCALIZERECTTRANSFORM_T2564129487_H
#ifndef UGUIALIGNGROUP_T4030345508_H
#define UGUIALIGNGROUP_T4030345508_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.UguiAlignGroup
struct  UguiAlignGroup_t4030345508  : public UguiLayoutControllerBase_t1432848723
{
public:
	// System.Boolean Utage.UguiAlignGroup::isAutoResize
	bool ___isAutoResize_4;
	// System.Single Utage.UguiAlignGroup::space
	float ___space_5;

public:
	inline static int32_t get_offset_of_isAutoResize_4() { return static_cast<int32_t>(offsetof(UguiAlignGroup_t4030345508, ___isAutoResize_4)); }
	inline bool get_isAutoResize_4() const { return ___isAutoResize_4; }
	inline bool* get_address_of_isAutoResize_4() { return &___isAutoResize_4; }
	inline void set_isAutoResize_4(bool value)
	{
		___isAutoResize_4 = value;
	}

	inline static int32_t get_offset_of_space_5() { return static_cast<int32_t>(offsetof(UguiAlignGroup_t4030345508, ___space_5)); }
	inline float get_space_5() const { return ___space_5; }
	inline float* get_address_of_space_5() { return &___space_5; }
	inline void set_space_5(float value)
	{
		___space_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UGUIALIGNGROUP_T4030345508_H
#ifndef LIPSYNCHDICING_T1869722688_H
#define LIPSYNCHDICING_T1869722688_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.LipSynchDicing
struct  LipSynchDicing_t1869722688  : public LipSynch2d_t3152759062
{
public:
	// Utage.DicingImage Utage.LipSynchDicing::dicing
	DicingImage_t2721298607 * ___dicing_17;

public:
	inline static int32_t get_offset_of_dicing_17() { return static_cast<int32_t>(offsetof(LipSynchDicing_t1869722688, ___dicing_17)); }
	inline DicingImage_t2721298607 * get_dicing_17() const { return ___dicing_17; }
	inline DicingImage_t2721298607 ** get_address_of_dicing_17() { return &___dicing_17; }
	inline void set_dicing_17(DicingImage_t2721298607 * value)
	{
		___dicing_17 = value;
		Il2CppCodeGenWriteBarrier((&___dicing_17), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LIPSYNCHDICING_T1869722688_H
#ifndef UGUIHORIZONTALALIGNGROUP_T3654853588_H
#define UGUIHORIZONTALALIGNGROUP_T3654853588_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.UguiHorizontalAlignGroup
struct  UguiHorizontalAlignGroup_t3654853588  : public UguiAlignGroup_t4030345508
{
public:
	// System.Single Utage.UguiHorizontalAlignGroup::paddingLeft
	float ___paddingLeft_6;
	// System.Single Utage.UguiHorizontalAlignGroup::paddingRight
	float ___paddingRight_7;
	// Utage.UguiHorizontalAlignGroup/AlignDirection Utage.UguiHorizontalAlignGroup::direction
	int32_t ___direction_8;

public:
	inline static int32_t get_offset_of_paddingLeft_6() { return static_cast<int32_t>(offsetof(UguiHorizontalAlignGroup_t3654853588, ___paddingLeft_6)); }
	inline float get_paddingLeft_6() const { return ___paddingLeft_6; }
	inline float* get_address_of_paddingLeft_6() { return &___paddingLeft_6; }
	inline void set_paddingLeft_6(float value)
	{
		___paddingLeft_6 = value;
	}

	inline static int32_t get_offset_of_paddingRight_7() { return static_cast<int32_t>(offsetof(UguiHorizontalAlignGroup_t3654853588, ___paddingRight_7)); }
	inline float get_paddingRight_7() const { return ___paddingRight_7; }
	inline float* get_address_of_paddingRight_7() { return &___paddingRight_7; }
	inline void set_paddingRight_7(float value)
	{
		___paddingRight_7 = value;
	}

	inline static int32_t get_offset_of_direction_8() { return static_cast<int32_t>(offsetof(UguiHorizontalAlignGroup_t3654853588, ___direction_8)); }
	inline int32_t get_direction_8() const { return ___direction_8; }
	inline int32_t* get_address_of_direction_8() { return &___direction_8; }
	inline void set_direction_8(int32_t value)
	{
		___direction_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UGUIHORIZONTALALIGNGROUP_T3654853588_H
#ifndef UGUIVERTICALALIGNGROUP_T1024558358_H
#define UGUIVERTICALALIGNGROUP_T1024558358_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.UguiVerticalAlignGroup
struct  UguiVerticalAlignGroup_t1024558358  : public UguiAlignGroup_t4030345508
{
public:
	// System.Single Utage.UguiVerticalAlignGroup::paddingTop
	float ___paddingTop_6;
	// System.Single Utage.UguiVerticalAlignGroup::paddingBottom
	float ___paddingBottom_7;
	// Utage.UguiVerticalAlignGroup/AlignDirection Utage.UguiVerticalAlignGroup::direction
	int32_t ___direction_8;

public:
	inline static int32_t get_offset_of_paddingTop_6() { return static_cast<int32_t>(offsetof(UguiVerticalAlignGroup_t1024558358, ___paddingTop_6)); }
	inline float get_paddingTop_6() const { return ___paddingTop_6; }
	inline float* get_address_of_paddingTop_6() { return &___paddingTop_6; }
	inline void set_paddingTop_6(float value)
	{
		___paddingTop_6 = value;
	}

	inline static int32_t get_offset_of_paddingBottom_7() { return static_cast<int32_t>(offsetof(UguiVerticalAlignGroup_t1024558358, ___paddingBottom_7)); }
	inline float get_paddingBottom_7() const { return ___paddingBottom_7; }
	inline float* get_address_of_paddingBottom_7() { return &___paddingBottom_7; }
	inline void set_paddingBottom_7(float value)
	{
		___paddingBottom_7 = value;
	}

	inline static int32_t get_offset_of_direction_8() { return static_cast<int32_t>(offsetof(UguiVerticalAlignGroup_t1024558358, ___direction_8)); }
	inline int32_t get_direction_8() const { return ___direction_8; }
	inline int32_t* get_address_of_direction_8() { return &___direction_8; }
	inline void set_direction_8(int32_t value)
	{
		___direction_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UGUIVERTICALALIGNGROUP_T1024558358_H
#ifndef UGUIBACKGROUNDRAYCASTER_T588614668_H
#define UGUIBACKGROUNDRAYCASTER_T588614668_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.UguiBackgroundRaycaster
struct  UguiBackgroundRaycaster_t588614668  : public BaseRaycaster_t2336171397
{
public:
	// UnityEngine.Camera Utage.UguiBackgroundRaycaster::cachedCamera
	Camera_t189460977 * ___cachedCamera_2;
	// Utage.LetterBoxCamera Utage.UguiBackgroundRaycaster::letterBoxCamera
	LetterBoxCamera_t3507617684 * ___letterBoxCamera_3;
	// System.Int32 Utage.UguiBackgroundRaycaster::m_Priority
	int32_t ___m_Priority_4;
	// System.Collections.Generic.List`1<UnityEngine.GameObject> Utage.UguiBackgroundRaycaster::targetObjectList
	List_1_t1125654279 * ___targetObjectList_5;

public:
	inline static int32_t get_offset_of_cachedCamera_2() { return static_cast<int32_t>(offsetof(UguiBackgroundRaycaster_t588614668, ___cachedCamera_2)); }
	inline Camera_t189460977 * get_cachedCamera_2() const { return ___cachedCamera_2; }
	inline Camera_t189460977 ** get_address_of_cachedCamera_2() { return &___cachedCamera_2; }
	inline void set_cachedCamera_2(Camera_t189460977 * value)
	{
		___cachedCamera_2 = value;
		Il2CppCodeGenWriteBarrier((&___cachedCamera_2), value);
	}

	inline static int32_t get_offset_of_letterBoxCamera_3() { return static_cast<int32_t>(offsetof(UguiBackgroundRaycaster_t588614668, ___letterBoxCamera_3)); }
	inline LetterBoxCamera_t3507617684 * get_letterBoxCamera_3() const { return ___letterBoxCamera_3; }
	inline LetterBoxCamera_t3507617684 ** get_address_of_letterBoxCamera_3() { return &___letterBoxCamera_3; }
	inline void set_letterBoxCamera_3(LetterBoxCamera_t3507617684 * value)
	{
		___letterBoxCamera_3 = value;
		Il2CppCodeGenWriteBarrier((&___letterBoxCamera_3), value);
	}

	inline static int32_t get_offset_of_m_Priority_4() { return static_cast<int32_t>(offsetof(UguiBackgroundRaycaster_t588614668, ___m_Priority_4)); }
	inline int32_t get_m_Priority_4() const { return ___m_Priority_4; }
	inline int32_t* get_address_of_m_Priority_4() { return &___m_Priority_4; }
	inline void set_m_Priority_4(int32_t value)
	{
		___m_Priority_4 = value;
	}

	inline static int32_t get_offset_of_targetObjectList_5() { return static_cast<int32_t>(offsetof(UguiBackgroundRaycaster_t588614668, ___targetObjectList_5)); }
	inline List_1_t1125654279 * get_targetObjectList_5() const { return ___targetObjectList_5; }
	inline List_1_t1125654279 ** get_address_of_targetObjectList_5() { return &___targetObjectList_5; }
	inline void set_targetObjectList_5(List_1_t1125654279 * value)
	{
		___targetObjectList_5 = value;
		Il2CppCodeGenWriteBarrier((&___targetObjectList_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UGUIBACKGROUNDRAYCASTER_T588614668_H
#ifndef LIPSYNCHAVATAR_T872933325_H
#define LIPSYNCHAVATAR_T872933325_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.LipSynchAvatar
struct  LipSynchAvatar_t872933325  : public LipSynch2d_t3152759062
{
public:
	// Utage.AvatarImage Utage.LipSynchAvatar::avator
	AvatarImage_t1946614104 * ___avator_17;

public:
	inline static int32_t get_offset_of_avator_17() { return static_cast<int32_t>(offsetof(LipSynchAvatar_t872933325, ___avator_17)); }
	inline AvatarImage_t1946614104 * get_avator_17() const { return ___avator_17; }
	inline AvatarImage_t1946614104 ** get_address_of_avator_17() { return &___avator_17; }
	inline void set_avator_17(AvatarImage_t1946614104 * value)
	{
		___avator_17 = value;
		Il2CppCodeGenWriteBarrier((&___avator_17), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LIPSYNCHAVATAR_T872933325_H
#ifndef MASKABLEGRAPHIC_T540192618_H
#define MASKABLEGRAPHIC_T540192618_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.MaskableGraphic
struct  MaskableGraphic_t540192618  : public Graphic_t2426225576
{
public:
	// System.Boolean UnityEngine.UI.MaskableGraphic::m_ShouldRecalculateStencil
	bool ___m_ShouldRecalculateStencil_19;
	// UnityEngine.Material UnityEngine.UI.MaskableGraphic::m_MaskMaterial
	Material_t193706927 * ___m_MaskMaterial_20;
	// UnityEngine.UI.RectMask2D UnityEngine.UI.MaskableGraphic::m_ParentMask
	RectMask2D_t1156185964 * ___m_ParentMask_21;
	// System.Boolean UnityEngine.UI.MaskableGraphic::m_Maskable
	bool ___m_Maskable_22;
	// System.Boolean UnityEngine.UI.MaskableGraphic::m_IncludeForMasking
	bool ___m_IncludeForMasking_23;
	// UnityEngine.UI.MaskableGraphic/CullStateChangedEvent UnityEngine.UI.MaskableGraphic::m_OnCullStateChanged
	CullStateChangedEvent_t3778758259 * ___m_OnCullStateChanged_24;
	// System.Boolean UnityEngine.UI.MaskableGraphic::m_ShouldRecalculate
	bool ___m_ShouldRecalculate_25;
	// System.Int32 UnityEngine.UI.MaskableGraphic::m_StencilValue
	int32_t ___m_StencilValue_26;
	// UnityEngine.Vector3[] UnityEngine.UI.MaskableGraphic::m_Corners
	Vector3U5BU5D_t1172311765* ___m_Corners_27;

public:
	inline static int32_t get_offset_of_m_ShouldRecalculateStencil_19() { return static_cast<int32_t>(offsetof(MaskableGraphic_t540192618, ___m_ShouldRecalculateStencil_19)); }
	inline bool get_m_ShouldRecalculateStencil_19() const { return ___m_ShouldRecalculateStencil_19; }
	inline bool* get_address_of_m_ShouldRecalculateStencil_19() { return &___m_ShouldRecalculateStencil_19; }
	inline void set_m_ShouldRecalculateStencil_19(bool value)
	{
		___m_ShouldRecalculateStencil_19 = value;
	}

	inline static int32_t get_offset_of_m_MaskMaterial_20() { return static_cast<int32_t>(offsetof(MaskableGraphic_t540192618, ___m_MaskMaterial_20)); }
	inline Material_t193706927 * get_m_MaskMaterial_20() const { return ___m_MaskMaterial_20; }
	inline Material_t193706927 ** get_address_of_m_MaskMaterial_20() { return &___m_MaskMaterial_20; }
	inline void set_m_MaskMaterial_20(Material_t193706927 * value)
	{
		___m_MaskMaterial_20 = value;
		Il2CppCodeGenWriteBarrier((&___m_MaskMaterial_20), value);
	}

	inline static int32_t get_offset_of_m_ParentMask_21() { return static_cast<int32_t>(offsetof(MaskableGraphic_t540192618, ___m_ParentMask_21)); }
	inline RectMask2D_t1156185964 * get_m_ParentMask_21() const { return ___m_ParentMask_21; }
	inline RectMask2D_t1156185964 ** get_address_of_m_ParentMask_21() { return &___m_ParentMask_21; }
	inline void set_m_ParentMask_21(RectMask2D_t1156185964 * value)
	{
		___m_ParentMask_21 = value;
		Il2CppCodeGenWriteBarrier((&___m_ParentMask_21), value);
	}

	inline static int32_t get_offset_of_m_Maskable_22() { return static_cast<int32_t>(offsetof(MaskableGraphic_t540192618, ___m_Maskable_22)); }
	inline bool get_m_Maskable_22() const { return ___m_Maskable_22; }
	inline bool* get_address_of_m_Maskable_22() { return &___m_Maskable_22; }
	inline void set_m_Maskable_22(bool value)
	{
		___m_Maskable_22 = value;
	}

	inline static int32_t get_offset_of_m_IncludeForMasking_23() { return static_cast<int32_t>(offsetof(MaskableGraphic_t540192618, ___m_IncludeForMasking_23)); }
	inline bool get_m_IncludeForMasking_23() const { return ___m_IncludeForMasking_23; }
	inline bool* get_address_of_m_IncludeForMasking_23() { return &___m_IncludeForMasking_23; }
	inline void set_m_IncludeForMasking_23(bool value)
	{
		___m_IncludeForMasking_23 = value;
	}

	inline static int32_t get_offset_of_m_OnCullStateChanged_24() { return static_cast<int32_t>(offsetof(MaskableGraphic_t540192618, ___m_OnCullStateChanged_24)); }
	inline CullStateChangedEvent_t3778758259 * get_m_OnCullStateChanged_24() const { return ___m_OnCullStateChanged_24; }
	inline CullStateChangedEvent_t3778758259 ** get_address_of_m_OnCullStateChanged_24() { return &___m_OnCullStateChanged_24; }
	inline void set_m_OnCullStateChanged_24(CullStateChangedEvent_t3778758259 * value)
	{
		___m_OnCullStateChanged_24 = value;
		Il2CppCodeGenWriteBarrier((&___m_OnCullStateChanged_24), value);
	}

	inline static int32_t get_offset_of_m_ShouldRecalculate_25() { return static_cast<int32_t>(offsetof(MaskableGraphic_t540192618, ___m_ShouldRecalculate_25)); }
	inline bool get_m_ShouldRecalculate_25() const { return ___m_ShouldRecalculate_25; }
	inline bool* get_address_of_m_ShouldRecalculate_25() { return &___m_ShouldRecalculate_25; }
	inline void set_m_ShouldRecalculate_25(bool value)
	{
		___m_ShouldRecalculate_25 = value;
	}

	inline static int32_t get_offset_of_m_StencilValue_26() { return static_cast<int32_t>(offsetof(MaskableGraphic_t540192618, ___m_StencilValue_26)); }
	inline int32_t get_m_StencilValue_26() const { return ___m_StencilValue_26; }
	inline int32_t* get_address_of_m_StencilValue_26() { return &___m_StencilValue_26; }
	inline void set_m_StencilValue_26(int32_t value)
	{
		___m_StencilValue_26 = value;
	}

	inline static int32_t get_offset_of_m_Corners_27() { return static_cast<int32_t>(offsetof(MaskableGraphic_t540192618, ___m_Corners_27)); }
	inline Vector3U5BU5D_t1172311765* get_m_Corners_27() const { return ___m_Corners_27; }
	inline Vector3U5BU5D_t1172311765** get_address_of_m_Corners_27() { return &___m_Corners_27; }
	inline void set_m_Corners_27(Vector3U5BU5D_t1172311765* value)
	{
		___m_Corners_27 = value;
		Il2CppCodeGenWriteBarrier((&___m_Corners_27), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MASKABLEGRAPHIC_T540192618_H
#ifndef UGUIHORIZONTALALIGNGROUPSCALEEFFECT_T1380971095_H
#define UGUIHORIZONTALALIGNGROUPSCALEEFFECT_T1380971095_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.UguiHorizontalAlignGroupScaleEffect
struct  UguiHorizontalAlignGroupScaleEffect_t1380971095  : public UguiHorizontalAlignGroup_t3654853588
{
public:
	// System.Single Utage.UguiHorizontalAlignGroupScaleEffect::scaleRangeLeft
	float ___scaleRangeLeft_9;
	// System.Single Utage.UguiHorizontalAlignGroupScaleEffect::scaleRangeWidth
	float ___scaleRangeWidth_10;
	// System.Boolean Utage.UguiHorizontalAlignGroupScaleEffect::ignoreLocalPositionToScaleEffectRage
	bool ___ignoreLocalPositionToScaleEffectRage_11;
	// System.Single Utage.UguiHorizontalAlignGroupScaleEffect::minScale
	float ___minScale_12;
	// System.Single Utage.UguiHorizontalAlignGroupScaleEffect::maxScale
	float ___maxScale_13;

public:
	inline static int32_t get_offset_of_scaleRangeLeft_9() { return static_cast<int32_t>(offsetof(UguiHorizontalAlignGroupScaleEffect_t1380971095, ___scaleRangeLeft_9)); }
	inline float get_scaleRangeLeft_9() const { return ___scaleRangeLeft_9; }
	inline float* get_address_of_scaleRangeLeft_9() { return &___scaleRangeLeft_9; }
	inline void set_scaleRangeLeft_9(float value)
	{
		___scaleRangeLeft_9 = value;
	}

	inline static int32_t get_offset_of_scaleRangeWidth_10() { return static_cast<int32_t>(offsetof(UguiHorizontalAlignGroupScaleEffect_t1380971095, ___scaleRangeWidth_10)); }
	inline float get_scaleRangeWidth_10() const { return ___scaleRangeWidth_10; }
	inline float* get_address_of_scaleRangeWidth_10() { return &___scaleRangeWidth_10; }
	inline void set_scaleRangeWidth_10(float value)
	{
		___scaleRangeWidth_10 = value;
	}

	inline static int32_t get_offset_of_ignoreLocalPositionToScaleEffectRage_11() { return static_cast<int32_t>(offsetof(UguiHorizontalAlignGroupScaleEffect_t1380971095, ___ignoreLocalPositionToScaleEffectRage_11)); }
	inline bool get_ignoreLocalPositionToScaleEffectRage_11() const { return ___ignoreLocalPositionToScaleEffectRage_11; }
	inline bool* get_address_of_ignoreLocalPositionToScaleEffectRage_11() { return &___ignoreLocalPositionToScaleEffectRage_11; }
	inline void set_ignoreLocalPositionToScaleEffectRage_11(bool value)
	{
		___ignoreLocalPositionToScaleEffectRage_11 = value;
	}

	inline static int32_t get_offset_of_minScale_12() { return static_cast<int32_t>(offsetof(UguiHorizontalAlignGroupScaleEffect_t1380971095, ___minScale_12)); }
	inline float get_minScale_12() const { return ___minScale_12; }
	inline float* get_address_of_minScale_12() { return &___minScale_12; }
	inline void set_minScale_12(float value)
	{
		___minScale_12 = value;
	}

	inline static int32_t get_offset_of_maxScale_13() { return static_cast<int32_t>(offsetof(UguiHorizontalAlignGroupScaleEffect_t1380971095, ___maxScale_13)); }
	inline float get_maxScale_13() const { return ___maxScale_13; }
	inline float* get_address_of_maxScale_13() { return &___maxScale_13; }
	inline void set_maxScale_13(float value)
	{
		___maxScale_13 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UGUIHORIZONTALALIGNGROUPSCALEEFFECT_T1380971095_H
#ifndef DICINGIMAGE_T2721298607_H
#define DICINGIMAGE_T2721298607_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.DicingImage
struct  DicingImage_t2721298607  : public MaskableGraphic_t540192618
{
public:
	// Utage.DicingTextures Utage.DicingImage::dicingData
	DicingTextures_t1684855170 * ___dicingData_28;
	// System.String Utage.DicingImage::pattern
	String_t* ___pattern_29;
	// System.String Utage.DicingImage::<MainPattern>k__BackingField
	String_t* ___U3CMainPatternU3Ek__BackingField_30;
	// System.Collections.Generic.Dictionary`2<System.String,System.String> Utage.DicingImage::patternOption
	Dictionary_2_t3943999495 * ___patternOption_31;
	// Utage.DicingTextureData Utage.DicingImage::patternData
	DicingTextureData_t752335795 * ___patternData_32;
	// System.Boolean Utage.DicingImage::skipTransParentCell
	bool ___skipTransParentCell_33;
	// UnityEngine.Rect Utage.DicingImage::uvRect
	Rect_t3681755626  ___uvRect_34;
	// UnityEngine.Texture Utage.DicingImage::m_Texture
	Texture_t2243626319 * ___m_Texture_35;

public:
	inline static int32_t get_offset_of_dicingData_28() { return static_cast<int32_t>(offsetof(DicingImage_t2721298607, ___dicingData_28)); }
	inline DicingTextures_t1684855170 * get_dicingData_28() const { return ___dicingData_28; }
	inline DicingTextures_t1684855170 ** get_address_of_dicingData_28() { return &___dicingData_28; }
	inline void set_dicingData_28(DicingTextures_t1684855170 * value)
	{
		___dicingData_28 = value;
		Il2CppCodeGenWriteBarrier((&___dicingData_28), value);
	}

	inline static int32_t get_offset_of_pattern_29() { return static_cast<int32_t>(offsetof(DicingImage_t2721298607, ___pattern_29)); }
	inline String_t* get_pattern_29() const { return ___pattern_29; }
	inline String_t** get_address_of_pattern_29() { return &___pattern_29; }
	inline void set_pattern_29(String_t* value)
	{
		___pattern_29 = value;
		Il2CppCodeGenWriteBarrier((&___pattern_29), value);
	}

	inline static int32_t get_offset_of_U3CMainPatternU3Ek__BackingField_30() { return static_cast<int32_t>(offsetof(DicingImage_t2721298607, ___U3CMainPatternU3Ek__BackingField_30)); }
	inline String_t* get_U3CMainPatternU3Ek__BackingField_30() const { return ___U3CMainPatternU3Ek__BackingField_30; }
	inline String_t** get_address_of_U3CMainPatternU3Ek__BackingField_30() { return &___U3CMainPatternU3Ek__BackingField_30; }
	inline void set_U3CMainPatternU3Ek__BackingField_30(String_t* value)
	{
		___U3CMainPatternU3Ek__BackingField_30 = value;
		Il2CppCodeGenWriteBarrier((&___U3CMainPatternU3Ek__BackingField_30), value);
	}

	inline static int32_t get_offset_of_patternOption_31() { return static_cast<int32_t>(offsetof(DicingImage_t2721298607, ___patternOption_31)); }
	inline Dictionary_2_t3943999495 * get_patternOption_31() const { return ___patternOption_31; }
	inline Dictionary_2_t3943999495 ** get_address_of_patternOption_31() { return &___patternOption_31; }
	inline void set_patternOption_31(Dictionary_2_t3943999495 * value)
	{
		___patternOption_31 = value;
		Il2CppCodeGenWriteBarrier((&___patternOption_31), value);
	}

	inline static int32_t get_offset_of_patternData_32() { return static_cast<int32_t>(offsetof(DicingImage_t2721298607, ___patternData_32)); }
	inline DicingTextureData_t752335795 * get_patternData_32() const { return ___patternData_32; }
	inline DicingTextureData_t752335795 ** get_address_of_patternData_32() { return &___patternData_32; }
	inline void set_patternData_32(DicingTextureData_t752335795 * value)
	{
		___patternData_32 = value;
		Il2CppCodeGenWriteBarrier((&___patternData_32), value);
	}

	inline static int32_t get_offset_of_skipTransParentCell_33() { return static_cast<int32_t>(offsetof(DicingImage_t2721298607, ___skipTransParentCell_33)); }
	inline bool get_skipTransParentCell_33() const { return ___skipTransParentCell_33; }
	inline bool* get_address_of_skipTransParentCell_33() { return &___skipTransParentCell_33; }
	inline void set_skipTransParentCell_33(bool value)
	{
		___skipTransParentCell_33 = value;
	}

	inline static int32_t get_offset_of_uvRect_34() { return static_cast<int32_t>(offsetof(DicingImage_t2721298607, ___uvRect_34)); }
	inline Rect_t3681755626  get_uvRect_34() const { return ___uvRect_34; }
	inline Rect_t3681755626 * get_address_of_uvRect_34() { return &___uvRect_34; }
	inline void set_uvRect_34(Rect_t3681755626  value)
	{
		___uvRect_34 = value;
	}

	inline static int32_t get_offset_of_m_Texture_35() { return static_cast<int32_t>(offsetof(DicingImage_t2721298607, ___m_Texture_35)); }
	inline Texture_t2243626319 * get_m_Texture_35() const { return ___m_Texture_35; }
	inline Texture_t2243626319 ** get_address_of_m_Texture_35() { return &___m_Texture_35; }
	inline void set_m_Texture_35(Texture_t2243626319 * value)
	{
		___m_Texture_35 = value;
		Il2CppCodeGenWriteBarrier((&___m_Texture_35), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DICINGIMAGE_T2721298607_H
#ifndef TEXT_T356221433_H
#define TEXT_T356221433_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Text
struct  Text_t356221433  : public MaskableGraphic_t540192618
{
public:
	// UnityEngine.UI.FontData UnityEngine.UI.Text::m_FontData
	FontData_t2614388407 * ___m_FontData_28;
	// System.String UnityEngine.UI.Text::m_Text
	String_t* ___m_Text_29;
	// UnityEngine.TextGenerator UnityEngine.UI.Text::m_TextCache
	TextGenerator_t647235000 * ___m_TextCache_30;
	// UnityEngine.TextGenerator UnityEngine.UI.Text::m_TextCacheForLayout
	TextGenerator_t647235000 * ___m_TextCacheForLayout_31;
	// System.Boolean UnityEngine.UI.Text::m_DisableFontTextureRebuiltCallback
	bool ___m_DisableFontTextureRebuiltCallback_33;
	// UnityEngine.UIVertex[] UnityEngine.UI.Text::m_TempVerts
	UIVertexU5BU5D_t3048644023* ___m_TempVerts_34;

public:
	inline static int32_t get_offset_of_m_FontData_28() { return static_cast<int32_t>(offsetof(Text_t356221433, ___m_FontData_28)); }
	inline FontData_t2614388407 * get_m_FontData_28() const { return ___m_FontData_28; }
	inline FontData_t2614388407 ** get_address_of_m_FontData_28() { return &___m_FontData_28; }
	inline void set_m_FontData_28(FontData_t2614388407 * value)
	{
		___m_FontData_28 = value;
		Il2CppCodeGenWriteBarrier((&___m_FontData_28), value);
	}

	inline static int32_t get_offset_of_m_Text_29() { return static_cast<int32_t>(offsetof(Text_t356221433, ___m_Text_29)); }
	inline String_t* get_m_Text_29() const { return ___m_Text_29; }
	inline String_t** get_address_of_m_Text_29() { return &___m_Text_29; }
	inline void set_m_Text_29(String_t* value)
	{
		___m_Text_29 = value;
		Il2CppCodeGenWriteBarrier((&___m_Text_29), value);
	}

	inline static int32_t get_offset_of_m_TextCache_30() { return static_cast<int32_t>(offsetof(Text_t356221433, ___m_TextCache_30)); }
	inline TextGenerator_t647235000 * get_m_TextCache_30() const { return ___m_TextCache_30; }
	inline TextGenerator_t647235000 ** get_address_of_m_TextCache_30() { return &___m_TextCache_30; }
	inline void set_m_TextCache_30(TextGenerator_t647235000 * value)
	{
		___m_TextCache_30 = value;
		Il2CppCodeGenWriteBarrier((&___m_TextCache_30), value);
	}

	inline static int32_t get_offset_of_m_TextCacheForLayout_31() { return static_cast<int32_t>(offsetof(Text_t356221433, ___m_TextCacheForLayout_31)); }
	inline TextGenerator_t647235000 * get_m_TextCacheForLayout_31() const { return ___m_TextCacheForLayout_31; }
	inline TextGenerator_t647235000 ** get_address_of_m_TextCacheForLayout_31() { return &___m_TextCacheForLayout_31; }
	inline void set_m_TextCacheForLayout_31(TextGenerator_t647235000 * value)
	{
		___m_TextCacheForLayout_31 = value;
		Il2CppCodeGenWriteBarrier((&___m_TextCacheForLayout_31), value);
	}

	inline static int32_t get_offset_of_m_DisableFontTextureRebuiltCallback_33() { return static_cast<int32_t>(offsetof(Text_t356221433, ___m_DisableFontTextureRebuiltCallback_33)); }
	inline bool get_m_DisableFontTextureRebuiltCallback_33() const { return ___m_DisableFontTextureRebuiltCallback_33; }
	inline bool* get_address_of_m_DisableFontTextureRebuiltCallback_33() { return &___m_DisableFontTextureRebuiltCallback_33; }
	inline void set_m_DisableFontTextureRebuiltCallback_33(bool value)
	{
		___m_DisableFontTextureRebuiltCallback_33 = value;
	}

	inline static int32_t get_offset_of_m_TempVerts_34() { return static_cast<int32_t>(offsetof(Text_t356221433, ___m_TempVerts_34)); }
	inline UIVertexU5BU5D_t3048644023* get_m_TempVerts_34() const { return ___m_TempVerts_34; }
	inline UIVertexU5BU5D_t3048644023** get_address_of_m_TempVerts_34() { return &___m_TempVerts_34; }
	inline void set_m_TempVerts_34(UIVertexU5BU5D_t3048644023* value)
	{
		___m_TempVerts_34 = value;
		Il2CppCodeGenWriteBarrier((&___m_TempVerts_34), value);
	}
};

struct Text_t356221433_StaticFields
{
public:
	// UnityEngine.Material UnityEngine.UI.Text::s_DefaultText
	Material_t193706927 * ___s_DefaultText_32;

public:
	inline static int32_t get_offset_of_s_DefaultText_32() { return static_cast<int32_t>(offsetof(Text_t356221433_StaticFields, ___s_DefaultText_32)); }
	inline Material_t193706927 * get_s_DefaultText_32() const { return ___s_DefaultText_32; }
	inline Material_t193706927 ** get_address_of_s_DefaultText_32() { return &___s_DefaultText_32; }
	inline void set_s_DefaultText_32(Material_t193706927 * value)
	{
		___s_DefaultText_32 = value;
		Il2CppCodeGenWriteBarrier((&___s_DefaultText_32), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TEXT_T356221433_H
#ifndef UGUIVERTICALALIGNGROUPSCALEEFFECT_T2606759091_H
#define UGUIVERTICALALIGNGROUPSCALEEFFECT_T2606759091_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.UguiVerticalAlignGroupScaleEffect
struct  UguiVerticalAlignGroupScaleEffect_t2606759091  : public UguiVerticalAlignGroup_t1024558358
{
public:
	// System.Single Utage.UguiVerticalAlignGroupScaleEffect::scaleRangeTop
	float ___scaleRangeTop_9;
	// System.Single Utage.UguiVerticalAlignGroupScaleEffect::scaleRangeHeight
	float ___scaleRangeHeight_10;
	// System.Boolean Utage.UguiVerticalAlignGroupScaleEffect::ignoreLocalPositionToScaleEffectRage
	bool ___ignoreLocalPositionToScaleEffectRage_11;
	// System.Single Utage.UguiVerticalAlignGroupScaleEffect::minScale
	float ___minScale_12;
	// System.Single Utage.UguiVerticalAlignGroupScaleEffect::maxScale
	float ___maxScale_13;

public:
	inline static int32_t get_offset_of_scaleRangeTop_9() { return static_cast<int32_t>(offsetof(UguiVerticalAlignGroupScaleEffect_t2606759091, ___scaleRangeTop_9)); }
	inline float get_scaleRangeTop_9() const { return ___scaleRangeTop_9; }
	inline float* get_address_of_scaleRangeTop_9() { return &___scaleRangeTop_9; }
	inline void set_scaleRangeTop_9(float value)
	{
		___scaleRangeTop_9 = value;
	}

	inline static int32_t get_offset_of_scaleRangeHeight_10() { return static_cast<int32_t>(offsetof(UguiVerticalAlignGroupScaleEffect_t2606759091, ___scaleRangeHeight_10)); }
	inline float get_scaleRangeHeight_10() const { return ___scaleRangeHeight_10; }
	inline float* get_address_of_scaleRangeHeight_10() { return &___scaleRangeHeight_10; }
	inline void set_scaleRangeHeight_10(float value)
	{
		___scaleRangeHeight_10 = value;
	}

	inline static int32_t get_offset_of_ignoreLocalPositionToScaleEffectRage_11() { return static_cast<int32_t>(offsetof(UguiVerticalAlignGroupScaleEffect_t2606759091, ___ignoreLocalPositionToScaleEffectRage_11)); }
	inline bool get_ignoreLocalPositionToScaleEffectRage_11() const { return ___ignoreLocalPositionToScaleEffectRage_11; }
	inline bool* get_address_of_ignoreLocalPositionToScaleEffectRage_11() { return &___ignoreLocalPositionToScaleEffectRage_11; }
	inline void set_ignoreLocalPositionToScaleEffectRage_11(bool value)
	{
		___ignoreLocalPositionToScaleEffectRage_11 = value;
	}

	inline static int32_t get_offset_of_minScale_12() { return static_cast<int32_t>(offsetof(UguiVerticalAlignGroupScaleEffect_t2606759091, ___minScale_12)); }
	inline float get_minScale_12() const { return ___minScale_12; }
	inline float* get_address_of_minScale_12() { return &___minScale_12; }
	inline void set_minScale_12(float value)
	{
		___minScale_12 = value;
	}

	inline static int32_t get_offset_of_maxScale_13() { return static_cast<int32_t>(offsetof(UguiVerticalAlignGroupScaleEffect_t2606759091, ___maxScale_13)); }
	inline float get_maxScale_13() const { return ___maxScale_13; }
	inline float* get_address_of_maxScale_13() { return &___maxScale_13; }
	inline void set_maxScale_13(float value)
	{
		___maxScale_13 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UGUIVERTICALALIGNGROUPSCALEEFFECT_T2606759091_H
#ifndef UGUINOVELTEXTBRPAGEICON_T2870639791_H
#define UGUINOVELTEXTBRPAGEICON_T2870639791_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.UguiNovelTextBrPageIcon
struct  UguiNovelTextBrPageIcon_t2870639791  : public Text_t356221433
{
public:
	// Utage.UguiNovelText Utage.UguiNovelTextBrPageIcon::novelText
	UguiNovelText_t4135744055 * ___novelText_35;

public:
	inline static int32_t get_offset_of_novelText_35() { return static_cast<int32_t>(offsetof(UguiNovelTextBrPageIcon_t2870639791, ___novelText_35)); }
	inline UguiNovelText_t4135744055 * get_novelText_35() const { return ___novelText_35; }
	inline UguiNovelText_t4135744055 ** get_address_of_novelText_35() { return &___novelText_35; }
	inline void set_novelText_35(UguiNovelText_t4135744055 * value)
	{
		___novelText_35 = value;
		Il2CppCodeGenWriteBarrier((&___novelText_35), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UGUINOVELTEXTBRPAGEICON_T2870639791_H
#ifndef UGUINOVELTEXT_T4135744055_H
#define UGUINOVELTEXT_T4135744055_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.UguiNovelText
struct  UguiNovelText_t4135744055  : public Text_t356221433
{
public:
	// Utage.UguiNovelTextGenerator Utage.UguiNovelText::textGenerator
	UguiNovelTextGenerator_t4216084310 * ___textGenerator_35;
	// UnityEngine.UIVertex[] Utage.UguiNovelText::m_TempVerts
	UIVertexU5BU5D_t3048644023* ___m_TempVerts_36;
	// System.Boolean Utage.UguiNovelText::isDirtyVerts
	bool ___isDirtyVerts_37;

public:
	inline static int32_t get_offset_of_textGenerator_35() { return static_cast<int32_t>(offsetof(UguiNovelText_t4135744055, ___textGenerator_35)); }
	inline UguiNovelTextGenerator_t4216084310 * get_textGenerator_35() const { return ___textGenerator_35; }
	inline UguiNovelTextGenerator_t4216084310 ** get_address_of_textGenerator_35() { return &___textGenerator_35; }
	inline void set_textGenerator_35(UguiNovelTextGenerator_t4216084310 * value)
	{
		___textGenerator_35 = value;
		Il2CppCodeGenWriteBarrier((&___textGenerator_35), value);
	}

	inline static int32_t get_offset_of_m_TempVerts_36() { return static_cast<int32_t>(offsetof(UguiNovelText_t4135744055, ___m_TempVerts_36)); }
	inline UIVertexU5BU5D_t3048644023* get_m_TempVerts_36() const { return ___m_TempVerts_36; }
	inline UIVertexU5BU5D_t3048644023** get_address_of_m_TempVerts_36() { return &___m_TempVerts_36; }
	inline void set_m_TempVerts_36(UIVertexU5BU5D_t3048644023* value)
	{
		___m_TempVerts_36 = value;
		Il2CppCodeGenWriteBarrier((&___m_TempVerts_36), value);
	}

	inline static int32_t get_offset_of_isDirtyVerts_37() { return static_cast<int32_t>(offsetof(UguiNovelText_t4135744055, ___isDirtyVerts_37)); }
	inline bool get_isDirtyVerts_37() const { return ___isDirtyVerts_37; }
	inline bool* get_address_of_isDirtyVerts_37() { return &___isDirtyVerts_37; }
	inline void set_isDirtyVerts_37(bool value)
	{
		___isDirtyVerts_37 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UGUINOVELTEXT_T4135744055_H





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2800 = { sizeof (LipSynchAvatar_t872933325), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2800[1] = 
{
	LipSynchAvatar_t872933325::get_offset_of_avator_17(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2801 = { sizeof (U3CCoUpdateLipSyncU3Ec__Iterator0_t1939142126), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2801[7] = 
{
	U3CCoUpdateLipSyncU3Ec__Iterator0_t1939142126::get_offset_of_U3CpatternU3E__1_0(),
	U3CCoUpdateLipSyncU3Ec__Iterator0_t1939142126::get_offset_of_U24locvar0_1(),
	U3CCoUpdateLipSyncU3Ec__Iterator0_t1939142126::get_offset_of_U3CdataU3E__2_2(),
	U3CCoUpdateLipSyncU3Ec__Iterator0_t1939142126::get_offset_of_U24this_3(),
	U3CCoUpdateLipSyncU3Ec__Iterator0_t1939142126::get_offset_of_U24current_4(),
	U3CCoUpdateLipSyncU3Ec__Iterator0_t1939142126::get_offset_of_U24disposing_5(),
	U3CCoUpdateLipSyncU3Ec__Iterator0_t1939142126::get_offset_of_U24PC_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2802 = { sizeof (DicingAnimation_t3770383148), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2802[5] = 
{
	DicingAnimation_t3770383148::get_offset_of_playOnAwake_2(),
	DicingAnimation_t3770383148::get_offset_of_wrapMode_3(),
	DicingAnimation_t3770383148::get_offset_of_reverse_4(),
	DicingAnimation_t3770383148::get_offset_of_frameRate_5(),
	DicingAnimation_t3770383148::get_offset_of_dicing_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2803 = { sizeof (U3CCoPlayU3Ec__Iterator0_t563534176), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2803[7] = 
{
	U3CCoPlayU3Ec__Iterator0_t563534176::get_offset_of_U3ClistU3E__0_0(),
	U3CCoPlayU3Ec__Iterator0_t563534176::get_offset_of_U3CisEndU3E__1_1(),
	U3CCoPlayU3Ec__Iterator0_t563534176::get_offset_of_onComplete_2(),
	U3CCoPlayU3Ec__Iterator0_t563534176::get_offset_of_U24this_3(),
	U3CCoPlayU3Ec__Iterator0_t563534176::get_offset_of_U24current_4(),
	U3CCoPlayU3Ec__Iterator0_t563534176::get_offset_of_U24disposing_5(),
	U3CCoPlayU3Ec__Iterator0_t563534176::get_offset_of_U24PC_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2804 = { sizeof (U3CCoPlayOnceU3Ec__Iterator1_t201519776), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2804[7] = 
{
	U3CCoPlayOnceU3Ec__Iterator1_t201519776::get_offset_of_patternList_0(),
	U3CCoPlayOnceU3Ec__Iterator1_t201519776::get_offset_of_U24locvar0_1(),
	U3CCoPlayOnceU3Ec__Iterator1_t201519776::get_offset_of_U3CpatternU3E__1_2(),
	U3CCoPlayOnceU3Ec__Iterator1_t201519776::get_offset_of_U24this_3(),
	U3CCoPlayOnceU3Ec__Iterator1_t201519776::get_offset_of_U24current_4(),
	U3CCoPlayOnceU3Ec__Iterator1_t201519776::get_offset_of_U24disposing_5(),
	U3CCoPlayOnceU3Ec__Iterator1_t201519776::get_offset_of_U24PC_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2805 = { sizeof (UIQuad_t2460659847), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2805[2] = 
{
	UIQuad_t2460659847::get_offset_of_v_0(),
	UIQuad_t2460659847::get_offset_of_uv_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2806 = { sizeof (DicingImage_t2721298607), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2806[8] = 
{
	DicingImage_t2721298607::get_offset_of_dicingData_28(),
	DicingImage_t2721298607::get_offset_of_pattern_29(),
	DicingImage_t2721298607::get_offset_of_U3CMainPatternU3Ek__BackingField_30(),
	DicingImage_t2721298607::get_offset_of_patternOption_31(),
	DicingImage_t2721298607::get_offset_of_patternData_32(),
	DicingImage_t2721298607::get_offset_of_skipTransParentCell_33(),
	DicingImage_t2721298607::get_offset_of_uvRect_34(),
	DicingImage_t2721298607::get_offset_of_m_Texture_35(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2807 = { sizeof (U3COnPopulateMeshU3Ec__AnonStorey0_t1522063813), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2807[3] = 
{
	U3COnPopulateMeshU3Ec__AnonStorey0_t1522063813::get_offset_of_vh_0(),
	U3COnPopulateMeshU3Ec__AnonStorey0_t1522063813::get_offset_of_color32_1(),
	U3COnPopulateMeshU3Ec__AnonStorey0_t1522063813::get_offset_of_index_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2808 = { sizeof (U3CHitTestU3Ec__AnonStorey1_t3427657523), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2808[2] = 
{
	U3CHitTestU3Ec__AnonStorey1_t3427657523::get_offset_of_localPosition_0(),
	U3CHitTestU3Ec__AnonStorey1_t3427657523::get_offset_of_isHit_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2809 = { sizeof (DicingTextureData_t752335795), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2809[7] = 
{
	DicingTextureData_t752335795::get_offset_of_name_0(),
	DicingTextureData_t752335795::get_offset_of_atlasName_1(),
	DicingTextureData_t752335795::get_offset_of_width_2(),
	DicingTextureData_t752335795::get_offset_of_height_3(),
	DicingTextureData_t752335795::get_offset_of_cellIndexList_4(),
	DicingTextureData_t752335795::get_offset_of_transparentIndex_5(),
	DicingTextureData_t752335795::get_offset_of_verts_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2810 = { sizeof (QuadVerts_t459249791), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2810[3] = 
{
	QuadVerts_t459249791::get_offset_of_v_0(),
	QuadVerts_t459249791::get_offset_of_uvRect_1(),
	QuadVerts_t459249791::get_offset_of_isAllTransparent_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2811 = { sizeof (U3CForeachVertexListU3Ec__AnonStorey0_t1404970979), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2811[3] = 
{
	U3CForeachVertexListU3Ec__AnonStorey0_t1404970979::get_offset_of_scale_0(),
	U3CForeachVertexListU3Ec__AnonStorey0_t1404970979::get_offset_of_position_1(),
	U3CForeachVertexListU3Ec__AnonStorey0_t1404970979::get_offset_of_function_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2812 = { sizeof (U3CForeachVertexListU3Ec__AnonStorey1_t1404970980), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2812[8] = 
{
	U3CForeachVertexListU3Ec__AnonStorey1_t1404970980::get_offset_of_scaleX_0(),
	U3CForeachVertexListU3Ec__AnonStorey1_t1404970980::get_offset_of_rect_1(),
	U3CForeachVertexListU3Ec__AnonStorey1_t1404970980::get_offset_of_fipOffsetX_2(),
	U3CForeachVertexListU3Ec__AnonStorey1_t1404970980::get_offset_of_scaleY_3(),
	U3CForeachVertexListU3Ec__AnonStorey1_t1404970980::get_offset_of_offsetY_4(),
	U3CForeachVertexListU3Ec__AnonStorey1_t1404970980::get_offset_of_fipOffsetY_5(),
	U3CForeachVertexListU3Ec__AnonStorey1_t1404970980::get_offset_of_function_6(),
	U3CForeachVertexListU3Ec__AnonStorey1_t1404970980::get_offset_of_U24this_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2813 = { sizeof (U3CForeachVertexListU3Ec__AnonStorey2_t1404970981), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2813[2] = 
{
	U3CForeachVertexListU3Ec__AnonStorey2_t1404970981::get_offset_of_offsetX_0(),
	U3CForeachVertexListU3Ec__AnonStorey2_t1404970981::get_offset_of_U3CU3Ef__refU241_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2814 = { sizeof (DicingTextures_t1684855170), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2814[4] = 
{
	DicingTextures_t1684855170::get_offset_of_cellSize_2(),
	DicingTextures_t1684855170::get_offset_of_padding_3(),
	DicingTextures_t1684855170::get_offset_of_atlasTextures_4(),
	DicingTextures_t1684855170::get_offset_of_textureDataList_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2815 = { sizeof (U3CExistsU3Ec__AnonStorey0_t771376738), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2815[1] = 
{
	U3CExistsU3Ec__AnonStorey0_t771376738::get_offset_of_pattern_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2816 = { sizeof (U3CGetTextureU3Ec__AnonStorey1_t379105648), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2816[1] = 
{
	U3CGetTextureU3Ec__AnonStorey1_t379105648::get_offset_of_name_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2817 = { sizeof (EyeBlinkDicing_t891209267), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2817[1] = 
{
	EyeBlinkDicing_t891209267::get_offset_of_dicing_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2818 = { sizeof (U3CCoEyeBlinkU3Ec__Iterator0_t2219955804), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2818[7] = 
{
	U3CCoEyeBlinkU3Ec__Iterator0_t2219955804::get_offset_of_U24locvar0_0(),
	U3CCoEyeBlinkU3Ec__Iterator0_t2219955804::get_offset_of_U3CdataU3E__1_1(),
	U3CCoEyeBlinkU3Ec__Iterator0_t2219955804::get_offset_of_onComplete_2(),
	U3CCoEyeBlinkU3Ec__Iterator0_t2219955804::get_offset_of_U24this_3(),
	U3CCoEyeBlinkU3Ec__Iterator0_t2219955804::get_offset_of_U24current_4(),
	U3CCoEyeBlinkU3Ec__Iterator0_t2219955804::get_offset_of_U24disposing_5(),
	U3CCoEyeBlinkU3Ec__Iterator0_t2219955804::get_offset_of_U24PC_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2819 = { sizeof (LipSynchDicing_t1869722688), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2819[1] = 
{
	LipSynchDicing_t1869722688::get_offset_of_dicing_17(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2820 = { sizeof (U3CCoUpdateLipSyncU3Ec__Iterator0_t137802369), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2820[7] = 
{
	U3CCoUpdateLipSyncU3Ec__Iterator0_t137802369::get_offset_of_U3CpatternU3E__1_0(),
	U3CCoUpdateLipSyncU3Ec__Iterator0_t137802369::get_offset_of_U24locvar0_1(),
	U3CCoUpdateLipSyncU3Ec__Iterator0_t137802369::get_offset_of_U3CdataU3E__2_2(),
	U3CCoUpdateLipSyncU3Ec__Iterator0_t137802369::get_offset_of_U24this_3(),
	U3CCoUpdateLipSyncU3Ec__Iterator0_t137802369::get_offset_of_U24current_4(),
	U3CCoUpdateLipSyncU3Ec__Iterator0_t137802369::get_offset_of_U24disposing_5(),
	U3CCoUpdateLipSyncU3Ec__Iterator0_t137802369::get_offset_of_U24PC_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2821 = { sizeof (EyeBlinkBase_t1662968566), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2821[5] = 
{
	EyeBlinkBase_t1662968566::get_offset_of_intervalTime_2(),
	EyeBlinkBase_t1662968566::get_offset_of_randomDoubleEyeBlink_3(),
	EyeBlinkBase_t1662968566::get_offset_of_intervalDoubleEyeBlink_4(),
	EyeBlinkBase_t1662968566::get_offset_of_eyeTag_5(),
	EyeBlinkBase_t1662968566::get_offset_of_animationData_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2822 = { sizeof (U3CCoUpateWaitingU3Ec__Iterator0_t2118272722), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2822[5] = 
{
	U3CCoUpateWaitingU3Ec__Iterator0_t2118272722::get_offset_of_waitTime_0(),
	U3CCoUpateWaitingU3Ec__Iterator0_t2118272722::get_offset_of_U24this_1(),
	U3CCoUpateWaitingU3Ec__Iterator0_t2118272722::get_offset_of_U24current_2(),
	U3CCoUpateWaitingU3Ec__Iterator0_t2118272722::get_offset_of_U24disposing_3(),
	U3CCoUpateWaitingU3Ec__Iterator0_t2118272722::get_offset_of_U24PC_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2823 = { sizeof (U3CCoDoubleEyeBlinkU3Ec__Iterator1_t2312577335), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2823[4] = 
{
	U3CCoDoubleEyeBlinkU3Ec__Iterator1_t2312577335::get_offset_of_U24this_0(),
	U3CCoDoubleEyeBlinkU3Ec__Iterator1_t2312577335::get_offset_of_U24current_1(),
	U3CCoDoubleEyeBlinkU3Ec__Iterator1_t2312577335::get_offset_of_U24disposing_2(),
	U3CCoDoubleEyeBlinkU3Ec__Iterator1_t2312577335::get_offset_of_U24PC_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2824 = { sizeof (LipSynch2d_t3152759062), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2824[8] = 
{
	LipSynch2d_t3152759062::get_offset_of_duration_9(),
	LipSynch2d_t3152759062::get_offset_of_interval_10(),
	LipSynch2d_t3152759062::get_offset_of_scaleVoiceVolume_11(),
	LipSynch2d_t3152759062::get_offset_of_lipTag_12(),
	LipSynch2d_t3152759062::get_offset_of_animationData_13(),
	LipSynch2d_t3152759062::get_offset_of_U3CLipSyncVolumeU3Ek__BackingField_14(),
	LipSynch2d_t3152759062::get_offset_of_target_15(),
	LipSynch2d_t3152759062::get_offset_of_coLypSync_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2825 = { sizeof (LipSynchType_t1469092900)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2825[4] = 
{
	LipSynchType_t1469092900::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2826 = { sizeof (LipSynchMode_t1462187095)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2826[3] = 
{
	LipSynchMode_t1462187095::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2827 = { sizeof (LipSynchEvent_t825925384), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2828 = { sizeof (LipSynchBase_t2376160345), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2828[7] = 
{
	LipSynchBase_t2376160345::get_offset_of_type_2(),
	LipSynchBase_t2376160345::get_offset_of_U3CEnableTextLipSyncU3Ek__BackingField_3(),
	LipSynchBase_t2376160345::get_offset_of_U3CLipSynchModeU3Ek__BackingField_4(),
	LipSynchBase_t2376160345::get_offset_of_OnCheckTextLipSync_5(),
	LipSynchBase_t2376160345::get_offset_of_characterLabel_6(),
	LipSynchBase_t2376160345::get_offset_of_U3CIsEnableU3Ek__BackingField_7(),
	LipSynchBase_t2376160345::get_offset_of_U3CIsPlayingU3Ek__BackingField_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2829 = { sizeof (UguiAlignGroup_t4030345508), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2829[2] = 
{
	UguiAlignGroup_t4030345508::get_offset_of_isAutoResize_4(),
	UguiAlignGroup_t4030345508::get_offset_of_space_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2830 = { sizeof (UguiHorizontalAlignGroup_t3654853588), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2830[3] = 
{
	UguiHorizontalAlignGroup_t3654853588::get_offset_of_paddingLeft_6(),
	UguiHorizontalAlignGroup_t3654853588::get_offset_of_paddingRight_7(),
	UguiHorizontalAlignGroup_t3654853588::get_offset_of_direction_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2831 = { sizeof (AlignDirection_t4010138607)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2831[3] = 
{
	AlignDirection_t4010138607::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2832 = { sizeof (UguiHorizontalAlignGroupScaleEffect_t1380971095), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2832[5] = 
{
	UguiHorizontalAlignGroupScaleEffect_t1380971095::get_offset_of_scaleRangeLeft_9(),
	UguiHorizontalAlignGroupScaleEffect_t1380971095::get_offset_of_scaleRangeWidth_10(),
	UguiHorizontalAlignGroupScaleEffect_t1380971095::get_offset_of_ignoreLocalPositionToScaleEffectRage_11(),
	UguiHorizontalAlignGroupScaleEffect_t1380971095::get_offset_of_minScale_12(),
	UguiHorizontalAlignGroupScaleEffect_t1380971095::get_offset_of_maxScale_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2833 = { sizeof (UguiVerticalAlignGroup_t1024558358), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2833[3] = 
{
	UguiVerticalAlignGroup_t1024558358::get_offset_of_paddingTop_6(),
	UguiVerticalAlignGroup_t1024558358::get_offset_of_paddingBottom_7(),
	UguiVerticalAlignGroup_t1024558358::get_offset_of_direction_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2834 = { sizeof (AlignDirection_t1523984715)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2834[3] = 
{
	AlignDirection_t1523984715::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2835 = { sizeof (UguiVerticalAlignGroupScaleEffect_t2606759091), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2835[5] = 
{
	UguiVerticalAlignGroupScaleEffect_t2606759091::get_offset_of_scaleRangeTop_9(),
	UguiVerticalAlignGroupScaleEffect_t2606759091::get_offset_of_scaleRangeHeight_10(),
	UguiVerticalAlignGroupScaleEffect_t2606759091::get_offset_of_ignoreLocalPositionToScaleEffectRage_11(),
	UguiVerticalAlignGroupScaleEffect_t2606759091::get_offset_of_minScale_12(),
	UguiVerticalAlignGroupScaleEffect_t2606759091::get_offset_of_maxScale_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2836 = { sizeof (UguiLayoutControllerBase_t1432848723), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2836[2] = 
{
	UguiLayoutControllerBase_t1432848723::get_offset_of_cachedRectTransform_2(),
	UguiLayoutControllerBase_t1432848723::get_offset_of_tracker_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2837 = { sizeof (UguiLetterBoxCanvasScaler_t1623094149), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2837[2] = 
{
	UguiLetterBoxCanvasScaler_t1623094149::get_offset_of_canvas_4(),
	UguiLetterBoxCanvasScaler_t1623094149::get_offset_of_letterBoxCamera_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2838 = { sizeof (UguiSizeFitter_t3082290857), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2838[1] = 
{
	UguiSizeFitter_t3082290857::get_offset_of_target_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2839 = { sizeof (UguiLocalizeBase_t685439682), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2839[2] = 
{
	UguiLocalizeBase_t685439682::get_offset_of_currentLanguage_2(),
	UguiLocalizeBase_t685439682::get_offset_of_isInit_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2840 = { sizeof (UguiLocalizeRectTransform_t2564129487), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2840[4] = 
{
	UguiLocalizeRectTransform_t2564129487::get_offset_of_settingList_4(),
	UguiLocalizeRectTransform_t2564129487::get_offset_of_defaultSetting_5(),
	UguiLocalizeRectTransform_t2564129487::get_offset_of_cachedRectTransform_6(),
	UguiLocalizeRectTransform_t2564129487::get_offset_of_U3CHasChangedU3Ek__BackingField_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2841 = { sizeof (Setting_t2370336044), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2841[3] = 
{
	Setting_t2370336044::get_offset_of_language_0(),
	Setting_t2370336044::get_offset_of_anchoredPosition_1(),
	Setting_t2370336044::get_offset_of_size_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2842 = { sizeof (UguiLocalizeTextSetting_t2621520384), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2842[3] = 
{
	UguiLocalizeTextSetting_t2621520384::get_offset_of_settingList_4(),
	UguiLocalizeTextSetting_t2621520384::get_offset_of_defaultSetting_5(),
	UguiLocalizeTextSetting_t2621520384::get_offset_of_cachedText_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2843 = { sizeof (Setting_t3544201405), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2843[4] = 
{
	Setting_t3544201405::get_offset_of_language_0(),
	Setting_t3544201405::get_offset_of_font_1(),
	Setting_t3544201405::get_offset_of_fontSize_2(),
	Setting_t3544201405::get_offset_of_lineSpacing_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2844 = { sizeof (UguiNovelText_t4135744055), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2844[3] = 
{
	UguiNovelText_t4135744055::get_offset_of_textGenerator_35(),
	UguiNovelText_t4135744055::get_offset_of_m_TempVerts_36(),
	UguiNovelText_t4135744055::get_offset_of_isDirtyVerts_37(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2845 = { sizeof (UguiNovelTextBrPageIcon_t2870639791), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2845[1] = 
{
	UguiNovelTextBrPageIcon_t2870639791::get_offset_of_novelText_35(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2846 = { sizeof (UguiNovelTextCharacter_t1244198124), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2846[20] = 
{
	UguiNovelTextCharacter_t1244198124::get_offset_of_isAutoLineBreak_0(),
	UguiNovelTextCharacter_t1244198124::get_offset_of_isKinsokuBurasage_1(),
	UguiNovelTextCharacter_t1244198124::get_offset_of_charData_2(),
	UguiNovelTextCharacter_t1244198124::get_offset_of_charInfo_3(),
	UguiNovelTextCharacter_t1244198124::get_offset_of_width_4(),
	UguiNovelTextCharacter_t1244198124::get_offset_of_fontSize_5(),
	UguiNovelTextCharacter_t1244198124::get_offset_of_defaultFontSize_6(),
	UguiNovelTextCharacter_t1244198124::get_offset_of_fontStyle_7(),
	UguiNovelTextCharacter_t1244198124::get_offset_of_verts_8(),
	UguiNovelTextCharacter_t1244198124::get_offset_of_U3CX0U3Ek__BackingField_9(),
	UguiNovelTextCharacter_t1244198124::get_offset_of_U3CY0U3Ek__BackingField_10(),
	UguiNovelTextCharacter_t1244198124::get_offset_of_U3COffsetXU3Ek__BackingField_11(),
	UguiNovelTextCharacter_t1244198124::get_offset_of_U3COffsetYU3Ek__BackingField_12(),
	UguiNovelTextCharacter_t1244198124::get_offset_of_isError_13(),
	UguiNovelTextCharacter_t1244198124::get_offset_of_U3CCustomSpaceU3Ek__BackingField_14(),
	UguiNovelTextCharacter_t1244198124::get_offset_of_isSprite_15(),
	UguiNovelTextCharacter_t1244198124::get_offset_of_U3CRubySpaceSizeU3Ek__BackingField_16(),
	UguiNovelTextCharacter_t1244198124::get_offset_of_U3CIsVisibleU3Ek__BackingField_17(),
	UguiNovelTextCharacter_t1244198124::get_offset_of_effectColor_18(),
	UguiNovelTextCharacter_t1244198124::get_offset_of_U3CBmpFontScaleU3Ek__BackingField_19(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2847 = { sizeof (UguiNovelTextEmojiData_t3935078235), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2847[4] = 
{
	UguiNovelTextEmojiData_t3935078235::get_offset_of_size_2(),
	UguiNovelTextEmojiData_t3935078235::get_offset_of_spriteTbl_3(),
	UguiNovelTextEmojiData_t3935078235::get_offset_of_spriteDictionary_4(),
	UguiNovelTextEmojiData_t3935078235::get_offset_of_spriteDictionaryStringKey_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2848 = { sizeof (OnClickLinkEvent_t134619553), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2849 = { sizeof (UguiNovelTextEventTrigger_t710914779), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2849[7] = 
{
	UguiNovelTextEventTrigger_t710914779::get_offset_of_generator_2(),
	UguiNovelTextEventTrigger_t710914779::get_offset_of_novelText_3(),
	UguiNovelTextEventTrigger_t710914779::get_offset_of_cachedRectTransform_4(),
	UguiNovelTextEventTrigger_t710914779::get_offset_of_OnClick_5(),
	UguiNovelTextEventTrigger_t710914779::get_offset_of_hoverColor_6(),
	UguiNovelTextEventTrigger_t710914779::get_offset_of_currentTarget_7(),
	UguiNovelTextEventTrigger_t710914779::get_offset_of_isEntered_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2850 = { sizeof (UguiNovelTextGenerator_t4216084310), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2850[30] = 
{
	UguiNovelTextGenerator_t4216084310::get_offset_of_novelText_2(),
	UguiNovelTextGenerator_t4216084310::get_offset_of_textData_3(),
	UguiNovelTextGenerator_t4216084310::get_offset_of_U3CMaxWidthU3Ek__BackingField_4(),
	UguiNovelTextGenerator_t4216084310::get_offset_of_U3CMaxHeightU3Ek__BackingField_5(),
	UguiNovelTextGenerator_t4216084310::get_offset_of_preferredHeight_6(),
	UguiNovelTextGenerator_t4216084310::get_offset_of_preferredWidth_7(),
	UguiNovelTextGenerator_t4216084310::get_offset_of_height_8(),
	UguiNovelTextGenerator_t4216084310::get_offset_of_space_9(),
	UguiNovelTextGenerator_t4216084310::get_offset_of_letterSpaceSize_10(),
	UguiNovelTextGenerator_t4216084310::get_offset_of_wordWrap_11(),
	UguiNovelTextGenerator_t4216084310::get_offset_of_lengthOfView_12(),
	UguiNovelTextGenerator_t4216084310::get_offset_of_textSettings_13(),
	UguiNovelTextGenerator_t4216084310::get_offset_of_rubySizeScale_14(),
	UguiNovelTextGenerator_t4216084310::get_offset_of_supOrSubSizeScale_15(),
	UguiNovelTextGenerator_t4216084310::get_offset_of_emojiData_16(),
	UguiNovelTextGenerator_t4216084310::get_offset_of_dashChar_17(),
	UguiNovelTextGenerator_t4216084310::get_offset_of_bmpFontSize_18(),
	UguiNovelTextGenerator_t4216084310::get_offset_of_isUnicodeFont_19(),
	UguiNovelTextGenerator_t4216084310::get_offset_of_cachedRectTransform_20(),
	UguiNovelTextGenerator_t4216084310::get_offset_of_characterDataList_21(),
	UguiNovelTextGenerator_t4216084310::get_offset_of_lineDataList_22(),
	UguiNovelTextGenerator_t4216084310::get_offset_of_endPosition_23(),
	UguiNovelTextGenerator_t4216084310::get_offset_of_additional_24(),
	UguiNovelTextGenerator_t4216084310::get_offset_of_graphicObjectList_25(),
	UguiNovelTextGenerator_t4216084310::get_offset_of_isInitGraphicObjectList_26(),
	UguiNovelTextGenerator_t4216084310::get_offset_of_hitGroupLists_27(),
	UguiNovelTextGenerator_t4216084310::get_offset_of_isDebugLog_28(),
	UguiNovelTextGenerator_t4216084310::get_offset_of_U3CCurrentChangeTypeU3Ek__BackingField_29(),
	UguiNovelTextGenerator_t4216084310::get_offset_of_U3CIsRebuidFontU3Ek__BackingField_30(),
	UguiNovelTextGenerator_t4216084310::get_offset_of_isRequestingCharactersInTexture_31(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2851 = { sizeof (WordWrap_t2124153)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2851[3] = 
{
	WordWrap_t2124153::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2852 = { sizeof (GraphicObject_t1133457478), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2852[2] = 
{
	GraphicObject_t1133457478::get_offset_of_character_0(),
	GraphicObject_t1133457478::get_offset_of_graphic_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2853 = { sizeof (ChagneType_t192839657)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2853[4] = 
{
	ChagneType_t192839657::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2854 = { sizeof (RequestCharactersInfo_t3077364824), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2854[3] = 
{
	RequestCharactersInfo_t3077364824::get_offset_of_characters_0(),
	RequestCharactersInfo_t3077364824::get_offset_of_size_1(),
	RequestCharactersInfo_t3077364824::get_offset_of_style_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2855 = { sizeof (UguiNovelTextGeneratorAdditional_t151164365), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2855[2] = 
{
	UguiNovelTextGeneratorAdditional_t151164365::get_offset_of_rubyList_0(),
	UguiNovelTextGeneratorAdditional_t151164365::get_offset_of_lineList_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2856 = { sizeof (UguiNovelTextGeneratorAdditionalLine_t1868820539), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2856[4] = 
{
	UguiNovelTextGeneratorAdditionalLine_t1868820539::get_offset_of_type_0(),
	UguiNovelTextGeneratorAdditionalLine_t1868820539::get_offset_of_characteData_1(),
	UguiNovelTextGeneratorAdditionalLine_t1868820539::get_offset_of_stringData_2(),
	UguiNovelTextGeneratorAdditionalLine_t1868820539::get_offset_of_textLine_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2857 = { sizeof (Type_t2522374058)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2857[3] = 
{
	Type_t2522374058::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2858 = { sizeof (UguiNovelTextGeneratorAdditionalRuby_t625726217), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2858[5] = 
{
	UguiNovelTextGeneratorAdditionalRuby_t625726217::get_offset_of_rubyList_0(),
	UguiNovelTextGeneratorAdditionalRuby_t625726217::get_offset_of_stringData_1(),
	UguiNovelTextGeneratorAdditionalRuby_t625726217::get_offset_of_rubyWidth_2(),
	UguiNovelTextGeneratorAdditionalRuby_t625726217::get_offset_of_stringWidth_3(),
	UguiNovelTextGeneratorAdditionalRuby_t625726217::get_offset_of_textLine_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2859 = { sizeof (UguiNovelTextHitArea_t1579411533), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2859[5] = 
{
	UguiNovelTextHitArea_t1579411533::get_offset_of_novelText_0(),
	UguiNovelTextHitArea_t1579411533::get_offset_of_U3CHitEventTypeU3Ek__BackingField_1(),
	UguiNovelTextHitArea_t1579411533::get_offset_of_U3CArgU3Ek__BackingField_2(),
	UguiNovelTextHitArea_t1579411533::get_offset_of_characters_3(),
	UguiNovelTextHitArea_t1579411533::get_offset_of_hitAreaList_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2860 = { sizeof (Type_t520746208)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2860[3] = 
{
	Type_t520746208::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2861 = { sizeof (UguiNovelTextLine_t2439763533), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2861[7] = 
{
	UguiNovelTextLine_t2439763533::get_offset_of_characters_0(),
	UguiNovelTextLine_t2439763533::get_offset_of_rubyCharacters_1(),
	UguiNovelTextLine_t2439763533::get_offset_of_isOver_2(),
	UguiNovelTextLine_t2439763533::get_offset_of_maxFontSize_3(),
	UguiNovelTextLine_t2439763533::get_offset_of_width_4(),
	UguiNovelTextLine_t2439763533::get_offset_of_totalHeight_5(),
	UguiNovelTextLine_t2439763533::get_offset_of_U3CY0U3Ek__BackingField_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2862 = { sizeof (UguiNovelTextSettings_t4201484304), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2862[6] = 
{
	UguiNovelTextSettings_t4201484304::get_offset_of_wordWrapSeparator_2(),
	UguiNovelTextSettings_t4201484304::get_offset_of_kinsokuTop_3(),
	UguiNovelTextSettings_t4201484304::get_offset_of_kinsokuEnd_4(),
	UguiNovelTextSettings_t4201484304::get_offset_of_ignoreLetterSpace_5(),
	UguiNovelTextSettings_t4201484304::get_offset_of_autoIndentation_6(),
	UguiNovelTextSettings_t4201484304::get_offset_of_forceIgonreJapaneseKinsoku_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2863 = { sizeof (UguiBackgroundRaycaster_t588614668), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2863[4] = 
{
	UguiBackgroundRaycaster_t588614668::get_offset_of_cachedCamera_2(),
	UguiBackgroundRaycaster_t588614668::get_offset_of_letterBoxCamera_3(),
	UguiBackgroundRaycaster_t588614668::get_offset_of_m_Priority_4(),
	UguiBackgroundRaycaster_t588614668::get_offset_of_targetObjectList_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2864 = { sizeof (UguiBackgroundRaycastReciever_t733380310), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2864[1] = 
{
	UguiBackgroundRaycastReciever_t733380310::get_offset_of_raycaster_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2865 = { sizeof (UguiButtonSe_t466015712), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2865[5] = 
{
	UguiButtonSe_t466015712::get_offset_of_selectable_2(),
	UguiButtonSe_t466015712::get_offset_of_clicked_3(),
	UguiButtonSe_t466015712::get_offset_of_highlited_4(),
	UguiButtonSe_t466015712::get_offset_of_clickedPlayMode_5(),
	UguiButtonSe_t466015712::get_offset_of_highlitedPlayMode_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2866 = { sizeof (UguiCategoryGridPage_t673822931), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2866[6] = 
{
	UguiCategoryGridPage_t673822931::get_offset_of_gridPage_2(),
	UguiCategoryGridPage_t673822931::get_offset_of_categoryToggleGroup_3(),
	UguiCategoryGridPage_t673822931::get_offset_of_categoryAlignGroup_4(),
	UguiCategoryGridPage_t673822931::get_offset_of_categoryPrefab_5(),
	UguiCategoryGridPage_t673822931::get_offset_of_buttonSpriteList_6(),
	UguiCategoryGridPage_t673822931::get_offset_of_categoryList_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2867 = { sizeof (U3CInitU3Ec__AnonStorey0_t2630862191), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2867[2] = 
{
	U3CInitU3Ec__AnonStorey0_t2630862191::get_offset_of_OpenCurrentCategory_0(),
	U3CInitU3Ec__AnonStorey0_t2630862191::get_offset_of_U24this_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2868 = { sizeof (UguiFadeTextureStream_t4272889985), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2868[5] = 
{
	UguiFadeTextureStream_t4272889985::get_offset_of_allowSkip_2(),
	UguiFadeTextureStream_t4272889985::get_offset_of_allowAllSkip_3(),
	UguiFadeTextureStream_t4272889985::get_offset_of_fadeTextures_4(),
	UguiFadeTextureStream_t4272889985::get_offset_of_isInput_5(),
	UguiFadeTextureStream_t4272889985::get_offset_of_isPlaying_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2869 = { sizeof (FadeTextureInfo_t2015309025), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2869[6] = 
{
	FadeTextureInfo_t2015309025::get_offset_of_texture_0(),
	FadeTextureInfo_t2015309025::get_offset_of_moviePath_1(),
	FadeTextureInfo_t2015309025::get_offset_of_fadeInTime_2(),
	FadeTextureInfo_t2015309025::get_offset_of_duration_3(),
	FadeTextureInfo_t2015309025::get_offset_of_fadeOutTime_4(),
	FadeTextureInfo_t2015309025::get_offset_of_allowSkip_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2870 = { sizeof (U3CCoPlayU3Ec__Iterator0_t252162737), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2870[10] = 
{
	U3CCoPlayU3Ec__Iterator0_t252162737::get_offset_of_U3CrawImageU3E__0_0(),
	U3CCoPlayU3Ec__Iterator0_t252162737::get_offset_of_U24locvar0_1(),
	U3CCoPlayU3Ec__Iterator0_t252162737::get_offset_of_U24locvar1_2(),
	U3CCoPlayU3Ec__Iterator0_t252162737::get_offset_of_U3CinfoU3E__1_3(),
	U3CCoPlayU3Ec__Iterator0_t252162737::get_offset_of_U3CallSkipU3E__2_4(),
	U3CCoPlayU3Ec__Iterator0_t252162737::get_offset_of_U3CtimeU3E__3_5(),
	U3CCoPlayU3Ec__Iterator0_t252162737::get_offset_of_U24this_6(),
	U3CCoPlayU3Ec__Iterator0_t252162737::get_offset_of_U24current_7(),
	U3CCoPlayU3Ec__Iterator0_t252162737::get_offset_of_U24disposing_8(),
	U3CCoPlayU3Ec__Iterator0_t252162737::get_offset_of_U24PC_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2871 = { sizeof (UguiGridPage_t1812803607), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2871[11] = 
{
	UguiGridPage_t1812803607::get_offset_of_grid_2(),
	UguiGridPage_t1812803607::get_offset_of_itemPrefab_3(),
	UguiGridPage_t1812803607::get_offset_of_pageCarouselToggles_4(),
	UguiGridPage_t1812803607::get_offset_of_pageCarouselAlignGroup_5(),
	UguiGridPage_t1812803607::get_offset_of_pageCarouselPrefab_6(),
	UguiGridPage_t1812803607::get_offset_of_cachedRectTransform_7(),
	UguiGridPage_t1812803607::get_offset_of_maxItemPerPage_8(),
	UguiGridPage_t1812803607::get_offset_of_maxItemNum_9(),
	UguiGridPage_t1812803607::get_offset_of_currentPage_10(),
	UguiGridPage_t1812803607::get_offset_of_items_11(),
	UguiGridPage_t1812803607::get_offset_of_CallbackCreateItem_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2872 = { sizeof (UguiIgnoreRaycaster_t1854879086), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2872[1] = 
{
	UguiIgnoreRaycaster_t1854879086::get_offset_of_ignoreRaycaster_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2873 = { sizeof (UguiListView_t169672791), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2873[11] = 
{
	UguiListView_t169672791::get_offset_of_scrollType_2(),
	UguiListView_t169672791::get_offset_of_itemPrefab_3(),
	UguiListView_t169672791::get_offset_of_content_4(),
	UguiListView_t169672791::get_offset_of_isStopScroolWithAllInnner_5(),
	UguiListView_t169672791::get_offset_of_isAutoCenteringOnRepostion_6(),
	UguiListView_t169672791::get_offset_of_positionGroup_7(),
	UguiListView_t169672791::get_offset_of_scrollRect_8(),
	UguiListView_t169672791::get_offset_of_scrollRectTransform_9(),
	UguiListView_t169672791::get_offset_of_minArrow_10(),
	UguiListView_t169672791::get_offset_of_maxArrow_11(),
	UguiListView_t169672791::get_offset_of_items_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2874 = { sizeof (Type_t1945770602)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2874[3] = 
{
	Type_t1945770602::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2875 = { sizeof (UguiLocalize_t1715753735), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2875[3] = 
{
	UguiLocalize_t1715753735::get_offset_of_key_4(),
	UguiLocalize_t1715753735::get_offset_of_defaultText_5(),
	UguiLocalize_t1715753735::get_offset_of_cachedText_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2876 = { sizeof (UguiToggleGroupIndexed_t999166452), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2876[12] = 
{
	UguiToggleGroupIndexed_t999166452::get_offset_of_toggles_2(),
	UguiToggleGroupIndexed_t999166452::get_offset_of_firstIndexOnAwake_3(),
	UguiToggleGroupIndexed_t999166452::get_offset_of_ignoreValueChangeOnAwake_4(),
	UguiToggleGroupIndexed_t999166452::get_offset_of_autoToggleInteractiveOff_5(),
	UguiToggleGroupIndexed_t999166452::get_offset_of_isLoopShift_6(),
	UguiToggleGroupIndexed_t999166452::get_offset_of_shiftLeftButton_7(),
	UguiToggleGroupIndexed_t999166452::get_offset_of_shiftRightButton_8(),
	UguiToggleGroupIndexed_t999166452::get_offset_of_jumpLeftEdgeButton_9(),
	UguiToggleGroupIndexed_t999166452::get_offset_of_jumpRightEdgeButton_10(),
	UguiToggleGroupIndexed_t999166452::get_offset_of_currentIndex_11(),
	UguiToggleGroupIndexed_t999166452::get_offset_of_OnValueChanged_12(),
	UguiToggleGroupIndexed_t999166452::get_offset_of_isIgnoreValueChange_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2877 = { sizeof (UguiTabButtonGroupEvent_t4293552753), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2878 = { sizeof (U3CAwakeU3Ec__AnonStorey0_t3297509173), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2878[2] = 
{
	U3CAwakeU3Ec__AnonStorey0_t3297509173::get_offset_of_toggle_0(),
	U3CAwakeU3Ec__AnonStorey0_t3297509173::get_offset_of_U24this_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2879 = { sizeof (U3COnToggleValueChangedU3Ec__AnonStorey1_t907687639), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2879[1] = 
{
	U3COnToggleValueChangedU3Ec__AnonStorey1_t907687639::get_offset_of_toggle_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2880 = { sizeof (U3CAddU3Ec__AnonStorey2_t2433847059), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2880[2] = 
{
	U3CAddU3Ec__AnonStorey2_t2433847059::get_offset_of_toggle_0(),
	U3CAddU3Ec__AnonStorey2_t2433847059::get_offset_of_U24this_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2881 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2882 = { sizeof (UguiView_t2174908005), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2882[10] = 
{
	UguiView_t2174908005::get_offset_of_prevView_2(),
	UguiView_t2174908005::get_offset_of_bgm_3(),
	UguiView_t2174908005::get_offset_of_isStopBgmIfNoneBgm_4(),
	UguiView_t2174908005::get_offset_of_onOpen_5(),
	UguiView_t2174908005::get_offset_of_onClose_6(),
	UguiView_t2174908005::get_offset_of_canvasGroup_7(),
	UguiView_t2174908005::get_offset_of_status_8(),
	UguiView_t2174908005::get_offset_of_storedCanvasGroupInteractable_9(),
	UguiView_t2174908005::get_offset_of_storedCanvasGroupBlocksRaycasts_10(),
	UguiView_t2174908005::get_offset_of_isStoredCanvasGroupInfo_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2883 = { sizeof (Status_t2577383376)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2883[6] = 
{
	Status_t2577383376::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2884 = { sizeof (U3CCoOpeningU3Ec__Iterator0_t101420245), -1, sizeof(U3CCoOpeningU3Ec__Iterator0_t101420245_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2884[8] = 
{
	U3CCoOpeningU3Ec__Iterator0_t101420245::get_offset_of_U3CtransitionsU3E__0_0(),
	U3CCoOpeningU3Ec__Iterator0_t101420245::get_offset_of_U24locvar0_1(),
	U3CCoOpeningU3Ec__Iterator0_t101420245::get_offset_of_U24locvar1_2(),
	U3CCoOpeningU3Ec__Iterator0_t101420245::get_offset_of_U24this_3(),
	U3CCoOpeningU3Ec__Iterator0_t101420245::get_offset_of_U24current_4(),
	U3CCoOpeningU3Ec__Iterator0_t101420245::get_offset_of_U24disposing_5(),
	U3CCoOpeningU3Ec__Iterator0_t101420245::get_offset_of_U24PC_6(),
	U3CCoOpeningU3Ec__Iterator0_t101420245_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2885 = { sizeof (U3CCoClosingU3Ec__Iterator1_t587244119), -1, sizeof(U3CCoClosingU3Ec__Iterator1_t587244119_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2885[8] = 
{
	U3CCoClosingU3Ec__Iterator1_t587244119::get_offset_of_U3CtransitionsU3E__0_0(),
	U3CCoClosingU3Ec__Iterator1_t587244119::get_offset_of_U24locvar0_1(),
	U3CCoClosingU3Ec__Iterator1_t587244119::get_offset_of_U24locvar1_2(),
	U3CCoClosingU3Ec__Iterator1_t587244119::get_offset_of_U24this_3(),
	U3CCoClosingU3Ec__Iterator1_t587244119::get_offset_of_U24current_4(),
	U3CCoClosingU3Ec__Iterator1_t587244119::get_offset_of_U24disposing_5(),
	U3CCoClosingU3Ec__Iterator1_t587244119::get_offset_of_U24PC_6(),
	U3CCoClosingU3Ec__Iterator1_t587244119_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2886 = { sizeof (UguiViewTransitionCrossFade_t1724059380), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2886[3] = 
{
	UguiViewTransitionCrossFade_t1724059380::get_offset_of_uguiView_2(),
	UguiViewTransitionCrossFade_t1724059380::get_offset_of_isPlaying_3(),
	UguiViewTransitionCrossFade_t1724059380::get_offset_of_time_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2887 = { sizeof (U3CCoOpenU3Ec__Iterator0_t4185887894), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2887[6] = 
{
	U3CCoOpenU3Ec__Iterator0_t4185887894::get_offset_of_U3CcanvasGroupU3E__0_0(),
	U3CCoOpenU3Ec__Iterator0_t4185887894::get_offset_of_U3CcurrentTimeU3E__0_1(),
	U3CCoOpenU3Ec__Iterator0_t4185887894::get_offset_of_U24this_2(),
	U3CCoOpenU3Ec__Iterator0_t4185887894::get_offset_of_U24current_3(),
	U3CCoOpenU3Ec__Iterator0_t4185887894::get_offset_of_U24disposing_4(),
	U3CCoOpenU3Ec__Iterator0_t4185887894::get_offset_of_U24PC_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2888 = { sizeof (U3CCoCloseU3Ec__Iterator1_t1558610921), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2888[6] = 
{
	U3CCoCloseU3Ec__Iterator1_t1558610921::get_offset_of_U3CcanvasGroupU3E__0_0(),
	U3CCoCloseU3Ec__Iterator1_t1558610921::get_offset_of_U3CcurrentTimeU3E__0_1(),
	U3CCoCloseU3Ec__Iterator1_t1558610921::get_offset_of_U24this_2(),
	U3CCoCloseU3Ec__Iterator1_t1558610921::get_offset_of_U24current_3(),
	U3CCoCloseU3Ec__Iterator1_t1558610921::get_offset_of_U24disposing_4(),
	U3CCoCloseU3Ec__Iterator1_t1558610921::get_offset_of_U24PC_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2889 = { sizeof (Alignment_t3342318601)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2889[12] = 
{
	Alignment_t3342318601::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2890 = { sizeof (AlignmentUtil_t2144597389), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2891 = { sizeof (ColorUtil_t1432289115), -1, sizeof(ColorUtil_t1432289115_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2891[23] = 
{
	ColorUtil_t1432289115_StaticFields::get_offset_of_Aqua_0(),
	ColorUtil_t1432289115_StaticFields::get_offset_of_Black_1(),
	ColorUtil_t1432289115_StaticFields::get_offset_of_Blue_2(),
	ColorUtil_t1432289115_StaticFields::get_offset_of_Brown_3(),
	ColorUtil_t1432289115_StaticFields::get_offset_of_Cyan_4(),
	ColorUtil_t1432289115_StaticFields::get_offset_of_Darkblue_5(),
	ColorUtil_t1432289115_StaticFields::get_offset_of_Fuchsia_6(),
	ColorUtil_t1432289115_StaticFields::get_offset_of_Green_7(),
	ColorUtil_t1432289115_StaticFields::get_offset_of_Grey_8(),
	ColorUtil_t1432289115_StaticFields::get_offset_of_Lightblue_9(),
	ColorUtil_t1432289115_StaticFields::get_offset_of_Lime_10(),
	ColorUtil_t1432289115_StaticFields::get_offset_of_Magenta_11(),
	ColorUtil_t1432289115_StaticFields::get_offset_of_Maroon_12(),
	ColorUtil_t1432289115_StaticFields::get_offset_of_Navy_13(),
	ColorUtil_t1432289115_StaticFields::get_offset_of_Olive_14(),
	ColorUtil_t1432289115_StaticFields::get_offset_of_Orange_15(),
	ColorUtil_t1432289115_StaticFields::get_offset_of_Purple_16(),
	ColorUtil_t1432289115_StaticFields::get_offset_of_Red_17(),
	ColorUtil_t1432289115_StaticFields::get_offset_of_Silver_18(),
	ColorUtil_t1432289115_StaticFields::get_offset_of_Teal_19(),
	ColorUtil_t1432289115_StaticFields::get_offset_of_White_20(),
	ColorUtil_t1432289115_StaticFields::get_offset_of_Yellow_21(),
	ColorUtil_t1432289115_StaticFields::get_offset_of_U3CU3Ef__switchU24mapB_22(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2892 = { sizeof (DontDestoryOnLoad_t3157743482), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2892[1] = 
{
	DontDestoryOnLoad_t3157743482::get_offset_of_dontDestoryOnLoad_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2893 = { sizeof (ExtensionUtil_t1676251555), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2893[11] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2894 = { sizeof (FlagsUtil_t2831559481), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2895 = { sizeof (InputUtil_t3545718610), -1, sizeof(InputUtil_t3545718610_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2895[1] = 
{
	InputUtil_t3545718610_StaticFields::get_offset_of_wheelSensitive_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2896 = { sizeof (LinearValue_t662057272), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2896[5] = 
{
	LinearValue_t662057272::get_offset_of_time_0(),
	LinearValue_t662057272::get_offset_of_timeCurrent_1(),
	LinearValue_t662057272::get_offset_of_valueBegin_2(),
	LinearValue_t662057272::get_offset_of_valueEnd_3(),
	LinearValue_t662057272::get_offset_of_valueCurrent_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2897 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2897[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2898 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2898[4] = 
{
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2899 = { sizeof (ParserUtil_t1878827749), -1, 0, 0 };
#ifdef __clang__
#pragma clang diagnostic pop
#endif
