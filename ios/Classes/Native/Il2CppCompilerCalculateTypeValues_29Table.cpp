﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "object-internals.h"

// System.Action`1<UtageUguiSceneGalleryItem>
struct Action_1_t30213095;
// UtageUguiSceneGalleryItem
struct UtageUguiSceneGalleryItem_t228413713;
// UtageUguiMainGame
struct UtageUguiMainGame_t4178963699;
// UtageUguiSceneGallery
struct UtageUguiSceneGallery_t811869004;
// Utage.AdvSaveManager
struct AdvSaveManager_t2382020483;
// System.Collections.Generic.List`1<Utage.AdvSaveData>
struct List_1_t3428608598;
// UtageUguiSaveLoad
struct UtageUguiSaveLoad_t130048949;
// UtageUguiConfig
struct UtageUguiConfig_t3500199096;
// UtageUguiSoundRoom
struct UtageUguiSoundRoom_t2656581854;
// System.String
struct String_t;
// Utage.AssetFile
struct AssetFile_t3383013256;
// UnityEngine.UI.Slider
struct Slider_t297367283;
// System.Action`1<UtageUguiSoundRoomItem>
struct Action_1_t3779640373;
// UtageUguiSoundRoomItem
struct UtageUguiSoundRoomItem_t3977840991;
// UtageUguiBoot
struct UtageUguiBoot_t1438884946;
// System.Globalization.CultureInfo
struct CultureInfo_t3500843524;
// UnityEngine.Events.UnityAction`1<UnityEngine.EventSystems.BaseEventData>
struct UnityAction_1_t4047591376;
// Utage.WrapperMoviePlayer
struct WrapperMoviePlayer_t3887243686;
// System.Action`1<UtageUguiCgGalleryItem>
struct Action_1_t1248543733;
// UtageUguiCgGalleryItem
struct UtageUguiCgGalleryItem_t1446744351;
// UtageUguiCgGallery
struct UtageUguiCgGallery_t1507549878;
// UtageUguiLoadWait
struct UtageUguiLoadWait_t319861767;
// System.Action`1<UtageUguiSaveLoadItem>
struct Action_1_t913746694;
// UtageUguiSaveLoadItem
struct UtageUguiSaveLoadItem_t1111947312;
// System.Void
struct Void_t1841601450;
// System.Char[]
struct CharU5BU5D_t1328083999;
// Utage.AdvUguiLoadGraphicFile
struct AdvUguiLoadGraphicFile_t1474676621;
// UnityEngine.UI.Text
struct Text_t356221433;
// Utage.AdvCgGalleryData
struct AdvCgGalleryData_t4276096361;
// Utage.AdvSoundSettingData
struct AdvSoundSettingData_t3135391222;
// Utage.AdvSceneGallerySettingData
struct AdvSceneGallerySettingData_t1280603049;
// UnityEngine.AudioClip
struct AudioClip_t1932558630;
// UnityEngine.Events.UnityEvent
struct UnityEvent_t408735097;
// UnityEngine.CanvasGroup
struct CanvasGroup_t3296560743;
// Utage.LipSynchEvent
struct LipSynchEvent_t825925384;
// UnityEngine.GameObject
struct GameObject_t1756533147;
// Utage.AdvEngine
struct AdvEngine_t1176753927;
// Utage.AdvGraphicObjectCustom
struct AdvGraphicObjectCustom_t976437613;
// Live2D.Cubism.Rendering.CubismRenderController
struct CubismRenderController_t3290083685;
// Utage.Live2DLipSynch
struct Live2DLipSynch_t3265978124;
// UnityEngine.UI.RawImage
struct RawImage_t2749640213;
// UnityEngine.Texture2D
struct Texture2D_t3542995729;
// UnityEngine.UI.Button
struct Button_t2872111280;
// Utage.AdvSaveData
struct AdvSaveData_t4059487466;
// UnityEngine.UI.Toggle
struct Toggle_t3976754468;
// UtageUguiGallery
struct UtageUguiGallery_t4263677524;
// UtageUguiCgGalleryViewer
struct UtageUguiCgGalleryViewer_t3438583972;
// Utage.UguiCategoryGridPage
struct UguiCategoryGridPage_t673822931;
// System.Collections.Generic.List`1<Utage.AdvCgGalleryData>
struct List_1_t3645217493;
// Utage.UguiGridPage
struct UguiGridPage_t1812803607;
// UnityEngine.UI.ScrollRect
struct ScrollRect_t1199013257;
// Utage.AdvEngineStarter
struct AdvEngineStarter_t330044300;
// UtageUguiTitle
struct UtageUguiTitle_t1483171752;
// UnityEngine.UI.Image
struct Image_t2042527209;
// Utage.OpenDialogEvent
struct OpenDialogEvent_t2352564994;
// Utage.UguiView[]
struct UguiViewU5BU5D_t1602826856;
// Utage.LetterBoxCamera
struct LetterBoxCamera_t3507617684;
// Utage.UguiFadeTextureStream
struct UguiFadeTextureStream_t4272889985;
// Utage.UguiListView
struct UguiListView_t169672791;
// System.Collections.Generic.List`1<Utage.AdvSoundSettingData>
struct List_1_t2504512354;
// System.Collections.Generic.List`1<Utage.AdvSceneGallerySettingData>
struct List_1_t649724181;
// Utage.UguiToggleGroupIndexed
struct UguiToggleGroupIndexed_t999166452;
// System.Collections.Generic.List`1<UtageUguiConfig/TagedMasterVolumSliders>
struct List_1_t3029804307;
// Live2D.Cubism.Framework.MouthMovement.CubismMouthController
struct CubismMouthController_t3330055306;
// Live2D.Cubism.Framework.MouthMovement.CubismAudioMouthInput
struct CubismAudioMouthInput_t1379032486;
// Live2D.Cubism.Framework.MouthMovement.CubismAutoMouthInput
struct CubismAutoMouthInput_t1722567851;




#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
#ifndef U3CINITU3EC__ANONSTOREY0_T2161906049_H
#define U3CINITU3EC__ANONSTOREY0_T2161906049_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UtageUguiSceneGalleryItem/<Init>c__AnonStorey0
struct  U3CInitU3Ec__AnonStorey0_t2161906049  : public RuntimeObject
{
public:
	// System.Action`1<UtageUguiSceneGalleryItem> UtageUguiSceneGalleryItem/<Init>c__AnonStorey0::ButtonClickedEvent
	Action_1_t30213095 * ___ButtonClickedEvent_0;
	// UtageUguiSceneGalleryItem UtageUguiSceneGalleryItem/<Init>c__AnonStorey0::$this
	UtageUguiSceneGalleryItem_t228413713 * ___U24this_1;

public:
	inline static int32_t get_offset_of_ButtonClickedEvent_0() { return static_cast<int32_t>(offsetof(U3CInitU3Ec__AnonStorey0_t2161906049, ___ButtonClickedEvent_0)); }
	inline Action_1_t30213095 * get_ButtonClickedEvent_0() const { return ___ButtonClickedEvent_0; }
	inline Action_1_t30213095 ** get_address_of_ButtonClickedEvent_0() { return &___ButtonClickedEvent_0; }
	inline void set_ButtonClickedEvent_0(Action_1_t30213095 * value)
	{
		___ButtonClickedEvent_0 = value;
		Il2CppCodeGenWriteBarrier((&___ButtonClickedEvent_0), value);
	}

	inline static int32_t get_offset_of_U24this_1() { return static_cast<int32_t>(offsetof(U3CInitU3Ec__AnonStorey0_t2161906049, ___U24this_1)); }
	inline UtageUguiSceneGalleryItem_t228413713 * get_U24this_1() const { return ___U24this_1; }
	inline UtageUguiSceneGalleryItem_t228413713 ** get_address_of_U24this_1() { return &___U24this_1; }
	inline void set_U24this_1(UtageUguiSceneGalleryItem_t228413713 * value)
	{
		___U24this_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CINITU3EC__ANONSTOREY0_T2161906049_H
#ifndef U3CCOQSAVEU3EC__ITERATOR3_T2917206258_H
#define U3CCOQSAVEU3EC__ITERATOR3_T2917206258_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UtageUguiMainGame/<CoQSave>c__Iterator3
struct  U3CCoQSaveU3Ec__Iterator3_t2917206258  : public RuntimeObject
{
public:
	// UtageUguiMainGame UtageUguiMainGame/<CoQSave>c__Iterator3::$this
	UtageUguiMainGame_t4178963699 * ___U24this_0;
	// System.Object UtageUguiMainGame/<CoQSave>c__Iterator3::$current
	RuntimeObject * ___U24current_1;
	// System.Boolean UtageUguiMainGame/<CoQSave>c__Iterator3::$disposing
	bool ___U24disposing_2;
	// System.Int32 UtageUguiMainGame/<CoQSave>c__Iterator3::$PC
	int32_t ___U24PC_3;

public:
	inline static int32_t get_offset_of_U24this_0() { return static_cast<int32_t>(offsetof(U3CCoQSaveU3Ec__Iterator3_t2917206258, ___U24this_0)); }
	inline UtageUguiMainGame_t4178963699 * get_U24this_0() const { return ___U24this_0; }
	inline UtageUguiMainGame_t4178963699 ** get_address_of_U24this_0() { return &___U24this_0; }
	inline void set_U24this_0(UtageUguiMainGame_t4178963699 * value)
	{
		___U24this_0 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_0), value);
	}

	inline static int32_t get_offset_of_U24current_1() { return static_cast<int32_t>(offsetof(U3CCoQSaveU3Ec__Iterator3_t2917206258, ___U24current_1)); }
	inline RuntimeObject * get_U24current_1() const { return ___U24current_1; }
	inline RuntimeObject ** get_address_of_U24current_1() { return &___U24current_1; }
	inline void set_U24current_1(RuntimeObject * value)
	{
		___U24current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_1), value);
	}

	inline static int32_t get_offset_of_U24disposing_2() { return static_cast<int32_t>(offsetof(U3CCoQSaveU3Ec__Iterator3_t2917206258, ___U24disposing_2)); }
	inline bool get_U24disposing_2() const { return ___U24disposing_2; }
	inline bool* get_address_of_U24disposing_2() { return &___U24disposing_2; }
	inline void set_U24disposing_2(bool value)
	{
		___U24disposing_2 = value;
	}

	inline static int32_t get_offset_of_U24PC_3() { return static_cast<int32_t>(offsetof(U3CCoQSaveU3Ec__Iterator3_t2917206258, ___U24PC_3)); }
	inline int32_t get_U24PC_3() const { return ___U24PC_3; }
	inline int32_t* get_address_of_U24PC_3() { return &___U24PC_3; }
	inline void set_U24PC_3(int32_t value)
	{
		___U24PC_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CCOQSAVEU3EC__ITERATOR3_T2917206258_H
#ifndef U3CCOWAITOPENU3EC__ITERATOR0_T710238179_H
#define U3CCOWAITOPENU3EC__ITERATOR0_T710238179_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UtageUguiSceneGallery/<CoWaitOpen>c__Iterator0
struct  U3CCoWaitOpenU3Ec__Iterator0_t710238179  : public RuntimeObject
{
public:
	// UtageUguiSceneGallery UtageUguiSceneGallery/<CoWaitOpen>c__Iterator0::$this
	UtageUguiSceneGallery_t811869004 * ___U24this_0;
	// System.Object UtageUguiSceneGallery/<CoWaitOpen>c__Iterator0::$current
	RuntimeObject * ___U24current_1;
	// System.Boolean UtageUguiSceneGallery/<CoWaitOpen>c__Iterator0::$disposing
	bool ___U24disposing_2;
	// System.Int32 UtageUguiSceneGallery/<CoWaitOpen>c__Iterator0::$PC
	int32_t ___U24PC_3;

public:
	inline static int32_t get_offset_of_U24this_0() { return static_cast<int32_t>(offsetof(U3CCoWaitOpenU3Ec__Iterator0_t710238179, ___U24this_0)); }
	inline UtageUguiSceneGallery_t811869004 * get_U24this_0() const { return ___U24this_0; }
	inline UtageUguiSceneGallery_t811869004 ** get_address_of_U24this_0() { return &___U24this_0; }
	inline void set_U24this_0(UtageUguiSceneGallery_t811869004 * value)
	{
		___U24this_0 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_0), value);
	}

	inline static int32_t get_offset_of_U24current_1() { return static_cast<int32_t>(offsetof(U3CCoWaitOpenU3Ec__Iterator0_t710238179, ___U24current_1)); }
	inline RuntimeObject * get_U24current_1() const { return ___U24current_1; }
	inline RuntimeObject ** get_address_of_U24current_1() { return &___U24current_1; }
	inline void set_U24current_1(RuntimeObject * value)
	{
		___U24current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_1), value);
	}

	inline static int32_t get_offset_of_U24disposing_2() { return static_cast<int32_t>(offsetof(U3CCoWaitOpenU3Ec__Iterator0_t710238179, ___U24disposing_2)); }
	inline bool get_U24disposing_2() const { return ___U24disposing_2; }
	inline bool* get_address_of_U24disposing_2() { return &___U24disposing_2; }
	inline void set_U24disposing_2(bool value)
	{
		___U24disposing_2 = value;
	}

	inline static int32_t get_offset_of_U24PC_3() { return static_cast<int32_t>(offsetof(U3CCoWaitOpenU3Ec__Iterator0_t710238179, ___U24PC_3)); }
	inline int32_t get_U24PC_3() const { return ___U24PC_3; }
	inline int32_t* get_address_of_U24PC_3() { return &___U24PC_3; }
	inline void set_U24PC_3(int32_t value)
	{
		___U24PC_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CCOWAITOPENU3EC__ITERATOR0_T710238179_H
#ifndef U3CCOWAITOPENU3EC__ITERATOR0_T1585171652_H
#define U3CCOWAITOPENU3EC__ITERATOR0_T1585171652_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UtageUguiSaveLoad/<CoWaitOpen>c__Iterator0
struct  U3CCoWaitOpenU3Ec__Iterator0_t1585171652  : public RuntimeObject
{
public:
	// Utage.AdvSaveManager UtageUguiSaveLoad/<CoWaitOpen>c__Iterator0::<saveManager>__0
	AdvSaveManager_t2382020483 * ___U3CsaveManagerU3E__0_0;
	// System.Collections.Generic.List`1<Utage.AdvSaveData> UtageUguiSaveLoad/<CoWaitOpen>c__Iterator0::<list>__0
	List_1_t3428608598 * ___U3ClistU3E__0_1;
	// UtageUguiSaveLoad UtageUguiSaveLoad/<CoWaitOpen>c__Iterator0::$this
	UtageUguiSaveLoad_t130048949 * ___U24this_2;
	// System.Object UtageUguiSaveLoad/<CoWaitOpen>c__Iterator0::$current
	RuntimeObject * ___U24current_3;
	// System.Boolean UtageUguiSaveLoad/<CoWaitOpen>c__Iterator0::$disposing
	bool ___U24disposing_4;
	// System.Int32 UtageUguiSaveLoad/<CoWaitOpen>c__Iterator0::$PC
	int32_t ___U24PC_5;

public:
	inline static int32_t get_offset_of_U3CsaveManagerU3E__0_0() { return static_cast<int32_t>(offsetof(U3CCoWaitOpenU3Ec__Iterator0_t1585171652, ___U3CsaveManagerU3E__0_0)); }
	inline AdvSaveManager_t2382020483 * get_U3CsaveManagerU3E__0_0() const { return ___U3CsaveManagerU3E__0_0; }
	inline AdvSaveManager_t2382020483 ** get_address_of_U3CsaveManagerU3E__0_0() { return &___U3CsaveManagerU3E__0_0; }
	inline void set_U3CsaveManagerU3E__0_0(AdvSaveManager_t2382020483 * value)
	{
		___U3CsaveManagerU3E__0_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CsaveManagerU3E__0_0), value);
	}

	inline static int32_t get_offset_of_U3ClistU3E__0_1() { return static_cast<int32_t>(offsetof(U3CCoWaitOpenU3Ec__Iterator0_t1585171652, ___U3ClistU3E__0_1)); }
	inline List_1_t3428608598 * get_U3ClistU3E__0_1() const { return ___U3ClistU3E__0_1; }
	inline List_1_t3428608598 ** get_address_of_U3ClistU3E__0_1() { return &___U3ClistU3E__0_1; }
	inline void set_U3ClistU3E__0_1(List_1_t3428608598 * value)
	{
		___U3ClistU3E__0_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3ClistU3E__0_1), value);
	}

	inline static int32_t get_offset_of_U24this_2() { return static_cast<int32_t>(offsetof(U3CCoWaitOpenU3Ec__Iterator0_t1585171652, ___U24this_2)); }
	inline UtageUguiSaveLoad_t130048949 * get_U24this_2() const { return ___U24this_2; }
	inline UtageUguiSaveLoad_t130048949 ** get_address_of_U24this_2() { return &___U24this_2; }
	inline void set_U24this_2(UtageUguiSaveLoad_t130048949 * value)
	{
		___U24this_2 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_2), value);
	}

	inline static int32_t get_offset_of_U24current_3() { return static_cast<int32_t>(offsetof(U3CCoWaitOpenU3Ec__Iterator0_t1585171652, ___U24current_3)); }
	inline RuntimeObject * get_U24current_3() const { return ___U24current_3; }
	inline RuntimeObject ** get_address_of_U24current_3() { return &___U24current_3; }
	inline void set_U24current_3(RuntimeObject * value)
	{
		___U24current_3 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_3), value);
	}

	inline static int32_t get_offset_of_U24disposing_4() { return static_cast<int32_t>(offsetof(U3CCoWaitOpenU3Ec__Iterator0_t1585171652, ___U24disposing_4)); }
	inline bool get_U24disposing_4() const { return ___U24disposing_4; }
	inline bool* get_address_of_U24disposing_4() { return &___U24disposing_4; }
	inline void set_U24disposing_4(bool value)
	{
		___U24disposing_4 = value;
	}

	inline static int32_t get_offset_of_U24PC_5() { return static_cast<int32_t>(offsetof(U3CCoWaitOpenU3Ec__Iterator0_t1585171652, ___U24PC_5)); }
	inline int32_t get_U24PC_5() const { return ___U24PC_5; }
	inline int32_t* get_address_of_U24PC_5() { return &___U24PC_5; }
	inline void set_U24PC_5(int32_t value)
	{
		___U24PC_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CCOWAITOPENU3EC__ITERATOR0_T1585171652_H
#ifndef U3CCOWAITOPENU3EC__ITERATOR0_T3688245207_H
#define U3CCOWAITOPENU3EC__ITERATOR0_T3688245207_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UtageUguiConfig/<CoWaitOpen>c__Iterator0
struct  U3CCoWaitOpenU3Ec__Iterator0_t3688245207  : public RuntimeObject
{
public:
	// UtageUguiConfig UtageUguiConfig/<CoWaitOpen>c__Iterator0::$this
	UtageUguiConfig_t3500199096 * ___U24this_0;
	// System.Object UtageUguiConfig/<CoWaitOpen>c__Iterator0::$current
	RuntimeObject * ___U24current_1;
	// System.Boolean UtageUguiConfig/<CoWaitOpen>c__Iterator0::$disposing
	bool ___U24disposing_2;
	// System.Int32 UtageUguiConfig/<CoWaitOpen>c__Iterator0::$PC
	int32_t ___U24PC_3;

public:
	inline static int32_t get_offset_of_U24this_0() { return static_cast<int32_t>(offsetof(U3CCoWaitOpenU3Ec__Iterator0_t3688245207, ___U24this_0)); }
	inline UtageUguiConfig_t3500199096 * get_U24this_0() const { return ___U24this_0; }
	inline UtageUguiConfig_t3500199096 ** get_address_of_U24this_0() { return &___U24this_0; }
	inline void set_U24this_0(UtageUguiConfig_t3500199096 * value)
	{
		___U24this_0 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_0), value);
	}

	inline static int32_t get_offset_of_U24current_1() { return static_cast<int32_t>(offsetof(U3CCoWaitOpenU3Ec__Iterator0_t3688245207, ___U24current_1)); }
	inline RuntimeObject * get_U24current_1() const { return ___U24current_1; }
	inline RuntimeObject ** get_address_of_U24current_1() { return &___U24current_1; }
	inline void set_U24current_1(RuntimeObject * value)
	{
		___U24current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_1), value);
	}

	inline static int32_t get_offset_of_U24disposing_2() { return static_cast<int32_t>(offsetof(U3CCoWaitOpenU3Ec__Iterator0_t3688245207, ___U24disposing_2)); }
	inline bool get_U24disposing_2() const { return ___U24disposing_2; }
	inline bool* get_address_of_U24disposing_2() { return &___U24disposing_2; }
	inline void set_U24disposing_2(bool value)
	{
		___U24disposing_2 = value;
	}

	inline static int32_t get_offset_of_U24PC_3() { return static_cast<int32_t>(offsetof(U3CCoWaitOpenU3Ec__Iterator0_t3688245207, ___U24PC_3)); }
	inline int32_t get_U24PC_3() const { return ___U24PC_3; }
	inline int32_t* get_address_of_U24PC_3() { return &___U24PC_3; }
	inline void set_U24PC_3(int32_t value)
	{
		___U24PC_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CCOWAITOPENU3EC__ITERATOR0_T3688245207_H
#ifndef U3CCOWAITOPENU3EC__ITERATOR0_T3263074609_H
#define U3CCOWAITOPENU3EC__ITERATOR0_T3263074609_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UtageUguiSoundRoom/<CoWaitOpen>c__Iterator0
struct  U3CCoWaitOpenU3Ec__Iterator0_t3263074609  : public RuntimeObject
{
public:
	// UtageUguiSoundRoom UtageUguiSoundRoom/<CoWaitOpen>c__Iterator0::$this
	UtageUguiSoundRoom_t2656581854 * ___U24this_0;
	// System.Object UtageUguiSoundRoom/<CoWaitOpen>c__Iterator0::$current
	RuntimeObject * ___U24current_1;
	// System.Boolean UtageUguiSoundRoom/<CoWaitOpen>c__Iterator0::$disposing
	bool ___U24disposing_2;
	// System.Int32 UtageUguiSoundRoom/<CoWaitOpen>c__Iterator0::$PC
	int32_t ___U24PC_3;

public:
	inline static int32_t get_offset_of_U24this_0() { return static_cast<int32_t>(offsetof(U3CCoWaitOpenU3Ec__Iterator0_t3263074609, ___U24this_0)); }
	inline UtageUguiSoundRoom_t2656581854 * get_U24this_0() const { return ___U24this_0; }
	inline UtageUguiSoundRoom_t2656581854 ** get_address_of_U24this_0() { return &___U24this_0; }
	inline void set_U24this_0(UtageUguiSoundRoom_t2656581854 * value)
	{
		___U24this_0 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_0), value);
	}

	inline static int32_t get_offset_of_U24current_1() { return static_cast<int32_t>(offsetof(U3CCoWaitOpenU3Ec__Iterator0_t3263074609, ___U24current_1)); }
	inline RuntimeObject * get_U24current_1() const { return ___U24current_1; }
	inline RuntimeObject ** get_address_of_U24current_1() { return &___U24current_1; }
	inline void set_U24current_1(RuntimeObject * value)
	{
		___U24current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_1), value);
	}

	inline static int32_t get_offset_of_U24disposing_2() { return static_cast<int32_t>(offsetof(U3CCoWaitOpenU3Ec__Iterator0_t3263074609, ___U24disposing_2)); }
	inline bool get_U24disposing_2() const { return ___U24disposing_2; }
	inline bool* get_address_of_U24disposing_2() { return &___U24disposing_2; }
	inline void set_U24disposing_2(bool value)
	{
		___U24disposing_2 = value;
	}

	inline static int32_t get_offset_of_U24PC_3() { return static_cast<int32_t>(offsetof(U3CCoWaitOpenU3Ec__Iterator0_t3263074609, ___U24PC_3)); }
	inline int32_t get_U24PC_3() const { return ___U24PC_3; }
	inline int32_t* get_address_of_U24PC_3() { return &___U24PC_3; }
	inline void set_U24PC_3(int32_t value)
	{
		___U24PC_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CCOWAITOPENU3EC__ITERATOR0_T3263074609_H
#ifndef U3CCOPLAYSOUNDU3EC__ITERATOR1_T3028347198_H
#define U3CCOPLAYSOUNDU3EC__ITERATOR1_T3028347198_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UtageUguiSoundRoom/<CoPlaySound>c__Iterator1
struct  U3CCoPlaySoundU3Ec__Iterator1_t3028347198  : public RuntimeObject
{
public:
	// System.String UtageUguiSoundRoom/<CoPlaySound>c__Iterator1::path
	String_t* ___path_0;
	// Utage.AssetFile UtageUguiSoundRoom/<CoPlaySound>c__Iterator1::<file>__0
	RuntimeObject* ___U3CfileU3E__0_1;
	// UtageUguiSoundRoom UtageUguiSoundRoom/<CoPlaySound>c__Iterator1::$this
	UtageUguiSoundRoom_t2656581854 * ___U24this_2;
	// System.Object UtageUguiSoundRoom/<CoPlaySound>c__Iterator1::$current
	RuntimeObject * ___U24current_3;
	// System.Boolean UtageUguiSoundRoom/<CoPlaySound>c__Iterator1::$disposing
	bool ___U24disposing_4;
	// System.Int32 UtageUguiSoundRoom/<CoPlaySound>c__Iterator1::$PC
	int32_t ___U24PC_5;

public:
	inline static int32_t get_offset_of_path_0() { return static_cast<int32_t>(offsetof(U3CCoPlaySoundU3Ec__Iterator1_t3028347198, ___path_0)); }
	inline String_t* get_path_0() const { return ___path_0; }
	inline String_t** get_address_of_path_0() { return &___path_0; }
	inline void set_path_0(String_t* value)
	{
		___path_0 = value;
		Il2CppCodeGenWriteBarrier((&___path_0), value);
	}

	inline static int32_t get_offset_of_U3CfileU3E__0_1() { return static_cast<int32_t>(offsetof(U3CCoPlaySoundU3Ec__Iterator1_t3028347198, ___U3CfileU3E__0_1)); }
	inline RuntimeObject* get_U3CfileU3E__0_1() const { return ___U3CfileU3E__0_1; }
	inline RuntimeObject** get_address_of_U3CfileU3E__0_1() { return &___U3CfileU3E__0_1; }
	inline void set_U3CfileU3E__0_1(RuntimeObject* value)
	{
		___U3CfileU3E__0_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CfileU3E__0_1), value);
	}

	inline static int32_t get_offset_of_U24this_2() { return static_cast<int32_t>(offsetof(U3CCoPlaySoundU3Ec__Iterator1_t3028347198, ___U24this_2)); }
	inline UtageUguiSoundRoom_t2656581854 * get_U24this_2() const { return ___U24this_2; }
	inline UtageUguiSoundRoom_t2656581854 ** get_address_of_U24this_2() { return &___U24this_2; }
	inline void set_U24this_2(UtageUguiSoundRoom_t2656581854 * value)
	{
		___U24this_2 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_2), value);
	}

	inline static int32_t get_offset_of_U24current_3() { return static_cast<int32_t>(offsetof(U3CCoPlaySoundU3Ec__Iterator1_t3028347198, ___U24current_3)); }
	inline RuntimeObject * get_U24current_3() const { return ___U24current_3; }
	inline RuntimeObject ** get_address_of_U24current_3() { return &___U24current_3; }
	inline void set_U24current_3(RuntimeObject * value)
	{
		___U24current_3 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_3), value);
	}

	inline static int32_t get_offset_of_U24disposing_4() { return static_cast<int32_t>(offsetof(U3CCoPlaySoundU3Ec__Iterator1_t3028347198, ___U24disposing_4)); }
	inline bool get_U24disposing_4() const { return ___U24disposing_4; }
	inline bool* get_address_of_U24disposing_4() { return &___U24disposing_4; }
	inline void set_U24disposing_4(bool value)
	{
		___U24disposing_4 = value;
	}

	inline static int32_t get_offset_of_U24PC_5() { return static_cast<int32_t>(offsetof(U3CCoPlaySoundU3Ec__Iterator1_t3028347198, ___U24PC_5)); }
	inline int32_t get_U24PC_5() const { return ___U24PC_5; }
	inline int32_t* get_address_of_U24PC_5() { return &___U24PC_5; }
	inline void set_U24PC_5(int32_t value)
	{
		___U24PC_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CCOPLAYSOUNDU3EC__ITERATOR1_T3028347198_H
#ifndef TAGEDMASTERVOLUMSLIDERS_T3660683175_H
#define TAGEDMASTERVOLUMSLIDERS_T3660683175_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UtageUguiConfig/TagedMasterVolumSliders
struct  TagedMasterVolumSliders_t3660683175  : public RuntimeObject
{
public:
	// System.String UtageUguiConfig/TagedMasterVolumSliders::tag
	String_t* ___tag_0;
	// UnityEngine.UI.Slider UtageUguiConfig/TagedMasterVolumSliders::volumeSlider
	Slider_t297367283 * ___volumeSlider_1;

public:
	inline static int32_t get_offset_of_tag_0() { return static_cast<int32_t>(offsetof(TagedMasterVolumSliders_t3660683175, ___tag_0)); }
	inline String_t* get_tag_0() const { return ___tag_0; }
	inline String_t** get_address_of_tag_0() { return &___tag_0; }
	inline void set_tag_0(String_t* value)
	{
		___tag_0 = value;
		Il2CppCodeGenWriteBarrier((&___tag_0), value);
	}

	inline static int32_t get_offset_of_volumeSlider_1() { return static_cast<int32_t>(offsetof(TagedMasterVolumSliders_t3660683175, ___volumeSlider_1)); }
	inline Slider_t297367283 * get_volumeSlider_1() const { return ___volumeSlider_1; }
	inline Slider_t297367283 ** get_address_of_volumeSlider_1() { return &___volumeSlider_1; }
	inline void set_volumeSlider_1(Slider_t297367283 * value)
	{
		___volumeSlider_1 = value;
		Il2CppCodeGenWriteBarrier((&___volumeSlider_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TAGEDMASTERVOLUMSLIDERS_T3660683175_H
#ifndef U3CINITU3EC__ANONSTOREY0_T1904244095_H
#define U3CINITU3EC__ANONSTOREY0_T1904244095_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UtageUguiSoundRoomItem/<Init>c__AnonStorey0
struct  U3CInitU3Ec__AnonStorey0_t1904244095  : public RuntimeObject
{
public:
	// System.Action`1<UtageUguiSoundRoomItem> UtageUguiSoundRoomItem/<Init>c__AnonStorey0::ButtonClickedEvent
	Action_1_t3779640373 * ___ButtonClickedEvent_0;
	// UtageUguiSoundRoomItem UtageUguiSoundRoomItem/<Init>c__AnonStorey0::$this
	UtageUguiSoundRoomItem_t3977840991 * ___U24this_1;

public:
	inline static int32_t get_offset_of_ButtonClickedEvent_0() { return static_cast<int32_t>(offsetof(U3CInitU3Ec__AnonStorey0_t1904244095, ___ButtonClickedEvent_0)); }
	inline Action_1_t3779640373 * get_ButtonClickedEvent_0() const { return ___ButtonClickedEvent_0; }
	inline Action_1_t3779640373 ** get_address_of_ButtonClickedEvent_0() { return &___ButtonClickedEvent_0; }
	inline void set_ButtonClickedEvent_0(Action_1_t3779640373 * value)
	{
		___ButtonClickedEvent_0 = value;
		Il2CppCodeGenWriteBarrier((&___ButtonClickedEvent_0), value);
	}

	inline static int32_t get_offset_of_U24this_1() { return static_cast<int32_t>(offsetof(U3CInitU3Ec__AnonStorey0_t1904244095, ___U24this_1)); }
	inline UtageUguiSoundRoomItem_t3977840991 * get_U24this_1() const { return ___U24this_1; }
	inline UtageUguiSoundRoomItem_t3977840991 ** get_address_of_U24this_1() { return &___U24this_1; }
	inline void set_U24this_1(UtageUguiSoundRoomItem_t3977840991 * value)
	{
		___U24this_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CINITU3EC__ANONSTOREY0_T1904244095_H
#ifndef VERSIONUTIL_T3068462550_H
#define VERSIONUTIL_T3068462550_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.VersionUtil
struct  VersionUtil_t3068462550  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VERSIONUTIL_T3068462550_H
#ifndef U3CCOUPDATEU3EC__ITERATOR0_T3541358793_H
#define U3CCOUPDATEU3EC__ITERATOR0_T3541358793_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UtageUguiBoot/<CoUpdate>c__Iterator0
struct  U3CCoUpdateU3Ec__Iterator0_t3541358793  : public RuntimeObject
{
public:
	// UtageUguiBoot UtageUguiBoot/<CoUpdate>c__Iterator0::$this
	UtageUguiBoot_t1438884946 * ___U24this_0;
	// System.Object UtageUguiBoot/<CoUpdate>c__Iterator0::$current
	RuntimeObject * ___U24current_1;
	// System.Boolean UtageUguiBoot/<CoUpdate>c__Iterator0::$disposing
	bool ___U24disposing_2;
	// System.Int32 UtageUguiBoot/<CoUpdate>c__Iterator0::$PC
	int32_t ___U24PC_3;

public:
	inline static int32_t get_offset_of_U24this_0() { return static_cast<int32_t>(offsetof(U3CCoUpdateU3Ec__Iterator0_t3541358793, ___U24this_0)); }
	inline UtageUguiBoot_t1438884946 * get_U24this_0() const { return ___U24this_0; }
	inline UtageUguiBoot_t1438884946 ** get_address_of_U24this_0() { return &___U24this_0; }
	inline void set_U24this_0(UtageUguiBoot_t1438884946 * value)
	{
		___U24this_0 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_0), value);
	}

	inline static int32_t get_offset_of_U24current_1() { return static_cast<int32_t>(offsetof(U3CCoUpdateU3Ec__Iterator0_t3541358793, ___U24current_1)); }
	inline RuntimeObject * get_U24current_1() const { return ___U24current_1; }
	inline RuntimeObject ** get_address_of_U24current_1() { return &___U24current_1; }
	inline void set_U24current_1(RuntimeObject * value)
	{
		___U24current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_1), value);
	}

	inline static int32_t get_offset_of_U24disposing_2() { return static_cast<int32_t>(offsetof(U3CCoUpdateU3Ec__Iterator0_t3541358793, ___U24disposing_2)); }
	inline bool get_U24disposing_2() const { return ___U24disposing_2; }
	inline bool* get_address_of_U24disposing_2() { return &___U24disposing_2; }
	inline void set_U24disposing_2(bool value)
	{
		___U24disposing_2 = value;
	}

	inline static int32_t get_offset_of_U24PC_3() { return static_cast<int32_t>(offsetof(U3CCoUpdateU3Ec__Iterator0_t3541358793, ___U24PC_3)); }
	inline int32_t get_U24PC_3() const { return ___U24PC_3; }
	inline int32_t* get_address_of_U24PC_3() { return &___U24PC_3; }
	inline void set_U24PC_3(int32_t value)
	{
		___U24PC_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CCOUPDATEU3EC__ITERATOR0_T3541358793_H
#ifndef VALUETYPE_T3507792607_H
#define VALUETYPE_T3507792607_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ValueType
struct  ValueType_t3507792607  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t3507792607_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t3507792607_marshaled_com
{
};
#endif // VALUETYPE_T3507792607_H
#ifndef PIVOTUTIL_T3576025374_H
#define PIVOTUTIL_T3576025374_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.PivotUtil
struct  PivotUtil_t3576025374  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PIVOTUTIL_T3576025374_H
#ifndef TIMEUTIL_T3155251811_H
#define TIMEUTIL_T3155251811_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.TimeUtil
struct  TimeUtil_t3155251811  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TIMEUTIL_T3155251811_H
#ifndef UTAGETOOLKIT_T285960976_H
#define UTAGETOOLKIT_T285960976_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.UtageToolKit
struct  UtageToolKit_t285960976  : public RuntimeObject
{
public:

public:
};

struct UtageToolKit_t285960976_StaticFields
{
public:
	// System.Globalization.CultureInfo Utage.UtageToolKit::cultureInfJp
	CultureInfo_t3500843524 * ___cultureInfJp_0;

public:
	inline static int32_t get_offset_of_cultureInfJp_0() { return static_cast<int32_t>(offsetof(UtageToolKit_t285960976_StaticFields, ___cultureInfJp_0)); }
	inline CultureInfo_t3500843524 * get_cultureInfJp_0() const { return ___cultureInfJp_0; }
	inline CultureInfo_t3500843524 ** get_address_of_cultureInfJp_0() { return &___cultureInfJp_0; }
	inline void set_cultureInfJp_0(CultureInfo_t3500843524 * value)
	{
		___cultureInfJp_0 = value;
		Il2CppCodeGenWriteBarrier((&___cultureInfJp_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UTAGETOOLKIT_T285960976_H
#ifndef U3CADDEVENTTRIGGERENTRYU3EC__ANONSTOREY0_T880254321_H
#define U3CADDEVENTTRIGGERENTRYU3EC__ANONSTOREY0_T880254321_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.UtageToolKit/<AddEventTriggerEntry>c__AnonStorey0
struct  U3CAddEventTriggerEntryU3Ec__AnonStorey0_t880254321  : public RuntimeObject
{
public:
	// UnityEngine.Events.UnityAction`1<UnityEngine.EventSystems.BaseEventData> Utage.UtageToolKit/<AddEventTriggerEntry>c__AnonStorey0::action
	UnityAction_1_t4047591376 * ___action_0;

public:
	inline static int32_t get_offset_of_action_0() { return static_cast<int32_t>(offsetof(U3CAddEventTriggerEntryU3Ec__AnonStorey0_t880254321, ___action_0)); }
	inline UnityAction_1_t4047591376 * get_action_0() const { return ___action_0; }
	inline UnityAction_1_t4047591376 ** get_address_of_action_0() { return &___action_0; }
	inline void set_action_0(UnityAction_1_t4047591376 * value)
	{
		___action_0 = value;
		Il2CppCodeGenWriteBarrier((&___action_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CADDEVENTTRIGGERENTRYU3EC__ANONSTOREY0_T880254321_H
#ifndef U3CCOWAITOPENU3EC__ITERATOR0_T2966467166_H
#define U3CCOWAITOPENU3EC__ITERATOR0_T2966467166_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UtageUguiMainGame/<CoWaitOpen>c__Iterator0
struct  U3CCoWaitOpenU3Ec__Iterator0_t2966467166  : public RuntimeObject
{
public:
	// UtageUguiMainGame UtageUguiMainGame/<CoWaitOpen>c__Iterator0::$this
	UtageUguiMainGame_t4178963699 * ___U24this_0;
	// System.Object UtageUguiMainGame/<CoWaitOpen>c__Iterator0::$current
	RuntimeObject * ___U24current_1;
	// System.Boolean UtageUguiMainGame/<CoWaitOpen>c__Iterator0::$disposing
	bool ___U24disposing_2;
	// System.Int32 UtageUguiMainGame/<CoWaitOpen>c__Iterator0::$PC
	int32_t ___U24PC_3;

public:
	inline static int32_t get_offset_of_U24this_0() { return static_cast<int32_t>(offsetof(U3CCoWaitOpenU3Ec__Iterator0_t2966467166, ___U24this_0)); }
	inline UtageUguiMainGame_t4178963699 * get_U24this_0() const { return ___U24this_0; }
	inline UtageUguiMainGame_t4178963699 ** get_address_of_U24this_0() { return &___U24this_0; }
	inline void set_U24this_0(UtageUguiMainGame_t4178963699 * value)
	{
		___U24this_0 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_0), value);
	}

	inline static int32_t get_offset_of_U24current_1() { return static_cast<int32_t>(offsetof(U3CCoWaitOpenU3Ec__Iterator0_t2966467166, ___U24current_1)); }
	inline RuntimeObject * get_U24current_1() const { return ___U24current_1; }
	inline RuntimeObject ** get_address_of_U24current_1() { return &___U24current_1; }
	inline void set_U24current_1(RuntimeObject * value)
	{
		___U24current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_1), value);
	}

	inline static int32_t get_offset_of_U24disposing_2() { return static_cast<int32_t>(offsetof(U3CCoWaitOpenU3Ec__Iterator0_t2966467166, ___U24disposing_2)); }
	inline bool get_U24disposing_2() const { return ___U24disposing_2; }
	inline bool* get_address_of_U24disposing_2() { return &___U24disposing_2; }
	inline void set_U24disposing_2(bool value)
	{
		___U24disposing_2 = value;
	}

	inline static int32_t get_offset_of_U24PC_3() { return static_cast<int32_t>(offsetof(U3CCoWaitOpenU3Ec__Iterator0_t2966467166, ___U24PC_3)); }
	inline int32_t get_U24PC_3() const { return ___U24PC_3; }
	inline int32_t* get_address_of_U24PC_3() { return &___U24PC_3; }
	inline void set_U24PC_3(int32_t value)
	{
		___U24PC_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CCOWAITOPENU3EC__ITERATOR0_T2966467166_H
#ifndef U3CCOPLAYMOBILEMOVIEU3EC__ITERATOR0_T154985798_H
#define U3CCOPLAYMOBILEMOVIEU3EC__ITERATOR0_T154985798_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.WrapperMoviePlayer/<CoPlayMobileMovie>c__Iterator0
struct  U3CCoPlayMobileMovieU3Ec__Iterator0_t154985798  : public RuntimeObject
{
public:
	// System.String Utage.WrapperMoviePlayer/<CoPlayMobileMovie>c__Iterator0::path
	String_t* ___path_0;
	// Utage.WrapperMoviePlayer Utage.WrapperMoviePlayer/<CoPlayMobileMovie>c__Iterator0::$this
	WrapperMoviePlayer_t3887243686 * ___U24this_1;
	// System.Object Utage.WrapperMoviePlayer/<CoPlayMobileMovie>c__Iterator0::$current
	RuntimeObject * ___U24current_2;
	// System.Boolean Utage.WrapperMoviePlayer/<CoPlayMobileMovie>c__Iterator0::$disposing
	bool ___U24disposing_3;
	// System.Int32 Utage.WrapperMoviePlayer/<CoPlayMobileMovie>c__Iterator0::$PC
	int32_t ___U24PC_4;

public:
	inline static int32_t get_offset_of_path_0() { return static_cast<int32_t>(offsetof(U3CCoPlayMobileMovieU3Ec__Iterator0_t154985798, ___path_0)); }
	inline String_t* get_path_0() const { return ___path_0; }
	inline String_t** get_address_of_path_0() { return &___path_0; }
	inline void set_path_0(String_t* value)
	{
		___path_0 = value;
		Il2CppCodeGenWriteBarrier((&___path_0), value);
	}

	inline static int32_t get_offset_of_U24this_1() { return static_cast<int32_t>(offsetof(U3CCoPlayMobileMovieU3Ec__Iterator0_t154985798, ___U24this_1)); }
	inline WrapperMoviePlayer_t3887243686 * get_U24this_1() const { return ___U24this_1; }
	inline WrapperMoviePlayer_t3887243686 ** get_address_of_U24this_1() { return &___U24this_1; }
	inline void set_U24this_1(WrapperMoviePlayer_t3887243686 * value)
	{
		___U24this_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_1), value);
	}

	inline static int32_t get_offset_of_U24current_2() { return static_cast<int32_t>(offsetof(U3CCoPlayMobileMovieU3Ec__Iterator0_t154985798, ___U24current_2)); }
	inline RuntimeObject * get_U24current_2() const { return ___U24current_2; }
	inline RuntimeObject ** get_address_of_U24current_2() { return &___U24current_2; }
	inline void set_U24current_2(RuntimeObject * value)
	{
		___U24current_2 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_2), value);
	}

	inline static int32_t get_offset_of_U24disposing_3() { return static_cast<int32_t>(offsetof(U3CCoPlayMobileMovieU3Ec__Iterator0_t154985798, ___U24disposing_3)); }
	inline bool get_U24disposing_3() const { return ___U24disposing_3; }
	inline bool* get_address_of_U24disposing_3() { return &___U24disposing_3; }
	inline void set_U24disposing_3(bool value)
	{
		___U24disposing_3 = value;
	}

	inline static int32_t get_offset_of_U24PC_4() { return static_cast<int32_t>(offsetof(U3CCoPlayMobileMovieU3Ec__Iterator0_t154985798, ___U24PC_4)); }
	inline int32_t get_U24PC_4() const { return ___U24PC_4; }
	inline int32_t* get_address_of_U24PC_4() { return &___U24PC_4; }
	inline void set_U24PC_4(int32_t value)
	{
		___U24PC_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CCOPLAYMOBILEMOVIEU3EC__ITERATOR0_T154985798_H
#ifndef U3CCOSAVEU3EC__ITERATOR2_T3798212420_H
#define U3CCOSAVEU3EC__ITERATOR2_T3798212420_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UtageUguiMainGame/<CoSave>c__Iterator2
struct  U3CCoSaveU3Ec__Iterator2_t3798212420  : public RuntimeObject
{
public:
	// UtageUguiMainGame UtageUguiMainGame/<CoSave>c__Iterator2::$this
	UtageUguiMainGame_t4178963699 * ___U24this_0;
	// System.Object UtageUguiMainGame/<CoSave>c__Iterator2::$current
	RuntimeObject * ___U24current_1;
	// System.Boolean UtageUguiMainGame/<CoSave>c__Iterator2::$disposing
	bool ___U24disposing_2;
	// System.Int32 UtageUguiMainGame/<CoSave>c__Iterator2::$PC
	int32_t ___U24PC_3;

public:
	inline static int32_t get_offset_of_U24this_0() { return static_cast<int32_t>(offsetof(U3CCoSaveU3Ec__Iterator2_t3798212420, ___U24this_0)); }
	inline UtageUguiMainGame_t4178963699 * get_U24this_0() const { return ___U24this_0; }
	inline UtageUguiMainGame_t4178963699 ** get_address_of_U24this_0() { return &___U24this_0; }
	inline void set_U24this_0(UtageUguiMainGame_t4178963699 * value)
	{
		___U24this_0 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_0), value);
	}

	inline static int32_t get_offset_of_U24current_1() { return static_cast<int32_t>(offsetof(U3CCoSaveU3Ec__Iterator2_t3798212420, ___U24current_1)); }
	inline RuntimeObject * get_U24current_1() const { return ___U24current_1; }
	inline RuntimeObject ** get_address_of_U24current_1() { return &___U24current_1; }
	inline void set_U24current_1(RuntimeObject * value)
	{
		___U24current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_1), value);
	}

	inline static int32_t get_offset_of_U24disposing_2() { return static_cast<int32_t>(offsetof(U3CCoSaveU3Ec__Iterator2_t3798212420, ___U24disposing_2)); }
	inline bool get_U24disposing_2() const { return ___U24disposing_2; }
	inline bool* get_address_of_U24disposing_2() { return &___U24disposing_2; }
	inline void set_U24disposing_2(bool value)
	{
		___U24disposing_2 = value;
	}

	inline static int32_t get_offset_of_U24PC_3() { return static_cast<int32_t>(offsetof(U3CCoSaveU3Ec__Iterator2_t3798212420, ___U24PC_3)); }
	inline int32_t get_U24PC_3() const { return ___U24PC_3; }
	inline int32_t* get_address_of_U24PC_3() { return &___U24PC_3; }
	inline void set_U24PC_3(int32_t value)
	{
		___U24PC_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CCOSAVEU3EC__ITERATOR2_T3798212420_H
#ifndef U3CINITU3EC__ANONSTOREY0_T168833639_H
#define U3CINITU3EC__ANONSTOREY0_T168833639_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UtageUguiCgGalleryItem/<Init>c__AnonStorey0
struct  U3CInitU3Ec__AnonStorey0_t168833639  : public RuntimeObject
{
public:
	// System.Action`1<UtageUguiCgGalleryItem> UtageUguiCgGalleryItem/<Init>c__AnonStorey0::ButtonClickedEvent
	Action_1_t1248543733 * ___ButtonClickedEvent_0;
	// UtageUguiCgGalleryItem UtageUguiCgGalleryItem/<Init>c__AnonStorey0::$this
	UtageUguiCgGalleryItem_t1446744351 * ___U24this_1;

public:
	inline static int32_t get_offset_of_ButtonClickedEvent_0() { return static_cast<int32_t>(offsetof(U3CInitU3Ec__AnonStorey0_t168833639, ___ButtonClickedEvent_0)); }
	inline Action_1_t1248543733 * get_ButtonClickedEvent_0() const { return ___ButtonClickedEvent_0; }
	inline Action_1_t1248543733 ** get_address_of_ButtonClickedEvent_0() { return &___ButtonClickedEvent_0; }
	inline void set_ButtonClickedEvent_0(Action_1_t1248543733 * value)
	{
		___ButtonClickedEvent_0 = value;
		Il2CppCodeGenWriteBarrier((&___ButtonClickedEvent_0), value);
	}

	inline static int32_t get_offset_of_U24this_1() { return static_cast<int32_t>(offsetof(U3CInitU3Ec__AnonStorey0_t168833639, ___U24this_1)); }
	inline UtageUguiCgGalleryItem_t1446744351 * get_U24this_1() const { return ___U24this_1; }
	inline UtageUguiCgGalleryItem_t1446744351 ** get_address_of_U24this_1() { return &___U24this_1; }
	inline void set_U24this_1(UtageUguiCgGalleryItem_t1446744351 * value)
	{
		___U24this_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CINITU3EC__ANONSTOREY0_T168833639_H
#ifndef U3CCOCAPTURESCREENU3EC__ITERATOR1_T4205168204_H
#define U3CCOCAPTURESCREENU3EC__ITERATOR1_T4205168204_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UtageUguiMainGame/<CoCaptureScreen>c__Iterator1
struct  U3CCoCaptureScreenU3Ec__Iterator1_t4205168204  : public RuntimeObject
{
public:
	// UtageUguiMainGame UtageUguiMainGame/<CoCaptureScreen>c__Iterator1::$this
	UtageUguiMainGame_t4178963699 * ___U24this_0;
	// System.Object UtageUguiMainGame/<CoCaptureScreen>c__Iterator1::$current
	RuntimeObject * ___U24current_1;
	// System.Boolean UtageUguiMainGame/<CoCaptureScreen>c__Iterator1::$disposing
	bool ___U24disposing_2;
	// System.Int32 UtageUguiMainGame/<CoCaptureScreen>c__Iterator1::$PC
	int32_t ___U24PC_3;

public:
	inline static int32_t get_offset_of_U24this_0() { return static_cast<int32_t>(offsetof(U3CCoCaptureScreenU3Ec__Iterator1_t4205168204, ___U24this_0)); }
	inline UtageUguiMainGame_t4178963699 * get_U24this_0() const { return ___U24this_0; }
	inline UtageUguiMainGame_t4178963699 ** get_address_of_U24this_0() { return &___U24this_0; }
	inline void set_U24this_0(UtageUguiMainGame_t4178963699 * value)
	{
		___U24this_0 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_0), value);
	}

	inline static int32_t get_offset_of_U24current_1() { return static_cast<int32_t>(offsetof(U3CCoCaptureScreenU3Ec__Iterator1_t4205168204, ___U24current_1)); }
	inline RuntimeObject * get_U24current_1() const { return ___U24current_1; }
	inline RuntimeObject ** get_address_of_U24current_1() { return &___U24current_1; }
	inline void set_U24current_1(RuntimeObject * value)
	{
		___U24current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_1), value);
	}

	inline static int32_t get_offset_of_U24disposing_2() { return static_cast<int32_t>(offsetof(U3CCoCaptureScreenU3Ec__Iterator1_t4205168204, ___U24disposing_2)); }
	inline bool get_U24disposing_2() const { return ___U24disposing_2; }
	inline bool* get_address_of_U24disposing_2() { return &___U24disposing_2; }
	inline void set_U24disposing_2(bool value)
	{
		___U24disposing_2 = value;
	}

	inline static int32_t get_offset_of_U24PC_3() { return static_cast<int32_t>(offsetof(U3CCoCaptureScreenU3Ec__Iterator1_t4205168204, ___U24PC_3)); }
	inline int32_t get_U24PC_3() const { return ___U24PC_3; }
	inline int32_t* get_address_of_U24PC_3() { return &___U24PC_3; }
	inline void set_U24PC_3(int32_t value)
	{
		___U24PC_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CCOCAPTURESCREENU3EC__ITERATOR1_T4205168204_H
#ifndef U3CCOWAITOPENU3EC__ITERATOR0_T771116017_H
#define U3CCOWAITOPENU3EC__ITERATOR0_T771116017_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UtageUguiCgGallery/<CoWaitOpen>c__Iterator0
struct  U3CCoWaitOpenU3Ec__Iterator0_t771116017  : public RuntimeObject
{
public:
	// UtageUguiCgGallery UtageUguiCgGallery/<CoWaitOpen>c__Iterator0::$this
	UtageUguiCgGallery_t1507549878 * ___U24this_0;
	// System.Object UtageUguiCgGallery/<CoWaitOpen>c__Iterator0::$current
	RuntimeObject * ___U24current_1;
	// System.Boolean UtageUguiCgGallery/<CoWaitOpen>c__Iterator0::$disposing
	bool ___U24disposing_2;
	// System.Int32 UtageUguiCgGallery/<CoWaitOpen>c__Iterator0::$PC
	int32_t ___U24PC_3;

public:
	inline static int32_t get_offset_of_U24this_0() { return static_cast<int32_t>(offsetof(U3CCoWaitOpenU3Ec__Iterator0_t771116017, ___U24this_0)); }
	inline UtageUguiCgGallery_t1507549878 * get_U24this_0() const { return ___U24this_0; }
	inline UtageUguiCgGallery_t1507549878 ** get_address_of_U24this_0() { return &___U24this_0; }
	inline void set_U24this_0(UtageUguiCgGallery_t1507549878 * value)
	{
		___U24this_0 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_0), value);
	}

	inline static int32_t get_offset_of_U24current_1() { return static_cast<int32_t>(offsetof(U3CCoWaitOpenU3Ec__Iterator0_t771116017, ___U24current_1)); }
	inline RuntimeObject * get_U24current_1() const { return ___U24current_1; }
	inline RuntimeObject ** get_address_of_U24current_1() { return &___U24current_1; }
	inline void set_U24current_1(RuntimeObject * value)
	{
		___U24current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_1), value);
	}

	inline static int32_t get_offset_of_U24disposing_2() { return static_cast<int32_t>(offsetof(U3CCoWaitOpenU3Ec__Iterator0_t771116017, ___U24disposing_2)); }
	inline bool get_U24disposing_2() const { return ___U24disposing_2; }
	inline bool* get_address_of_U24disposing_2() { return &___U24disposing_2; }
	inline void set_U24disposing_2(bool value)
	{
		___U24disposing_2 = value;
	}

	inline static int32_t get_offset_of_U24PC_3() { return static_cast<int32_t>(offsetof(U3CCoWaitOpenU3Ec__Iterator0_t771116017, ___U24PC_3)); }
	inline int32_t get_U24PC_3() const { return ___U24PC_3; }
	inline int32_t* get_address_of_U24PC_3() { return &___U24PC_3; }
	inline void set_U24PC_3(int32_t value)
	{
		___U24PC_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CCOWAITOPENU3EC__ITERATOR0_T771116017_H
#ifndef U3CCOUPDATELOADINGU3EC__ITERATOR0_T3542804446_H
#define U3CCOUPDATELOADINGU3EC__ITERATOR0_T3542804446_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UtageUguiLoadWait/<CoUpdateLoading>c__Iterator0
struct  U3CCoUpdateLoadingU3Ec__Iterator0_t3542804446  : public RuntimeObject
{
public:
	// System.Int32 UtageUguiLoadWait/<CoUpdateLoading>c__Iterator0::<maxCountDownLoad>__0
	int32_t ___U3CmaxCountDownLoadU3E__0_0;
	// System.Int32 UtageUguiLoadWait/<CoUpdateLoading>c__Iterator0::<countDownLoading>__0
	int32_t ___U3CcountDownLoadingU3E__0_1;
	// System.Int32 UtageUguiLoadWait/<CoUpdateLoading>c__Iterator0::<countDownLoaded>__1
	int32_t ___U3CcountDownLoadedU3E__1_2;
	// UtageUguiLoadWait UtageUguiLoadWait/<CoUpdateLoading>c__Iterator0::$this
	UtageUguiLoadWait_t319861767 * ___U24this_3;
	// System.Object UtageUguiLoadWait/<CoUpdateLoading>c__Iterator0::$current
	RuntimeObject * ___U24current_4;
	// System.Boolean UtageUguiLoadWait/<CoUpdateLoading>c__Iterator0::$disposing
	bool ___U24disposing_5;
	// System.Int32 UtageUguiLoadWait/<CoUpdateLoading>c__Iterator0::$PC
	int32_t ___U24PC_6;

public:
	inline static int32_t get_offset_of_U3CmaxCountDownLoadU3E__0_0() { return static_cast<int32_t>(offsetof(U3CCoUpdateLoadingU3Ec__Iterator0_t3542804446, ___U3CmaxCountDownLoadU3E__0_0)); }
	inline int32_t get_U3CmaxCountDownLoadU3E__0_0() const { return ___U3CmaxCountDownLoadU3E__0_0; }
	inline int32_t* get_address_of_U3CmaxCountDownLoadU3E__0_0() { return &___U3CmaxCountDownLoadU3E__0_0; }
	inline void set_U3CmaxCountDownLoadU3E__0_0(int32_t value)
	{
		___U3CmaxCountDownLoadU3E__0_0 = value;
	}

	inline static int32_t get_offset_of_U3CcountDownLoadingU3E__0_1() { return static_cast<int32_t>(offsetof(U3CCoUpdateLoadingU3Ec__Iterator0_t3542804446, ___U3CcountDownLoadingU3E__0_1)); }
	inline int32_t get_U3CcountDownLoadingU3E__0_1() const { return ___U3CcountDownLoadingU3E__0_1; }
	inline int32_t* get_address_of_U3CcountDownLoadingU3E__0_1() { return &___U3CcountDownLoadingU3E__0_1; }
	inline void set_U3CcountDownLoadingU3E__0_1(int32_t value)
	{
		___U3CcountDownLoadingU3E__0_1 = value;
	}

	inline static int32_t get_offset_of_U3CcountDownLoadedU3E__1_2() { return static_cast<int32_t>(offsetof(U3CCoUpdateLoadingU3Ec__Iterator0_t3542804446, ___U3CcountDownLoadedU3E__1_2)); }
	inline int32_t get_U3CcountDownLoadedU3E__1_2() const { return ___U3CcountDownLoadedU3E__1_2; }
	inline int32_t* get_address_of_U3CcountDownLoadedU3E__1_2() { return &___U3CcountDownLoadedU3E__1_2; }
	inline void set_U3CcountDownLoadedU3E__1_2(int32_t value)
	{
		___U3CcountDownLoadedU3E__1_2 = value;
	}

	inline static int32_t get_offset_of_U24this_3() { return static_cast<int32_t>(offsetof(U3CCoUpdateLoadingU3Ec__Iterator0_t3542804446, ___U24this_3)); }
	inline UtageUguiLoadWait_t319861767 * get_U24this_3() const { return ___U24this_3; }
	inline UtageUguiLoadWait_t319861767 ** get_address_of_U24this_3() { return &___U24this_3; }
	inline void set_U24this_3(UtageUguiLoadWait_t319861767 * value)
	{
		___U24this_3 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_3), value);
	}

	inline static int32_t get_offset_of_U24current_4() { return static_cast<int32_t>(offsetof(U3CCoUpdateLoadingU3Ec__Iterator0_t3542804446, ___U24current_4)); }
	inline RuntimeObject * get_U24current_4() const { return ___U24current_4; }
	inline RuntimeObject ** get_address_of_U24current_4() { return &___U24current_4; }
	inline void set_U24current_4(RuntimeObject * value)
	{
		___U24current_4 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_4), value);
	}

	inline static int32_t get_offset_of_U24disposing_5() { return static_cast<int32_t>(offsetof(U3CCoUpdateLoadingU3Ec__Iterator0_t3542804446, ___U24disposing_5)); }
	inline bool get_U24disposing_5() const { return ___U24disposing_5; }
	inline bool* get_address_of_U24disposing_5() { return &___U24disposing_5; }
	inline void set_U24disposing_5(bool value)
	{
		___U24disposing_5 = value;
	}

	inline static int32_t get_offset_of_U24PC_6() { return static_cast<int32_t>(offsetof(U3CCoUpdateLoadingU3Ec__Iterator0_t3542804446, ___U24PC_6)); }
	inline int32_t get_U24PC_6() const { return ___U24PC_6; }
	inline int32_t* get_address_of_U24PC_6() { return &___U24PC_6; }
	inline void set_U24PC_6(int32_t value)
	{
		___U24PC_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CCOUPDATELOADINGU3EC__ITERATOR0_T3542804446_H
#ifndef WRAPPERUNITYVERSION_T3346077438_H
#define WRAPPERUNITYVERSION_T3346077438_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.WrapperUnityVersion
struct  WrapperUnityVersion_t3346077438  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WRAPPERUNITYVERSION_T3346077438_H
#ifndef U3CINITU3EC__ANONSTOREY0_T1559879322_H
#define U3CINITU3EC__ANONSTOREY0_T1559879322_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UtageUguiSaveLoadItem/<Init>c__AnonStorey0
struct  U3CInitU3Ec__AnonStorey0_t1559879322  : public RuntimeObject
{
public:
	// System.Action`1<UtageUguiSaveLoadItem> UtageUguiSaveLoadItem/<Init>c__AnonStorey0::ButtonClickedEvent
	Action_1_t913746694 * ___ButtonClickedEvent_0;
	// UtageUguiSaveLoadItem UtageUguiSaveLoadItem/<Init>c__AnonStorey0::$this
	UtageUguiSaveLoadItem_t1111947312 * ___U24this_1;

public:
	inline static int32_t get_offset_of_ButtonClickedEvent_0() { return static_cast<int32_t>(offsetof(U3CInitU3Ec__AnonStorey0_t1559879322, ___ButtonClickedEvent_0)); }
	inline Action_1_t913746694 * get_ButtonClickedEvent_0() const { return ___ButtonClickedEvent_0; }
	inline Action_1_t913746694 ** get_address_of_ButtonClickedEvent_0() { return &___ButtonClickedEvent_0; }
	inline void set_ButtonClickedEvent_0(Action_1_t913746694 * value)
	{
		___ButtonClickedEvent_0 = value;
		Il2CppCodeGenWriteBarrier((&___ButtonClickedEvent_0), value);
	}

	inline static int32_t get_offset_of_U24this_1() { return static_cast<int32_t>(offsetof(U3CInitU3Ec__AnonStorey0_t1559879322, ___U24this_1)); }
	inline UtageUguiSaveLoadItem_t1111947312 * get_U24this_1() const { return ___U24this_1; }
	inline UtageUguiSaveLoadItem_t1111947312 ** get_address_of_U24this_1() { return &___U24this_1; }
	inline void set_U24this_1(UtageUguiSaveLoadItem_t1111947312 * value)
	{
		___U24this_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CINITU3EC__ANONSTOREY0_T1559879322_H
#ifndef VECTOR3_T2243707580_H
#define VECTOR3_T2243707580_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector3
struct  Vector3_t2243707580 
{
public:
	// System.Single UnityEngine.Vector3::x
	float ___x_1;
	// System.Single UnityEngine.Vector3::y
	float ___y_2;
	// System.Single UnityEngine.Vector3::z
	float ___z_3;

public:
	inline static int32_t get_offset_of_x_1() { return static_cast<int32_t>(offsetof(Vector3_t2243707580, ___x_1)); }
	inline float get_x_1() const { return ___x_1; }
	inline float* get_address_of_x_1() { return &___x_1; }
	inline void set_x_1(float value)
	{
		___x_1 = value;
	}

	inline static int32_t get_offset_of_y_2() { return static_cast<int32_t>(offsetof(Vector3_t2243707580, ___y_2)); }
	inline float get_y_2() const { return ___y_2; }
	inline float* get_address_of_y_2() { return &___y_2; }
	inline void set_y_2(float value)
	{
		___y_2 = value;
	}

	inline static int32_t get_offset_of_z_3() { return static_cast<int32_t>(offsetof(Vector3_t2243707580, ___z_3)); }
	inline float get_z_3() const { return ___z_3; }
	inline float* get_address_of_z_3() { return &___z_3; }
	inline void set_z_3(float value)
	{
		___z_3 = value;
	}
};

struct Vector3_t2243707580_StaticFields
{
public:
	// UnityEngine.Vector3 UnityEngine.Vector3::zeroVector
	Vector3_t2243707580  ___zeroVector_4;
	// UnityEngine.Vector3 UnityEngine.Vector3::oneVector
	Vector3_t2243707580  ___oneVector_5;
	// UnityEngine.Vector3 UnityEngine.Vector3::upVector
	Vector3_t2243707580  ___upVector_6;
	// UnityEngine.Vector3 UnityEngine.Vector3::downVector
	Vector3_t2243707580  ___downVector_7;
	// UnityEngine.Vector3 UnityEngine.Vector3::leftVector
	Vector3_t2243707580  ___leftVector_8;
	// UnityEngine.Vector3 UnityEngine.Vector3::rightVector
	Vector3_t2243707580  ___rightVector_9;
	// UnityEngine.Vector3 UnityEngine.Vector3::forwardVector
	Vector3_t2243707580  ___forwardVector_10;
	// UnityEngine.Vector3 UnityEngine.Vector3::backVector
	Vector3_t2243707580  ___backVector_11;
	// UnityEngine.Vector3 UnityEngine.Vector3::positiveInfinityVector
	Vector3_t2243707580  ___positiveInfinityVector_12;
	// UnityEngine.Vector3 UnityEngine.Vector3::negativeInfinityVector
	Vector3_t2243707580  ___negativeInfinityVector_13;

public:
	inline static int32_t get_offset_of_zeroVector_4() { return static_cast<int32_t>(offsetof(Vector3_t2243707580_StaticFields, ___zeroVector_4)); }
	inline Vector3_t2243707580  get_zeroVector_4() const { return ___zeroVector_4; }
	inline Vector3_t2243707580 * get_address_of_zeroVector_4() { return &___zeroVector_4; }
	inline void set_zeroVector_4(Vector3_t2243707580  value)
	{
		___zeroVector_4 = value;
	}

	inline static int32_t get_offset_of_oneVector_5() { return static_cast<int32_t>(offsetof(Vector3_t2243707580_StaticFields, ___oneVector_5)); }
	inline Vector3_t2243707580  get_oneVector_5() const { return ___oneVector_5; }
	inline Vector3_t2243707580 * get_address_of_oneVector_5() { return &___oneVector_5; }
	inline void set_oneVector_5(Vector3_t2243707580  value)
	{
		___oneVector_5 = value;
	}

	inline static int32_t get_offset_of_upVector_6() { return static_cast<int32_t>(offsetof(Vector3_t2243707580_StaticFields, ___upVector_6)); }
	inline Vector3_t2243707580  get_upVector_6() const { return ___upVector_6; }
	inline Vector3_t2243707580 * get_address_of_upVector_6() { return &___upVector_6; }
	inline void set_upVector_6(Vector3_t2243707580  value)
	{
		___upVector_6 = value;
	}

	inline static int32_t get_offset_of_downVector_7() { return static_cast<int32_t>(offsetof(Vector3_t2243707580_StaticFields, ___downVector_7)); }
	inline Vector3_t2243707580  get_downVector_7() const { return ___downVector_7; }
	inline Vector3_t2243707580 * get_address_of_downVector_7() { return &___downVector_7; }
	inline void set_downVector_7(Vector3_t2243707580  value)
	{
		___downVector_7 = value;
	}

	inline static int32_t get_offset_of_leftVector_8() { return static_cast<int32_t>(offsetof(Vector3_t2243707580_StaticFields, ___leftVector_8)); }
	inline Vector3_t2243707580  get_leftVector_8() const { return ___leftVector_8; }
	inline Vector3_t2243707580 * get_address_of_leftVector_8() { return &___leftVector_8; }
	inline void set_leftVector_8(Vector3_t2243707580  value)
	{
		___leftVector_8 = value;
	}

	inline static int32_t get_offset_of_rightVector_9() { return static_cast<int32_t>(offsetof(Vector3_t2243707580_StaticFields, ___rightVector_9)); }
	inline Vector3_t2243707580  get_rightVector_9() const { return ___rightVector_9; }
	inline Vector3_t2243707580 * get_address_of_rightVector_9() { return &___rightVector_9; }
	inline void set_rightVector_9(Vector3_t2243707580  value)
	{
		___rightVector_9 = value;
	}

	inline static int32_t get_offset_of_forwardVector_10() { return static_cast<int32_t>(offsetof(Vector3_t2243707580_StaticFields, ___forwardVector_10)); }
	inline Vector3_t2243707580  get_forwardVector_10() const { return ___forwardVector_10; }
	inline Vector3_t2243707580 * get_address_of_forwardVector_10() { return &___forwardVector_10; }
	inline void set_forwardVector_10(Vector3_t2243707580  value)
	{
		___forwardVector_10 = value;
	}

	inline static int32_t get_offset_of_backVector_11() { return static_cast<int32_t>(offsetof(Vector3_t2243707580_StaticFields, ___backVector_11)); }
	inline Vector3_t2243707580  get_backVector_11() const { return ___backVector_11; }
	inline Vector3_t2243707580 * get_address_of_backVector_11() { return &___backVector_11; }
	inline void set_backVector_11(Vector3_t2243707580  value)
	{
		___backVector_11 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_12() { return static_cast<int32_t>(offsetof(Vector3_t2243707580_StaticFields, ___positiveInfinityVector_12)); }
	inline Vector3_t2243707580  get_positiveInfinityVector_12() const { return ___positiveInfinityVector_12; }
	inline Vector3_t2243707580 * get_address_of_positiveInfinityVector_12() { return &___positiveInfinityVector_12; }
	inline void set_positiveInfinityVector_12(Vector3_t2243707580  value)
	{
		___positiveInfinityVector_12 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_13() { return static_cast<int32_t>(offsetof(Vector3_t2243707580_StaticFields, ___negativeInfinityVector_13)); }
	inline Vector3_t2243707580  get_negativeInfinityVector_13() const { return ___negativeInfinityVector_13; }
	inline Vector3_t2243707580 * get_address_of_negativeInfinityVector_13() { return &___negativeInfinityVector_13; }
	inline void set_negativeInfinityVector_13(Vector3_t2243707580  value)
	{
		___negativeInfinityVector_13 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR3_T2243707580_H
#ifndef COLOR_T2020392075_H
#define COLOR_T2020392075_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Color
struct  Color_t2020392075 
{
public:
	// System.Single UnityEngine.Color::r
	float ___r_0;
	// System.Single UnityEngine.Color::g
	float ___g_1;
	// System.Single UnityEngine.Color::b
	float ___b_2;
	// System.Single UnityEngine.Color::a
	float ___a_3;

public:
	inline static int32_t get_offset_of_r_0() { return static_cast<int32_t>(offsetof(Color_t2020392075, ___r_0)); }
	inline float get_r_0() const { return ___r_0; }
	inline float* get_address_of_r_0() { return &___r_0; }
	inline void set_r_0(float value)
	{
		___r_0 = value;
	}

	inline static int32_t get_offset_of_g_1() { return static_cast<int32_t>(offsetof(Color_t2020392075, ___g_1)); }
	inline float get_g_1() const { return ___g_1; }
	inline float* get_address_of_g_1() { return &___g_1; }
	inline void set_g_1(float value)
	{
		___g_1 = value;
	}

	inline static int32_t get_offset_of_b_2() { return static_cast<int32_t>(offsetof(Color_t2020392075, ___b_2)); }
	inline float get_b_2() const { return ___b_2; }
	inline float* get_address_of_b_2() { return &___b_2; }
	inline void set_b_2(float value)
	{
		___b_2 = value;
	}

	inline static int32_t get_offset_of_a_3() { return static_cast<int32_t>(offsetof(Color_t2020392075, ___a_3)); }
	inline float get_a_3() const { return ___a_3; }
	inline float* get_address_of_a_3() { return &___a_3; }
	inline void set_a_3(float value)
	{
		___a_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLOR_T2020392075_H
#ifndef INTPTR_T_H
#define INTPTR_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTPTR_T_H
#ifndef ENUM_T2459695545_H
#define ENUM_T2459695545_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Enum
struct  Enum_t2459695545  : public ValueType_t3507792607
{
public:

public:
};

struct Enum_t2459695545_StaticFields
{
public:
	// System.Char[] System.Enum::split_char
	CharU5BU5D_t1328083999* ___split_char_0;

public:
	inline static int32_t get_offset_of_split_char_0() { return static_cast<int32_t>(offsetof(Enum_t2459695545_StaticFields, ___split_char_0)); }
	inline CharU5BU5D_t1328083999* get_split_char_0() const { return ___split_char_0; }
	inline CharU5BU5D_t1328083999** get_address_of_split_char_0() { return &___split_char_0; }
	inline void set_split_char_0(CharU5BU5D_t1328083999* value)
	{
		___split_char_0 = value;
		Il2CppCodeGenWriteBarrier((&___split_char_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t2459695545_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t2459695545_marshaled_com
{
};
#endif // ENUM_T2459695545_H
#ifndef OBJECT_T1021602117_H
#define OBJECT_T1021602117_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Object
struct  Object_t1021602117  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Object::m_CachedPtr
	intptr_t ___m_CachedPtr_0;

public:
	inline static int32_t get_offset_of_m_CachedPtr_0() { return static_cast<int32_t>(offsetof(Object_t1021602117, ___m_CachedPtr_0)); }
	inline intptr_t get_m_CachedPtr_0() const { return ___m_CachedPtr_0; }
	inline intptr_t* get_address_of_m_CachedPtr_0() { return &___m_CachedPtr_0; }
	inline void set_m_CachedPtr_0(intptr_t value)
	{
		___m_CachedPtr_0 = value;
	}
};

struct Object_t1021602117_StaticFields
{
public:
	// System.Int32 UnityEngine.Object::OffsetOfInstanceIDInCPlusPlusObject
	int32_t ___OffsetOfInstanceIDInCPlusPlusObject_1;

public:
	inline static int32_t get_offset_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return static_cast<int32_t>(offsetof(Object_t1021602117_StaticFields, ___OffsetOfInstanceIDInCPlusPlusObject_1)); }
	inline int32_t get_OffsetOfInstanceIDInCPlusPlusObject_1() const { return ___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline int32_t* get_address_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return &___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline void set_OffsetOfInstanceIDInCPlusPlusObject_1(int32_t value)
	{
		___OffsetOfInstanceIDInCPlusPlusObject_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.Object
struct Object_t1021602117_marshaled_pinvoke
{
	intptr_t ___m_CachedPtr_0;
};
// Native definition for COM marshalling of UnityEngine.Object
struct Object_t1021602117_marshaled_com
{
	intptr_t ___m_CachedPtr_0;
};
#endif // OBJECT_T1021602117_H
#ifndef LIPSYNCHMODE_T1462187095_H
#define LIPSYNCHMODE_T1462187095_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.LipSynchMode
struct  LipSynchMode_t1462187095 
{
public:
	// System.Int32 Utage.LipSynchMode::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(LipSynchMode_t1462187095, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LIPSYNCHMODE_T1462187095_H
#ifndef LIPSYNCHTYPE_T1469092900_H
#define LIPSYNCHTYPE_T1469092900_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.LipSynchType
struct  LipSynchType_t1469092900 
{
public:
	// System.Int32 Utage.LipSynchType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(LipSynchType_t1469092900, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LIPSYNCHTYPE_T1469092900_H
#ifndef STATUS_T2577383376_H
#define STATUS_T2577383376_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.UguiView/Status
struct  Status_t2577383376 
{
public:
	// System.Int32 Utage.UguiView/Status::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(Status_t2577383376, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STATUS_T2577383376_H
#ifndef BOOTTYPE_T3158257586_H
#define BOOTTYPE_T3158257586_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UtageUguiMainGame/BootType
struct  BootType_t3158257586 
{
public:
	// System.Int32 UtageUguiMainGame/BootType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(BootType_t3158257586, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BOOTTYPE_T3158257586_H
#ifndef TYPE_T2484462392_H
#define TYPE_T2484462392_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UtageUguiLoadWait/Type
struct  Type_t2484462392 
{
public:
	// System.Int32 UtageUguiLoadWait/Type::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(Type_t2484462392, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TYPE_T2484462392_H
#ifndef STATE_T3198281263_H
#define STATE_T3198281263_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UtageUguiLoadWait/State
struct  State_t3198281263 
{
public:
	// System.Int32 UtageUguiLoadWait/State::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(State_t3198281263, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STATE_T3198281263_H
#ifndef PIVOT_T1385159954_H
#define PIVOT_T1385159954_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.Pivot
struct  Pivot_t1385159954 
{
public:
	// System.Int32 Utage.Pivot::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(Pivot_t1385159954, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PIVOT_T1385159954_H
#ifndef COMPONENT_T3819376471_H
#define COMPONENT_T3819376471_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Component
struct  Component_t3819376471  : public Object_t1021602117
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPONENT_T3819376471_H
#ifndef BEHAVIOUR_T955675639_H
#define BEHAVIOUR_T955675639_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Behaviour
struct  Behaviour_t955675639  : public Component_t3819376471
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BEHAVIOUR_T955675639_H
#ifndef MONOBEHAVIOUR_T1158329972_H
#define MONOBEHAVIOUR_T1158329972_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.MonoBehaviour
struct  MonoBehaviour_t1158329972  : public Behaviour_t955675639
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MONOBEHAVIOUR_T1158329972_H
#ifndef UTAGEUGUICGGALLERYITEM_T1446744351_H
#define UTAGEUGUICGGALLERYITEM_T1446744351_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UtageUguiCgGalleryItem
struct  UtageUguiCgGalleryItem_t1446744351  : public MonoBehaviour_t1158329972
{
public:
	// Utage.AdvUguiLoadGraphicFile UtageUguiCgGalleryItem::texture
	AdvUguiLoadGraphicFile_t1474676621 * ___texture_2;
	// UnityEngine.UI.Text UtageUguiCgGalleryItem::count
	Text_t356221433 * ___count_3;
	// Utage.AdvCgGalleryData UtageUguiCgGalleryItem::data
	AdvCgGalleryData_t4276096361 * ___data_4;

public:
	inline static int32_t get_offset_of_texture_2() { return static_cast<int32_t>(offsetof(UtageUguiCgGalleryItem_t1446744351, ___texture_2)); }
	inline AdvUguiLoadGraphicFile_t1474676621 * get_texture_2() const { return ___texture_2; }
	inline AdvUguiLoadGraphicFile_t1474676621 ** get_address_of_texture_2() { return &___texture_2; }
	inline void set_texture_2(AdvUguiLoadGraphicFile_t1474676621 * value)
	{
		___texture_2 = value;
		Il2CppCodeGenWriteBarrier((&___texture_2), value);
	}

	inline static int32_t get_offset_of_count_3() { return static_cast<int32_t>(offsetof(UtageUguiCgGalleryItem_t1446744351, ___count_3)); }
	inline Text_t356221433 * get_count_3() const { return ___count_3; }
	inline Text_t356221433 ** get_address_of_count_3() { return &___count_3; }
	inline void set_count_3(Text_t356221433 * value)
	{
		___count_3 = value;
		Il2CppCodeGenWriteBarrier((&___count_3), value);
	}

	inline static int32_t get_offset_of_data_4() { return static_cast<int32_t>(offsetof(UtageUguiCgGalleryItem_t1446744351, ___data_4)); }
	inline AdvCgGalleryData_t4276096361 * get_data_4() const { return ___data_4; }
	inline AdvCgGalleryData_t4276096361 ** get_address_of_data_4() { return &___data_4; }
	inline void set_data_4(AdvCgGalleryData_t4276096361 * value)
	{
		___data_4 = value;
		Il2CppCodeGenWriteBarrier((&___data_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UTAGEUGUICGGALLERYITEM_T1446744351_H
#ifndef UTAGEUGUISOUNDROOMITEM_T3977840991_H
#define UTAGEUGUISOUNDROOMITEM_T3977840991_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UtageUguiSoundRoomItem
struct  UtageUguiSoundRoomItem_t3977840991  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.UI.Text UtageUguiSoundRoomItem::title
	Text_t356221433 * ___title_2;
	// Utage.AdvSoundSettingData UtageUguiSoundRoomItem::data
	AdvSoundSettingData_t3135391222 * ___data_3;

public:
	inline static int32_t get_offset_of_title_2() { return static_cast<int32_t>(offsetof(UtageUguiSoundRoomItem_t3977840991, ___title_2)); }
	inline Text_t356221433 * get_title_2() const { return ___title_2; }
	inline Text_t356221433 ** get_address_of_title_2() { return &___title_2; }
	inline void set_title_2(Text_t356221433 * value)
	{
		___title_2 = value;
		Il2CppCodeGenWriteBarrier((&___title_2), value);
	}

	inline static int32_t get_offset_of_data_3() { return static_cast<int32_t>(offsetof(UtageUguiSoundRoomItem_t3977840991, ___data_3)); }
	inline AdvSoundSettingData_t3135391222 * get_data_3() const { return ___data_3; }
	inline AdvSoundSettingData_t3135391222 ** get_address_of_data_3() { return &___data_3; }
	inline void set_data_3(AdvSoundSettingData_t3135391222 * value)
	{
		___data_3 = value;
		Il2CppCodeGenWriteBarrier((&___data_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UTAGEUGUISOUNDROOMITEM_T3977840991_H
#ifndef UTAGEUGUISCENEGALLERYITEM_T228413713_H
#define UTAGEUGUISCENEGALLERYITEM_T228413713_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UtageUguiSceneGalleryItem
struct  UtageUguiSceneGalleryItem_t228413713  : public MonoBehaviour_t1158329972
{
public:
	// Utage.AdvUguiLoadGraphicFile UtageUguiSceneGalleryItem::texture
	AdvUguiLoadGraphicFile_t1474676621 * ___texture_2;
	// UnityEngine.UI.Text UtageUguiSceneGalleryItem::title
	Text_t356221433 * ___title_3;
	// Utage.AdvSceneGallerySettingData UtageUguiSceneGalleryItem::data
	AdvSceneGallerySettingData_t1280603049 * ___data_4;

public:
	inline static int32_t get_offset_of_texture_2() { return static_cast<int32_t>(offsetof(UtageUguiSceneGalleryItem_t228413713, ___texture_2)); }
	inline AdvUguiLoadGraphicFile_t1474676621 * get_texture_2() const { return ___texture_2; }
	inline AdvUguiLoadGraphicFile_t1474676621 ** get_address_of_texture_2() { return &___texture_2; }
	inline void set_texture_2(AdvUguiLoadGraphicFile_t1474676621 * value)
	{
		___texture_2 = value;
		Il2CppCodeGenWriteBarrier((&___texture_2), value);
	}

	inline static int32_t get_offset_of_title_3() { return static_cast<int32_t>(offsetof(UtageUguiSceneGalleryItem_t228413713, ___title_3)); }
	inline Text_t356221433 * get_title_3() const { return ___title_3; }
	inline Text_t356221433 ** get_address_of_title_3() { return &___title_3; }
	inline void set_title_3(Text_t356221433 * value)
	{
		___title_3 = value;
		Il2CppCodeGenWriteBarrier((&___title_3), value);
	}

	inline static int32_t get_offset_of_data_4() { return static_cast<int32_t>(offsetof(UtageUguiSceneGalleryItem_t228413713, ___data_4)); }
	inline AdvSceneGallerySettingData_t1280603049 * get_data_4() const { return ___data_4; }
	inline AdvSceneGallerySettingData_t1280603049 ** get_address_of_data_4() { return &___data_4; }
	inline void set_data_4(AdvSceneGallerySettingData_t1280603049 * value)
	{
		___data_4 = value;
		Il2CppCodeGenWriteBarrier((&___data_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UTAGEUGUISCENEGALLERYITEM_T228413713_H
#ifndef UGUIVIEW_T2174908005_H
#define UGUIVIEW_T2174908005_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.UguiView
struct  UguiView_t2174908005  : public MonoBehaviour_t1158329972
{
public:
	// Utage.UguiView Utage.UguiView::prevView
	UguiView_t2174908005 * ___prevView_2;
	// UnityEngine.AudioClip Utage.UguiView::bgm
	AudioClip_t1932558630 * ___bgm_3;
	// System.Boolean Utage.UguiView::isStopBgmIfNoneBgm
	bool ___isStopBgmIfNoneBgm_4;
	// UnityEngine.Events.UnityEvent Utage.UguiView::onOpen
	UnityEvent_t408735097 * ___onOpen_5;
	// UnityEngine.Events.UnityEvent Utage.UguiView::onClose
	UnityEvent_t408735097 * ___onClose_6;
	// UnityEngine.CanvasGroup Utage.UguiView::canvasGroup
	CanvasGroup_t3296560743 * ___canvasGroup_7;
	// Utage.UguiView/Status Utage.UguiView::status
	int32_t ___status_8;
	// System.Boolean Utage.UguiView::storedCanvasGroupInteractable
	bool ___storedCanvasGroupInteractable_9;
	// System.Boolean Utage.UguiView::storedCanvasGroupBlocksRaycasts
	bool ___storedCanvasGroupBlocksRaycasts_10;
	// System.Boolean Utage.UguiView::isStoredCanvasGroupInfo
	bool ___isStoredCanvasGroupInfo_11;

public:
	inline static int32_t get_offset_of_prevView_2() { return static_cast<int32_t>(offsetof(UguiView_t2174908005, ___prevView_2)); }
	inline UguiView_t2174908005 * get_prevView_2() const { return ___prevView_2; }
	inline UguiView_t2174908005 ** get_address_of_prevView_2() { return &___prevView_2; }
	inline void set_prevView_2(UguiView_t2174908005 * value)
	{
		___prevView_2 = value;
		Il2CppCodeGenWriteBarrier((&___prevView_2), value);
	}

	inline static int32_t get_offset_of_bgm_3() { return static_cast<int32_t>(offsetof(UguiView_t2174908005, ___bgm_3)); }
	inline AudioClip_t1932558630 * get_bgm_3() const { return ___bgm_3; }
	inline AudioClip_t1932558630 ** get_address_of_bgm_3() { return &___bgm_3; }
	inline void set_bgm_3(AudioClip_t1932558630 * value)
	{
		___bgm_3 = value;
		Il2CppCodeGenWriteBarrier((&___bgm_3), value);
	}

	inline static int32_t get_offset_of_isStopBgmIfNoneBgm_4() { return static_cast<int32_t>(offsetof(UguiView_t2174908005, ___isStopBgmIfNoneBgm_4)); }
	inline bool get_isStopBgmIfNoneBgm_4() const { return ___isStopBgmIfNoneBgm_4; }
	inline bool* get_address_of_isStopBgmIfNoneBgm_4() { return &___isStopBgmIfNoneBgm_4; }
	inline void set_isStopBgmIfNoneBgm_4(bool value)
	{
		___isStopBgmIfNoneBgm_4 = value;
	}

	inline static int32_t get_offset_of_onOpen_5() { return static_cast<int32_t>(offsetof(UguiView_t2174908005, ___onOpen_5)); }
	inline UnityEvent_t408735097 * get_onOpen_5() const { return ___onOpen_5; }
	inline UnityEvent_t408735097 ** get_address_of_onOpen_5() { return &___onOpen_5; }
	inline void set_onOpen_5(UnityEvent_t408735097 * value)
	{
		___onOpen_5 = value;
		Il2CppCodeGenWriteBarrier((&___onOpen_5), value);
	}

	inline static int32_t get_offset_of_onClose_6() { return static_cast<int32_t>(offsetof(UguiView_t2174908005, ___onClose_6)); }
	inline UnityEvent_t408735097 * get_onClose_6() const { return ___onClose_6; }
	inline UnityEvent_t408735097 ** get_address_of_onClose_6() { return &___onClose_6; }
	inline void set_onClose_6(UnityEvent_t408735097 * value)
	{
		___onClose_6 = value;
		Il2CppCodeGenWriteBarrier((&___onClose_6), value);
	}

	inline static int32_t get_offset_of_canvasGroup_7() { return static_cast<int32_t>(offsetof(UguiView_t2174908005, ___canvasGroup_7)); }
	inline CanvasGroup_t3296560743 * get_canvasGroup_7() const { return ___canvasGroup_7; }
	inline CanvasGroup_t3296560743 ** get_address_of_canvasGroup_7() { return &___canvasGroup_7; }
	inline void set_canvasGroup_7(CanvasGroup_t3296560743 * value)
	{
		___canvasGroup_7 = value;
		Il2CppCodeGenWriteBarrier((&___canvasGroup_7), value);
	}

	inline static int32_t get_offset_of_status_8() { return static_cast<int32_t>(offsetof(UguiView_t2174908005, ___status_8)); }
	inline int32_t get_status_8() const { return ___status_8; }
	inline int32_t* get_address_of_status_8() { return &___status_8; }
	inline void set_status_8(int32_t value)
	{
		___status_8 = value;
	}

	inline static int32_t get_offset_of_storedCanvasGroupInteractable_9() { return static_cast<int32_t>(offsetof(UguiView_t2174908005, ___storedCanvasGroupInteractable_9)); }
	inline bool get_storedCanvasGroupInteractable_9() const { return ___storedCanvasGroupInteractable_9; }
	inline bool* get_address_of_storedCanvasGroupInteractable_9() { return &___storedCanvasGroupInteractable_9; }
	inline void set_storedCanvasGroupInteractable_9(bool value)
	{
		___storedCanvasGroupInteractable_9 = value;
	}

	inline static int32_t get_offset_of_storedCanvasGroupBlocksRaycasts_10() { return static_cast<int32_t>(offsetof(UguiView_t2174908005, ___storedCanvasGroupBlocksRaycasts_10)); }
	inline bool get_storedCanvasGroupBlocksRaycasts_10() const { return ___storedCanvasGroupBlocksRaycasts_10; }
	inline bool* get_address_of_storedCanvasGroupBlocksRaycasts_10() { return &___storedCanvasGroupBlocksRaycasts_10; }
	inline void set_storedCanvasGroupBlocksRaycasts_10(bool value)
	{
		___storedCanvasGroupBlocksRaycasts_10 = value;
	}

	inline static int32_t get_offset_of_isStoredCanvasGroupInfo_11() { return static_cast<int32_t>(offsetof(UguiView_t2174908005, ___isStoredCanvasGroupInfo_11)); }
	inline bool get_isStoredCanvasGroupInfo_11() const { return ___isStoredCanvasGroupInfo_11; }
	inline bool* get_address_of_isStoredCanvasGroupInfo_11() { return &___isStoredCanvasGroupInfo_11; }
	inline void set_isStoredCanvasGroupInfo_11(bool value)
	{
		___isStoredCanvasGroupInfo_11 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UGUIVIEW_T2174908005_H
#ifndef LIPSYNCHBASE_T2376160345_H
#define LIPSYNCHBASE_T2376160345_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.LipSynchBase
struct  LipSynchBase_t2376160345  : public MonoBehaviour_t1158329972
{
public:
	// Utage.LipSynchType Utage.LipSynchBase::type
	int32_t ___type_2;
	// System.Boolean Utage.LipSynchBase::<EnableTextLipSync>k__BackingField
	bool ___U3CEnableTextLipSyncU3Ek__BackingField_3;
	// Utage.LipSynchMode Utage.LipSynchBase::<LipSynchMode>k__BackingField
	int32_t ___U3CLipSynchModeU3Ek__BackingField_4;
	// Utage.LipSynchEvent Utage.LipSynchBase::OnCheckTextLipSync
	LipSynchEvent_t825925384 * ___OnCheckTextLipSync_5;
	// System.String Utage.LipSynchBase::characterLabel
	String_t* ___characterLabel_6;
	// System.Boolean Utage.LipSynchBase::<IsEnable>k__BackingField
	bool ___U3CIsEnableU3Ek__BackingField_7;
	// System.Boolean Utage.LipSynchBase::<IsPlaying>k__BackingField
	bool ___U3CIsPlayingU3Ek__BackingField_8;

public:
	inline static int32_t get_offset_of_type_2() { return static_cast<int32_t>(offsetof(LipSynchBase_t2376160345, ___type_2)); }
	inline int32_t get_type_2() const { return ___type_2; }
	inline int32_t* get_address_of_type_2() { return &___type_2; }
	inline void set_type_2(int32_t value)
	{
		___type_2 = value;
	}

	inline static int32_t get_offset_of_U3CEnableTextLipSyncU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(LipSynchBase_t2376160345, ___U3CEnableTextLipSyncU3Ek__BackingField_3)); }
	inline bool get_U3CEnableTextLipSyncU3Ek__BackingField_3() const { return ___U3CEnableTextLipSyncU3Ek__BackingField_3; }
	inline bool* get_address_of_U3CEnableTextLipSyncU3Ek__BackingField_3() { return &___U3CEnableTextLipSyncU3Ek__BackingField_3; }
	inline void set_U3CEnableTextLipSyncU3Ek__BackingField_3(bool value)
	{
		___U3CEnableTextLipSyncU3Ek__BackingField_3 = value;
	}

	inline static int32_t get_offset_of_U3CLipSynchModeU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(LipSynchBase_t2376160345, ___U3CLipSynchModeU3Ek__BackingField_4)); }
	inline int32_t get_U3CLipSynchModeU3Ek__BackingField_4() const { return ___U3CLipSynchModeU3Ek__BackingField_4; }
	inline int32_t* get_address_of_U3CLipSynchModeU3Ek__BackingField_4() { return &___U3CLipSynchModeU3Ek__BackingField_4; }
	inline void set_U3CLipSynchModeU3Ek__BackingField_4(int32_t value)
	{
		___U3CLipSynchModeU3Ek__BackingField_4 = value;
	}

	inline static int32_t get_offset_of_OnCheckTextLipSync_5() { return static_cast<int32_t>(offsetof(LipSynchBase_t2376160345, ___OnCheckTextLipSync_5)); }
	inline LipSynchEvent_t825925384 * get_OnCheckTextLipSync_5() const { return ___OnCheckTextLipSync_5; }
	inline LipSynchEvent_t825925384 ** get_address_of_OnCheckTextLipSync_5() { return &___OnCheckTextLipSync_5; }
	inline void set_OnCheckTextLipSync_5(LipSynchEvent_t825925384 * value)
	{
		___OnCheckTextLipSync_5 = value;
		Il2CppCodeGenWriteBarrier((&___OnCheckTextLipSync_5), value);
	}

	inline static int32_t get_offset_of_characterLabel_6() { return static_cast<int32_t>(offsetof(LipSynchBase_t2376160345, ___characterLabel_6)); }
	inline String_t* get_characterLabel_6() const { return ___characterLabel_6; }
	inline String_t** get_address_of_characterLabel_6() { return &___characterLabel_6; }
	inline void set_characterLabel_6(String_t* value)
	{
		___characterLabel_6 = value;
		Il2CppCodeGenWriteBarrier((&___characterLabel_6), value);
	}

	inline static int32_t get_offset_of_U3CIsEnableU3Ek__BackingField_7() { return static_cast<int32_t>(offsetof(LipSynchBase_t2376160345, ___U3CIsEnableU3Ek__BackingField_7)); }
	inline bool get_U3CIsEnableU3Ek__BackingField_7() const { return ___U3CIsEnableU3Ek__BackingField_7; }
	inline bool* get_address_of_U3CIsEnableU3Ek__BackingField_7() { return &___U3CIsEnableU3Ek__BackingField_7; }
	inline void set_U3CIsEnableU3Ek__BackingField_7(bool value)
	{
		___U3CIsEnableU3Ek__BackingField_7 = value;
	}

	inline static int32_t get_offset_of_U3CIsPlayingU3Ek__BackingField_8() { return static_cast<int32_t>(offsetof(LipSynchBase_t2376160345, ___U3CIsPlayingU3Ek__BackingField_8)); }
	inline bool get_U3CIsPlayingU3Ek__BackingField_8() const { return ___U3CIsPlayingU3Ek__BackingField_8; }
	inline bool* get_address_of_U3CIsPlayingU3Ek__BackingField_8() { return &___U3CIsPlayingU3Ek__BackingField_8; }
	inline void set_U3CIsPlayingU3Ek__BackingField_8(bool value)
	{
		___U3CIsPlayingU3Ek__BackingField_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LIPSYNCHBASE_T2376160345_H
#ifndef WRAPPERMOVIEPLAYER_T3887243686_H
#define WRAPPERMOVIEPLAYER_T3887243686_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.WrapperMoviePlayer
struct  WrapperMoviePlayer_t3887243686  : public MonoBehaviour_t1158329972
{
public:
	// System.Boolean Utage.WrapperMoviePlayer::isPlaying
	bool ___isPlaying_3;
	// System.Boolean Utage.WrapperMoviePlayer::cancel
	bool ___cancel_4;
	// UnityEngine.Color Utage.WrapperMoviePlayer::bgColor
	Color_t2020392075  ___bgColor_5;
	// System.Boolean Utage.WrapperMoviePlayer::ignoreCancel
	bool ___ignoreCancel_6;
	// System.Single Utage.WrapperMoviePlayer::cancelFadeTime
	float ___cancelFadeTime_7;
	// UnityEngine.GameObject Utage.WrapperMoviePlayer::renderTarget
	GameObject_t1756533147 * ___renderTarget_8;
	// System.Boolean Utage.WrapperMoviePlayer::overrideRootDirectory
	bool ___overrideRootDirectory_9;
	// System.String Utage.WrapperMoviePlayer::rootDirectory
	String_t* ___rootDirectory_10;

public:
	inline static int32_t get_offset_of_isPlaying_3() { return static_cast<int32_t>(offsetof(WrapperMoviePlayer_t3887243686, ___isPlaying_3)); }
	inline bool get_isPlaying_3() const { return ___isPlaying_3; }
	inline bool* get_address_of_isPlaying_3() { return &___isPlaying_3; }
	inline void set_isPlaying_3(bool value)
	{
		___isPlaying_3 = value;
	}

	inline static int32_t get_offset_of_cancel_4() { return static_cast<int32_t>(offsetof(WrapperMoviePlayer_t3887243686, ___cancel_4)); }
	inline bool get_cancel_4() const { return ___cancel_4; }
	inline bool* get_address_of_cancel_4() { return &___cancel_4; }
	inline void set_cancel_4(bool value)
	{
		___cancel_4 = value;
	}

	inline static int32_t get_offset_of_bgColor_5() { return static_cast<int32_t>(offsetof(WrapperMoviePlayer_t3887243686, ___bgColor_5)); }
	inline Color_t2020392075  get_bgColor_5() const { return ___bgColor_5; }
	inline Color_t2020392075 * get_address_of_bgColor_5() { return &___bgColor_5; }
	inline void set_bgColor_5(Color_t2020392075  value)
	{
		___bgColor_5 = value;
	}

	inline static int32_t get_offset_of_ignoreCancel_6() { return static_cast<int32_t>(offsetof(WrapperMoviePlayer_t3887243686, ___ignoreCancel_6)); }
	inline bool get_ignoreCancel_6() const { return ___ignoreCancel_6; }
	inline bool* get_address_of_ignoreCancel_6() { return &___ignoreCancel_6; }
	inline void set_ignoreCancel_6(bool value)
	{
		___ignoreCancel_6 = value;
	}

	inline static int32_t get_offset_of_cancelFadeTime_7() { return static_cast<int32_t>(offsetof(WrapperMoviePlayer_t3887243686, ___cancelFadeTime_7)); }
	inline float get_cancelFadeTime_7() const { return ___cancelFadeTime_7; }
	inline float* get_address_of_cancelFadeTime_7() { return &___cancelFadeTime_7; }
	inline void set_cancelFadeTime_7(float value)
	{
		___cancelFadeTime_7 = value;
	}

	inline static int32_t get_offset_of_renderTarget_8() { return static_cast<int32_t>(offsetof(WrapperMoviePlayer_t3887243686, ___renderTarget_8)); }
	inline GameObject_t1756533147 * get_renderTarget_8() const { return ___renderTarget_8; }
	inline GameObject_t1756533147 ** get_address_of_renderTarget_8() { return &___renderTarget_8; }
	inline void set_renderTarget_8(GameObject_t1756533147 * value)
	{
		___renderTarget_8 = value;
		Il2CppCodeGenWriteBarrier((&___renderTarget_8), value);
	}

	inline static int32_t get_offset_of_overrideRootDirectory_9() { return static_cast<int32_t>(offsetof(WrapperMoviePlayer_t3887243686, ___overrideRootDirectory_9)); }
	inline bool get_overrideRootDirectory_9() const { return ___overrideRootDirectory_9; }
	inline bool* get_address_of_overrideRootDirectory_9() { return &___overrideRootDirectory_9; }
	inline void set_overrideRootDirectory_9(bool value)
	{
		___overrideRootDirectory_9 = value;
	}

	inline static int32_t get_offset_of_rootDirectory_10() { return static_cast<int32_t>(offsetof(WrapperMoviePlayer_t3887243686, ___rootDirectory_10)); }
	inline String_t* get_rootDirectory_10() const { return ___rootDirectory_10; }
	inline String_t** get_address_of_rootDirectory_10() { return &___rootDirectory_10; }
	inline void set_rootDirectory_10(String_t* value)
	{
		___rootDirectory_10 = value;
		Il2CppCodeGenWriteBarrier((&___rootDirectory_10), value);
	}
};

struct WrapperMoviePlayer_t3887243686_StaticFields
{
public:
	// Utage.WrapperMoviePlayer Utage.WrapperMoviePlayer::instance
	WrapperMoviePlayer_t3887243686 * ___instance_2;

public:
	inline static int32_t get_offset_of_instance_2() { return static_cast<int32_t>(offsetof(WrapperMoviePlayer_t3887243686_StaticFields, ___instance_2)); }
	inline WrapperMoviePlayer_t3887243686 * get_instance_2() const { return ___instance_2; }
	inline WrapperMoviePlayer_t3887243686 ** get_address_of_instance_2() { return &___instance_2; }
	inline void set_instance_2(WrapperMoviePlayer_t3887243686 * value)
	{
		___instance_2 = value;
		Il2CppCodeGenWriteBarrier((&___instance_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WRAPPERMOVIEPLAYER_T3887243686_H
#ifndef ADVGRAPHICOBJECTLIVE2D_T716049902_H
#define ADVGRAPHICOBJECTLIVE2D_T716049902_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.AdvGraphicObjectLive2D
struct  AdvGraphicObjectLive2D_t716049902  : public MonoBehaviour_t1158329972
{
public:
	// Utage.AdvEngine Utage.AdvGraphicObjectLive2D::engine
	AdvEngine_t1176753927 * ___engine_2;
	// Utage.AdvGraphicObjectCustom Utage.AdvGraphicObjectLive2D::advObj
	AdvGraphicObjectCustom_t976437613 * ___advObj_3;
	// Live2D.Cubism.Rendering.CubismRenderController Utage.AdvGraphicObjectLive2D::renderController
	CubismRenderController_t3290083685 * ___renderController_4;
	// Utage.Live2DLipSynch Utage.AdvGraphicObjectLive2D::lipSynch
	Live2DLipSynch_t3265978124 * ___lipSynch_5;

public:
	inline static int32_t get_offset_of_engine_2() { return static_cast<int32_t>(offsetof(AdvGraphicObjectLive2D_t716049902, ___engine_2)); }
	inline AdvEngine_t1176753927 * get_engine_2() const { return ___engine_2; }
	inline AdvEngine_t1176753927 ** get_address_of_engine_2() { return &___engine_2; }
	inline void set_engine_2(AdvEngine_t1176753927 * value)
	{
		___engine_2 = value;
		Il2CppCodeGenWriteBarrier((&___engine_2), value);
	}

	inline static int32_t get_offset_of_advObj_3() { return static_cast<int32_t>(offsetof(AdvGraphicObjectLive2D_t716049902, ___advObj_3)); }
	inline AdvGraphicObjectCustom_t976437613 * get_advObj_3() const { return ___advObj_3; }
	inline AdvGraphicObjectCustom_t976437613 ** get_address_of_advObj_3() { return &___advObj_3; }
	inline void set_advObj_3(AdvGraphicObjectCustom_t976437613 * value)
	{
		___advObj_3 = value;
		Il2CppCodeGenWriteBarrier((&___advObj_3), value);
	}

	inline static int32_t get_offset_of_renderController_4() { return static_cast<int32_t>(offsetof(AdvGraphicObjectLive2D_t716049902, ___renderController_4)); }
	inline CubismRenderController_t3290083685 * get_renderController_4() const { return ___renderController_4; }
	inline CubismRenderController_t3290083685 ** get_address_of_renderController_4() { return &___renderController_4; }
	inline void set_renderController_4(CubismRenderController_t3290083685 * value)
	{
		___renderController_4 = value;
		Il2CppCodeGenWriteBarrier((&___renderController_4), value);
	}

	inline static int32_t get_offset_of_lipSynch_5() { return static_cast<int32_t>(offsetof(AdvGraphicObjectLive2D_t716049902, ___lipSynch_5)); }
	inline Live2DLipSynch_t3265978124 * get_lipSynch_5() const { return ___lipSynch_5; }
	inline Live2DLipSynch_t3265978124 ** get_address_of_lipSynch_5() { return &___lipSynch_5; }
	inline void set_lipSynch_5(Live2DLipSynch_t3265978124 * value)
	{
		___lipSynch_5 = value;
		Il2CppCodeGenWriteBarrier((&___lipSynch_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ADVGRAPHICOBJECTLIVE2D_T716049902_H
#ifndef UTAGEUGUISAVELOADITEM_T1111947312_H
#define UTAGEUGUISAVELOADITEM_T1111947312_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UtageUguiSaveLoadItem
struct  UtageUguiSaveLoadItem_t1111947312  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.UI.Text UtageUguiSaveLoadItem::text
	Text_t356221433 * ___text_2;
	// UnityEngine.UI.Text UtageUguiSaveLoadItem::no
	Text_t356221433 * ___no_3;
	// UnityEngine.UI.Text UtageUguiSaveLoadItem::date
	Text_t356221433 * ___date_4;
	// UnityEngine.UI.RawImage UtageUguiSaveLoadItem::captureImage
	RawImage_t2749640213 * ___captureImage_5;
	// UnityEngine.Texture2D UtageUguiSaveLoadItem::autoSaveIcon
	Texture2D_t3542995729 * ___autoSaveIcon_6;
	// System.String UtageUguiSaveLoadItem::textEmpty
	String_t* ___textEmpty_7;
	// UnityEngine.UI.Button UtageUguiSaveLoadItem::button
	Button_t2872111280 * ___button_8;
	// Utage.AdvSaveData UtageUguiSaveLoadItem::data
	AdvSaveData_t4059487466 * ___data_9;
	// System.Int32 UtageUguiSaveLoadItem::index
	int32_t ___index_10;
	// UnityEngine.Color UtageUguiSaveLoadItem::defaultColor
	Color_t2020392075  ___defaultColor_11;

public:
	inline static int32_t get_offset_of_text_2() { return static_cast<int32_t>(offsetof(UtageUguiSaveLoadItem_t1111947312, ___text_2)); }
	inline Text_t356221433 * get_text_2() const { return ___text_2; }
	inline Text_t356221433 ** get_address_of_text_2() { return &___text_2; }
	inline void set_text_2(Text_t356221433 * value)
	{
		___text_2 = value;
		Il2CppCodeGenWriteBarrier((&___text_2), value);
	}

	inline static int32_t get_offset_of_no_3() { return static_cast<int32_t>(offsetof(UtageUguiSaveLoadItem_t1111947312, ___no_3)); }
	inline Text_t356221433 * get_no_3() const { return ___no_3; }
	inline Text_t356221433 ** get_address_of_no_3() { return &___no_3; }
	inline void set_no_3(Text_t356221433 * value)
	{
		___no_3 = value;
		Il2CppCodeGenWriteBarrier((&___no_3), value);
	}

	inline static int32_t get_offset_of_date_4() { return static_cast<int32_t>(offsetof(UtageUguiSaveLoadItem_t1111947312, ___date_4)); }
	inline Text_t356221433 * get_date_4() const { return ___date_4; }
	inline Text_t356221433 ** get_address_of_date_4() { return &___date_4; }
	inline void set_date_4(Text_t356221433 * value)
	{
		___date_4 = value;
		Il2CppCodeGenWriteBarrier((&___date_4), value);
	}

	inline static int32_t get_offset_of_captureImage_5() { return static_cast<int32_t>(offsetof(UtageUguiSaveLoadItem_t1111947312, ___captureImage_5)); }
	inline RawImage_t2749640213 * get_captureImage_5() const { return ___captureImage_5; }
	inline RawImage_t2749640213 ** get_address_of_captureImage_5() { return &___captureImage_5; }
	inline void set_captureImage_5(RawImage_t2749640213 * value)
	{
		___captureImage_5 = value;
		Il2CppCodeGenWriteBarrier((&___captureImage_5), value);
	}

	inline static int32_t get_offset_of_autoSaveIcon_6() { return static_cast<int32_t>(offsetof(UtageUguiSaveLoadItem_t1111947312, ___autoSaveIcon_6)); }
	inline Texture2D_t3542995729 * get_autoSaveIcon_6() const { return ___autoSaveIcon_6; }
	inline Texture2D_t3542995729 ** get_address_of_autoSaveIcon_6() { return &___autoSaveIcon_6; }
	inline void set_autoSaveIcon_6(Texture2D_t3542995729 * value)
	{
		___autoSaveIcon_6 = value;
		Il2CppCodeGenWriteBarrier((&___autoSaveIcon_6), value);
	}

	inline static int32_t get_offset_of_textEmpty_7() { return static_cast<int32_t>(offsetof(UtageUguiSaveLoadItem_t1111947312, ___textEmpty_7)); }
	inline String_t* get_textEmpty_7() const { return ___textEmpty_7; }
	inline String_t** get_address_of_textEmpty_7() { return &___textEmpty_7; }
	inline void set_textEmpty_7(String_t* value)
	{
		___textEmpty_7 = value;
		Il2CppCodeGenWriteBarrier((&___textEmpty_7), value);
	}

	inline static int32_t get_offset_of_button_8() { return static_cast<int32_t>(offsetof(UtageUguiSaveLoadItem_t1111947312, ___button_8)); }
	inline Button_t2872111280 * get_button_8() const { return ___button_8; }
	inline Button_t2872111280 ** get_address_of_button_8() { return &___button_8; }
	inline void set_button_8(Button_t2872111280 * value)
	{
		___button_8 = value;
		Il2CppCodeGenWriteBarrier((&___button_8), value);
	}

	inline static int32_t get_offset_of_data_9() { return static_cast<int32_t>(offsetof(UtageUguiSaveLoadItem_t1111947312, ___data_9)); }
	inline AdvSaveData_t4059487466 * get_data_9() const { return ___data_9; }
	inline AdvSaveData_t4059487466 ** get_address_of_data_9() { return &___data_9; }
	inline void set_data_9(AdvSaveData_t4059487466 * value)
	{
		___data_9 = value;
		Il2CppCodeGenWriteBarrier((&___data_9), value);
	}

	inline static int32_t get_offset_of_index_10() { return static_cast<int32_t>(offsetof(UtageUguiSaveLoadItem_t1111947312, ___index_10)); }
	inline int32_t get_index_10() const { return ___index_10; }
	inline int32_t* get_address_of_index_10() { return &___index_10; }
	inline void set_index_10(int32_t value)
	{
		___index_10 = value;
	}

	inline static int32_t get_offset_of_defaultColor_11() { return static_cast<int32_t>(offsetof(UtageUguiSaveLoadItem_t1111947312, ___defaultColor_11)); }
	inline Color_t2020392075  get_defaultColor_11() const { return ___defaultColor_11; }
	inline Color_t2020392075 * get_address_of_defaultColor_11() { return &___defaultColor_11; }
	inline void set_defaultColor_11(Color_t2020392075  value)
	{
		___defaultColor_11 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UTAGEUGUISAVELOADITEM_T1111947312_H
#ifndef UTAGEUGUISKIPBUTTONSTATE_T1473314082_H
#define UTAGEUGUISKIPBUTTONSTATE_T1473314082_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UtageUguiSkipButtonState
struct  UtageUguiSkipButtonState_t1473314082  : public MonoBehaviour_t1158329972
{
public:
	// Utage.AdvEngine UtageUguiSkipButtonState::engine
	AdvEngine_t1176753927 * ___engine_2;
	// UnityEngine.UI.Toggle UtageUguiSkipButtonState::target
	Toggle_t3976754468 * ___target_3;

public:
	inline static int32_t get_offset_of_engine_2() { return static_cast<int32_t>(offsetof(UtageUguiSkipButtonState_t1473314082, ___engine_2)); }
	inline AdvEngine_t1176753927 * get_engine_2() const { return ___engine_2; }
	inline AdvEngine_t1176753927 ** get_address_of_engine_2() { return &___engine_2; }
	inline void set_engine_2(AdvEngine_t1176753927 * value)
	{
		___engine_2 = value;
		Il2CppCodeGenWriteBarrier((&___engine_2), value);
	}

	inline static int32_t get_offset_of_target_3() { return static_cast<int32_t>(offsetof(UtageUguiSkipButtonState_t1473314082, ___target_3)); }
	inline Toggle_t3976754468 * get_target_3() const { return ___target_3; }
	inline Toggle_t3976754468 ** get_address_of_target_3() { return &___target_3; }
	inline void set_target_3(Toggle_t3976754468 * value)
	{
		___target_3 = value;
		Il2CppCodeGenWriteBarrier((&___target_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UTAGEUGUISKIPBUTTONSTATE_T1473314082_H
#ifndef UTAGEUGUISTARTCHAPTER_T1126311843_H
#define UTAGEUGUISTARTCHAPTER_T1126311843_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UtageUguiStartChapter
struct  UtageUguiStartChapter_t1126311843  : public MonoBehaviour_t1158329972
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UTAGEUGUISTARTCHAPTER_T1126311843_H
#ifndef UTAGEUGUICONFIGTAGGEDMASTERVOLUME_T3772792928_H
#define UTAGEUGUICONFIGTAGGEDMASTERVOLUME_T3772792928_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UtageUguiConfigTaggedMasterVolume
struct  UtageUguiConfigTaggedMasterVolume_t3772792928  : public MonoBehaviour_t1158329972
{
public:
	// System.String UtageUguiConfigTaggedMasterVolume::volumeTag
	String_t* ___volumeTag_2;
	// UtageUguiConfig UtageUguiConfigTaggedMasterVolume::config
	UtageUguiConfig_t3500199096 * ___config_3;

public:
	inline static int32_t get_offset_of_volumeTag_2() { return static_cast<int32_t>(offsetof(UtageUguiConfigTaggedMasterVolume_t3772792928, ___volumeTag_2)); }
	inline String_t* get_volumeTag_2() const { return ___volumeTag_2; }
	inline String_t** get_address_of_volumeTag_2() { return &___volumeTag_2; }
	inline void set_volumeTag_2(String_t* value)
	{
		___volumeTag_2 = value;
		Il2CppCodeGenWriteBarrier((&___volumeTag_2), value);
	}

	inline static int32_t get_offset_of_config_3() { return static_cast<int32_t>(offsetof(UtageUguiConfigTaggedMasterVolume_t3772792928, ___config_3)); }
	inline UtageUguiConfig_t3500199096 * get_config_3() const { return ___config_3; }
	inline UtageUguiConfig_t3500199096 ** get_address_of_config_3() { return &___config_3; }
	inline void set_config_3(UtageUguiConfig_t3500199096 * value)
	{
		___config_3 = value;
		Il2CppCodeGenWriteBarrier((&___config_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UTAGEUGUICONFIGTAGGEDMASTERVOLUME_T3772792928_H
#ifndef UTAGEUGUICGGALLERY_T1507549878_H
#define UTAGEUGUICGGALLERY_T1507549878_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UtageUguiCgGallery
struct  UtageUguiCgGallery_t1507549878  : public UguiView_t2174908005
{
public:
	// UtageUguiGallery UtageUguiCgGallery::gallery
	UtageUguiGallery_t4263677524 * ___gallery_12;
	// UtageUguiCgGalleryViewer UtageUguiCgGallery::CgView
	UtageUguiCgGalleryViewer_t3438583972 * ___CgView_13;
	// Utage.UguiCategoryGridPage UtageUguiCgGallery::categoryGridPage
	UguiCategoryGridPage_t673822931 * ___categoryGridPage_14;
	// System.Collections.Generic.List`1<Utage.AdvCgGalleryData> UtageUguiCgGallery::itemDataList
	List_1_t3645217493 * ___itemDataList_15;
	// Utage.AdvEngine UtageUguiCgGallery::engine
	AdvEngine_t1176753927 * ___engine_16;
	// System.Boolean UtageUguiCgGallery::isInit
	bool ___isInit_17;

public:
	inline static int32_t get_offset_of_gallery_12() { return static_cast<int32_t>(offsetof(UtageUguiCgGallery_t1507549878, ___gallery_12)); }
	inline UtageUguiGallery_t4263677524 * get_gallery_12() const { return ___gallery_12; }
	inline UtageUguiGallery_t4263677524 ** get_address_of_gallery_12() { return &___gallery_12; }
	inline void set_gallery_12(UtageUguiGallery_t4263677524 * value)
	{
		___gallery_12 = value;
		Il2CppCodeGenWriteBarrier((&___gallery_12), value);
	}

	inline static int32_t get_offset_of_CgView_13() { return static_cast<int32_t>(offsetof(UtageUguiCgGallery_t1507549878, ___CgView_13)); }
	inline UtageUguiCgGalleryViewer_t3438583972 * get_CgView_13() const { return ___CgView_13; }
	inline UtageUguiCgGalleryViewer_t3438583972 ** get_address_of_CgView_13() { return &___CgView_13; }
	inline void set_CgView_13(UtageUguiCgGalleryViewer_t3438583972 * value)
	{
		___CgView_13 = value;
		Il2CppCodeGenWriteBarrier((&___CgView_13), value);
	}

	inline static int32_t get_offset_of_categoryGridPage_14() { return static_cast<int32_t>(offsetof(UtageUguiCgGallery_t1507549878, ___categoryGridPage_14)); }
	inline UguiCategoryGridPage_t673822931 * get_categoryGridPage_14() const { return ___categoryGridPage_14; }
	inline UguiCategoryGridPage_t673822931 ** get_address_of_categoryGridPage_14() { return &___categoryGridPage_14; }
	inline void set_categoryGridPage_14(UguiCategoryGridPage_t673822931 * value)
	{
		___categoryGridPage_14 = value;
		Il2CppCodeGenWriteBarrier((&___categoryGridPage_14), value);
	}

	inline static int32_t get_offset_of_itemDataList_15() { return static_cast<int32_t>(offsetof(UtageUguiCgGallery_t1507549878, ___itemDataList_15)); }
	inline List_1_t3645217493 * get_itemDataList_15() const { return ___itemDataList_15; }
	inline List_1_t3645217493 ** get_address_of_itemDataList_15() { return &___itemDataList_15; }
	inline void set_itemDataList_15(List_1_t3645217493 * value)
	{
		___itemDataList_15 = value;
		Il2CppCodeGenWriteBarrier((&___itemDataList_15), value);
	}

	inline static int32_t get_offset_of_engine_16() { return static_cast<int32_t>(offsetof(UtageUguiCgGallery_t1507549878, ___engine_16)); }
	inline AdvEngine_t1176753927 * get_engine_16() const { return ___engine_16; }
	inline AdvEngine_t1176753927 ** get_address_of_engine_16() { return &___engine_16; }
	inline void set_engine_16(AdvEngine_t1176753927 * value)
	{
		___engine_16 = value;
		Il2CppCodeGenWriteBarrier((&___engine_16), value);
	}

	inline static int32_t get_offset_of_isInit_17() { return static_cast<int32_t>(offsetof(UtageUguiCgGallery_t1507549878, ___isInit_17)); }
	inline bool get_isInit_17() const { return ___isInit_17; }
	inline bool* get_address_of_isInit_17() { return &___isInit_17; }
	inline void set_isInit_17(bool value)
	{
		___isInit_17 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UTAGEUGUICGGALLERY_T1507549878_H
#ifndef UTAGEUGUISAVELOAD_T130048949_H
#define UTAGEUGUISAVELOAD_T130048949_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UtageUguiSaveLoad
struct  UtageUguiSaveLoad_t130048949  : public UguiView_t2174908005
{
public:
	// Utage.UguiGridPage UtageUguiSaveLoad::gridPage
	UguiGridPage_t1812803607 * ___gridPage_12;
	// System.Collections.Generic.List`1<Utage.AdvSaveData> UtageUguiSaveLoad::itemDataList
	List_1_t3428608598 * ___itemDataList_13;
	// Utage.AdvEngine UtageUguiSaveLoad::engine
	AdvEngine_t1176753927 * ___engine_14;
	// UtageUguiMainGame UtageUguiSaveLoad::mainGame
	UtageUguiMainGame_t4178963699 * ___mainGame_15;
	// UnityEngine.GameObject UtageUguiSaveLoad::saveRoot
	GameObject_t1756533147 * ___saveRoot_16;
	// UnityEngine.GameObject UtageUguiSaveLoad::loadRoot
	GameObject_t1756533147 * ___loadRoot_17;
	// System.Boolean UtageUguiSaveLoad::isSave
	bool ___isSave_18;
	// System.Boolean UtageUguiSaveLoad::isInit
	bool ___isInit_19;
	// System.Int32 UtageUguiSaveLoad::lastPage
	int32_t ___lastPage_20;

public:
	inline static int32_t get_offset_of_gridPage_12() { return static_cast<int32_t>(offsetof(UtageUguiSaveLoad_t130048949, ___gridPage_12)); }
	inline UguiGridPage_t1812803607 * get_gridPage_12() const { return ___gridPage_12; }
	inline UguiGridPage_t1812803607 ** get_address_of_gridPage_12() { return &___gridPage_12; }
	inline void set_gridPage_12(UguiGridPage_t1812803607 * value)
	{
		___gridPage_12 = value;
		Il2CppCodeGenWriteBarrier((&___gridPage_12), value);
	}

	inline static int32_t get_offset_of_itemDataList_13() { return static_cast<int32_t>(offsetof(UtageUguiSaveLoad_t130048949, ___itemDataList_13)); }
	inline List_1_t3428608598 * get_itemDataList_13() const { return ___itemDataList_13; }
	inline List_1_t3428608598 ** get_address_of_itemDataList_13() { return &___itemDataList_13; }
	inline void set_itemDataList_13(List_1_t3428608598 * value)
	{
		___itemDataList_13 = value;
		Il2CppCodeGenWriteBarrier((&___itemDataList_13), value);
	}

	inline static int32_t get_offset_of_engine_14() { return static_cast<int32_t>(offsetof(UtageUguiSaveLoad_t130048949, ___engine_14)); }
	inline AdvEngine_t1176753927 * get_engine_14() const { return ___engine_14; }
	inline AdvEngine_t1176753927 ** get_address_of_engine_14() { return &___engine_14; }
	inline void set_engine_14(AdvEngine_t1176753927 * value)
	{
		___engine_14 = value;
		Il2CppCodeGenWriteBarrier((&___engine_14), value);
	}

	inline static int32_t get_offset_of_mainGame_15() { return static_cast<int32_t>(offsetof(UtageUguiSaveLoad_t130048949, ___mainGame_15)); }
	inline UtageUguiMainGame_t4178963699 * get_mainGame_15() const { return ___mainGame_15; }
	inline UtageUguiMainGame_t4178963699 ** get_address_of_mainGame_15() { return &___mainGame_15; }
	inline void set_mainGame_15(UtageUguiMainGame_t4178963699 * value)
	{
		___mainGame_15 = value;
		Il2CppCodeGenWriteBarrier((&___mainGame_15), value);
	}

	inline static int32_t get_offset_of_saveRoot_16() { return static_cast<int32_t>(offsetof(UtageUguiSaveLoad_t130048949, ___saveRoot_16)); }
	inline GameObject_t1756533147 * get_saveRoot_16() const { return ___saveRoot_16; }
	inline GameObject_t1756533147 ** get_address_of_saveRoot_16() { return &___saveRoot_16; }
	inline void set_saveRoot_16(GameObject_t1756533147 * value)
	{
		___saveRoot_16 = value;
		Il2CppCodeGenWriteBarrier((&___saveRoot_16), value);
	}

	inline static int32_t get_offset_of_loadRoot_17() { return static_cast<int32_t>(offsetof(UtageUguiSaveLoad_t130048949, ___loadRoot_17)); }
	inline GameObject_t1756533147 * get_loadRoot_17() const { return ___loadRoot_17; }
	inline GameObject_t1756533147 ** get_address_of_loadRoot_17() { return &___loadRoot_17; }
	inline void set_loadRoot_17(GameObject_t1756533147 * value)
	{
		___loadRoot_17 = value;
		Il2CppCodeGenWriteBarrier((&___loadRoot_17), value);
	}

	inline static int32_t get_offset_of_isSave_18() { return static_cast<int32_t>(offsetof(UtageUguiSaveLoad_t130048949, ___isSave_18)); }
	inline bool get_isSave_18() const { return ___isSave_18; }
	inline bool* get_address_of_isSave_18() { return &___isSave_18; }
	inline void set_isSave_18(bool value)
	{
		___isSave_18 = value;
	}

	inline static int32_t get_offset_of_isInit_19() { return static_cast<int32_t>(offsetof(UtageUguiSaveLoad_t130048949, ___isInit_19)); }
	inline bool get_isInit_19() const { return ___isInit_19; }
	inline bool* get_address_of_isInit_19() { return &___isInit_19; }
	inline void set_isInit_19(bool value)
	{
		___isInit_19 = value;
	}

	inline static int32_t get_offset_of_lastPage_20() { return static_cast<int32_t>(offsetof(UtageUguiSaveLoad_t130048949, ___lastPage_20)); }
	inline int32_t get_lastPage_20() const { return ___lastPage_20; }
	inline int32_t* get_address_of_lastPage_20() { return &___lastPage_20; }
	inline void set_lastPage_20(int32_t value)
	{
		___lastPage_20 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UTAGEUGUISAVELOAD_T130048949_H
#ifndef UTAGEUGUICGGALLERYVIEWER_T3438583972_H
#define UTAGEUGUICGGALLERYVIEWER_T3438583972_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UtageUguiCgGalleryViewer
struct  UtageUguiCgGalleryViewer_t3438583972  : public UguiView_t2174908005
{
public:
	// UtageUguiGallery UtageUguiCgGalleryViewer::gallery
	UtageUguiGallery_t4263677524 * ___gallery_12;
	// Utage.AdvUguiLoadGraphicFile UtageUguiCgGalleryViewer::texture
	AdvUguiLoadGraphicFile_t1474676621 * ___texture_13;
	// Utage.AdvEngine UtageUguiCgGalleryViewer::engine
	AdvEngine_t1176753927 * ___engine_14;
	// UnityEngine.UI.ScrollRect UtageUguiCgGalleryViewer::scrollRect
	ScrollRect_t1199013257 * ___scrollRect_15;
	// UnityEngine.Vector3 UtageUguiCgGalleryViewer::startContentPosition
	Vector3_t2243707580  ___startContentPosition_16;
	// System.Boolean UtageUguiCgGalleryViewer::isEnableClick
	bool ___isEnableClick_17;
	// System.Boolean UtageUguiCgGalleryViewer::isLoadEnd
	bool ___isLoadEnd_18;
	// Utage.AdvCgGalleryData UtageUguiCgGalleryViewer::data
	AdvCgGalleryData_t4276096361 * ___data_19;
	// System.Int32 UtageUguiCgGalleryViewer::currentIndex
	int32_t ___currentIndex_20;

public:
	inline static int32_t get_offset_of_gallery_12() { return static_cast<int32_t>(offsetof(UtageUguiCgGalleryViewer_t3438583972, ___gallery_12)); }
	inline UtageUguiGallery_t4263677524 * get_gallery_12() const { return ___gallery_12; }
	inline UtageUguiGallery_t4263677524 ** get_address_of_gallery_12() { return &___gallery_12; }
	inline void set_gallery_12(UtageUguiGallery_t4263677524 * value)
	{
		___gallery_12 = value;
		Il2CppCodeGenWriteBarrier((&___gallery_12), value);
	}

	inline static int32_t get_offset_of_texture_13() { return static_cast<int32_t>(offsetof(UtageUguiCgGalleryViewer_t3438583972, ___texture_13)); }
	inline AdvUguiLoadGraphicFile_t1474676621 * get_texture_13() const { return ___texture_13; }
	inline AdvUguiLoadGraphicFile_t1474676621 ** get_address_of_texture_13() { return &___texture_13; }
	inline void set_texture_13(AdvUguiLoadGraphicFile_t1474676621 * value)
	{
		___texture_13 = value;
		Il2CppCodeGenWriteBarrier((&___texture_13), value);
	}

	inline static int32_t get_offset_of_engine_14() { return static_cast<int32_t>(offsetof(UtageUguiCgGalleryViewer_t3438583972, ___engine_14)); }
	inline AdvEngine_t1176753927 * get_engine_14() const { return ___engine_14; }
	inline AdvEngine_t1176753927 ** get_address_of_engine_14() { return &___engine_14; }
	inline void set_engine_14(AdvEngine_t1176753927 * value)
	{
		___engine_14 = value;
		Il2CppCodeGenWriteBarrier((&___engine_14), value);
	}

	inline static int32_t get_offset_of_scrollRect_15() { return static_cast<int32_t>(offsetof(UtageUguiCgGalleryViewer_t3438583972, ___scrollRect_15)); }
	inline ScrollRect_t1199013257 * get_scrollRect_15() const { return ___scrollRect_15; }
	inline ScrollRect_t1199013257 ** get_address_of_scrollRect_15() { return &___scrollRect_15; }
	inline void set_scrollRect_15(ScrollRect_t1199013257 * value)
	{
		___scrollRect_15 = value;
		Il2CppCodeGenWriteBarrier((&___scrollRect_15), value);
	}

	inline static int32_t get_offset_of_startContentPosition_16() { return static_cast<int32_t>(offsetof(UtageUguiCgGalleryViewer_t3438583972, ___startContentPosition_16)); }
	inline Vector3_t2243707580  get_startContentPosition_16() const { return ___startContentPosition_16; }
	inline Vector3_t2243707580 * get_address_of_startContentPosition_16() { return &___startContentPosition_16; }
	inline void set_startContentPosition_16(Vector3_t2243707580  value)
	{
		___startContentPosition_16 = value;
	}

	inline static int32_t get_offset_of_isEnableClick_17() { return static_cast<int32_t>(offsetof(UtageUguiCgGalleryViewer_t3438583972, ___isEnableClick_17)); }
	inline bool get_isEnableClick_17() const { return ___isEnableClick_17; }
	inline bool* get_address_of_isEnableClick_17() { return &___isEnableClick_17; }
	inline void set_isEnableClick_17(bool value)
	{
		___isEnableClick_17 = value;
	}

	inline static int32_t get_offset_of_isLoadEnd_18() { return static_cast<int32_t>(offsetof(UtageUguiCgGalleryViewer_t3438583972, ___isLoadEnd_18)); }
	inline bool get_isLoadEnd_18() const { return ___isLoadEnd_18; }
	inline bool* get_address_of_isLoadEnd_18() { return &___isLoadEnd_18; }
	inline void set_isLoadEnd_18(bool value)
	{
		___isLoadEnd_18 = value;
	}

	inline static int32_t get_offset_of_data_19() { return static_cast<int32_t>(offsetof(UtageUguiCgGalleryViewer_t3438583972, ___data_19)); }
	inline AdvCgGalleryData_t4276096361 * get_data_19() const { return ___data_19; }
	inline AdvCgGalleryData_t4276096361 ** get_address_of_data_19() { return &___data_19; }
	inline void set_data_19(AdvCgGalleryData_t4276096361 * value)
	{
		___data_19 = value;
		Il2CppCodeGenWriteBarrier((&___data_19), value);
	}

	inline static int32_t get_offset_of_currentIndex_20() { return static_cast<int32_t>(offsetof(UtageUguiCgGalleryViewer_t3438583972, ___currentIndex_20)); }
	inline int32_t get_currentIndex_20() const { return ___currentIndex_20; }
	inline int32_t* get_address_of_currentIndex_20() { return &___currentIndex_20; }
	inline void set_currentIndex_20(int32_t value)
	{
		___currentIndex_20 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UTAGEUGUICGGALLERYVIEWER_T3438583972_H
#ifndef UTAGEUGUILOADWAIT_T319861767_H
#define UTAGEUGUILOADWAIT_T319861767_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UtageUguiLoadWait
struct  UtageUguiLoadWait_t319861767  : public UguiView_t2174908005
{
public:
	// Utage.AdvEngine UtageUguiLoadWait::engine
	AdvEngine_t1176753927 * ___engine_12;
	// Utage.AdvEngineStarter UtageUguiLoadWait::starter
	AdvEngineStarter_t330044300 * ___starter_13;
	// System.Boolean UtageUguiLoadWait::isAutoCacheFileLoad
	bool ___isAutoCacheFileLoad_14;
	// UtageUguiTitle UtageUguiLoadWait::title
	UtageUguiTitle_t1483171752 * ___title_15;
	// System.String UtageUguiLoadWait::bootSceneName
	String_t* ___bootSceneName_16;
	// UnityEngine.GameObject UtageUguiLoadWait::buttonSkip
	GameObject_t1756533147 * ___buttonSkip_17;
	// UnityEngine.GameObject UtageUguiLoadWait::buttonBack
	GameObject_t1756533147 * ___buttonBack_18;
	// UnityEngine.GameObject UtageUguiLoadWait::buttonDownload
	GameObject_t1756533147 * ___buttonDownload_19;
	// UnityEngine.GameObject UtageUguiLoadWait::loadingBarRoot
	GameObject_t1756533147 * ___loadingBarRoot_20;
	// UnityEngine.UI.Image UtageUguiLoadWait::loadingBar
	Image_t2042527209 * ___loadingBar_21;
	// UnityEngine.UI.Text UtageUguiLoadWait::textMain
	Text_t356221433 * ___textMain_22;
	// UnityEngine.UI.Text UtageUguiLoadWait::textCount
	Text_t356221433 * ___textCount_23;
	// Utage.OpenDialogEvent UtageUguiLoadWait::onOpenDialog
	OpenDialogEvent_t2352564994 * ___onOpenDialog_24;
	// UtageUguiLoadWait/State UtageUguiLoadWait::<CurrentState>k__BackingField
	int32_t ___U3CCurrentStateU3Ek__BackingField_25;
	// UtageUguiLoadWait/Type UtageUguiLoadWait::<DownloadType>k__BackingField
	int32_t ___U3CDownloadTypeU3Ek__BackingField_26;

public:
	inline static int32_t get_offset_of_engine_12() { return static_cast<int32_t>(offsetof(UtageUguiLoadWait_t319861767, ___engine_12)); }
	inline AdvEngine_t1176753927 * get_engine_12() const { return ___engine_12; }
	inline AdvEngine_t1176753927 ** get_address_of_engine_12() { return &___engine_12; }
	inline void set_engine_12(AdvEngine_t1176753927 * value)
	{
		___engine_12 = value;
		Il2CppCodeGenWriteBarrier((&___engine_12), value);
	}

	inline static int32_t get_offset_of_starter_13() { return static_cast<int32_t>(offsetof(UtageUguiLoadWait_t319861767, ___starter_13)); }
	inline AdvEngineStarter_t330044300 * get_starter_13() const { return ___starter_13; }
	inline AdvEngineStarter_t330044300 ** get_address_of_starter_13() { return &___starter_13; }
	inline void set_starter_13(AdvEngineStarter_t330044300 * value)
	{
		___starter_13 = value;
		Il2CppCodeGenWriteBarrier((&___starter_13), value);
	}

	inline static int32_t get_offset_of_isAutoCacheFileLoad_14() { return static_cast<int32_t>(offsetof(UtageUguiLoadWait_t319861767, ___isAutoCacheFileLoad_14)); }
	inline bool get_isAutoCacheFileLoad_14() const { return ___isAutoCacheFileLoad_14; }
	inline bool* get_address_of_isAutoCacheFileLoad_14() { return &___isAutoCacheFileLoad_14; }
	inline void set_isAutoCacheFileLoad_14(bool value)
	{
		___isAutoCacheFileLoad_14 = value;
	}

	inline static int32_t get_offset_of_title_15() { return static_cast<int32_t>(offsetof(UtageUguiLoadWait_t319861767, ___title_15)); }
	inline UtageUguiTitle_t1483171752 * get_title_15() const { return ___title_15; }
	inline UtageUguiTitle_t1483171752 ** get_address_of_title_15() { return &___title_15; }
	inline void set_title_15(UtageUguiTitle_t1483171752 * value)
	{
		___title_15 = value;
		Il2CppCodeGenWriteBarrier((&___title_15), value);
	}

	inline static int32_t get_offset_of_bootSceneName_16() { return static_cast<int32_t>(offsetof(UtageUguiLoadWait_t319861767, ___bootSceneName_16)); }
	inline String_t* get_bootSceneName_16() const { return ___bootSceneName_16; }
	inline String_t** get_address_of_bootSceneName_16() { return &___bootSceneName_16; }
	inline void set_bootSceneName_16(String_t* value)
	{
		___bootSceneName_16 = value;
		Il2CppCodeGenWriteBarrier((&___bootSceneName_16), value);
	}

	inline static int32_t get_offset_of_buttonSkip_17() { return static_cast<int32_t>(offsetof(UtageUguiLoadWait_t319861767, ___buttonSkip_17)); }
	inline GameObject_t1756533147 * get_buttonSkip_17() const { return ___buttonSkip_17; }
	inline GameObject_t1756533147 ** get_address_of_buttonSkip_17() { return &___buttonSkip_17; }
	inline void set_buttonSkip_17(GameObject_t1756533147 * value)
	{
		___buttonSkip_17 = value;
		Il2CppCodeGenWriteBarrier((&___buttonSkip_17), value);
	}

	inline static int32_t get_offset_of_buttonBack_18() { return static_cast<int32_t>(offsetof(UtageUguiLoadWait_t319861767, ___buttonBack_18)); }
	inline GameObject_t1756533147 * get_buttonBack_18() const { return ___buttonBack_18; }
	inline GameObject_t1756533147 ** get_address_of_buttonBack_18() { return &___buttonBack_18; }
	inline void set_buttonBack_18(GameObject_t1756533147 * value)
	{
		___buttonBack_18 = value;
		Il2CppCodeGenWriteBarrier((&___buttonBack_18), value);
	}

	inline static int32_t get_offset_of_buttonDownload_19() { return static_cast<int32_t>(offsetof(UtageUguiLoadWait_t319861767, ___buttonDownload_19)); }
	inline GameObject_t1756533147 * get_buttonDownload_19() const { return ___buttonDownload_19; }
	inline GameObject_t1756533147 ** get_address_of_buttonDownload_19() { return &___buttonDownload_19; }
	inline void set_buttonDownload_19(GameObject_t1756533147 * value)
	{
		___buttonDownload_19 = value;
		Il2CppCodeGenWriteBarrier((&___buttonDownload_19), value);
	}

	inline static int32_t get_offset_of_loadingBarRoot_20() { return static_cast<int32_t>(offsetof(UtageUguiLoadWait_t319861767, ___loadingBarRoot_20)); }
	inline GameObject_t1756533147 * get_loadingBarRoot_20() const { return ___loadingBarRoot_20; }
	inline GameObject_t1756533147 ** get_address_of_loadingBarRoot_20() { return &___loadingBarRoot_20; }
	inline void set_loadingBarRoot_20(GameObject_t1756533147 * value)
	{
		___loadingBarRoot_20 = value;
		Il2CppCodeGenWriteBarrier((&___loadingBarRoot_20), value);
	}

	inline static int32_t get_offset_of_loadingBar_21() { return static_cast<int32_t>(offsetof(UtageUguiLoadWait_t319861767, ___loadingBar_21)); }
	inline Image_t2042527209 * get_loadingBar_21() const { return ___loadingBar_21; }
	inline Image_t2042527209 ** get_address_of_loadingBar_21() { return &___loadingBar_21; }
	inline void set_loadingBar_21(Image_t2042527209 * value)
	{
		___loadingBar_21 = value;
		Il2CppCodeGenWriteBarrier((&___loadingBar_21), value);
	}

	inline static int32_t get_offset_of_textMain_22() { return static_cast<int32_t>(offsetof(UtageUguiLoadWait_t319861767, ___textMain_22)); }
	inline Text_t356221433 * get_textMain_22() const { return ___textMain_22; }
	inline Text_t356221433 ** get_address_of_textMain_22() { return &___textMain_22; }
	inline void set_textMain_22(Text_t356221433 * value)
	{
		___textMain_22 = value;
		Il2CppCodeGenWriteBarrier((&___textMain_22), value);
	}

	inline static int32_t get_offset_of_textCount_23() { return static_cast<int32_t>(offsetof(UtageUguiLoadWait_t319861767, ___textCount_23)); }
	inline Text_t356221433 * get_textCount_23() const { return ___textCount_23; }
	inline Text_t356221433 ** get_address_of_textCount_23() { return &___textCount_23; }
	inline void set_textCount_23(Text_t356221433 * value)
	{
		___textCount_23 = value;
		Il2CppCodeGenWriteBarrier((&___textCount_23), value);
	}

	inline static int32_t get_offset_of_onOpenDialog_24() { return static_cast<int32_t>(offsetof(UtageUguiLoadWait_t319861767, ___onOpenDialog_24)); }
	inline OpenDialogEvent_t2352564994 * get_onOpenDialog_24() const { return ___onOpenDialog_24; }
	inline OpenDialogEvent_t2352564994 ** get_address_of_onOpenDialog_24() { return &___onOpenDialog_24; }
	inline void set_onOpenDialog_24(OpenDialogEvent_t2352564994 * value)
	{
		___onOpenDialog_24 = value;
		Il2CppCodeGenWriteBarrier((&___onOpenDialog_24), value);
	}

	inline static int32_t get_offset_of_U3CCurrentStateU3Ek__BackingField_25() { return static_cast<int32_t>(offsetof(UtageUguiLoadWait_t319861767, ___U3CCurrentStateU3Ek__BackingField_25)); }
	inline int32_t get_U3CCurrentStateU3Ek__BackingField_25() const { return ___U3CCurrentStateU3Ek__BackingField_25; }
	inline int32_t* get_address_of_U3CCurrentStateU3Ek__BackingField_25() { return &___U3CCurrentStateU3Ek__BackingField_25; }
	inline void set_U3CCurrentStateU3Ek__BackingField_25(int32_t value)
	{
		___U3CCurrentStateU3Ek__BackingField_25 = value;
	}

	inline static int32_t get_offset_of_U3CDownloadTypeU3Ek__BackingField_26() { return static_cast<int32_t>(offsetof(UtageUguiLoadWait_t319861767, ___U3CDownloadTypeU3Ek__BackingField_26)); }
	inline int32_t get_U3CDownloadTypeU3Ek__BackingField_26() const { return ___U3CDownloadTypeU3Ek__BackingField_26; }
	inline int32_t* get_address_of_U3CDownloadTypeU3Ek__BackingField_26() { return &___U3CDownloadTypeU3Ek__BackingField_26; }
	inline void set_U3CDownloadTypeU3Ek__BackingField_26(int32_t value)
	{
		___U3CDownloadTypeU3Ek__BackingField_26 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UTAGEUGUILOADWAIT_T319861767_H
#ifndef UTAGEUGUIGALLERY_T4263677524_H
#define UTAGEUGUIGALLERY_T4263677524_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UtageUguiGallery
struct  UtageUguiGallery_t4263677524  : public UguiView_t2174908005
{
public:
	// Utage.UguiView[] UtageUguiGallery::views
	UguiViewU5BU5D_t1602826856* ___views_12;
	// System.Int32 UtageUguiGallery::tabIndex
	int32_t ___tabIndex_13;

public:
	inline static int32_t get_offset_of_views_12() { return static_cast<int32_t>(offsetof(UtageUguiGallery_t4263677524, ___views_12)); }
	inline UguiViewU5BU5D_t1602826856* get_views_12() const { return ___views_12; }
	inline UguiViewU5BU5D_t1602826856** get_address_of_views_12() { return &___views_12; }
	inline void set_views_12(UguiViewU5BU5D_t1602826856* value)
	{
		___views_12 = value;
		Il2CppCodeGenWriteBarrier((&___views_12), value);
	}

	inline static int32_t get_offset_of_tabIndex_13() { return static_cast<int32_t>(offsetof(UtageUguiGallery_t4263677524, ___tabIndex_13)); }
	inline int32_t get_tabIndex_13() const { return ___tabIndex_13; }
	inline int32_t* get_address_of_tabIndex_13() { return &___tabIndex_13; }
	inline void set_tabIndex_13(int32_t value)
	{
		___tabIndex_13 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UTAGEUGUIGALLERY_T4263677524_H
#ifndef UTAGEUGUIMAINGAME_T4178963699_H
#define UTAGEUGUIMAINGAME_T4178963699_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UtageUguiMainGame
struct  UtageUguiMainGame_t4178963699  : public UguiView_t2174908005
{
public:
	// Utage.AdvEngine UtageUguiMainGame::engine
	AdvEngine_t1176753927 * ___engine_12;
	// Utage.LetterBoxCamera UtageUguiMainGame::letterBoxCamera
	LetterBoxCamera_t3507617684 * ___letterBoxCamera_13;
	// UtageUguiTitle UtageUguiMainGame::title
	UtageUguiTitle_t1483171752 * ___title_14;
	// UtageUguiConfig UtageUguiMainGame::config
	UtageUguiConfig_t3500199096 * ___config_15;
	// UtageUguiSaveLoad UtageUguiMainGame::saveLoad
	UtageUguiSaveLoad_t130048949 * ___saveLoad_16;
	// UtageUguiGallery UtageUguiMainGame::gallery
	UtageUguiGallery_t4263677524 * ___gallery_17;
	// UnityEngine.GameObject UtageUguiMainGame::buttons
	GameObject_t1756533147 * ___buttons_18;
	// UnityEngine.UI.Toggle UtageUguiMainGame::checkSkip
	Toggle_t3976754468 * ___checkSkip_19;
	// UnityEngine.UI.Toggle UtageUguiMainGame::checkAuto
	Toggle_t3976754468 * ___checkAuto_20;
	// UtageUguiMainGame/BootType UtageUguiMainGame::bootType
	int32_t ___bootType_21;
	// Utage.AdvSaveData UtageUguiMainGame::loadData
	AdvSaveData_t4059487466 * ___loadData_22;
	// System.Boolean UtageUguiMainGame::isInit
	bool ___isInit_23;
	// System.String UtageUguiMainGame::scenarioLabel
	String_t* ___scenarioLabel_24;

public:
	inline static int32_t get_offset_of_engine_12() { return static_cast<int32_t>(offsetof(UtageUguiMainGame_t4178963699, ___engine_12)); }
	inline AdvEngine_t1176753927 * get_engine_12() const { return ___engine_12; }
	inline AdvEngine_t1176753927 ** get_address_of_engine_12() { return &___engine_12; }
	inline void set_engine_12(AdvEngine_t1176753927 * value)
	{
		___engine_12 = value;
		Il2CppCodeGenWriteBarrier((&___engine_12), value);
	}

	inline static int32_t get_offset_of_letterBoxCamera_13() { return static_cast<int32_t>(offsetof(UtageUguiMainGame_t4178963699, ___letterBoxCamera_13)); }
	inline LetterBoxCamera_t3507617684 * get_letterBoxCamera_13() const { return ___letterBoxCamera_13; }
	inline LetterBoxCamera_t3507617684 ** get_address_of_letterBoxCamera_13() { return &___letterBoxCamera_13; }
	inline void set_letterBoxCamera_13(LetterBoxCamera_t3507617684 * value)
	{
		___letterBoxCamera_13 = value;
		Il2CppCodeGenWriteBarrier((&___letterBoxCamera_13), value);
	}

	inline static int32_t get_offset_of_title_14() { return static_cast<int32_t>(offsetof(UtageUguiMainGame_t4178963699, ___title_14)); }
	inline UtageUguiTitle_t1483171752 * get_title_14() const { return ___title_14; }
	inline UtageUguiTitle_t1483171752 ** get_address_of_title_14() { return &___title_14; }
	inline void set_title_14(UtageUguiTitle_t1483171752 * value)
	{
		___title_14 = value;
		Il2CppCodeGenWriteBarrier((&___title_14), value);
	}

	inline static int32_t get_offset_of_config_15() { return static_cast<int32_t>(offsetof(UtageUguiMainGame_t4178963699, ___config_15)); }
	inline UtageUguiConfig_t3500199096 * get_config_15() const { return ___config_15; }
	inline UtageUguiConfig_t3500199096 ** get_address_of_config_15() { return &___config_15; }
	inline void set_config_15(UtageUguiConfig_t3500199096 * value)
	{
		___config_15 = value;
		Il2CppCodeGenWriteBarrier((&___config_15), value);
	}

	inline static int32_t get_offset_of_saveLoad_16() { return static_cast<int32_t>(offsetof(UtageUguiMainGame_t4178963699, ___saveLoad_16)); }
	inline UtageUguiSaveLoad_t130048949 * get_saveLoad_16() const { return ___saveLoad_16; }
	inline UtageUguiSaveLoad_t130048949 ** get_address_of_saveLoad_16() { return &___saveLoad_16; }
	inline void set_saveLoad_16(UtageUguiSaveLoad_t130048949 * value)
	{
		___saveLoad_16 = value;
		Il2CppCodeGenWriteBarrier((&___saveLoad_16), value);
	}

	inline static int32_t get_offset_of_gallery_17() { return static_cast<int32_t>(offsetof(UtageUguiMainGame_t4178963699, ___gallery_17)); }
	inline UtageUguiGallery_t4263677524 * get_gallery_17() const { return ___gallery_17; }
	inline UtageUguiGallery_t4263677524 ** get_address_of_gallery_17() { return &___gallery_17; }
	inline void set_gallery_17(UtageUguiGallery_t4263677524 * value)
	{
		___gallery_17 = value;
		Il2CppCodeGenWriteBarrier((&___gallery_17), value);
	}

	inline static int32_t get_offset_of_buttons_18() { return static_cast<int32_t>(offsetof(UtageUguiMainGame_t4178963699, ___buttons_18)); }
	inline GameObject_t1756533147 * get_buttons_18() const { return ___buttons_18; }
	inline GameObject_t1756533147 ** get_address_of_buttons_18() { return &___buttons_18; }
	inline void set_buttons_18(GameObject_t1756533147 * value)
	{
		___buttons_18 = value;
		Il2CppCodeGenWriteBarrier((&___buttons_18), value);
	}

	inline static int32_t get_offset_of_checkSkip_19() { return static_cast<int32_t>(offsetof(UtageUguiMainGame_t4178963699, ___checkSkip_19)); }
	inline Toggle_t3976754468 * get_checkSkip_19() const { return ___checkSkip_19; }
	inline Toggle_t3976754468 ** get_address_of_checkSkip_19() { return &___checkSkip_19; }
	inline void set_checkSkip_19(Toggle_t3976754468 * value)
	{
		___checkSkip_19 = value;
		Il2CppCodeGenWriteBarrier((&___checkSkip_19), value);
	}

	inline static int32_t get_offset_of_checkAuto_20() { return static_cast<int32_t>(offsetof(UtageUguiMainGame_t4178963699, ___checkAuto_20)); }
	inline Toggle_t3976754468 * get_checkAuto_20() const { return ___checkAuto_20; }
	inline Toggle_t3976754468 ** get_address_of_checkAuto_20() { return &___checkAuto_20; }
	inline void set_checkAuto_20(Toggle_t3976754468 * value)
	{
		___checkAuto_20 = value;
		Il2CppCodeGenWriteBarrier((&___checkAuto_20), value);
	}

	inline static int32_t get_offset_of_bootType_21() { return static_cast<int32_t>(offsetof(UtageUguiMainGame_t4178963699, ___bootType_21)); }
	inline int32_t get_bootType_21() const { return ___bootType_21; }
	inline int32_t* get_address_of_bootType_21() { return &___bootType_21; }
	inline void set_bootType_21(int32_t value)
	{
		___bootType_21 = value;
	}

	inline static int32_t get_offset_of_loadData_22() { return static_cast<int32_t>(offsetof(UtageUguiMainGame_t4178963699, ___loadData_22)); }
	inline AdvSaveData_t4059487466 * get_loadData_22() const { return ___loadData_22; }
	inline AdvSaveData_t4059487466 ** get_address_of_loadData_22() { return &___loadData_22; }
	inline void set_loadData_22(AdvSaveData_t4059487466 * value)
	{
		___loadData_22 = value;
		Il2CppCodeGenWriteBarrier((&___loadData_22), value);
	}

	inline static int32_t get_offset_of_isInit_23() { return static_cast<int32_t>(offsetof(UtageUguiMainGame_t4178963699, ___isInit_23)); }
	inline bool get_isInit_23() const { return ___isInit_23; }
	inline bool* get_address_of_isInit_23() { return &___isInit_23; }
	inline void set_isInit_23(bool value)
	{
		___isInit_23 = value;
	}

	inline static int32_t get_offset_of_scenarioLabel_24() { return static_cast<int32_t>(offsetof(UtageUguiMainGame_t4178963699, ___scenarioLabel_24)); }
	inline String_t* get_scenarioLabel_24() const { return ___scenarioLabel_24; }
	inline String_t** get_address_of_scenarioLabel_24() { return &___scenarioLabel_24; }
	inline void set_scenarioLabel_24(String_t* value)
	{
		___scenarioLabel_24 = value;
		Il2CppCodeGenWriteBarrier((&___scenarioLabel_24), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UTAGEUGUIMAINGAME_T4178963699_H
#ifndef UTAGEUGUIBOOT_T1438884946_H
#define UTAGEUGUIBOOT_T1438884946_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UtageUguiBoot
struct  UtageUguiBoot_t1438884946  : public UguiView_t2174908005
{
public:
	// Utage.AdvEngine UtageUguiBoot::engine
	AdvEngine_t1176753927 * ___engine_12;
	// Utage.UguiFadeTextureStream UtageUguiBoot::fadeTextureStream
	UguiFadeTextureStream_t4272889985 * ___fadeTextureStream_13;
	// UtageUguiTitle UtageUguiBoot::title
	UtageUguiTitle_t1483171752 * ___title_14;
	// UtageUguiLoadWait UtageUguiBoot::loadWait
	UtageUguiLoadWait_t319861767 * ___loadWait_15;
	// System.Boolean UtageUguiBoot::isWaitBoot
	bool ___isWaitBoot_16;
	// System.Boolean UtageUguiBoot::isWaitDownLoad
	bool ___isWaitDownLoad_17;
	// System.Boolean UtageUguiBoot::isWaitSplashScreen
	bool ___isWaitSplashScreen_18;

public:
	inline static int32_t get_offset_of_engine_12() { return static_cast<int32_t>(offsetof(UtageUguiBoot_t1438884946, ___engine_12)); }
	inline AdvEngine_t1176753927 * get_engine_12() const { return ___engine_12; }
	inline AdvEngine_t1176753927 ** get_address_of_engine_12() { return &___engine_12; }
	inline void set_engine_12(AdvEngine_t1176753927 * value)
	{
		___engine_12 = value;
		Il2CppCodeGenWriteBarrier((&___engine_12), value);
	}

	inline static int32_t get_offset_of_fadeTextureStream_13() { return static_cast<int32_t>(offsetof(UtageUguiBoot_t1438884946, ___fadeTextureStream_13)); }
	inline UguiFadeTextureStream_t4272889985 * get_fadeTextureStream_13() const { return ___fadeTextureStream_13; }
	inline UguiFadeTextureStream_t4272889985 ** get_address_of_fadeTextureStream_13() { return &___fadeTextureStream_13; }
	inline void set_fadeTextureStream_13(UguiFadeTextureStream_t4272889985 * value)
	{
		___fadeTextureStream_13 = value;
		Il2CppCodeGenWriteBarrier((&___fadeTextureStream_13), value);
	}

	inline static int32_t get_offset_of_title_14() { return static_cast<int32_t>(offsetof(UtageUguiBoot_t1438884946, ___title_14)); }
	inline UtageUguiTitle_t1483171752 * get_title_14() const { return ___title_14; }
	inline UtageUguiTitle_t1483171752 ** get_address_of_title_14() { return &___title_14; }
	inline void set_title_14(UtageUguiTitle_t1483171752 * value)
	{
		___title_14 = value;
		Il2CppCodeGenWriteBarrier((&___title_14), value);
	}

	inline static int32_t get_offset_of_loadWait_15() { return static_cast<int32_t>(offsetof(UtageUguiBoot_t1438884946, ___loadWait_15)); }
	inline UtageUguiLoadWait_t319861767 * get_loadWait_15() const { return ___loadWait_15; }
	inline UtageUguiLoadWait_t319861767 ** get_address_of_loadWait_15() { return &___loadWait_15; }
	inline void set_loadWait_15(UtageUguiLoadWait_t319861767 * value)
	{
		___loadWait_15 = value;
		Il2CppCodeGenWriteBarrier((&___loadWait_15), value);
	}

	inline static int32_t get_offset_of_isWaitBoot_16() { return static_cast<int32_t>(offsetof(UtageUguiBoot_t1438884946, ___isWaitBoot_16)); }
	inline bool get_isWaitBoot_16() const { return ___isWaitBoot_16; }
	inline bool* get_address_of_isWaitBoot_16() { return &___isWaitBoot_16; }
	inline void set_isWaitBoot_16(bool value)
	{
		___isWaitBoot_16 = value;
	}

	inline static int32_t get_offset_of_isWaitDownLoad_17() { return static_cast<int32_t>(offsetof(UtageUguiBoot_t1438884946, ___isWaitDownLoad_17)); }
	inline bool get_isWaitDownLoad_17() const { return ___isWaitDownLoad_17; }
	inline bool* get_address_of_isWaitDownLoad_17() { return &___isWaitDownLoad_17; }
	inline void set_isWaitDownLoad_17(bool value)
	{
		___isWaitDownLoad_17 = value;
	}

	inline static int32_t get_offset_of_isWaitSplashScreen_18() { return static_cast<int32_t>(offsetof(UtageUguiBoot_t1438884946, ___isWaitSplashScreen_18)); }
	inline bool get_isWaitSplashScreen_18() const { return ___isWaitSplashScreen_18; }
	inline bool* get_address_of_isWaitSplashScreen_18() { return &___isWaitSplashScreen_18; }
	inline void set_isWaitSplashScreen_18(bool value)
	{
		___isWaitSplashScreen_18 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UTAGEUGUIBOOT_T1438884946_H
#ifndef UTAGEUGUISOUNDROOM_T2656581854_H
#define UTAGEUGUISOUNDROOM_T2656581854_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UtageUguiSoundRoom
struct  UtageUguiSoundRoom_t2656581854  : public UguiView_t2174908005
{
public:
	// UtageUguiGallery UtageUguiSoundRoom::gallery
	UtageUguiGallery_t4263677524 * ___gallery_12;
	// Utage.UguiListView UtageUguiSoundRoom::listView
	UguiListView_t169672791 * ___listView_13;
	// System.Collections.Generic.List`1<Utage.AdvSoundSettingData> UtageUguiSoundRoom::itemDataList
	List_1_t2504512354 * ___itemDataList_14;
	// Utage.AdvEngine UtageUguiSoundRoom::engine
	AdvEngine_t1176753927 * ___engine_15;
	// System.Boolean UtageUguiSoundRoom::isInit
	bool ___isInit_16;
	// System.Boolean UtageUguiSoundRoom::isChangedBgm
	bool ___isChangedBgm_17;

public:
	inline static int32_t get_offset_of_gallery_12() { return static_cast<int32_t>(offsetof(UtageUguiSoundRoom_t2656581854, ___gallery_12)); }
	inline UtageUguiGallery_t4263677524 * get_gallery_12() const { return ___gallery_12; }
	inline UtageUguiGallery_t4263677524 ** get_address_of_gallery_12() { return &___gallery_12; }
	inline void set_gallery_12(UtageUguiGallery_t4263677524 * value)
	{
		___gallery_12 = value;
		Il2CppCodeGenWriteBarrier((&___gallery_12), value);
	}

	inline static int32_t get_offset_of_listView_13() { return static_cast<int32_t>(offsetof(UtageUguiSoundRoom_t2656581854, ___listView_13)); }
	inline UguiListView_t169672791 * get_listView_13() const { return ___listView_13; }
	inline UguiListView_t169672791 ** get_address_of_listView_13() { return &___listView_13; }
	inline void set_listView_13(UguiListView_t169672791 * value)
	{
		___listView_13 = value;
		Il2CppCodeGenWriteBarrier((&___listView_13), value);
	}

	inline static int32_t get_offset_of_itemDataList_14() { return static_cast<int32_t>(offsetof(UtageUguiSoundRoom_t2656581854, ___itemDataList_14)); }
	inline List_1_t2504512354 * get_itemDataList_14() const { return ___itemDataList_14; }
	inline List_1_t2504512354 ** get_address_of_itemDataList_14() { return &___itemDataList_14; }
	inline void set_itemDataList_14(List_1_t2504512354 * value)
	{
		___itemDataList_14 = value;
		Il2CppCodeGenWriteBarrier((&___itemDataList_14), value);
	}

	inline static int32_t get_offset_of_engine_15() { return static_cast<int32_t>(offsetof(UtageUguiSoundRoom_t2656581854, ___engine_15)); }
	inline AdvEngine_t1176753927 * get_engine_15() const { return ___engine_15; }
	inline AdvEngine_t1176753927 ** get_address_of_engine_15() { return &___engine_15; }
	inline void set_engine_15(AdvEngine_t1176753927 * value)
	{
		___engine_15 = value;
		Il2CppCodeGenWriteBarrier((&___engine_15), value);
	}

	inline static int32_t get_offset_of_isInit_16() { return static_cast<int32_t>(offsetof(UtageUguiSoundRoom_t2656581854, ___isInit_16)); }
	inline bool get_isInit_16() const { return ___isInit_16; }
	inline bool* get_address_of_isInit_16() { return &___isInit_16; }
	inline void set_isInit_16(bool value)
	{
		___isInit_16 = value;
	}

	inline static int32_t get_offset_of_isChangedBgm_17() { return static_cast<int32_t>(offsetof(UtageUguiSoundRoom_t2656581854, ___isChangedBgm_17)); }
	inline bool get_isChangedBgm_17() const { return ___isChangedBgm_17; }
	inline bool* get_address_of_isChangedBgm_17() { return &___isChangedBgm_17; }
	inline void set_isChangedBgm_17(bool value)
	{
		___isChangedBgm_17 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UTAGEUGUISOUNDROOM_T2656581854_H
#ifndef UTAGEUGUITITLE_T1483171752_H
#define UTAGEUGUITITLE_T1483171752_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UtageUguiTitle
struct  UtageUguiTitle_t1483171752  : public UguiView_t2174908005
{
public:
	// Utage.AdvEngineStarter UtageUguiTitle::starter
	AdvEngineStarter_t330044300 * ___starter_12;
	// UtageUguiMainGame UtageUguiTitle::mainGame
	UtageUguiMainGame_t4178963699 * ___mainGame_13;
	// UtageUguiConfig UtageUguiTitle::config
	UtageUguiConfig_t3500199096 * ___config_14;
	// UtageUguiSaveLoad UtageUguiTitle::load
	UtageUguiSaveLoad_t130048949 * ___load_15;
	// UtageUguiGallery UtageUguiTitle::gallery
	UtageUguiGallery_t4263677524 * ___gallery_16;
	// UtageUguiLoadWait UtageUguiTitle::download
	UtageUguiLoadWait_t319861767 * ___download_17;
	// UnityEngine.GameObject UtageUguiTitle::downloadButton
	GameObject_t1756533147 * ___downloadButton_18;

public:
	inline static int32_t get_offset_of_starter_12() { return static_cast<int32_t>(offsetof(UtageUguiTitle_t1483171752, ___starter_12)); }
	inline AdvEngineStarter_t330044300 * get_starter_12() const { return ___starter_12; }
	inline AdvEngineStarter_t330044300 ** get_address_of_starter_12() { return &___starter_12; }
	inline void set_starter_12(AdvEngineStarter_t330044300 * value)
	{
		___starter_12 = value;
		Il2CppCodeGenWriteBarrier((&___starter_12), value);
	}

	inline static int32_t get_offset_of_mainGame_13() { return static_cast<int32_t>(offsetof(UtageUguiTitle_t1483171752, ___mainGame_13)); }
	inline UtageUguiMainGame_t4178963699 * get_mainGame_13() const { return ___mainGame_13; }
	inline UtageUguiMainGame_t4178963699 ** get_address_of_mainGame_13() { return &___mainGame_13; }
	inline void set_mainGame_13(UtageUguiMainGame_t4178963699 * value)
	{
		___mainGame_13 = value;
		Il2CppCodeGenWriteBarrier((&___mainGame_13), value);
	}

	inline static int32_t get_offset_of_config_14() { return static_cast<int32_t>(offsetof(UtageUguiTitle_t1483171752, ___config_14)); }
	inline UtageUguiConfig_t3500199096 * get_config_14() const { return ___config_14; }
	inline UtageUguiConfig_t3500199096 ** get_address_of_config_14() { return &___config_14; }
	inline void set_config_14(UtageUguiConfig_t3500199096 * value)
	{
		___config_14 = value;
		Il2CppCodeGenWriteBarrier((&___config_14), value);
	}

	inline static int32_t get_offset_of_load_15() { return static_cast<int32_t>(offsetof(UtageUguiTitle_t1483171752, ___load_15)); }
	inline UtageUguiSaveLoad_t130048949 * get_load_15() const { return ___load_15; }
	inline UtageUguiSaveLoad_t130048949 ** get_address_of_load_15() { return &___load_15; }
	inline void set_load_15(UtageUguiSaveLoad_t130048949 * value)
	{
		___load_15 = value;
		Il2CppCodeGenWriteBarrier((&___load_15), value);
	}

	inline static int32_t get_offset_of_gallery_16() { return static_cast<int32_t>(offsetof(UtageUguiTitle_t1483171752, ___gallery_16)); }
	inline UtageUguiGallery_t4263677524 * get_gallery_16() const { return ___gallery_16; }
	inline UtageUguiGallery_t4263677524 ** get_address_of_gallery_16() { return &___gallery_16; }
	inline void set_gallery_16(UtageUguiGallery_t4263677524 * value)
	{
		___gallery_16 = value;
		Il2CppCodeGenWriteBarrier((&___gallery_16), value);
	}

	inline static int32_t get_offset_of_download_17() { return static_cast<int32_t>(offsetof(UtageUguiTitle_t1483171752, ___download_17)); }
	inline UtageUguiLoadWait_t319861767 * get_download_17() const { return ___download_17; }
	inline UtageUguiLoadWait_t319861767 ** get_address_of_download_17() { return &___download_17; }
	inline void set_download_17(UtageUguiLoadWait_t319861767 * value)
	{
		___download_17 = value;
		Il2CppCodeGenWriteBarrier((&___download_17), value);
	}

	inline static int32_t get_offset_of_downloadButton_18() { return static_cast<int32_t>(offsetof(UtageUguiTitle_t1483171752, ___downloadButton_18)); }
	inline GameObject_t1756533147 * get_downloadButton_18() const { return ___downloadButton_18; }
	inline GameObject_t1756533147 ** get_address_of_downloadButton_18() { return &___downloadButton_18; }
	inline void set_downloadButton_18(GameObject_t1756533147 * value)
	{
		___downloadButton_18 = value;
		Il2CppCodeGenWriteBarrier((&___downloadButton_18), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UTAGEUGUITITLE_T1483171752_H
#ifndef UTAGEUGUISCENEGALLERY_T811869004_H
#define UTAGEUGUISCENEGALLERY_T811869004_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UtageUguiSceneGallery
struct  UtageUguiSceneGallery_t811869004  : public UguiView_t2174908005
{
public:
	// Utage.UguiCategoryGridPage UtageUguiSceneGallery::categoryGridPage
	UguiCategoryGridPage_t673822931 * ___categoryGridPage_12;
	// UtageUguiGallery UtageUguiSceneGallery::gallery
	UtageUguiGallery_t4263677524 * ___gallery_13;
	// UtageUguiMainGame UtageUguiSceneGallery::mainGame
	UtageUguiMainGame_t4178963699 * ___mainGame_14;
	// Utage.AdvEngine UtageUguiSceneGallery::engine
	AdvEngine_t1176753927 * ___engine_15;
	// System.Boolean UtageUguiSceneGallery::isInit
	bool ___isInit_16;
	// System.Collections.Generic.List`1<Utage.AdvSceneGallerySettingData> UtageUguiSceneGallery::itemDataList
	List_1_t649724181 * ___itemDataList_17;

public:
	inline static int32_t get_offset_of_categoryGridPage_12() { return static_cast<int32_t>(offsetof(UtageUguiSceneGallery_t811869004, ___categoryGridPage_12)); }
	inline UguiCategoryGridPage_t673822931 * get_categoryGridPage_12() const { return ___categoryGridPage_12; }
	inline UguiCategoryGridPage_t673822931 ** get_address_of_categoryGridPage_12() { return &___categoryGridPage_12; }
	inline void set_categoryGridPage_12(UguiCategoryGridPage_t673822931 * value)
	{
		___categoryGridPage_12 = value;
		Il2CppCodeGenWriteBarrier((&___categoryGridPage_12), value);
	}

	inline static int32_t get_offset_of_gallery_13() { return static_cast<int32_t>(offsetof(UtageUguiSceneGallery_t811869004, ___gallery_13)); }
	inline UtageUguiGallery_t4263677524 * get_gallery_13() const { return ___gallery_13; }
	inline UtageUguiGallery_t4263677524 ** get_address_of_gallery_13() { return &___gallery_13; }
	inline void set_gallery_13(UtageUguiGallery_t4263677524 * value)
	{
		___gallery_13 = value;
		Il2CppCodeGenWriteBarrier((&___gallery_13), value);
	}

	inline static int32_t get_offset_of_mainGame_14() { return static_cast<int32_t>(offsetof(UtageUguiSceneGallery_t811869004, ___mainGame_14)); }
	inline UtageUguiMainGame_t4178963699 * get_mainGame_14() const { return ___mainGame_14; }
	inline UtageUguiMainGame_t4178963699 ** get_address_of_mainGame_14() { return &___mainGame_14; }
	inline void set_mainGame_14(UtageUguiMainGame_t4178963699 * value)
	{
		___mainGame_14 = value;
		Il2CppCodeGenWriteBarrier((&___mainGame_14), value);
	}

	inline static int32_t get_offset_of_engine_15() { return static_cast<int32_t>(offsetof(UtageUguiSceneGallery_t811869004, ___engine_15)); }
	inline AdvEngine_t1176753927 * get_engine_15() const { return ___engine_15; }
	inline AdvEngine_t1176753927 ** get_address_of_engine_15() { return &___engine_15; }
	inline void set_engine_15(AdvEngine_t1176753927 * value)
	{
		___engine_15 = value;
		Il2CppCodeGenWriteBarrier((&___engine_15), value);
	}

	inline static int32_t get_offset_of_isInit_16() { return static_cast<int32_t>(offsetof(UtageUguiSceneGallery_t811869004, ___isInit_16)); }
	inline bool get_isInit_16() const { return ___isInit_16; }
	inline bool* get_address_of_isInit_16() { return &___isInit_16; }
	inline void set_isInit_16(bool value)
	{
		___isInit_16 = value;
	}

	inline static int32_t get_offset_of_itemDataList_17() { return static_cast<int32_t>(offsetof(UtageUguiSceneGallery_t811869004, ___itemDataList_17)); }
	inline List_1_t649724181 * get_itemDataList_17() const { return ___itemDataList_17; }
	inline List_1_t649724181 ** get_address_of_itemDataList_17() { return &___itemDataList_17; }
	inline void set_itemDataList_17(List_1_t649724181 * value)
	{
		___itemDataList_17 = value;
		Il2CppCodeGenWriteBarrier((&___itemDataList_17), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UTAGEUGUISCENEGALLERY_T811869004_H
#ifndef UTAGEUGUICONFIG_T3500199096_H
#define UTAGEUGUICONFIG_T3500199096_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UtageUguiConfig
struct  UtageUguiConfig_t3500199096  : public UguiView_t2174908005
{
public:
	// Utage.AdvEngine UtageUguiConfig::engine
	AdvEngine_t1176753927 * ___engine_12;
	// UtageUguiTitle UtageUguiConfig::title
	UtageUguiTitle_t1483171752 * ___title_13;
	// UnityEngine.UI.Toggle UtageUguiConfig::checkFullscreen
	Toggle_t3976754468 * ___checkFullscreen_14;
	// UnityEngine.UI.Toggle UtageUguiConfig::checkMouseWheel
	Toggle_t3976754468 * ___checkMouseWheel_15;
	// UnityEngine.UI.Toggle UtageUguiConfig::checkSkipUnread
	Toggle_t3976754468 * ___checkSkipUnread_16;
	// UnityEngine.UI.Toggle UtageUguiConfig::checkStopSkipInSelection
	Toggle_t3976754468 * ___checkStopSkipInSelection_17;
	// UnityEngine.UI.Toggle UtageUguiConfig::checkHideMessageWindowOnPlyaingVoice
	Toggle_t3976754468 * ___checkHideMessageWindowOnPlyaingVoice_18;
	// UnityEngine.UI.Slider UtageUguiConfig::sliderMessageSpeed
	Slider_t297367283 * ___sliderMessageSpeed_19;
	// UnityEngine.UI.Slider UtageUguiConfig::sliderMessageSpeedRead
	Slider_t297367283 * ___sliderMessageSpeedRead_20;
	// UnityEngine.UI.Slider UtageUguiConfig::sliderAutoBrPageSpeed
	Slider_t297367283 * ___sliderAutoBrPageSpeed_21;
	// UnityEngine.UI.Slider UtageUguiConfig::sliderMessageWindowTransparency
	Slider_t297367283 * ___sliderMessageWindowTransparency_22;
	// UnityEngine.UI.Slider UtageUguiConfig::sliderSoundMasterVolume
	Slider_t297367283 * ___sliderSoundMasterVolume_23;
	// UnityEngine.UI.Slider UtageUguiConfig::sliderBgmVolume
	Slider_t297367283 * ___sliderBgmVolume_24;
	// UnityEngine.UI.Slider UtageUguiConfig::sliderSeVolume
	Slider_t297367283 * ___sliderSeVolume_25;
	// UnityEngine.UI.Slider UtageUguiConfig::sliderAmbienceVolume
	Slider_t297367283 * ___sliderAmbienceVolume_26;
	// UnityEngine.UI.Slider UtageUguiConfig::sliderVoiceVolume
	Slider_t297367283 * ___sliderVoiceVolume_27;
	// Utage.UguiToggleGroupIndexed UtageUguiConfig::radioButtonsVoiceStopType
	UguiToggleGroupIndexed_t999166452 * ___radioButtonsVoiceStopType_28;
	// System.Collections.Generic.List`1<UtageUguiConfig/TagedMasterVolumSliders> UtageUguiConfig::tagedMasterVolumSliders
	List_1_t3029804307 * ___tagedMasterVolumSliders_29;
	// System.Boolean UtageUguiConfig::isInit
	bool ___isInit_30;

public:
	inline static int32_t get_offset_of_engine_12() { return static_cast<int32_t>(offsetof(UtageUguiConfig_t3500199096, ___engine_12)); }
	inline AdvEngine_t1176753927 * get_engine_12() const { return ___engine_12; }
	inline AdvEngine_t1176753927 ** get_address_of_engine_12() { return &___engine_12; }
	inline void set_engine_12(AdvEngine_t1176753927 * value)
	{
		___engine_12 = value;
		Il2CppCodeGenWriteBarrier((&___engine_12), value);
	}

	inline static int32_t get_offset_of_title_13() { return static_cast<int32_t>(offsetof(UtageUguiConfig_t3500199096, ___title_13)); }
	inline UtageUguiTitle_t1483171752 * get_title_13() const { return ___title_13; }
	inline UtageUguiTitle_t1483171752 ** get_address_of_title_13() { return &___title_13; }
	inline void set_title_13(UtageUguiTitle_t1483171752 * value)
	{
		___title_13 = value;
		Il2CppCodeGenWriteBarrier((&___title_13), value);
	}

	inline static int32_t get_offset_of_checkFullscreen_14() { return static_cast<int32_t>(offsetof(UtageUguiConfig_t3500199096, ___checkFullscreen_14)); }
	inline Toggle_t3976754468 * get_checkFullscreen_14() const { return ___checkFullscreen_14; }
	inline Toggle_t3976754468 ** get_address_of_checkFullscreen_14() { return &___checkFullscreen_14; }
	inline void set_checkFullscreen_14(Toggle_t3976754468 * value)
	{
		___checkFullscreen_14 = value;
		Il2CppCodeGenWriteBarrier((&___checkFullscreen_14), value);
	}

	inline static int32_t get_offset_of_checkMouseWheel_15() { return static_cast<int32_t>(offsetof(UtageUguiConfig_t3500199096, ___checkMouseWheel_15)); }
	inline Toggle_t3976754468 * get_checkMouseWheel_15() const { return ___checkMouseWheel_15; }
	inline Toggle_t3976754468 ** get_address_of_checkMouseWheel_15() { return &___checkMouseWheel_15; }
	inline void set_checkMouseWheel_15(Toggle_t3976754468 * value)
	{
		___checkMouseWheel_15 = value;
		Il2CppCodeGenWriteBarrier((&___checkMouseWheel_15), value);
	}

	inline static int32_t get_offset_of_checkSkipUnread_16() { return static_cast<int32_t>(offsetof(UtageUguiConfig_t3500199096, ___checkSkipUnread_16)); }
	inline Toggle_t3976754468 * get_checkSkipUnread_16() const { return ___checkSkipUnread_16; }
	inline Toggle_t3976754468 ** get_address_of_checkSkipUnread_16() { return &___checkSkipUnread_16; }
	inline void set_checkSkipUnread_16(Toggle_t3976754468 * value)
	{
		___checkSkipUnread_16 = value;
		Il2CppCodeGenWriteBarrier((&___checkSkipUnread_16), value);
	}

	inline static int32_t get_offset_of_checkStopSkipInSelection_17() { return static_cast<int32_t>(offsetof(UtageUguiConfig_t3500199096, ___checkStopSkipInSelection_17)); }
	inline Toggle_t3976754468 * get_checkStopSkipInSelection_17() const { return ___checkStopSkipInSelection_17; }
	inline Toggle_t3976754468 ** get_address_of_checkStopSkipInSelection_17() { return &___checkStopSkipInSelection_17; }
	inline void set_checkStopSkipInSelection_17(Toggle_t3976754468 * value)
	{
		___checkStopSkipInSelection_17 = value;
		Il2CppCodeGenWriteBarrier((&___checkStopSkipInSelection_17), value);
	}

	inline static int32_t get_offset_of_checkHideMessageWindowOnPlyaingVoice_18() { return static_cast<int32_t>(offsetof(UtageUguiConfig_t3500199096, ___checkHideMessageWindowOnPlyaingVoice_18)); }
	inline Toggle_t3976754468 * get_checkHideMessageWindowOnPlyaingVoice_18() const { return ___checkHideMessageWindowOnPlyaingVoice_18; }
	inline Toggle_t3976754468 ** get_address_of_checkHideMessageWindowOnPlyaingVoice_18() { return &___checkHideMessageWindowOnPlyaingVoice_18; }
	inline void set_checkHideMessageWindowOnPlyaingVoice_18(Toggle_t3976754468 * value)
	{
		___checkHideMessageWindowOnPlyaingVoice_18 = value;
		Il2CppCodeGenWriteBarrier((&___checkHideMessageWindowOnPlyaingVoice_18), value);
	}

	inline static int32_t get_offset_of_sliderMessageSpeed_19() { return static_cast<int32_t>(offsetof(UtageUguiConfig_t3500199096, ___sliderMessageSpeed_19)); }
	inline Slider_t297367283 * get_sliderMessageSpeed_19() const { return ___sliderMessageSpeed_19; }
	inline Slider_t297367283 ** get_address_of_sliderMessageSpeed_19() { return &___sliderMessageSpeed_19; }
	inline void set_sliderMessageSpeed_19(Slider_t297367283 * value)
	{
		___sliderMessageSpeed_19 = value;
		Il2CppCodeGenWriteBarrier((&___sliderMessageSpeed_19), value);
	}

	inline static int32_t get_offset_of_sliderMessageSpeedRead_20() { return static_cast<int32_t>(offsetof(UtageUguiConfig_t3500199096, ___sliderMessageSpeedRead_20)); }
	inline Slider_t297367283 * get_sliderMessageSpeedRead_20() const { return ___sliderMessageSpeedRead_20; }
	inline Slider_t297367283 ** get_address_of_sliderMessageSpeedRead_20() { return &___sliderMessageSpeedRead_20; }
	inline void set_sliderMessageSpeedRead_20(Slider_t297367283 * value)
	{
		___sliderMessageSpeedRead_20 = value;
		Il2CppCodeGenWriteBarrier((&___sliderMessageSpeedRead_20), value);
	}

	inline static int32_t get_offset_of_sliderAutoBrPageSpeed_21() { return static_cast<int32_t>(offsetof(UtageUguiConfig_t3500199096, ___sliderAutoBrPageSpeed_21)); }
	inline Slider_t297367283 * get_sliderAutoBrPageSpeed_21() const { return ___sliderAutoBrPageSpeed_21; }
	inline Slider_t297367283 ** get_address_of_sliderAutoBrPageSpeed_21() { return &___sliderAutoBrPageSpeed_21; }
	inline void set_sliderAutoBrPageSpeed_21(Slider_t297367283 * value)
	{
		___sliderAutoBrPageSpeed_21 = value;
		Il2CppCodeGenWriteBarrier((&___sliderAutoBrPageSpeed_21), value);
	}

	inline static int32_t get_offset_of_sliderMessageWindowTransparency_22() { return static_cast<int32_t>(offsetof(UtageUguiConfig_t3500199096, ___sliderMessageWindowTransparency_22)); }
	inline Slider_t297367283 * get_sliderMessageWindowTransparency_22() const { return ___sliderMessageWindowTransparency_22; }
	inline Slider_t297367283 ** get_address_of_sliderMessageWindowTransparency_22() { return &___sliderMessageWindowTransparency_22; }
	inline void set_sliderMessageWindowTransparency_22(Slider_t297367283 * value)
	{
		___sliderMessageWindowTransparency_22 = value;
		Il2CppCodeGenWriteBarrier((&___sliderMessageWindowTransparency_22), value);
	}

	inline static int32_t get_offset_of_sliderSoundMasterVolume_23() { return static_cast<int32_t>(offsetof(UtageUguiConfig_t3500199096, ___sliderSoundMasterVolume_23)); }
	inline Slider_t297367283 * get_sliderSoundMasterVolume_23() const { return ___sliderSoundMasterVolume_23; }
	inline Slider_t297367283 ** get_address_of_sliderSoundMasterVolume_23() { return &___sliderSoundMasterVolume_23; }
	inline void set_sliderSoundMasterVolume_23(Slider_t297367283 * value)
	{
		___sliderSoundMasterVolume_23 = value;
		Il2CppCodeGenWriteBarrier((&___sliderSoundMasterVolume_23), value);
	}

	inline static int32_t get_offset_of_sliderBgmVolume_24() { return static_cast<int32_t>(offsetof(UtageUguiConfig_t3500199096, ___sliderBgmVolume_24)); }
	inline Slider_t297367283 * get_sliderBgmVolume_24() const { return ___sliderBgmVolume_24; }
	inline Slider_t297367283 ** get_address_of_sliderBgmVolume_24() { return &___sliderBgmVolume_24; }
	inline void set_sliderBgmVolume_24(Slider_t297367283 * value)
	{
		___sliderBgmVolume_24 = value;
		Il2CppCodeGenWriteBarrier((&___sliderBgmVolume_24), value);
	}

	inline static int32_t get_offset_of_sliderSeVolume_25() { return static_cast<int32_t>(offsetof(UtageUguiConfig_t3500199096, ___sliderSeVolume_25)); }
	inline Slider_t297367283 * get_sliderSeVolume_25() const { return ___sliderSeVolume_25; }
	inline Slider_t297367283 ** get_address_of_sliderSeVolume_25() { return &___sliderSeVolume_25; }
	inline void set_sliderSeVolume_25(Slider_t297367283 * value)
	{
		___sliderSeVolume_25 = value;
		Il2CppCodeGenWriteBarrier((&___sliderSeVolume_25), value);
	}

	inline static int32_t get_offset_of_sliderAmbienceVolume_26() { return static_cast<int32_t>(offsetof(UtageUguiConfig_t3500199096, ___sliderAmbienceVolume_26)); }
	inline Slider_t297367283 * get_sliderAmbienceVolume_26() const { return ___sliderAmbienceVolume_26; }
	inline Slider_t297367283 ** get_address_of_sliderAmbienceVolume_26() { return &___sliderAmbienceVolume_26; }
	inline void set_sliderAmbienceVolume_26(Slider_t297367283 * value)
	{
		___sliderAmbienceVolume_26 = value;
		Il2CppCodeGenWriteBarrier((&___sliderAmbienceVolume_26), value);
	}

	inline static int32_t get_offset_of_sliderVoiceVolume_27() { return static_cast<int32_t>(offsetof(UtageUguiConfig_t3500199096, ___sliderVoiceVolume_27)); }
	inline Slider_t297367283 * get_sliderVoiceVolume_27() const { return ___sliderVoiceVolume_27; }
	inline Slider_t297367283 ** get_address_of_sliderVoiceVolume_27() { return &___sliderVoiceVolume_27; }
	inline void set_sliderVoiceVolume_27(Slider_t297367283 * value)
	{
		___sliderVoiceVolume_27 = value;
		Il2CppCodeGenWriteBarrier((&___sliderVoiceVolume_27), value);
	}

	inline static int32_t get_offset_of_radioButtonsVoiceStopType_28() { return static_cast<int32_t>(offsetof(UtageUguiConfig_t3500199096, ___radioButtonsVoiceStopType_28)); }
	inline UguiToggleGroupIndexed_t999166452 * get_radioButtonsVoiceStopType_28() const { return ___radioButtonsVoiceStopType_28; }
	inline UguiToggleGroupIndexed_t999166452 ** get_address_of_radioButtonsVoiceStopType_28() { return &___radioButtonsVoiceStopType_28; }
	inline void set_radioButtonsVoiceStopType_28(UguiToggleGroupIndexed_t999166452 * value)
	{
		___radioButtonsVoiceStopType_28 = value;
		Il2CppCodeGenWriteBarrier((&___radioButtonsVoiceStopType_28), value);
	}

	inline static int32_t get_offset_of_tagedMasterVolumSliders_29() { return static_cast<int32_t>(offsetof(UtageUguiConfig_t3500199096, ___tagedMasterVolumSliders_29)); }
	inline List_1_t3029804307 * get_tagedMasterVolumSliders_29() const { return ___tagedMasterVolumSliders_29; }
	inline List_1_t3029804307 ** get_address_of_tagedMasterVolumSliders_29() { return &___tagedMasterVolumSliders_29; }
	inline void set_tagedMasterVolumSliders_29(List_1_t3029804307 * value)
	{
		___tagedMasterVolumSliders_29 = value;
		Il2CppCodeGenWriteBarrier((&___tagedMasterVolumSliders_29), value);
	}

	inline static int32_t get_offset_of_isInit_30() { return static_cast<int32_t>(offsetof(UtageUguiConfig_t3500199096, ___isInit_30)); }
	inline bool get_isInit_30() const { return ___isInit_30; }
	inline bool* get_address_of_isInit_30() { return &___isInit_30; }
	inline void set_isInit_30(bool value)
	{
		___isInit_30 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UTAGEUGUICONFIG_T3500199096_H
#ifndef LIVE2DLIPSYNCH_T3265978124_H
#define LIVE2DLIPSYNCH_T3265978124_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.Live2DLipSynch
struct  Live2DLipSynch_t3265978124  : public LipSynchBase_t2376160345
{
public:
	// Live2D.Cubism.Framework.MouthMovement.CubismMouthController Utage.Live2DLipSynch::controller
	CubismMouthController_t3330055306 * ___controller_9;
	// Live2D.Cubism.Framework.MouthMovement.CubismAudioMouthInput Utage.Live2DLipSynch::_audio
	CubismAudioMouthInput_t1379032486 * ____audio_10;
	// Live2D.Cubism.Framework.MouthMovement.CubismAutoMouthInput Utage.Live2DLipSynch::auto
	CubismAutoMouthInput_t1722567851 * ___auto_11;

public:
	inline static int32_t get_offset_of_controller_9() { return static_cast<int32_t>(offsetof(Live2DLipSynch_t3265978124, ___controller_9)); }
	inline CubismMouthController_t3330055306 * get_controller_9() const { return ___controller_9; }
	inline CubismMouthController_t3330055306 ** get_address_of_controller_9() { return &___controller_9; }
	inline void set_controller_9(CubismMouthController_t3330055306 * value)
	{
		___controller_9 = value;
		Il2CppCodeGenWriteBarrier((&___controller_9), value);
	}

	inline static int32_t get_offset_of__audio_10() { return static_cast<int32_t>(offsetof(Live2DLipSynch_t3265978124, ____audio_10)); }
	inline CubismAudioMouthInput_t1379032486 * get__audio_10() const { return ____audio_10; }
	inline CubismAudioMouthInput_t1379032486 ** get_address_of__audio_10() { return &____audio_10; }
	inline void set__audio_10(CubismAudioMouthInput_t1379032486 * value)
	{
		____audio_10 = value;
		Il2CppCodeGenWriteBarrier((&____audio_10), value);
	}

	inline static int32_t get_offset_of_auto_11() { return static_cast<int32_t>(offsetof(Live2DLipSynch_t3265978124, ___auto_11)); }
	inline CubismAutoMouthInput_t1722567851 * get_auto_11() const { return ___auto_11; }
	inline CubismAutoMouthInput_t1722567851 ** get_address_of_auto_11() { return &___auto_11; }
	inline void set_auto_11(CubismAutoMouthInput_t1722567851 * value)
	{
		___auto_11 = value;
		Il2CppCodeGenWriteBarrier((&___auto_11), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LIVE2DLIPSYNCH_T3265978124_H





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2900 = { sizeof (Pivot_t1385159954)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2900[10] = 
{
	Pivot_t1385159954::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2901 = { sizeof (PivotUtil_t3576025374), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2902 = { sizeof (TimeUtil_t3155251811), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2903 = { sizeof (UtageToolKit_t285960976), -1, sizeof(UtageToolKit_t285960976_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2903[1] = 
{
	UtageToolKit_t285960976_StaticFields::get_offset_of_cultureInfJp_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2904 = { sizeof (U3CAddEventTriggerEntryU3Ec__AnonStorey0_t880254321), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2904[1] = 
{
	U3CAddEventTriggerEntryU3Ec__AnonStorey0_t880254321::get_offset_of_action_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2905 = { sizeof (WrapperMoviePlayer_t3887243686), -1, sizeof(WrapperMoviePlayer_t3887243686_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2905[9] = 
{
	WrapperMoviePlayer_t3887243686_StaticFields::get_offset_of_instance_2(),
	WrapperMoviePlayer_t3887243686::get_offset_of_isPlaying_3(),
	WrapperMoviePlayer_t3887243686::get_offset_of_cancel_4(),
	WrapperMoviePlayer_t3887243686::get_offset_of_bgColor_5(),
	WrapperMoviePlayer_t3887243686::get_offset_of_ignoreCancel_6(),
	WrapperMoviePlayer_t3887243686::get_offset_of_cancelFadeTime_7(),
	WrapperMoviePlayer_t3887243686::get_offset_of_renderTarget_8(),
	WrapperMoviePlayer_t3887243686::get_offset_of_overrideRootDirectory_9(),
	WrapperMoviePlayer_t3887243686::get_offset_of_rootDirectory_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2906 = { sizeof (U3CCoPlayMobileMovieU3Ec__Iterator0_t154985798), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2906[5] = 
{
	U3CCoPlayMobileMovieU3Ec__Iterator0_t154985798::get_offset_of_path_0(),
	U3CCoPlayMobileMovieU3Ec__Iterator0_t154985798::get_offset_of_U24this_1(),
	U3CCoPlayMobileMovieU3Ec__Iterator0_t154985798::get_offset_of_U24current_2(),
	U3CCoPlayMobileMovieU3Ec__Iterator0_t154985798::get_offset_of_U24disposing_3(),
	U3CCoPlayMobileMovieU3Ec__Iterator0_t154985798::get_offset_of_U24PC_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2907 = { sizeof (WrapperUnityVersion_t3346077438), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2908 = { sizeof (UtageUguiCgGallery_t1507549878), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2908[6] = 
{
	UtageUguiCgGallery_t1507549878::get_offset_of_gallery_12(),
	UtageUguiCgGallery_t1507549878::get_offset_of_CgView_13(),
	UtageUguiCgGallery_t1507549878::get_offset_of_categoryGridPage_14(),
	UtageUguiCgGallery_t1507549878::get_offset_of_itemDataList_15(),
	UtageUguiCgGallery_t1507549878::get_offset_of_engine_16(),
	UtageUguiCgGallery_t1507549878::get_offset_of_isInit_17(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2909 = { sizeof (U3CCoWaitOpenU3Ec__Iterator0_t771116017), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2909[4] = 
{
	U3CCoWaitOpenU3Ec__Iterator0_t771116017::get_offset_of_U24this_0(),
	U3CCoWaitOpenU3Ec__Iterator0_t771116017::get_offset_of_U24current_1(),
	U3CCoWaitOpenU3Ec__Iterator0_t771116017::get_offset_of_U24disposing_2(),
	U3CCoWaitOpenU3Ec__Iterator0_t771116017::get_offset_of_U24PC_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2910 = { sizeof (UtageUguiCgGalleryItem_t1446744351), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2910[3] = 
{
	UtageUguiCgGalleryItem_t1446744351::get_offset_of_texture_2(),
	UtageUguiCgGalleryItem_t1446744351::get_offset_of_count_3(),
	UtageUguiCgGalleryItem_t1446744351::get_offset_of_data_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2911 = { sizeof (U3CInitU3Ec__AnonStorey0_t168833639), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2911[2] = 
{
	U3CInitU3Ec__AnonStorey0_t168833639::get_offset_of_ButtonClickedEvent_0(),
	U3CInitU3Ec__AnonStorey0_t168833639::get_offset_of_U24this_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2912 = { sizeof (UtageUguiCgGalleryViewer_t3438583972), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2912[9] = 
{
	UtageUguiCgGalleryViewer_t3438583972::get_offset_of_gallery_12(),
	UtageUguiCgGalleryViewer_t3438583972::get_offset_of_texture_13(),
	UtageUguiCgGalleryViewer_t3438583972::get_offset_of_engine_14(),
	UtageUguiCgGalleryViewer_t3438583972::get_offset_of_scrollRect_15(),
	UtageUguiCgGalleryViewer_t3438583972::get_offset_of_startContentPosition_16(),
	UtageUguiCgGalleryViewer_t3438583972::get_offset_of_isEnableClick_17(),
	UtageUguiCgGalleryViewer_t3438583972::get_offset_of_isLoadEnd_18(),
	UtageUguiCgGalleryViewer_t3438583972::get_offset_of_data_19(),
	UtageUguiCgGalleryViewer_t3438583972::get_offset_of_currentIndex_20(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2913 = { sizeof (UtageUguiGallery_t4263677524), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2913[2] = 
{
	UtageUguiGallery_t4263677524::get_offset_of_views_12(),
	UtageUguiGallery_t4263677524::get_offset_of_tabIndex_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2914 = { sizeof (UtageUguiSceneGallery_t811869004), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2914[6] = 
{
	UtageUguiSceneGallery_t811869004::get_offset_of_categoryGridPage_12(),
	UtageUguiSceneGallery_t811869004::get_offset_of_gallery_13(),
	UtageUguiSceneGallery_t811869004::get_offset_of_mainGame_14(),
	UtageUguiSceneGallery_t811869004::get_offset_of_engine_15(),
	UtageUguiSceneGallery_t811869004::get_offset_of_isInit_16(),
	UtageUguiSceneGallery_t811869004::get_offset_of_itemDataList_17(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2915 = { sizeof (U3CCoWaitOpenU3Ec__Iterator0_t710238179), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2915[4] = 
{
	U3CCoWaitOpenU3Ec__Iterator0_t710238179::get_offset_of_U24this_0(),
	U3CCoWaitOpenU3Ec__Iterator0_t710238179::get_offset_of_U24current_1(),
	U3CCoWaitOpenU3Ec__Iterator0_t710238179::get_offset_of_U24disposing_2(),
	U3CCoWaitOpenU3Ec__Iterator0_t710238179::get_offset_of_U24PC_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2916 = { sizeof (UtageUguiSceneGalleryItem_t228413713), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2916[3] = 
{
	UtageUguiSceneGalleryItem_t228413713::get_offset_of_texture_2(),
	UtageUguiSceneGalleryItem_t228413713::get_offset_of_title_3(),
	UtageUguiSceneGalleryItem_t228413713::get_offset_of_data_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2917 = { sizeof (U3CInitU3Ec__AnonStorey0_t2161906049), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2917[2] = 
{
	U3CInitU3Ec__AnonStorey0_t2161906049::get_offset_of_ButtonClickedEvent_0(),
	U3CInitU3Ec__AnonStorey0_t2161906049::get_offset_of_U24this_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2918 = { sizeof (UtageUguiSoundRoom_t2656581854), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2918[6] = 
{
	UtageUguiSoundRoom_t2656581854::get_offset_of_gallery_12(),
	UtageUguiSoundRoom_t2656581854::get_offset_of_listView_13(),
	UtageUguiSoundRoom_t2656581854::get_offset_of_itemDataList_14(),
	UtageUguiSoundRoom_t2656581854::get_offset_of_engine_15(),
	UtageUguiSoundRoom_t2656581854::get_offset_of_isInit_16(),
	UtageUguiSoundRoom_t2656581854::get_offset_of_isChangedBgm_17(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2919 = { sizeof (U3CCoWaitOpenU3Ec__Iterator0_t3263074609), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2919[4] = 
{
	U3CCoWaitOpenU3Ec__Iterator0_t3263074609::get_offset_of_U24this_0(),
	U3CCoWaitOpenU3Ec__Iterator0_t3263074609::get_offset_of_U24current_1(),
	U3CCoWaitOpenU3Ec__Iterator0_t3263074609::get_offset_of_U24disposing_2(),
	U3CCoWaitOpenU3Ec__Iterator0_t3263074609::get_offset_of_U24PC_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2920 = { sizeof (U3CCoPlaySoundU3Ec__Iterator1_t3028347198), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2920[6] = 
{
	U3CCoPlaySoundU3Ec__Iterator1_t3028347198::get_offset_of_path_0(),
	U3CCoPlaySoundU3Ec__Iterator1_t3028347198::get_offset_of_U3CfileU3E__0_1(),
	U3CCoPlaySoundU3Ec__Iterator1_t3028347198::get_offset_of_U24this_2(),
	U3CCoPlaySoundU3Ec__Iterator1_t3028347198::get_offset_of_U24current_3(),
	U3CCoPlaySoundU3Ec__Iterator1_t3028347198::get_offset_of_U24disposing_4(),
	U3CCoPlaySoundU3Ec__Iterator1_t3028347198::get_offset_of_U24PC_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2921 = { sizeof (UtageUguiSoundRoomItem_t3977840991), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2921[2] = 
{
	UtageUguiSoundRoomItem_t3977840991::get_offset_of_title_2(),
	UtageUguiSoundRoomItem_t3977840991::get_offset_of_data_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2922 = { sizeof (U3CInitU3Ec__AnonStorey0_t1904244095), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2922[2] = 
{
	U3CInitU3Ec__AnonStorey0_t1904244095::get_offset_of_ButtonClickedEvent_0(),
	U3CInitU3Ec__AnonStorey0_t1904244095::get_offset_of_U24this_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2923 = { sizeof (UtageUguiBoot_t1438884946), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2923[7] = 
{
	UtageUguiBoot_t1438884946::get_offset_of_engine_12(),
	UtageUguiBoot_t1438884946::get_offset_of_fadeTextureStream_13(),
	UtageUguiBoot_t1438884946::get_offset_of_title_14(),
	UtageUguiBoot_t1438884946::get_offset_of_loadWait_15(),
	UtageUguiBoot_t1438884946::get_offset_of_isWaitBoot_16(),
	UtageUguiBoot_t1438884946::get_offset_of_isWaitDownLoad_17(),
	UtageUguiBoot_t1438884946::get_offset_of_isWaitSplashScreen_18(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2924 = { sizeof (U3CCoUpdateU3Ec__Iterator0_t3541358793), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2924[4] = 
{
	U3CCoUpdateU3Ec__Iterator0_t3541358793::get_offset_of_U24this_0(),
	U3CCoUpdateU3Ec__Iterator0_t3541358793::get_offset_of_U24current_1(),
	U3CCoUpdateU3Ec__Iterator0_t3541358793::get_offset_of_U24disposing_2(),
	U3CCoUpdateU3Ec__Iterator0_t3541358793::get_offset_of_U24PC_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2925 = { sizeof (UtageUguiConfig_t3500199096), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2925[19] = 
{
	UtageUguiConfig_t3500199096::get_offset_of_engine_12(),
	UtageUguiConfig_t3500199096::get_offset_of_title_13(),
	UtageUguiConfig_t3500199096::get_offset_of_checkFullscreen_14(),
	UtageUguiConfig_t3500199096::get_offset_of_checkMouseWheel_15(),
	UtageUguiConfig_t3500199096::get_offset_of_checkSkipUnread_16(),
	UtageUguiConfig_t3500199096::get_offset_of_checkStopSkipInSelection_17(),
	UtageUguiConfig_t3500199096::get_offset_of_checkHideMessageWindowOnPlyaingVoice_18(),
	UtageUguiConfig_t3500199096::get_offset_of_sliderMessageSpeed_19(),
	UtageUguiConfig_t3500199096::get_offset_of_sliderMessageSpeedRead_20(),
	UtageUguiConfig_t3500199096::get_offset_of_sliderAutoBrPageSpeed_21(),
	UtageUguiConfig_t3500199096::get_offset_of_sliderMessageWindowTransparency_22(),
	UtageUguiConfig_t3500199096::get_offset_of_sliderSoundMasterVolume_23(),
	UtageUguiConfig_t3500199096::get_offset_of_sliderBgmVolume_24(),
	UtageUguiConfig_t3500199096::get_offset_of_sliderSeVolume_25(),
	UtageUguiConfig_t3500199096::get_offset_of_sliderAmbienceVolume_26(),
	UtageUguiConfig_t3500199096::get_offset_of_sliderVoiceVolume_27(),
	UtageUguiConfig_t3500199096::get_offset_of_radioButtonsVoiceStopType_28(),
	UtageUguiConfig_t3500199096::get_offset_of_tagedMasterVolumSliders_29(),
	UtageUguiConfig_t3500199096::get_offset_of_isInit_30(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2926 = { sizeof (TagedMasterVolumSliders_t3660683175), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2926[2] = 
{
	TagedMasterVolumSliders_t3660683175::get_offset_of_tag_0(),
	TagedMasterVolumSliders_t3660683175::get_offset_of_volumeSlider_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2927 = { sizeof (U3CCoWaitOpenU3Ec__Iterator0_t3688245207), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2927[4] = 
{
	U3CCoWaitOpenU3Ec__Iterator0_t3688245207::get_offset_of_U24this_0(),
	U3CCoWaitOpenU3Ec__Iterator0_t3688245207::get_offset_of_U24current_1(),
	U3CCoWaitOpenU3Ec__Iterator0_t3688245207::get_offset_of_U24disposing_2(),
	U3CCoWaitOpenU3Ec__Iterator0_t3688245207::get_offset_of_U24PC_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2928 = { sizeof (UtageUguiConfigTaggedMasterVolume_t3772792928), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2928[2] = 
{
	UtageUguiConfigTaggedMasterVolume_t3772792928::get_offset_of_volumeTag_2(),
	UtageUguiConfigTaggedMasterVolume_t3772792928::get_offset_of_config_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2929 = { sizeof (UtageUguiLoadWait_t319861767), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2929[15] = 
{
	UtageUguiLoadWait_t319861767::get_offset_of_engine_12(),
	UtageUguiLoadWait_t319861767::get_offset_of_starter_13(),
	UtageUguiLoadWait_t319861767::get_offset_of_isAutoCacheFileLoad_14(),
	UtageUguiLoadWait_t319861767::get_offset_of_title_15(),
	UtageUguiLoadWait_t319861767::get_offset_of_bootSceneName_16(),
	UtageUguiLoadWait_t319861767::get_offset_of_buttonSkip_17(),
	UtageUguiLoadWait_t319861767::get_offset_of_buttonBack_18(),
	UtageUguiLoadWait_t319861767::get_offset_of_buttonDownload_19(),
	UtageUguiLoadWait_t319861767::get_offset_of_loadingBarRoot_20(),
	UtageUguiLoadWait_t319861767::get_offset_of_loadingBar_21(),
	UtageUguiLoadWait_t319861767::get_offset_of_textMain_22(),
	UtageUguiLoadWait_t319861767::get_offset_of_textCount_23(),
	UtageUguiLoadWait_t319861767::get_offset_of_onOpenDialog_24(),
	UtageUguiLoadWait_t319861767::get_offset_of_U3CCurrentStateU3Ek__BackingField_25(),
	UtageUguiLoadWait_t319861767::get_offset_of_U3CDownloadTypeU3Ek__BackingField_26(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2930 = { sizeof (State_t3198281263)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2930[4] = 
{
	State_t3198281263::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2931 = { sizeof (Type_t2484462392)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2931[4] = 
{
	Type_t2484462392::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2932 = { sizeof (U3CCoUpdateLoadingU3Ec__Iterator0_t3542804446), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2932[7] = 
{
	U3CCoUpdateLoadingU3Ec__Iterator0_t3542804446::get_offset_of_U3CmaxCountDownLoadU3E__0_0(),
	U3CCoUpdateLoadingU3Ec__Iterator0_t3542804446::get_offset_of_U3CcountDownLoadingU3E__0_1(),
	U3CCoUpdateLoadingU3Ec__Iterator0_t3542804446::get_offset_of_U3CcountDownLoadedU3E__1_2(),
	U3CCoUpdateLoadingU3Ec__Iterator0_t3542804446::get_offset_of_U24this_3(),
	U3CCoUpdateLoadingU3Ec__Iterator0_t3542804446::get_offset_of_U24current_4(),
	U3CCoUpdateLoadingU3Ec__Iterator0_t3542804446::get_offset_of_U24disposing_5(),
	U3CCoUpdateLoadingU3Ec__Iterator0_t3542804446::get_offset_of_U24PC_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2933 = { sizeof (UtageUguiMainGame_t4178963699), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2933[13] = 
{
	UtageUguiMainGame_t4178963699::get_offset_of_engine_12(),
	UtageUguiMainGame_t4178963699::get_offset_of_letterBoxCamera_13(),
	UtageUguiMainGame_t4178963699::get_offset_of_title_14(),
	UtageUguiMainGame_t4178963699::get_offset_of_config_15(),
	UtageUguiMainGame_t4178963699::get_offset_of_saveLoad_16(),
	UtageUguiMainGame_t4178963699::get_offset_of_gallery_17(),
	UtageUguiMainGame_t4178963699::get_offset_of_buttons_18(),
	UtageUguiMainGame_t4178963699::get_offset_of_checkSkip_19(),
	UtageUguiMainGame_t4178963699::get_offset_of_checkAuto_20(),
	UtageUguiMainGame_t4178963699::get_offset_of_bootType_21(),
	UtageUguiMainGame_t4178963699::get_offset_of_loadData_22(),
	UtageUguiMainGame_t4178963699::get_offset_of_isInit_23(),
	UtageUguiMainGame_t4178963699::get_offset_of_scenarioLabel_24(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2934 = { sizeof (BootType_t3158257586)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2934[6] = 
{
	BootType_t3158257586::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2935 = { sizeof (U3CCoWaitOpenU3Ec__Iterator0_t2966467166), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2935[4] = 
{
	U3CCoWaitOpenU3Ec__Iterator0_t2966467166::get_offset_of_U24this_0(),
	U3CCoWaitOpenU3Ec__Iterator0_t2966467166::get_offset_of_U24current_1(),
	U3CCoWaitOpenU3Ec__Iterator0_t2966467166::get_offset_of_U24disposing_2(),
	U3CCoWaitOpenU3Ec__Iterator0_t2966467166::get_offset_of_U24PC_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2936 = { sizeof (U3CCoCaptureScreenU3Ec__Iterator1_t4205168204), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2936[4] = 
{
	U3CCoCaptureScreenU3Ec__Iterator1_t4205168204::get_offset_of_U24this_0(),
	U3CCoCaptureScreenU3Ec__Iterator1_t4205168204::get_offset_of_U24current_1(),
	U3CCoCaptureScreenU3Ec__Iterator1_t4205168204::get_offset_of_U24disposing_2(),
	U3CCoCaptureScreenU3Ec__Iterator1_t4205168204::get_offset_of_U24PC_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2937 = { sizeof (U3CCoSaveU3Ec__Iterator2_t3798212420), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2937[4] = 
{
	U3CCoSaveU3Ec__Iterator2_t3798212420::get_offset_of_U24this_0(),
	U3CCoSaveU3Ec__Iterator2_t3798212420::get_offset_of_U24current_1(),
	U3CCoSaveU3Ec__Iterator2_t3798212420::get_offset_of_U24disposing_2(),
	U3CCoSaveU3Ec__Iterator2_t3798212420::get_offset_of_U24PC_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2938 = { sizeof (U3CCoQSaveU3Ec__Iterator3_t2917206258), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2938[4] = 
{
	U3CCoQSaveU3Ec__Iterator3_t2917206258::get_offset_of_U24this_0(),
	U3CCoQSaveU3Ec__Iterator3_t2917206258::get_offset_of_U24current_1(),
	U3CCoQSaveU3Ec__Iterator3_t2917206258::get_offset_of_U24disposing_2(),
	U3CCoQSaveU3Ec__Iterator3_t2917206258::get_offset_of_U24PC_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2939 = { sizeof (UtageUguiSaveLoad_t130048949), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2939[9] = 
{
	UtageUguiSaveLoad_t130048949::get_offset_of_gridPage_12(),
	UtageUguiSaveLoad_t130048949::get_offset_of_itemDataList_13(),
	UtageUguiSaveLoad_t130048949::get_offset_of_engine_14(),
	UtageUguiSaveLoad_t130048949::get_offset_of_mainGame_15(),
	UtageUguiSaveLoad_t130048949::get_offset_of_saveRoot_16(),
	UtageUguiSaveLoad_t130048949::get_offset_of_loadRoot_17(),
	UtageUguiSaveLoad_t130048949::get_offset_of_isSave_18(),
	UtageUguiSaveLoad_t130048949::get_offset_of_isInit_19(),
	UtageUguiSaveLoad_t130048949::get_offset_of_lastPage_20(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2940 = { sizeof (U3CCoWaitOpenU3Ec__Iterator0_t1585171652), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2940[6] = 
{
	U3CCoWaitOpenU3Ec__Iterator0_t1585171652::get_offset_of_U3CsaveManagerU3E__0_0(),
	U3CCoWaitOpenU3Ec__Iterator0_t1585171652::get_offset_of_U3ClistU3E__0_1(),
	U3CCoWaitOpenU3Ec__Iterator0_t1585171652::get_offset_of_U24this_2(),
	U3CCoWaitOpenU3Ec__Iterator0_t1585171652::get_offset_of_U24current_3(),
	U3CCoWaitOpenU3Ec__Iterator0_t1585171652::get_offset_of_U24disposing_4(),
	U3CCoWaitOpenU3Ec__Iterator0_t1585171652::get_offset_of_U24PC_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2941 = { sizeof (UtageUguiSaveLoadItem_t1111947312), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2941[10] = 
{
	UtageUguiSaveLoadItem_t1111947312::get_offset_of_text_2(),
	UtageUguiSaveLoadItem_t1111947312::get_offset_of_no_3(),
	UtageUguiSaveLoadItem_t1111947312::get_offset_of_date_4(),
	UtageUguiSaveLoadItem_t1111947312::get_offset_of_captureImage_5(),
	UtageUguiSaveLoadItem_t1111947312::get_offset_of_autoSaveIcon_6(),
	UtageUguiSaveLoadItem_t1111947312::get_offset_of_textEmpty_7(),
	UtageUguiSaveLoadItem_t1111947312::get_offset_of_button_8(),
	UtageUguiSaveLoadItem_t1111947312::get_offset_of_data_9(),
	UtageUguiSaveLoadItem_t1111947312::get_offset_of_index_10(),
	UtageUguiSaveLoadItem_t1111947312::get_offset_of_defaultColor_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2942 = { sizeof (U3CInitU3Ec__AnonStorey0_t1559879322), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2942[2] = 
{
	U3CInitU3Ec__AnonStorey0_t1559879322::get_offset_of_ButtonClickedEvent_0(),
	U3CInitU3Ec__AnonStorey0_t1559879322::get_offset_of_U24this_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2943 = { sizeof (UtageUguiSkipButtonState_t1473314082), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2943[2] = 
{
	UtageUguiSkipButtonState_t1473314082::get_offset_of_engine_2(),
	UtageUguiSkipButtonState_t1473314082::get_offset_of_target_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2944 = { sizeof (UtageUguiStartChapter_t1126311843), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2945 = { sizeof (UtageUguiTitle_t1483171752), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2945[7] = 
{
	UtageUguiTitle_t1483171752::get_offset_of_starter_12(),
	UtageUguiTitle_t1483171752::get_offset_of_mainGame_13(),
	UtageUguiTitle_t1483171752::get_offset_of_config_14(),
	UtageUguiTitle_t1483171752::get_offset_of_load_15(),
	UtageUguiTitle_t1483171752::get_offset_of_gallery_16(),
	UtageUguiTitle_t1483171752::get_offset_of_download_17(),
	UtageUguiTitle_t1483171752::get_offset_of_downloadButton_18(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2946 = { sizeof (VersionUtil_t3068462550), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2946[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2947 = { sizeof (AdvGraphicObjectLive2D_t716049902), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2947[4] = 
{
	AdvGraphicObjectLive2D_t716049902::get_offset_of_engine_2(),
	AdvGraphicObjectLive2D_t716049902::get_offset_of_advObj_3(),
	AdvGraphicObjectLive2D_t716049902::get_offset_of_renderController_4(),
	AdvGraphicObjectLive2D_t716049902::get_offset_of_lipSynch_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2948 = { sizeof (Live2DLipSynch_t3265978124), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2948[3] = 
{
	Live2DLipSynch_t3265978124::get_offset_of_controller_9(),
	Live2DLipSynch_t3265978124::get_offset_of__audio_10(),
	Live2DLipSynch_t3265978124::get_offset_of_auto_11(),
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
