﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "object-internals.h"

// System.Func`2<UnityEngine.UI.ILayoutElement,System.Single>
struct Func_2_t1976155184;
// UnityEngine.RectTransform
struct RectTransform_t3349966182;
// UnityEngine.UI.ObjectPool`1<UnityEngine.UI.LayoutRebuilder>
struct ObjectPool_1_t701624289;
// UnityEngine.RectTransform/ReapplyDrivenProperties
struct ReapplyDrivenProperties_t2020713228;
// System.Predicate`1<UnityEngine.Component>
struct Predicate_1_t2262346586;
// UnityEngine.Events.UnityAction`1<UnityEngine.Component>
struct UnityAction_1_t890994926;
// UnityEngine.Vector3[]
struct Vector3U5BU5D_t1172311765;
// iTween
struct iTween_t488923914;
// UnityEngine.UI.ReflectionMethodsCache/Raycast3DCallback
struct Raycast3DCallback_t3928470916;
// UnityEngine.UI.ReflectionMethodsCache/RaycastAllCallback
struct RaycastAllCallback_t3435657708;
// UnityEngine.UI.ReflectionMethodsCache/Raycast2DCallback
struct Raycast2DCallback_t2260664863;
// UnityEngine.UI.ReflectionMethodsCache/GetRayIntersectionAllCallback
struct GetRayIntersectionAllCallback_t2213949596;
// UnityEngine.UI.ReflectionMethodsCache/GetRayIntersectionAllNonAllocCallback
struct GetRayIntersectionAllNonAllocCallback_t3246763936;
// UnityEngine.UI.ReflectionMethodsCache/GetRaycastNonAllocCallback
struct GetRaycastNonAllocCallback_t1074830945;
// System.Collections.Generic.List`1<Live2D.Cubism.Core.Unmanaged.CubismUnmanagedMemory/AllocationItem>
struct List_1_t2298555224;
// System.String
struct String_t;
// UnityEngine.Object
struct Object_t1021602117;
// System.Collections.Generic.List`1<UnityEngine.Analytics.TrackableProperty/FieldWithTarget>
struct List_1_t1625295921;
// Live2D.Cubism.Core.CubismLogging/UnmanagedLogDelegate
struct UnmanagedLogDelegate_t742859164;
// Live2D.Cubism.Core.CubismTaskQueue/CubismTaskHandler
struct CubismTaskHandler_t1959870214;
// System.Comparison`1<Live2D.Cubism.Core.CubismParameter>
struct Comparison_1_t831449101;
// System.Comparison`1<Live2D.Cubism.Core.CubismPart>
struct Comparison_1_t795782513;
// System.Comparison`1<Live2D.Cubism.Core.CubismDrawable>
struct Comparison_1_t3970950164;
// System.Char[]
struct CharU5BU5D_t1328083999;
// System.Void
struct Void_t1841601450;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// System.DelegateData
struct DelegateData_t1572802995;
// UnityEngine.Collider2D
struct Collider2D_t646061738;
// UnityEngine.Collider
struct Collider_t3497673348;
// System.Collections.Generic.List`1<UnityEngine.Vector3>
struct List_1_t1612828712;
// System.Collections.Generic.List`1<UnityEngine.Color32>
struct List_1_t243638650;
// System.Collections.Generic.List`1<UnityEngine.Vector2>
struct List_1_t1612828711;
// System.Collections.Generic.List`1<UnityEngine.Vector4>
struct List_1_t1612828713;
// System.Collections.Generic.List`1<System.Int32>
struct List_1_t1440998580;
// System.String[]
struct StringU5BU5D_t1642385972;
// Live2D.Cubism.Core.Unmanaged.CubismUnmanagedByteArrayView
struct CubismUnmanagedByteArrayView_t2855206621;
// Live2D.Cubism.Core.Unmanaged.CubismUnmanagedIntArrayView
struct CubismUnmanagedIntArrayView_t965365490;
// Live2D.Cubism.Core.Unmanaged.CubismUnmanagedFloatArrayView
struct CubismUnmanagedFloatArrayView_t1153227075;
// Live2D.Cubism.Core.Unmanaged.CubismUnmanagedIntArrayView[]
struct CubismUnmanagedIntArrayViewU5BU5D_t1335536903;
// Live2D.Cubism.Core.Unmanaged.CubismUnmanagedFloatArrayView[]
struct CubismUnmanagedFloatArrayViewU5BU5D_t3463987794;
// Live2D.Cubism.Core.Unmanaged.CubismUnmanagedUshortArrayView[]
struct CubismUnmanagedUshortArrayViewU5BU5D_t513752903;
// Live2D.Cubism.Core.Unmanaged.CubismUnmanagedModel
struct CubismUnmanagedModel_t586274666;
// Live2D.Cubism.Core.CubismMoc
struct CubismMoc_t1358225636;
// Live2D.Cubism.Core.CubismDynamicDrawableData[]
struct CubismDynamicDrawableDataU5BU5D_t2021340059;
// UnityEngine.RaycastHit[]
struct RaycastHitU5BU5D_t1214023521;
// System.IAsyncResult
struct IAsyncResult_t1999651008;
// System.AsyncCallback
struct AsyncCallback_t163412349;
// UnityEngine.RaycastHit2D[]
struct RaycastHit2DU5BU5D_t4176517891;
// Live2D.Cubism.Core.CubismModel
struct CubismModel_t2381008000;
// Live2D.Cubism.Core.ICubismTask
struct ICubismTask_t4057632211;
// System.Byte[]
struct ByteU5BU5D_t3397334013;
// Live2D.Cubism.Core.Unmanaged.CubismUnmanagedMoc
struct CubismUnmanagedMoc_t2103361662;
// Live2D.Cubism.Core.Unmanaged.CubismUnmanagedParts
struct CubismUnmanagedParts_t1124036275;
// Live2D.Cubism.Core.CubismModel/DynamicDrawableDataHandler
struct DynamicDrawableDataHandler_t2657212100;
// Live2D.Cubism.Core.CubismTaskableModel
struct CubismTaskableModel_t1216042951;
// Live2D.Cubism.Core.CubismParameter[]
struct CubismParameterU5BU5D_t2848401775;
// Live2D.Cubism.Core.CubismPart[]
struct CubismPartU5BU5D_t650530299;
// Live2D.Cubism.Core.CubismDrawable[]
struct CubismDrawableU5BU5D_t2169581932;
// Live2D.Cubism.Core.Unmanaged.CubismUnmanagedParameters
struct CubismUnmanagedParameters_t2764188193;
// Live2D.Cubism.Core.Unmanaged.CubismUnmanagedDrawables
struct CubismUnmanagedDrawables_t771654660;
// System.Collections.Generic.List`1<System.Collections.Hashtable>
struct List_1_t278961118;
// UnityEngine.GameObject
struct GameObject_t1756533147;
// System.Collections.Hashtable
struct Hashtable_t909839986;
// iTween/EasingFunction
struct EasingFunction_t3676968174;
// iTween/ApplyTween
struct ApplyTween_t747394300;
// UnityEngine.AudioSource
struct AudioSource_t1135106623;
// UnityEngine.Vector2[]
struct Vector2U5BU5D_t686124026;
// UnityEngine.Color[0...,0...]
struct ColorU5B0___U2C0___U5D_t672350443;
// System.Single[]
struct SingleU5BU5D_t577127397;
// UnityEngine.Rect[]
struct RectU5BU5D_t1299715887;
// iTween/CRSpline
struct CRSpline_t4177960625;
// UnityEngine.Transform
struct Transform_t3275118058;
// System.Collections.Generic.Dictionary`2<System.String,System.Int32>
struct Dictionary_2_t3986656710;
// System.Collections.Generic.Dictionary`2<System.String,System.Object>
struct Dictionary_2_t309261261;
// UnityEngine.Analytics.TrackableProperty
struct TrackableProperty_t1304606600;
// UnityEngine.RectOffset
struct RectOffset_t3387826427;
// System.Collections.Generic.List`1<UnityEngine.RectTransform>
struct List_1_t2719087314;
// UnityEngine.UI.Graphic
struct Graphic_t2426225576;
// UnityEngine.Canvas
struct Canvas_t209405766;




#ifndef U3CMODULEU3E_T3783534240_H
#define U3CMODULEU3E_T3783534240_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <Module>
struct  U3CModuleU3E_t3783534240 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CMODULEU3E_T3783534240_H
#ifndef U3CMODULEU3E_T3783534238_H
#define U3CMODULEU3E_T3783534238_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <Module>
struct  U3CModuleU3E_t3783534238 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CMODULEU3E_T3783534238_H
#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
#ifndef U3CMODULEU3E_T3783534239_H
#define U3CMODULEU3E_T3783534239_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <Module>
struct  U3CModuleU3E_t3783534239 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CMODULEU3E_T3783534239_H
#ifndef LAYOUTUTILITY_T4076838048_H
#define LAYOUTUTILITY_T4076838048_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.LayoutUtility
struct  LayoutUtility_t4076838048  : public RuntimeObject
{
public:

public:
};

struct LayoutUtility_t4076838048_StaticFields
{
public:
	// System.Func`2<UnityEngine.UI.ILayoutElement,System.Single> UnityEngine.UI.LayoutUtility::<>f__am$cache0
	Func_2_t1976155184 * ___U3CU3Ef__amU24cache0_0;
	// System.Func`2<UnityEngine.UI.ILayoutElement,System.Single> UnityEngine.UI.LayoutUtility::<>f__am$cache1
	Func_2_t1976155184 * ___U3CU3Ef__amU24cache1_1;
	// System.Func`2<UnityEngine.UI.ILayoutElement,System.Single> UnityEngine.UI.LayoutUtility::<>f__am$cache2
	Func_2_t1976155184 * ___U3CU3Ef__amU24cache2_2;
	// System.Func`2<UnityEngine.UI.ILayoutElement,System.Single> UnityEngine.UI.LayoutUtility::<>f__am$cache3
	Func_2_t1976155184 * ___U3CU3Ef__amU24cache3_3;
	// System.Func`2<UnityEngine.UI.ILayoutElement,System.Single> UnityEngine.UI.LayoutUtility::<>f__am$cache4
	Func_2_t1976155184 * ___U3CU3Ef__amU24cache4_4;
	// System.Func`2<UnityEngine.UI.ILayoutElement,System.Single> UnityEngine.UI.LayoutUtility::<>f__am$cache5
	Func_2_t1976155184 * ___U3CU3Ef__amU24cache5_5;
	// System.Func`2<UnityEngine.UI.ILayoutElement,System.Single> UnityEngine.UI.LayoutUtility::<>f__am$cache6
	Func_2_t1976155184 * ___U3CU3Ef__amU24cache6_6;
	// System.Func`2<UnityEngine.UI.ILayoutElement,System.Single> UnityEngine.UI.LayoutUtility::<>f__am$cache7
	Func_2_t1976155184 * ___U3CU3Ef__amU24cache7_7;

public:
	inline static int32_t get_offset_of_U3CU3Ef__amU24cache0_0() { return static_cast<int32_t>(offsetof(LayoutUtility_t4076838048_StaticFields, ___U3CU3Ef__amU24cache0_0)); }
	inline Func_2_t1976155184 * get_U3CU3Ef__amU24cache0_0() const { return ___U3CU3Ef__amU24cache0_0; }
	inline Func_2_t1976155184 ** get_address_of_U3CU3Ef__amU24cache0_0() { return &___U3CU3Ef__amU24cache0_0; }
	inline void set_U3CU3Ef__amU24cache0_0(Func_2_t1976155184 * value)
	{
		___U3CU3Ef__amU24cache0_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache0_0), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache1_1() { return static_cast<int32_t>(offsetof(LayoutUtility_t4076838048_StaticFields, ___U3CU3Ef__amU24cache1_1)); }
	inline Func_2_t1976155184 * get_U3CU3Ef__amU24cache1_1() const { return ___U3CU3Ef__amU24cache1_1; }
	inline Func_2_t1976155184 ** get_address_of_U3CU3Ef__amU24cache1_1() { return &___U3CU3Ef__amU24cache1_1; }
	inline void set_U3CU3Ef__amU24cache1_1(Func_2_t1976155184 * value)
	{
		___U3CU3Ef__amU24cache1_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache1_1), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache2_2() { return static_cast<int32_t>(offsetof(LayoutUtility_t4076838048_StaticFields, ___U3CU3Ef__amU24cache2_2)); }
	inline Func_2_t1976155184 * get_U3CU3Ef__amU24cache2_2() const { return ___U3CU3Ef__amU24cache2_2; }
	inline Func_2_t1976155184 ** get_address_of_U3CU3Ef__amU24cache2_2() { return &___U3CU3Ef__amU24cache2_2; }
	inline void set_U3CU3Ef__amU24cache2_2(Func_2_t1976155184 * value)
	{
		___U3CU3Ef__amU24cache2_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache2_2), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache3_3() { return static_cast<int32_t>(offsetof(LayoutUtility_t4076838048_StaticFields, ___U3CU3Ef__amU24cache3_3)); }
	inline Func_2_t1976155184 * get_U3CU3Ef__amU24cache3_3() const { return ___U3CU3Ef__amU24cache3_3; }
	inline Func_2_t1976155184 ** get_address_of_U3CU3Ef__amU24cache3_3() { return &___U3CU3Ef__amU24cache3_3; }
	inline void set_U3CU3Ef__amU24cache3_3(Func_2_t1976155184 * value)
	{
		___U3CU3Ef__amU24cache3_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache3_3), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache4_4() { return static_cast<int32_t>(offsetof(LayoutUtility_t4076838048_StaticFields, ___U3CU3Ef__amU24cache4_4)); }
	inline Func_2_t1976155184 * get_U3CU3Ef__amU24cache4_4() const { return ___U3CU3Ef__amU24cache4_4; }
	inline Func_2_t1976155184 ** get_address_of_U3CU3Ef__amU24cache4_4() { return &___U3CU3Ef__amU24cache4_4; }
	inline void set_U3CU3Ef__amU24cache4_4(Func_2_t1976155184 * value)
	{
		___U3CU3Ef__amU24cache4_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache4_4), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache5_5() { return static_cast<int32_t>(offsetof(LayoutUtility_t4076838048_StaticFields, ___U3CU3Ef__amU24cache5_5)); }
	inline Func_2_t1976155184 * get_U3CU3Ef__amU24cache5_5() const { return ___U3CU3Ef__amU24cache5_5; }
	inline Func_2_t1976155184 ** get_address_of_U3CU3Ef__amU24cache5_5() { return &___U3CU3Ef__amU24cache5_5; }
	inline void set_U3CU3Ef__amU24cache5_5(Func_2_t1976155184 * value)
	{
		___U3CU3Ef__amU24cache5_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache5_5), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache6_6() { return static_cast<int32_t>(offsetof(LayoutUtility_t4076838048_StaticFields, ___U3CU3Ef__amU24cache6_6)); }
	inline Func_2_t1976155184 * get_U3CU3Ef__amU24cache6_6() const { return ___U3CU3Ef__amU24cache6_6; }
	inline Func_2_t1976155184 ** get_address_of_U3CU3Ef__amU24cache6_6() { return &___U3CU3Ef__amU24cache6_6; }
	inline void set_U3CU3Ef__amU24cache6_6(Func_2_t1976155184 * value)
	{
		___U3CU3Ef__amU24cache6_6 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache6_6), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache7_7() { return static_cast<int32_t>(offsetof(LayoutUtility_t4076838048_StaticFields, ___U3CU3Ef__amU24cache7_7)); }
	inline Func_2_t1976155184 * get_U3CU3Ef__amU24cache7_7() const { return ___U3CU3Ef__amU24cache7_7; }
	inline Func_2_t1976155184 ** get_address_of_U3CU3Ef__amU24cache7_7() { return &___U3CU3Ef__amU24cache7_7; }
	inline void set_U3CU3Ef__amU24cache7_7(Func_2_t1976155184 * value)
	{
		___U3CU3Ef__amU24cache7_7 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache7_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LAYOUTUTILITY_T4076838048_H
#ifndef LAYOUTREBUILDER_T2155218138_H
#define LAYOUTREBUILDER_T2155218138_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.LayoutRebuilder
struct  LayoutRebuilder_t2155218138  : public RuntimeObject
{
public:
	// UnityEngine.RectTransform UnityEngine.UI.LayoutRebuilder::m_ToRebuild
	RectTransform_t3349966182 * ___m_ToRebuild_0;
	// System.Int32 UnityEngine.UI.LayoutRebuilder::m_CachedHashFromTransform
	int32_t ___m_CachedHashFromTransform_1;

public:
	inline static int32_t get_offset_of_m_ToRebuild_0() { return static_cast<int32_t>(offsetof(LayoutRebuilder_t2155218138, ___m_ToRebuild_0)); }
	inline RectTransform_t3349966182 * get_m_ToRebuild_0() const { return ___m_ToRebuild_0; }
	inline RectTransform_t3349966182 ** get_address_of_m_ToRebuild_0() { return &___m_ToRebuild_0; }
	inline void set_m_ToRebuild_0(RectTransform_t3349966182 * value)
	{
		___m_ToRebuild_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_ToRebuild_0), value);
	}

	inline static int32_t get_offset_of_m_CachedHashFromTransform_1() { return static_cast<int32_t>(offsetof(LayoutRebuilder_t2155218138, ___m_CachedHashFromTransform_1)); }
	inline int32_t get_m_CachedHashFromTransform_1() const { return ___m_CachedHashFromTransform_1; }
	inline int32_t* get_address_of_m_CachedHashFromTransform_1() { return &___m_CachedHashFromTransform_1; }
	inline void set_m_CachedHashFromTransform_1(int32_t value)
	{
		___m_CachedHashFromTransform_1 = value;
	}
};

struct LayoutRebuilder_t2155218138_StaticFields
{
public:
	// UnityEngine.UI.ObjectPool`1<UnityEngine.UI.LayoutRebuilder> UnityEngine.UI.LayoutRebuilder::s_Rebuilders
	ObjectPool_1_t701624289 * ___s_Rebuilders_2;
	// UnityEngine.RectTransform/ReapplyDrivenProperties UnityEngine.UI.LayoutRebuilder::<>f__mg$cache0
	ReapplyDrivenProperties_t2020713228 * ___U3CU3Ef__mgU24cache0_3;
	// System.Predicate`1<UnityEngine.Component> UnityEngine.UI.LayoutRebuilder::<>f__am$cache0
	Predicate_1_t2262346586 * ___U3CU3Ef__amU24cache0_4;
	// UnityEngine.Events.UnityAction`1<UnityEngine.Component> UnityEngine.UI.LayoutRebuilder::<>f__am$cache1
	UnityAction_1_t890994926 * ___U3CU3Ef__amU24cache1_5;
	// UnityEngine.Events.UnityAction`1<UnityEngine.Component> UnityEngine.UI.LayoutRebuilder::<>f__am$cache2
	UnityAction_1_t890994926 * ___U3CU3Ef__amU24cache2_6;
	// UnityEngine.Events.UnityAction`1<UnityEngine.Component> UnityEngine.UI.LayoutRebuilder::<>f__am$cache3
	UnityAction_1_t890994926 * ___U3CU3Ef__amU24cache3_7;
	// UnityEngine.Events.UnityAction`1<UnityEngine.Component> UnityEngine.UI.LayoutRebuilder::<>f__am$cache4
	UnityAction_1_t890994926 * ___U3CU3Ef__amU24cache4_8;

public:
	inline static int32_t get_offset_of_s_Rebuilders_2() { return static_cast<int32_t>(offsetof(LayoutRebuilder_t2155218138_StaticFields, ___s_Rebuilders_2)); }
	inline ObjectPool_1_t701624289 * get_s_Rebuilders_2() const { return ___s_Rebuilders_2; }
	inline ObjectPool_1_t701624289 ** get_address_of_s_Rebuilders_2() { return &___s_Rebuilders_2; }
	inline void set_s_Rebuilders_2(ObjectPool_1_t701624289 * value)
	{
		___s_Rebuilders_2 = value;
		Il2CppCodeGenWriteBarrier((&___s_Rebuilders_2), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache0_3() { return static_cast<int32_t>(offsetof(LayoutRebuilder_t2155218138_StaticFields, ___U3CU3Ef__mgU24cache0_3)); }
	inline ReapplyDrivenProperties_t2020713228 * get_U3CU3Ef__mgU24cache0_3() const { return ___U3CU3Ef__mgU24cache0_3; }
	inline ReapplyDrivenProperties_t2020713228 ** get_address_of_U3CU3Ef__mgU24cache0_3() { return &___U3CU3Ef__mgU24cache0_3; }
	inline void set_U3CU3Ef__mgU24cache0_3(ReapplyDrivenProperties_t2020713228 * value)
	{
		___U3CU3Ef__mgU24cache0_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache0_3), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache0_4() { return static_cast<int32_t>(offsetof(LayoutRebuilder_t2155218138_StaticFields, ___U3CU3Ef__amU24cache0_4)); }
	inline Predicate_1_t2262346586 * get_U3CU3Ef__amU24cache0_4() const { return ___U3CU3Ef__amU24cache0_4; }
	inline Predicate_1_t2262346586 ** get_address_of_U3CU3Ef__amU24cache0_4() { return &___U3CU3Ef__amU24cache0_4; }
	inline void set_U3CU3Ef__amU24cache0_4(Predicate_1_t2262346586 * value)
	{
		___U3CU3Ef__amU24cache0_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache0_4), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache1_5() { return static_cast<int32_t>(offsetof(LayoutRebuilder_t2155218138_StaticFields, ___U3CU3Ef__amU24cache1_5)); }
	inline UnityAction_1_t890994926 * get_U3CU3Ef__amU24cache1_5() const { return ___U3CU3Ef__amU24cache1_5; }
	inline UnityAction_1_t890994926 ** get_address_of_U3CU3Ef__amU24cache1_5() { return &___U3CU3Ef__amU24cache1_5; }
	inline void set_U3CU3Ef__amU24cache1_5(UnityAction_1_t890994926 * value)
	{
		___U3CU3Ef__amU24cache1_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache1_5), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache2_6() { return static_cast<int32_t>(offsetof(LayoutRebuilder_t2155218138_StaticFields, ___U3CU3Ef__amU24cache2_6)); }
	inline UnityAction_1_t890994926 * get_U3CU3Ef__amU24cache2_6() const { return ___U3CU3Ef__amU24cache2_6; }
	inline UnityAction_1_t890994926 ** get_address_of_U3CU3Ef__amU24cache2_6() { return &___U3CU3Ef__amU24cache2_6; }
	inline void set_U3CU3Ef__amU24cache2_6(UnityAction_1_t890994926 * value)
	{
		___U3CU3Ef__amU24cache2_6 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache2_6), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache3_7() { return static_cast<int32_t>(offsetof(LayoutRebuilder_t2155218138_StaticFields, ___U3CU3Ef__amU24cache3_7)); }
	inline UnityAction_1_t890994926 * get_U3CU3Ef__amU24cache3_7() const { return ___U3CU3Ef__amU24cache3_7; }
	inline UnityAction_1_t890994926 ** get_address_of_U3CU3Ef__amU24cache3_7() { return &___U3CU3Ef__amU24cache3_7; }
	inline void set_U3CU3Ef__amU24cache3_7(UnityAction_1_t890994926 * value)
	{
		___U3CU3Ef__amU24cache3_7 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache3_7), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache4_8() { return static_cast<int32_t>(offsetof(LayoutRebuilder_t2155218138_StaticFields, ___U3CU3Ef__amU24cache4_8)); }
	inline UnityAction_1_t890994926 * get_U3CU3Ef__amU24cache4_8() const { return ___U3CU3Ef__amU24cache4_8; }
	inline UnityAction_1_t890994926 ** get_address_of_U3CU3Ef__amU24cache4_8() { return &___U3CU3Ef__amU24cache4_8; }
	inline void set_U3CU3Ef__amU24cache4_8(UnityAction_1_t890994926 * value)
	{
		___U3CU3Ef__amU24cache4_8 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache4_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LAYOUTREBUILDER_T2155218138_H
#ifndef RECTANGULARVERTEXCLIPPER_T3349113845_H
#define RECTANGULARVERTEXCLIPPER_T3349113845_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.RectangularVertexClipper
struct  RectangularVertexClipper_t3349113845  : public RuntimeObject
{
public:
	// UnityEngine.Vector3[] UnityEngine.UI.RectangularVertexClipper::m_WorldCorners
	Vector3U5BU5D_t1172311765* ___m_WorldCorners_0;
	// UnityEngine.Vector3[] UnityEngine.UI.RectangularVertexClipper::m_CanvasCorners
	Vector3U5BU5D_t1172311765* ___m_CanvasCorners_1;

public:
	inline static int32_t get_offset_of_m_WorldCorners_0() { return static_cast<int32_t>(offsetof(RectangularVertexClipper_t3349113845, ___m_WorldCorners_0)); }
	inline Vector3U5BU5D_t1172311765* get_m_WorldCorners_0() const { return ___m_WorldCorners_0; }
	inline Vector3U5BU5D_t1172311765** get_address_of_m_WorldCorners_0() { return &___m_WorldCorners_0; }
	inline void set_m_WorldCorners_0(Vector3U5BU5D_t1172311765* value)
	{
		___m_WorldCorners_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_WorldCorners_0), value);
	}

	inline static int32_t get_offset_of_m_CanvasCorners_1() { return static_cast<int32_t>(offsetof(RectangularVertexClipper_t3349113845, ___m_CanvasCorners_1)); }
	inline Vector3U5BU5D_t1172311765* get_m_CanvasCorners_1() const { return ___m_CanvasCorners_1; }
	inline Vector3U5BU5D_t1172311765** get_address_of_m_CanvasCorners_1() { return &___m_CanvasCorners_1; }
	inline void set_m_CanvasCorners_1(Vector3U5BU5D_t1172311765* value)
	{
		___m_CanvasCorners_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_CanvasCorners_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RECTANGULARVERTEXCLIPPER_T3349113845_H
#ifndef U3CDELAYEDSETDIRTYU3EC__ITERATOR0_T3228926346_H
#define U3CDELAYEDSETDIRTYU3EC__ITERATOR0_T3228926346_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.LayoutGroup/<DelayedSetDirty>c__Iterator0
struct  U3CDelayedSetDirtyU3Ec__Iterator0_t3228926346  : public RuntimeObject
{
public:
	// UnityEngine.RectTransform UnityEngine.UI.LayoutGroup/<DelayedSetDirty>c__Iterator0::rectTransform
	RectTransform_t3349966182 * ___rectTransform_0;
	// System.Object UnityEngine.UI.LayoutGroup/<DelayedSetDirty>c__Iterator0::$current
	RuntimeObject * ___U24current_1;
	// System.Boolean UnityEngine.UI.LayoutGroup/<DelayedSetDirty>c__Iterator0::$disposing
	bool ___U24disposing_2;
	// System.Int32 UnityEngine.UI.LayoutGroup/<DelayedSetDirty>c__Iterator0::$PC
	int32_t ___U24PC_3;

public:
	inline static int32_t get_offset_of_rectTransform_0() { return static_cast<int32_t>(offsetof(U3CDelayedSetDirtyU3Ec__Iterator0_t3228926346, ___rectTransform_0)); }
	inline RectTransform_t3349966182 * get_rectTransform_0() const { return ___rectTransform_0; }
	inline RectTransform_t3349966182 ** get_address_of_rectTransform_0() { return &___rectTransform_0; }
	inline void set_rectTransform_0(RectTransform_t3349966182 * value)
	{
		___rectTransform_0 = value;
		Il2CppCodeGenWriteBarrier((&___rectTransform_0), value);
	}

	inline static int32_t get_offset_of_U24current_1() { return static_cast<int32_t>(offsetof(U3CDelayedSetDirtyU3Ec__Iterator0_t3228926346, ___U24current_1)); }
	inline RuntimeObject * get_U24current_1() const { return ___U24current_1; }
	inline RuntimeObject ** get_address_of_U24current_1() { return &___U24current_1; }
	inline void set_U24current_1(RuntimeObject * value)
	{
		___U24current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_1), value);
	}

	inline static int32_t get_offset_of_U24disposing_2() { return static_cast<int32_t>(offsetof(U3CDelayedSetDirtyU3Ec__Iterator0_t3228926346, ___U24disposing_2)); }
	inline bool get_U24disposing_2() const { return ___U24disposing_2; }
	inline bool* get_address_of_U24disposing_2() { return &___U24disposing_2; }
	inline void set_U24disposing_2(bool value)
	{
		___U24disposing_2 = value;
	}

	inline static int32_t get_offset_of_U24PC_3() { return static_cast<int32_t>(offsetof(U3CDelayedSetDirtyU3Ec__Iterator0_t3228926346, ___U24PC_3)); }
	inline int32_t get_U24PC_3() const { return ___U24PC_3; }
	inline int32_t* get_address_of_U24PC_3() { return &___U24PC_3; }
	inline void set_U24PC_3(int32_t value)
	{
		___U24PC_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CDELAYEDSETDIRTYU3EC__ITERATOR0_T3228926346_H
#ifndef U3CTWEENDELAYU3EC__ITERATOR0_T1889752800_H
#define U3CTWEENDELAYU3EC__ITERATOR0_T1889752800_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// iTween/<TweenDelay>c__Iterator0
struct  U3CTweenDelayU3Ec__Iterator0_t1889752800  : public RuntimeObject
{
public:
	// iTween iTween/<TweenDelay>c__Iterator0::$this
	iTween_t488923914 * ___U24this_0;
	// System.Object iTween/<TweenDelay>c__Iterator0::$current
	RuntimeObject * ___U24current_1;
	// System.Boolean iTween/<TweenDelay>c__Iterator0::$disposing
	bool ___U24disposing_2;
	// System.Int32 iTween/<TweenDelay>c__Iterator0::$PC
	int32_t ___U24PC_3;

public:
	inline static int32_t get_offset_of_U24this_0() { return static_cast<int32_t>(offsetof(U3CTweenDelayU3Ec__Iterator0_t1889752800, ___U24this_0)); }
	inline iTween_t488923914 * get_U24this_0() const { return ___U24this_0; }
	inline iTween_t488923914 ** get_address_of_U24this_0() { return &___U24this_0; }
	inline void set_U24this_0(iTween_t488923914 * value)
	{
		___U24this_0 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_0), value);
	}

	inline static int32_t get_offset_of_U24current_1() { return static_cast<int32_t>(offsetof(U3CTweenDelayU3Ec__Iterator0_t1889752800, ___U24current_1)); }
	inline RuntimeObject * get_U24current_1() const { return ___U24current_1; }
	inline RuntimeObject ** get_address_of_U24current_1() { return &___U24current_1; }
	inline void set_U24current_1(RuntimeObject * value)
	{
		___U24current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_1), value);
	}

	inline static int32_t get_offset_of_U24disposing_2() { return static_cast<int32_t>(offsetof(U3CTweenDelayU3Ec__Iterator0_t1889752800, ___U24disposing_2)); }
	inline bool get_U24disposing_2() const { return ___U24disposing_2; }
	inline bool* get_address_of_U24disposing_2() { return &___U24disposing_2; }
	inline void set_U24disposing_2(bool value)
	{
		___U24disposing_2 = value;
	}

	inline static int32_t get_offset_of_U24PC_3() { return static_cast<int32_t>(offsetof(U3CTweenDelayU3Ec__Iterator0_t1889752800, ___U24PC_3)); }
	inline int32_t get_U24PC_3() const { return ___U24PC_3; }
	inline int32_t* get_address_of_U24PC_3() { return &___U24PC_3; }
	inline void set_U24PC_3(int32_t value)
	{
		___U24PC_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CTWEENDELAYU3EC__ITERATOR0_T1889752800_H
#ifndef CRSPLINE_T4177960625_H
#define CRSPLINE_T4177960625_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// iTween/CRSpline
struct  CRSpline_t4177960625  : public RuntimeObject
{
public:
	// UnityEngine.Vector3[] iTween/CRSpline::pts
	Vector3U5BU5D_t1172311765* ___pts_0;

public:
	inline static int32_t get_offset_of_pts_0() { return static_cast<int32_t>(offsetof(CRSpline_t4177960625, ___pts_0)); }
	inline Vector3U5BU5D_t1172311765* get_pts_0() const { return ___pts_0; }
	inline Vector3U5BU5D_t1172311765** get_address_of_pts_0() { return &___pts_0; }
	inline void set_pts_0(Vector3U5BU5D_t1172311765* value)
	{
		___pts_0 = value;
		Il2CppCodeGenWriteBarrier((&___pts_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CRSPLINE_T4177960625_H
#ifndef REFLECTIONMETHODSCACHE_T3343836395_H
#define REFLECTIONMETHODSCACHE_T3343836395_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.ReflectionMethodsCache
struct  ReflectionMethodsCache_t3343836395  : public RuntimeObject
{
public:
	// UnityEngine.UI.ReflectionMethodsCache/Raycast3DCallback UnityEngine.UI.ReflectionMethodsCache::raycast3D
	Raycast3DCallback_t3928470916 * ___raycast3D_0;
	// UnityEngine.UI.ReflectionMethodsCache/RaycastAllCallback UnityEngine.UI.ReflectionMethodsCache::raycast3DAll
	RaycastAllCallback_t3435657708 * ___raycast3DAll_1;
	// UnityEngine.UI.ReflectionMethodsCache/Raycast2DCallback UnityEngine.UI.ReflectionMethodsCache::raycast2D
	Raycast2DCallback_t2260664863 * ___raycast2D_2;
	// UnityEngine.UI.ReflectionMethodsCache/GetRayIntersectionAllCallback UnityEngine.UI.ReflectionMethodsCache::getRayIntersectionAll
	GetRayIntersectionAllCallback_t2213949596 * ___getRayIntersectionAll_3;
	// UnityEngine.UI.ReflectionMethodsCache/GetRayIntersectionAllNonAllocCallback UnityEngine.UI.ReflectionMethodsCache::getRayIntersectionAllNonAlloc
	GetRayIntersectionAllNonAllocCallback_t3246763936 * ___getRayIntersectionAllNonAlloc_4;
	// UnityEngine.UI.ReflectionMethodsCache/GetRaycastNonAllocCallback UnityEngine.UI.ReflectionMethodsCache::getRaycastNonAlloc
	GetRaycastNonAllocCallback_t1074830945 * ___getRaycastNonAlloc_5;

public:
	inline static int32_t get_offset_of_raycast3D_0() { return static_cast<int32_t>(offsetof(ReflectionMethodsCache_t3343836395, ___raycast3D_0)); }
	inline Raycast3DCallback_t3928470916 * get_raycast3D_0() const { return ___raycast3D_0; }
	inline Raycast3DCallback_t3928470916 ** get_address_of_raycast3D_0() { return &___raycast3D_0; }
	inline void set_raycast3D_0(Raycast3DCallback_t3928470916 * value)
	{
		___raycast3D_0 = value;
		Il2CppCodeGenWriteBarrier((&___raycast3D_0), value);
	}

	inline static int32_t get_offset_of_raycast3DAll_1() { return static_cast<int32_t>(offsetof(ReflectionMethodsCache_t3343836395, ___raycast3DAll_1)); }
	inline RaycastAllCallback_t3435657708 * get_raycast3DAll_1() const { return ___raycast3DAll_1; }
	inline RaycastAllCallback_t3435657708 ** get_address_of_raycast3DAll_1() { return &___raycast3DAll_1; }
	inline void set_raycast3DAll_1(RaycastAllCallback_t3435657708 * value)
	{
		___raycast3DAll_1 = value;
		Il2CppCodeGenWriteBarrier((&___raycast3DAll_1), value);
	}

	inline static int32_t get_offset_of_raycast2D_2() { return static_cast<int32_t>(offsetof(ReflectionMethodsCache_t3343836395, ___raycast2D_2)); }
	inline Raycast2DCallback_t2260664863 * get_raycast2D_2() const { return ___raycast2D_2; }
	inline Raycast2DCallback_t2260664863 ** get_address_of_raycast2D_2() { return &___raycast2D_2; }
	inline void set_raycast2D_2(Raycast2DCallback_t2260664863 * value)
	{
		___raycast2D_2 = value;
		Il2CppCodeGenWriteBarrier((&___raycast2D_2), value);
	}

	inline static int32_t get_offset_of_getRayIntersectionAll_3() { return static_cast<int32_t>(offsetof(ReflectionMethodsCache_t3343836395, ___getRayIntersectionAll_3)); }
	inline GetRayIntersectionAllCallback_t2213949596 * get_getRayIntersectionAll_3() const { return ___getRayIntersectionAll_3; }
	inline GetRayIntersectionAllCallback_t2213949596 ** get_address_of_getRayIntersectionAll_3() { return &___getRayIntersectionAll_3; }
	inline void set_getRayIntersectionAll_3(GetRayIntersectionAllCallback_t2213949596 * value)
	{
		___getRayIntersectionAll_3 = value;
		Il2CppCodeGenWriteBarrier((&___getRayIntersectionAll_3), value);
	}

	inline static int32_t get_offset_of_getRayIntersectionAllNonAlloc_4() { return static_cast<int32_t>(offsetof(ReflectionMethodsCache_t3343836395, ___getRayIntersectionAllNonAlloc_4)); }
	inline GetRayIntersectionAllNonAllocCallback_t3246763936 * get_getRayIntersectionAllNonAlloc_4() const { return ___getRayIntersectionAllNonAlloc_4; }
	inline GetRayIntersectionAllNonAllocCallback_t3246763936 ** get_address_of_getRayIntersectionAllNonAlloc_4() { return &___getRayIntersectionAllNonAlloc_4; }
	inline void set_getRayIntersectionAllNonAlloc_4(GetRayIntersectionAllNonAllocCallback_t3246763936 * value)
	{
		___getRayIntersectionAllNonAlloc_4 = value;
		Il2CppCodeGenWriteBarrier((&___getRayIntersectionAllNonAlloc_4), value);
	}

	inline static int32_t get_offset_of_getRaycastNonAlloc_5() { return static_cast<int32_t>(offsetof(ReflectionMethodsCache_t3343836395, ___getRaycastNonAlloc_5)); }
	inline GetRaycastNonAllocCallback_t1074830945 * get_getRaycastNonAlloc_5() const { return ___getRaycastNonAlloc_5; }
	inline GetRaycastNonAllocCallback_t1074830945 ** get_address_of_getRaycastNonAlloc_5() { return &___getRaycastNonAlloc_5; }
	inline void set_getRaycastNonAlloc_5(GetRaycastNonAllocCallback_t1074830945 * value)
	{
		___getRaycastNonAlloc_5 = value;
		Il2CppCodeGenWriteBarrier((&___getRaycastNonAlloc_5), value);
	}
};

struct ReflectionMethodsCache_t3343836395_StaticFields
{
public:
	// UnityEngine.UI.ReflectionMethodsCache UnityEngine.UI.ReflectionMethodsCache::s_ReflectionMethodsCache
	ReflectionMethodsCache_t3343836395 * ___s_ReflectionMethodsCache_6;

public:
	inline static int32_t get_offset_of_s_ReflectionMethodsCache_6() { return static_cast<int32_t>(offsetof(ReflectionMethodsCache_t3343836395_StaticFields, ___s_ReflectionMethodsCache_6)); }
	inline ReflectionMethodsCache_t3343836395 * get_s_ReflectionMethodsCache_6() const { return ___s_ReflectionMethodsCache_6; }
	inline ReflectionMethodsCache_t3343836395 ** get_address_of_s_ReflectionMethodsCache_6() { return &___s_ReflectionMethodsCache_6; }
	inline void set_s_ReflectionMethodsCache_6(ReflectionMethodsCache_t3343836395 * value)
	{
		___s_ReflectionMethodsCache_6 = value;
		Il2CppCodeGenWriteBarrier((&___s_ReflectionMethodsCache_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // REFLECTIONMETHODSCACHE_T3343836395_H
#ifndef CUBISMUNMANAGEDMEMORY_T1337132358_H
#define CUBISMUNMANAGEDMEMORY_T1337132358_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Live2D.Cubism.Core.Unmanaged.CubismUnmanagedMemory
struct  CubismUnmanagedMemory_t1337132358  : public RuntimeObject
{
public:

public:
};

struct CubismUnmanagedMemory_t1337132358_StaticFields
{
public:
	// System.Collections.Generic.List`1<Live2D.Cubism.Core.Unmanaged.CubismUnmanagedMemory/AllocationItem> Live2D.Cubism.Core.Unmanaged.CubismUnmanagedMemory::<Allocations>k__BackingField
	List_1_t2298555224 * ___U3CAllocationsU3Ek__BackingField_0;

public:
	inline static int32_t get_offset_of_U3CAllocationsU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(CubismUnmanagedMemory_t1337132358_StaticFields, ___U3CAllocationsU3Ek__BackingField_0)); }
	inline List_1_t2298555224 * get_U3CAllocationsU3Ek__BackingField_0() const { return ___U3CAllocationsU3Ek__BackingField_0; }
	inline List_1_t2298555224 ** get_address_of_U3CAllocationsU3Ek__BackingField_0() { return &___U3CAllocationsU3Ek__BackingField_0; }
	inline void set_U3CAllocationsU3Ek__BackingField_0(List_1_t2298555224 * value)
	{
		___U3CAllocationsU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CAllocationsU3Ek__BackingField_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CUBISMUNMANAGEDMEMORY_T1337132358_H
#ifndef FIELDWITHTARGET_T2256174789_H
#define FIELDWITHTARGET_T2256174789_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Analytics.TrackableProperty/FieldWithTarget
struct  FieldWithTarget_t2256174789  : public RuntimeObject
{
public:
	// System.String UnityEngine.Analytics.TrackableProperty/FieldWithTarget::m_ParamName
	String_t* ___m_ParamName_0;
	// UnityEngine.Object UnityEngine.Analytics.TrackableProperty/FieldWithTarget::m_Target
	Object_t1021602117 * ___m_Target_1;
	// System.String UnityEngine.Analytics.TrackableProperty/FieldWithTarget::m_FieldPath
	String_t* ___m_FieldPath_2;
	// System.String UnityEngine.Analytics.TrackableProperty/FieldWithTarget::m_TypeString
	String_t* ___m_TypeString_3;
	// System.Boolean UnityEngine.Analytics.TrackableProperty/FieldWithTarget::m_DoStatic
	bool ___m_DoStatic_4;
	// System.String UnityEngine.Analytics.TrackableProperty/FieldWithTarget::m_StaticString
	String_t* ___m_StaticString_5;

public:
	inline static int32_t get_offset_of_m_ParamName_0() { return static_cast<int32_t>(offsetof(FieldWithTarget_t2256174789, ___m_ParamName_0)); }
	inline String_t* get_m_ParamName_0() const { return ___m_ParamName_0; }
	inline String_t** get_address_of_m_ParamName_0() { return &___m_ParamName_0; }
	inline void set_m_ParamName_0(String_t* value)
	{
		___m_ParamName_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_ParamName_0), value);
	}

	inline static int32_t get_offset_of_m_Target_1() { return static_cast<int32_t>(offsetof(FieldWithTarget_t2256174789, ___m_Target_1)); }
	inline Object_t1021602117 * get_m_Target_1() const { return ___m_Target_1; }
	inline Object_t1021602117 ** get_address_of_m_Target_1() { return &___m_Target_1; }
	inline void set_m_Target_1(Object_t1021602117 * value)
	{
		___m_Target_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_Target_1), value);
	}

	inline static int32_t get_offset_of_m_FieldPath_2() { return static_cast<int32_t>(offsetof(FieldWithTarget_t2256174789, ___m_FieldPath_2)); }
	inline String_t* get_m_FieldPath_2() const { return ___m_FieldPath_2; }
	inline String_t** get_address_of_m_FieldPath_2() { return &___m_FieldPath_2; }
	inline void set_m_FieldPath_2(String_t* value)
	{
		___m_FieldPath_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_FieldPath_2), value);
	}

	inline static int32_t get_offset_of_m_TypeString_3() { return static_cast<int32_t>(offsetof(FieldWithTarget_t2256174789, ___m_TypeString_3)); }
	inline String_t* get_m_TypeString_3() const { return ___m_TypeString_3; }
	inline String_t** get_address_of_m_TypeString_3() { return &___m_TypeString_3; }
	inline void set_m_TypeString_3(String_t* value)
	{
		___m_TypeString_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_TypeString_3), value);
	}

	inline static int32_t get_offset_of_m_DoStatic_4() { return static_cast<int32_t>(offsetof(FieldWithTarget_t2256174789, ___m_DoStatic_4)); }
	inline bool get_m_DoStatic_4() const { return ___m_DoStatic_4; }
	inline bool* get_address_of_m_DoStatic_4() { return &___m_DoStatic_4; }
	inline void set_m_DoStatic_4(bool value)
	{
		___m_DoStatic_4 = value;
	}

	inline static int32_t get_offset_of_m_StaticString_5() { return static_cast<int32_t>(offsetof(FieldWithTarget_t2256174789, ___m_StaticString_5)); }
	inline String_t* get_m_StaticString_5() const { return ___m_StaticString_5; }
	inline String_t** get_address_of_m_StaticString_5() { return &___m_StaticString_5; }
	inline void set_m_StaticString_5(String_t* value)
	{
		___m_StaticString_5 = value;
		Il2CppCodeGenWriteBarrier((&___m_StaticString_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FIELDWITHTARGET_T2256174789_H
#ifndef TRACKABLEPROPERTY_T1304606600_H
#define TRACKABLEPROPERTY_T1304606600_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Analytics.TrackableProperty
struct  TrackableProperty_t1304606600  : public RuntimeObject
{
public:
	// System.Collections.Generic.List`1<UnityEngine.Analytics.TrackableProperty/FieldWithTarget> UnityEngine.Analytics.TrackableProperty::m_Fields
	List_1_t1625295921 * ___m_Fields_1;

public:
	inline static int32_t get_offset_of_m_Fields_1() { return static_cast<int32_t>(offsetof(TrackableProperty_t1304606600, ___m_Fields_1)); }
	inline List_1_t1625295921 * get_m_Fields_1() const { return ___m_Fields_1; }
	inline List_1_t1625295921 ** get_address_of_m_Fields_1() { return &___m_Fields_1; }
	inline void set_m_Fields_1(List_1_t1625295921 * value)
	{
		___m_Fields_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_Fields_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRACKABLEPROPERTY_T1304606600_H
#ifndef BASEVERTEXEFFECT_T2504093552_H
#define BASEVERTEXEFFECT_T2504093552_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.BaseVertexEffect
struct  BaseVertexEffect_t2504093552  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BASEVERTEXEFFECT_T2504093552_H
#ifndef VALUETYPE_T3507792607_H
#define VALUETYPE_T3507792607_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ValueType
struct  ValueType_t3507792607  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t3507792607_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t3507792607_marshaled_com
{
};
#endif // VALUETYPE_T3507792607_H
#ifndef CUBISMCOREDLL_T2084904188_H
#define CUBISMCOREDLL_T2084904188_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Live2D.Cubism.Core.Unmanaged.CubismCoreDll
struct  CubismCoreDll_t2084904188  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CUBISMCOREDLL_T2084904188_H
#ifndef CUBISMDYNAMICDRAWABLEDATA_T537076974_H
#define CUBISMDYNAMICDRAWABLEDATA_T537076974_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Live2D.Cubism.Core.CubismDynamicDrawableData
struct  CubismDynamicDrawableData_t537076974  : public RuntimeObject
{
public:
	// System.Byte Live2D.Cubism.Core.CubismDynamicDrawableData::<Flags>k__BackingField
	uint8_t ___U3CFlagsU3Ek__BackingField_0;
	// System.Single Live2D.Cubism.Core.CubismDynamicDrawableData::<Opacity>k__BackingField
	float ___U3COpacityU3Ek__BackingField_1;
	// System.Int32 Live2D.Cubism.Core.CubismDynamicDrawableData::<DrawOrder>k__BackingField
	int32_t ___U3CDrawOrderU3Ek__BackingField_2;
	// System.Int32 Live2D.Cubism.Core.CubismDynamicDrawableData::<RenderOrder>k__BackingField
	int32_t ___U3CRenderOrderU3Ek__BackingField_3;
	// UnityEngine.Vector3[] Live2D.Cubism.Core.CubismDynamicDrawableData::<VertexPositions>k__BackingField
	Vector3U5BU5D_t1172311765* ___U3CVertexPositionsU3Ek__BackingField_4;

public:
	inline static int32_t get_offset_of_U3CFlagsU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(CubismDynamicDrawableData_t537076974, ___U3CFlagsU3Ek__BackingField_0)); }
	inline uint8_t get_U3CFlagsU3Ek__BackingField_0() const { return ___U3CFlagsU3Ek__BackingField_0; }
	inline uint8_t* get_address_of_U3CFlagsU3Ek__BackingField_0() { return &___U3CFlagsU3Ek__BackingField_0; }
	inline void set_U3CFlagsU3Ek__BackingField_0(uint8_t value)
	{
		___U3CFlagsU3Ek__BackingField_0 = value;
	}

	inline static int32_t get_offset_of_U3COpacityU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(CubismDynamicDrawableData_t537076974, ___U3COpacityU3Ek__BackingField_1)); }
	inline float get_U3COpacityU3Ek__BackingField_1() const { return ___U3COpacityU3Ek__BackingField_1; }
	inline float* get_address_of_U3COpacityU3Ek__BackingField_1() { return &___U3COpacityU3Ek__BackingField_1; }
	inline void set_U3COpacityU3Ek__BackingField_1(float value)
	{
		___U3COpacityU3Ek__BackingField_1 = value;
	}

	inline static int32_t get_offset_of_U3CDrawOrderU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(CubismDynamicDrawableData_t537076974, ___U3CDrawOrderU3Ek__BackingField_2)); }
	inline int32_t get_U3CDrawOrderU3Ek__BackingField_2() const { return ___U3CDrawOrderU3Ek__BackingField_2; }
	inline int32_t* get_address_of_U3CDrawOrderU3Ek__BackingField_2() { return &___U3CDrawOrderU3Ek__BackingField_2; }
	inline void set_U3CDrawOrderU3Ek__BackingField_2(int32_t value)
	{
		___U3CDrawOrderU3Ek__BackingField_2 = value;
	}

	inline static int32_t get_offset_of_U3CRenderOrderU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(CubismDynamicDrawableData_t537076974, ___U3CRenderOrderU3Ek__BackingField_3)); }
	inline int32_t get_U3CRenderOrderU3Ek__BackingField_3() const { return ___U3CRenderOrderU3Ek__BackingField_3; }
	inline int32_t* get_address_of_U3CRenderOrderU3Ek__BackingField_3() { return &___U3CRenderOrderU3Ek__BackingField_3; }
	inline void set_U3CRenderOrderU3Ek__BackingField_3(int32_t value)
	{
		___U3CRenderOrderU3Ek__BackingField_3 = value;
	}

	inline static int32_t get_offset_of_U3CVertexPositionsU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(CubismDynamicDrawableData_t537076974, ___U3CVertexPositionsU3Ek__BackingField_4)); }
	inline Vector3U5BU5D_t1172311765* get_U3CVertexPositionsU3Ek__BackingField_4() const { return ___U3CVertexPositionsU3Ek__BackingField_4; }
	inline Vector3U5BU5D_t1172311765** get_address_of_U3CVertexPositionsU3Ek__BackingField_4() { return &___U3CVertexPositionsU3Ek__BackingField_4; }
	inline void set_U3CVertexPositionsU3Ek__BackingField_4(Vector3U5BU5D_t1172311765* value)
	{
		___U3CVertexPositionsU3Ek__BackingField_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CVertexPositionsU3Ek__BackingField_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CUBISMDYNAMICDRAWABLEDATA_T537076974_H
#ifndef CUBISMLOGGING_T979420036_H
#define CUBISMLOGGING_T979420036_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Live2D.Cubism.Core.CubismLogging
struct  CubismLogging_t979420036  : public RuntimeObject
{
public:

public:
};

struct CubismLogging_t979420036_StaticFields
{
public:
	// Live2D.Cubism.Core.CubismLogging/UnmanagedLogDelegate Live2D.Cubism.Core.CubismLogging::<LogDelegate>k__BackingField
	UnmanagedLogDelegate_t742859164 * ___U3CLogDelegateU3Ek__BackingField_0;
	// Live2D.Cubism.Core.CubismLogging/UnmanagedLogDelegate Live2D.Cubism.Core.CubismLogging::<>f__mg$cache0
	UnmanagedLogDelegate_t742859164 * ___U3CU3Ef__mgU24cache0_1;

public:
	inline static int32_t get_offset_of_U3CLogDelegateU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(CubismLogging_t979420036_StaticFields, ___U3CLogDelegateU3Ek__BackingField_0)); }
	inline UnmanagedLogDelegate_t742859164 * get_U3CLogDelegateU3Ek__BackingField_0() const { return ___U3CLogDelegateU3Ek__BackingField_0; }
	inline UnmanagedLogDelegate_t742859164 ** get_address_of_U3CLogDelegateU3Ek__BackingField_0() { return &___U3CLogDelegateU3Ek__BackingField_0; }
	inline void set_U3CLogDelegateU3Ek__BackingField_0(UnmanagedLogDelegate_t742859164 * value)
	{
		___U3CLogDelegateU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CLogDelegateU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache0_1() { return static_cast<int32_t>(offsetof(CubismLogging_t979420036_StaticFields, ___U3CU3Ef__mgU24cache0_1)); }
	inline UnmanagedLogDelegate_t742859164 * get_U3CU3Ef__mgU24cache0_1() const { return ___U3CU3Ef__mgU24cache0_1; }
	inline UnmanagedLogDelegate_t742859164 ** get_address_of_U3CU3Ef__mgU24cache0_1() { return &___U3CU3Ef__mgU24cache0_1; }
	inline void set_U3CU3Ef__mgU24cache0_1(UnmanagedLogDelegate_t742859164 * value)
	{
		___U3CU3Ef__mgU24cache0_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache0_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CUBISMLOGGING_T979420036_H
#ifndef BYTEEXTENSIONMETHODS_T2486717825_H
#define BYTEEXTENSIONMETHODS_T2486717825_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Live2D.Cubism.Core.Unmanaged.ByteExtensionMethods
struct  ByteExtensionMethods_t2486717825  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BYTEEXTENSIONMETHODS_T2486717825_H
#ifndef GAMEOBJECTEXTENSIONMETHODS_T2024879434_H
#define GAMEOBJECTEXTENSIONMETHODS_T2024879434_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Live2D.Cubism.Core.GameObjectExtensionMethods
struct  GameObjectExtensionMethods_t2024879434  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GAMEOBJECTEXTENSIONMETHODS_T2024879434_H
#ifndef CUBISMTASKQUEUE_T1273848959_H
#define CUBISMTASKQUEUE_T1273848959_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Live2D.Cubism.Core.CubismTaskQueue
struct  CubismTaskQueue_t1273848959  : public RuntimeObject
{
public:

public:
};

struct CubismTaskQueue_t1273848959_StaticFields
{
public:
	// Live2D.Cubism.Core.CubismTaskQueue/CubismTaskHandler Live2D.Cubism.Core.CubismTaskQueue::OnTask
	CubismTaskHandler_t1959870214 * ___OnTask_0;

public:
	inline static int32_t get_offset_of_OnTask_0() { return static_cast<int32_t>(offsetof(CubismTaskQueue_t1273848959_StaticFields, ___OnTask_0)); }
	inline CubismTaskHandler_t1959870214 * get_OnTask_0() const { return ___OnTask_0; }
	inline CubismTaskHandler_t1959870214 ** get_address_of_OnTask_0() { return &___OnTask_0; }
	inline void set_OnTask_0(CubismTaskHandler_t1959870214 * value)
	{
		___OnTask_0 = value;
		Il2CppCodeGenWriteBarrier((&___OnTask_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CUBISMTASKQUEUE_T1273848959_H
#ifndef COMPONENTEXTENSIONMETHODS_T619716316_H
#define COMPONENTEXTENSIONMETHODS_T619716316_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Live2D.Cubism.Core.ComponentExtensionMethods
struct  ComponentExtensionMethods_t619716316  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPONENTEXTENSIONMETHODS_T619716316_H
#ifndef U3CTWEENRESTARTU3EC__ITERATOR1_T3903815285_H
#define U3CTWEENRESTARTU3EC__ITERATOR1_T3903815285_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// iTween/<TweenRestart>c__Iterator1
struct  U3CTweenRestartU3Ec__Iterator1_t3903815285  : public RuntimeObject
{
public:
	// iTween iTween/<TweenRestart>c__Iterator1::$this
	iTween_t488923914 * ___U24this_0;
	// System.Object iTween/<TweenRestart>c__Iterator1::$current
	RuntimeObject * ___U24current_1;
	// System.Boolean iTween/<TweenRestart>c__Iterator1::$disposing
	bool ___U24disposing_2;
	// System.Int32 iTween/<TweenRestart>c__Iterator1::$PC
	int32_t ___U24PC_3;

public:
	inline static int32_t get_offset_of_U24this_0() { return static_cast<int32_t>(offsetof(U3CTweenRestartU3Ec__Iterator1_t3903815285, ___U24this_0)); }
	inline iTween_t488923914 * get_U24this_0() const { return ___U24this_0; }
	inline iTween_t488923914 ** get_address_of_U24this_0() { return &___U24this_0; }
	inline void set_U24this_0(iTween_t488923914 * value)
	{
		___U24this_0 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_0), value);
	}

	inline static int32_t get_offset_of_U24current_1() { return static_cast<int32_t>(offsetof(U3CTweenRestartU3Ec__Iterator1_t3903815285, ___U24current_1)); }
	inline RuntimeObject * get_U24current_1() const { return ___U24current_1; }
	inline RuntimeObject ** get_address_of_U24current_1() { return &___U24current_1; }
	inline void set_U24current_1(RuntimeObject * value)
	{
		___U24current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_1), value);
	}

	inline static int32_t get_offset_of_U24disposing_2() { return static_cast<int32_t>(offsetof(U3CTweenRestartU3Ec__Iterator1_t3903815285, ___U24disposing_2)); }
	inline bool get_U24disposing_2() const { return ___U24disposing_2; }
	inline bool* get_address_of_U24disposing_2() { return &___U24disposing_2; }
	inline void set_U24disposing_2(bool value)
	{
		___U24disposing_2 = value;
	}

	inline static int32_t get_offset_of_U24PC_3() { return static_cast<int32_t>(offsetof(U3CTweenRestartU3Ec__Iterator1_t3903815285, ___U24PC_3)); }
	inline int32_t get_U24PC_3() const { return ___U24PC_3; }
	inline int32_t* get_address_of_U24PC_3() { return &___U24PC_3; }
	inline void set_U24PC_3(int32_t value)
	{
		___U24PC_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CTWEENRESTARTU3EC__ITERATOR1_T3903815285_H
#ifndef U3CSTARTU3EC__ITERATOR2_T1700299370_H
#define U3CSTARTU3EC__ITERATOR2_T1700299370_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// iTween/<Start>c__Iterator2
struct  U3CStartU3Ec__Iterator2_t1700299370  : public RuntimeObject
{
public:
	// iTween iTween/<Start>c__Iterator2::$this
	iTween_t488923914 * ___U24this_0;
	// System.Object iTween/<Start>c__Iterator2::$current
	RuntimeObject * ___U24current_1;
	// System.Boolean iTween/<Start>c__Iterator2::$disposing
	bool ___U24disposing_2;
	// System.Int32 iTween/<Start>c__Iterator2::$PC
	int32_t ___U24PC_3;

public:
	inline static int32_t get_offset_of_U24this_0() { return static_cast<int32_t>(offsetof(U3CStartU3Ec__Iterator2_t1700299370, ___U24this_0)); }
	inline iTween_t488923914 * get_U24this_0() const { return ___U24this_0; }
	inline iTween_t488923914 ** get_address_of_U24this_0() { return &___U24this_0; }
	inline void set_U24this_0(iTween_t488923914 * value)
	{
		___U24this_0 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_0), value);
	}

	inline static int32_t get_offset_of_U24current_1() { return static_cast<int32_t>(offsetof(U3CStartU3Ec__Iterator2_t1700299370, ___U24current_1)); }
	inline RuntimeObject * get_U24current_1() const { return ___U24current_1; }
	inline RuntimeObject ** get_address_of_U24current_1() { return &___U24current_1; }
	inline void set_U24current_1(RuntimeObject * value)
	{
		___U24current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_1), value);
	}

	inline static int32_t get_offset_of_U24disposing_2() { return static_cast<int32_t>(offsetof(U3CStartU3Ec__Iterator2_t1700299370, ___U24disposing_2)); }
	inline bool get_U24disposing_2() const { return ___U24disposing_2; }
	inline bool* get_address_of_U24disposing_2() { return &___U24disposing_2; }
	inline void set_U24disposing_2(bool value)
	{
		___U24disposing_2 = value;
	}

	inline static int32_t get_offset_of_U24PC_3() { return static_cast<int32_t>(offsetof(U3CStartU3Ec__Iterator2_t1700299370, ___U24PC_3)); }
	inline int32_t get_U24PC_3() const { return ___U24PC_3; }
	inline int32_t* get_address_of_U24PC_3() { return &___U24PC_3; }
	inline void set_U24PC_3(int32_t value)
	{
		___U24PC_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CSTARTU3EC__ITERATOR2_T1700299370_H
#ifndef ARRAYEXTENSIONMETHODS_T1733224102_H
#define ARRAYEXTENSIONMETHODS_T1733224102_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Live2D.Cubism.Core.ArrayExtensionMethods
struct  ArrayExtensionMethods_t1733224102  : public RuntimeObject
{
public:

public:
};

struct ArrayExtensionMethods_t1733224102_StaticFields
{
public:
	// System.Comparison`1<Live2D.Cubism.Core.CubismParameter> Live2D.Cubism.Core.ArrayExtensionMethods::<>f__am$cache0
	Comparison_1_t831449101 * ___U3CU3Ef__amU24cache0_0;
	// System.Comparison`1<Live2D.Cubism.Core.CubismPart> Live2D.Cubism.Core.ArrayExtensionMethods::<>f__am$cache1
	Comparison_1_t795782513 * ___U3CU3Ef__amU24cache1_1;
	// System.Comparison`1<Live2D.Cubism.Core.CubismDrawable> Live2D.Cubism.Core.ArrayExtensionMethods::<>f__am$cache2
	Comparison_1_t3970950164 * ___U3CU3Ef__amU24cache2_2;

public:
	inline static int32_t get_offset_of_U3CU3Ef__amU24cache0_0() { return static_cast<int32_t>(offsetof(ArrayExtensionMethods_t1733224102_StaticFields, ___U3CU3Ef__amU24cache0_0)); }
	inline Comparison_1_t831449101 * get_U3CU3Ef__amU24cache0_0() const { return ___U3CU3Ef__amU24cache0_0; }
	inline Comparison_1_t831449101 ** get_address_of_U3CU3Ef__amU24cache0_0() { return &___U3CU3Ef__amU24cache0_0; }
	inline void set_U3CU3Ef__amU24cache0_0(Comparison_1_t831449101 * value)
	{
		___U3CU3Ef__amU24cache0_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache0_0), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache1_1() { return static_cast<int32_t>(offsetof(ArrayExtensionMethods_t1733224102_StaticFields, ___U3CU3Ef__amU24cache1_1)); }
	inline Comparison_1_t795782513 * get_U3CU3Ef__amU24cache1_1() const { return ___U3CU3Ef__amU24cache1_1; }
	inline Comparison_1_t795782513 ** get_address_of_U3CU3Ef__amU24cache1_1() { return &___U3CU3Ef__amU24cache1_1; }
	inline void set_U3CU3Ef__amU24cache1_1(Comparison_1_t795782513 * value)
	{
		___U3CU3Ef__amU24cache1_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache1_1), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache2_2() { return static_cast<int32_t>(offsetof(ArrayExtensionMethods_t1733224102_StaticFields, ___U3CU3Ef__amU24cache2_2)); }
	inline Comparison_1_t3970950164 * get_U3CU3Ef__amU24cache2_2() const { return ___U3CU3Ef__amU24cache2_2; }
	inline Comparison_1_t3970950164 ** get_address_of_U3CU3Ef__amU24cache2_2() { return &___U3CU3Ef__amU24cache2_2; }
	inline void set_U3CU3Ef__amU24cache2_2(Comparison_1_t3970950164 * value)
	{
		___U3CU3Ef__amU24cache2_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache2_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ARRAYEXTENSIONMETHODS_T1733224102_H
#ifndef U3CFINDBYIDU3EC__ANONSTOREY0_T1513557713_H
#define U3CFINDBYIDU3EC__ANONSTOREY0_T1513557713_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Live2D.Cubism.Core.ArrayExtensionMethods/<FindById>c__AnonStorey0
struct  U3CFindByIdU3Ec__AnonStorey0_t1513557713  : public RuntimeObject
{
public:
	// System.String Live2D.Cubism.Core.ArrayExtensionMethods/<FindById>c__AnonStorey0::id
	String_t* ___id_0;

public:
	inline static int32_t get_offset_of_id_0() { return static_cast<int32_t>(offsetof(U3CFindByIdU3Ec__AnonStorey0_t1513557713, ___id_0)); }
	inline String_t* get_id_0() const { return ___id_0; }
	inline String_t** get_address_of_id_0() { return &___id_0; }
	inline void set_id_0(String_t* value)
	{
		___id_0 = value;
		Il2CppCodeGenWriteBarrier((&___id_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CFINDBYIDU3EC__ANONSTOREY0_T1513557713_H
#ifndef U3CFINDBYIDU3EC__ANONSTOREY1_T1513557712_H
#define U3CFINDBYIDU3EC__ANONSTOREY1_T1513557712_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Live2D.Cubism.Core.ArrayExtensionMethods/<FindById>c__AnonStorey1
struct  U3CFindByIdU3Ec__AnonStorey1_t1513557712  : public RuntimeObject
{
public:
	// System.String Live2D.Cubism.Core.ArrayExtensionMethods/<FindById>c__AnonStorey1::id
	String_t* ___id_0;

public:
	inline static int32_t get_offset_of_id_0() { return static_cast<int32_t>(offsetof(U3CFindByIdU3Ec__AnonStorey1_t1513557712, ___id_0)); }
	inline String_t* get_id_0() const { return ___id_0; }
	inline String_t** get_address_of_id_0() { return &___id_0; }
	inline void set_id_0(String_t* value)
	{
		___id_0 = value;
		Il2CppCodeGenWriteBarrier((&___id_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CFINDBYIDU3EC__ANONSTOREY1_T1513557712_H
#ifndef U3CFINDBYIDU3EC__ANONSTOREY2_T1513557715_H
#define U3CFINDBYIDU3EC__ANONSTOREY2_T1513557715_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Live2D.Cubism.Core.ArrayExtensionMethods/<FindById>c__AnonStorey2
struct  U3CFindByIdU3Ec__AnonStorey2_t1513557715  : public RuntimeObject
{
public:
	// System.String Live2D.Cubism.Core.ArrayExtensionMethods/<FindById>c__AnonStorey2::id
	String_t* ___id_0;

public:
	inline static int32_t get_offset_of_id_0() { return static_cast<int32_t>(offsetof(U3CFindByIdU3Ec__AnonStorey2_t1513557715, ___id_0)); }
	inline String_t* get_id_0() const { return ___id_0; }
	inline String_t** get_address_of_id_0() { return &___id_0; }
	inline void set_id_0(String_t* value)
	{
		___id_0 = value;
		Il2CppCodeGenWriteBarrier((&___id_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CFINDBYIDU3EC__ANONSTOREY2_T1513557715_H
#ifndef ENUM_T2459695545_H
#define ENUM_T2459695545_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Enum
struct  Enum_t2459695545  : public ValueType_t3507792607
{
public:

public:
};

struct Enum_t2459695545_StaticFields
{
public:
	// System.Char[] System.Enum::split_char
	CharU5BU5D_t1328083999* ___split_char_0;

public:
	inline static int32_t get_offset_of_split_char_0() { return static_cast<int32_t>(offsetof(Enum_t2459695545_StaticFields, ___split_char_0)); }
	inline CharU5BU5D_t1328083999* get_split_char_0() const { return ___split_char_0; }
	inline CharU5BU5D_t1328083999** get_address_of_split_char_0() { return &___split_char_0; }
	inline void set_split_char_0(CharU5BU5D_t1328083999* value)
	{
		___split_char_0 = value;
		Il2CppCodeGenWriteBarrier((&___split_char_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t2459695545_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t2459695545_marshaled_com
{
};
#endif // ENUM_T2459695545_H
#ifndef U24ARRAYTYPEU3D12_T1568637717_H
#define U24ARRAYTYPEU3D12_T1568637717_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>/$ArrayType=12
#pragma pack(push, tp, 1)
struct  U24ArrayTypeU3D12_t1568637717 
{
public:
	union
	{
		struct
		{
		};
		uint8_t U24ArrayTypeU3D12_t1568637717__padding[12];
	};

public:
};
#pragma pack(pop, tp)

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U24ARRAYTYPEU3D12_T1568637717_H
#ifndef DRIVENRECTTRANSFORMTRACKER_T154385424_H
#define DRIVENRECTTRANSFORMTRACKER_T154385424_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.DrivenRectTransformTracker
struct  DrivenRectTransformTracker_t154385424 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DRIVENRECTTRANSFORMTRACKER_T154385424_H
#ifndef VECTOR2_T2243707579_H
#define VECTOR2_T2243707579_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector2
struct  Vector2_t2243707579 
{
public:
	// System.Single UnityEngine.Vector2::x
	float ___x_0;
	// System.Single UnityEngine.Vector2::y
	float ___y_1;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Vector2_t2243707579, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Vector2_t2243707579, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}
};

struct Vector2_t2243707579_StaticFields
{
public:
	// UnityEngine.Vector2 UnityEngine.Vector2::zeroVector
	Vector2_t2243707579  ___zeroVector_2;
	// UnityEngine.Vector2 UnityEngine.Vector2::oneVector
	Vector2_t2243707579  ___oneVector_3;
	// UnityEngine.Vector2 UnityEngine.Vector2::upVector
	Vector2_t2243707579  ___upVector_4;
	// UnityEngine.Vector2 UnityEngine.Vector2::downVector
	Vector2_t2243707579  ___downVector_5;
	// UnityEngine.Vector2 UnityEngine.Vector2::leftVector
	Vector2_t2243707579  ___leftVector_6;
	// UnityEngine.Vector2 UnityEngine.Vector2::rightVector
	Vector2_t2243707579  ___rightVector_7;
	// UnityEngine.Vector2 UnityEngine.Vector2::positiveInfinityVector
	Vector2_t2243707579  ___positiveInfinityVector_8;
	// UnityEngine.Vector2 UnityEngine.Vector2::negativeInfinityVector
	Vector2_t2243707579  ___negativeInfinityVector_9;

public:
	inline static int32_t get_offset_of_zeroVector_2() { return static_cast<int32_t>(offsetof(Vector2_t2243707579_StaticFields, ___zeroVector_2)); }
	inline Vector2_t2243707579  get_zeroVector_2() const { return ___zeroVector_2; }
	inline Vector2_t2243707579 * get_address_of_zeroVector_2() { return &___zeroVector_2; }
	inline void set_zeroVector_2(Vector2_t2243707579  value)
	{
		___zeroVector_2 = value;
	}

	inline static int32_t get_offset_of_oneVector_3() { return static_cast<int32_t>(offsetof(Vector2_t2243707579_StaticFields, ___oneVector_3)); }
	inline Vector2_t2243707579  get_oneVector_3() const { return ___oneVector_3; }
	inline Vector2_t2243707579 * get_address_of_oneVector_3() { return &___oneVector_3; }
	inline void set_oneVector_3(Vector2_t2243707579  value)
	{
		___oneVector_3 = value;
	}

	inline static int32_t get_offset_of_upVector_4() { return static_cast<int32_t>(offsetof(Vector2_t2243707579_StaticFields, ___upVector_4)); }
	inline Vector2_t2243707579  get_upVector_4() const { return ___upVector_4; }
	inline Vector2_t2243707579 * get_address_of_upVector_4() { return &___upVector_4; }
	inline void set_upVector_4(Vector2_t2243707579  value)
	{
		___upVector_4 = value;
	}

	inline static int32_t get_offset_of_downVector_5() { return static_cast<int32_t>(offsetof(Vector2_t2243707579_StaticFields, ___downVector_5)); }
	inline Vector2_t2243707579  get_downVector_5() const { return ___downVector_5; }
	inline Vector2_t2243707579 * get_address_of_downVector_5() { return &___downVector_5; }
	inline void set_downVector_5(Vector2_t2243707579  value)
	{
		___downVector_5 = value;
	}

	inline static int32_t get_offset_of_leftVector_6() { return static_cast<int32_t>(offsetof(Vector2_t2243707579_StaticFields, ___leftVector_6)); }
	inline Vector2_t2243707579  get_leftVector_6() const { return ___leftVector_6; }
	inline Vector2_t2243707579 * get_address_of_leftVector_6() { return &___leftVector_6; }
	inline void set_leftVector_6(Vector2_t2243707579  value)
	{
		___leftVector_6 = value;
	}

	inline static int32_t get_offset_of_rightVector_7() { return static_cast<int32_t>(offsetof(Vector2_t2243707579_StaticFields, ___rightVector_7)); }
	inline Vector2_t2243707579  get_rightVector_7() const { return ___rightVector_7; }
	inline Vector2_t2243707579 * get_address_of_rightVector_7() { return &___rightVector_7; }
	inline void set_rightVector_7(Vector2_t2243707579  value)
	{
		___rightVector_7 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_8() { return static_cast<int32_t>(offsetof(Vector2_t2243707579_StaticFields, ___positiveInfinityVector_8)); }
	inline Vector2_t2243707579  get_positiveInfinityVector_8() const { return ___positiveInfinityVector_8; }
	inline Vector2_t2243707579 * get_address_of_positiveInfinityVector_8() { return &___positiveInfinityVector_8; }
	inline void set_positiveInfinityVector_8(Vector2_t2243707579  value)
	{
		___positiveInfinityVector_8 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_9() { return static_cast<int32_t>(offsetof(Vector2_t2243707579_StaticFields, ___negativeInfinityVector_9)); }
	inline Vector2_t2243707579  get_negativeInfinityVector_9() const { return ___negativeInfinityVector_9; }
	inline Vector2_t2243707579 * get_address_of_negativeInfinityVector_9() { return &___negativeInfinityVector_9; }
	inline void set_negativeInfinityVector_9(Vector2_t2243707579  value)
	{
		___negativeInfinityVector_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR2_T2243707579_H
#ifndef INT32_T2071877448_H
#define INT32_T2071877448_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Int32
struct  Int32_t2071877448 
{
public:
	// System.Int32 System.Int32::m_value
	int32_t ___m_value_2;

public:
	inline static int32_t get_offset_of_m_value_2() { return static_cast<int32_t>(offsetof(Int32_t2071877448, ___m_value_2)); }
	inline int32_t get_m_value_2() const { return ___m_value_2; }
	inline int32_t* get_address_of_m_value_2() { return &___m_value_2; }
	inline void set_m_value_2(int32_t value)
	{
		___m_value_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INT32_T2071877448_H
#ifndef INTPTR_T_H
#define INTPTR_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTPTR_T_H
#ifndef VECTOR4_T2243707581_H
#define VECTOR4_T2243707581_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector4
struct  Vector4_t2243707581 
{
public:
	// System.Single UnityEngine.Vector4::x
	float ___x_1;
	// System.Single UnityEngine.Vector4::y
	float ___y_2;
	// System.Single UnityEngine.Vector4::z
	float ___z_3;
	// System.Single UnityEngine.Vector4::w
	float ___w_4;

public:
	inline static int32_t get_offset_of_x_1() { return static_cast<int32_t>(offsetof(Vector4_t2243707581, ___x_1)); }
	inline float get_x_1() const { return ___x_1; }
	inline float* get_address_of_x_1() { return &___x_1; }
	inline void set_x_1(float value)
	{
		___x_1 = value;
	}

	inline static int32_t get_offset_of_y_2() { return static_cast<int32_t>(offsetof(Vector4_t2243707581, ___y_2)); }
	inline float get_y_2() const { return ___y_2; }
	inline float* get_address_of_y_2() { return &___y_2; }
	inline void set_y_2(float value)
	{
		___y_2 = value;
	}

	inline static int32_t get_offset_of_z_3() { return static_cast<int32_t>(offsetof(Vector4_t2243707581, ___z_3)); }
	inline float get_z_3() const { return ___z_3; }
	inline float* get_address_of_z_3() { return &___z_3; }
	inline void set_z_3(float value)
	{
		___z_3 = value;
	}

	inline static int32_t get_offset_of_w_4() { return static_cast<int32_t>(offsetof(Vector4_t2243707581, ___w_4)); }
	inline float get_w_4() const { return ___w_4; }
	inline float* get_address_of_w_4() { return &___w_4; }
	inline void set_w_4(float value)
	{
		___w_4 = value;
	}
};

struct Vector4_t2243707581_StaticFields
{
public:
	// UnityEngine.Vector4 UnityEngine.Vector4::zeroVector
	Vector4_t2243707581  ___zeroVector_5;
	// UnityEngine.Vector4 UnityEngine.Vector4::oneVector
	Vector4_t2243707581  ___oneVector_6;
	// UnityEngine.Vector4 UnityEngine.Vector4::positiveInfinityVector
	Vector4_t2243707581  ___positiveInfinityVector_7;
	// UnityEngine.Vector4 UnityEngine.Vector4::negativeInfinityVector
	Vector4_t2243707581  ___negativeInfinityVector_8;

public:
	inline static int32_t get_offset_of_zeroVector_5() { return static_cast<int32_t>(offsetof(Vector4_t2243707581_StaticFields, ___zeroVector_5)); }
	inline Vector4_t2243707581  get_zeroVector_5() const { return ___zeroVector_5; }
	inline Vector4_t2243707581 * get_address_of_zeroVector_5() { return &___zeroVector_5; }
	inline void set_zeroVector_5(Vector4_t2243707581  value)
	{
		___zeroVector_5 = value;
	}

	inline static int32_t get_offset_of_oneVector_6() { return static_cast<int32_t>(offsetof(Vector4_t2243707581_StaticFields, ___oneVector_6)); }
	inline Vector4_t2243707581  get_oneVector_6() const { return ___oneVector_6; }
	inline Vector4_t2243707581 * get_address_of_oneVector_6() { return &___oneVector_6; }
	inline void set_oneVector_6(Vector4_t2243707581  value)
	{
		___oneVector_6 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_7() { return static_cast<int32_t>(offsetof(Vector4_t2243707581_StaticFields, ___positiveInfinityVector_7)); }
	inline Vector4_t2243707581  get_positiveInfinityVector_7() const { return ___positiveInfinityVector_7; }
	inline Vector4_t2243707581 * get_address_of_positiveInfinityVector_7() { return &___positiveInfinityVector_7; }
	inline void set_positiveInfinityVector_7(Vector4_t2243707581  value)
	{
		___positiveInfinityVector_7 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_8() { return static_cast<int32_t>(offsetof(Vector4_t2243707581_StaticFields, ___negativeInfinityVector_8)); }
	inline Vector4_t2243707581  get_negativeInfinityVector_8() const { return ___negativeInfinityVector_8; }
	inline Vector4_t2243707581 * get_address_of_negativeInfinityVector_8() { return &___negativeInfinityVector_8; }
	inline void set_negativeInfinityVector_8(Vector4_t2243707581  value)
	{
		___negativeInfinityVector_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR4_T2243707581_H
#ifndef VECTOR3_T2243707580_H
#define VECTOR3_T2243707580_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector3
struct  Vector3_t2243707580 
{
public:
	// System.Single UnityEngine.Vector3::x
	float ___x_1;
	// System.Single UnityEngine.Vector3::y
	float ___y_2;
	// System.Single UnityEngine.Vector3::z
	float ___z_3;

public:
	inline static int32_t get_offset_of_x_1() { return static_cast<int32_t>(offsetof(Vector3_t2243707580, ___x_1)); }
	inline float get_x_1() const { return ___x_1; }
	inline float* get_address_of_x_1() { return &___x_1; }
	inline void set_x_1(float value)
	{
		___x_1 = value;
	}

	inline static int32_t get_offset_of_y_2() { return static_cast<int32_t>(offsetof(Vector3_t2243707580, ___y_2)); }
	inline float get_y_2() const { return ___y_2; }
	inline float* get_address_of_y_2() { return &___y_2; }
	inline void set_y_2(float value)
	{
		___y_2 = value;
	}

	inline static int32_t get_offset_of_z_3() { return static_cast<int32_t>(offsetof(Vector3_t2243707580, ___z_3)); }
	inline float get_z_3() const { return ___z_3; }
	inline float* get_address_of_z_3() { return &___z_3; }
	inline void set_z_3(float value)
	{
		___z_3 = value;
	}
};

struct Vector3_t2243707580_StaticFields
{
public:
	// UnityEngine.Vector3 UnityEngine.Vector3::zeroVector
	Vector3_t2243707580  ___zeroVector_4;
	// UnityEngine.Vector3 UnityEngine.Vector3::oneVector
	Vector3_t2243707580  ___oneVector_5;
	// UnityEngine.Vector3 UnityEngine.Vector3::upVector
	Vector3_t2243707580  ___upVector_6;
	// UnityEngine.Vector3 UnityEngine.Vector3::downVector
	Vector3_t2243707580  ___downVector_7;
	// UnityEngine.Vector3 UnityEngine.Vector3::leftVector
	Vector3_t2243707580  ___leftVector_8;
	// UnityEngine.Vector3 UnityEngine.Vector3::rightVector
	Vector3_t2243707580  ___rightVector_9;
	// UnityEngine.Vector3 UnityEngine.Vector3::forwardVector
	Vector3_t2243707580  ___forwardVector_10;
	// UnityEngine.Vector3 UnityEngine.Vector3::backVector
	Vector3_t2243707580  ___backVector_11;
	// UnityEngine.Vector3 UnityEngine.Vector3::positiveInfinityVector
	Vector3_t2243707580  ___positiveInfinityVector_12;
	// UnityEngine.Vector3 UnityEngine.Vector3::negativeInfinityVector
	Vector3_t2243707580  ___negativeInfinityVector_13;

public:
	inline static int32_t get_offset_of_zeroVector_4() { return static_cast<int32_t>(offsetof(Vector3_t2243707580_StaticFields, ___zeroVector_4)); }
	inline Vector3_t2243707580  get_zeroVector_4() const { return ___zeroVector_4; }
	inline Vector3_t2243707580 * get_address_of_zeroVector_4() { return &___zeroVector_4; }
	inline void set_zeroVector_4(Vector3_t2243707580  value)
	{
		___zeroVector_4 = value;
	}

	inline static int32_t get_offset_of_oneVector_5() { return static_cast<int32_t>(offsetof(Vector3_t2243707580_StaticFields, ___oneVector_5)); }
	inline Vector3_t2243707580  get_oneVector_5() const { return ___oneVector_5; }
	inline Vector3_t2243707580 * get_address_of_oneVector_5() { return &___oneVector_5; }
	inline void set_oneVector_5(Vector3_t2243707580  value)
	{
		___oneVector_5 = value;
	}

	inline static int32_t get_offset_of_upVector_6() { return static_cast<int32_t>(offsetof(Vector3_t2243707580_StaticFields, ___upVector_6)); }
	inline Vector3_t2243707580  get_upVector_6() const { return ___upVector_6; }
	inline Vector3_t2243707580 * get_address_of_upVector_6() { return &___upVector_6; }
	inline void set_upVector_6(Vector3_t2243707580  value)
	{
		___upVector_6 = value;
	}

	inline static int32_t get_offset_of_downVector_7() { return static_cast<int32_t>(offsetof(Vector3_t2243707580_StaticFields, ___downVector_7)); }
	inline Vector3_t2243707580  get_downVector_7() const { return ___downVector_7; }
	inline Vector3_t2243707580 * get_address_of_downVector_7() { return &___downVector_7; }
	inline void set_downVector_7(Vector3_t2243707580  value)
	{
		___downVector_7 = value;
	}

	inline static int32_t get_offset_of_leftVector_8() { return static_cast<int32_t>(offsetof(Vector3_t2243707580_StaticFields, ___leftVector_8)); }
	inline Vector3_t2243707580  get_leftVector_8() const { return ___leftVector_8; }
	inline Vector3_t2243707580 * get_address_of_leftVector_8() { return &___leftVector_8; }
	inline void set_leftVector_8(Vector3_t2243707580  value)
	{
		___leftVector_8 = value;
	}

	inline static int32_t get_offset_of_rightVector_9() { return static_cast<int32_t>(offsetof(Vector3_t2243707580_StaticFields, ___rightVector_9)); }
	inline Vector3_t2243707580  get_rightVector_9() const { return ___rightVector_9; }
	inline Vector3_t2243707580 * get_address_of_rightVector_9() { return &___rightVector_9; }
	inline void set_rightVector_9(Vector3_t2243707580  value)
	{
		___rightVector_9 = value;
	}

	inline static int32_t get_offset_of_forwardVector_10() { return static_cast<int32_t>(offsetof(Vector3_t2243707580_StaticFields, ___forwardVector_10)); }
	inline Vector3_t2243707580  get_forwardVector_10() const { return ___forwardVector_10; }
	inline Vector3_t2243707580 * get_address_of_forwardVector_10() { return &___forwardVector_10; }
	inline void set_forwardVector_10(Vector3_t2243707580  value)
	{
		___forwardVector_10 = value;
	}

	inline static int32_t get_offset_of_backVector_11() { return static_cast<int32_t>(offsetof(Vector3_t2243707580_StaticFields, ___backVector_11)); }
	inline Vector3_t2243707580  get_backVector_11() const { return ___backVector_11; }
	inline Vector3_t2243707580 * get_address_of_backVector_11() { return &___backVector_11; }
	inline void set_backVector_11(Vector3_t2243707580  value)
	{
		___backVector_11 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_12() { return static_cast<int32_t>(offsetof(Vector3_t2243707580_StaticFields, ___positiveInfinityVector_12)); }
	inline Vector3_t2243707580  get_positiveInfinityVector_12() const { return ___positiveInfinityVector_12; }
	inline Vector3_t2243707580 * get_address_of_positiveInfinityVector_12() { return &___positiveInfinityVector_12; }
	inline void set_positiveInfinityVector_12(Vector3_t2243707580  value)
	{
		___positiveInfinityVector_12 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_13() { return static_cast<int32_t>(offsetof(Vector3_t2243707580_StaticFields, ___negativeInfinityVector_13)); }
	inline Vector3_t2243707580  get_negativeInfinityVector_13() const { return ___negativeInfinityVector_13; }
	inline Vector3_t2243707580 * get_address_of_negativeInfinityVector_13() { return &___negativeInfinityVector_13; }
	inline void set_negativeInfinityVector_13(Vector3_t2243707580  value)
	{
		___negativeInfinityVector_13 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR3_T2243707580_H
#ifndef COLOR_T2020392075_H
#define COLOR_T2020392075_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Color
struct  Color_t2020392075 
{
public:
	// System.Single UnityEngine.Color::r
	float ___r_0;
	// System.Single UnityEngine.Color::g
	float ___g_1;
	// System.Single UnityEngine.Color::b
	float ___b_2;
	// System.Single UnityEngine.Color::a
	float ___a_3;

public:
	inline static int32_t get_offset_of_r_0() { return static_cast<int32_t>(offsetof(Color_t2020392075, ___r_0)); }
	inline float get_r_0() const { return ___r_0; }
	inline float* get_address_of_r_0() { return &___r_0; }
	inline void set_r_0(float value)
	{
		___r_0 = value;
	}

	inline static int32_t get_offset_of_g_1() { return static_cast<int32_t>(offsetof(Color_t2020392075, ___g_1)); }
	inline float get_g_1() const { return ___g_1; }
	inline float* get_address_of_g_1() { return &___g_1; }
	inline void set_g_1(float value)
	{
		___g_1 = value;
	}

	inline static int32_t get_offset_of_b_2() { return static_cast<int32_t>(offsetof(Color_t2020392075, ___b_2)); }
	inline float get_b_2() const { return ___b_2; }
	inline float* get_address_of_b_2() { return &___b_2; }
	inline void set_b_2(float value)
	{
		___b_2 = value;
	}

	inline static int32_t get_offset_of_a_3() { return static_cast<int32_t>(offsetof(Color_t2020392075, ___a_3)); }
	inline float get_a_3() const { return ___a_3; }
	inline float* get_address_of_a_3() { return &___a_3; }
	inline void set_a_3(float value)
	{
		___a_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLOR_T2020392075_H
#ifndef VOID_T1841601450_H
#define VOID_T1841601450_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Void
struct  Void_t1841601450 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VOID_T1841601450_H
#ifndef SINGLE_T2076509932_H
#define SINGLE_T2076509932_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Single
struct  Single_t2076509932 
{
public:
	// System.Single System.Single::m_value
	float ___m_value_7;

public:
	inline static int32_t get_offset_of_m_value_7() { return static_cast<int32_t>(offsetof(Single_t2076509932, ___m_value_7)); }
	inline float get_m_value_7() const { return ___m_value_7; }
	inline float* get_address_of_m_value_7() { return &___m_value_7; }
	inline void set_m_value_7(float value)
	{
		___m_value_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SINGLE_T2076509932_H
#ifndef CUBISMUNMANAGEDINTARRAYVIEW_T965365490_H
#define CUBISMUNMANAGEDINTARRAYVIEW_T965365490_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Live2D.Cubism.Core.Unmanaged.CubismUnmanagedIntArrayView
struct  CubismUnmanagedIntArrayView_t965365490  : public RuntimeObject
{
public:
	// System.Int32 Live2D.Cubism.Core.Unmanaged.CubismUnmanagedIntArrayView::<Length>k__BackingField
	int32_t ___U3CLengthU3Ek__BackingField_0;
	// System.IntPtr Live2D.Cubism.Core.Unmanaged.CubismUnmanagedIntArrayView::<Address>k__BackingField
	intptr_t ___U3CAddressU3Ek__BackingField_1;

public:
	inline static int32_t get_offset_of_U3CLengthU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(CubismUnmanagedIntArrayView_t965365490, ___U3CLengthU3Ek__BackingField_0)); }
	inline int32_t get_U3CLengthU3Ek__BackingField_0() const { return ___U3CLengthU3Ek__BackingField_0; }
	inline int32_t* get_address_of_U3CLengthU3Ek__BackingField_0() { return &___U3CLengthU3Ek__BackingField_0; }
	inline void set_U3CLengthU3Ek__BackingField_0(int32_t value)
	{
		___U3CLengthU3Ek__BackingField_0 = value;
	}

	inline static int32_t get_offset_of_U3CAddressU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(CubismUnmanagedIntArrayView_t965365490, ___U3CAddressU3Ek__BackingField_1)); }
	inline intptr_t get_U3CAddressU3Ek__BackingField_1() const { return ___U3CAddressU3Ek__BackingField_1; }
	inline intptr_t* get_address_of_U3CAddressU3Ek__BackingField_1() { return &___U3CAddressU3Ek__BackingField_1; }
	inline void set_U3CAddressU3Ek__BackingField_1(intptr_t value)
	{
		___U3CAddressU3Ek__BackingField_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CUBISMUNMANAGEDINTARRAYVIEW_T965365490_H
#ifndef CONSTRAINT_T3558160636_H
#define CONSTRAINT_T3558160636_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.GridLayoutGroup/Constraint
struct  Constraint_t3558160636 
{
public:
	// System.Int32 UnityEngine.UI.GridLayoutGroup/Constraint::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(Constraint_t3558160636, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONSTRAINT_T3558160636_H
#ifndef ASPECTMODE_T1166448724_H
#define ASPECTMODE_T1166448724_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.AspectRatioFitter/AspectMode
struct  AspectMode_t1166448724 
{
public:
	// System.Int32 UnityEngine.UI.AspectRatioFitter/AspectMode::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(AspectMode_t1166448724, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ASPECTMODE_T1166448724_H
#ifndef OBJECT_T1021602117_H
#define OBJECT_T1021602117_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Object
struct  Object_t1021602117  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Object::m_CachedPtr
	intptr_t ___m_CachedPtr_0;

public:
	inline static int32_t get_offset_of_m_CachedPtr_0() { return static_cast<int32_t>(offsetof(Object_t1021602117, ___m_CachedPtr_0)); }
	inline intptr_t get_m_CachedPtr_0() const { return ___m_CachedPtr_0; }
	inline intptr_t* get_address_of_m_CachedPtr_0() { return &___m_CachedPtr_0; }
	inline void set_m_CachedPtr_0(intptr_t value)
	{
		___m_CachedPtr_0 = value;
	}
};

struct Object_t1021602117_StaticFields
{
public:
	// System.Int32 UnityEngine.Object::OffsetOfInstanceIDInCPlusPlusObject
	int32_t ___OffsetOfInstanceIDInCPlusPlusObject_1;

public:
	inline static int32_t get_offset_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return static_cast<int32_t>(offsetof(Object_t1021602117_StaticFields, ___OffsetOfInstanceIDInCPlusPlusObject_1)); }
	inline int32_t get_OffsetOfInstanceIDInCPlusPlusObject_1() const { return ___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline int32_t* get_address_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return &___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline void set_OffsetOfInstanceIDInCPlusPlusObject_1(int32_t value)
	{
		___OffsetOfInstanceIDInCPlusPlusObject_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.Object
struct Object_t1021602117_marshaled_pinvoke
{
	intptr_t ___m_CachedPtr_0;
};
// Native definition for COM marshalling of UnityEngine.Object
struct Object_t1021602117_marshaled_com
{
	intptr_t ___m_CachedPtr_0;
};
#endif // OBJECT_T1021602117_H
#ifndef CUBISMUNMANAGEDUSHORTARRAYVIEW_T2529819058_H
#define CUBISMUNMANAGEDUSHORTARRAYVIEW_T2529819058_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Live2D.Cubism.Core.Unmanaged.CubismUnmanagedUshortArrayView
struct  CubismUnmanagedUshortArrayView_t2529819058  : public RuntimeObject
{
public:
	// System.Int32 Live2D.Cubism.Core.Unmanaged.CubismUnmanagedUshortArrayView::<Length>k__BackingField
	int32_t ___U3CLengthU3Ek__BackingField_0;
	// System.IntPtr Live2D.Cubism.Core.Unmanaged.CubismUnmanagedUshortArrayView::<Address>k__BackingField
	intptr_t ___U3CAddressU3Ek__BackingField_1;

public:
	inline static int32_t get_offset_of_U3CLengthU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(CubismUnmanagedUshortArrayView_t2529819058, ___U3CLengthU3Ek__BackingField_0)); }
	inline int32_t get_U3CLengthU3Ek__BackingField_0() const { return ___U3CLengthU3Ek__BackingField_0; }
	inline int32_t* get_address_of_U3CLengthU3Ek__BackingField_0() { return &___U3CLengthU3Ek__BackingField_0; }
	inline void set_U3CLengthU3Ek__BackingField_0(int32_t value)
	{
		___U3CLengthU3Ek__BackingField_0 = value;
	}

	inline static int32_t get_offset_of_U3CAddressU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(CubismUnmanagedUshortArrayView_t2529819058, ___U3CAddressU3Ek__BackingField_1)); }
	inline intptr_t get_U3CAddressU3Ek__BackingField_1() const { return ___U3CAddressU3Ek__BackingField_1; }
	inline intptr_t* get_address_of_U3CAddressU3Ek__BackingField_1() { return &___U3CAddressU3Ek__BackingField_1; }
	inline void set_U3CAddressU3Ek__BackingField_1(intptr_t value)
	{
		___U3CAddressU3Ek__BackingField_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CUBISMUNMANAGEDUSHORTARRAYVIEW_T2529819058_H
#ifndef TASKSTATE_T3546624310_H
#define TASKSTATE_T3546624310_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Live2D.Cubism.Core.CubismTaskableModel/TaskState
struct  TaskState_t3546624310 
{
public:
	// System.Int32 Live2D.Cubism.Core.CubismTaskableModel/TaskState::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(TaskState_t3546624310, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TASKSTATE_T3546624310_H
#ifndef AXIS_T1431825778_H
#define AXIS_T1431825778_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.GridLayoutGroup/Axis
struct  Axis_t1431825778 
{
public:
	// System.Int32 UnityEngine.UI.GridLayoutGroup/Axis::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(Axis_t1431825778, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AXIS_T1431825778_H
#ifndef UNIT_T3220761768_H
#define UNIT_T3220761768_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.CanvasScaler/Unit
struct  Unit_t3220761768 
{
public:
	// System.Int32 UnityEngine.UI.CanvasScaler/Unit::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(Unit_t3220761768, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNIT_T3220761768_H
#ifndef SCREENMATCHMODE_T1916789528_H
#define SCREENMATCHMODE_T1916789528_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.CanvasScaler/ScreenMatchMode
struct  ScreenMatchMode_t1916789528 
{
public:
	// System.Int32 UnityEngine.UI.CanvasScaler/ScreenMatchMode::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(ScreenMatchMode_t1916789528, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SCREENMATCHMODE_T1916789528_H
#ifndef SCALEMODE_T987318053_H
#define SCALEMODE_T987318053_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.CanvasScaler/ScaleMode
struct  ScaleMode_t987318053 
{
public:
	// System.Int32 UnityEngine.UI.CanvasScaler/ScaleMode::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(ScaleMode_t987318053, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SCALEMODE_T987318053_H
#ifndef FITMODE_T4030874534_H
#define FITMODE_T4030874534_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.ContentSizeFitter/FitMode
struct  FitMode_t4030874534 
{
public:
	// System.Int32 UnityEngine.UI.ContentSizeFitter/FitMode::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(FitMode_t4030874534, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FITMODE_T4030874534_H
#ifndef DELEGATE_T3022476291_H
#define DELEGATE_T3022476291_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Delegate
struct  Delegate_t3022476291  : public RuntimeObject
{
public:
	// System.IntPtr System.Delegate::method_ptr
	Il2CppMethodPointer ___method_ptr_0;
	// System.IntPtr System.Delegate::invoke_impl
	intptr_t ___invoke_impl_1;
	// System.Object System.Delegate::m_target
	RuntimeObject * ___m_target_2;
	// System.IntPtr System.Delegate::method
	intptr_t ___method_3;
	// System.IntPtr System.Delegate::delegate_trampoline
	intptr_t ___delegate_trampoline_4;
	// System.IntPtr System.Delegate::method_code
	intptr_t ___method_code_5;
	// System.Reflection.MethodInfo System.Delegate::method_info
	MethodInfo_t * ___method_info_6;
	// System.Reflection.MethodInfo System.Delegate::original_method_info
	MethodInfo_t * ___original_method_info_7;
	// System.DelegateData System.Delegate::data
	DelegateData_t1572802995 * ___data_8;

public:
	inline static int32_t get_offset_of_method_ptr_0() { return static_cast<int32_t>(offsetof(Delegate_t3022476291, ___method_ptr_0)); }
	inline Il2CppMethodPointer get_method_ptr_0() const { return ___method_ptr_0; }
	inline Il2CppMethodPointer* get_address_of_method_ptr_0() { return &___method_ptr_0; }
	inline void set_method_ptr_0(Il2CppMethodPointer value)
	{
		___method_ptr_0 = value;
	}

	inline static int32_t get_offset_of_invoke_impl_1() { return static_cast<int32_t>(offsetof(Delegate_t3022476291, ___invoke_impl_1)); }
	inline intptr_t get_invoke_impl_1() const { return ___invoke_impl_1; }
	inline intptr_t* get_address_of_invoke_impl_1() { return &___invoke_impl_1; }
	inline void set_invoke_impl_1(intptr_t value)
	{
		___invoke_impl_1 = value;
	}

	inline static int32_t get_offset_of_m_target_2() { return static_cast<int32_t>(offsetof(Delegate_t3022476291, ___m_target_2)); }
	inline RuntimeObject * get_m_target_2() const { return ___m_target_2; }
	inline RuntimeObject ** get_address_of_m_target_2() { return &___m_target_2; }
	inline void set_m_target_2(RuntimeObject * value)
	{
		___m_target_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_target_2), value);
	}

	inline static int32_t get_offset_of_method_3() { return static_cast<int32_t>(offsetof(Delegate_t3022476291, ___method_3)); }
	inline intptr_t get_method_3() const { return ___method_3; }
	inline intptr_t* get_address_of_method_3() { return &___method_3; }
	inline void set_method_3(intptr_t value)
	{
		___method_3 = value;
	}

	inline static int32_t get_offset_of_delegate_trampoline_4() { return static_cast<int32_t>(offsetof(Delegate_t3022476291, ___delegate_trampoline_4)); }
	inline intptr_t get_delegate_trampoline_4() const { return ___delegate_trampoline_4; }
	inline intptr_t* get_address_of_delegate_trampoline_4() { return &___delegate_trampoline_4; }
	inline void set_delegate_trampoline_4(intptr_t value)
	{
		___delegate_trampoline_4 = value;
	}

	inline static int32_t get_offset_of_method_code_5() { return static_cast<int32_t>(offsetof(Delegate_t3022476291, ___method_code_5)); }
	inline intptr_t get_method_code_5() const { return ___method_code_5; }
	inline intptr_t* get_address_of_method_code_5() { return &___method_code_5; }
	inline void set_method_code_5(intptr_t value)
	{
		___method_code_5 = value;
	}

	inline static int32_t get_offset_of_method_info_6() { return static_cast<int32_t>(offsetof(Delegate_t3022476291, ___method_info_6)); }
	inline MethodInfo_t * get_method_info_6() const { return ___method_info_6; }
	inline MethodInfo_t ** get_address_of_method_info_6() { return &___method_info_6; }
	inline void set_method_info_6(MethodInfo_t * value)
	{
		___method_info_6 = value;
		Il2CppCodeGenWriteBarrier((&___method_info_6), value);
	}

	inline static int32_t get_offset_of_original_method_info_7() { return static_cast<int32_t>(offsetof(Delegate_t3022476291, ___original_method_info_7)); }
	inline MethodInfo_t * get_original_method_info_7() const { return ___original_method_info_7; }
	inline MethodInfo_t ** get_address_of_original_method_info_7() { return &___original_method_info_7; }
	inline void set_original_method_info_7(MethodInfo_t * value)
	{
		___original_method_info_7 = value;
		Il2CppCodeGenWriteBarrier((&___original_method_info_7), value);
	}

	inline static int32_t get_offset_of_data_8() { return static_cast<int32_t>(offsetof(Delegate_t3022476291, ___data_8)); }
	inline DelegateData_t1572802995 * get_data_8() const { return ___data_8; }
	inline DelegateData_t1572802995 ** get_address_of_data_8() { return &___data_8; }
	inline void set_data_8(DelegateData_t1572802995 * value)
	{
		___data_8 = value;
		Il2CppCodeGenWriteBarrier((&___data_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DELEGATE_T3022476291_H
#ifndef CORNER_T1077473318_H
#define CORNER_T1077473318_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.GridLayoutGroup/Corner
struct  Corner_t1077473318 
{
public:
	// System.Int32 UnityEngine.UI.GridLayoutGroup/Corner::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(Corner_t1077473318, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CORNER_T1077473318_H
#ifndef CUBISMUNMANAGEDBYTEARRAYVIEW_T2855206621_H
#define CUBISMUNMANAGEDBYTEARRAYVIEW_T2855206621_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Live2D.Cubism.Core.Unmanaged.CubismUnmanagedByteArrayView
struct  CubismUnmanagedByteArrayView_t2855206621  : public RuntimeObject
{
public:
	// System.Int32 Live2D.Cubism.Core.Unmanaged.CubismUnmanagedByteArrayView::<Length>k__BackingField
	int32_t ___U3CLengthU3Ek__BackingField_0;
	// System.IntPtr Live2D.Cubism.Core.Unmanaged.CubismUnmanagedByteArrayView::<Address>k__BackingField
	intptr_t ___U3CAddressU3Ek__BackingField_1;

public:
	inline static int32_t get_offset_of_U3CLengthU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(CubismUnmanagedByteArrayView_t2855206621, ___U3CLengthU3Ek__BackingField_0)); }
	inline int32_t get_U3CLengthU3Ek__BackingField_0() const { return ___U3CLengthU3Ek__BackingField_0; }
	inline int32_t* get_address_of_U3CLengthU3Ek__BackingField_0() { return &___U3CLengthU3Ek__BackingField_0; }
	inline void set_U3CLengthU3Ek__BackingField_0(int32_t value)
	{
		___U3CLengthU3Ek__BackingField_0 = value;
	}

	inline static int32_t get_offset_of_U3CAddressU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(CubismUnmanagedByteArrayView_t2855206621, ___U3CAddressU3Ek__BackingField_1)); }
	inline intptr_t get_U3CAddressU3Ek__BackingField_1() const { return ___U3CAddressU3Ek__BackingField_1; }
	inline intptr_t* get_address_of_U3CAddressU3Ek__BackingField_1() { return &___U3CAddressU3Ek__BackingField_1; }
	inline void set_U3CAddressU3Ek__BackingField_1(intptr_t value)
	{
		___U3CAddressU3Ek__BackingField_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CUBISMUNMANAGEDBYTEARRAYVIEW_T2855206621_H
#ifndef RAYCASTHIT2D_T4063908774_H
#define RAYCASTHIT2D_T4063908774_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.RaycastHit2D
struct  RaycastHit2D_t4063908774 
{
public:
	// UnityEngine.Vector2 UnityEngine.RaycastHit2D::m_Centroid
	Vector2_t2243707579  ___m_Centroid_0;
	// UnityEngine.Vector2 UnityEngine.RaycastHit2D::m_Point
	Vector2_t2243707579  ___m_Point_1;
	// UnityEngine.Vector2 UnityEngine.RaycastHit2D::m_Normal
	Vector2_t2243707579  ___m_Normal_2;
	// System.Single UnityEngine.RaycastHit2D::m_Distance
	float ___m_Distance_3;
	// System.Single UnityEngine.RaycastHit2D::m_Fraction
	float ___m_Fraction_4;
	// UnityEngine.Collider2D UnityEngine.RaycastHit2D::m_Collider
	Collider2D_t646061738 * ___m_Collider_5;

public:
	inline static int32_t get_offset_of_m_Centroid_0() { return static_cast<int32_t>(offsetof(RaycastHit2D_t4063908774, ___m_Centroid_0)); }
	inline Vector2_t2243707579  get_m_Centroid_0() const { return ___m_Centroid_0; }
	inline Vector2_t2243707579 * get_address_of_m_Centroid_0() { return &___m_Centroid_0; }
	inline void set_m_Centroid_0(Vector2_t2243707579  value)
	{
		___m_Centroid_0 = value;
	}

	inline static int32_t get_offset_of_m_Point_1() { return static_cast<int32_t>(offsetof(RaycastHit2D_t4063908774, ___m_Point_1)); }
	inline Vector2_t2243707579  get_m_Point_1() const { return ___m_Point_1; }
	inline Vector2_t2243707579 * get_address_of_m_Point_1() { return &___m_Point_1; }
	inline void set_m_Point_1(Vector2_t2243707579  value)
	{
		___m_Point_1 = value;
	}

	inline static int32_t get_offset_of_m_Normal_2() { return static_cast<int32_t>(offsetof(RaycastHit2D_t4063908774, ___m_Normal_2)); }
	inline Vector2_t2243707579  get_m_Normal_2() const { return ___m_Normal_2; }
	inline Vector2_t2243707579 * get_address_of_m_Normal_2() { return &___m_Normal_2; }
	inline void set_m_Normal_2(Vector2_t2243707579  value)
	{
		___m_Normal_2 = value;
	}

	inline static int32_t get_offset_of_m_Distance_3() { return static_cast<int32_t>(offsetof(RaycastHit2D_t4063908774, ___m_Distance_3)); }
	inline float get_m_Distance_3() const { return ___m_Distance_3; }
	inline float* get_address_of_m_Distance_3() { return &___m_Distance_3; }
	inline void set_m_Distance_3(float value)
	{
		___m_Distance_3 = value;
	}

	inline static int32_t get_offset_of_m_Fraction_4() { return static_cast<int32_t>(offsetof(RaycastHit2D_t4063908774, ___m_Fraction_4)); }
	inline float get_m_Fraction_4() const { return ___m_Fraction_4; }
	inline float* get_address_of_m_Fraction_4() { return &___m_Fraction_4; }
	inline void set_m_Fraction_4(float value)
	{
		___m_Fraction_4 = value;
	}

	inline static int32_t get_offset_of_m_Collider_5() { return static_cast<int32_t>(offsetof(RaycastHit2D_t4063908774, ___m_Collider_5)); }
	inline Collider2D_t646061738 * get_m_Collider_5() const { return ___m_Collider_5; }
	inline Collider2D_t646061738 ** get_address_of_m_Collider_5() { return &___m_Collider_5; }
	inline void set_m_Collider_5(Collider2D_t646061738 * value)
	{
		___m_Collider_5 = value;
		Il2CppCodeGenWriteBarrier((&___m_Collider_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.RaycastHit2D
struct RaycastHit2D_t4063908774_marshaled_pinvoke
{
	Vector2_t2243707579  ___m_Centroid_0;
	Vector2_t2243707579  ___m_Point_1;
	Vector2_t2243707579  ___m_Normal_2;
	float ___m_Distance_3;
	float ___m_Fraction_4;
	Collider2D_t646061738 * ___m_Collider_5;
};
// Native definition for COM marshalling of UnityEngine.RaycastHit2D
struct RaycastHit2D_t4063908774_marshaled_com
{
	Vector2_t2243707579  ___m_Centroid_0;
	Vector2_t2243707579  ___m_Point_1;
	Vector2_t2243707579  ___m_Normal_2;
	float ___m_Distance_3;
	float ___m_Fraction_4;
	Collider2D_t646061738 * ___m_Collider_5;
};
#endif // RAYCASTHIT2D_T4063908774_H
#ifndef RAYCASTHIT_T87180320_H
#define RAYCASTHIT_T87180320_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.RaycastHit
struct  RaycastHit_t87180320 
{
public:
	// UnityEngine.Vector3 UnityEngine.RaycastHit::m_Point
	Vector3_t2243707580  ___m_Point_0;
	// UnityEngine.Vector3 UnityEngine.RaycastHit::m_Normal
	Vector3_t2243707580  ___m_Normal_1;
	// System.Int32 UnityEngine.RaycastHit::m_FaceID
	int32_t ___m_FaceID_2;
	// System.Single UnityEngine.RaycastHit::m_Distance
	float ___m_Distance_3;
	// UnityEngine.Vector2 UnityEngine.RaycastHit::m_UV
	Vector2_t2243707579  ___m_UV_4;
	// UnityEngine.Collider UnityEngine.RaycastHit::m_Collider
	Collider_t3497673348 * ___m_Collider_5;

public:
	inline static int32_t get_offset_of_m_Point_0() { return static_cast<int32_t>(offsetof(RaycastHit_t87180320, ___m_Point_0)); }
	inline Vector3_t2243707580  get_m_Point_0() const { return ___m_Point_0; }
	inline Vector3_t2243707580 * get_address_of_m_Point_0() { return &___m_Point_0; }
	inline void set_m_Point_0(Vector3_t2243707580  value)
	{
		___m_Point_0 = value;
	}

	inline static int32_t get_offset_of_m_Normal_1() { return static_cast<int32_t>(offsetof(RaycastHit_t87180320, ___m_Normal_1)); }
	inline Vector3_t2243707580  get_m_Normal_1() const { return ___m_Normal_1; }
	inline Vector3_t2243707580 * get_address_of_m_Normal_1() { return &___m_Normal_1; }
	inline void set_m_Normal_1(Vector3_t2243707580  value)
	{
		___m_Normal_1 = value;
	}

	inline static int32_t get_offset_of_m_FaceID_2() { return static_cast<int32_t>(offsetof(RaycastHit_t87180320, ___m_FaceID_2)); }
	inline int32_t get_m_FaceID_2() const { return ___m_FaceID_2; }
	inline int32_t* get_address_of_m_FaceID_2() { return &___m_FaceID_2; }
	inline void set_m_FaceID_2(int32_t value)
	{
		___m_FaceID_2 = value;
	}

	inline static int32_t get_offset_of_m_Distance_3() { return static_cast<int32_t>(offsetof(RaycastHit_t87180320, ___m_Distance_3)); }
	inline float get_m_Distance_3() const { return ___m_Distance_3; }
	inline float* get_address_of_m_Distance_3() { return &___m_Distance_3; }
	inline void set_m_Distance_3(float value)
	{
		___m_Distance_3 = value;
	}

	inline static int32_t get_offset_of_m_UV_4() { return static_cast<int32_t>(offsetof(RaycastHit_t87180320, ___m_UV_4)); }
	inline Vector2_t2243707579  get_m_UV_4() const { return ___m_UV_4; }
	inline Vector2_t2243707579 * get_address_of_m_UV_4() { return &___m_UV_4; }
	inline void set_m_UV_4(Vector2_t2243707579  value)
	{
		___m_UV_4 = value;
	}

	inline static int32_t get_offset_of_m_Collider_5() { return static_cast<int32_t>(offsetof(RaycastHit_t87180320, ___m_Collider_5)); }
	inline Collider_t3497673348 * get_m_Collider_5() const { return ___m_Collider_5; }
	inline Collider_t3497673348 ** get_address_of_m_Collider_5() { return &___m_Collider_5; }
	inline void set_m_Collider_5(Collider_t3497673348 * value)
	{
		___m_Collider_5 = value;
		Il2CppCodeGenWriteBarrier((&___m_Collider_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.RaycastHit
struct RaycastHit_t87180320_marshaled_pinvoke
{
	Vector3_t2243707580  ___m_Point_0;
	Vector3_t2243707580  ___m_Normal_1;
	int32_t ___m_FaceID_2;
	float ___m_Distance_3;
	Vector2_t2243707579  ___m_UV_4;
	Collider_t3497673348 * ___m_Collider_5;
};
// Native definition for COM marshalling of UnityEngine.RaycastHit
struct RaycastHit_t87180320_marshaled_com
{
	Vector3_t2243707580  ___m_Point_0;
	Vector3_t2243707580  ___m_Normal_1;
	int32_t ___m_FaceID_2;
	float ___m_Distance_3;
	Vector2_t2243707579  ___m_UV_4;
	Collider_t3497673348 * ___m_Collider_5;
};
#endif // RAYCASTHIT_T87180320_H
#ifndef TRIGGER_T1068911718_H
#define TRIGGER_T1068911718_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Analytics.AnalyticsTracker/Trigger
struct  Trigger_t1068911718 
{
public:
	// System.Int32 UnityEngine.Analytics.AnalyticsTracker/Trigger::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(Trigger_t1068911718, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRIGGER_T1068911718_H
#ifndef RAY_T2469606224_H
#define RAY_T2469606224_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Ray
struct  Ray_t2469606224 
{
public:
	// UnityEngine.Vector3 UnityEngine.Ray::m_Origin
	Vector3_t2243707580  ___m_Origin_0;
	// UnityEngine.Vector3 UnityEngine.Ray::m_Direction
	Vector3_t2243707580  ___m_Direction_1;

public:
	inline static int32_t get_offset_of_m_Origin_0() { return static_cast<int32_t>(offsetof(Ray_t2469606224, ___m_Origin_0)); }
	inline Vector3_t2243707580  get_m_Origin_0() const { return ___m_Origin_0; }
	inline Vector3_t2243707580 * get_address_of_m_Origin_0() { return &___m_Origin_0; }
	inline void set_m_Origin_0(Vector3_t2243707580  value)
	{
		___m_Origin_0 = value;
	}

	inline static int32_t get_offset_of_m_Direction_1() { return static_cast<int32_t>(offsetof(Ray_t2469606224, ___m_Direction_1)); }
	inline Vector3_t2243707580  get_m_Direction_1() const { return ___m_Direction_1; }
	inline Vector3_t2243707580 * get_address_of_m_Direction_1() { return &___m_Direction_1; }
	inline void set_m_Direction_1(Vector3_t2243707580  value)
	{
		___m_Direction_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RAY_T2469606224_H
#ifndef VERTEXHELPER_T385374196_H
#define VERTEXHELPER_T385374196_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.VertexHelper
struct  VertexHelper_t385374196  : public RuntimeObject
{
public:
	// System.Collections.Generic.List`1<UnityEngine.Vector3> UnityEngine.UI.VertexHelper::m_Positions
	List_1_t1612828712 * ___m_Positions_0;
	// System.Collections.Generic.List`1<UnityEngine.Color32> UnityEngine.UI.VertexHelper::m_Colors
	List_1_t243638650 * ___m_Colors_1;
	// System.Collections.Generic.List`1<UnityEngine.Vector2> UnityEngine.UI.VertexHelper::m_Uv0S
	List_1_t1612828711 * ___m_Uv0S_2;
	// System.Collections.Generic.List`1<UnityEngine.Vector2> UnityEngine.UI.VertexHelper::m_Uv1S
	List_1_t1612828711 * ___m_Uv1S_3;
	// System.Collections.Generic.List`1<UnityEngine.Vector2> UnityEngine.UI.VertexHelper::m_Uv2S
	List_1_t1612828711 * ___m_Uv2S_4;
	// System.Collections.Generic.List`1<UnityEngine.Vector2> UnityEngine.UI.VertexHelper::m_Uv3S
	List_1_t1612828711 * ___m_Uv3S_5;
	// System.Collections.Generic.List`1<UnityEngine.Vector3> UnityEngine.UI.VertexHelper::m_Normals
	List_1_t1612828712 * ___m_Normals_6;
	// System.Collections.Generic.List`1<UnityEngine.Vector4> UnityEngine.UI.VertexHelper::m_Tangents
	List_1_t1612828713 * ___m_Tangents_7;
	// System.Collections.Generic.List`1<System.Int32> UnityEngine.UI.VertexHelper::m_Indices
	List_1_t1440998580 * ___m_Indices_8;

public:
	inline static int32_t get_offset_of_m_Positions_0() { return static_cast<int32_t>(offsetof(VertexHelper_t385374196, ___m_Positions_0)); }
	inline List_1_t1612828712 * get_m_Positions_0() const { return ___m_Positions_0; }
	inline List_1_t1612828712 ** get_address_of_m_Positions_0() { return &___m_Positions_0; }
	inline void set_m_Positions_0(List_1_t1612828712 * value)
	{
		___m_Positions_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_Positions_0), value);
	}

	inline static int32_t get_offset_of_m_Colors_1() { return static_cast<int32_t>(offsetof(VertexHelper_t385374196, ___m_Colors_1)); }
	inline List_1_t243638650 * get_m_Colors_1() const { return ___m_Colors_1; }
	inline List_1_t243638650 ** get_address_of_m_Colors_1() { return &___m_Colors_1; }
	inline void set_m_Colors_1(List_1_t243638650 * value)
	{
		___m_Colors_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_Colors_1), value);
	}

	inline static int32_t get_offset_of_m_Uv0S_2() { return static_cast<int32_t>(offsetof(VertexHelper_t385374196, ___m_Uv0S_2)); }
	inline List_1_t1612828711 * get_m_Uv0S_2() const { return ___m_Uv0S_2; }
	inline List_1_t1612828711 ** get_address_of_m_Uv0S_2() { return &___m_Uv0S_2; }
	inline void set_m_Uv0S_2(List_1_t1612828711 * value)
	{
		___m_Uv0S_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_Uv0S_2), value);
	}

	inline static int32_t get_offset_of_m_Uv1S_3() { return static_cast<int32_t>(offsetof(VertexHelper_t385374196, ___m_Uv1S_3)); }
	inline List_1_t1612828711 * get_m_Uv1S_3() const { return ___m_Uv1S_3; }
	inline List_1_t1612828711 ** get_address_of_m_Uv1S_3() { return &___m_Uv1S_3; }
	inline void set_m_Uv1S_3(List_1_t1612828711 * value)
	{
		___m_Uv1S_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_Uv1S_3), value);
	}

	inline static int32_t get_offset_of_m_Uv2S_4() { return static_cast<int32_t>(offsetof(VertexHelper_t385374196, ___m_Uv2S_4)); }
	inline List_1_t1612828711 * get_m_Uv2S_4() const { return ___m_Uv2S_4; }
	inline List_1_t1612828711 ** get_address_of_m_Uv2S_4() { return &___m_Uv2S_4; }
	inline void set_m_Uv2S_4(List_1_t1612828711 * value)
	{
		___m_Uv2S_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_Uv2S_4), value);
	}

	inline static int32_t get_offset_of_m_Uv3S_5() { return static_cast<int32_t>(offsetof(VertexHelper_t385374196, ___m_Uv3S_5)); }
	inline List_1_t1612828711 * get_m_Uv3S_5() const { return ___m_Uv3S_5; }
	inline List_1_t1612828711 ** get_address_of_m_Uv3S_5() { return &___m_Uv3S_5; }
	inline void set_m_Uv3S_5(List_1_t1612828711 * value)
	{
		___m_Uv3S_5 = value;
		Il2CppCodeGenWriteBarrier((&___m_Uv3S_5), value);
	}

	inline static int32_t get_offset_of_m_Normals_6() { return static_cast<int32_t>(offsetof(VertexHelper_t385374196, ___m_Normals_6)); }
	inline List_1_t1612828712 * get_m_Normals_6() const { return ___m_Normals_6; }
	inline List_1_t1612828712 ** get_address_of_m_Normals_6() { return &___m_Normals_6; }
	inline void set_m_Normals_6(List_1_t1612828712 * value)
	{
		___m_Normals_6 = value;
		Il2CppCodeGenWriteBarrier((&___m_Normals_6), value);
	}

	inline static int32_t get_offset_of_m_Tangents_7() { return static_cast<int32_t>(offsetof(VertexHelper_t385374196, ___m_Tangents_7)); }
	inline List_1_t1612828713 * get_m_Tangents_7() const { return ___m_Tangents_7; }
	inline List_1_t1612828713 ** get_address_of_m_Tangents_7() { return &___m_Tangents_7; }
	inline void set_m_Tangents_7(List_1_t1612828713 * value)
	{
		___m_Tangents_7 = value;
		Il2CppCodeGenWriteBarrier((&___m_Tangents_7), value);
	}

	inline static int32_t get_offset_of_m_Indices_8() { return static_cast<int32_t>(offsetof(VertexHelper_t385374196, ___m_Indices_8)); }
	inline List_1_t1440998580 * get_m_Indices_8() const { return ___m_Indices_8; }
	inline List_1_t1440998580 ** get_address_of_m_Indices_8() { return &___m_Indices_8; }
	inline void set_m_Indices_8(List_1_t1440998580 * value)
	{
		___m_Indices_8 = value;
		Il2CppCodeGenWriteBarrier((&___m_Indices_8), value);
	}
};

struct VertexHelper_t385374196_StaticFields
{
public:
	// UnityEngine.Vector4 UnityEngine.UI.VertexHelper::s_DefaultTangent
	Vector4_t2243707581  ___s_DefaultTangent_9;
	// UnityEngine.Vector3 UnityEngine.UI.VertexHelper::s_DefaultNormal
	Vector3_t2243707580  ___s_DefaultNormal_10;

public:
	inline static int32_t get_offset_of_s_DefaultTangent_9() { return static_cast<int32_t>(offsetof(VertexHelper_t385374196_StaticFields, ___s_DefaultTangent_9)); }
	inline Vector4_t2243707581  get_s_DefaultTangent_9() const { return ___s_DefaultTangent_9; }
	inline Vector4_t2243707581 * get_address_of_s_DefaultTangent_9() { return &___s_DefaultTangent_9; }
	inline void set_s_DefaultTangent_9(Vector4_t2243707581  value)
	{
		___s_DefaultTangent_9 = value;
	}

	inline static int32_t get_offset_of_s_DefaultNormal_10() { return static_cast<int32_t>(offsetof(VertexHelper_t385374196_StaticFields, ___s_DefaultNormal_10)); }
	inline Vector3_t2243707580  get_s_DefaultNormal_10() const { return ___s_DefaultNormal_10; }
	inline Vector3_t2243707580 * get_address_of_s_DefaultNormal_10() { return &___s_DefaultNormal_10; }
	inline void set_s_DefaultNormal_10(Vector3_t2243707580  value)
	{
		___s_DefaultNormal_10 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VERTEXHELPER_T385374196_H
#ifndef U3CPRIVATEIMPLEMENTATIONDETAILSU3E_T1486305141_H
#define U3CPRIVATEIMPLEMENTATIONDETAILSU3E_T1486305141_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>
struct  U3CPrivateImplementationDetailsU3E_t1486305141  : public RuntimeObject
{
public:

public:
};

struct U3CPrivateImplementationDetailsU3E_t1486305141_StaticFields
{
public:
	// <PrivateImplementationDetails>/$ArrayType=12 <PrivateImplementationDetails>::$field-7BBE37982E6C057ED87163CAFC7FD6E5E42EEA46
	U24ArrayTypeU3D12_t1568637717  ___U24fieldU2D7BBE37982E6C057ED87163CAFC7FD6E5E42EEA46_0;

public:
	inline static int32_t get_offset_of_U24fieldU2D7BBE37982E6C057ED87163CAFC7FD6E5E42EEA46_0() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t1486305141_StaticFields, ___U24fieldU2D7BBE37982E6C057ED87163CAFC7FD6E5E42EEA46_0)); }
	inline U24ArrayTypeU3D12_t1568637717  get_U24fieldU2D7BBE37982E6C057ED87163CAFC7FD6E5E42EEA46_0() const { return ___U24fieldU2D7BBE37982E6C057ED87163CAFC7FD6E5E42EEA46_0; }
	inline U24ArrayTypeU3D12_t1568637717 * get_address_of_U24fieldU2D7BBE37982E6C057ED87163CAFC7FD6E5E42EEA46_0() { return &___U24fieldU2D7BBE37982E6C057ED87163CAFC7FD6E5E42EEA46_0; }
	inline void set_U24fieldU2D7BBE37982E6C057ED87163CAFC7FD6E5E42EEA46_0(U24ArrayTypeU3D12_t1568637717  value)
	{
		___U24fieldU2D7BBE37982E6C057ED87163CAFC7FD6E5E42EEA46_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CPRIVATEIMPLEMENTATIONDETAILSU3E_T1486305141_H
#ifndef TEXTANCHOR_T112990806_H
#define TEXTANCHOR_T112990806_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.TextAnchor
struct  TextAnchor_t112990806 
{
public:
	// System.Int32 UnityEngine.TextAnchor::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(TextAnchor_t112990806, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TEXTANCHOR_T112990806_H
#ifndef ALLOCATIONITEM_T2929434092_H
#define ALLOCATIONITEM_T2929434092_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Live2D.Cubism.Core.Unmanaged.CubismUnmanagedMemory/AllocationItem
struct  AllocationItem_t2929434092 
{
public:
	// System.IntPtr Live2D.Cubism.Core.Unmanaged.CubismUnmanagedMemory/AllocationItem::UnalignedAddress
	intptr_t ___UnalignedAddress_0;
	// System.IntPtr Live2D.Cubism.Core.Unmanaged.CubismUnmanagedMemory/AllocationItem::AlignedAddress
	intptr_t ___AlignedAddress_1;

public:
	inline static int32_t get_offset_of_UnalignedAddress_0() { return static_cast<int32_t>(offsetof(AllocationItem_t2929434092, ___UnalignedAddress_0)); }
	inline intptr_t get_UnalignedAddress_0() const { return ___UnalignedAddress_0; }
	inline intptr_t* get_address_of_UnalignedAddress_0() { return &___UnalignedAddress_0; }
	inline void set_UnalignedAddress_0(intptr_t value)
	{
		___UnalignedAddress_0 = value;
	}

	inline static int32_t get_offset_of_AlignedAddress_1() { return static_cast<int32_t>(offsetof(AllocationItem_t2929434092, ___AlignedAddress_1)); }
	inline intptr_t get_AlignedAddress_1() const { return ___AlignedAddress_1; }
	inline intptr_t* get_address_of_AlignedAddress_1() { return &___AlignedAddress_1; }
	inline void set_AlignedAddress_1(intptr_t value)
	{
		___AlignedAddress_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ALLOCATIONITEM_T2929434092_H
#ifndef CUBISMUNMANAGEDFLOATARRAYVIEW_T1153227075_H
#define CUBISMUNMANAGEDFLOATARRAYVIEW_T1153227075_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Live2D.Cubism.Core.Unmanaged.CubismUnmanagedFloatArrayView
struct  CubismUnmanagedFloatArrayView_t1153227075  : public RuntimeObject
{
public:
	// System.Int32 Live2D.Cubism.Core.Unmanaged.CubismUnmanagedFloatArrayView::<Length>k__BackingField
	int32_t ___U3CLengthU3Ek__BackingField_0;
	// System.IntPtr Live2D.Cubism.Core.Unmanaged.CubismUnmanagedFloatArrayView::<Address>k__BackingField
	intptr_t ___U3CAddressU3Ek__BackingField_1;

public:
	inline static int32_t get_offset_of_U3CLengthU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(CubismUnmanagedFloatArrayView_t1153227075, ___U3CLengthU3Ek__BackingField_0)); }
	inline int32_t get_U3CLengthU3Ek__BackingField_0() const { return ___U3CLengthU3Ek__BackingField_0; }
	inline int32_t* get_address_of_U3CLengthU3Ek__BackingField_0() { return &___U3CLengthU3Ek__BackingField_0; }
	inline void set_U3CLengthU3Ek__BackingField_0(int32_t value)
	{
		___U3CLengthU3Ek__BackingField_0 = value;
	}

	inline static int32_t get_offset_of_U3CAddressU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(CubismUnmanagedFloatArrayView_t1153227075, ___U3CAddressU3Ek__BackingField_1)); }
	inline intptr_t get_U3CAddressU3Ek__BackingField_1() const { return ___U3CAddressU3Ek__BackingField_1; }
	inline intptr_t* get_address_of_U3CAddressU3Ek__BackingField_1() { return &___U3CAddressU3Ek__BackingField_1; }
	inline void set_U3CAddressU3Ek__BackingField_1(intptr_t value)
	{
		___U3CAddressU3Ek__BackingField_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CUBISMUNMANAGEDFLOATARRAYVIEW_T1153227075_H
#ifndef SPACE_T4278750806_H
#define SPACE_T4278750806_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Space
struct  Space_t4278750806 
{
public:
	// System.Int32 UnityEngine.Space::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(Space_t4278750806, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SPACE_T4278750806_H
#ifndef CUBISMUNMANAGEDDRAWABLES_T771654660_H
#define CUBISMUNMANAGEDDRAWABLES_T771654660_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Live2D.Cubism.Core.Unmanaged.CubismUnmanagedDrawables
struct  CubismUnmanagedDrawables_t771654660  : public RuntimeObject
{
public:
	// System.Int32 Live2D.Cubism.Core.Unmanaged.CubismUnmanagedDrawables::<Count>k__BackingField
	int32_t ___U3CCountU3Ek__BackingField_0;
	// System.String[] Live2D.Cubism.Core.Unmanaged.CubismUnmanagedDrawables::<Ids>k__BackingField
	StringU5BU5D_t1642385972* ___U3CIdsU3Ek__BackingField_1;
	// Live2D.Cubism.Core.Unmanaged.CubismUnmanagedByteArrayView Live2D.Cubism.Core.Unmanaged.CubismUnmanagedDrawables::<ConstantFlags>k__BackingField
	CubismUnmanagedByteArrayView_t2855206621 * ___U3CConstantFlagsU3Ek__BackingField_2;
	// Live2D.Cubism.Core.Unmanaged.CubismUnmanagedByteArrayView Live2D.Cubism.Core.Unmanaged.CubismUnmanagedDrawables::<DynamicFlags>k__BackingField
	CubismUnmanagedByteArrayView_t2855206621 * ___U3CDynamicFlagsU3Ek__BackingField_3;
	// Live2D.Cubism.Core.Unmanaged.CubismUnmanagedIntArrayView Live2D.Cubism.Core.Unmanaged.CubismUnmanagedDrawables::<TextureIndices>k__BackingField
	CubismUnmanagedIntArrayView_t965365490 * ___U3CTextureIndicesU3Ek__BackingField_4;
	// Live2D.Cubism.Core.Unmanaged.CubismUnmanagedIntArrayView Live2D.Cubism.Core.Unmanaged.CubismUnmanagedDrawables::<DrawOrders>k__BackingField
	CubismUnmanagedIntArrayView_t965365490 * ___U3CDrawOrdersU3Ek__BackingField_5;
	// Live2D.Cubism.Core.Unmanaged.CubismUnmanagedIntArrayView Live2D.Cubism.Core.Unmanaged.CubismUnmanagedDrawables::<RenderOrders>k__BackingField
	CubismUnmanagedIntArrayView_t965365490 * ___U3CRenderOrdersU3Ek__BackingField_6;
	// Live2D.Cubism.Core.Unmanaged.CubismUnmanagedFloatArrayView Live2D.Cubism.Core.Unmanaged.CubismUnmanagedDrawables::<Opacities>k__BackingField
	CubismUnmanagedFloatArrayView_t1153227075 * ___U3COpacitiesU3Ek__BackingField_7;
	// Live2D.Cubism.Core.Unmanaged.CubismUnmanagedIntArrayView Live2D.Cubism.Core.Unmanaged.CubismUnmanagedDrawables::<MaskCounts>k__BackingField
	CubismUnmanagedIntArrayView_t965365490 * ___U3CMaskCountsU3Ek__BackingField_8;
	// Live2D.Cubism.Core.Unmanaged.CubismUnmanagedIntArrayView[] Live2D.Cubism.Core.Unmanaged.CubismUnmanagedDrawables::<Masks>k__BackingField
	CubismUnmanagedIntArrayViewU5BU5D_t1335536903* ___U3CMasksU3Ek__BackingField_9;
	// Live2D.Cubism.Core.Unmanaged.CubismUnmanagedIntArrayView Live2D.Cubism.Core.Unmanaged.CubismUnmanagedDrawables::<VertexCounts>k__BackingField
	CubismUnmanagedIntArrayView_t965365490 * ___U3CVertexCountsU3Ek__BackingField_10;
	// Live2D.Cubism.Core.Unmanaged.CubismUnmanagedFloatArrayView[] Live2D.Cubism.Core.Unmanaged.CubismUnmanagedDrawables::<VertexPositions>k__BackingField
	CubismUnmanagedFloatArrayViewU5BU5D_t3463987794* ___U3CVertexPositionsU3Ek__BackingField_11;
	// Live2D.Cubism.Core.Unmanaged.CubismUnmanagedFloatArrayView[] Live2D.Cubism.Core.Unmanaged.CubismUnmanagedDrawables::<VertexUvs>k__BackingField
	CubismUnmanagedFloatArrayViewU5BU5D_t3463987794* ___U3CVertexUvsU3Ek__BackingField_12;
	// Live2D.Cubism.Core.Unmanaged.CubismUnmanagedIntArrayView Live2D.Cubism.Core.Unmanaged.CubismUnmanagedDrawables::<IndexCounts>k__BackingField
	CubismUnmanagedIntArrayView_t965365490 * ___U3CIndexCountsU3Ek__BackingField_13;
	// Live2D.Cubism.Core.Unmanaged.CubismUnmanagedUshortArrayView[] Live2D.Cubism.Core.Unmanaged.CubismUnmanagedDrawables::<Indices>k__BackingField
	CubismUnmanagedUshortArrayViewU5BU5D_t513752903* ___U3CIndicesU3Ek__BackingField_14;
	// System.IntPtr Live2D.Cubism.Core.Unmanaged.CubismUnmanagedDrawables::<ModelPtr>k__BackingField
	intptr_t ___U3CModelPtrU3Ek__BackingField_15;

public:
	inline static int32_t get_offset_of_U3CCountU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(CubismUnmanagedDrawables_t771654660, ___U3CCountU3Ek__BackingField_0)); }
	inline int32_t get_U3CCountU3Ek__BackingField_0() const { return ___U3CCountU3Ek__BackingField_0; }
	inline int32_t* get_address_of_U3CCountU3Ek__BackingField_0() { return &___U3CCountU3Ek__BackingField_0; }
	inline void set_U3CCountU3Ek__BackingField_0(int32_t value)
	{
		___U3CCountU3Ek__BackingField_0 = value;
	}

	inline static int32_t get_offset_of_U3CIdsU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(CubismUnmanagedDrawables_t771654660, ___U3CIdsU3Ek__BackingField_1)); }
	inline StringU5BU5D_t1642385972* get_U3CIdsU3Ek__BackingField_1() const { return ___U3CIdsU3Ek__BackingField_1; }
	inline StringU5BU5D_t1642385972** get_address_of_U3CIdsU3Ek__BackingField_1() { return &___U3CIdsU3Ek__BackingField_1; }
	inline void set_U3CIdsU3Ek__BackingField_1(StringU5BU5D_t1642385972* value)
	{
		___U3CIdsU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CIdsU3Ek__BackingField_1), value);
	}

	inline static int32_t get_offset_of_U3CConstantFlagsU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(CubismUnmanagedDrawables_t771654660, ___U3CConstantFlagsU3Ek__BackingField_2)); }
	inline CubismUnmanagedByteArrayView_t2855206621 * get_U3CConstantFlagsU3Ek__BackingField_2() const { return ___U3CConstantFlagsU3Ek__BackingField_2; }
	inline CubismUnmanagedByteArrayView_t2855206621 ** get_address_of_U3CConstantFlagsU3Ek__BackingField_2() { return &___U3CConstantFlagsU3Ek__BackingField_2; }
	inline void set_U3CConstantFlagsU3Ek__BackingField_2(CubismUnmanagedByteArrayView_t2855206621 * value)
	{
		___U3CConstantFlagsU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CConstantFlagsU3Ek__BackingField_2), value);
	}

	inline static int32_t get_offset_of_U3CDynamicFlagsU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(CubismUnmanagedDrawables_t771654660, ___U3CDynamicFlagsU3Ek__BackingField_3)); }
	inline CubismUnmanagedByteArrayView_t2855206621 * get_U3CDynamicFlagsU3Ek__BackingField_3() const { return ___U3CDynamicFlagsU3Ek__BackingField_3; }
	inline CubismUnmanagedByteArrayView_t2855206621 ** get_address_of_U3CDynamicFlagsU3Ek__BackingField_3() { return &___U3CDynamicFlagsU3Ek__BackingField_3; }
	inline void set_U3CDynamicFlagsU3Ek__BackingField_3(CubismUnmanagedByteArrayView_t2855206621 * value)
	{
		___U3CDynamicFlagsU3Ek__BackingField_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CDynamicFlagsU3Ek__BackingField_3), value);
	}

	inline static int32_t get_offset_of_U3CTextureIndicesU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(CubismUnmanagedDrawables_t771654660, ___U3CTextureIndicesU3Ek__BackingField_4)); }
	inline CubismUnmanagedIntArrayView_t965365490 * get_U3CTextureIndicesU3Ek__BackingField_4() const { return ___U3CTextureIndicesU3Ek__BackingField_4; }
	inline CubismUnmanagedIntArrayView_t965365490 ** get_address_of_U3CTextureIndicesU3Ek__BackingField_4() { return &___U3CTextureIndicesU3Ek__BackingField_4; }
	inline void set_U3CTextureIndicesU3Ek__BackingField_4(CubismUnmanagedIntArrayView_t965365490 * value)
	{
		___U3CTextureIndicesU3Ek__BackingField_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CTextureIndicesU3Ek__BackingField_4), value);
	}

	inline static int32_t get_offset_of_U3CDrawOrdersU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(CubismUnmanagedDrawables_t771654660, ___U3CDrawOrdersU3Ek__BackingField_5)); }
	inline CubismUnmanagedIntArrayView_t965365490 * get_U3CDrawOrdersU3Ek__BackingField_5() const { return ___U3CDrawOrdersU3Ek__BackingField_5; }
	inline CubismUnmanagedIntArrayView_t965365490 ** get_address_of_U3CDrawOrdersU3Ek__BackingField_5() { return &___U3CDrawOrdersU3Ek__BackingField_5; }
	inline void set_U3CDrawOrdersU3Ek__BackingField_5(CubismUnmanagedIntArrayView_t965365490 * value)
	{
		___U3CDrawOrdersU3Ek__BackingField_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3CDrawOrdersU3Ek__BackingField_5), value);
	}

	inline static int32_t get_offset_of_U3CRenderOrdersU3Ek__BackingField_6() { return static_cast<int32_t>(offsetof(CubismUnmanagedDrawables_t771654660, ___U3CRenderOrdersU3Ek__BackingField_6)); }
	inline CubismUnmanagedIntArrayView_t965365490 * get_U3CRenderOrdersU3Ek__BackingField_6() const { return ___U3CRenderOrdersU3Ek__BackingField_6; }
	inline CubismUnmanagedIntArrayView_t965365490 ** get_address_of_U3CRenderOrdersU3Ek__BackingField_6() { return &___U3CRenderOrdersU3Ek__BackingField_6; }
	inline void set_U3CRenderOrdersU3Ek__BackingField_6(CubismUnmanagedIntArrayView_t965365490 * value)
	{
		___U3CRenderOrdersU3Ek__BackingField_6 = value;
		Il2CppCodeGenWriteBarrier((&___U3CRenderOrdersU3Ek__BackingField_6), value);
	}

	inline static int32_t get_offset_of_U3COpacitiesU3Ek__BackingField_7() { return static_cast<int32_t>(offsetof(CubismUnmanagedDrawables_t771654660, ___U3COpacitiesU3Ek__BackingField_7)); }
	inline CubismUnmanagedFloatArrayView_t1153227075 * get_U3COpacitiesU3Ek__BackingField_7() const { return ___U3COpacitiesU3Ek__BackingField_7; }
	inline CubismUnmanagedFloatArrayView_t1153227075 ** get_address_of_U3COpacitiesU3Ek__BackingField_7() { return &___U3COpacitiesU3Ek__BackingField_7; }
	inline void set_U3COpacitiesU3Ek__BackingField_7(CubismUnmanagedFloatArrayView_t1153227075 * value)
	{
		___U3COpacitiesU3Ek__BackingField_7 = value;
		Il2CppCodeGenWriteBarrier((&___U3COpacitiesU3Ek__BackingField_7), value);
	}

	inline static int32_t get_offset_of_U3CMaskCountsU3Ek__BackingField_8() { return static_cast<int32_t>(offsetof(CubismUnmanagedDrawables_t771654660, ___U3CMaskCountsU3Ek__BackingField_8)); }
	inline CubismUnmanagedIntArrayView_t965365490 * get_U3CMaskCountsU3Ek__BackingField_8() const { return ___U3CMaskCountsU3Ek__BackingField_8; }
	inline CubismUnmanagedIntArrayView_t965365490 ** get_address_of_U3CMaskCountsU3Ek__BackingField_8() { return &___U3CMaskCountsU3Ek__BackingField_8; }
	inline void set_U3CMaskCountsU3Ek__BackingField_8(CubismUnmanagedIntArrayView_t965365490 * value)
	{
		___U3CMaskCountsU3Ek__BackingField_8 = value;
		Il2CppCodeGenWriteBarrier((&___U3CMaskCountsU3Ek__BackingField_8), value);
	}

	inline static int32_t get_offset_of_U3CMasksU3Ek__BackingField_9() { return static_cast<int32_t>(offsetof(CubismUnmanagedDrawables_t771654660, ___U3CMasksU3Ek__BackingField_9)); }
	inline CubismUnmanagedIntArrayViewU5BU5D_t1335536903* get_U3CMasksU3Ek__BackingField_9() const { return ___U3CMasksU3Ek__BackingField_9; }
	inline CubismUnmanagedIntArrayViewU5BU5D_t1335536903** get_address_of_U3CMasksU3Ek__BackingField_9() { return &___U3CMasksU3Ek__BackingField_9; }
	inline void set_U3CMasksU3Ek__BackingField_9(CubismUnmanagedIntArrayViewU5BU5D_t1335536903* value)
	{
		___U3CMasksU3Ek__BackingField_9 = value;
		Il2CppCodeGenWriteBarrier((&___U3CMasksU3Ek__BackingField_9), value);
	}

	inline static int32_t get_offset_of_U3CVertexCountsU3Ek__BackingField_10() { return static_cast<int32_t>(offsetof(CubismUnmanagedDrawables_t771654660, ___U3CVertexCountsU3Ek__BackingField_10)); }
	inline CubismUnmanagedIntArrayView_t965365490 * get_U3CVertexCountsU3Ek__BackingField_10() const { return ___U3CVertexCountsU3Ek__BackingField_10; }
	inline CubismUnmanagedIntArrayView_t965365490 ** get_address_of_U3CVertexCountsU3Ek__BackingField_10() { return &___U3CVertexCountsU3Ek__BackingField_10; }
	inline void set_U3CVertexCountsU3Ek__BackingField_10(CubismUnmanagedIntArrayView_t965365490 * value)
	{
		___U3CVertexCountsU3Ek__BackingField_10 = value;
		Il2CppCodeGenWriteBarrier((&___U3CVertexCountsU3Ek__BackingField_10), value);
	}

	inline static int32_t get_offset_of_U3CVertexPositionsU3Ek__BackingField_11() { return static_cast<int32_t>(offsetof(CubismUnmanagedDrawables_t771654660, ___U3CVertexPositionsU3Ek__BackingField_11)); }
	inline CubismUnmanagedFloatArrayViewU5BU5D_t3463987794* get_U3CVertexPositionsU3Ek__BackingField_11() const { return ___U3CVertexPositionsU3Ek__BackingField_11; }
	inline CubismUnmanagedFloatArrayViewU5BU5D_t3463987794** get_address_of_U3CVertexPositionsU3Ek__BackingField_11() { return &___U3CVertexPositionsU3Ek__BackingField_11; }
	inline void set_U3CVertexPositionsU3Ek__BackingField_11(CubismUnmanagedFloatArrayViewU5BU5D_t3463987794* value)
	{
		___U3CVertexPositionsU3Ek__BackingField_11 = value;
		Il2CppCodeGenWriteBarrier((&___U3CVertexPositionsU3Ek__BackingField_11), value);
	}

	inline static int32_t get_offset_of_U3CVertexUvsU3Ek__BackingField_12() { return static_cast<int32_t>(offsetof(CubismUnmanagedDrawables_t771654660, ___U3CVertexUvsU3Ek__BackingField_12)); }
	inline CubismUnmanagedFloatArrayViewU5BU5D_t3463987794* get_U3CVertexUvsU3Ek__BackingField_12() const { return ___U3CVertexUvsU3Ek__BackingField_12; }
	inline CubismUnmanagedFloatArrayViewU5BU5D_t3463987794** get_address_of_U3CVertexUvsU3Ek__BackingField_12() { return &___U3CVertexUvsU3Ek__BackingField_12; }
	inline void set_U3CVertexUvsU3Ek__BackingField_12(CubismUnmanagedFloatArrayViewU5BU5D_t3463987794* value)
	{
		___U3CVertexUvsU3Ek__BackingField_12 = value;
		Il2CppCodeGenWriteBarrier((&___U3CVertexUvsU3Ek__BackingField_12), value);
	}

	inline static int32_t get_offset_of_U3CIndexCountsU3Ek__BackingField_13() { return static_cast<int32_t>(offsetof(CubismUnmanagedDrawables_t771654660, ___U3CIndexCountsU3Ek__BackingField_13)); }
	inline CubismUnmanagedIntArrayView_t965365490 * get_U3CIndexCountsU3Ek__BackingField_13() const { return ___U3CIndexCountsU3Ek__BackingField_13; }
	inline CubismUnmanagedIntArrayView_t965365490 ** get_address_of_U3CIndexCountsU3Ek__BackingField_13() { return &___U3CIndexCountsU3Ek__BackingField_13; }
	inline void set_U3CIndexCountsU3Ek__BackingField_13(CubismUnmanagedIntArrayView_t965365490 * value)
	{
		___U3CIndexCountsU3Ek__BackingField_13 = value;
		Il2CppCodeGenWriteBarrier((&___U3CIndexCountsU3Ek__BackingField_13), value);
	}

	inline static int32_t get_offset_of_U3CIndicesU3Ek__BackingField_14() { return static_cast<int32_t>(offsetof(CubismUnmanagedDrawables_t771654660, ___U3CIndicesU3Ek__BackingField_14)); }
	inline CubismUnmanagedUshortArrayViewU5BU5D_t513752903* get_U3CIndicesU3Ek__BackingField_14() const { return ___U3CIndicesU3Ek__BackingField_14; }
	inline CubismUnmanagedUshortArrayViewU5BU5D_t513752903** get_address_of_U3CIndicesU3Ek__BackingField_14() { return &___U3CIndicesU3Ek__BackingField_14; }
	inline void set_U3CIndicesU3Ek__BackingField_14(CubismUnmanagedUshortArrayViewU5BU5D_t513752903* value)
	{
		___U3CIndicesU3Ek__BackingField_14 = value;
		Il2CppCodeGenWriteBarrier((&___U3CIndicesU3Ek__BackingField_14), value);
	}

	inline static int32_t get_offset_of_U3CModelPtrU3Ek__BackingField_15() { return static_cast<int32_t>(offsetof(CubismUnmanagedDrawables_t771654660, ___U3CModelPtrU3Ek__BackingField_15)); }
	inline intptr_t get_U3CModelPtrU3Ek__BackingField_15() const { return ___U3CModelPtrU3Ek__BackingField_15; }
	inline intptr_t* get_address_of_U3CModelPtrU3Ek__BackingField_15() { return &___U3CModelPtrU3Ek__BackingField_15; }
	inline void set_U3CModelPtrU3Ek__BackingField_15(intptr_t value)
	{
		___U3CModelPtrU3Ek__BackingField_15 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CUBISMUNMANAGEDDRAWABLES_T771654660_H
#ifndef NAMEDVALUECOLOR_T2874784184_H
#define NAMEDVALUECOLOR_T2874784184_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// iTween/NamedValueColor
struct  NamedValueColor_t2874784184 
{
public:
	// System.Int32 iTween/NamedValueColor::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(NamedValueColor_t2874784184, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NAMEDVALUECOLOR_T2874784184_H
#ifndef LOOPTYPE_T1490651981_H
#define LOOPTYPE_T1490651981_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// iTween/LoopType
struct  LoopType_t1490651981 
{
public:
	// System.Int32 iTween/LoopType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(LoopType_t1490651981, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LOOPTYPE_T1490651981_H
#ifndef EASETYPE_T818674011_H
#define EASETYPE_T818674011_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// iTween/EaseType
struct  EaseType_t818674011 
{
public:
	// System.Int32 iTween/EaseType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(EaseType_t818674011, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EASETYPE_T818674011_H
#ifndef MULTICASTDELEGATE_T3201952435_H
#define MULTICASTDELEGATE_T3201952435_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.MulticastDelegate
struct  MulticastDelegate_t3201952435  : public Delegate_t3022476291
{
public:
	// System.MulticastDelegate System.MulticastDelegate::prev
	MulticastDelegate_t3201952435 * ___prev_9;
	// System.MulticastDelegate System.MulticastDelegate::kpm_next
	MulticastDelegate_t3201952435 * ___kpm_next_10;

public:
	inline static int32_t get_offset_of_prev_9() { return static_cast<int32_t>(offsetof(MulticastDelegate_t3201952435, ___prev_9)); }
	inline MulticastDelegate_t3201952435 * get_prev_9() const { return ___prev_9; }
	inline MulticastDelegate_t3201952435 ** get_address_of_prev_9() { return &___prev_9; }
	inline void set_prev_9(MulticastDelegate_t3201952435 * value)
	{
		___prev_9 = value;
		Il2CppCodeGenWriteBarrier((&___prev_9), value);
	}

	inline static int32_t get_offset_of_kpm_next_10() { return static_cast<int32_t>(offsetof(MulticastDelegate_t3201952435, ___kpm_next_10)); }
	inline MulticastDelegate_t3201952435 * get_kpm_next_10() const { return ___kpm_next_10; }
	inline MulticastDelegate_t3201952435 ** get_address_of_kpm_next_10() { return &___kpm_next_10; }
	inline void set_kpm_next_10(MulticastDelegate_t3201952435 * value)
	{
		___kpm_next_10 = value;
		Il2CppCodeGenWriteBarrier((&___kpm_next_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MULTICASTDELEGATE_T3201952435_H
#ifndef SCRIPTABLEOBJECT_T1975622470_H
#define SCRIPTABLEOBJECT_T1975622470_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.ScriptableObject
struct  ScriptableObject_t1975622470  : public Object_t1021602117
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.ScriptableObject
struct ScriptableObject_t1975622470_marshaled_pinvoke : public Object_t1021602117_marshaled_pinvoke
{
};
// Native definition for COM marshalling of UnityEngine.ScriptableObject
struct ScriptableObject_t1975622470_marshaled_com : public Object_t1021602117_marshaled_com
{
};
#endif // SCRIPTABLEOBJECT_T1975622470_H
#ifndef CUBISMTASKABLEMODEL_T1216042951_H
#define CUBISMTASKABLEMODEL_T1216042951_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Live2D.Cubism.Core.CubismTaskableModel
struct  CubismTaskableModel_t1216042951  : public RuntimeObject
{
public:
	// Live2D.Cubism.Core.Unmanaged.CubismUnmanagedModel Live2D.Cubism.Core.CubismTaskableModel::<UnmanagedModel>k__BackingField
	CubismUnmanagedModel_t586274666 * ___U3CUnmanagedModelU3Ek__BackingField_0;
	// Live2D.Cubism.Core.CubismMoc Live2D.Cubism.Core.CubismTaskableModel::<Moc>k__BackingField
	CubismMoc_t1358225636 * ___U3CMocU3Ek__BackingField_1;
	// Live2D.Cubism.Core.CubismDynamicDrawableData[] Live2D.Cubism.Core.CubismTaskableModel::_dynamicDrawableData
	CubismDynamicDrawableDataU5BU5D_t2021340059* ____dynamicDrawableData_2;
	// System.Boolean Live2D.Cubism.Core.CubismTaskableModel::<ShouldReleaseUnmanaged>k__BackingField
	bool ___U3CShouldReleaseUnmanagedU3Ek__BackingField_3;
	// System.Object Live2D.Cubism.Core.CubismTaskableModel::<Lock>k__BackingField
	RuntimeObject * ___U3CLockU3Ek__BackingField_4;
	// Live2D.Cubism.Core.CubismTaskableModel/TaskState Live2D.Cubism.Core.CubismTaskableModel::<State>k__BackingField
	int32_t ___U3CStateU3Ek__BackingField_5;

public:
	inline static int32_t get_offset_of_U3CUnmanagedModelU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(CubismTaskableModel_t1216042951, ___U3CUnmanagedModelU3Ek__BackingField_0)); }
	inline CubismUnmanagedModel_t586274666 * get_U3CUnmanagedModelU3Ek__BackingField_0() const { return ___U3CUnmanagedModelU3Ek__BackingField_0; }
	inline CubismUnmanagedModel_t586274666 ** get_address_of_U3CUnmanagedModelU3Ek__BackingField_0() { return &___U3CUnmanagedModelU3Ek__BackingField_0; }
	inline void set_U3CUnmanagedModelU3Ek__BackingField_0(CubismUnmanagedModel_t586274666 * value)
	{
		___U3CUnmanagedModelU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CUnmanagedModelU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_U3CMocU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(CubismTaskableModel_t1216042951, ___U3CMocU3Ek__BackingField_1)); }
	inline CubismMoc_t1358225636 * get_U3CMocU3Ek__BackingField_1() const { return ___U3CMocU3Ek__BackingField_1; }
	inline CubismMoc_t1358225636 ** get_address_of_U3CMocU3Ek__BackingField_1() { return &___U3CMocU3Ek__BackingField_1; }
	inline void set_U3CMocU3Ek__BackingField_1(CubismMoc_t1358225636 * value)
	{
		___U3CMocU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CMocU3Ek__BackingField_1), value);
	}

	inline static int32_t get_offset_of__dynamicDrawableData_2() { return static_cast<int32_t>(offsetof(CubismTaskableModel_t1216042951, ____dynamicDrawableData_2)); }
	inline CubismDynamicDrawableDataU5BU5D_t2021340059* get__dynamicDrawableData_2() const { return ____dynamicDrawableData_2; }
	inline CubismDynamicDrawableDataU5BU5D_t2021340059** get_address_of__dynamicDrawableData_2() { return &____dynamicDrawableData_2; }
	inline void set__dynamicDrawableData_2(CubismDynamicDrawableDataU5BU5D_t2021340059* value)
	{
		____dynamicDrawableData_2 = value;
		Il2CppCodeGenWriteBarrier((&____dynamicDrawableData_2), value);
	}

	inline static int32_t get_offset_of_U3CShouldReleaseUnmanagedU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(CubismTaskableModel_t1216042951, ___U3CShouldReleaseUnmanagedU3Ek__BackingField_3)); }
	inline bool get_U3CShouldReleaseUnmanagedU3Ek__BackingField_3() const { return ___U3CShouldReleaseUnmanagedU3Ek__BackingField_3; }
	inline bool* get_address_of_U3CShouldReleaseUnmanagedU3Ek__BackingField_3() { return &___U3CShouldReleaseUnmanagedU3Ek__BackingField_3; }
	inline void set_U3CShouldReleaseUnmanagedU3Ek__BackingField_3(bool value)
	{
		___U3CShouldReleaseUnmanagedU3Ek__BackingField_3 = value;
	}

	inline static int32_t get_offset_of_U3CLockU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(CubismTaskableModel_t1216042951, ___U3CLockU3Ek__BackingField_4)); }
	inline RuntimeObject * get_U3CLockU3Ek__BackingField_4() const { return ___U3CLockU3Ek__BackingField_4; }
	inline RuntimeObject ** get_address_of_U3CLockU3Ek__BackingField_4() { return &___U3CLockU3Ek__BackingField_4; }
	inline void set_U3CLockU3Ek__BackingField_4(RuntimeObject * value)
	{
		___U3CLockU3Ek__BackingField_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CLockU3Ek__BackingField_4), value);
	}

	inline static int32_t get_offset_of_U3CStateU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(CubismTaskableModel_t1216042951, ___U3CStateU3Ek__BackingField_5)); }
	inline int32_t get_U3CStateU3Ek__BackingField_5() const { return ___U3CStateU3Ek__BackingField_5; }
	inline int32_t* get_address_of_U3CStateU3Ek__BackingField_5() { return &___U3CStateU3Ek__BackingField_5; }
	inline void set_U3CStateU3Ek__BackingField_5(int32_t value)
	{
		___U3CStateU3Ek__BackingField_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CUBISMTASKABLEMODEL_T1216042951_H
#ifndef COMPONENT_T3819376471_H
#define COMPONENT_T3819376471_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Component
struct  Component_t3819376471  : public Object_t1021602117
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPONENT_T3819376471_H
#ifndef DEFAULTS_T4204852305_H
#define DEFAULTS_T4204852305_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// iTween/Defaults
struct  Defaults_t4204852305  : public RuntimeObject
{
public:

public:
};

struct Defaults_t4204852305_StaticFields
{
public:
	// System.Single iTween/Defaults::time
	float ___time_0;
	// System.Single iTween/Defaults::delay
	float ___delay_1;
	// iTween/NamedValueColor iTween/Defaults::namedColorValue
	int32_t ___namedColorValue_2;
	// iTween/LoopType iTween/Defaults::loopType
	int32_t ___loopType_3;
	// iTween/EaseType iTween/Defaults::easeType
	int32_t ___easeType_4;
	// System.Single iTween/Defaults::lookSpeed
	float ___lookSpeed_5;
	// System.Boolean iTween/Defaults::isLocal
	bool ___isLocal_6;
	// UnityEngine.Space iTween/Defaults::space
	int32_t ___space_7;
	// System.Boolean iTween/Defaults::orientToPath
	bool ___orientToPath_8;
	// UnityEngine.Color iTween/Defaults::color
	Color_t2020392075  ___color_9;
	// System.Single iTween/Defaults::updateTimePercentage
	float ___updateTimePercentage_10;
	// System.Single iTween/Defaults::updateTime
	float ___updateTime_11;
	// System.Int32 iTween/Defaults::cameraFadeDepth
	int32_t ___cameraFadeDepth_12;
	// System.Single iTween/Defaults::lookAhead
	float ___lookAhead_13;
	// System.Boolean iTween/Defaults::useRealTime
	bool ___useRealTime_14;
	// UnityEngine.Vector3 iTween/Defaults::up
	Vector3_t2243707580  ___up_15;

public:
	inline static int32_t get_offset_of_time_0() { return static_cast<int32_t>(offsetof(Defaults_t4204852305_StaticFields, ___time_0)); }
	inline float get_time_0() const { return ___time_0; }
	inline float* get_address_of_time_0() { return &___time_0; }
	inline void set_time_0(float value)
	{
		___time_0 = value;
	}

	inline static int32_t get_offset_of_delay_1() { return static_cast<int32_t>(offsetof(Defaults_t4204852305_StaticFields, ___delay_1)); }
	inline float get_delay_1() const { return ___delay_1; }
	inline float* get_address_of_delay_1() { return &___delay_1; }
	inline void set_delay_1(float value)
	{
		___delay_1 = value;
	}

	inline static int32_t get_offset_of_namedColorValue_2() { return static_cast<int32_t>(offsetof(Defaults_t4204852305_StaticFields, ___namedColorValue_2)); }
	inline int32_t get_namedColorValue_2() const { return ___namedColorValue_2; }
	inline int32_t* get_address_of_namedColorValue_2() { return &___namedColorValue_2; }
	inline void set_namedColorValue_2(int32_t value)
	{
		___namedColorValue_2 = value;
	}

	inline static int32_t get_offset_of_loopType_3() { return static_cast<int32_t>(offsetof(Defaults_t4204852305_StaticFields, ___loopType_3)); }
	inline int32_t get_loopType_3() const { return ___loopType_3; }
	inline int32_t* get_address_of_loopType_3() { return &___loopType_3; }
	inline void set_loopType_3(int32_t value)
	{
		___loopType_3 = value;
	}

	inline static int32_t get_offset_of_easeType_4() { return static_cast<int32_t>(offsetof(Defaults_t4204852305_StaticFields, ___easeType_4)); }
	inline int32_t get_easeType_4() const { return ___easeType_4; }
	inline int32_t* get_address_of_easeType_4() { return &___easeType_4; }
	inline void set_easeType_4(int32_t value)
	{
		___easeType_4 = value;
	}

	inline static int32_t get_offset_of_lookSpeed_5() { return static_cast<int32_t>(offsetof(Defaults_t4204852305_StaticFields, ___lookSpeed_5)); }
	inline float get_lookSpeed_5() const { return ___lookSpeed_5; }
	inline float* get_address_of_lookSpeed_5() { return &___lookSpeed_5; }
	inline void set_lookSpeed_5(float value)
	{
		___lookSpeed_5 = value;
	}

	inline static int32_t get_offset_of_isLocal_6() { return static_cast<int32_t>(offsetof(Defaults_t4204852305_StaticFields, ___isLocal_6)); }
	inline bool get_isLocal_6() const { return ___isLocal_6; }
	inline bool* get_address_of_isLocal_6() { return &___isLocal_6; }
	inline void set_isLocal_6(bool value)
	{
		___isLocal_6 = value;
	}

	inline static int32_t get_offset_of_space_7() { return static_cast<int32_t>(offsetof(Defaults_t4204852305_StaticFields, ___space_7)); }
	inline int32_t get_space_7() const { return ___space_7; }
	inline int32_t* get_address_of_space_7() { return &___space_7; }
	inline void set_space_7(int32_t value)
	{
		___space_7 = value;
	}

	inline static int32_t get_offset_of_orientToPath_8() { return static_cast<int32_t>(offsetof(Defaults_t4204852305_StaticFields, ___orientToPath_8)); }
	inline bool get_orientToPath_8() const { return ___orientToPath_8; }
	inline bool* get_address_of_orientToPath_8() { return &___orientToPath_8; }
	inline void set_orientToPath_8(bool value)
	{
		___orientToPath_8 = value;
	}

	inline static int32_t get_offset_of_color_9() { return static_cast<int32_t>(offsetof(Defaults_t4204852305_StaticFields, ___color_9)); }
	inline Color_t2020392075  get_color_9() const { return ___color_9; }
	inline Color_t2020392075 * get_address_of_color_9() { return &___color_9; }
	inline void set_color_9(Color_t2020392075  value)
	{
		___color_9 = value;
	}

	inline static int32_t get_offset_of_updateTimePercentage_10() { return static_cast<int32_t>(offsetof(Defaults_t4204852305_StaticFields, ___updateTimePercentage_10)); }
	inline float get_updateTimePercentage_10() const { return ___updateTimePercentage_10; }
	inline float* get_address_of_updateTimePercentage_10() { return &___updateTimePercentage_10; }
	inline void set_updateTimePercentage_10(float value)
	{
		___updateTimePercentage_10 = value;
	}

	inline static int32_t get_offset_of_updateTime_11() { return static_cast<int32_t>(offsetof(Defaults_t4204852305_StaticFields, ___updateTime_11)); }
	inline float get_updateTime_11() const { return ___updateTime_11; }
	inline float* get_address_of_updateTime_11() { return &___updateTime_11; }
	inline void set_updateTime_11(float value)
	{
		___updateTime_11 = value;
	}

	inline static int32_t get_offset_of_cameraFadeDepth_12() { return static_cast<int32_t>(offsetof(Defaults_t4204852305_StaticFields, ___cameraFadeDepth_12)); }
	inline int32_t get_cameraFadeDepth_12() const { return ___cameraFadeDepth_12; }
	inline int32_t* get_address_of_cameraFadeDepth_12() { return &___cameraFadeDepth_12; }
	inline void set_cameraFadeDepth_12(int32_t value)
	{
		___cameraFadeDepth_12 = value;
	}

	inline static int32_t get_offset_of_lookAhead_13() { return static_cast<int32_t>(offsetof(Defaults_t4204852305_StaticFields, ___lookAhead_13)); }
	inline float get_lookAhead_13() const { return ___lookAhead_13; }
	inline float* get_address_of_lookAhead_13() { return &___lookAhead_13; }
	inline void set_lookAhead_13(float value)
	{
		___lookAhead_13 = value;
	}

	inline static int32_t get_offset_of_useRealTime_14() { return static_cast<int32_t>(offsetof(Defaults_t4204852305_StaticFields, ___useRealTime_14)); }
	inline bool get_useRealTime_14() const { return ___useRealTime_14; }
	inline bool* get_address_of_useRealTime_14() { return &___useRealTime_14; }
	inline void set_useRealTime_14(bool value)
	{
		___useRealTime_14 = value;
	}

	inline static int32_t get_offset_of_up_15() { return static_cast<int32_t>(offsetof(Defaults_t4204852305_StaticFields, ___up_15)); }
	inline Vector3_t2243707580  get_up_15() const { return ___up_15; }
	inline Vector3_t2243707580 * get_address_of_up_15() { return &___up_15; }
	inline void set_up_15(Vector3_t2243707580  value)
	{
		___up_15 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DEFAULTS_T4204852305_H
#ifndef RAYCASTALLCALLBACK_T3435657708_H
#define RAYCASTALLCALLBACK_T3435657708_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.ReflectionMethodsCache/RaycastAllCallback
struct  RaycastAllCallback_t3435657708  : public MulticastDelegate_t3201952435
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RAYCASTALLCALLBACK_T3435657708_H
#ifndef GETRAYINTERSECTIONALLCALLBACK_T2213949596_H
#define GETRAYINTERSECTIONALLCALLBACK_T2213949596_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.ReflectionMethodsCache/GetRayIntersectionAllCallback
struct  GetRayIntersectionAllCallback_t2213949596  : public MulticastDelegate_t3201952435
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GETRAYINTERSECTIONALLCALLBACK_T2213949596_H
#ifndef APPLYTWEEN_T747394300_H
#define APPLYTWEEN_T747394300_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// iTween/ApplyTween
struct  ApplyTween_t747394300  : public MulticastDelegate_t3201952435
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // APPLYTWEEN_T747394300_H
#ifndef EASINGFUNCTION_T3676968174_H
#define EASINGFUNCTION_T3676968174_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// iTween/EasingFunction
struct  EasingFunction_t3676968174  : public MulticastDelegate_t3201952435
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EASINGFUNCTION_T3676968174_H
#ifndef GETRAYINTERSECTIONALLNONALLOCCALLBACK_T3246763936_H
#define GETRAYINTERSECTIONALLNONALLOCCALLBACK_T3246763936_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.ReflectionMethodsCache/GetRayIntersectionAllNonAllocCallback
struct  GetRayIntersectionAllNonAllocCallback_t3246763936  : public MulticastDelegate_t3201952435
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GETRAYINTERSECTIONALLNONALLOCCALLBACK_T3246763936_H
#ifndef RAYCAST2DCALLBACK_T2260664863_H
#define RAYCAST2DCALLBACK_T2260664863_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.ReflectionMethodsCache/Raycast2DCallback
struct  Raycast2DCallback_t2260664863  : public MulticastDelegate_t3201952435
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RAYCAST2DCALLBACK_T2260664863_H
#ifndef BEHAVIOUR_T955675639_H
#define BEHAVIOUR_T955675639_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Behaviour
struct  Behaviour_t955675639  : public Component_t3819376471
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BEHAVIOUR_T955675639_H
#ifndef DYNAMICDRAWABLEDATAHANDLER_T2657212100_H
#define DYNAMICDRAWABLEDATAHANDLER_T2657212100_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Live2D.Cubism.Core.CubismModel/DynamicDrawableDataHandler
struct  DynamicDrawableDataHandler_t2657212100  : public MulticastDelegate_t3201952435
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DYNAMICDRAWABLEDATAHANDLER_T2657212100_H
#ifndef CUBISMTASKHANDLER_T1959870214_H
#define CUBISMTASKHANDLER_T1959870214_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Live2D.Cubism.Core.CubismTaskQueue/CubismTaskHandler
struct  CubismTaskHandler_t1959870214  : public MulticastDelegate_t3201952435
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CUBISMTASKHANDLER_T1959870214_H
#ifndef CUBISMMOC_T1358225636_H
#define CUBISMMOC_T1358225636_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Live2D.Cubism.Core.CubismMoc
struct  CubismMoc_t1358225636  : public ScriptableObject_t1975622470
{
public:
	// System.Byte[] Live2D.Cubism.Core.CubismMoc::_bytes
	ByteU5BU5D_t3397334013* ____bytes_2;
	// Live2D.Cubism.Core.Unmanaged.CubismUnmanagedMoc Live2D.Cubism.Core.CubismMoc::<UnmanagedMoc>k__BackingField
	CubismUnmanagedMoc_t2103361662 * ___U3CUnmanagedMocU3Ek__BackingField_3;
	// System.Int32 Live2D.Cubism.Core.CubismMoc::<ReferenceCount>k__BackingField
	int32_t ___U3CReferenceCountU3Ek__BackingField_4;

public:
	inline static int32_t get_offset_of__bytes_2() { return static_cast<int32_t>(offsetof(CubismMoc_t1358225636, ____bytes_2)); }
	inline ByteU5BU5D_t3397334013* get__bytes_2() const { return ____bytes_2; }
	inline ByteU5BU5D_t3397334013** get_address_of__bytes_2() { return &____bytes_2; }
	inline void set__bytes_2(ByteU5BU5D_t3397334013* value)
	{
		____bytes_2 = value;
		Il2CppCodeGenWriteBarrier((&____bytes_2), value);
	}

	inline static int32_t get_offset_of_U3CUnmanagedMocU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(CubismMoc_t1358225636, ___U3CUnmanagedMocU3Ek__BackingField_3)); }
	inline CubismUnmanagedMoc_t2103361662 * get_U3CUnmanagedMocU3Ek__BackingField_3() const { return ___U3CUnmanagedMocU3Ek__BackingField_3; }
	inline CubismUnmanagedMoc_t2103361662 ** get_address_of_U3CUnmanagedMocU3Ek__BackingField_3() { return &___U3CUnmanagedMocU3Ek__BackingField_3; }
	inline void set_U3CUnmanagedMocU3Ek__BackingField_3(CubismUnmanagedMoc_t2103361662 * value)
	{
		___U3CUnmanagedMocU3Ek__BackingField_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CUnmanagedMocU3Ek__BackingField_3), value);
	}

	inline static int32_t get_offset_of_U3CReferenceCountU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(CubismMoc_t1358225636, ___U3CReferenceCountU3Ek__BackingField_4)); }
	inline int32_t get_U3CReferenceCountU3Ek__BackingField_4() const { return ___U3CReferenceCountU3Ek__BackingField_4; }
	inline int32_t* get_address_of_U3CReferenceCountU3Ek__BackingField_4() { return &___U3CReferenceCountU3Ek__BackingField_4; }
	inline void set_U3CReferenceCountU3Ek__BackingField_4(int32_t value)
	{
		___U3CReferenceCountU3Ek__BackingField_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CUBISMMOC_T1358225636_H
#ifndef UNMANAGEDLOGDELEGATE_T742859164_H
#define UNMANAGEDLOGDELEGATE_T742859164_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Live2D.Cubism.Core.CubismLogging/UnmanagedLogDelegate
struct  UnmanagedLogDelegate_t742859164  : public MulticastDelegate_t3201952435
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNMANAGEDLOGDELEGATE_T742859164_H
#ifndef RAYCAST3DCALLBACK_T3928470916_H
#define RAYCAST3DCALLBACK_T3928470916_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.ReflectionMethodsCache/Raycast3DCallback
struct  Raycast3DCallback_t3928470916  : public MulticastDelegate_t3201952435
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RAYCAST3DCALLBACK_T3928470916_H
#ifndef GETRAYCASTNONALLOCCALLBACK_T1074830945_H
#define GETRAYCASTNONALLOCCALLBACK_T1074830945_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.ReflectionMethodsCache/GetRaycastNonAllocCallback
struct  GetRaycastNonAllocCallback_t1074830945  : public MulticastDelegate_t3201952435
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GETRAYCASTNONALLOCCALLBACK_T1074830945_H
#ifndef MONOBEHAVIOUR_T1158329972_H
#define MONOBEHAVIOUR_T1158329972_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.MonoBehaviour
struct  MonoBehaviour_t1158329972  : public Behaviour_t955675639
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MONOBEHAVIOUR_T1158329972_H
#ifndef SAMPLEINFO_T4001993458_H
#define SAMPLEINFO_T4001993458_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SampleInfo
struct  SampleInfo_t4001993458  : public MonoBehaviour_t1158329972
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SAMPLEINFO_T4001993458_H
#ifndef CUBISMPART_T3829010958_H
#define CUBISMPART_T3829010958_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Live2D.Cubism.Core.CubismPart
struct  CubismPart_t3829010958  : public MonoBehaviour_t1158329972
{
public:
	// Live2D.Cubism.Core.Unmanaged.CubismUnmanagedParts Live2D.Cubism.Core.CubismPart::<UnmanagedParts>k__BackingField
	CubismUnmanagedParts_t1124036275 * ___U3CUnmanagedPartsU3Ek__BackingField_2;
	// System.Int32 Live2D.Cubism.Core.CubismPart::_unmanagedIndex
	int32_t ____unmanagedIndex_3;
	// System.Single Live2D.Cubism.Core.CubismPart::Opacity
	float ___Opacity_4;

public:
	inline static int32_t get_offset_of_U3CUnmanagedPartsU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(CubismPart_t3829010958, ___U3CUnmanagedPartsU3Ek__BackingField_2)); }
	inline CubismUnmanagedParts_t1124036275 * get_U3CUnmanagedPartsU3Ek__BackingField_2() const { return ___U3CUnmanagedPartsU3Ek__BackingField_2; }
	inline CubismUnmanagedParts_t1124036275 ** get_address_of_U3CUnmanagedPartsU3Ek__BackingField_2() { return &___U3CUnmanagedPartsU3Ek__BackingField_2; }
	inline void set_U3CUnmanagedPartsU3Ek__BackingField_2(CubismUnmanagedParts_t1124036275 * value)
	{
		___U3CUnmanagedPartsU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CUnmanagedPartsU3Ek__BackingField_2), value);
	}

	inline static int32_t get_offset_of__unmanagedIndex_3() { return static_cast<int32_t>(offsetof(CubismPart_t3829010958, ____unmanagedIndex_3)); }
	inline int32_t get__unmanagedIndex_3() const { return ____unmanagedIndex_3; }
	inline int32_t* get_address_of__unmanagedIndex_3() { return &____unmanagedIndex_3; }
	inline void set__unmanagedIndex_3(int32_t value)
	{
		____unmanagedIndex_3 = value;
	}

	inline static int32_t get_offset_of_Opacity_4() { return static_cast<int32_t>(offsetof(CubismPart_t3829010958, ___Opacity_4)); }
	inline float get_Opacity_4() const { return ___Opacity_4; }
	inline float* get_address_of_Opacity_4() { return &___Opacity_4; }
	inline void set_Opacity_4(float value)
	{
		___Opacity_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CUBISMPART_T3829010958_H
#ifndef CHILDTESTSCRIPT_T4144785889_H
#define CHILDTESTSCRIPT_T4144785889_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ChildTestScript
struct  ChildTestScript_t4144785889  : public MonoBehaviour_t1158329972
{
public:
	// System.Boolean ChildTestScript::show
	bool ___show_2;

public:
	inline static int32_t get_offset_of_show_2() { return static_cast<int32_t>(offsetof(ChildTestScript_t4144785889, ___show_2)); }
	inline bool get_show_2() const { return ___show_2; }
	inline bool* get_address_of_show_2() { return &___show_2; }
	inline void set_show_2(bool value)
	{
		___show_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CHILDTESTSCRIPT_T4144785889_H
#ifndef CUBISMMODEL_T2381008000_H
#define CUBISMMODEL_T2381008000_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Live2D.Cubism.Core.CubismModel
struct  CubismModel_t2381008000  : public MonoBehaviour_t1158329972
{
public:
	// Live2D.Cubism.Core.CubismModel/DynamicDrawableDataHandler Live2D.Cubism.Core.CubismModel::OnDynamicDrawableData
	DynamicDrawableDataHandler_t2657212100 * ___OnDynamicDrawableData_2;
	// Live2D.Cubism.Core.CubismMoc Live2D.Cubism.Core.CubismModel::_moc
	CubismMoc_t1358225636 * ____moc_3;
	// Live2D.Cubism.Core.CubismTaskableModel Live2D.Cubism.Core.CubismModel::<TaskableModel>k__BackingField
	CubismTaskableModel_t1216042951 * ___U3CTaskableModelU3Ek__BackingField_4;
	// Live2D.Cubism.Core.CubismParameter[] Live2D.Cubism.Core.CubismModel::_parameters
	CubismParameterU5BU5D_t2848401775* ____parameters_5;
	// Live2D.Cubism.Core.CubismPart[] Live2D.Cubism.Core.CubismModel::_parts
	CubismPartU5BU5D_t650530299* ____parts_6;
	// Live2D.Cubism.Core.CubismDrawable[] Live2D.Cubism.Core.CubismModel::_drawables
	CubismDrawableU5BU5D_t2169581932* ____drawables_7;
	// System.Boolean Live2D.Cubism.Core.CubismModel::<WasJustEnabled>k__BackingField
	bool ___U3CWasJustEnabledU3Ek__BackingField_8;
	// System.Int32 Live2D.Cubism.Core.CubismModel::<LastTick>k__BackingField
	int32_t ___U3CLastTickU3Ek__BackingField_9;

public:
	inline static int32_t get_offset_of_OnDynamicDrawableData_2() { return static_cast<int32_t>(offsetof(CubismModel_t2381008000, ___OnDynamicDrawableData_2)); }
	inline DynamicDrawableDataHandler_t2657212100 * get_OnDynamicDrawableData_2() const { return ___OnDynamicDrawableData_2; }
	inline DynamicDrawableDataHandler_t2657212100 ** get_address_of_OnDynamicDrawableData_2() { return &___OnDynamicDrawableData_2; }
	inline void set_OnDynamicDrawableData_2(DynamicDrawableDataHandler_t2657212100 * value)
	{
		___OnDynamicDrawableData_2 = value;
		Il2CppCodeGenWriteBarrier((&___OnDynamicDrawableData_2), value);
	}

	inline static int32_t get_offset_of__moc_3() { return static_cast<int32_t>(offsetof(CubismModel_t2381008000, ____moc_3)); }
	inline CubismMoc_t1358225636 * get__moc_3() const { return ____moc_3; }
	inline CubismMoc_t1358225636 ** get_address_of__moc_3() { return &____moc_3; }
	inline void set__moc_3(CubismMoc_t1358225636 * value)
	{
		____moc_3 = value;
		Il2CppCodeGenWriteBarrier((&____moc_3), value);
	}

	inline static int32_t get_offset_of_U3CTaskableModelU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(CubismModel_t2381008000, ___U3CTaskableModelU3Ek__BackingField_4)); }
	inline CubismTaskableModel_t1216042951 * get_U3CTaskableModelU3Ek__BackingField_4() const { return ___U3CTaskableModelU3Ek__BackingField_4; }
	inline CubismTaskableModel_t1216042951 ** get_address_of_U3CTaskableModelU3Ek__BackingField_4() { return &___U3CTaskableModelU3Ek__BackingField_4; }
	inline void set_U3CTaskableModelU3Ek__BackingField_4(CubismTaskableModel_t1216042951 * value)
	{
		___U3CTaskableModelU3Ek__BackingField_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CTaskableModelU3Ek__BackingField_4), value);
	}

	inline static int32_t get_offset_of__parameters_5() { return static_cast<int32_t>(offsetof(CubismModel_t2381008000, ____parameters_5)); }
	inline CubismParameterU5BU5D_t2848401775* get__parameters_5() const { return ____parameters_5; }
	inline CubismParameterU5BU5D_t2848401775** get_address_of__parameters_5() { return &____parameters_5; }
	inline void set__parameters_5(CubismParameterU5BU5D_t2848401775* value)
	{
		____parameters_5 = value;
		Il2CppCodeGenWriteBarrier((&____parameters_5), value);
	}

	inline static int32_t get_offset_of__parts_6() { return static_cast<int32_t>(offsetof(CubismModel_t2381008000, ____parts_6)); }
	inline CubismPartU5BU5D_t650530299* get__parts_6() const { return ____parts_6; }
	inline CubismPartU5BU5D_t650530299** get_address_of__parts_6() { return &____parts_6; }
	inline void set__parts_6(CubismPartU5BU5D_t650530299* value)
	{
		____parts_6 = value;
		Il2CppCodeGenWriteBarrier((&____parts_6), value);
	}

	inline static int32_t get_offset_of__drawables_7() { return static_cast<int32_t>(offsetof(CubismModel_t2381008000, ____drawables_7)); }
	inline CubismDrawableU5BU5D_t2169581932* get__drawables_7() const { return ____drawables_7; }
	inline CubismDrawableU5BU5D_t2169581932** get_address_of__drawables_7() { return &____drawables_7; }
	inline void set__drawables_7(CubismDrawableU5BU5D_t2169581932* value)
	{
		____drawables_7 = value;
		Il2CppCodeGenWriteBarrier((&____drawables_7), value);
	}

	inline static int32_t get_offset_of_U3CWasJustEnabledU3Ek__BackingField_8() { return static_cast<int32_t>(offsetof(CubismModel_t2381008000, ___U3CWasJustEnabledU3Ek__BackingField_8)); }
	inline bool get_U3CWasJustEnabledU3Ek__BackingField_8() const { return ___U3CWasJustEnabledU3Ek__BackingField_8; }
	inline bool* get_address_of_U3CWasJustEnabledU3Ek__BackingField_8() { return &___U3CWasJustEnabledU3Ek__BackingField_8; }
	inline void set_U3CWasJustEnabledU3Ek__BackingField_8(bool value)
	{
		___U3CWasJustEnabledU3Ek__BackingField_8 = value;
	}

	inline static int32_t get_offset_of_U3CLastTickU3Ek__BackingField_9() { return static_cast<int32_t>(offsetof(CubismModel_t2381008000, ___U3CLastTickU3Ek__BackingField_9)); }
	inline int32_t get_U3CLastTickU3Ek__BackingField_9() const { return ___U3CLastTickU3Ek__BackingField_9; }
	inline int32_t* get_address_of_U3CLastTickU3Ek__BackingField_9() { return &___U3CLastTickU3Ek__BackingField_9; }
	inline void set_U3CLastTickU3Ek__BackingField_9(int32_t value)
	{
		___U3CLastTickU3Ek__BackingField_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CUBISMMODEL_T2381008000_H
#ifndef CUBISMPARAMETER_T3864677546_H
#define CUBISMPARAMETER_T3864677546_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Live2D.Cubism.Core.CubismParameter
struct  CubismParameter_t3864677546  : public MonoBehaviour_t1158329972
{
public:
	// Live2D.Cubism.Core.Unmanaged.CubismUnmanagedParameters Live2D.Cubism.Core.CubismParameter::<UnmanagedParameters>k__BackingField
	CubismUnmanagedParameters_t2764188193 * ___U3CUnmanagedParametersU3Ek__BackingField_2;
	// System.Int32 Live2D.Cubism.Core.CubismParameter::_unmanagedIndex
	int32_t ____unmanagedIndex_3;
	// System.Single Live2D.Cubism.Core.CubismParameter::Value
	float ___Value_4;

public:
	inline static int32_t get_offset_of_U3CUnmanagedParametersU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(CubismParameter_t3864677546, ___U3CUnmanagedParametersU3Ek__BackingField_2)); }
	inline CubismUnmanagedParameters_t2764188193 * get_U3CUnmanagedParametersU3Ek__BackingField_2() const { return ___U3CUnmanagedParametersU3Ek__BackingField_2; }
	inline CubismUnmanagedParameters_t2764188193 ** get_address_of_U3CUnmanagedParametersU3Ek__BackingField_2() { return &___U3CUnmanagedParametersU3Ek__BackingField_2; }
	inline void set_U3CUnmanagedParametersU3Ek__BackingField_2(CubismUnmanagedParameters_t2764188193 * value)
	{
		___U3CUnmanagedParametersU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CUnmanagedParametersU3Ek__BackingField_2), value);
	}

	inline static int32_t get_offset_of__unmanagedIndex_3() { return static_cast<int32_t>(offsetof(CubismParameter_t3864677546, ____unmanagedIndex_3)); }
	inline int32_t get__unmanagedIndex_3() const { return ____unmanagedIndex_3; }
	inline int32_t* get_address_of__unmanagedIndex_3() { return &____unmanagedIndex_3; }
	inline void set__unmanagedIndex_3(int32_t value)
	{
		____unmanagedIndex_3 = value;
	}

	inline static int32_t get_offset_of_Value_4() { return static_cast<int32_t>(offsetof(CubismParameter_t3864677546, ___Value_4)); }
	inline float get_Value_4() const { return ___Value_4; }
	inline float* get_address_of_Value_4() { return &___Value_4; }
	inline void set_Value_4(float value)
	{
		___Value_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CUBISMPARAMETER_T3864677546_H
#ifndef CUBISMDRAWABLE_T2709211313_H
#define CUBISMDRAWABLE_T2709211313_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Live2D.Cubism.Core.CubismDrawable
struct  CubismDrawable_t2709211313  : public MonoBehaviour_t1158329972
{
public:
	// Live2D.Cubism.Core.Unmanaged.CubismUnmanagedDrawables Live2D.Cubism.Core.CubismDrawable::<UnmanagedDrawables>k__BackingField
	CubismUnmanagedDrawables_t771654660 * ___U3CUnmanagedDrawablesU3Ek__BackingField_2;
	// System.Int32 Live2D.Cubism.Core.CubismDrawable::_unmanagedIndex
	int32_t ____unmanagedIndex_3;

public:
	inline static int32_t get_offset_of_U3CUnmanagedDrawablesU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(CubismDrawable_t2709211313, ___U3CUnmanagedDrawablesU3Ek__BackingField_2)); }
	inline CubismUnmanagedDrawables_t771654660 * get_U3CUnmanagedDrawablesU3Ek__BackingField_2() const { return ___U3CUnmanagedDrawablesU3Ek__BackingField_2; }
	inline CubismUnmanagedDrawables_t771654660 ** get_address_of_U3CUnmanagedDrawablesU3Ek__BackingField_2() { return &___U3CUnmanagedDrawablesU3Ek__BackingField_2; }
	inline void set_U3CUnmanagedDrawablesU3Ek__BackingField_2(CubismUnmanagedDrawables_t771654660 * value)
	{
		___U3CUnmanagedDrawablesU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CUnmanagedDrawablesU3Ek__BackingField_2), value);
	}

	inline static int32_t get_offset_of__unmanagedIndex_3() { return static_cast<int32_t>(offsetof(CubismDrawable_t2709211313, ____unmanagedIndex_3)); }
	inline int32_t get__unmanagedIndex_3() const { return ____unmanagedIndex_3; }
	inline int32_t* get_address_of__unmanagedIndex_3() { return &____unmanagedIndex_3; }
	inline void set__unmanagedIndex_3(int32_t value)
	{
		____unmanagedIndex_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CUBISMDRAWABLE_T2709211313_H
#ifndef ROTATESAMPLE_T3367094513_H
#define ROTATESAMPLE_T3367094513_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// RotateSample
struct  RotateSample_t3367094513  : public MonoBehaviour_t1158329972
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ROTATESAMPLE_T3367094513_H
#ifndef MOVESAMPLE_T4188468541_H
#define MOVESAMPLE_T4188468541_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MoveSample
struct  MoveSample_t4188468541  : public MonoBehaviour_t1158329972
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MOVESAMPLE_T4188468541_H
#ifndef ITWEEN_T488923914_H
#define ITWEEN_T488923914_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// iTween
struct  iTween_t488923914  : public MonoBehaviour_t1158329972
{
public:
	// System.String iTween::id
	String_t* ___id_4;
	// System.String iTween::type
	String_t* ___type_5;
	// System.String iTween::method
	String_t* ___method_6;
	// iTween/EaseType iTween::easeType
	int32_t ___easeType_7;
	// System.Single iTween::time
	float ___time_8;
	// System.Single iTween::delay
	float ___delay_9;
	// iTween/LoopType iTween::loopType
	int32_t ___loopType_10;
	// System.Boolean iTween::isRunning
	bool ___isRunning_11;
	// System.Boolean iTween::isPaused
	bool ___isPaused_12;
	// System.String iTween::_name
	String_t* ____name_13;
	// System.Single iTween::runningTime
	float ___runningTime_14;
	// System.Single iTween::percentage
	float ___percentage_15;
	// System.Single iTween::delayStarted
	float ___delayStarted_16;
	// System.Boolean iTween::kinematic
	bool ___kinematic_17;
	// System.Boolean iTween::isLocal
	bool ___isLocal_18;
	// System.Boolean iTween::loop
	bool ___loop_19;
	// System.Boolean iTween::reverse
	bool ___reverse_20;
	// System.Boolean iTween::wasPaused
	bool ___wasPaused_21;
	// System.Boolean iTween::physics
	bool ___physics_22;
	// System.Collections.Hashtable iTween::tweenArguments
	Hashtable_t909839986 * ___tweenArguments_23;
	// UnityEngine.Space iTween::space
	int32_t ___space_24;
	// iTween/EasingFunction iTween::ease
	EasingFunction_t3676968174 * ___ease_25;
	// iTween/ApplyTween iTween::apply
	ApplyTween_t747394300 * ___apply_26;
	// UnityEngine.AudioSource iTween::audioSource
	AudioSource_t1135106623 * ___audioSource_27;
	// UnityEngine.Vector3[] iTween::vector3s
	Vector3U5BU5D_t1172311765* ___vector3s_28;
	// UnityEngine.Vector2[] iTween::vector2s
	Vector2U5BU5D_t686124026* ___vector2s_29;
	// UnityEngine.Color[0...,0...] iTween::colors
	ColorU5B0___U2C0___U5D_t672350443* ___colors_30;
	// System.Single[] iTween::floats
	SingleU5BU5D_t577127397* ___floats_31;
	// UnityEngine.Rect[] iTween::rects
	RectU5BU5D_t1299715887* ___rects_32;
	// iTween/CRSpline iTween::path
	CRSpline_t4177960625 * ___path_33;
	// UnityEngine.Vector3 iTween::preUpdate
	Vector3_t2243707580  ___preUpdate_34;
	// UnityEngine.Vector3 iTween::postUpdate
	Vector3_t2243707580  ___postUpdate_35;
	// iTween/NamedValueColor iTween::namedcolorvalue
	int32_t ___namedcolorvalue_36;
	// System.Single iTween::lastRealTime
	float ___lastRealTime_37;
	// System.Boolean iTween::useRealTime
	bool ___useRealTime_38;
	// UnityEngine.Transform iTween::thisTransform
	Transform_t3275118058 * ___thisTransform_39;

public:
	inline static int32_t get_offset_of_id_4() { return static_cast<int32_t>(offsetof(iTween_t488923914, ___id_4)); }
	inline String_t* get_id_4() const { return ___id_4; }
	inline String_t** get_address_of_id_4() { return &___id_4; }
	inline void set_id_4(String_t* value)
	{
		___id_4 = value;
		Il2CppCodeGenWriteBarrier((&___id_4), value);
	}

	inline static int32_t get_offset_of_type_5() { return static_cast<int32_t>(offsetof(iTween_t488923914, ___type_5)); }
	inline String_t* get_type_5() const { return ___type_5; }
	inline String_t** get_address_of_type_5() { return &___type_5; }
	inline void set_type_5(String_t* value)
	{
		___type_5 = value;
		Il2CppCodeGenWriteBarrier((&___type_5), value);
	}

	inline static int32_t get_offset_of_method_6() { return static_cast<int32_t>(offsetof(iTween_t488923914, ___method_6)); }
	inline String_t* get_method_6() const { return ___method_6; }
	inline String_t** get_address_of_method_6() { return &___method_6; }
	inline void set_method_6(String_t* value)
	{
		___method_6 = value;
		Il2CppCodeGenWriteBarrier((&___method_6), value);
	}

	inline static int32_t get_offset_of_easeType_7() { return static_cast<int32_t>(offsetof(iTween_t488923914, ___easeType_7)); }
	inline int32_t get_easeType_7() const { return ___easeType_7; }
	inline int32_t* get_address_of_easeType_7() { return &___easeType_7; }
	inline void set_easeType_7(int32_t value)
	{
		___easeType_7 = value;
	}

	inline static int32_t get_offset_of_time_8() { return static_cast<int32_t>(offsetof(iTween_t488923914, ___time_8)); }
	inline float get_time_8() const { return ___time_8; }
	inline float* get_address_of_time_8() { return &___time_8; }
	inline void set_time_8(float value)
	{
		___time_8 = value;
	}

	inline static int32_t get_offset_of_delay_9() { return static_cast<int32_t>(offsetof(iTween_t488923914, ___delay_9)); }
	inline float get_delay_9() const { return ___delay_9; }
	inline float* get_address_of_delay_9() { return &___delay_9; }
	inline void set_delay_9(float value)
	{
		___delay_9 = value;
	}

	inline static int32_t get_offset_of_loopType_10() { return static_cast<int32_t>(offsetof(iTween_t488923914, ___loopType_10)); }
	inline int32_t get_loopType_10() const { return ___loopType_10; }
	inline int32_t* get_address_of_loopType_10() { return &___loopType_10; }
	inline void set_loopType_10(int32_t value)
	{
		___loopType_10 = value;
	}

	inline static int32_t get_offset_of_isRunning_11() { return static_cast<int32_t>(offsetof(iTween_t488923914, ___isRunning_11)); }
	inline bool get_isRunning_11() const { return ___isRunning_11; }
	inline bool* get_address_of_isRunning_11() { return &___isRunning_11; }
	inline void set_isRunning_11(bool value)
	{
		___isRunning_11 = value;
	}

	inline static int32_t get_offset_of_isPaused_12() { return static_cast<int32_t>(offsetof(iTween_t488923914, ___isPaused_12)); }
	inline bool get_isPaused_12() const { return ___isPaused_12; }
	inline bool* get_address_of_isPaused_12() { return &___isPaused_12; }
	inline void set_isPaused_12(bool value)
	{
		___isPaused_12 = value;
	}

	inline static int32_t get_offset_of__name_13() { return static_cast<int32_t>(offsetof(iTween_t488923914, ____name_13)); }
	inline String_t* get__name_13() const { return ____name_13; }
	inline String_t** get_address_of__name_13() { return &____name_13; }
	inline void set__name_13(String_t* value)
	{
		____name_13 = value;
		Il2CppCodeGenWriteBarrier((&____name_13), value);
	}

	inline static int32_t get_offset_of_runningTime_14() { return static_cast<int32_t>(offsetof(iTween_t488923914, ___runningTime_14)); }
	inline float get_runningTime_14() const { return ___runningTime_14; }
	inline float* get_address_of_runningTime_14() { return &___runningTime_14; }
	inline void set_runningTime_14(float value)
	{
		___runningTime_14 = value;
	}

	inline static int32_t get_offset_of_percentage_15() { return static_cast<int32_t>(offsetof(iTween_t488923914, ___percentage_15)); }
	inline float get_percentage_15() const { return ___percentage_15; }
	inline float* get_address_of_percentage_15() { return &___percentage_15; }
	inline void set_percentage_15(float value)
	{
		___percentage_15 = value;
	}

	inline static int32_t get_offset_of_delayStarted_16() { return static_cast<int32_t>(offsetof(iTween_t488923914, ___delayStarted_16)); }
	inline float get_delayStarted_16() const { return ___delayStarted_16; }
	inline float* get_address_of_delayStarted_16() { return &___delayStarted_16; }
	inline void set_delayStarted_16(float value)
	{
		___delayStarted_16 = value;
	}

	inline static int32_t get_offset_of_kinematic_17() { return static_cast<int32_t>(offsetof(iTween_t488923914, ___kinematic_17)); }
	inline bool get_kinematic_17() const { return ___kinematic_17; }
	inline bool* get_address_of_kinematic_17() { return &___kinematic_17; }
	inline void set_kinematic_17(bool value)
	{
		___kinematic_17 = value;
	}

	inline static int32_t get_offset_of_isLocal_18() { return static_cast<int32_t>(offsetof(iTween_t488923914, ___isLocal_18)); }
	inline bool get_isLocal_18() const { return ___isLocal_18; }
	inline bool* get_address_of_isLocal_18() { return &___isLocal_18; }
	inline void set_isLocal_18(bool value)
	{
		___isLocal_18 = value;
	}

	inline static int32_t get_offset_of_loop_19() { return static_cast<int32_t>(offsetof(iTween_t488923914, ___loop_19)); }
	inline bool get_loop_19() const { return ___loop_19; }
	inline bool* get_address_of_loop_19() { return &___loop_19; }
	inline void set_loop_19(bool value)
	{
		___loop_19 = value;
	}

	inline static int32_t get_offset_of_reverse_20() { return static_cast<int32_t>(offsetof(iTween_t488923914, ___reverse_20)); }
	inline bool get_reverse_20() const { return ___reverse_20; }
	inline bool* get_address_of_reverse_20() { return &___reverse_20; }
	inline void set_reverse_20(bool value)
	{
		___reverse_20 = value;
	}

	inline static int32_t get_offset_of_wasPaused_21() { return static_cast<int32_t>(offsetof(iTween_t488923914, ___wasPaused_21)); }
	inline bool get_wasPaused_21() const { return ___wasPaused_21; }
	inline bool* get_address_of_wasPaused_21() { return &___wasPaused_21; }
	inline void set_wasPaused_21(bool value)
	{
		___wasPaused_21 = value;
	}

	inline static int32_t get_offset_of_physics_22() { return static_cast<int32_t>(offsetof(iTween_t488923914, ___physics_22)); }
	inline bool get_physics_22() const { return ___physics_22; }
	inline bool* get_address_of_physics_22() { return &___physics_22; }
	inline void set_physics_22(bool value)
	{
		___physics_22 = value;
	}

	inline static int32_t get_offset_of_tweenArguments_23() { return static_cast<int32_t>(offsetof(iTween_t488923914, ___tweenArguments_23)); }
	inline Hashtable_t909839986 * get_tweenArguments_23() const { return ___tweenArguments_23; }
	inline Hashtable_t909839986 ** get_address_of_tweenArguments_23() { return &___tweenArguments_23; }
	inline void set_tweenArguments_23(Hashtable_t909839986 * value)
	{
		___tweenArguments_23 = value;
		Il2CppCodeGenWriteBarrier((&___tweenArguments_23), value);
	}

	inline static int32_t get_offset_of_space_24() { return static_cast<int32_t>(offsetof(iTween_t488923914, ___space_24)); }
	inline int32_t get_space_24() const { return ___space_24; }
	inline int32_t* get_address_of_space_24() { return &___space_24; }
	inline void set_space_24(int32_t value)
	{
		___space_24 = value;
	}

	inline static int32_t get_offset_of_ease_25() { return static_cast<int32_t>(offsetof(iTween_t488923914, ___ease_25)); }
	inline EasingFunction_t3676968174 * get_ease_25() const { return ___ease_25; }
	inline EasingFunction_t3676968174 ** get_address_of_ease_25() { return &___ease_25; }
	inline void set_ease_25(EasingFunction_t3676968174 * value)
	{
		___ease_25 = value;
		Il2CppCodeGenWriteBarrier((&___ease_25), value);
	}

	inline static int32_t get_offset_of_apply_26() { return static_cast<int32_t>(offsetof(iTween_t488923914, ___apply_26)); }
	inline ApplyTween_t747394300 * get_apply_26() const { return ___apply_26; }
	inline ApplyTween_t747394300 ** get_address_of_apply_26() { return &___apply_26; }
	inline void set_apply_26(ApplyTween_t747394300 * value)
	{
		___apply_26 = value;
		Il2CppCodeGenWriteBarrier((&___apply_26), value);
	}

	inline static int32_t get_offset_of_audioSource_27() { return static_cast<int32_t>(offsetof(iTween_t488923914, ___audioSource_27)); }
	inline AudioSource_t1135106623 * get_audioSource_27() const { return ___audioSource_27; }
	inline AudioSource_t1135106623 ** get_address_of_audioSource_27() { return &___audioSource_27; }
	inline void set_audioSource_27(AudioSource_t1135106623 * value)
	{
		___audioSource_27 = value;
		Il2CppCodeGenWriteBarrier((&___audioSource_27), value);
	}

	inline static int32_t get_offset_of_vector3s_28() { return static_cast<int32_t>(offsetof(iTween_t488923914, ___vector3s_28)); }
	inline Vector3U5BU5D_t1172311765* get_vector3s_28() const { return ___vector3s_28; }
	inline Vector3U5BU5D_t1172311765** get_address_of_vector3s_28() { return &___vector3s_28; }
	inline void set_vector3s_28(Vector3U5BU5D_t1172311765* value)
	{
		___vector3s_28 = value;
		Il2CppCodeGenWriteBarrier((&___vector3s_28), value);
	}

	inline static int32_t get_offset_of_vector2s_29() { return static_cast<int32_t>(offsetof(iTween_t488923914, ___vector2s_29)); }
	inline Vector2U5BU5D_t686124026* get_vector2s_29() const { return ___vector2s_29; }
	inline Vector2U5BU5D_t686124026** get_address_of_vector2s_29() { return &___vector2s_29; }
	inline void set_vector2s_29(Vector2U5BU5D_t686124026* value)
	{
		___vector2s_29 = value;
		Il2CppCodeGenWriteBarrier((&___vector2s_29), value);
	}

	inline static int32_t get_offset_of_colors_30() { return static_cast<int32_t>(offsetof(iTween_t488923914, ___colors_30)); }
	inline ColorU5B0___U2C0___U5D_t672350443* get_colors_30() const { return ___colors_30; }
	inline ColorU5B0___U2C0___U5D_t672350443** get_address_of_colors_30() { return &___colors_30; }
	inline void set_colors_30(ColorU5B0___U2C0___U5D_t672350443* value)
	{
		___colors_30 = value;
		Il2CppCodeGenWriteBarrier((&___colors_30), value);
	}

	inline static int32_t get_offset_of_floats_31() { return static_cast<int32_t>(offsetof(iTween_t488923914, ___floats_31)); }
	inline SingleU5BU5D_t577127397* get_floats_31() const { return ___floats_31; }
	inline SingleU5BU5D_t577127397** get_address_of_floats_31() { return &___floats_31; }
	inline void set_floats_31(SingleU5BU5D_t577127397* value)
	{
		___floats_31 = value;
		Il2CppCodeGenWriteBarrier((&___floats_31), value);
	}

	inline static int32_t get_offset_of_rects_32() { return static_cast<int32_t>(offsetof(iTween_t488923914, ___rects_32)); }
	inline RectU5BU5D_t1299715887* get_rects_32() const { return ___rects_32; }
	inline RectU5BU5D_t1299715887** get_address_of_rects_32() { return &___rects_32; }
	inline void set_rects_32(RectU5BU5D_t1299715887* value)
	{
		___rects_32 = value;
		Il2CppCodeGenWriteBarrier((&___rects_32), value);
	}

	inline static int32_t get_offset_of_path_33() { return static_cast<int32_t>(offsetof(iTween_t488923914, ___path_33)); }
	inline CRSpline_t4177960625 * get_path_33() const { return ___path_33; }
	inline CRSpline_t4177960625 ** get_address_of_path_33() { return &___path_33; }
	inline void set_path_33(CRSpline_t4177960625 * value)
	{
		___path_33 = value;
		Il2CppCodeGenWriteBarrier((&___path_33), value);
	}

	inline static int32_t get_offset_of_preUpdate_34() { return static_cast<int32_t>(offsetof(iTween_t488923914, ___preUpdate_34)); }
	inline Vector3_t2243707580  get_preUpdate_34() const { return ___preUpdate_34; }
	inline Vector3_t2243707580 * get_address_of_preUpdate_34() { return &___preUpdate_34; }
	inline void set_preUpdate_34(Vector3_t2243707580  value)
	{
		___preUpdate_34 = value;
	}

	inline static int32_t get_offset_of_postUpdate_35() { return static_cast<int32_t>(offsetof(iTween_t488923914, ___postUpdate_35)); }
	inline Vector3_t2243707580  get_postUpdate_35() const { return ___postUpdate_35; }
	inline Vector3_t2243707580 * get_address_of_postUpdate_35() { return &___postUpdate_35; }
	inline void set_postUpdate_35(Vector3_t2243707580  value)
	{
		___postUpdate_35 = value;
	}

	inline static int32_t get_offset_of_namedcolorvalue_36() { return static_cast<int32_t>(offsetof(iTween_t488923914, ___namedcolorvalue_36)); }
	inline int32_t get_namedcolorvalue_36() const { return ___namedcolorvalue_36; }
	inline int32_t* get_address_of_namedcolorvalue_36() { return &___namedcolorvalue_36; }
	inline void set_namedcolorvalue_36(int32_t value)
	{
		___namedcolorvalue_36 = value;
	}

	inline static int32_t get_offset_of_lastRealTime_37() { return static_cast<int32_t>(offsetof(iTween_t488923914, ___lastRealTime_37)); }
	inline float get_lastRealTime_37() const { return ___lastRealTime_37; }
	inline float* get_address_of_lastRealTime_37() { return &___lastRealTime_37; }
	inline void set_lastRealTime_37(float value)
	{
		___lastRealTime_37 = value;
	}

	inline static int32_t get_offset_of_useRealTime_38() { return static_cast<int32_t>(offsetof(iTween_t488923914, ___useRealTime_38)); }
	inline bool get_useRealTime_38() const { return ___useRealTime_38; }
	inline bool* get_address_of_useRealTime_38() { return &___useRealTime_38; }
	inline void set_useRealTime_38(bool value)
	{
		___useRealTime_38 = value;
	}

	inline static int32_t get_offset_of_thisTransform_39() { return static_cast<int32_t>(offsetof(iTween_t488923914, ___thisTransform_39)); }
	inline Transform_t3275118058 * get_thisTransform_39() const { return ___thisTransform_39; }
	inline Transform_t3275118058 ** get_address_of_thisTransform_39() { return &___thisTransform_39; }
	inline void set_thisTransform_39(Transform_t3275118058 * value)
	{
		___thisTransform_39 = value;
		Il2CppCodeGenWriteBarrier((&___thisTransform_39), value);
	}
};

struct iTween_t488923914_StaticFields
{
public:
	// System.Collections.Generic.List`1<System.Collections.Hashtable> iTween::tweens
	List_1_t278961118 * ___tweens_2;
	// UnityEngine.GameObject iTween::cameraFade
	GameObject_t1756533147 * ___cameraFade_3;
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> iTween::<>f__switch$map0
	Dictionary_2_t3986656710 * ___U3CU3Ef__switchU24map0_40;

public:
	inline static int32_t get_offset_of_tweens_2() { return static_cast<int32_t>(offsetof(iTween_t488923914_StaticFields, ___tweens_2)); }
	inline List_1_t278961118 * get_tweens_2() const { return ___tweens_2; }
	inline List_1_t278961118 ** get_address_of_tweens_2() { return &___tweens_2; }
	inline void set_tweens_2(List_1_t278961118 * value)
	{
		___tweens_2 = value;
		Il2CppCodeGenWriteBarrier((&___tweens_2), value);
	}

	inline static int32_t get_offset_of_cameraFade_3() { return static_cast<int32_t>(offsetof(iTween_t488923914_StaticFields, ___cameraFade_3)); }
	inline GameObject_t1756533147 * get_cameraFade_3() const { return ___cameraFade_3; }
	inline GameObject_t1756533147 ** get_address_of_cameraFade_3() { return &___cameraFade_3; }
	inline void set_cameraFade_3(GameObject_t1756533147 * value)
	{
		___cameraFade_3 = value;
		Il2CppCodeGenWriteBarrier((&___cameraFade_3), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__switchU24map0_40() { return static_cast<int32_t>(offsetof(iTween_t488923914_StaticFields, ___U3CU3Ef__switchU24map0_40)); }
	inline Dictionary_2_t3986656710 * get_U3CU3Ef__switchU24map0_40() const { return ___U3CU3Ef__switchU24map0_40; }
	inline Dictionary_2_t3986656710 ** get_address_of_U3CU3Ef__switchU24map0_40() { return &___U3CU3Ef__switchU24map0_40; }
	inline void set_U3CU3Ef__switchU24map0_40(Dictionary_2_t3986656710 * value)
	{
		___U3CU3Ef__switchU24map0_40 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__switchU24map0_40), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ITWEEN_T488923914_H
#ifndef UIBEHAVIOUR_T3960014691_H
#define UIBEHAVIOUR_T3960014691_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.EventSystems.UIBehaviour
struct  UIBehaviour_t3960014691  : public MonoBehaviour_t1158329972
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UIBEHAVIOUR_T3960014691_H
#ifndef ANALYTICSTRACKER_T2191537572_H
#define ANALYTICSTRACKER_T2191537572_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Analytics.AnalyticsTracker
struct  AnalyticsTracker_t2191537572  : public MonoBehaviour_t1158329972
{
public:
	// System.String UnityEngine.Analytics.AnalyticsTracker::m_EventName
	String_t* ___m_EventName_2;
	// System.Collections.Generic.Dictionary`2<System.String,System.Object> UnityEngine.Analytics.AnalyticsTracker::m_Dict
	Dictionary_2_t309261261 * ___m_Dict_3;
	// System.Int32 UnityEngine.Analytics.AnalyticsTracker::m_PrevDictHash
	int32_t ___m_PrevDictHash_4;
	// UnityEngine.Analytics.TrackableProperty UnityEngine.Analytics.AnalyticsTracker::m_TrackableProperty
	TrackableProperty_t1304606600 * ___m_TrackableProperty_5;
	// UnityEngine.Analytics.AnalyticsTracker/Trigger UnityEngine.Analytics.AnalyticsTracker::m_Trigger
	int32_t ___m_Trigger_6;

public:
	inline static int32_t get_offset_of_m_EventName_2() { return static_cast<int32_t>(offsetof(AnalyticsTracker_t2191537572, ___m_EventName_2)); }
	inline String_t* get_m_EventName_2() const { return ___m_EventName_2; }
	inline String_t** get_address_of_m_EventName_2() { return &___m_EventName_2; }
	inline void set_m_EventName_2(String_t* value)
	{
		___m_EventName_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_EventName_2), value);
	}

	inline static int32_t get_offset_of_m_Dict_3() { return static_cast<int32_t>(offsetof(AnalyticsTracker_t2191537572, ___m_Dict_3)); }
	inline Dictionary_2_t309261261 * get_m_Dict_3() const { return ___m_Dict_3; }
	inline Dictionary_2_t309261261 ** get_address_of_m_Dict_3() { return &___m_Dict_3; }
	inline void set_m_Dict_3(Dictionary_2_t309261261 * value)
	{
		___m_Dict_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_Dict_3), value);
	}

	inline static int32_t get_offset_of_m_PrevDictHash_4() { return static_cast<int32_t>(offsetof(AnalyticsTracker_t2191537572, ___m_PrevDictHash_4)); }
	inline int32_t get_m_PrevDictHash_4() const { return ___m_PrevDictHash_4; }
	inline int32_t* get_address_of_m_PrevDictHash_4() { return &___m_PrevDictHash_4; }
	inline void set_m_PrevDictHash_4(int32_t value)
	{
		___m_PrevDictHash_4 = value;
	}

	inline static int32_t get_offset_of_m_TrackableProperty_5() { return static_cast<int32_t>(offsetof(AnalyticsTracker_t2191537572, ___m_TrackableProperty_5)); }
	inline TrackableProperty_t1304606600 * get_m_TrackableProperty_5() const { return ___m_TrackableProperty_5; }
	inline TrackableProperty_t1304606600 ** get_address_of_m_TrackableProperty_5() { return &___m_TrackableProperty_5; }
	inline void set_m_TrackableProperty_5(TrackableProperty_t1304606600 * value)
	{
		___m_TrackableProperty_5 = value;
		Il2CppCodeGenWriteBarrier((&___m_TrackableProperty_5), value);
	}

	inline static int32_t get_offset_of_m_Trigger_6() { return static_cast<int32_t>(offsetof(AnalyticsTracker_t2191537572, ___m_Trigger_6)); }
	inline int32_t get_m_Trigger_6() const { return ___m_Trigger_6; }
	inline int32_t* get_address_of_m_Trigger_6() { return &___m_Trigger_6; }
	inline void set_m_Trigger_6(int32_t value)
	{
		___m_Trigger_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ANALYTICSTRACKER_T2191537572_H
#ifndef LAYOUTELEMENT_T2808691390_H
#define LAYOUTELEMENT_T2808691390_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.LayoutElement
struct  LayoutElement_t2808691390  : public UIBehaviour_t3960014691
{
public:
	// System.Boolean UnityEngine.UI.LayoutElement::m_IgnoreLayout
	bool ___m_IgnoreLayout_2;
	// System.Single UnityEngine.UI.LayoutElement::m_MinWidth
	float ___m_MinWidth_3;
	// System.Single UnityEngine.UI.LayoutElement::m_MinHeight
	float ___m_MinHeight_4;
	// System.Single UnityEngine.UI.LayoutElement::m_PreferredWidth
	float ___m_PreferredWidth_5;
	// System.Single UnityEngine.UI.LayoutElement::m_PreferredHeight
	float ___m_PreferredHeight_6;
	// System.Single UnityEngine.UI.LayoutElement::m_FlexibleWidth
	float ___m_FlexibleWidth_7;
	// System.Single UnityEngine.UI.LayoutElement::m_FlexibleHeight
	float ___m_FlexibleHeight_8;
	// System.Int32 UnityEngine.UI.LayoutElement::m_LayoutPriority
	int32_t ___m_LayoutPriority_9;

public:
	inline static int32_t get_offset_of_m_IgnoreLayout_2() { return static_cast<int32_t>(offsetof(LayoutElement_t2808691390, ___m_IgnoreLayout_2)); }
	inline bool get_m_IgnoreLayout_2() const { return ___m_IgnoreLayout_2; }
	inline bool* get_address_of_m_IgnoreLayout_2() { return &___m_IgnoreLayout_2; }
	inline void set_m_IgnoreLayout_2(bool value)
	{
		___m_IgnoreLayout_2 = value;
	}

	inline static int32_t get_offset_of_m_MinWidth_3() { return static_cast<int32_t>(offsetof(LayoutElement_t2808691390, ___m_MinWidth_3)); }
	inline float get_m_MinWidth_3() const { return ___m_MinWidth_3; }
	inline float* get_address_of_m_MinWidth_3() { return &___m_MinWidth_3; }
	inline void set_m_MinWidth_3(float value)
	{
		___m_MinWidth_3 = value;
	}

	inline static int32_t get_offset_of_m_MinHeight_4() { return static_cast<int32_t>(offsetof(LayoutElement_t2808691390, ___m_MinHeight_4)); }
	inline float get_m_MinHeight_4() const { return ___m_MinHeight_4; }
	inline float* get_address_of_m_MinHeight_4() { return &___m_MinHeight_4; }
	inline void set_m_MinHeight_4(float value)
	{
		___m_MinHeight_4 = value;
	}

	inline static int32_t get_offset_of_m_PreferredWidth_5() { return static_cast<int32_t>(offsetof(LayoutElement_t2808691390, ___m_PreferredWidth_5)); }
	inline float get_m_PreferredWidth_5() const { return ___m_PreferredWidth_5; }
	inline float* get_address_of_m_PreferredWidth_5() { return &___m_PreferredWidth_5; }
	inline void set_m_PreferredWidth_5(float value)
	{
		___m_PreferredWidth_5 = value;
	}

	inline static int32_t get_offset_of_m_PreferredHeight_6() { return static_cast<int32_t>(offsetof(LayoutElement_t2808691390, ___m_PreferredHeight_6)); }
	inline float get_m_PreferredHeight_6() const { return ___m_PreferredHeight_6; }
	inline float* get_address_of_m_PreferredHeight_6() { return &___m_PreferredHeight_6; }
	inline void set_m_PreferredHeight_6(float value)
	{
		___m_PreferredHeight_6 = value;
	}

	inline static int32_t get_offset_of_m_FlexibleWidth_7() { return static_cast<int32_t>(offsetof(LayoutElement_t2808691390, ___m_FlexibleWidth_7)); }
	inline float get_m_FlexibleWidth_7() const { return ___m_FlexibleWidth_7; }
	inline float* get_address_of_m_FlexibleWidth_7() { return &___m_FlexibleWidth_7; }
	inline void set_m_FlexibleWidth_7(float value)
	{
		___m_FlexibleWidth_7 = value;
	}

	inline static int32_t get_offset_of_m_FlexibleHeight_8() { return static_cast<int32_t>(offsetof(LayoutElement_t2808691390, ___m_FlexibleHeight_8)); }
	inline float get_m_FlexibleHeight_8() const { return ___m_FlexibleHeight_8; }
	inline float* get_address_of_m_FlexibleHeight_8() { return &___m_FlexibleHeight_8; }
	inline void set_m_FlexibleHeight_8(float value)
	{
		___m_FlexibleHeight_8 = value;
	}

	inline static int32_t get_offset_of_m_LayoutPriority_9() { return static_cast<int32_t>(offsetof(LayoutElement_t2808691390, ___m_LayoutPriority_9)); }
	inline int32_t get_m_LayoutPriority_9() const { return ___m_LayoutPriority_9; }
	inline int32_t* get_address_of_m_LayoutPriority_9() { return &___m_LayoutPriority_9; }
	inline void set_m_LayoutPriority_9(int32_t value)
	{
		___m_LayoutPriority_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LAYOUTELEMENT_T2808691390_H
#ifndef LAYOUTGROUP_T3962498969_H
#define LAYOUTGROUP_T3962498969_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.LayoutGroup
struct  LayoutGroup_t3962498969  : public UIBehaviour_t3960014691
{
public:
	// UnityEngine.RectOffset UnityEngine.UI.LayoutGroup::m_Padding
	RectOffset_t3387826427 * ___m_Padding_2;
	// UnityEngine.TextAnchor UnityEngine.UI.LayoutGroup::m_ChildAlignment
	int32_t ___m_ChildAlignment_3;
	// UnityEngine.RectTransform UnityEngine.UI.LayoutGroup::m_Rect
	RectTransform_t3349966182 * ___m_Rect_4;
	// UnityEngine.DrivenRectTransformTracker UnityEngine.UI.LayoutGroup::m_Tracker
	DrivenRectTransformTracker_t154385424  ___m_Tracker_5;
	// UnityEngine.Vector2 UnityEngine.UI.LayoutGroup::m_TotalMinSize
	Vector2_t2243707579  ___m_TotalMinSize_6;
	// UnityEngine.Vector2 UnityEngine.UI.LayoutGroup::m_TotalPreferredSize
	Vector2_t2243707579  ___m_TotalPreferredSize_7;
	// UnityEngine.Vector2 UnityEngine.UI.LayoutGroup::m_TotalFlexibleSize
	Vector2_t2243707579  ___m_TotalFlexibleSize_8;
	// System.Collections.Generic.List`1<UnityEngine.RectTransform> UnityEngine.UI.LayoutGroup::m_RectChildren
	List_1_t2719087314 * ___m_RectChildren_9;

public:
	inline static int32_t get_offset_of_m_Padding_2() { return static_cast<int32_t>(offsetof(LayoutGroup_t3962498969, ___m_Padding_2)); }
	inline RectOffset_t3387826427 * get_m_Padding_2() const { return ___m_Padding_2; }
	inline RectOffset_t3387826427 ** get_address_of_m_Padding_2() { return &___m_Padding_2; }
	inline void set_m_Padding_2(RectOffset_t3387826427 * value)
	{
		___m_Padding_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_Padding_2), value);
	}

	inline static int32_t get_offset_of_m_ChildAlignment_3() { return static_cast<int32_t>(offsetof(LayoutGroup_t3962498969, ___m_ChildAlignment_3)); }
	inline int32_t get_m_ChildAlignment_3() const { return ___m_ChildAlignment_3; }
	inline int32_t* get_address_of_m_ChildAlignment_3() { return &___m_ChildAlignment_3; }
	inline void set_m_ChildAlignment_3(int32_t value)
	{
		___m_ChildAlignment_3 = value;
	}

	inline static int32_t get_offset_of_m_Rect_4() { return static_cast<int32_t>(offsetof(LayoutGroup_t3962498969, ___m_Rect_4)); }
	inline RectTransform_t3349966182 * get_m_Rect_4() const { return ___m_Rect_4; }
	inline RectTransform_t3349966182 ** get_address_of_m_Rect_4() { return &___m_Rect_4; }
	inline void set_m_Rect_4(RectTransform_t3349966182 * value)
	{
		___m_Rect_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_Rect_4), value);
	}

	inline static int32_t get_offset_of_m_Tracker_5() { return static_cast<int32_t>(offsetof(LayoutGroup_t3962498969, ___m_Tracker_5)); }
	inline DrivenRectTransformTracker_t154385424  get_m_Tracker_5() const { return ___m_Tracker_5; }
	inline DrivenRectTransformTracker_t154385424 * get_address_of_m_Tracker_5() { return &___m_Tracker_5; }
	inline void set_m_Tracker_5(DrivenRectTransformTracker_t154385424  value)
	{
		___m_Tracker_5 = value;
	}

	inline static int32_t get_offset_of_m_TotalMinSize_6() { return static_cast<int32_t>(offsetof(LayoutGroup_t3962498969, ___m_TotalMinSize_6)); }
	inline Vector2_t2243707579  get_m_TotalMinSize_6() const { return ___m_TotalMinSize_6; }
	inline Vector2_t2243707579 * get_address_of_m_TotalMinSize_6() { return &___m_TotalMinSize_6; }
	inline void set_m_TotalMinSize_6(Vector2_t2243707579  value)
	{
		___m_TotalMinSize_6 = value;
	}

	inline static int32_t get_offset_of_m_TotalPreferredSize_7() { return static_cast<int32_t>(offsetof(LayoutGroup_t3962498969, ___m_TotalPreferredSize_7)); }
	inline Vector2_t2243707579  get_m_TotalPreferredSize_7() const { return ___m_TotalPreferredSize_7; }
	inline Vector2_t2243707579 * get_address_of_m_TotalPreferredSize_7() { return &___m_TotalPreferredSize_7; }
	inline void set_m_TotalPreferredSize_7(Vector2_t2243707579  value)
	{
		___m_TotalPreferredSize_7 = value;
	}

	inline static int32_t get_offset_of_m_TotalFlexibleSize_8() { return static_cast<int32_t>(offsetof(LayoutGroup_t3962498969, ___m_TotalFlexibleSize_8)); }
	inline Vector2_t2243707579  get_m_TotalFlexibleSize_8() const { return ___m_TotalFlexibleSize_8; }
	inline Vector2_t2243707579 * get_address_of_m_TotalFlexibleSize_8() { return &___m_TotalFlexibleSize_8; }
	inline void set_m_TotalFlexibleSize_8(Vector2_t2243707579  value)
	{
		___m_TotalFlexibleSize_8 = value;
	}

	inline static int32_t get_offset_of_m_RectChildren_9() { return static_cast<int32_t>(offsetof(LayoutGroup_t3962498969, ___m_RectChildren_9)); }
	inline List_1_t2719087314 * get_m_RectChildren_9() const { return ___m_RectChildren_9; }
	inline List_1_t2719087314 ** get_address_of_m_RectChildren_9() { return &___m_RectChildren_9; }
	inline void set_m_RectChildren_9(List_1_t2719087314 * value)
	{
		___m_RectChildren_9 = value;
		Il2CppCodeGenWriteBarrier((&___m_RectChildren_9), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LAYOUTGROUP_T3962498969_H
#ifndef ASPECTRATIOFITTER_T3114550109_H
#define ASPECTRATIOFITTER_T3114550109_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.AspectRatioFitter
struct  AspectRatioFitter_t3114550109  : public UIBehaviour_t3960014691
{
public:
	// UnityEngine.UI.AspectRatioFitter/AspectMode UnityEngine.UI.AspectRatioFitter::m_AspectMode
	int32_t ___m_AspectMode_2;
	// System.Single UnityEngine.UI.AspectRatioFitter::m_AspectRatio
	float ___m_AspectRatio_3;
	// UnityEngine.RectTransform UnityEngine.UI.AspectRatioFitter::m_Rect
	RectTransform_t3349966182 * ___m_Rect_4;
	// UnityEngine.DrivenRectTransformTracker UnityEngine.UI.AspectRatioFitter::m_Tracker
	DrivenRectTransformTracker_t154385424  ___m_Tracker_5;

public:
	inline static int32_t get_offset_of_m_AspectMode_2() { return static_cast<int32_t>(offsetof(AspectRatioFitter_t3114550109, ___m_AspectMode_2)); }
	inline int32_t get_m_AspectMode_2() const { return ___m_AspectMode_2; }
	inline int32_t* get_address_of_m_AspectMode_2() { return &___m_AspectMode_2; }
	inline void set_m_AspectMode_2(int32_t value)
	{
		___m_AspectMode_2 = value;
	}

	inline static int32_t get_offset_of_m_AspectRatio_3() { return static_cast<int32_t>(offsetof(AspectRatioFitter_t3114550109, ___m_AspectRatio_3)); }
	inline float get_m_AspectRatio_3() const { return ___m_AspectRatio_3; }
	inline float* get_address_of_m_AspectRatio_3() { return &___m_AspectRatio_3; }
	inline void set_m_AspectRatio_3(float value)
	{
		___m_AspectRatio_3 = value;
	}

	inline static int32_t get_offset_of_m_Rect_4() { return static_cast<int32_t>(offsetof(AspectRatioFitter_t3114550109, ___m_Rect_4)); }
	inline RectTransform_t3349966182 * get_m_Rect_4() const { return ___m_Rect_4; }
	inline RectTransform_t3349966182 ** get_address_of_m_Rect_4() { return &___m_Rect_4; }
	inline void set_m_Rect_4(RectTransform_t3349966182 * value)
	{
		___m_Rect_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_Rect_4), value);
	}

	inline static int32_t get_offset_of_m_Tracker_5() { return static_cast<int32_t>(offsetof(AspectRatioFitter_t3114550109, ___m_Tracker_5)); }
	inline DrivenRectTransformTracker_t154385424  get_m_Tracker_5() const { return ___m_Tracker_5; }
	inline DrivenRectTransformTracker_t154385424 * get_address_of_m_Tracker_5() { return &___m_Tracker_5; }
	inline void set_m_Tracker_5(DrivenRectTransformTracker_t154385424  value)
	{
		___m_Tracker_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ASPECTRATIOFITTER_T3114550109_H
#ifndef BASEMESHEFFECT_T1728560551_H
#define BASEMESHEFFECT_T1728560551_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.BaseMeshEffect
struct  BaseMeshEffect_t1728560551  : public UIBehaviour_t3960014691
{
public:
	// UnityEngine.UI.Graphic UnityEngine.UI.BaseMeshEffect::m_Graphic
	Graphic_t2426225576 * ___m_Graphic_2;

public:
	inline static int32_t get_offset_of_m_Graphic_2() { return static_cast<int32_t>(offsetof(BaseMeshEffect_t1728560551, ___m_Graphic_2)); }
	inline Graphic_t2426225576 * get_m_Graphic_2() const { return ___m_Graphic_2; }
	inline Graphic_t2426225576 ** get_address_of_m_Graphic_2() { return &___m_Graphic_2; }
	inline void set_m_Graphic_2(Graphic_t2426225576 * value)
	{
		___m_Graphic_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_Graphic_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BASEMESHEFFECT_T1728560551_H
#ifndef CONTENTSIZEFITTER_T1325211874_H
#define CONTENTSIZEFITTER_T1325211874_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.ContentSizeFitter
struct  ContentSizeFitter_t1325211874  : public UIBehaviour_t3960014691
{
public:
	// UnityEngine.UI.ContentSizeFitter/FitMode UnityEngine.UI.ContentSizeFitter::m_HorizontalFit
	int32_t ___m_HorizontalFit_2;
	// UnityEngine.UI.ContentSizeFitter/FitMode UnityEngine.UI.ContentSizeFitter::m_VerticalFit
	int32_t ___m_VerticalFit_3;
	// UnityEngine.RectTransform UnityEngine.UI.ContentSizeFitter::m_Rect
	RectTransform_t3349966182 * ___m_Rect_4;
	// UnityEngine.DrivenRectTransformTracker UnityEngine.UI.ContentSizeFitter::m_Tracker
	DrivenRectTransformTracker_t154385424  ___m_Tracker_5;

public:
	inline static int32_t get_offset_of_m_HorizontalFit_2() { return static_cast<int32_t>(offsetof(ContentSizeFitter_t1325211874, ___m_HorizontalFit_2)); }
	inline int32_t get_m_HorizontalFit_2() const { return ___m_HorizontalFit_2; }
	inline int32_t* get_address_of_m_HorizontalFit_2() { return &___m_HorizontalFit_2; }
	inline void set_m_HorizontalFit_2(int32_t value)
	{
		___m_HorizontalFit_2 = value;
	}

	inline static int32_t get_offset_of_m_VerticalFit_3() { return static_cast<int32_t>(offsetof(ContentSizeFitter_t1325211874, ___m_VerticalFit_3)); }
	inline int32_t get_m_VerticalFit_3() const { return ___m_VerticalFit_3; }
	inline int32_t* get_address_of_m_VerticalFit_3() { return &___m_VerticalFit_3; }
	inline void set_m_VerticalFit_3(int32_t value)
	{
		___m_VerticalFit_3 = value;
	}

	inline static int32_t get_offset_of_m_Rect_4() { return static_cast<int32_t>(offsetof(ContentSizeFitter_t1325211874, ___m_Rect_4)); }
	inline RectTransform_t3349966182 * get_m_Rect_4() const { return ___m_Rect_4; }
	inline RectTransform_t3349966182 ** get_address_of_m_Rect_4() { return &___m_Rect_4; }
	inline void set_m_Rect_4(RectTransform_t3349966182 * value)
	{
		___m_Rect_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_Rect_4), value);
	}

	inline static int32_t get_offset_of_m_Tracker_5() { return static_cast<int32_t>(offsetof(ContentSizeFitter_t1325211874, ___m_Tracker_5)); }
	inline DrivenRectTransformTracker_t154385424  get_m_Tracker_5() const { return ___m_Tracker_5; }
	inline DrivenRectTransformTracker_t154385424 * get_address_of_m_Tracker_5() { return &___m_Tracker_5; }
	inline void set_m_Tracker_5(DrivenRectTransformTracker_t154385424  value)
	{
		___m_Tracker_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONTENTSIZEFITTER_T1325211874_H
#ifndef CANVASSCALER_T2574720772_H
#define CANVASSCALER_T2574720772_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.CanvasScaler
struct  CanvasScaler_t2574720772  : public UIBehaviour_t3960014691
{
public:
	// UnityEngine.UI.CanvasScaler/ScaleMode UnityEngine.UI.CanvasScaler::m_UiScaleMode
	int32_t ___m_UiScaleMode_2;
	// System.Single UnityEngine.UI.CanvasScaler::m_ReferencePixelsPerUnit
	float ___m_ReferencePixelsPerUnit_3;
	// System.Single UnityEngine.UI.CanvasScaler::m_ScaleFactor
	float ___m_ScaleFactor_4;
	// UnityEngine.Vector2 UnityEngine.UI.CanvasScaler::m_ReferenceResolution
	Vector2_t2243707579  ___m_ReferenceResolution_5;
	// UnityEngine.UI.CanvasScaler/ScreenMatchMode UnityEngine.UI.CanvasScaler::m_ScreenMatchMode
	int32_t ___m_ScreenMatchMode_6;
	// System.Single UnityEngine.UI.CanvasScaler::m_MatchWidthOrHeight
	float ___m_MatchWidthOrHeight_7;
	// UnityEngine.UI.CanvasScaler/Unit UnityEngine.UI.CanvasScaler::m_PhysicalUnit
	int32_t ___m_PhysicalUnit_9;
	// System.Single UnityEngine.UI.CanvasScaler::m_FallbackScreenDPI
	float ___m_FallbackScreenDPI_10;
	// System.Single UnityEngine.UI.CanvasScaler::m_DefaultSpriteDPI
	float ___m_DefaultSpriteDPI_11;
	// System.Single UnityEngine.UI.CanvasScaler::m_DynamicPixelsPerUnit
	float ___m_DynamicPixelsPerUnit_12;
	// UnityEngine.Canvas UnityEngine.UI.CanvasScaler::m_Canvas
	Canvas_t209405766 * ___m_Canvas_13;
	// System.Single UnityEngine.UI.CanvasScaler::m_PrevScaleFactor
	float ___m_PrevScaleFactor_14;
	// System.Single UnityEngine.UI.CanvasScaler::m_PrevReferencePixelsPerUnit
	float ___m_PrevReferencePixelsPerUnit_15;

public:
	inline static int32_t get_offset_of_m_UiScaleMode_2() { return static_cast<int32_t>(offsetof(CanvasScaler_t2574720772, ___m_UiScaleMode_2)); }
	inline int32_t get_m_UiScaleMode_2() const { return ___m_UiScaleMode_2; }
	inline int32_t* get_address_of_m_UiScaleMode_2() { return &___m_UiScaleMode_2; }
	inline void set_m_UiScaleMode_2(int32_t value)
	{
		___m_UiScaleMode_2 = value;
	}

	inline static int32_t get_offset_of_m_ReferencePixelsPerUnit_3() { return static_cast<int32_t>(offsetof(CanvasScaler_t2574720772, ___m_ReferencePixelsPerUnit_3)); }
	inline float get_m_ReferencePixelsPerUnit_3() const { return ___m_ReferencePixelsPerUnit_3; }
	inline float* get_address_of_m_ReferencePixelsPerUnit_3() { return &___m_ReferencePixelsPerUnit_3; }
	inline void set_m_ReferencePixelsPerUnit_3(float value)
	{
		___m_ReferencePixelsPerUnit_3 = value;
	}

	inline static int32_t get_offset_of_m_ScaleFactor_4() { return static_cast<int32_t>(offsetof(CanvasScaler_t2574720772, ___m_ScaleFactor_4)); }
	inline float get_m_ScaleFactor_4() const { return ___m_ScaleFactor_4; }
	inline float* get_address_of_m_ScaleFactor_4() { return &___m_ScaleFactor_4; }
	inline void set_m_ScaleFactor_4(float value)
	{
		___m_ScaleFactor_4 = value;
	}

	inline static int32_t get_offset_of_m_ReferenceResolution_5() { return static_cast<int32_t>(offsetof(CanvasScaler_t2574720772, ___m_ReferenceResolution_5)); }
	inline Vector2_t2243707579  get_m_ReferenceResolution_5() const { return ___m_ReferenceResolution_5; }
	inline Vector2_t2243707579 * get_address_of_m_ReferenceResolution_5() { return &___m_ReferenceResolution_5; }
	inline void set_m_ReferenceResolution_5(Vector2_t2243707579  value)
	{
		___m_ReferenceResolution_5 = value;
	}

	inline static int32_t get_offset_of_m_ScreenMatchMode_6() { return static_cast<int32_t>(offsetof(CanvasScaler_t2574720772, ___m_ScreenMatchMode_6)); }
	inline int32_t get_m_ScreenMatchMode_6() const { return ___m_ScreenMatchMode_6; }
	inline int32_t* get_address_of_m_ScreenMatchMode_6() { return &___m_ScreenMatchMode_6; }
	inline void set_m_ScreenMatchMode_6(int32_t value)
	{
		___m_ScreenMatchMode_6 = value;
	}

	inline static int32_t get_offset_of_m_MatchWidthOrHeight_7() { return static_cast<int32_t>(offsetof(CanvasScaler_t2574720772, ___m_MatchWidthOrHeight_7)); }
	inline float get_m_MatchWidthOrHeight_7() const { return ___m_MatchWidthOrHeight_7; }
	inline float* get_address_of_m_MatchWidthOrHeight_7() { return &___m_MatchWidthOrHeight_7; }
	inline void set_m_MatchWidthOrHeight_7(float value)
	{
		___m_MatchWidthOrHeight_7 = value;
	}

	inline static int32_t get_offset_of_m_PhysicalUnit_9() { return static_cast<int32_t>(offsetof(CanvasScaler_t2574720772, ___m_PhysicalUnit_9)); }
	inline int32_t get_m_PhysicalUnit_9() const { return ___m_PhysicalUnit_9; }
	inline int32_t* get_address_of_m_PhysicalUnit_9() { return &___m_PhysicalUnit_9; }
	inline void set_m_PhysicalUnit_9(int32_t value)
	{
		___m_PhysicalUnit_9 = value;
	}

	inline static int32_t get_offset_of_m_FallbackScreenDPI_10() { return static_cast<int32_t>(offsetof(CanvasScaler_t2574720772, ___m_FallbackScreenDPI_10)); }
	inline float get_m_FallbackScreenDPI_10() const { return ___m_FallbackScreenDPI_10; }
	inline float* get_address_of_m_FallbackScreenDPI_10() { return &___m_FallbackScreenDPI_10; }
	inline void set_m_FallbackScreenDPI_10(float value)
	{
		___m_FallbackScreenDPI_10 = value;
	}

	inline static int32_t get_offset_of_m_DefaultSpriteDPI_11() { return static_cast<int32_t>(offsetof(CanvasScaler_t2574720772, ___m_DefaultSpriteDPI_11)); }
	inline float get_m_DefaultSpriteDPI_11() const { return ___m_DefaultSpriteDPI_11; }
	inline float* get_address_of_m_DefaultSpriteDPI_11() { return &___m_DefaultSpriteDPI_11; }
	inline void set_m_DefaultSpriteDPI_11(float value)
	{
		___m_DefaultSpriteDPI_11 = value;
	}

	inline static int32_t get_offset_of_m_DynamicPixelsPerUnit_12() { return static_cast<int32_t>(offsetof(CanvasScaler_t2574720772, ___m_DynamicPixelsPerUnit_12)); }
	inline float get_m_DynamicPixelsPerUnit_12() const { return ___m_DynamicPixelsPerUnit_12; }
	inline float* get_address_of_m_DynamicPixelsPerUnit_12() { return &___m_DynamicPixelsPerUnit_12; }
	inline void set_m_DynamicPixelsPerUnit_12(float value)
	{
		___m_DynamicPixelsPerUnit_12 = value;
	}

	inline static int32_t get_offset_of_m_Canvas_13() { return static_cast<int32_t>(offsetof(CanvasScaler_t2574720772, ___m_Canvas_13)); }
	inline Canvas_t209405766 * get_m_Canvas_13() const { return ___m_Canvas_13; }
	inline Canvas_t209405766 ** get_address_of_m_Canvas_13() { return &___m_Canvas_13; }
	inline void set_m_Canvas_13(Canvas_t209405766 * value)
	{
		___m_Canvas_13 = value;
		Il2CppCodeGenWriteBarrier((&___m_Canvas_13), value);
	}

	inline static int32_t get_offset_of_m_PrevScaleFactor_14() { return static_cast<int32_t>(offsetof(CanvasScaler_t2574720772, ___m_PrevScaleFactor_14)); }
	inline float get_m_PrevScaleFactor_14() const { return ___m_PrevScaleFactor_14; }
	inline float* get_address_of_m_PrevScaleFactor_14() { return &___m_PrevScaleFactor_14; }
	inline void set_m_PrevScaleFactor_14(float value)
	{
		___m_PrevScaleFactor_14 = value;
	}

	inline static int32_t get_offset_of_m_PrevReferencePixelsPerUnit_15() { return static_cast<int32_t>(offsetof(CanvasScaler_t2574720772, ___m_PrevReferencePixelsPerUnit_15)); }
	inline float get_m_PrevReferencePixelsPerUnit_15() const { return ___m_PrevReferencePixelsPerUnit_15; }
	inline float* get_address_of_m_PrevReferencePixelsPerUnit_15() { return &___m_PrevReferencePixelsPerUnit_15; }
	inline void set_m_PrevReferencePixelsPerUnit_15(float value)
	{
		___m_PrevReferencePixelsPerUnit_15 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CANVASSCALER_T2574720772_H
#ifndef SHADOW_T4269599528_H
#define SHADOW_T4269599528_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Shadow
struct  Shadow_t4269599528  : public BaseMeshEffect_t1728560551
{
public:
	// UnityEngine.Color UnityEngine.UI.Shadow::m_EffectColor
	Color_t2020392075  ___m_EffectColor_3;
	// UnityEngine.Vector2 UnityEngine.UI.Shadow::m_EffectDistance
	Vector2_t2243707579  ___m_EffectDistance_4;
	// System.Boolean UnityEngine.UI.Shadow::m_UseGraphicAlpha
	bool ___m_UseGraphicAlpha_5;

public:
	inline static int32_t get_offset_of_m_EffectColor_3() { return static_cast<int32_t>(offsetof(Shadow_t4269599528, ___m_EffectColor_3)); }
	inline Color_t2020392075  get_m_EffectColor_3() const { return ___m_EffectColor_3; }
	inline Color_t2020392075 * get_address_of_m_EffectColor_3() { return &___m_EffectColor_3; }
	inline void set_m_EffectColor_3(Color_t2020392075  value)
	{
		___m_EffectColor_3 = value;
	}

	inline static int32_t get_offset_of_m_EffectDistance_4() { return static_cast<int32_t>(offsetof(Shadow_t4269599528, ___m_EffectDistance_4)); }
	inline Vector2_t2243707579  get_m_EffectDistance_4() const { return ___m_EffectDistance_4; }
	inline Vector2_t2243707579 * get_address_of_m_EffectDistance_4() { return &___m_EffectDistance_4; }
	inline void set_m_EffectDistance_4(Vector2_t2243707579  value)
	{
		___m_EffectDistance_4 = value;
	}

	inline static int32_t get_offset_of_m_UseGraphicAlpha_5() { return static_cast<int32_t>(offsetof(Shadow_t4269599528, ___m_UseGraphicAlpha_5)); }
	inline bool get_m_UseGraphicAlpha_5() const { return ___m_UseGraphicAlpha_5; }
	inline bool* get_address_of_m_UseGraphicAlpha_5() { return &___m_UseGraphicAlpha_5; }
	inline void set_m_UseGraphicAlpha_5(bool value)
	{
		___m_UseGraphicAlpha_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SHADOW_T4269599528_H
#ifndef GRIDLAYOUTGROUP_T1515633077_H
#define GRIDLAYOUTGROUP_T1515633077_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.GridLayoutGroup
struct  GridLayoutGroup_t1515633077  : public LayoutGroup_t3962498969
{
public:
	// UnityEngine.UI.GridLayoutGroup/Corner UnityEngine.UI.GridLayoutGroup::m_StartCorner
	int32_t ___m_StartCorner_10;
	// UnityEngine.UI.GridLayoutGroup/Axis UnityEngine.UI.GridLayoutGroup::m_StartAxis
	int32_t ___m_StartAxis_11;
	// UnityEngine.Vector2 UnityEngine.UI.GridLayoutGroup::m_CellSize
	Vector2_t2243707579  ___m_CellSize_12;
	// UnityEngine.Vector2 UnityEngine.UI.GridLayoutGroup::m_Spacing
	Vector2_t2243707579  ___m_Spacing_13;
	// UnityEngine.UI.GridLayoutGroup/Constraint UnityEngine.UI.GridLayoutGroup::m_Constraint
	int32_t ___m_Constraint_14;
	// System.Int32 UnityEngine.UI.GridLayoutGroup::m_ConstraintCount
	int32_t ___m_ConstraintCount_15;

public:
	inline static int32_t get_offset_of_m_StartCorner_10() { return static_cast<int32_t>(offsetof(GridLayoutGroup_t1515633077, ___m_StartCorner_10)); }
	inline int32_t get_m_StartCorner_10() const { return ___m_StartCorner_10; }
	inline int32_t* get_address_of_m_StartCorner_10() { return &___m_StartCorner_10; }
	inline void set_m_StartCorner_10(int32_t value)
	{
		___m_StartCorner_10 = value;
	}

	inline static int32_t get_offset_of_m_StartAxis_11() { return static_cast<int32_t>(offsetof(GridLayoutGroup_t1515633077, ___m_StartAxis_11)); }
	inline int32_t get_m_StartAxis_11() const { return ___m_StartAxis_11; }
	inline int32_t* get_address_of_m_StartAxis_11() { return &___m_StartAxis_11; }
	inline void set_m_StartAxis_11(int32_t value)
	{
		___m_StartAxis_11 = value;
	}

	inline static int32_t get_offset_of_m_CellSize_12() { return static_cast<int32_t>(offsetof(GridLayoutGroup_t1515633077, ___m_CellSize_12)); }
	inline Vector2_t2243707579  get_m_CellSize_12() const { return ___m_CellSize_12; }
	inline Vector2_t2243707579 * get_address_of_m_CellSize_12() { return &___m_CellSize_12; }
	inline void set_m_CellSize_12(Vector2_t2243707579  value)
	{
		___m_CellSize_12 = value;
	}

	inline static int32_t get_offset_of_m_Spacing_13() { return static_cast<int32_t>(offsetof(GridLayoutGroup_t1515633077, ___m_Spacing_13)); }
	inline Vector2_t2243707579  get_m_Spacing_13() const { return ___m_Spacing_13; }
	inline Vector2_t2243707579 * get_address_of_m_Spacing_13() { return &___m_Spacing_13; }
	inline void set_m_Spacing_13(Vector2_t2243707579  value)
	{
		___m_Spacing_13 = value;
	}

	inline static int32_t get_offset_of_m_Constraint_14() { return static_cast<int32_t>(offsetof(GridLayoutGroup_t1515633077, ___m_Constraint_14)); }
	inline int32_t get_m_Constraint_14() const { return ___m_Constraint_14; }
	inline int32_t* get_address_of_m_Constraint_14() { return &___m_Constraint_14; }
	inline void set_m_Constraint_14(int32_t value)
	{
		___m_Constraint_14 = value;
	}

	inline static int32_t get_offset_of_m_ConstraintCount_15() { return static_cast<int32_t>(offsetof(GridLayoutGroup_t1515633077, ___m_ConstraintCount_15)); }
	inline int32_t get_m_ConstraintCount_15() const { return ___m_ConstraintCount_15; }
	inline int32_t* get_address_of_m_ConstraintCount_15() { return &___m_ConstraintCount_15; }
	inline void set_m_ConstraintCount_15(int32_t value)
	{
		___m_ConstraintCount_15 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GRIDLAYOUTGROUP_T1515633077_H
#ifndef HORIZONTALORVERTICALLAYOUTGROUP_T1968298610_H
#define HORIZONTALORVERTICALLAYOUTGROUP_T1968298610_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.HorizontalOrVerticalLayoutGroup
struct  HorizontalOrVerticalLayoutGroup_t1968298610  : public LayoutGroup_t3962498969
{
public:
	// System.Single UnityEngine.UI.HorizontalOrVerticalLayoutGroup::m_Spacing
	float ___m_Spacing_10;
	// System.Boolean UnityEngine.UI.HorizontalOrVerticalLayoutGroup::m_ChildForceExpandWidth
	bool ___m_ChildForceExpandWidth_11;
	// System.Boolean UnityEngine.UI.HorizontalOrVerticalLayoutGroup::m_ChildForceExpandHeight
	bool ___m_ChildForceExpandHeight_12;
	// System.Boolean UnityEngine.UI.HorizontalOrVerticalLayoutGroup::m_ChildControlWidth
	bool ___m_ChildControlWidth_13;
	// System.Boolean UnityEngine.UI.HorizontalOrVerticalLayoutGroup::m_ChildControlHeight
	bool ___m_ChildControlHeight_14;

public:
	inline static int32_t get_offset_of_m_Spacing_10() { return static_cast<int32_t>(offsetof(HorizontalOrVerticalLayoutGroup_t1968298610, ___m_Spacing_10)); }
	inline float get_m_Spacing_10() const { return ___m_Spacing_10; }
	inline float* get_address_of_m_Spacing_10() { return &___m_Spacing_10; }
	inline void set_m_Spacing_10(float value)
	{
		___m_Spacing_10 = value;
	}

	inline static int32_t get_offset_of_m_ChildForceExpandWidth_11() { return static_cast<int32_t>(offsetof(HorizontalOrVerticalLayoutGroup_t1968298610, ___m_ChildForceExpandWidth_11)); }
	inline bool get_m_ChildForceExpandWidth_11() const { return ___m_ChildForceExpandWidth_11; }
	inline bool* get_address_of_m_ChildForceExpandWidth_11() { return &___m_ChildForceExpandWidth_11; }
	inline void set_m_ChildForceExpandWidth_11(bool value)
	{
		___m_ChildForceExpandWidth_11 = value;
	}

	inline static int32_t get_offset_of_m_ChildForceExpandHeight_12() { return static_cast<int32_t>(offsetof(HorizontalOrVerticalLayoutGroup_t1968298610, ___m_ChildForceExpandHeight_12)); }
	inline bool get_m_ChildForceExpandHeight_12() const { return ___m_ChildForceExpandHeight_12; }
	inline bool* get_address_of_m_ChildForceExpandHeight_12() { return &___m_ChildForceExpandHeight_12; }
	inline void set_m_ChildForceExpandHeight_12(bool value)
	{
		___m_ChildForceExpandHeight_12 = value;
	}

	inline static int32_t get_offset_of_m_ChildControlWidth_13() { return static_cast<int32_t>(offsetof(HorizontalOrVerticalLayoutGroup_t1968298610, ___m_ChildControlWidth_13)); }
	inline bool get_m_ChildControlWidth_13() const { return ___m_ChildControlWidth_13; }
	inline bool* get_address_of_m_ChildControlWidth_13() { return &___m_ChildControlWidth_13; }
	inline void set_m_ChildControlWidth_13(bool value)
	{
		___m_ChildControlWidth_13 = value;
	}

	inline static int32_t get_offset_of_m_ChildControlHeight_14() { return static_cast<int32_t>(offsetof(HorizontalOrVerticalLayoutGroup_t1968298610, ___m_ChildControlHeight_14)); }
	inline bool get_m_ChildControlHeight_14() const { return ___m_ChildControlHeight_14; }
	inline bool* get_address_of_m_ChildControlHeight_14() { return &___m_ChildControlHeight_14; }
	inline void set_m_ChildControlHeight_14(bool value)
	{
		___m_ChildControlHeight_14 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HORIZONTALORVERTICALLAYOUTGROUP_T1968298610_H
#ifndef POSITIONASUV1_T1102546563_H
#define POSITIONASUV1_T1102546563_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.PositionAsUV1
struct  PositionAsUV1_t1102546563  : public BaseMeshEffect_t1728560551
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // POSITIONASUV1_T1102546563_H
#ifndef HORIZONTALLAYOUTGROUP_T2875670365_H
#define HORIZONTALLAYOUTGROUP_T2875670365_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.HorizontalLayoutGroup
struct  HorizontalLayoutGroup_t2875670365  : public HorizontalOrVerticalLayoutGroup_t1968298610
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HORIZONTALLAYOUTGROUP_T2875670365_H
#ifndef OUTLINE_T1417504278_H
#define OUTLINE_T1417504278_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Outline
struct  Outline_t1417504278  : public Shadow_t4269599528
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OUTLINE_T1417504278_H
#ifndef VERTICALLAYOUTGROUP_T2468316403_H
#define VERTICALLAYOUTGROUP_T2468316403_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.VerticalLayoutGroup
struct  VerticalLayoutGroup_t2468316403  : public HorizontalOrVerticalLayoutGroup_t1968298610
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VERTICALLAYOUTGROUP_T2468316403_H





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1900 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1901 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1902 = { sizeof (RectangularVertexClipper_t3349113845), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1902[2] = 
{
	RectangularVertexClipper_t3349113845::get_offset_of_m_WorldCorners_0(),
	RectangularVertexClipper_t3349113845::get_offset_of_m_CanvasCorners_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1903 = { sizeof (AspectRatioFitter_t3114550109), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1903[4] = 
{
	AspectRatioFitter_t3114550109::get_offset_of_m_AspectMode_2(),
	AspectRatioFitter_t3114550109::get_offset_of_m_AspectRatio_3(),
	AspectRatioFitter_t3114550109::get_offset_of_m_Rect_4(),
	AspectRatioFitter_t3114550109::get_offset_of_m_Tracker_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1904 = { sizeof (AspectMode_t1166448724)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1904[6] = 
{
	AspectMode_t1166448724::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1905 = { sizeof (CanvasScaler_t2574720772), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1905[14] = 
{
	CanvasScaler_t2574720772::get_offset_of_m_UiScaleMode_2(),
	CanvasScaler_t2574720772::get_offset_of_m_ReferencePixelsPerUnit_3(),
	CanvasScaler_t2574720772::get_offset_of_m_ScaleFactor_4(),
	CanvasScaler_t2574720772::get_offset_of_m_ReferenceResolution_5(),
	CanvasScaler_t2574720772::get_offset_of_m_ScreenMatchMode_6(),
	CanvasScaler_t2574720772::get_offset_of_m_MatchWidthOrHeight_7(),
	0,
	CanvasScaler_t2574720772::get_offset_of_m_PhysicalUnit_9(),
	CanvasScaler_t2574720772::get_offset_of_m_FallbackScreenDPI_10(),
	CanvasScaler_t2574720772::get_offset_of_m_DefaultSpriteDPI_11(),
	CanvasScaler_t2574720772::get_offset_of_m_DynamicPixelsPerUnit_12(),
	CanvasScaler_t2574720772::get_offset_of_m_Canvas_13(),
	CanvasScaler_t2574720772::get_offset_of_m_PrevScaleFactor_14(),
	CanvasScaler_t2574720772::get_offset_of_m_PrevReferencePixelsPerUnit_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1906 = { sizeof (ScaleMode_t987318053)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1906[4] = 
{
	ScaleMode_t987318053::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1907 = { sizeof (ScreenMatchMode_t1916789528)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1907[4] = 
{
	ScreenMatchMode_t1916789528::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1908 = { sizeof (Unit_t3220761768)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1908[6] = 
{
	Unit_t3220761768::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1909 = { sizeof (ContentSizeFitter_t1325211874), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1909[4] = 
{
	ContentSizeFitter_t1325211874::get_offset_of_m_HorizontalFit_2(),
	ContentSizeFitter_t1325211874::get_offset_of_m_VerticalFit_3(),
	ContentSizeFitter_t1325211874::get_offset_of_m_Rect_4(),
	ContentSizeFitter_t1325211874::get_offset_of_m_Tracker_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1910 = { sizeof (FitMode_t4030874534)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1910[4] = 
{
	FitMode_t4030874534::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1911 = { sizeof (GridLayoutGroup_t1515633077), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1911[6] = 
{
	GridLayoutGroup_t1515633077::get_offset_of_m_StartCorner_10(),
	GridLayoutGroup_t1515633077::get_offset_of_m_StartAxis_11(),
	GridLayoutGroup_t1515633077::get_offset_of_m_CellSize_12(),
	GridLayoutGroup_t1515633077::get_offset_of_m_Spacing_13(),
	GridLayoutGroup_t1515633077::get_offset_of_m_Constraint_14(),
	GridLayoutGroup_t1515633077::get_offset_of_m_ConstraintCount_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1912 = { sizeof (Corner_t1077473318)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1912[5] = 
{
	Corner_t1077473318::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1913 = { sizeof (Axis_t1431825778)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1913[3] = 
{
	Axis_t1431825778::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1914 = { sizeof (Constraint_t3558160636)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1914[4] = 
{
	Constraint_t3558160636::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1915 = { sizeof (HorizontalLayoutGroup_t2875670365), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1916 = { sizeof (HorizontalOrVerticalLayoutGroup_t1968298610), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1916[5] = 
{
	HorizontalOrVerticalLayoutGroup_t1968298610::get_offset_of_m_Spacing_10(),
	HorizontalOrVerticalLayoutGroup_t1968298610::get_offset_of_m_ChildForceExpandWidth_11(),
	HorizontalOrVerticalLayoutGroup_t1968298610::get_offset_of_m_ChildForceExpandHeight_12(),
	HorizontalOrVerticalLayoutGroup_t1968298610::get_offset_of_m_ChildControlWidth_13(),
	HorizontalOrVerticalLayoutGroup_t1968298610::get_offset_of_m_ChildControlHeight_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1917 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1918 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1919 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1920 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1921 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1922 = { sizeof (LayoutElement_t2808691390), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1922[8] = 
{
	LayoutElement_t2808691390::get_offset_of_m_IgnoreLayout_2(),
	LayoutElement_t2808691390::get_offset_of_m_MinWidth_3(),
	LayoutElement_t2808691390::get_offset_of_m_MinHeight_4(),
	LayoutElement_t2808691390::get_offset_of_m_PreferredWidth_5(),
	LayoutElement_t2808691390::get_offset_of_m_PreferredHeight_6(),
	LayoutElement_t2808691390::get_offset_of_m_FlexibleWidth_7(),
	LayoutElement_t2808691390::get_offset_of_m_FlexibleHeight_8(),
	LayoutElement_t2808691390::get_offset_of_m_LayoutPriority_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1923 = { sizeof (LayoutGroup_t3962498969), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1923[8] = 
{
	LayoutGroup_t3962498969::get_offset_of_m_Padding_2(),
	LayoutGroup_t3962498969::get_offset_of_m_ChildAlignment_3(),
	LayoutGroup_t3962498969::get_offset_of_m_Rect_4(),
	LayoutGroup_t3962498969::get_offset_of_m_Tracker_5(),
	LayoutGroup_t3962498969::get_offset_of_m_TotalMinSize_6(),
	LayoutGroup_t3962498969::get_offset_of_m_TotalPreferredSize_7(),
	LayoutGroup_t3962498969::get_offset_of_m_TotalFlexibleSize_8(),
	LayoutGroup_t3962498969::get_offset_of_m_RectChildren_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1924 = { sizeof (U3CDelayedSetDirtyU3Ec__Iterator0_t3228926346), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1924[4] = 
{
	U3CDelayedSetDirtyU3Ec__Iterator0_t3228926346::get_offset_of_rectTransform_0(),
	U3CDelayedSetDirtyU3Ec__Iterator0_t3228926346::get_offset_of_U24current_1(),
	U3CDelayedSetDirtyU3Ec__Iterator0_t3228926346::get_offset_of_U24disposing_2(),
	U3CDelayedSetDirtyU3Ec__Iterator0_t3228926346::get_offset_of_U24PC_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1925 = { sizeof (LayoutRebuilder_t2155218138), -1, sizeof(LayoutRebuilder_t2155218138_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1925[9] = 
{
	LayoutRebuilder_t2155218138::get_offset_of_m_ToRebuild_0(),
	LayoutRebuilder_t2155218138::get_offset_of_m_CachedHashFromTransform_1(),
	LayoutRebuilder_t2155218138_StaticFields::get_offset_of_s_Rebuilders_2(),
	LayoutRebuilder_t2155218138_StaticFields::get_offset_of_U3CU3Ef__mgU24cache0_3(),
	LayoutRebuilder_t2155218138_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_4(),
	LayoutRebuilder_t2155218138_StaticFields::get_offset_of_U3CU3Ef__amU24cache1_5(),
	LayoutRebuilder_t2155218138_StaticFields::get_offset_of_U3CU3Ef__amU24cache2_6(),
	LayoutRebuilder_t2155218138_StaticFields::get_offset_of_U3CU3Ef__amU24cache3_7(),
	LayoutRebuilder_t2155218138_StaticFields::get_offset_of_U3CU3Ef__amU24cache4_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1926 = { sizeof (LayoutUtility_t4076838048), -1, sizeof(LayoutUtility_t4076838048_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1926[8] = 
{
	LayoutUtility_t4076838048_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_0(),
	LayoutUtility_t4076838048_StaticFields::get_offset_of_U3CU3Ef__amU24cache1_1(),
	LayoutUtility_t4076838048_StaticFields::get_offset_of_U3CU3Ef__amU24cache2_2(),
	LayoutUtility_t4076838048_StaticFields::get_offset_of_U3CU3Ef__amU24cache3_3(),
	LayoutUtility_t4076838048_StaticFields::get_offset_of_U3CU3Ef__amU24cache4_4(),
	LayoutUtility_t4076838048_StaticFields::get_offset_of_U3CU3Ef__amU24cache5_5(),
	LayoutUtility_t4076838048_StaticFields::get_offset_of_U3CU3Ef__amU24cache6_6(),
	LayoutUtility_t4076838048_StaticFields::get_offset_of_U3CU3Ef__amU24cache7_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1927 = { sizeof (VerticalLayoutGroup_t2468316403), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1928 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1929 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable1929[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1930 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable1930[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1931 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable1931[4] = 
{
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1932 = { sizeof (ReflectionMethodsCache_t3343836395), -1, sizeof(ReflectionMethodsCache_t3343836395_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1932[7] = 
{
	ReflectionMethodsCache_t3343836395::get_offset_of_raycast3D_0(),
	ReflectionMethodsCache_t3343836395::get_offset_of_raycast3DAll_1(),
	ReflectionMethodsCache_t3343836395::get_offset_of_raycast2D_2(),
	ReflectionMethodsCache_t3343836395::get_offset_of_getRayIntersectionAll_3(),
	ReflectionMethodsCache_t3343836395::get_offset_of_getRayIntersectionAllNonAlloc_4(),
	ReflectionMethodsCache_t3343836395::get_offset_of_getRaycastNonAlloc_5(),
	ReflectionMethodsCache_t3343836395_StaticFields::get_offset_of_s_ReflectionMethodsCache_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1933 = { sizeof (Raycast3DCallback_t3928470916), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1934 = { sizeof (Raycast2DCallback_t2260664863), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1935 = { sizeof (RaycastAllCallback_t3435657708), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1936 = { sizeof (GetRayIntersectionAllCallback_t2213949596), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1937 = { sizeof (GetRayIntersectionAllNonAllocCallback_t3246763936), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1938 = { sizeof (GetRaycastNonAllocCallback_t1074830945), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1939 = { sizeof (VertexHelper_t385374196), -1, sizeof(VertexHelper_t385374196_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1939[11] = 
{
	VertexHelper_t385374196::get_offset_of_m_Positions_0(),
	VertexHelper_t385374196::get_offset_of_m_Colors_1(),
	VertexHelper_t385374196::get_offset_of_m_Uv0S_2(),
	VertexHelper_t385374196::get_offset_of_m_Uv1S_3(),
	VertexHelper_t385374196::get_offset_of_m_Uv2S_4(),
	VertexHelper_t385374196::get_offset_of_m_Uv3S_5(),
	VertexHelper_t385374196::get_offset_of_m_Normals_6(),
	VertexHelper_t385374196::get_offset_of_m_Tangents_7(),
	VertexHelper_t385374196::get_offset_of_m_Indices_8(),
	VertexHelper_t385374196_StaticFields::get_offset_of_s_DefaultTangent_9(),
	VertexHelper_t385374196_StaticFields::get_offset_of_s_DefaultNormal_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1940 = { sizeof (BaseVertexEffect_t2504093552), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1941 = { sizeof (BaseMeshEffect_t1728560551), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1941[1] = 
{
	BaseMeshEffect_t1728560551::get_offset_of_m_Graphic_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1942 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1943 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1944 = { sizeof (Outline_t1417504278), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1945 = { sizeof (PositionAsUV1_t1102546563), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1946 = { sizeof (Shadow_t4269599528), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1946[4] = 
{
	Shadow_t4269599528::get_offset_of_m_EffectColor_3(),
	Shadow_t4269599528::get_offset_of_m_EffectDistance_4(),
	Shadow_t4269599528::get_offset_of_m_UseGraphicAlpha_5(),
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1947 = { sizeof (U3CPrivateImplementationDetailsU3E_t1486305141), -1, sizeof(U3CPrivateImplementationDetailsU3E_t1486305141_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1947[1] = 
{
	U3CPrivateImplementationDetailsU3E_t1486305141_StaticFields::get_offset_of_U24fieldU2D7BBE37982E6C057ED87163CAFC7FD6E5E42EEA46_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1948 = { sizeof (U24ArrayTypeU3D12_t1568637717)+ sizeof (RuntimeObject), sizeof(U24ArrayTypeU3D12_t1568637717 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1949 = { sizeof (U3CModuleU3E_t3783534238), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1950 = { sizeof (AnalyticsTracker_t2191537572), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1950[5] = 
{
	AnalyticsTracker_t2191537572::get_offset_of_m_EventName_2(),
	AnalyticsTracker_t2191537572::get_offset_of_m_Dict_3(),
	AnalyticsTracker_t2191537572::get_offset_of_m_PrevDictHash_4(),
	AnalyticsTracker_t2191537572::get_offset_of_m_TrackableProperty_5(),
	AnalyticsTracker_t2191537572::get_offset_of_m_Trigger_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1951 = { sizeof (Trigger_t1068911718)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1951[8] = 
{
	Trigger_t1068911718::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1952 = { sizeof (TrackableProperty_t1304606600), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1952[2] = 
{
	0,
	TrackableProperty_t1304606600::get_offset_of_m_Fields_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1953 = { sizeof (FieldWithTarget_t2256174789), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1953[6] = 
{
	FieldWithTarget_t2256174789::get_offset_of_m_ParamName_0(),
	FieldWithTarget_t2256174789::get_offset_of_m_Target_1(),
	FieldWithTarget_t2256174789::get_offset_of_m_FieldPath_2(),
	FieldWithTarget_t2256174789::get_offset_of_m_TypeString_3(),
	FieldWithTarget_t2256174789::get_offset_of_m_DoStatic_4(),
	FieldWithTarget_t2256174789::get_offset_of_m_StaticString_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1954 = { sizeof (U3CModuleU3E_t3783534239), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1955 = { sizeof (iTween_t488923914), -1, sizeof(iTween_t488923914_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1955[39] = 
{
	iTween_t488923914_StaticFields::get_offset_of_tweens_2(),
	iTween_t488923914_StaticFields::get_offset_of_cameraFade_3(),
	iTween_t488923914::get_offset_of_id_4(),
	iTween_t488923914::get_offset_of_type_5(),
	iTween_t488923914::get_offset_of_method_6(),
	iTween_t488923914::get_offset_of_easeType_7(),
	iTween_t488923914::get_offset_of_time_8(),
	iTween_t488923914::get_offset_of_delay_9(),
	iTween_t488923914::get_offset_of_loopType_10(),
	iTween_t488923914::get_offset_of_isRunning_11(),
	iTween_t488923914::get_offset_of_isPaused_12(),
	iTween_t488923914::get_offset_of__name_13(),
	iTween_t488923914::get_offset_of_runningTime_14(),
	iTween_t488923914::get_offset_of_percentage_15(),
	iTween_t488923914::get_offset_of_delayStarted_16(),
	iTween_t488923914::get_offset_of_kinematic_17(),
	iTween_t488923914::get_offset_of_isLocal_18(),
	iTween_t488923914::get_offset_of_loop_19(),
	iTween_t488923914::get_offset_of_reverse_20(),
	iTween_t488923914::get_offset_of_wasPaused_21(),
	iTween_t488923914::get_offset_of_physics_22(),
	iTween_t488923914::get_offset_of_tweenArguments_23(),
	iTween_t488923914::get_offset_of_space_24(),
	iTween_t488923914::get_offset_of_ease_25(),
	iTween_t488923914::get_offset_of_apply_26(),
	iTween_t488923914::get_offset_of_audioSource_27(),
	iTween_t488923914::get_offset_of_vector3s_28(),
	iTween_t488923914::get_offset_of_vector2s_29(),
	iTween_t488923914::get_offset_of_colors_30(),
	iTween_t488923914::get_offset_of_floats_31(),
	iTween_t488923914::get_offset_of_rects_32(),
	iTween_t488923914::get_offset_of_path_33(),
	iTween_t488923914::get_offset_of_preUpdate_34(),
	iTween_t488923914::get_offset_of_postUpdate_35(),
	iTween_t488923914::get_offset_of_namedcolorvalue_36(),
	iTween_t488923914::get_offset_of_lastRealTime_37(),
	iTween_t488923914::get_offset_of_useRealTime_38(),
	iTween_t488923914::get_offset_of_thisTransform_39(),
	iTween_t488923914_StaticFields::get_offset_of_U3CU3Ef__switchU24map0_40(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1956 = { sizeof (EasingFunction_t3676968174), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1957 = { sizeof (ApplyTween_t747394300), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1958 = { sizeof (EaseType_t818674011)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1958[34] = 
{
	EaseType_t818674011::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1959 = { sizeof (LoopType_t1490651981)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1959[4] = 
{
	LoopType_t1490651981::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1960 = { sizeof (NamedValueColor_t2874784184)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1960[5] = 
{
	NamedValueColor_t2874784184::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1961 = { sizeof (Defaults_t4204852305), -1, sizeof(Defaults_t4204852305_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1961[16] = 
{
	Defaults_t4204852305_StaticFields::get_offset_of_time_0(),
	Defaults_t4204852305_StaticFields::get_offset_of_delay_1(),
	Defaults_t4204852305_StaticFields::get_offset_of_namedColorValue_2(),
	Defaults_t4204852305_StaticFields::get_offset_of_loopType_3(),
	Defaults_t4204852305_StaticFields::get_offset_of_easeType_4(),
	Defaults_t4204852305_StaticFields::get_offset_of_lookSpeed_5(),
	Defaults_t4204852305_StaticFields::get_offset_of_isLocal_6(),
	Defaults_t4204852305_StaticFields::get_offset_of_space_7(),
	Defaults_t4204852305_StaticFields::get_offset_of_orientToPath_8(),
	Defaults_t4204852305_StaticFields::get_offset_of_color_9(),
	Defaults_t4204852305_StaticFields::get_offset_of_updateTimePercentage_10(),
	Defaults_t4204852305_StaticFields::get_offset_of_updateTime_11(),
	Defaults_t4204852305_StaticFields::get_offset_of_cameraFadeDepth_12(),
	Defaults_t4204852305_StaticFields::get_offset_of_lookAhead_13(),
	Defaults_t4204852305_StaticFields::get_offset_of_useRealTime_14(),
	Defaults_t4204852305_StaticFields::get_offset_of_up_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1962 = { sizeof (CRSpline_t4177960625), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1962[1] = 
{
	CRSpline_t4177960625::get_offset_of_pts_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1963 = { sizeof (U3CTweenDelayU3Ec__Iterator0_t1889752800), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1963[4] = 
{
	U3CTweenDelayU3Ec__Iterator0_t1889752800::get_offset_of_U24this_0(),
	U3CTweenDelayU3Ec__Iterator0_t1889752800::get_offset_of_U24current_1(),
	U3CTweenDelayU3Ec__Iterator0_t1889752800::get_offset_of_U24disposing_2(),
	U3CTweenDelayU3Ec__Iterator0_t1889752800::get_offset_of_U24PC_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1964 = { sizeof (U3CTweenRestartU3Ec__Iterator1_t3903815285), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1964[4] = 
{
	U3CTweenRestartU3Ec__Iterator1_t3903815285::get_offset_of_U24this_0(),
	U3CTweenRestartU3Ec__Iterator1_t3903815285::get_offset_of_U24current_1(),
	U3CTweenRestartU3Ec__Iterator1_t3903815285::get_offset_of_U24disposing_2(),
	U3CTweenRestartU3Ec__Iterator1_t3903815285::get_offset_of_U24PC_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1965 = { sizeof (U3CStartU3Ec__Iterator2_t1700299370), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1965[4] = 
{
	U3CStartU3Ec__Iterator2_t1700299370::get_offset_of_U24this_0(),
	U3CStartU3Ec__Iterator2_t1700299370::get_offset_of_U24current_1(),
	U3CStartU3Ec__Iterator2_t1700299370::get_offset_of_U24disposing_2(),
	U3CStartU3Ec__Iterator2_t1700299370::get_offset_of_U24PC_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1966 = { sizeof (MoveSample_t4188468541), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1967 = { sizeof (RotateSample_t3367094513), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1968 = { sizeof (SampleInfo_t4001993458), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1969 = { sizeof (U3CModuleU3E_t3783534240), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1970 = { sizeof (ChildTestScript_t4144785889), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1970[1] = 
{
	ChildTestScript_t4144785889::get_offset_of_show_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1971 = { sizeof (ArrayExtensionMethods_t1733224102), -1, sizeof(ArrayExtensionMethods_t1733224102_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1971[3] = 
{
	ArrayExtensionMethods_t1733224102_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_0(),
	ArrayExtensionMethods_t1733224102_StaticFields::get_offset_of_U3CU3Ef__amU24cache1_1(),
	ArrayExtensionMethods_t1733224102_StaticFields::get_offset_of_U3CU3Ef__amU24cache2_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1972 = { sizeof (U3CFindByIdU3Ec__AnonStorey0_t1513557713), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1972[1] = 
{
	U3CFindByIdU3Ec__AnonStorey0_t1513557713::get_offset_of_id_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1973 = { sizeof (U3CFindByIdU3Ec__AnonStorey1_t1513557712), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1973[1] = 
{
	U3CFindByIdU3Ec__AnonStorey1_t1513557712::get_offset_of_id_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1974 = { sizeof (U3CFindByIdU3Ec__AnonStorey2_t1513557715), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1974[1] = 
{
	U3CFindByIdU3Ec__AnonStorey2_t1513557715::get_offset_of_id_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1975 = { sizeof (ComponentExtensionMethods_t619716316), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1976 = { sizeof (CubismDrawable_t2709211313), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1976[2] = 
{
	CubismDrawable_t2709211313::get_offset_of_U3CUnmanagedDrawablesU3Ek__BackingField_2(),
	CubismDrawable_t2709211313::get_offset_of__unmanagedIndex_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1977 = { sizeof (CubismDynamicDrawableData_t537076974), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1977[5] = 
{
	CubismDynamicDrawableData_t537076974::get_offset_of_U3CFlagsU3Ek__BackingField_0(),
	CubismDynamicDrawableData_t537076974::get_offset_of_U3COpacityU3Ek__BackingField_1(),
	CubismDynamicDrawableData_t537076974::get_offset_of_U3CDrawOrderU3Ek__BackingField_2(),
	CubismDynamicDrawableData_t537076974::get_offset_of_U3CRenderOrderU3Ek__BackingField_3(),
	CubismDynamicDrawableData_t537076974::get_offset_of_U3CVertexPositionsU3Ek__BackingField_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1978 = { sizeof (CubismLogging_t979420036), -1, sizeof(CubismLogging_t979420036_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1978[2] = 
{
	CubismLogging_t979420036_StaticFields::get_offset_of_U3CLogDelegateU3Ek__BackingField_0(),
	CubismLogging_t979420036_StaticFields::get_offset_of_U3CU3Ef__mgU24cache0_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1979 = { sizeof (UnmanagedLogDelegate_t742859164), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1980 = { sizeof (CubismMoc_t1358225636), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1980[3] = 
{
	CubismMoc_t1358225636::get_offset_of__bytes_2(),
	CubismMoc_t1358225636::get_offset_of_U3CUnmanagedMocU3Ek__BackingField_3(),
	CubismMoc_t1358225636::get_offset_of_U3CReferenceCountU3Ek__BackingField_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1981 = { sizeof (CubismModel_t2381008000), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1981[8] = 
{
	CubismModel_t2381008000::get_offset_of_OnDynamicDrawableData_2(),
	CubismModel_t2381008000::get_offset_of__moc_3(),
	CubismModel_t2381008000::get_offset_of_U3CTaskableModelU3Ek__BackingField_4(),
	CubismModel_t2381008000::get_offset_of__parameters_5(),
	CubismModel_t2381008000::get_offset_of__parts_6(),
	CubismModel_t2381008000::get_offset_of__drawables_7(),
	CubismModel_t2381008000::get_offset_of_U3CWasJustEnabledU3Ek__BackingField_8(),
	CubismModel_t2381008000::get_offset_of_U3CLastTickU3Ek__BackingField_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1982 = { sizeof (DynamicDrawableDataHandler_t2657212100), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1983 = { sizeof (CubismParameter_t3864677546), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1983[3] = 
{
	CubismParameter_t3864677546::get_offset_of_U3CUnmanagedParametersU3Ek__BackingField_2(),
	CubismParameter_t3864677546::get_offset_of__unmanagedIndex_3(),
	CubismParameter_t3864677546::get_offset_of_Value_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1984 = { sizeof (CubismPart_t3829010958), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1984[3] = 
{
	CubismPart_t3829010958::get_offset_of_U3CUnmanagedPartsU3Ek__BackingField_2(),
	CubismPart_t3829010958::get_offset_of__unmanagedIndex_3(),
	CubismPart_t3829010958::get_offset_of_Opacity_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1985 = { sizeof (CubismTaskableModel_t1216042951), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1985[6] = 
{
	CubismTaskableModel_t1216042951::get_offset_of_U3CUnmanagedModelU3Ek__BackingField_0(),
	CubismTaskableModel_t1216042951::get_offset_of_U3CMocU3Ek__BackingField_1(),
	CubismTaskableModel_t1216042951::get_offset_of__dynamicDrawableData_2(),
	CubismTaskableModel_t1216042951::get_offset_of_U3CShouldReleaseUnmanagedU3Ek__BackingField_3(),
	CubismTaskableModel_t1216042951::get_offset_of_U3CLockU3Ek__BackingField_4(),
	CubismTaskableModel_t1216042951::get_offset_of_U3CStateU3Ek__BackingField_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1986 = { sizeof (TaskState_t3546624310)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1986[5] = 
{
	TaskState_t3546624310::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1987 = { sizeof (CubismTaskQueue_t1273848959), -1, sizeof(CubismTaskQueue_t1273848959_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1987[1] = 
{
	CubismTaskQueue_t1273848959_StaticFields::get_offset_of_OnTask_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1988 = { sizeof (CubismTaskHandler_t1959870214), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1989 = { sizeof (GameObjectExtensionMethods_t2024879434), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1990 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1991 = { sizeof (ByteExtensionMethods_t2486717825), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1992 = { sizeof (CubismCoreDll_t2084904188), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1992[12] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1993 = { sizeof (CubismUnmanagedIntArrayView_t965365490), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1993[2] = 
{
	CubismUnmanagedIntArrayView_t965365490::get_offset_of_U3CLengthU3Ek__BackingField_0(),
	CubismUnmanagedIntArrayView_t965365490::get_offset_of_U3CAddressU3Ek__BackingField_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1994 = { sizeof (CubismUnmanagedUshortArrayView_t2529819058), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1994[2] = 
{
	CubismUnmanagedUshortArrayView_t2529819058::get_offset_of_U3CLengthU3Ek__BackingField_0(),
	CubismUnmanagedUshortArrayView_t2529819058::get_offset_of_U3CAddressU3Ek__BackingField_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1995 = { sizeof (CubismUnmanagedByteArrayView_t2855206621), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1995[2] = 
{
	CubismUnmanagedByteArrayView_t2855206621::get_offset_of_U3CLengthU3Ek__BackingField_0(),
	CubismUnmanagedByteArrayView_t2855206621::get_offset_of_U3CAddressU3Ek__BackingField_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1996 = { sizeof (CubismUnmanagedFloatArrayView_t1153227075), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1996[2] = 
{
	CubismUnmanagedFloatArrayView_t1153227075::get_offset_of_U3CLengthU3Ek__BackingField_0(),
	CubismUnmanagedFloatArrayView_t1153227075::get_offset_of_U3CAddressU3Ek__BackingField_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1997 = { sizeof (CubismUnmanagedDrawables_t771654660), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1997[16] = 
{
	CubismUnmanagedDrawables_t771654660::get_offset_of_U3CCountU3Ek__BackingField_0(),
	CubismUnmanagedDrawables_t771654660::get_offset_of_U3CIdsU3Ek__BackingField_1(),
	CubismUnmanagedDrawables_t771654660::get_offset_of_U3CConstantFlagsU3Ek__BackingField_2(),
	CubismUnmanagedDrawables_t771654660::get_offset_of_U3CDynamicFlagsU3Ek__BackingField_3(),
	CubismUnmanagedDrawables_t771654660::get_offset_of_U3CTextureIndicesU3Ek__BackingField_4(),
	CubismUnmanagedDrawables_t771654660::get_offset_of_U3CDrawOrdersU3Ek__BackingField_5(),
	CubismUnmanagedDrawables_t771654660::get_offset_of_U3CRenderOrdersU3Ek__BackingField_6(),
	CubismUnmanagedDrawables_t771654660::get_offset_of_U3COpacitiesU3Ek__BackingField_7(),
	CubismUnmanagedDrawables_t771654660::get_offset_of_U3CMaskCountsU3Ek__BackingField_8(),
	CubismUnmanagedDrawables_t771654660::get_offset_of_U3CMasksU3Ek__BackingField_9(),
	CubismUnmanagedDrawables_t771654660::get_offset_of_U3CVertexCountsU3Ek__BackingField_10(),
	CubismUnmanagedDrawables_t771654660::get_offset_of_U3CVertexPositionsU3Ek__BackingField_11(),
	CubismUnmanagedDrawables_t771654660::get_offset_of_U3CVertexUvsU3Ek__BackingField_12(),
	CubismUnmanagedDrawables_t771654660::get_offset_of_U3CIndexCountsU3Ek__BackingField_13(),
	CubismUnmanagedDrawables_t771654660::get_offset_of_U3CIndicesU3Ek__BackingField_14(),
	CubismUnmanagedDrawables_t771654660::get_offset_of_U3CModelPtrU3Ek__BackingField_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1998 = { sizeof (CubismUnmanagedMemory_t1337132358), -1, sizeof(CubismUnmanagedMemory_t1337132358_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1998[1] = 
{
	CubismUnmanagedMemory_t1337132358_StaticFields::get_offset_of_U3CAllocationsU3Ek__BackingField_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1999 = { sizeof (AllocationItem_t2929434092)+ sizeof (RuntimeObject), sizeof(AllocationItem_t2929434092 ), 0, 0 };
extern const int32_t g_FieldOffsetTable1999[2] = 
{
	AllocationItem_t2929434092::get_offset_of_UnalignedAddress_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	AllocationItem_t2929434092::get_offset_of_AlignedAddress_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
