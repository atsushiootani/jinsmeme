﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "object-internals.h"

// System.String
struct String_t;
// Utage.WWWEx
struct WWWEx_t1775400116;
// System.Action
struct Action_t3226471752;
// Utage.AssetFileUtage
struct AssetFileUtage_t3568203916;
// Utage.AssetFileUtage/<LoadAssetBundleAsync>c__Iterator3/<LoadAssetBundleAsync>c__AnonStorey6
struct U3CLoadAssetBundleAsyncU3Ec__AnonStorey6_t1993755113;
// UnityEngine.ResourceRequest
struct ResourceRequest_t2560315377;
// Utage.AssetFileUtage/<LoadAsync>c__Iterator0
struct U3CLoadAsyncU3Ec__Iterator0_t2893323214;
// UnityEngine.AssetBundle
struct AssetBundle_t2054978754;
// Utage.AssetFileUtage/<LoadAssetBundleAsync>c__Iterator3
struct U3CLoadAssetBundleAsyncU3Ec__Iterator3_t2280417849;
// UnityEngine.Object
struct Object_t1021602117;
// System.Collections.Generic.List`1<Utage.ExpressionToken>
struct List_1_t2114587879;
// UnityEngine.AssetBundleRequest
struct AssetBundleRequest_t2674559435;
// UnityEngine.Object[]
struct ObjectU5BU5D_t4217747464;
// Utage.AssetBundleInfoManager
struct AssetBundleInfoManager_t223439115;
// Utage.AssetFileBase
struct AssetFileBase_t4101360697;
// Utage.AssetFileManager/<LoadAsync>c__Iterator2
struct U3CLoadAsyncU3Ec__Iterator2_t109345333;
// Utage.AssetFileManager
struct AssetFileManager_t1095395563;
// Utage.Compression/Node[]
struct NodeU5BU5D_t2732557534;
// System.Int32[]
struct Int32U5BU5D_t3030399641;
// Utage.AssetFileManager/<LoadAsync>c__Iterator2/<LoadAsync>c__AnonStorey5
struct U3CLoadAsyncU3Ec__AnonStorey5_t3302198958;
// Utage.AssetFileUtage/<LoadAsync>c__Iterator0/<LoadAsync>c__AnonStorey5
struct U3CLoadAsyncU3Ec__AnonStorey5_t4158068117;
// UnityEngine.Texture2D
struct Texture2D_t3542995729;
// UnityEngine.AudioClip
struct AudioClip_t1932558630;
// UnityEngine.TextAsset
struct TextAsset_t3973159845;
// System.Action`1<Utage.AssetFile>
struct Action_1_t3184812638;
// UnityEngine.WWW
struct WWW_t2919945039;
// System.Action`1<UnityEngine.WWW>
struct Action_1_t2721744421;
// System.Collections.Generic.List`1<Utage.MiniAnimationData/Data>
struct List_1_t4284043194;
// UnityEngine.RectTransform
struct RectTransform_t3349966182;
// Utage.LinkTransform
struct LinkTransform_t2861363074;
// System.Action`1<System.Single>
struct Action_1_t1878309314;
// Utage.CurveAnimation
struct CurveAnimation_t2521618615;
// UnityEngine.Events.InvokableCallList
struct InvokableCallList_t2295673753;
// UnityEngine.Events.PersistentCallGroup
struct PersistentCallGroup_t339478082;
// UnityEngine.Events.UnityAction
struct UnityAction_t4025899511;
// System.Object[]
struct ObjectU5BU5D_t3614634134;
// System.Void
struct Void_t1841601450;
// System.Char[]
struct CharU5BU5D_t1328083999;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// System.DelegateData
struct DelegateData_t1572802995;
// System.Collections.Generic.List`1<System.String>
struct List_1_t1398341365;
// System.Collections.Generic.List`1<Utage.AssetFileSetting>
struct List_1_t1239078948;
// Utage.ExpressionToken[]
struct ExpressionTokenU5BU5D_t142795834;
// System.Collections.Generic.Dictionary`2<System.String,System.Int32>
struct Dictionary_2_t3986656710;
// Utage.AssetFileManagerSettings
struct AssetFileManagerSettings_t1473658718;
// Utage.AssetBundleInfo
struct AssetBundleInfo_t1249246306;
// Utage.AssetFileSetting
struct AssetFileSetting_t1869957816;
// Utage.AssetFileInfo
struct AssetFileInfo_t1386031564;
// Utage.IAssetFileSettingData
struct IAssetFileSettingData_t3884760591;
// System.Collections.Generic.HashSet`1<System.Object>
struct HashSet_1_t1022910149;
// System.Predicate`1<System.Object>
struct Predicate_1_t1132419410;
// System.Action`1<Utage.WWWEx>
struct Action_1_t1577199498;
// System.Byte[]
struct ByteU5BU5D_t3397334013;
// System.IAsyncResult
struct IAsyncResult_t1999651008;
// System.AsyncCallback
struct AsyncCallback_t163412349;
// Utage.StaticAsset
struct StaticAsset_t1112757920;
// Utage.AssetFile
struct AssetFile_t3383013256;
// System.Func`3<System.Byte[],System.Byte[],System.Byte[]>
struct Func_3_t1164674077;
// System.Action`4<System.Byte[],System.Byte[],System.Int32,System.Int32>
struct Action_4_t3159657519;
// System.Int16[]
struct Int16U5BU5D_t3104283263;
// System.Single[]
struct SingleU5BU5D_t577127397;
// Utage.CustomLoadManager/FindAsset
struct FindAsset_t4163870574;
// System.Collections.Generic.Dictionary`2<System.String,Utage.AssetBundleInfo>
struct Dictionary_2_t3164025568;
// Utage.FileIOManager
struct FileIOManager_t855502573;
// Utage.MinMaxInt
struct MinMaxInt_t3259591635;
// Utage.AssetFileDummyOnLoadError
struct AssetFileDummyOnLoadError_t419678579;
// System.Collections.Generic.List`1<Utage.AssetFileBase>
struct List_1_t3470481829;
// System.Collections.Generic.Dictionary`2<System.String,Utage.AssetFileBase>
struct Dictionary_2_t1721172663;
// Utage.CustomLoadManager
struct CustomLoadManager_t3941822120;
// Utage.StaticAssetManager
struct StaticAssetManager_t393950057;
// UnityEngine.Events.UnityEvent
struct UnityEvent_t408735097;
// System.Collections.Generic.Dictionary`2<UnityEngine.ParticleSystem,System.Single>
struct Dictionary_2_t3839890066;
// System.Collections.Generic.List`1<Utage.StaticAsset>
struct List_1_t481879052;
// UnityEngine.Transform
struct Transform_t3275118058;
// UnityEngine.AnimationCurve
struct AnimationCurve_t3306541151;
// Utage.CurveAnimationEvent
struct CurveAnimationEvent_t3593772963;
// UnityEngine.Coroutine
struct Coroutine_t2299508840;
// UnityEngine.UI.Graphic
struct Graphic_t2426225576;




#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
#ifndef NODE_T1298946215_H
#define NODE_T1298946215_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.Compression/Node
struct  Node_t1298946215  : public RuntimeObject
{
public:
	// System.Int32 Utage.Compression/Node::mNext
	int32_t ___mNext_0;
	// System.Int32 Utage.Compression/Node::mPrev
	int32_t ___mPrev_1;
	// System.Int32 Utage.Compression/Node::mPos
	int32_t ___mPos_2;

public:
	inline static int32_t get_offset_of_mNext_0() { return static_cast<int32_t>(offsetof(Node_t1298946215, ___mNext_0)); }
	inline int32_t get_mNext_0() const { return ___mNext_0; }
	inline int32_t* get_address_of_mNext_0() { return &___mNext_0; }
	inline void set_mNext_0(int32_t value)
	{
		___mNext_0 = value;
	}

	inline static int32_t get_offset_of_mPrev_1() { return static_cast<int32_t>(offsetof(Node_t1298946215, ___mPrev_1)); }
	inline int32_t get_mPrev_1() const { return ___mPrev_1; }
	inline int32_t* get_address_of_mPrev_1() { return &___mPrev_1; }
	inline void set_mPrev_1(int32_t value)
	{
		___mPrev_1 = value;
	}

	inline static int32_t get_offset_of_mPos_2() { return static_cast<int32_t>(offsetof(Node_t1298946215, ___mPos_2)); }
	inline int32_t get_mPos_2() const { return ___mPos_2; }
	inline int32_t* get_address_of_mPos_2() { return &___mPos_2; }
	inline void set_mPos_2(int32_t value)
	{
		___mPos_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NODE_T1298946215_H
#ifndef UTAGEEXTENSIONS_T58130528_H
#define UTAGEEXTENSIONS_T58130528_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UtageExtensions.UtageExtensions
struct  UtageExtensions_t58130528  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UTAGEEXTENSIONS_T58130528_H
#ifndef U3CLOADASSETBUNDLEASYNCU3EC__ITERATOR3_T2280417849_H
#define U3CLOADASSETBUNDLEASYNCU3EC__ITERATOR3_T2280417849_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.AssetFileUtage/<LoadAssetBundleAsync>c__Iterator3
struct  U3CLoadAssetBundleAsyncU3Ec__Iterator3_t2280417849  : public RuntimeObject
{
public:
	// System.String Utage.AssetFileUtage/<LoadAssetBundleAsync>c__Iterator3::path
	String_t* ___path_0;
	// Utage.WWWEx Utage.AssetFileUtage/<LoadAssetBundleAsync>c__Iterator3::<wwwEx>__0
	WWWEx_t1775400116 * ___U3CwwwExU3E__0_1;
	// System.Action Utage.AssetFileUtage/<LoadAssetBundleAsync>c__Iterator3::onComplete
	Action_t3226471752 * ___onComplete_2;
	// System.Action Utage.AssetFileUtage/<LoadAssetBundleAsync>c__Iterator3::onFailed
	Action_t3226471752 * ___onFailed_3;
	// Utage.AssetFileUtage Utage.AssetFileUtage/<LoadAssetBundleAsync>c__Iterator3::$this
	AssetFileUtage_t3568203916 * ___U24this_4;
	// System.Object Utage.AssetFileUtage/<LoadAssetBundleAsync>c__Iterator3::$current
	RuntimeObject * ___U24current_5;
	// System.Boolean Utage.AssetFileUtage/<LoadAssetBundleAsync>c__Iterator3::$disposing
	bool ___U24disposing_6;
	// System.Int32 Utage.AssetFileUtage/<LoadAssetBundleAsync>c__Iterator3::$PC
	int32_t ___U24PC_7;
	// Utage.AssetFileUtage/<LoadAssetBundleAsync>c__Iterator3/<LoadAssetBundleAsync>c__AnonStorey6 Utage.AssetFileUtage/<LoadAssetBundleAsync>c__Iterator3::$locvar0
	U3CLoadAssetBundleAsyncU3Ec__AnonStorey6_t1993755113 * ___U24locvar0_8;

public:
	inline static int32_t get_offset_of_path_0() { return static_cast<int32_t>(offsetof(U3CLoadAssetBundleAsyncU3Ec__Iterator3_t2280417849, ___path_0)); }
	inline String_t* get_path_0() const { return ___path_0; }
	inline String_t** get_address_of_path_0() { return &___path_0; }
	inline void set_path_0(String_t* value)
	{
		___path_0 = value;
		Il2CppCodeGenWriteBarrier((&___path_0), value);
	}

	inline static int32_t get_offset_of_U3CwwwExU3E__0_1() { return static_cast<int32_t>(offsetof(U3CLoadAssetBundleAsyncU3Ec__Iterator3_t2280417849, ___U3CwwwExU3E__0_1)); }
	inline WWWEx_t1775400116 * get_U3CwwwExU3E__0_1() const { return ___U3CwwwExU3E__0_1; }
	inline WWWEx_t1775400116 ** get_address_of_U3CwwwExU3E__0_1() { return &___U3CwwwExU3E__0_1; }
	inline void set_U3CwwwExU3E__0_1(WWWEx_t1775400116 * value)
	{
		___U3CwwwExU3E__0_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CwwwExU3E__0_1), value);
	}

	inline static int32_t get_offset_of_onComplete_2() { return static_cast<int32_t>(offsetof(U3CLoadAssetBundleAsyncU3Ec__Iterator3_t2280417849, ___onComplete_2)); }
	inline Action_t3226471752 * get_onComplete_2() const { return ___onComplete_2; }
	inline Action_t3226471752 ** get_address_of_onComplete_2() { return &___onComplete_2; }
	inline void set_onComplete_2(Action_t3226471752 * value)
	{
		___onComplete_2 = value;
		Il2CppCodeGenWriteBarrier((&___onComplete_2), value);
	}

	inline static int32_t get_offset_of_onFailed_3() { return static_cast<int32_t>(offsetof(U3CLoadAssetBundleAsyncU3Ec__Iterator3_t2280417849, ___onFailed_3)); }
	inline Action_t3226471752 * get_onFailed_3() const { return ___onFailed_3; }
	inline Action_t3226471752 ** get_address_of_onFailed_3() { return &___onFailed_3; }
	inline void set_onFailed_3(Action_t3226471752 * value)
	{
		___onFailed_3 = value;
		Il2CppCodeGenWriteBarrier((&___onFailed_3), value);
	}

	inline static int32_t get_offset_of_U24this_4() { return static_cast<int32_t>(offsetof(U3CLoadAssetBundleAsyncU3Ec__Iterator3_t2280417849, ___U24this_4)); }
	inline AssetFileUtage_t3568203916 * get_U24this_4() const { return ___U24this_4; }
	inline AssetFileUtage_t3568203916 ** get_address_of_U24this_4() { return &___U24this_4; }
	inline void set_U24this_4(AssetFileUtage_t3568203916 * value)
	{
		___U24this_4 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_4), value);
	}

	inline static int32_t get_offset_of_U24current_5() { return static_cast<int32_t>(offsetof(U3CLoadAssetBundleAsyncU3Ec__Iterator3_t2280417849, ___U24current_5)); }
	inline RuntimeObject * get_U24current_5() const { return ___U24current_5; }
	inline RuntimeObject ** get_address_of_U24current_5() { return &___U24current_5; }
	inline void set_U24current_5(RuntimeObject * value)
	{
		___U24current_5 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_5), value);
	}

	inline static int32_t get_offset_of_U24disposing_6() { return static_cast<int32_t>(offsetof(U3CLoadAssetBundleAsyncU3Ec__Iterator3_t2280417849, ___U24disposing_6)); }
	inline bool get_U24disposing_6() const { return ___U24disposing_6; }
	inline bool* get_address_of_U24disposing_6() { return &___U24disposing_6; }
	inline void set_U24disposing_6(bool value)
	{
		___U24disposing_6 = value;
	}

	inline static int32_t get_offset_of_U24PC_7() { return static_cast<int32_t>(offsetof(U3CLoadAssetBundleAsyncU3Ec__Iterator3_t2280417849, ___U24PC_7)); }
	inline int32_t get_U24PC_7() const { return ___U24PC_7; }
	inline int32_t* get_address_of_U24PC_7() { return &___U24PC_7; }
	inline void set_U24PC_7(int32_t value)
	{
		___U24PC_7 = value;
	}

	inline static int32_t get_offset_of_U24locvar0_8() { return static_cast<int32_t>(offsetof(U3CLoadAssetBundleAsyncU3Ec__Iterator3_t2280417849, ___U24locvar0_8)); }
	inline U3CLoadAssetBundleAsyncU3Ec__AnonStorey6_t1993755113 * get_U24locvar0_8() const { return ___U24locvar0_8; }
	inline U3CLoadAssetBundleAsyncU3Ec__AnonStorey6_t1993755113 ** get_address_of_U24locvar0_8() { return &___U24locvar0_8; }
	inline void set_U24locvar0_8(U3CLoadAssetBundleAsyncU3Ec__AnonStorey6_t1993755113 * value)
	{
		___U24locvar0_8 = value;
		Il2CppCodeGenWriteBarrier((&___U24locvar0_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CLOADASSETBUNDLEASYNCU3EC__ITERATOR3_T2280417849_H
#ifndef ASSETBUNDLEHELPER_T1847879106_H
#define ASSETBUNDLEHELPER_T1847879106_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.AssetBundleHelper
struct  AssetBundleHelper_t1847879106  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ASSETBUNDLEHELPER_T1847879106_H
#ifndef U3CLOADRESOURCEASYNCU3EC__ITERATOR2_T3762021256_H
#define U3CLOADRESOURCEASYNCU3EC__ITERATOR2_T3762021256_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.AssetFileUtage/<LoadResourceAsync>c__Iterator2
struct  U3CLoadResourceAsyncU3Ec__Iterator2_t3762021256  : public RuntimeObject
{
public:
	// System.String Utage.AssetFileUtage/<LoadResourceAsync>c__Iterator2::loadPath
	String_t* ___loadPath_0;
	// UnityEngine.ResourceRequest Utage.AssetFileUtage/<LoadResourceAsync>c__Iterator2::<request>__0
	ResourceRequest_t2560315377 * ___U3CrequestU3E__0_1;
	// System.Action Utage.AssetFileUtage/<LoadResourceAsync>c__Iterator2::onComplete
	Action_t3226471752 * ___onComplete_2;
	// System.Action Utage.AssetFileUtage/<LoadResourceAsync>c__Iterator2::onFailed
	Action_t3226471752 * ___onFailed_3;
	// Utage.AssetFileUtage Utage.AssetFileUtage/<LoadResourceAsync>c__Iterator2::$this
	AssetFileUtage_t3568203916 * ___U24this_4;
	// System.Object Utage.AssetFileUtage/<LoadResourceAsync>c__Iterator2::$current
	RuntimeObject * ___U24current_5;
	// System.Boolean Utage.AssetFileUtage/<LoadResourceAsync>c__Iterator2::$disposing
	bool ___U24disposing_6;
	// System.Int32 Utage.AssetFileUtage/<LoadResourceAsync>c__Iterator2::$PC
	int32_t ___U24PC_7;

public:
	inline static int32_t get_offset_of_loadPath_0() { return static_cast<int32_t>(offsetof(U3CLoadResourceAsyncU3Ec__Iterator2_t3762021256, ___loadPath_0)); }
	inline String_t* get_loadPath_0() const { return ___loadPath_0; }
	inline String_t** get_address_of_loadPath_0() { return &___loadPath_0; }
	inline void set_loadPath_0(String_t* value)
	{
		___loadPath_0 = value;
		Il2CppCodeGenWriteBarrier((&___loadPath_0), value);
	}

	inline static int32_t get_offset_of_U3CrequestU3E__0_1() { return static_cast<int32_t>(offsetof(U3CLoadResourceAsyncU3Ec__Iterator2_t3762021256, ___U3CrequestU3E__0_1)); }
	inline ResourceRequest_t2560315377 * get_U3CrequestU3E__0_1() const { return ___U3CrequestU3E__0_1; }
	inline ResourceRequest_t2560315377 ** get_address_of_U3CrequestU3E__0_1() { return &___U3CrequestU3E__0_1; }
	inline void set_U3CrequestU3E__0_1(ResourceRequest_t2560315377 * value)
	{
		___U3CrequestU3E__0_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CrequestU3E__0_1), value);
	}

	inline static int32_t get_offset_of_onComplete_2() { return static_cast<int32_t>(offsetof(U3CLoadResourceAsyncU3Ec__Iterator2_t3762021256, ___onComplete_2)); }
	inline Action_t3226471752 * get_onComplete_2() const { return ___onComplete_2; }
	inline Action_t3226471752 ** get_address_of_onComplete_2() { return &___onComplete_2; }
	inline void set_onComplete_2(Action_t3226471752 * value)
	{
		___onComplete_2 = value;
		Il2CppCodeGenWriteBarrier((&___onComplete_2), value);
	}

	inline static int32_t get_offset_of_onFailed_3() { return static_cast<int32_t>(offsetof(U3CLoadResourceAsyncU3Ec__Iterator2_t3762021256, ___onFailed_3)); }
	inline Action_t3226471752 * get_onFailed_3() const { return ___onFailed_3; }
	inline Action_t3226471752 ** get_address_of_onFailed_3() { return &___onFailed_3; }
	inline void set_onFailed_3(Action_t3226471752 * value)
	{
		___onFailed_3 = value;
		Il2CppCodeGenWriteBarrier((&___onFailed_3), value);
	}

	inline static int32_t get_offset_of_U24this_4() { return static_cast<int32_t>(offsetof(U3CLoadResourceAsyncU3Ec__Iterator2_t3762021256, ___U24this_4)); }
	inline AssetFileUtage_t3568203916 * get_U24this_4() const { return ___U24this_4; }
	inline AssetFileUtage_t3568203916 ** get_address_of_U24this_4() { return &___U24this_4; }
	inline void set_U24this_4(AssetFileUtage_t3568203916 * value)
	{
		___U24this_4 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_4), value);
	}

	inline static int32_t get_offset_of_U24current_5() { return static_cast<int32_t>(offsetof(U3CLoadResourceAsyncU3Ec__Iterator2_t3762021256, ___U24current_5)); }
	inline RuntimeObject * get_U24current_5() const { return ___U24current_5; }
	inline RuntimeObject ** get_address_of_U24current_5() { return &___U24current_5; }
	inline void set_U24current_5(RuntimeObject * value)
	{
		___U24current_5 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_5), value);
	}

	inline static int32_t get_offset_of_U24disposing_6() { return static_cast<int32_t>(offsetof(U3CLoadResourceAsyncU3Ec__Iterator2_t3762021256, ___U24disposing_6)); }
	inline bool get_U24disposing_6() const { return ___U24disposing_6; }
	inline bool* get_address_of_U24disposing_6() { return &___U24disposing_6; }
	inline void set_U24disposing_6(bool value)
	{
		___U24disposing_6 = value;
	}

	inline static int32_t get_offset_of_U24PC_7() { return static_cast<int32_t>(offsetof(U3CLoadResourceAsyncU3Ec__Iterator2_t3762021256, ___U24PC_7)); }
	inline int32_t get_U24PC_7() const { return ___U24PC_7; }
	inline int32_t* get_address_of_U24PC_7() { return &___U24PC_7; }
	inline void set_U24PC_7(int32_t value)
	{
		___U24PC_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CLOADRESOURCEASYNCU3EC__ITERATOR2_T3762021256_H
#ifndef U3CLOADASYNCU3EC__ANONSTOREY5_T4158068117_H
#define U3CLOADASYNCU3EC__ANONSTOREY5_T4158068117_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.AssetFileUtage/<LoadAsync>c__Iterator0/<LoadAsync>c__AnonStorey5
struct  U3CLoadAsyncU3Ec__AnonStorey5_t4158068117  : public RuntimeObject
{
public:
	// System.Action Utage.AssetFileUtage/<LoadAsync>c__Iterator0/<LoadAsync>c__AnonStorey5::onComplete
	Action_t3226471752 * ___onComplete_0;
	// System.Action Utage.AssetFileUtage/<LoadAsync>c__Iterator0/<LoadAsync>c__AnonStorey5::onFailed
	Action_t3226471752 * ___onFailed_1;
	// Utage.AssetFileUtage/<LoadAsync>c__Iterator0 Utage.AssetFileUtage/<LoadAsync>c__Iterator0/<LoadAsync>c__AnonStorey5::<>f__ref$0
	U3CLoadAsyncU3Ec__Iterator0_t2893323214 * ___U3CU3Ef__refU240_2;

public:
	inline static int32_t get_offset_of_onComplete_0() { return static_cast<int32_t>(offsetof(U3CLoadAsyncU3Ec__AnonStorey5_t4158068117, ___onComplete_0)); }
	inline Action_t3226471752 * get_onComplete_0() const { return ___onComplete_0; }
	inline Action_t3226471752 ** get_address_of_onComplete_0() { return &___onComplete_0; }
	inline void set_onComplete_0(Action_t3226471752 * value)
	{
		___onComplete_0 = value;
		Il2CppCodeGenWriteBarrier((&___onComplete_0), value);
	}

	inline static int32_t get_offset_of_onFailed_1() { return static_cast<int32_t>(offsetof(U3CLoadAsyncU3Ec__AnonStorey5_t4158068117, ___onFailed_1)); }
	inline Action_t3226471752 * get_onFailed_1() const { return ___onFailed_1; }
	inline Action_t3226471752 ** get_address_of_onFailed_1() { return &___onFailed_1; }
	inline void set_onFailed_1(Action_t3226471752 * value)
	{
		___onFailed_1 = value;
		Il2CppCodeGenWriteBarrier((&___onFailed_1), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__refU240_2() { return static_cast<int32_t>(offsetof(U3CLoadAsyncU3Ec__AnonStorey5_t4158068117, ___U3CU3Ef__refU240_2)); }
	inline U3CLoadAsyncU3Ec__Iterator0_t2893323214 * get_U3CU3Ef__refU240_2() const { return ___U3CU3Ef__refU240_2; }
	inline U3CLoadAsyncU3Ec__Iterator0_t2893323214 ** get_address_of_U3CU3Ef__refU240_2() { return &___U3CU3Ef__refU240_2; }
	inline void set_U3CU3Ef__refU240_2(U3CLoadAsyncU3Ec__Iterator0_t2893323214 * value)
	{
		___U3CU3Ef__refU240_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__refU240_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CLOADASYNCU3EC__ANONSTOREY5_T4158068117_H
#ifndef U3CLOADASSETBUNDLEASYNCU3EC__ANONSTOREY6_T1993755113_H
#define U3CLOADASSETBUNDLEASYNCU3EC__ANONSTOREY6_T1993755113_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.AssetFileUtage/<LoadAssetBundleAsync>c__Iterator3/<LoadAssetBundleAsync>c__AnonStorey6
struct  U3CLoadAssetBundleAsyncU3Ec__AnonStorey6_t1993755113  : public RuntimeObject
{
public:
	// System.Action Utage.AssetFileUtage/<LoadAssetBundleAsync>c__Iterator3/<LoadAssetBundleAsync>c__AnonStorey6::onComplete
	Action_t3226471752 * ___onComplete_0;
	// UnityEngine.AssetBundle Utage.AssetFileUtage/<LoadAssetBundleAsync>c__Iterator3/<LoadAssetBundleAsync>c__AnonStorey6::assetBundle
	AssetBundle_t2054978754 * ___assetBundle_1;
	// System.Action Utage.AssetFileUtage/<LoadAssetBundleAsync>c__Iterator3/<LoadAssetBundleAsync>c__AnonStorey6::onFailed
	Action_t3226471752 * ___onFailed_2;
	// Utage.AssetFileUtage/<LoadAssetBundleAsync>c__Iterator3 Utage.AssetFileUtage/<LoadAssetBundleAsync>c__Iterator3/<LoadAssetBundleAsync>c__AnonStorey6::<>f__ref$3
	U3CLoadAssetBundleAsyncU3Ec__Iterator3_t2280417849 * ___U3CU3Ef__refU243_3;

public:
	inline static int32_t get_offset_of_onComplete_0() { return static_cast<int32_t>(offsetof(U3CLoadAssetBundleAsyncU3Ec__AnonStorey6_t1993755113, ___onComplete_0)); }
	inline Action_t3226471752 * get_onComplete_0() const { return ___onComplete_0; }
	inline Action_t3226471752 ** get_address_of_onComplete_0() { return &___onComplete_0; }
	inline void set_onComplete_0(Action_t3226471752 * value)
	{
		___onComplete_0 = value;
		Il2CppCodeGenWriteBarrier((&___onComplete_0), value);
	}

	inline static int32_t get_offset_of_assetBundle_1() { return static_cast<int32_t>(offsetof(U3CLoadAssetBundleAsyncU3Ec__AnonStorey6_t1993755113, ___assetBundle_1)); }
	inline AssetBundle_t2054978754 * get_assetBundle_1() const { return ___assetBundle_1; }
	inline AssetBundle_t2054978754 ** get_address_of_assetBundle_1() { return &___assetBundle_1; }
	inline void set_assetBundle_1(AssetBundle_t2054978754 * value)
	{
		___assetBundle_1 = value;
		Il2CppCodeGenWriteBarrier((&___assetBundle_1), value);
	}

	inline static int32_t get_offset_of_onFailed_2() { return static_cast<int32_t>(offsetof(U3CLoadAssetBundleAsyncU3Ec__AnonStorey6_t1993755113, ___onFailed_2)); }
	inline Action_t3226471752 * get_onFailed_2() const { return ___onFailed_2; }
	inline Action_t3226471752 ** get_address_of_onFailed_2() { return &___onFailed_2; }
	inline void set_onFailed_2(Action_t3226471752 * value)
	{
		___onFailed_2 = value;
		Il2CppCodeGenWriteBarrier((&___onFailed_2), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__refU243_3() { return static_cast<int32_t>(offsetof(U3CLoadAssetBundleAsyncU3Ec__AnonStorey6_t1993755113, ___U3CU3Ef__refU243_3)); }
	inline U3CLoadAssetBundleAsyncU3Ec__Iterator3_t2280417849 * get_U3CU3Ef__refU243_3() const { return ___U3CU3Ef__refU243_3; }
	inline U3CLoadAssetBundleAsyncU3Ec__Iterator3_t2280417849 ** get_address_of_U3CU3Ef__refU243_3() { return &___U3CU3Ef__refU243_3; }
	inline void set_U3CU3Ef__refU243_3(U3CLoadAssetBundleAsyncU3Ec__Iterator3_t2280417849 * value)
	{
		___U3CU3Ef__refU243_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__refU243_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CLOADASSETBUNDLEASYNCU3EC__ANONSTOREY6_T1993755113_H
#ifndef U3CLOADASYNCU3EC__ITERATOR0_T1730758704_H
#define U3CLOADASYNCU3EC__ITERATOR0_T1730758704_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.StaticAssetFile/<LoadAsync>c__Iterator0
struct  U3CLoadAsyncU3Ec__Iterator0_t1730758704  : public RuntimeObject
{
public:
	// System.Action Utage.StaticAssetFile/<LoadAsync>c__Iterator0::onComplete
	Action_t3226471752 * ___onComplete_0;
	// System.Object Utage.StaticAssetFile/<LoadAsync>c__Iterator0::$current
	RuntimeObject * ___U24current_1;
	// System.Boolean Utage.StaticAssetFile/<LoadAsync>c__Iterator0::$disposing
	bool ___U24disposing_2;
	// System.Int32 Utage.StaticAssetFile/<LoadAsync>c__Iterator0::$PC
	int32_t ___U24PC_3;

public:
	inline static int32_t get_offset_of_onComplete_0() { return static_cast<int32_t>(offsetof(U3CLoadAsyncU3Ec__Iterator0_t1730758704, ___onComplete_0)); }
	inline Action_t3226471752 * get_onComplete_0() const { return ___onComplete_0; }
	inline Action_t3226471752 ** get_address_of_onComplete_0() { return &___onComplete_0; }
	inline void set_onComplete_0(Action_t3226471752 * value)
	{
		___onComplete_0 = value;
		Il2CppCodeGenWriteBarrier((&___onComplete_0), value);
	}

	inline static int32_t get_offset_of_U24current_1() { return static_cast<int32_t>(offsetof(U3CLoadAsyncU3Ec__Iterator0_t1730758704, ___U24current_1)); }
	inline RuntimeObject * get_U24current_1() const { return ___U24current_1; }
	inline RuntimeObject ** get_address_of_U24current_1() { return &___U24current_1; }
	inline void set_U24current_1(RuntimeObject * value)
	{
		___U24current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_1), value);
	}

	inline static int32_t get_offset_of_U24disposing_2() { return static_cast<int32_t>(offsetof(U3CLoadAsyncU3Ec__Iterator0_t1730758704, ___U24disposing_2)); }
	inline bool get_U24disposing_2() const { return ___U24disposing_2; }
	inline bool* get_address_of_U24disposing_2() { return &___U24disposing_2; }
	inline void set_U24disposing_2(bool value)
	{
		___U24disposing_2 = value;
	}

	inline static int32_t get_offset_of_U24PC_3() { return static_cast<int32_t>(offsetof(U3CLoadAsyncU3Ec__Iterator0_t1730758704, ___U24PC_3)); }
	inline int32_t get_U24PC_3() const { return ___U24PC_3; }
	inline int32_t* get_address_of_U24PC_3() { return &___U24PC_3; }
	inline void set_U24PC_3(int32_t value)
	{
		___U24PC_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CLOADASYNCU3EC__ITERATOR0_T1730758704_H
#ifndef STATICASSET_T1112757920_H
#define STATICASSET_T1112757920_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.StaticAsset
struct  StaticAsset_t1112757920  : public RuntimeObject
{
public:
	// UnityEngine.Object Utage.StaticAsset::asset
	Object_t1021602117 * ___asset_0;

public:
	inline static int32_t get_offset_of_asset_0() { return static_cast<int32_t>(offsetof(StaticAsset_t1112757920, ___asset_0)); }
	inline Object_t1021602117 * get_asset_0() const { return ___asset_0; }
	inline Object_t1021602117 ** get_address_of_asset_0() { return &___asset_0; }
	inline void set_asset_0(Object_t1021602117 * value)
	{
		___asset_0 = value;
		Il2CppCodeGenWriteBarrier((&___asset_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STATICASSET_T1112757920_H
#ifndef U3CFINDASSETFILEU3EC__ANONSTOREY0_T574198236_H
#define U3CFINDASSETFILEU3EC__ANONSTOREY0_T574198236_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.StaticAssetManager/<FindAssetFile>c__AnonStorey0
struct  U3CFindAssetFileU3Ec__AnonStorey0_t574198236  : public RuntimeObject
{
public:
	// System.String Utage.StaticAssetManager/<FindAssetFile>c__AnonStorey0::assetName
	String_t* ___assetName_0;

public:
	inline static int32_t get_offset_of_assetName_0() { return static_cast<int32_t>(offsetof(U3CFindAssetFileU3Ec__AnonStorey0_t574198236, ___assetName_0)); }
	inline String_t* get_assetName_0() const { return ___assetName_0; }
	inline String_t** get_address_of_assetName_0() { return &___assetName_0; }
	inline void set_assetName_0(String_t* value)
	{
		___assetName_0 = value;
		Il2CppCodeGenWriteBarrier((&___assetName_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CFINDASSETFILEU3EC__ANONSTOREY0_T574198236_H
#ifndef EXPRESSIONCAST_T1679256093_H
#define EXPRESSIONCAST_T1679256093_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.ExpressionCast
struct  ExpressionCast_t1679256093  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EXPRESSIONCAST_T1679256093_H
#ifndef EXPRESSIONPARSER_T665800307_H
#define EXPRESSIONPARSER_T665800307_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.ExpressionParser
struct  ExpressionParser_t665800307  : public RuntimeObject
{
public:
	// System.String Utage.ExpressionParser::exp
	String_t* ___exp_0;
	// System.String Utage.ExpressionParser::errorMsg
	String_t* ___errorMsg_1;
	// System.Collections.Generic.List`1<Utage.ExpressionToken> Utage.ExpressionParser::tokens
	List_1_t2114587879 * ___tokens_2;

public:
	inline static int32_t get_offset_of_exp_0() { return static_cast<int32_t>(offsetof(ExpressionParser_t665800307, ___exp_0)); }
	inline String_t* get_exp_0() const { return ___exp_0; }
	inline String_t** get_address_of_exp_0() { return &___exp_0; }
	inline void set_exp_0(String_t* value)
	{
		___exp_0 = value;
		Il2CppCodeGenWriteBarrier((&___exp_0), value);
	}

	inline static int32_t get_offset_of_errorMsg_1() { return static_cast<int32_t>(offsetof(ExpressionParser_t665800307, ___errorMsg_1)); }
	inline String_t* get_errorMsg_1() const { return ___errorMsg_1; }
	inline String_t** get_address_of_errorMsg_1() { return &___errorMsg_1; }
	inline void set_errorMsg_1(String_t* value)
	{
		___errorMsg_1 = value;
		Il2CppCodeGenWriteBarrier((&___errorMsg_1), value);
	}

	inline static int32_t get_offset_of_tokens_2() { return static_cast<int32_t>(offsetof(ExpressionParser_t665800307, ___tokens_2)); }
	inline List_1_t2114587879 * get_tokens_2() const { return ___tokens_2; }
	inline List_1_t2114587879 ** get_address_of_tokens_2() { return &___tokens_2; }
	inline void set_tokens_2(List_1_t2114587879 * value)
	{
		___tokens_2 = value;
		Il2CppCodeGenWriteBarrier((&___tokens_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EXPRESSIONPARSER_T665800307_H
#ifndef U3CLOADASSETBUNDLEASYNCU3EC__ITERATOR4_T2280417856_H
#define U3CLOADASSETBUNDLEASYNCU3EC__ITERATOR4_T2280417856_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.AssetFileUtage/<LoadAssetBundleAsync>c__Iterator4
struct  U3CLoadAssetBundleAsyncU3Ec__Iterator4_t2280417856  : public RuntimeObject
{
public:
	// UnityEngine.AssetBundle Utage.AssetFileUtage/<LoadAssetBundleAsync>c__Iterator4::assetBundle
	AssetBundle_t2054978754 * ___assetBundle_0;
	// UnityEngine.AssetBundleRequest Utage.AssetFileUtage/<LoadAssetBundleAsync>c__Iterator4::<request>__0
	AssetBundleRequest_t2674559435 * ___U3CrequestU3E__0_1;
	// UnityEngine.Object[] Utage.AssetFileUtage/<LoadAssetBundleAsync>c__Iterator4::<assets>__0
	ObjectU5BU5D_t4217747464* ___U3CassetsU3E__0_2;
	// System.Action Utage.AssetFileUtage/<LoadAssetBundleAsync>c__Iterator4::onFailed
	Action_t3226471752 * ___onFailed_3;
	// System.Action Utage.AssetFileUtage/<LoadAssetBundleAsync>c__Iterator4::onComplete
	Action_t3226471752 * ___onComplete_4;
	// Utage.AssetFileUtage Utage.AssetFileUtage/<LoadAssetBundleAsync>c__Iterator4::$this
	AssetFileUtage_t3568203916 * ___U24this_5;
	// System.Object Utage.AssetFileUtage/<LoadAssetBundleAsync>c__Iterator4::$current
	RuntimeObject * ___U24current_6;
	// System.Boolean Utage.AssetFileUtage/<LoadAssetBundleAsync>c__Iterator4::$disposing
	bool ___U24disposing_7;
	// System.Int32 Utage.AssetFileUtage/<LoadAssetBundleAsync>c__Iterator4::$PC
	int32_t ___U24PC_8;

public:
	inline static int32_t get_offset_of_assetBundle_0() { return static_cast<int32_t>(offsetof(U3CLoadAssetBundleAsyncU3Ec__Iterator4_t2280417856, ___assetBundle_0)); }
	inline AssetBundle_t2054978754 * get_assetBundle_0() const { return ___assetBundle_0; }
	inline AssetBundle_t2054978754 ** get_address_of_assetBundle_0() { return &___assetBundle_0; }
	inline void set_assetBundle_0(AssetBundle_t2054978754 * value)
	{
		___assetBundle_0 = value;
		Il2CppCodeGenWriteBarrier((&___assetBundle_0), value);
	}

	inline static int32_t get_offset_of_U3CrequestU3E__0_1() { return static_cast<int32_t>(offsetof(U3CLoadAssetBundleAsyncU3Ec__Iterator4_t2280417856, ___U3CrequestU3E__0_1)); }
	inline AssetBundleRequest_t2674559435 * get_U3CrequestU3E__0_1() const { return ___U3CrequestU3E__0_1; }
	inline AssetBundleRequest_t2674559435 ** get_address_of_U3CrequestU3E__0_1() { return &___U3CrequestU3E__0_1; }
	inline void set_U3CrequestU3E__0_1(AssetBundleRequest_t2674559435 * value)
	{
		___U3CrequestU3E__0_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CrequestU3E__0_1), value);
	}

	inline static int32_t get_offset_of_U3CassetsU3E__0_2() { return static_cast<int32_t>(offsetof(U3CLoadAssetBundleAsyncU3Ec__Iterator4_t2280417856, ___U3CassetsU3E__0_2)); }
	inline ObjectU5BU5D_t4217747464* get_U3CassetsU3E__0_2() const { return ___U3CassetsU3E__0_2; }
	inline ObjectU5BU5D_t4217747464** get_address_of_U3CassetsU3E__0_2() { return &___U3CassetsU3E__0_2; }
	inline void set_U3CassetsU3E__0_2(ObjectU5BU5D_t4217747464* value)
	{
		___U3CassetsU3E__0_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CassetsU3E__0_2), value);
	}

	inline static int32_t get_offset_of_onFailed_3() { return static_cast<int32_t>(offsetof(U3CLoadAssetBundleAsyncU3Ec__Iterator4_t2280417856, ___onFailed_3)); }
	inline Action_t3226471752 * get_onFailed_3() const { return ___onFailed_3; }
	inline Action_t3226471752 ** get_address_of_onFailed_3() { return &___onFailed_3; }
	inline void set_onFailed_3(Action_t3226471752 * value)
	{
		___onFailed_3 = value;
		Il2CppCodeGenWriteBarrier((&___onFailed_3), value);
	}

	inline static int32_t get_offset_of_onComplete_4() { return static_cast<int32_t>(offsetof(U3CLoadAssetBundleAsyncU3Ec__Iterator4_t2280417856, ___onComplete_4)); }
	inline Action_t3226471752 * get_onComplete_4() const { return ___onComplete_4; }
	inline Action_t3226471752 ** get_address_of_onComplete_4() { return &___onComplete_4; }
	inline void set_onComplete_4(Action_t3226471752 * value)
	{
		___onComplete_4 = value;
		Il2CppCodeGenWriteBarrier((&___onComplete_4), value);
	}

	inline static int32_t get_offset_of_U24this_5() { return static_cast<int32_t>(offsetof(U3CLoadAssetBundleAsyncU3Ec__Iterator4_t2280417856, ___U24this_5)); }
	inline AssetFileUtage_t3568203916 * get_U24this_5() const { return ___U24this_5; }
	inline AssetFileUtage_t3568203916 ** get_address_of_U24this_5() { return &___U24this_5; }
	inline void set_U24this_5(AssetFileUtage_t3568203916 * value)
	{
		___U24this_5 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_5), value);
	}

	inline static int32_t get_offset_of_U24current_6() { return static_cast<int32_t>(offsetof(U3CLoadAssetBundleAsyncU3Ec__Iterator4_t2280417856, ___U24current_6)); }
	inline RuntimeObject * get_U24current_6() const { return ___U24current_6; }
	inline RuntimeObject ** get_address_of_U24current_6() { return &___U24current_6; }
	inline void set_U24current_6(RuntimeObject * value)
	{
		___U24current_6 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_6), value);
	}

	inline static int32_t get_offset_of_U24disposing_7() { return static_cast<int32_t>(offsetof(U3CLoadAssetBundleAsyncU3Ec__Iterator4_t2280417856, ___U24disposing_7)); }
	inline bool get_U24disposing_7() const { return ___U24disposing_7; }
	inline bool* get_address_of_U24disposing_7() { return &___U24disposing_7; }
	inline void set_U24disposing_7(bool value)
	{
		___U24disposing_7 = value;
	}

	inline static int32_t get_offset_of_U24PC_8() { return static_cast<int32_t>(offsetof(U3CLoadAssetBundleAsyncU3Ec__Iterator4_t2280417856, ___U24PC_8)); }
	inline int32_t get_U24PC_8() const { return ___U24PC_8; }
	inline int32_t* get_address_of_U24PC_8() { return &___U24PC_8; }
	inline void set_U24PC_8(int32_t value)
	{
		___U24PC_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CLOADASSETBUNDLEASYNCU3EC__ITERATOR4_T2280417856_H
#ifndef U3CDOWNLOADMANIFESTASYNCU3EC__ANONSTOREY0_T3359135894_H
#define U3CDOWNLOADMANIFESTASYNCU3EC__ANONSTOREY0_T3359135894_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.AssetBundleInfoManager/<DownloadManifestAsync>c__AnonStorey0
struct  U3CDownloadManifestAsyncU3Ec__AnonStorey0_t3359135894  : public RuntimeObject
{
public:
	// System.String Utage.AssetBundleInfoManager/<DownloadManifestAsync>c__AnonStorey0::rootUrl
	String_t* ___rootUrl_0;
	// System.String Utage.AssetBundleInfoManager/<DownloadManifestAsync>c__AnonStorey0::relativeUrl
	String_t* ___relativeUrl_1;
	// Utage.WWWEx Utage.AssetBundleInfoManager/<DownloadManifestAsync>c__AnonStorey0::wwwEx
	WWWEx_t1775400116 * ___wwwEx_2;
	// System.Action Utage.AssetBundleInfoManager/<DownloadManifestAsync>c__AnonStorey0::onComplete
	Action_t3226471752 * ___onComplete_3;
	// System.Action Utage.AssetBundleInfoManager/<DownloadManifestAsync>c__AnonStorey0::onFailed
	Action_t3226471752 * ___onFailed_4;
	// Utage.AssetBundleInfoManager Utage.AssetBundleInfoManager/<DownloadManifestAsync>c__AnonStorey0::$this
	AssetBundleInfoManager_t223439115 * ___U24this_5;

public:
	inline static int32_t get_offset_of_rootUrl_0() { return static_cast<int32_t>(offsetof(U3CDownloadManifestAsyncU3Ec__AnonStorey0_t3359135894, ___rootUrl_0)); }
	inline String_t* get_rootUrl_0() const { return ___rootUrl_0; }
	inline String_t** get_address_of_rootUrl_0() { return &___rootUrl_0; }
	inline void set_rootUrl_0(String_t* value)
	{
		___rootUrl_0 = value;
		Il2CppCodeGenWriteBarrier((&___rootUrl_0), value);
	}

	inline static int32_t get_offset_of_relativeUrl_1() { return static_cast<int32_t>(offsetof(U3CDownloadManifestAsyncU3Ec__AnonStorey0_t3359135894, ___relativeUrl_1)); }
	inline String_t* get_relativeUrl_1() const { return ___relativeUrl_1; }
	inline String_t** get_address_of_relativeUrl_1() { return &___relativeUrl_1; }
	inline void set_relativeUrl_1(String_t* value)
	{
		___relativeUrl_1 = value;
		Il2CppCodeGenWriteBarrier((&___relativeUrl_1), value);
	}

	inline static int32_t get_offset_of_wwwEx_2() { return static_cast<int32_t>(offsetof(U3CDownloadManifestAsyncU3Ec__AnonStorey0_t3359135894, ___wwwEx_2)); }
	inline WWWEx_t1775400116 * get_wwwEx_2() const { return ___wwwEx_2; }
	inline WWWEx_t1775400116 ** get_address_of_wwwEx_2() { return &___wwwEx_2; }
	inline void set_wwwEx_2(WWWEx_t1775400116 * value)
	{
		___wwwEx_2 = value;
		Il2CppCodeGenWriteBarrier((&___wwwEx_2), value);
	}

	inline static int32_t get_offset_of_onComplete_3() { return static_cast<int32_t>(offsetof(U3CDownloadManifestAsyncU3Ec__AnonStorey0_t3359135894, ___onComplete_3)); }
	inline Action_t3226471752 * get_onComplete_3() const { return ___onComplete_3; }
	inline Action_t3226471752 ** get_address_of_onComplete_3() { return &___onComplete_3; }
	inline void set_onComplete_3(Action_t3226471752 * value)
	{
		___onComplete_3 = value;
		Il2CppCodeGenWriteBarrier((&___onComplete_3), value);
	}

	inline static int32_t get_offset_of_onFailed_4() { return static_cast<int32_t>(offsetof(U3CDownloadManifestAsyncU3Ec__AnonStorey0_t3359135894, ___onFailed_4)); }
	inline Action_t3226471752 * get_onFailed_4() const { return ___onFailed_4; }
	inline Action_t3226471752 ** get_address_of_onFailed_4() { return &___onFailed_4; }
	inline void set_onFailed_4(Action_t3226471752 * value)
	{
		___onFailed_4 = value;
		Il2CppCodeGenWriteBarrier((&___onFailed_4), value);
	}

	inline static int32_t get_offset_of_U24this_5() { return static_cast<int32_t>(offsetof(U3CDownloadManifestAsyncU3Ec__AnonStorey0_t3359135894, ___U24this_5)); }
	inline AssetBundleInfoManager_t223439115 * get_U24this_5() const { return ___U24this_5; }
	inline AssetBundleInfoManager_t223439115 ** get_address_of_U24this_5() { return &___U24this_5; }
	inline void set_U24this_5(AssetBundleInfoManager_t223439115 * value)
	{
		___U24this_5 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CDOWNLOADMANIFESTASYNCU3EC__ANONSTOREY0_T3359135894_H
#ifndef U3CLOADASYNCU3EC__ANONSTOREY5_T3302198958_H
#define U3CLOADASYNCU3EC__ANONSTOREY5_T3302198958_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.AssetFileManager/<LoadAsync>c__Iterator2/<LoadAsync>c__AnonStorey5
struct  U3CLoadAsyncU3Ec__AnonStorey5_t3302198958  : public RuntimeObject
{
public:
	// Utage.AssetFileBase Utage.AssetFileManager/<LoadAsync>c__Iterator2/<LoadAsync>c__AnonStorey5::file
	AssetFileBase_t4101360697 * ___file_0;
	// Utage.AssetFileManager/<LoadAsync>c__Iterator2 Utage.AssetFileManager/<LoadAsync>c__Iterator2/<LoadAsync>c__AnonStorey5::<>f__ref$2
	U3CLoadAsyncU3Ec__Iterator2_t109345333 * ___U3CU3Ef__refU242_1;

public:
	inline static int32_t get_offset_of_file_0() { return static_cast<int32_t>(offsetof(U3CLoadAsyncU3Ec__AnonStorey5_t3302198958, ___file_0)); }
	inline AssetFileBase_t4101360697 * get_file_0() const { return ___file_0; }
	inline AssetFileBase_t4101360697 ** get_address_of_file_0() { return &___file_0; }
	inline void set_file_0(AssetFileBase_t4101360697 * value)
	{
		___file_0 = value;
		Il2CppCodeGenWriteBarrier((&___file_0), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__refU242_1() { return static_cast<int32_t>(offsetof(U3CLoadAsyncU3Ec__AnonStorey5_t3302198958, ___U3CU3Ef__refU242_1)); }
	inline U3CLoadAsyncU3Ec__Iterator2_t109345333 * get_U3CU3Ef__refU242_1() const { return ___U3CU3Ef__refU242_1; }
	inline U3CLoadAsyncU3Ec__Iterator2_t109345333 ** get_address_of_U3CU3Ef__refU242_1() { return &___U3CU3Ef__refU242_1; }
	inline void set_U3CU3Ef__refU242_1(U3CLoadAsyncU3Ec__Iterator2_t109345333 * value)
	{
		___U3CU3Ef__refU242_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__refU242_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CLOADASYNCU3EC__ANONSTOREY5_T3302198958_H
#ifndef U3CUNLOADUNUSEDASSETSASYNCU3EC__ITERATOR3_T4247547924_H
#define U3CUNLOADUNUSEDASSETSASYNCU3EC__ITERATOR3_T4247547924_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.AssetFileManager/<UnloadUnusedAssetsAsync>c__Iterator3
struct  U3CUnloadUnusedAssetsAsyncU3Ec__Iterator3_t4247547924  : public RuntimeObject
{
public:
	// Utage.AssetFileManager Utage.AssetFileManager/<UnloadUnusedAssetsAsync>c__Iterator3::$this
	AssetFileManager_t1095395563 * ___U24this_0;
	// System.Object Utage.AssetFileManager/<UnloadUnusedAssetsAsync>c__Iterator3::$current
	RuntimeObject * ___U24current_1;
	// System.Boolean Utage.AssetFileManager/<UnloadUnusedAssetsAsync>c__Iterator3::$disposing
	bool ___U24disposing_2;
	// System.Int32 Utage.AssetFileManager/<UnloadUnusedAssetsAsync>c__Iterator3::$PC
	int32_t ___U24PC_3;

public:
	inline static int32_t get_offset_of_U24this_0() { return static_cast<int32_t>(offsetof(U3CUnloadUnusedAssetsAsyncU3Ec__Iterator3_t4247547924, ___U24this_0)); }
	inline AssetFileManager_t1095395563 * get_U24this_0() const { return ___U24this_0; }
	inline AssetFileManager_t1095395563 ** get_address_of_U24this_0() { return &___U24this_0; }
	inline void set_U24this_0(AssetFileManager_t1095395563 * value)
	{
		___U24this_0 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_0), value);
	}

	inline static int32_t get_offset_of_U24current_1() { return static_cast<int32_t>(offsetof(U3CUnloadUnusedAssetsAsyncU3Ec__Iterator3_t4247547924, ___U24current_1)); }
	inline RuntimeObject * get_U24current_1() const { return ___U24current_1; }
	inline RuntimeObject ** get_address_of_U24current_1() { return &___U24current_1; }
	inline void set_U24current_1(RuntimeObject * value)
	{
		___U24current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_1), value);
	}

	inline static int32_t get_offset_of_U24disposing_2() { return static_cast<int32_t>(offsetof(U3CUnloadUnusedAssetsAsyncU3Ec__Iterator3_t4247547924, ___U24disposing_2)); }
	inline bool get_U24disposing_2() const { return ___U24disposing_2; }
	inline bool* get_address_of_U24disposing_2() { return &___U24disposing_2; }
	inline void set_U24disposing_2(bool value)
	{
		___U24disposing_2 = value;
	}

	inline static int32_t get_offset_of_U24PC_3() { return static_cast<int32_t>(offsetof(U3CUnloadUnusedAssetsAsyncU3Ec__Iterator3_t4247547924, ___U24PC_3)); }
	inline int32_t get_U24PC_3() const { return ___U24PC_3; }
	inline int32_t* get_address_of_U24PC_3() { return &___U24PC_3; }
	inline void set_U24PC_3(int32_t value)
	{
		___U24PC_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CUNLOADUNUSEDASSETSASYNCU3EC__ITERATOR3_T4247547924_H
#ifndef U3CFINDSETTINGFROMPATHU3EC__ANONSTOREY1_T1929813237_H
#define U3CFINDSETTINGFROMPATHU3EC__ANONSTOREY1_T1929813237_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.AssetFileManagerSettings/<FindSettingFromPath>c__AnonStorey1
struct  U3CFindSettingFromPathU3Ec__AnonStorey1_t1929813237  : public RuntimeObject
{
public:
	// System.String Utage.AssetFileManagerSettings/<FindSettingFromPath>c__AnonStorey1::path
	String_t* ___path_0;

public:
	inline static int32_t get_offset_of_path_0() { return static_cast<int32_t>(offsetof(U3CFindSettingFromPathU3Ec__AnonStorey1_t1929813237, ___path_0)); }
	inline String_t* get_path_0() const { return ___path_0; }
	inline String_t** get_address_of_path_0() { return &___path_0; }
	inline void set_path_0(String_t* value)
	{
		___path_0 = value;
		Il2CppCodeGenWriteBarrier((&___path_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CFINDSETTINGFROMPATHU3EC__ANONSTOREY1_T1929813237_H
#ifndef FILEPATHUTIL_T4082793301_H
#define FILEPATHUTIL_T4082793301_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.FilePathUtil
struct  FilePathUtil_t4082793301  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FILEPATHUTIL_T4082793301_H
#ifndef COMPRESSION_T2003203368_H
#define COMPRESSION_T2003203368_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.Compression
struct  Compression_t2003203368  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPRESSION_T2003203368_H
#ifndef INDEX_T675362613_H
#define INDEX_T675362613_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.Compression/Index
struct  Index_t675362613  : public RuntimeObject
{
public:
	// Utage.Compression/Node[] Utage.Compression/Index::mNodes
	NodeU5BU5D_t2732557534* ___mNodes_0;
	// System.Int32[] Utage.Compression/Index::mStack
	Int32U5BU5D_t3030399641* ___mStack_1;
	// System.Int32 Utage.Compression/Index::mStackPos
	int32_t ___mStackPos_2;

public:
	inline static int32_t get_offset_of_mNodes_0() { return static_cast<int32_t>(offsetof(Index_t675362613, ___mNodes_0)); }
	inline NodeU5BU5D_t2732557534* get_mNodes_0() const { return ___mNodes_0; }
	inline NodeU5BU5D_t2732557534** get_address_of_mNodes_0() { return &___mNodes_0; }
	inline void set_mNodes_0(NodeU5BU5D_t2732557534* value)
	{
		___mNodes_0 = value;
		Il2CppCodeGenWriteBarrier((&___mNodes_0), value);
	}

	inline static int32_t get_offset_of_mStack_1() { return static_cast<int32_t>(offsetof(Index_t675362613, ___mStack_1)); }
	inline Int32U5BU5D_t3030399641* get_mStack_1() const { return ___mStack_1; }
	inline Int32U5BU5D_t3030399641** get_address_of_mStack_1() { return &___mStack_1; }
	inline void set_mStack_1(Int32U5BU5D_t3030399641* value)
	{
		___mStack_1 = value;
		Il2CppCodeGenWriteBarrier((&___mStack_1), value);
	}

	inline static int32_t get_offset_of_mStackPos_2() { return static_cast<int32_t>(offsetof(Index_t675362613, ___mStackPos_2)); }
	inline int32_t get_mStackPos_2() const { return ___mStackPos_2; }
	inline int32_t* get_address_of_mStackPos_2() { return &___mStackPos_2; }
	inline void set_mStackPos_2(int32_t value)
	{
		___mStackPos_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INDEX_T675362613_H
#ifndef U3CLOADASYNCU3EC__ITERATOR2_T109345333_H
#define U3CLOADASYNCU3EC__ITERATOR2_T109345333_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.AssetFileManager/<LoadAsync>c__Iterator2
struct  U3CLoadAsyncU3Ec__Iterator2_t109345333  : public RuntimeObject
{
public:
	// Utage.AssetFileBase Utage.AssetFileManager/<LoadAsync>c__Iterator2::file
	AssetFileBase_t4101360697 * ___file_0;
	// Utage.AssetFileManager Utage.AssetFileManager/<LoadAsync>c__Iterator2::$this
	AssetFileManager_t1095395563 * ___U24this_1;
	// System.Object Utage.AssetFileManager/<LoadAsync>c__Iterator2::$current
	RuntimeObject * ___U24current_2;
	// System.Boolean Utage.AssetFileManager/<LoadAsync>c__Iterator2::$disposing
	bool ___U24disposing_3;
	// System.Int32 Utage.AssetFileManager/<LoadAsync>c__Iterator2::$PC
	int32_t ___U24PC_4;
	// Utage.AssetFileManager/<LoadAsync>c__Iterator2/<LoadAsync>c__AnonStorey5 Utage.AssetFileManager/<LoadAsync>c__Iterator2::$locvar0
	U3CLoadAsyncU3Ec__AnonStorey5_t3302198958 * ___U24locvar0_5;

public:
	inline static int32_t get_offset_of_file_0() { return static_cast<int32_t>(offsetof(U3CLoadAsyncU3Ec__Iterator2_t109345333, ___file_0)); }
	inline AssetFileBase_t4101360697 * get_file_0() const { return ___file_0; }
	inline AssetFileBase_t4101360697 ** get_address_of_file_0() { return &___file_0; }
	inline void set_file_0(AssetFileBase_t4101360697 * value)
	{
		___file_0 = value;
		Il2CppCodeGenWriteBarrier((&___file_0), value);
	}

	inline static int32_t get_offset_of_U24this_1() { return static_cast<int32_t>(offsetof(U3CLoadAsyncU3Ec__Iterator2_t109345333, ___U24this_1)); }
	inline AssetFileManager_t1095395563 * get_U24this_1() const { return ___U24this_1; }
	inline AssetFileManager_t1095395563 ** get_address_of_U24this_1() { return &___U24this_1; }
	inline void set_U24this_1(AssetFileManager_t1095395563 * value)
	{
		___U24this_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_1), value);
	}

	inline static int32_t get_offset_of_U24current_2() { return static_cast<int32_t>(offsetof(U3CLoadAsyncU3Ec__Iterator2_t109345333, ___U24current_2)); }
	inline RuntimeObject * get_U24current_2() const { return ___U24current_2; }
	inline RuntimeObject ** get_address_of_U24current_2() { return &___U24current_2; }
	inline void set_U24current_2(RuntimeObject * value)
	{
		___U24current_2 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_2), value);
	}

	inline static int32_t get_offset_of_U24disposing_3() { return static_cast<int32_t>(offsetof(U3CLoadAsyncU3Ec__Iterator2_t109345333, ___U24disposing_3)); }
	inline bool get_U24disposing_3() const { return ___U24disposing_3; }
	inline bool* get_address_of_U24disposing_3() { return &___U24disposing_3; }
	inline void set_U24disposing_3(bool value)
	{
		___U24disposing_3 = value;
	}

	inline static int32_t get_offset_of_U24PC_4() { return static_cast<int32_t>(offsetof(U3CLoadAsyncU3Ec__Iterator2_t109345333, ___U24PC_4)); }
	inline int32_t get_U24PC_4() const { return ___U24PC_4; }
	inline int32_t* get_address_of_U24PC_4() { return &___U24PC_4; }
	inline void set_U24PC_4(int32_t value)
	{
		___U24PC_4 = value;
	}

	inline static int32_t get_offset_of_U24locvar0_5() { return static_cast<int32_t>(offsetof(U3CLoadAsyncU3Ec__Iterator2_t109345333, ___U24locvar0_5)); }
	inline U3CLoadAsyncU3Ec__AnonStorey5_t3302198958 * get_U24locvar0_5() const { return ___U24locvar0_5; }
	inline U3CLoadAsyncU3Ec__AnonStorey5_t3302198958 ** get_address_of_U24locvar0_5() { return &___U24locvar0_5; }
	inline void set_U24locvar0_5(U3CLoadAsyncU3Ec__AnonStorey5_t3302198958 * value)
	{
		___U24locvar0_5 = value;
		Il2CppCodeGenWriteBarrier((&___U24locvar0_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CLOADASYNCU3EC__ITERATOR2_T109345333_H
#ifndef U3CLOADCACHEMANIFESTASYNCU3EC__ANONSTOREY1_T1823896243_H
#define U3CLOADCACHEMANIFESTASYNCU3EC__ANONSTOREY1_T1823896243_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.AssetBundleInfoManager/<LoadCacheManifestAsync>c__AnonStorey1
struct  U3CLoadCacheManifestAsyncU3Ec__AnonStorey1_t1823896243  : public RuntimeObject
{
public:
	// System.String Utage.AssetBundleInfoManager/<LoadCacheManifestAsync>c__AnonStorey1::rootUrl
	String_t* ___rootUrl_0;
	// System.Action Utage.AssetBundleInfoManager/<LoadCacheManifestAsync>c__AnonStorey1::onComplete
	Action_t3226471752 * ___onComplete_1;
	// System.Action Utage.AssetBundleInfoManager/<LoadCacheManifestAsync>c__AnonStorey1::onFailed
	Action_t3226471752 * ___onFailed_2;
	// Utage.AssetBundleInfoManager Utage.AssetBundleInfoManager/<LoadCacheManifestAsync>c__AnonStorey1::$this
	AssetBundleInfoManager_t223439115 * ___U24this_3;

public:
	inline static int32_t get_offset_of_rootUrl_0() { return static_cast<int32_t>(offsetof(U3CLoadCacheManifestAsyncU3Ec__AnonStorey1_t1823896243, ___rootUrl_0)); }
	inline String_t* get_rootUrl_0() const { return ___rootUrl_0; }
	inline String_t** get_address_of_rootUrl_0() { return &___rootUrl_0; }
	inline void set_rootUrl_0(String_t* value)
	{
		___rootUrl_0 = value;
		Il2CppCodeGenWriteBarrier((&___rootUrl_0), value);
	}

	inline static int32_t get_offset_of_onComplete_1() { return static_cast<int32_t>(offsetof(U3CLoadCacheManifestAsyncU3Ec__AnonStorey1_t1823896243, ___onComplete_1)); }
	inline Action_t3226471752 * get_onComplete_1() const { return ___onComplete_1; }
	inline Action_t3226471752 ** get_address_of_onComplete_1() { return &___onComplete_1; }
	inline void set_onComplete_1(Action_t3226471752 * value)
	{
		___onComplete_1 = value;
		Il2CppCodeGenWriteBarrier((&___onComplete_1), value);
	}

	inline static int32_t get_offset_of_onFailed_2() { return static_cast<int32_t>(offsetof(U3CLoadCacheManifestAsyncU3Ec__AnonStorey1_t1823896243, ___onFailed_2)); }
	inline Action_t3226471752 * get_onFailed_2() const { return ___onFailed_2; }
	inline Action_t3226471752 ** get_address_of_onFailed_2() { return &___onFailed_2; }
	inline void set_onFailed_2(Action_t3226471752 * value)
	{
		___onFailed_2 = value;
		Il2CppCodeGenWriteBarrier((&___onFailed_2), value);
	}

	inline static int32_t get_offset_of_U24this_3() { return static_cast<int32_t>(offsetof(U3CLoadCacheManifestAsyncU3Ec__AnonStorey1_t1823896243, ___U24this_3)); }
	inline AssetBundleInfoManager_t223439115 * get_U24this_3() const { return ___U24this_3; }
	inline AssetBundleInfoManager_t223439115 ** get_address_of_U24this_3() { return &___U24this_3; }
	inline void set_U24this_3(AssetBundleInfoManager_t223439115 * value)
	{
		___U24this_3 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CLOADCACHEMANIFESTASYNCU3EC__ANONSTOREY1_T1823896243_H
#ifndef U3CLOADASYNCU3EC__ITERATOR0_T2893323214_H
#define U3CLOADASYNCU3EC__ITERATOR0_T2893323214_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.AssetFileUtage/<LoadAsync>c__Iterator0
struct  U3CLoadAsyncU3Ec__Iterator0_t2893323214  : public RuntimeObject
{
public:
	// System.Action Utage.AssetFileUtage/<LoadAsync>c__Iterator0::onComplete
	Action_t3226471752 * ___onComplete_0;
	// System.Action Utage.AssetFileUtage/<LoadAsync>c__Iterator0::onFailed
	Action_t3226471752 * ___onFailed_1;
	// Utage.AssetFileUtage Utage.AssetFileUtage/<LoadAsync>c__Iterator0::$this
	AssetFileUtage_t3568203916 * ___U24this_2;
	// System.Object Utage.AssetFileUtage/<LoadAsync>c__Iterator0::$current
	RuntimeObject * ___U24current_3;
	// System.Boolean Utage.AssetFileUtage/<LoadAsync>c__Iterator0::$disposing
	bool ___U24disposing_4;
	// System.Int32 Utage.AssetFileUtage/<LoadAsync>c__Iterator0::$PC
	int32_t ___U24PC_5;
	// Utage.AssetFileUtage/<LoadAsync>c__Iterator0/<LoadAsync>c__AnonStorey5 Utage.AssetFileUtage/<LoadAsync>c__Iterator0::$locvar0
	U3CLoadAsyncU3Ec__AnonStorey5_t4158068117 * ___U24locvar0_6;

public:
	inline static int32_t get_offset_of_onComplete_0() { return static_cast<int32_t>(offsetof(U3CLoadAsyncU3Ec__Iterator0_t2893323214, ___onComplete_0)); }
	inline Action_t3226471752 * get_onComplete_0() const { return ___onComplete_0; }
	inline Action_t3226471752 ** get_address_of_onComplete_0() { return &___onComplete_0; }
	inline void set_onComplete_0(Action_t3226471752 * value)
	{
		___onComplete_0 = value;
		Il2CppCodeGenWriteBarrier((&___onComplete_0), value);
	}

	inline static int32_t get_offset_of_onFailed_1() { return static_cast<int32_t>(offsetof(U3CLoadAsyncU3Ec__Iterator0_t2893323214, ___onFailed_1)); }
	inline Action_t3226471752 * get_onFailed_1() const { return ___onFailed_1; }
	inline Action_t3226471752 ** get_address_of_onFailed_1() { return &___onFailed_1; }
	inline void set_onFailed_1(Action_t3226471752 * value)
	{
		___onFailed_1 = value;
		Il2CppCodeGenWriteBarrier((&___onFailed_1), value);
	}

	inline static int32_t get_offset_of_U24this_2() { return static_cast<int32_t>(offsetof(U3CLoadAsyncU3Ec__Iterator0_t2893323214, ___U24this_2)); }
	inline AssetFileUtage_t3568203916 * get_U24this_2() const { return ___U24this_2; }
	inline AssetFileUtage_t3568203916 ** get_address_of_U24this_2() { return &___U24this_2; }
	inline void set_U24this_2(AssetFileUtage_t3568203916 * value)
	{
		___U24this_2 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_2), value);
	}

	inline static int32_t get_offset_of_U24current_3() { return static_cast<int32_t>(offsetof(U3CLoadAsyncU3Ec__Iterator0_t2893323214, ___U24current_3)); }
	inline RuntimeObject * get_U24current_3() const { return ___U24current_3; }
	inline RuntimeObject ** get_address_of_U24current_3() { return &___U24current_3; }
	inline void set_U24current_3(RuntimeObject * value)
	{
		___U24current_3 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_3), value);
	}

	inline static int32_t get_offset_of_U24disposing_4() { return static_cast<int32_t>(offsetof(U3CLoadAsyncU3Ec__Iterator0_t2893323214, ___U24disposing_4)); }
	inline bool get_U24disposing_4() const { return ___U24disposing_4; }
	inline bool* get_address_of_U24disposing_4() { return &___U24disposing_4; }
	inline void set_U24disposing_4(bool value)
	{
		___U24disposing_4 = value;
	}

	inline static int32_t get_offset_of_U24PC_5() { return static_cast<int32_t>(offsetof(U3CLoadAsyncU3Ec__Iterator0_t2893323214, ___U24PC_5)); }
	inline int32_t get_U24PC_5() const { return ___U24PC_5; }
	inline int32_t* get_address_of_U24PC_5() { return &___U24PC_5; }
	inline void set_U24PC_5(int32_t value)
	{
		___U24PC_5 = value;
	}

	inline static int32_t get_offset_of_U24locvar0_6() { return static_cast<int32_t>(offsetof(U3CLoadAsyncU3Ec__Iterator0_t2893323214, ___U24locvar0_6)); }
	inline U3CLoadAsyncU3Ec__AnonStorey5_t4158068117 * get_U24locvar0_6() const { return ___U24locvar0_6; }
	inline U3CLoadAsyncU3Ec__AnonStorey5_t4158068117 ** get_address_of_U24locvar0_6() { return &___U24locvar0_6; }
	inline void set_U24locvar0_6(U3CLoadAsyncU3Ec__AnonStorey5_t4158068117 * value)
	{
		___U24locvar0_6 = value;
		Il2CppCodeGenWriteBarrier((&___U24locvar0_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CLOADASYNCU3EC__ITERATOR0_T2893323214_H
#ifndef ASSETFILEDUMMYONLOADERROR_T419678579_H
#define ASSETFILEDUMMYONLOADERROR_T419678579_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.AssetFileDummyOnLoadError
struct  AssetFileDummyOnLoadError_t419678579  : public RuntimeObject
{
public:
	// System.Boolean Utage.AssetFileDummyOnLoadError::isEnable
	bool ___isEnable_0;
	// System.Boolean Utage.AssetFileDummyOnLoadError::outputErrorLog
	bool ___outputErrorLog_1;
	// UnityEngine.Texture2D Utage.AssetFileDummyOnLoadError::texture
	Texture2D_t3542995729 * ___texture_2;
	// UnityEngine.AudioClip Utage.AssetFileDummyOnLoadError::sound
	AudioClip_t1932558630 * ___sound_3;
	// UnityEngine.TextAsset Utage.AssetFileDummyOnLoadError::text
	TextAsset_t3973159845 * ___text_4;
	// UnityEngine.Object Utage.AssetFileDummyOnLoadError::asset
	Object_t1021602117 * ___asset_5;

public:
	inline static int32_t get_offset_of_isEnable_0() { return static_cast<int32_t>(offsetof(AssetFileDummyOnLoadError_t419678579, ___isEnable_0)); }
	inline bool get_isEnable_0() const { return ___isEnable_0; }
	inline bool* get_address_of_isEnable_0() { return &___isEnable_0; }
	inline void set_isEnable_0(bool value)
	{
		___isEnable_0 = value;
	}

	inline static int32_t get_offset_of_outputErrorLog_1() { return static_cast<int32_t>(offsetof(AssetFileDummyOnLoadError_t419678579, ___outputErrorLog_1)); }
	inline bool get_outputErrorLog_1() const { return ___outputErrorLog_1; }
	inline bool* get_address_of_outputErrorLog_1() { return &___outputErrorLog_1; }
	inline void set_outputErrorLog_1(bool value)
	{
		___outputErrorLog_1 = value;
	}

	inline static int32_t get_offset_of_texture_2() { return static_cast<int32_t>(offsetof(AssetFileDummyOnLoadError_t419678579, ___texture_2)); }
	inline Texture2D_t3542995729 * get_texture_2() const { return ___texture_2; }
	inline Texture2D_t3542995729 ** get_address_of_texture_2() { return &___texture_2; }
	inline void set_texture_2(Texture2D_t3542995729 * value)
	{
		___texture_2 = value;
		Il2CppCodeGenWriteBarrier((&___texture_2), value);
	}

	inline static int32_t get_offset_of_sound_3() { return static_cast<int32_t>(offsetof(AssetFileDummyOnLoadError_t419678579, ___sound_3)); }
	inline AudioClip_t1932558630 * get_sound_3() const { return ___sound_3; }
	inline AudioClip_t1932558630 ** get_address_of_sound_3() { return &___sound_3; }
	inline void set_sound_3(AudioClip_t1932558630 * value)
	{
		___sound_3 = value;
		Il2CppCodeGenWriteBarrier((&___sound_3), value);
	}

	inline static int32_t get_offset_of_text_4() { return static_cast<int32_t>(offsetof(AssetFileDummyOnLoadError_t419678579, ___text_4)); }
	inline TextAsset_t3973159845 * get_text_4() const { return ___text_4; }
	inline TextAsset_t3973159845 ** get_address_of_text_4() { return &___text_4; }
	inline void set_text_4(TextAsset_t3973159845 * value)
	{
		___text_4 = value;
		Il2CppCodeGenWriteBarrier((&___text_4), value);
	}

	inline static int32_t get_offset_of_asset_5() { return static_cast<int32_t>(offsetof(AssetFileDummyOnLoadError_t419678579, ___asset_5)); }
	inline Object_t1021602117 * get_asset_5() const { return ___asset_5; }
	inline Object_t1021602117 ** get_address_of_asset_5() { return &___asset_5; }
	inline void set_asset_5(Object_t1021602117 * value)
	{
		___asset_5 = value;
		Il2CppCodeGenWriteBarrier((&___asset_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ASSETFILEDUMMYONLOADERROR_T419678579_H
#ifndef U3CCALLBACKFILELOADERRORU3EC__ANONSTOREY4_T834973050_H
#define U3CCALLBACKFILELOADERRORU3EC__ANONSTOREY4_T834973050_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.AssetFileManager/<CallbackFileLoadError>c__AnonStorey4
struct  U3CCallbackFileLoadErrorU3Ec__AnonStorey4_t834973050  : public RuntimeObject
{
public:
	// Utage.AssetFileBase Utage.AssetFileManager/<CallbackFileLoadError>c__AnonStorey4::errorFile
	AssetFileBase_t4101360697 * ___errorFile_0;
	// Utage.AssetFileManager Utage.AssetFileManager/<CallbackFileLoadError>c__AnonStorey4::$this
	AssetFileManager_t1095395563 * ___U24this_1;

public:
	inline static int32_t get_offset_of_errorFile_0() { return static_cast<int32_t>(offsetof(U3CCallbackFileLoadErrorU3Ec__AnonStorey4_t834973050, ___errorFile_0)); }
	inline AssetFileBase_t4101360697 * get_errorFile_0() const { return ___errorFile_0; }
	inline AssetFileBase_t4101360697 ** get_address_of_errorFile_0() { return &___errorFile_0; }
	inline void set_errorFile_0(AssetFileBase_t4101360697 * value)
	{
		___errorFile_0 = value;
		Il2CppCodeGenWriteBarrier((&___errorFile_0), value);
	}

	inline static int32_t get_offset_of_U24this_1() { return static_cast<int32_t>(offsetof(U3CCallbackFileLoadErrorU3Ec__AnonStorey4_t834973050, ___U24this_1)); }
	inline AssetFileManager_t1095395563 * get_U24this_1() const { return ___U24this_1; }
	inline AssetFileManager_t1095395563 ** get_address_of_U24this_1() { return &___U24this_1; }
	inline void set_U24this_1(AssetFileManager_t1095395563 * value)
	{
		___U24this_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CCALLBACKFILELOADERRORU3EC__ANONSTOREY4_T834973050_H
#ifndef U3CCOWAITRETRYU3EC__ITERATOR0_T361625508_H
#define U3CCOWAITRETRYU3EC__ITERATOR0_T361625508_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.AssetFileManager/<CoWaitRetry>c__Iterator0
struct  U3CCoWaitRetryU3Ec__Iterator0_t361625508  : public RuntimeObject
{
public:
	// Utage.AssetFileBase Utage.AssetFileManager/<CoWaitRetry>c__Iterator0::file
	AssetFileBase_t4101360697 * ___file_0;
	// Utage.AssetFileManager Utage.AssetFileManager/<CoWaitRetry>c__Iterator0::$this
	AssetFileManager_t1095395563 * ___U24this_1;
	// System.Object Utage.AssetFileManager/<CoWaitRetry>c__Iterator0::$current
	RuntimeObject * ___U24current_2;
	// System.Boolean Utage.AssetFileManager/<CoWaitRetry>c__Iterator0::$disposing
	bool ___U24disposing_3;
	// System.Int32 Utage.AssetFileManager/<CoWaitRetry>c__Iterator0::$PC
	int32_t ___U24PC_4;

public:
	inline static int32_t get_offset_of_file_0() { return static_cast<int32_t>(offsetof(U3CCoWaitRetryU3Ec__Iterator0_t361625508, ___file_0)); }
	inline AssetFileBase_t4101360697 * get_file_0() const { return ___file_0; }
	inline AssetFileBase_t4101360697 ** get_address_of_file_0() { return &___file_0; }
	inline void set_file_0(AssetFileBase_t4101360697 * value)
	{
		___file_0 = value;
		Il2CppCodeGenWriteBarrier((&___file_0), value);
	}

	inline static int32_t get_offset_of_U24this_1() { return static_cast<int32_t>(offsetof(U3CCoWaitRetryU3Ec__Iterator0_t361625508, ___U24this_1)); }
	inline AssetFileManager_t1095395563 * get_U24this_1() const { return ___U24this_1; }
	inline AssetFileManager_t1095395563 ** get_address_of_U24this_1() { return &___U24this_1; }
	inline void set_U24this_1(AssetFileManager_t1095395563 * value)
	{
		___U24this_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_1), value);
	}

	inline static int32_t get_offset_of_U24current_2() { return static_cast<int32_t>(offsetof(U3CCoWaitRetryU3Ec__Iterator0_t361625508, ___U24current_2)); }
	inline RuntimeObject * get_U24current_2() const { return ___U24current_2; }
	inline RuntimeObject ** get_address_of_U24current_2() { return &___U24current_2; }
	inline void set_U24current_2(RuntimeObject * value)
	{
		___U24current_2 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_2), value);
	}

	inline static int32_t get_offset_of_U24disposing_3() { return static_cast<int32_t>(offsetof(U3CCoWaitRetryU3Ec__Iterator0_t361625508, ___U24disposing_3)); }
	inline bool get_U24disposing_3() const { return ___U24disposing_3; }
	inline bool* get_address_of_U24disposing_3() { return &___U24disposing_3; }
	inline void set_U24disposing_3(bool value)
	{
		___U24disposing_3 = value;
	}

	inline static int32_t get_offset_of_U24PC_4() { return static_cast<int32_t>(offsetof(U3CCoWaitRetryU3Ec__Iterator0_t361625508, ___U24PC_4)); }
	inline int32_t get_U24PC_4() const { return ___U24PC_4; }
	inline int32_t* get_address_of_U24PC_4() { return &___U24PC_4; }
	inline void set_U24PC_4(int32_t value)
	{
		___U24PC_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CCOWAITRETRYU3EC__ITERATOR0_T361625508_H
#ifndef U3CCOLOADWAITU3EC__ITERATOR1_T578320303_H
#define U3CCOLOADWAITU3EC__ITERATOR1_T578320303_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.AssetFileManager/<CoLoadWait>c__Iterator1
struct  U3CCoLoadWaitU3Ec__Iterator1_t578320303  : public RuntimeObject
{
public:
	// Utage.AssetFileBase Utage.AssetFileManager/<CoLoadWait>c__Iterator1::file
	AssetFileBase_t4101360697 * ___file_0;
	// System.Action`1<Utage.AssetFile> Utage.AssetFileManager/<CoLoadWait>c__Iterator1::onComplete
	Action_1_t3184812638 * ___onComplete_1;
	// Utage.AssetFileManager Utage.AssetFileManager/<CoLoadWait>c__Iterator1::$this
	AssetFileManager_t1095395563 * ___U24this_2;
	// System.Object Utage.AssetFileManager/<CoLoadWait>c__Iterator1::$current
	RuntimeObject * ___U24current_3;
	// System.Boolean Utage.AssetFileManager/<CoLoadWait>c__Iterator1::$disposing
	bool ___U24disposing_4;
	// System.Int32 Utage.AssetFileManager/<CoLoadWait>c__Iterator1::$PC
	int32_t ___U24PC_5;

public:
	inline static int32_t get_offset_of_file_0() { return static_cast<int32_t>(offsetof(U3CCoLoadWaitU3Ec__Iterator1_t578320303, ___file_0)); }
	inline AssetFileBase_t4101360697 * get_file_0() const { return ___file_0; }
	inline AssetFileBase_t4101360697 ** get_address_of_file_0() { return &___file_0; }
	inline void set_file_0(AssetFileBase_t4101360697 * value)
	{
		___file_0 = value;
		Il2CppCodeGenWriteBarrier((&___file_0), value);
	}

	inline static int32_t get_offset_of_onComplete_1() { return static_cast<int32_t>(offsetof(U3CCoLoadWaitU3Ec__Iterator1_t578320303, ___onComplete_1)); }
	inline Action_1_t3184812638 * get_onComplete_1() const { return ___onComplete_1; }
	inline Action_1_t3184812638 ** get_address_of_onComplete_1() { return &___onComplete_1; }
	inline void set_onComplete_1(Action_1_t3184812638 * value)
	{
		___onComplete_1 = value;
		Il2CppCodeGenWriteBarrier((&___onComplete_1), value);
	}

	inline static int32_t get_offset_of_U24this_2() { return static_cast<int32_t>(offsetof(U3CCoLoadWaitU3Ec__Iterator1_t578320303, ___U24this_2)); }
	inline AssetFileManager_t1095395563 * get_U24this_2() const { return ___U24this_2; }
	inline AssetFileManager_t1095395563 ** get_address_of_U24this_2() { return &___U24this_2; }
	inline void set_U24this_2(AssetFileManager_t1095395563 * value)
	{
		___U24this_2 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_2), value);
	}

	inline static int32_t get_offset_of_U24current_3() { return static_cast<int32_t>(offsetof(U3CCoLoadWaitU3Ec__Iterator1_t578320303, ___U24current_3)); }
	inline RuntimeObject * get_U24current_3() const { return ___U24current_3; }
	inline RuntimeObject ** get_address_of_U24current_3() { return &___U24current_3; }
	inline void set_U24current_3(RuntimeObject * value)
	{
		___U24current_3 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_3), value);
	}

	inline static int32_t get_offset_of_U24disposing_4() { return static_cast<int32_t>(offsetof(U3CCoLoadWaitU3Ec__Iterator1_t578320303, ___U24disposing_4)); }
	inline bool get_U24disposing_4() const { return ___U24disposing_4; }
	inline bool* get_address_of_U24disposing_4() { return &___U24disposing_4; }
	inline void set_U24disposing_4(bool value)
	{
		___U24disposing_4 = value;
	}

	inline static int32_t get_offset_of_U24PC_5() { return static_cast<int32_t>(offsetof(U3CCoLoadWaitU3Ec__Iterator1_t578320303, ___U24PC_5)); }
	inline int32_t get_U24PC_5() const { return ___U24PC_5; }
	inline int32_t* get_address_of_U24PC_5() { return &___U24PC_5; }
	inline void set_U24PC_5(int32_t value)
	{
		___U24PC_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CCOLOADWAITU3EC__ITERATOR1_T578320303_H
#ifndef U3CLOADASYNCSUBU3EC__ITERATOR0_T3752071966_H
#define U3CLOADASYNCSUBU3EC__ITERATOR0_T3752071966_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.WWWEx/<LoadAsyncSub>c__Iterator0
struct  U3CLoadAsyncSubU3Ec__Iterator0_t3752071966  : public RuntimeObject
{
public:
	// System.Boolean Utage.WWWEx/<LoadAsyncSub>c__Iterator0::<retry>__0
	bool ___U3CretryU3E__0_0;
	// UnityEngine.WWW Utage.WWWEx/<LoadAsyncSub>c__Iterator0::<www>__1
	WWW_t2919945039 * ___U3CwwwU3E__1_1;
	// System.Single Utage.WWWEx/<LoadAsyncSub>c__Iterator0::<time>__2
	float ___U3CtimeU3E__2_2;
	// System.Boolean Utage.WWWEx/<LoadAsyncSub>c__Iterator0::<isTimeOut>__2
	bool ___U3CisTimeOutU3E__2_3;
	// System.Int32 Utage.WWWEx/<LoadAsyncSub>c__Iterator0::retryCount
	int32_t ___retryCount_4;
	// System.Action`1<UnityEngine.WWW> Utage.WWWEx/<LoadAsyncSub>c__Iterator0::onTimeOut
	Action_1_t2721744421 * ___onTimeOut_5;
	// System.Action`1<UnityEngine.WWW> Utage.WWWEx/<LoadAsyncSub>c__Iterator0::onFailed
	Action_1_t2721744421 * ___onFailed_6;
	// System.Action`1<UnityEngine.WWW> Utage.WWWEx/<LoadAsyncSub>c__Iterator0::onCopmlete
	Action_1_t2721744421 * ___onCopmlete_7;
	// Utage.WWWEx Utage.WWWEx/<LoadAsyncSub>c__Iterator0::$this
	WWWEx_t1775400116 * ___U24this_8;
	// System.Object Utage.WWWEx/<LoadAsyncSub>c__Iterator0::$current
	RuntimeObject * ___U24current_9;
	// System.Boolean Utage.WWWEx/<LoadAsyncSub>c__Iterator0::$disposing
	bool ___U24disposing_10;
	// System.Int32 Utage.WWWEx/<LoadAsyncSub>c__Iterator0::$PC
	int32_t ___U24PC_11;

public:
	inline static int32_t get_offset_of_U3CretryU3E__0_0() { return static_cast<int32_t>(offsetof(U3CLoadAsyncSubU3Ec__Iterator0_t3752071966, ___U3CretryU3E__0_0)); }
	inline bool get_U3CretryU3E__0_0() const { return ___U3CretryU3E__0_0; }
	inline bool* get_address_of_U3CretryU3E__0_0() { return &___U3CretryU3E__0_0; }
	inline void set_U3CretryU3E__0_0(bool value)
	{
		___U3CretryU3E__0_0 = value;
	}

	inline static int32_t get_offset_of_U3CwwwU3E__1_1() { return static_cast<int32_t>(offsetof(U3CLoadAsyncSubU3Ec__Iterator0_t3752071966, ___U3CwwwU3E__1_1)); }
	inline WWW_t2919945039 * get_U3CwwwU3E__1_1() const { return ___U3CwwwU3E__1_1; }
	inline WWW_t2919945039 ** get_address_of_U3CwwwU3E__1_1() { return &___U3CwwwU3E__1_1; }
	inline void set_U3CwwwU3E__1_1(WWW_t2919945039 * value)
	{
		___U3CwwwU3E__1_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CwwwU3E__1_1), value);
	}

	inline static int32_t get_offset_of_U3CtimeU3E__2_2() { return static_cast<int32_t>(offsetof(U3CLoadAsyncSubU3Ec__Iterator0_t3752071966, ___U3CtimeU3E__2_2)); }
	inline float get_U3CtimeU3E__2_2() const { return ___U3CtimeU3E__2_2; }
	inline float* get_address_of_U3CtimeU3E__2_2() { return &___U3CtimeU3E__2_2; }
	inline void set_U3CtimeU3E__2_2(float value)
	{
		___U3CtimeU3E__2_2 = value;
	}

	inline static int32_t get_offset_of_U3CisTimeOutU3E__2_3() { return static_cast<int32_t>(offsetof(U3CLoadAsyncSubU3Ec__Iterator0_t3752071966, ___U3CisTimeOutU3E__2_3)); }
	inline bool get_U3CisTimeOutU3E__2_3() const { return ___U3CisTimeOutU3E__2_3; }
	inline bool* get_address_of_U3CisTimeOutU3E__2_3() { return &___U3CisTimeOutU3E__2_3; }
	inline void set_U3CisTimeOutU3E__2_3(bool value)
	{
		___U3CisTimeOutU3E__2_3 = value;
	}

	inline static int32_t get_offset_of_retryCount_4() { return static_cast<int32_t>(offsetof(U3CLoadAsyncSubU3Ec__Iterator0_t3752071966, ___retryCount_4)); }
	inline int32_t get_retryCount_4() const { return ___retryCount_4; }
	inline int32_t* get_address_of_retryCount_4() { return &___retryCount_4; }
	inline void set_retryCount_4(int32_t value)
	{
		___retryCount_4 = value;
	}

	inline static int32_t get_offset_of_onTimeOut_5() { return static_cast<int32_t>(offsetof(U3CLoadAsyncSubU3Ec__Iterator0_t3752071966, ___onTimeOut_5)); }
	inline Action_1_t2721744421 * get_onTimeOut_5() const { return ___onTimeOut_5; }
	inline Action_1_t2721744421 ** get_address_of_onTimeOut_5() { return &___onTimeOut_5; }
	inline void set_onTimeOut_5(Action_1_t2721744421 * value)
	{
		___onTimeOut_5 = value;
		Il2CppCodeGenWriteBarrier((&___onTimeOut_5), value);
	}

	inline static int32_t get_offset_of_onFailed_6() { return static_cast<int32_t>(offsetof(U3CLoadAsyncSubU3Ec__Iterator0_t3752071966, ___onFailed_6)); }
	inline Action_1_t2721744421 * get_onFailed_6() const { return ___onFailed_6; }
	inline Action_1_t2721744421 ** get_address_of_onFailed_6() { return &___onFailed_6; }
	inline void set_onFailed_6(Action_1_t2721744421 * value)
	{
		___onFailed_6 = value;
		Il2CppCodeGenWriteBarrier((&___onFailed_6), value);
	}

	inline static int32_t get_offset_of_onCopmlete_7() { return static_cast<int32_t>(offsetof(U3CLoadAsyncSubU3Ec__Iterator0_t3752071966, ___onCopmlete_7)); }
	inline Action_1_t2721744421 * get_onCopmlete_7() const { return ___onCopmlete_7; }
	inline Action_1_t2721744421 ** get_address_of_onCopmlete_7() { return &___onCopmlete_7; }
	inline void set_onCopmlete_7(Action_1_t2721744421 * value)
	{
		___onCopmlete_7 = value;
		Il2CppCodeGenWriteBarrier((&___onCopmlete_7), value);
	}

	inline static int32_t get_offset_of_U24this_8() { return static_cast<int32_t>(offsetof(U3CLoadAsyncSubU3Ec__Iterator0_t3752071966, ___U24this_8)); }
	inline WWWEx_t1775400116 * get_U24this_8() const { return ___U24this_8; }
	inline WWWEx_t1775400116 ** get_address_of_U24this_8() { return &___U24this_8; }
	inline void set_U24this_8(WWWEx_t1775400116 * value)
	{
		___U24this_8 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_8), value);
	}

	inline static int32_t get_offset_of_U24current_9() { return static_cast<int32_t>(offsetof(U3CLoadAsyncSubU3Ec__Iterator0_t3752071966, ___U24current_9)); }
	inline RuntimeObject * get_U24current_9() const { return ___U24current_9; }
	inline RuntimeObject ** get_address_of_U24current_9() { return &___U24current_9; }
	inline void set_U24current_9(RuntimeObject * value)
	{
		___U24current_9 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_9), value);
	}

	inline static int32_t get_offset_of_U24disposing_10() { return static_cast<int32_t>(offsetof(U3CLoadAsyncSubU3Ec__Iterator0_t3752071966, ___U24disposing_10)); }
	inline bool get_U24disposing_10() const { return ___U24disposing_10; }
	inline bool* get_address_of_U24disposing_10() { return &___U24disposing_10; }
	inline void set_U24disposing_10(bool value)
	{
		___U24disposing_10 = value;
	}

	inline static int32_t get_offset_of_U24PC_11() { return static_cast<int32_t>(offsetof(U3CLoadAsyncSubU3Ec__Iterator0_t3752071966, ___U24PC_11)); }
	inline int32_t get_U24PC_11() const { return ___U24PC_11; }
	inline int32_t* get_address_of_U24PC_11() { return &___U24PC_11; }
	inline void set_U24PC_11(int32_t value)
	{
		___U24PC_11 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CLOADASYNCSUBU3EC__ITERATOR0_T3752071966_H
#ifndef DATA_T619954766_H
#define DATA_T619954766_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.MiniAnimationData/Data
struct  Data_t619954766  : public RuntimeObject
{
public:
	// System.Single Utage.MiniAnimationData/Data::duration
	float ___duration_0;
	// System.String Utage.MiniAnimationData/Data::name
	String_t* ___name_1;

public:
	inline static int32_t get_offset_of_duration_0() { return static_cast<int32_t>(offsetof(Data_t619954766, ___duration_0)); }
	inline float get_duration_0() const { return ___duration_0; }
	inline float* get_address_of_duration_0() { return &___duration_0; }
	inline void set_duration_0(float value)
	{
		___duration_0 = value;
	}

	inline static int32_t get_offset_of_name_1() { return static_cast<int32_t>(offsetof(Data_t619954766, ___name_1)); }
	inline String_t* get_name_1() const { return ___name_1; }
	inline String_t** get_address_of_name_1() { return &___name_1; }
	inline void set_name_1(String_t* value)
	{
		___name_1 = value;
		Il2CppCodeGenWriteBarrier((&___name_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DATA_T619954766_H
#ifndef MINIANIMATIONDATA_T521227391_H
#define MINIANIMATIONDATA_T521227391_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.MiniAnimationData
struct  MiniAnimationData_t521227391  : public RuntimeObject
{
public:
	// System.Collections.Generic.List`1<Utage.MiniAnimationData/Data> Utage.MiniAnimationData::dataList
	List_1_t4284043194 * ___dataList_0;

public:
	inline static int32_t get_offset_of_dataList_0() { return static_cast<int32_t>(offsetof(MiniAnimationData_t521227391, ___dataList_0)); }
	inline List_1_t4284043194 * get_dataList_0() const { return ___dataList_0; }
	inline List_1_t4284043194 ** get_address_of_dataList_0() { return &___dataList_0; }
	inline void set_dataList_0(List_1_t4284043194 * value)
	{
		___dataList_0 = value;
		Il2CppCodeGenWriteBarrier((&___dataList_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MINIANIMATIONDATA_T521227391_H
#ifndef U3CCOUPDATEU3EC__ITERATOR0_T418606427_H
#define U3CCOUPDATEU3EC__ITERATOR0_T418606427_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.LinkTransform/<CoUpdate>c__Iterator0
struct  U3CCoUpdateU3Ec__Iterator0_t418606427  : public RuntimeObject
{
public:
	// UnityEngine.RectTransform Utage.LinkTransform/<CoUpdate>c__Iterator0::<rectTransform>__1
	RectTransform_t3349966182 * ___U3CrectTransformU3E__1_0;
	// Utage.LinkTransform Utage.LinkTransform/<CoUpdate>c__Iterator0::$this
	LinkTransform_t2861363074 * ___U24this_1;
	// System.Object Utage.LinkTransform/<CoUpdate>c__Iterator0::$current
	RuntimeObject * ___U24current_2;
	// System.Boolean Utage.LinkTransform/<CoUpdate>c__Iterator0::$disposing
	bool ___U24disposing_3;
	// System.Int32 Utage.LinkTransform/<CoUpdate>c__Iterator0::$PC
	int32_t ___U24PC_4;

public:
	inline static int32_t get_offset_of_U3CrectTransformU3E__1_0() { return static_cast<int32_t>(offsetof(U3CCoUpdateU3Ec__Iterator0_t418606427, ___U3CrectTransformU3E__1_0)); }
	inline RectTransform_t3349966182 * get_U3CrectTransformU3E__1_0() const { return ___U3CrectTransformU3E__1_0; }
	inline RectTransform_t3349966182 ** get_address_of_U3CrectTransformU3E__1_0() { return &___U3CrectTransformU3E__1_0; }
	inline void set_U3CrectTransformU3E__1_0(RectTransform_t3349966182 * value)
	{
		___U3CrectTransformU3E__1_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CrectTransformU3E__1_0), value);
	}

	inline static int32_t get_offset_of_U24this_1() { return static_cast<int32_t>(offsetof(U3CCoUpdateU3Ec__Iterator0_t418606427, ___U24this_1)); }
	inline LinkTransform_t2861363074 * get_U24this_1() const { return ___U24this_1; }
	inline LinkTransform_t2861363074 ** get_address_of_U24this_1() { return &___U24this_1; }
	inline void set_U24this_1(LinkTransform_t2861363074 * value)
	{
		___U24this_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_1), value);
	}

	inline static int32_t get_offset_of_U24current_2() { return static_cast<int32_t>(offsetof(U3CCoUpdateU3Ec__Iterator0_t418606427, ___U24current_2)); }
	inline RuntimeObject * get_U24current_2() const { return ___U24current_2; }
	inline RuntimeObject ** get_address_of_U24current_2() { return &___U24current_2; }
	inline void set_U24current_2(RuntimeObject * value)
	{
		___U24current_2 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_2), value);
	}

	inline static int32_t get_offset_of_U24disposing_3() { return static_cast<int32_t>(offsetof(U3CCoUpdateU3Ec__Iterator0_t418606427, ___U24disposing_3)); }
	inline bool get_U24disposing_3() const { return ___U24disposing_3; }
	inline bool* get_address_of_U24disposing_3() { return &___U24disposing_3; }
	inline void set_U24disposing_3(bool value)
	{
		___U24disposing_3 = value;
	}

	inline static int32_t get_offset_of_U24PC_4() { return static_cast<int32_t>(offsetof(U3CCoUpdateU3Ec__Iterator0_t418606427, ___U24PC_4)); }
	inline int32_t get_U24PC_4() const { return ___U24PC_4; }
	inline int32_t* get_address_of_U24PC_4() { return &___U24PC_4; }
	inline void set_U24PC_4(int32_t value)
	{
		___U24PC_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CCOUPDATEU3EC__ITERATOR0_T418606427_H
#ifndef U3CCOANIMATIONU3EC__ITERATOR0_T1277845393_H
#define U3CCOANIMATIONU3EC__ITERATOR0_T1277845393_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.CurveAnimation/<CoAnimation>c__Iterator0
struct  U3CCoAnimationU3Ec__Iterator0_t1277845393  : public RuntimeObject
{
public:
	// System.Single Utage.CurveAnimation/<CoAnimation>c__Iterator0::<delayStartTime>__1
	float ___U3CdelayStartTimeU3E__1_0;
	// System.Single Utage.CurveAnimation/<CoAnimation>c__Iterator0::<endTime>__0
	float ___U3CendTimeU3E__0_1;
	// System.Single Utage.CurveAnimation/<CoAnimation>c__Iterator0::<startTime>__0
	float ___U3CstartTimeU3E__0_2;
	// System.Action`1<System.Single> Utage.CurveAnimation/<CoAnimation>c__Iterator0::onUpdate
	Action_1_t1878309314 * ___onUpdate_3;
	// System.Action Utage.CurveAnimation/<CoAnimation>c__Iterator0::onComplete
	Action_t3226471752 * ___onComplete_4;
	// Utage.CurveAnimation Utage.CurveAnimation/<CoAnimation>c__Iterator0::$this
	CurveAnimation_t2521618615 * ___U24this_5;
	// System.Object Utage.CurveAnimation/<CoAnimation>c__Iterator0::$current
	RuntimeObject * ___U24current_6;
	// System.Boolean Utage.CurveAnimation/<CoAnimation>c__Iterator0::$disposing
	bool ___U24disposing_7;
	// System.Int32 Utage.CurveAnimation/<CoAnimation>c__Iterator0::$PC
	int32_t ___U24PC_8;

public:
	inline static int32_t get_offset_of_U3CdelayStartTimeU3E__1_0() { return static_cast<int32_t>(offsetof(U3CCoAnimationU3Ec__Iterator0_t1277845393, ___U3CdelayStartTimeU3E__1_0)); }
	inline float get_U3CdelayStartTimeU3E__1_0() const { return ___U3CdelayStartTimeU3E__1_0; }
	inline float* get_address_of_U3CdelayStartTimeU3E__1_0() { return &___U3CdelayStartTimeU3E__1_0; }
	inline void set_U3CdelayStartTimeU3E__1_0(float value)
	{
		___U3CdelayStartTimeU3E__1_0 = value;
	}

	inline static int32_t get_offset_of_U3CendTimeU3E__0_1() { return static_cast<int32_t>(offsetof(U3CCoAnimationU3Ec__Iterator0_t1277845393, ___U3CendTimeU3E__0_1)); }
	inline float get_U3CendTimeU3E__0_1() const { return ___U3CendTimeU3E__0_1; }
	inline float* get_address_of_U3CendTimeU3E__0_1() { return &___U3CendTimeU3E__0_1; }
	inline void set_U3CendTimeU3E__0_1(float value)
	{
		___U3CendTimeU3E__0_1 = value;
	}

	inline static int32_t get_offset_of_U3CstartTimeU3E__0_2() { return static_cast<int32_t>(offsetof(U3CCoAnimationU3Ec__Iterator0_t1277845393, ___U3CstartTimeU3E__0_2)); }
	inline float get_U3CstartTimeU3E__0_2() const { return ___U3CstartTimeU3E__0_2; }
	inline float* get_address_of_U3CstartTimeU3E__0_2() { return &___U3CstartTimeU3E__0_2; }
	inline void set_U3CstartTimeU3E__0_2(float value)
	{
		___U3CstartTimeU3E__0_2 = value;
	}

	inline static int32_t get_offset_of_onUpdate_3() { return static_cast<int32_t>(offsetof(U3CCoAnimationU3Ec__Iterator0_t1277845393, ___onUpdate_3)); }
	inline Action_1_t1878309314 * get_onUpdate_3() const { return ___onUpdate_3; }
	inline Action_1_t1878309314 ** get_address_of_onUpdate_3() { return &___onUpdate_3; }
	inline void set_onUpdate_3(Action_1_t1878309314 * value)
	{
		___onUpdate_3 = value;
		Il2CppCodeGenWriteBarrier((&___onUpdate_3), value);
	}

	inline static int32_t get_offset_of_onComplete_4() { return static_cast<int32_t>(offsetof(U3CCoAnimationU3Ec__Iterator0_t1277845393, ___onComplete_4)); }
	inline Action_t3226471752 * get_onComplete_4() const { return ___onComplete_4; }
	inline Action_t3226471752 ** get_address_of_onComplete_4() { return &___onComplete_4; }
	inline void set_onComplete_4(Action_t3226471752 * value)
	{
		___onComplete_4 = value;
		Il2CppCodeGenWriteBarrier((&___onComplete_4), value);
	}

	inline static int32_t get_offset_of_U24this_5() { return static_cast<int32_t>(offsetof(U3CCoAnimationU3Ec__Iterator0_t1277845393, ___U24this_5)); }
	inline CurveAnimation_t2521618615 * get_U24this_5() const { return ___U24this_5; }
	inline CurveAnimation_t2521618615 ** get_address_of_U24this_5() { return &___U24this_5; }
	inline void set_U24this_5(CurveAnimation_t2521618615 * value)
	{
		___U24this_5 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_5), value);
	}

	inline static int32_t get_offset_of_U24current_6() { return static_cast<int32_t>(offsetof(U3CCoAnimationU3Ec__Iterator0_t1277845393, ___U24current_6)); }
	inline RuntimeObject * get_U24current_6() const { return ___U24current_6; }
	inline RuntimeObject ** get_address_of_U24current_6() { return &___U24current_6; }
	inline void set_U24current_6(RuntimeObject * value)
	{
		___U24current_6 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_6), value);
	}

	inline static int32_t get_offset_of_U24disposing_7() { return static_cast<int32_t>(offsetof(U3CCoAnimationU3Ec__Iterator0_t1277845393, ___U24disposing_7)); }
	inline bool get_U24disposing_7() const { return ___U24disposing_7; }
	inline bool* get_address_of_U24disposing_7() { return &___U24disposing_7; }
	inline void set_U24disposing_7(bool value)
	{
		___U24disposing_7 = value;
	}

	inline static int32_t get_offset_of_U24PC_8() { return static_cast<int32_t>(offsetof(U3CCoAnimationU3Ec__Iterator0_t1277845393, ___U24PC_8)); }
	inline int32_t get_U24PC_8() const { return ___U24PC_8; }
	inline int32_t* get_address_of_U24PC_8() { return &___U24PC_8; }
	inline void set_U24PC_8(int32_t value)
	{
		___U24PC_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CCOANIMATIONU3EC__ITERATOR0_T1277845393_H
#ifndef CRYPT_T3650163410_H
#define CRYPT_T3650163410_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.Crypt
struct  Crypt_t3650163410  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CRYPT_T3650163410_H
#ifndef ATTRIBUTE_T542643598_H
#define ATTRIBUTE_T542643598_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Attribute
struct  Attribute_t542643598  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ATTRIBUTE_T542643598_H
#ifndef VALUETYPE_T3507792607_H
#define VALUETYPE_T3507792607_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ValueType
struct  ValueType_t3507792607  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t3507792607_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t3507792607_marshaled_com
{
};
#endif // VALUETYPE_T3507792607_H
#ifndef UNITYEVENTBASE_T828812576_H
#define UNITYEVENTBASE_T828812576_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Events.UnityEventBase
struct  UnityEventBase_t828812576  : public RuntimeObject
{
public:
	// UnityEngine.Events.InvokableCallList UnityEngine.Events.UnityEventBase::m_Calls
	InvokableCallList_t2295673753 * ___m_Calls_0;
	// UnityEngine.Events.PersistentCallGroup UnityEngine.Events.UnityEventBase::m_PersistentCalls
	PersistentCallGroup_t339478082 * ___m_PersistentCalls_1;
	// System.String UnityEngine.Events.UnityEventBase::m_TypeName
	String_t* ___m_TypeName_2;
	// System.Boolean UnityEngine.Events.UnityEventBase::m_CallsDirty
	bool ___m_CallsDirty_3;

public:
	inline static int32_t get_offset_of_m_Calls_0() { return static_cast<int32_t>(offsetof(UnityEventBase_t828812576, ___m_Calls_0)); }
	inline InvokableCallList_t2295673753 * get_m_Calls_0() const { return ___m_Calls_0; }
	inline InvokableCallList_t2295673753 ** get_address_of_m_Calls_0() { return &___m_Calls_0; }
	inline void set_m_Calls_0(InvokableCallList_t2295673753 * value)
	{
		___m_Calls_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_Calls_0), value);
	}

	inline static int32_t get_offset_of_m_PersistentCalls_1() { return static_cast<int32_t>(offsetof(UnityEventBase_t828812576, ___m_PersistentCalls_1)); }
	inline PersistentCallGroup_t339478082 * get_m_PersistentCalls_1() const { return ___m_PersistentCalls_1; }
	inline PersistentCallGroup_t339478082 ** get_address_of_m_PersistentCalls_1() { return &___m_PersistentCalls_1; }
	inline void set_m_PersistentCalls_1(PersistentCallGroup_t339478082 * value)
	{
		___m_PersistentCalls_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_PersistentCalls_1), value);
	}

	inline static int32_t get_offset_of_m_TypeName_2() { return static_cast<int32_t>(offsetof(UnityEventBase_t828812576, ___m_TypeName_2)); }
	inline String_t* get_m_TypeName_2() const { return ___m_TypeName_2; }
	inline String_t** get_address_of_m_TypeName_2() { return &___m_TypeName_2; }
	inline void set_m_TypeName_2(String_t* value)
	{
		___m_TypeName_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_TypeName_2), value);
	}

	inline static int32_t get_offset_of_m_CallsDirty_3() { return static_cast<int32_t>(offsetof(UnityEventBase_t828812576, ___m_CallsDirty_3)); }
	inline bool get_m_CallsDirty_3() const { return ___m_CallsDirty_3; }
	inline bool* get_address_of_m_CallsDirty_3() { return &___m_CallsDirty_3; }
	inline void set_m_CallsDirty_3(bool value)
	{
		___m_CallsDirty_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYEVENTBASE_T828812576_H
#ifndef U3CLOADASYNCU3EC__ANONSTOREY3_T105557981_H
#define U3CLOADASYNCU3EC__ANONSTOREY3_T105557981_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.WWWEx/<LoadAsync>c__AnonStorey3
struct  U3CLoadAsyncU3Ec__AnonStorey3_t105557981  : public RuntimeObject
{
public:
	// System.Action`1<UnityEngine.WWW> Utage.WWWEx/<LoadAsync>c__AnonStorey3::onComplete
	Action_1_t2721744421 * ___onComplete_0;
	// System.Action`1<UnityEngine.WWW> Utage.WWWEx/<LoadAsync>c__AnonStorey3::onFailed
	Action_1_t2721744421 * ___onFailed_1;
	// Utage.WWWEx Utage.WWWEx/<LoadAsync>c__AnonStorey3::$this
	WWWEx_t1775400116 * ___U24this_2;

public:
	inline static int32_t get_offset_of_onComplete_0() { return static_cast<int32_t>(offsetof(U3CLoadAsyncU3Ec__AnonStorey3_t105557981, ___onComplete_0)); }
	inline Action_1_t2721744421 * get_onComplete_0() const { return ___onComplete_0; }
	inline Action_1_t2721744421 ** get_address_of_onComplete_0() { return &___onComplete_0; }
	inline void set_onComplete_0(Action_1_t2721744421 * value)
	{
		___onComplete_0 = value;
		Il2CppCodeGenWriteBarrier((&___onComplete_0), value);
	}

	inline static int32_t get_offset_of_onFailed_1() { return static_cast<int32_t>(offsetof(U3CLoadAsyncU3Ec__AnonStorey3_t105557981, ___onFailed_1)); }
	inline Action_1_t2721744421 * get_onFailed_1() const { return ___onFailed_1; }
	inline Action_1_t2721744421 ** get_address_of_onFailed_1() { return &___onFailed_1; }
	inline void set_onFailed_1(Action_1_t2721744421 * value)
	{
		___onFailed_1 = value;
		Il2CppCodeGenWriteBarrier((&___onFailed_1), value);
	}

	inline static int32_t get_offset_of_U24this_2() { return static_cast<int32_t>(offsetof(U3CLoadAsyncU3Ec__AnonStorey3_t105557981, ___U24this_2)); }
	inline WWWEx_t1775400116 * get_U24this_2() const { return ___U24this_2; }
	inline WWWEx_t1775400116 ** get_address_of_U24this_2() { return &___U24this_2; }
	inline void set_U24this_2(WWWEx_t1775400116 * value)
	{
		___U24this_2 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CLOADASYNCU3EC__ANONSTOREY3_T105557981_H
#ifndef MOTIONPLAYTYPEUTIL_T2936258342_H
#define MOTIONPLAYTYPEUTIL_T2936258342_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.MotionPlayTypeUtil
struct  MotionPlayTypeUtil_t2936258342  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MOTIONPLAYTYPEUTIL_T2936258342_H
#ifndef BUTTONEVENTINFO_T1085784078_H
#define BUTTONEVENTINFO_T1085784078_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.ButtonEventInfo
struct  ButtonEventInfo_t1085784078  : public RuntimeObject
{
public:
	// System.String Utage.ButtonEventInfo::text
	String_t* ___text_0;
	// UnityEngine.Events.UnityAction Utage.ButtonEventInfo::callBackClicked
	UnityAction_t4025899511 * ___callBackClicked_1;

public:
	inline static int32_t get_offset_of_text_0() { return static_cast<int32_t>(offsetof(ButtonEventInfo_t1085784078, ___text_0)); }
	inline String_t* get_text_0() const { return ___text_0; }
	inline String_t** get_address_of_text_0() { return &___text_0; }
	inline void set_text_0(String_t* value)
	{
		___text_0 = value;
		Il2CppCodeGenWriteBarrier((&___text_0), value);
	}

	inline static int32_t get_offset_of_callBackClicked_1() { return static_cast<int32_t>(offsetof(ButtonEventInfo_t1085784078, ___callBackClicked_1)); }
	inline UnityAction_t4025899511 * get_callBackClicked_1() const { return ___callBackClicked_1; }
	inline UnityAction_t4025899511 ** get_address_of_callBackClicked_1() { return &___callBackClicked_1; }
	inline void set_callBackClicked_1(UnityAction_t4025899511 * value)
	{
		___callBackClicked_1 = value;
		Il2CppCodeGenWriteBarrier((&___callBackClicked_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BUTTONEVENTINFO_T1085784078_H
#ifndef UNITYEVENT_1_T3863924733_H
#define UNITYEVENT_1_T3863924733_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Events.UnityEvent`1<System.Boolean>
struct  UnityEvent_1_t3863924733  : public UnityEventBase_t828812576
{
public:
	// System.Object[] UnityEngine.Events.UnityEvent`1::m_InvokeArray
	ObjectU5BU5D_t3614634134* ___m_InvokeArray_4;

public:
	inline static int32_t get_offset_of_m_InvokeArray_4() { return static_cast<int32_t>(offsetof(UnityEvent_1_t3863924733, ___m_InvokeArray_4)); }
	inline ObjectU5BU5D_t3614634134* get_m_InvokeArray_4() const { return ___m_InvokeArray_4; }
	inline ObjectU5BU5D_t3614634134** get_address_of_m_InvokeArray_4() { return &___m_InvokeArray_4; }
	inline void set_m_InvokeArray_4(ObjectU5BU5D_t3614634134* value)
	{
		___m_InvokeArray_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_InvokeArray_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYEVENT_1_T3863924733_H
#ifndef UNITYEVENT_1_T2067570248_H
#define UNITYEVENT_1_T2067570248_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Events.UnityEvent`1<System.String>
struct  UnityEvent_1_t2067570248  : public UnityEventBase_t828812576
{
public:
	// System.Object[] UnityEngine.Events.UnityEvent`1::m_InvokeArray
	ObjectU5BU5D_t3614634134* ___m_InvokeArray_4;

public:
	inline static int32_t get_offset_of_m_InvokeArray_4() { return static_cast<int32_t>(offsetof(UnityEvent_1_t2067570248, ___m_InvokeArray_4)); }
	inline ObjectU5BU5D_t3614634134* get_m_InvokeArray_4() const { return ___m_InvokeArray_4; }
	inline ObjectU5BU5D_t3614634134** get_address_of_m_InvokeArray_4() { return &___m_InvokeArray_4; }
	inline void set_m_InvokeArray_4(ObjectU5BU5D_t3614634134* value)
	{
		___m_InvokeArray_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_InvokeArray_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYEVENT_1_T2067570248_H
#ifndef UNITYEVENT_4_T1303696871_H
#define UNITYEVENT_4_T1303696871_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Events.UnityEvent`4<System.String,Utage.ButtonEventInfo,Utage.ButtonEventInfo,Utage.ButtonEventInfo>
struct  UnityEvent_4_t1303696871  : public UnityEventBase_t828812576
{
public:
	// System.Object[] UnityEngine.Events.UnityEvent`4::m_InvokeArray
	ObjectU5BU5D_t3614634134* ___m_InvokeArray_4;

public:
	inline static int32_t get_offset_of_m_InvokeArray_4() { return static_cast<int32_t>(offsetof(UnityEvent_4_t1303696871, ___m_InvokeArray_4)); }
	inline ObjectU5BU5D_t3614634134* get_m_InvokeArray_4() const { return ___m_InvokeArray_4; }
	inline ObjectU5BU5D_t3614634134** get_address_of_m_InvokeArray_4() { return &___m_InvokeArray_4; }
	inline void set_m_InvokeArray_4(ObjectU5BU5D_t3614634134* value)
	{
		___m_InvokeArray_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_InvokeArray_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYEVENT_4_T1303696871_H
#ifndef HASH128_T2836532937_H
#define HASH128_T2836532937_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Hash128
struct  Hash128_t2836532937 
{
public:
	// System.UInt32 UnityEngine.Hash128::m_u32_0
	uint32_t ___m_u32_0_0;
	// System.UInt32 UnityEngine.Hash128::m_u32_1
	uint32_t ___m_u32_1_1;
	// System.UInt32 UnityEngine.Hash128::m_u32_2
	uint32_t ___m_u32_2_2;
	// System.UInt32 UnityEngine.Hash128::m_u32_3
	uint32_t ___m_u32_3_3;

public:
	inline static int32_t get_offset_of_m_u32_0_0() { return static_cast<int32_t>(offsetof(Hash128_t2836532937, ___m_u32_0_0)); }
	inline uint32_t get_m_u32_0_0() const { return ___m_u32_0_0; }
	inline uint32_t* get_address_of_m_u32_0_0() { return &___m_u32_0_0; }
	inline void set_m_u32_0_0(uint32_t value)
	{
		___m_u32_0_0 = value;
	}

	inline static int32_t get_offset_of_m_u32_1_1() { return static_cast<int32_t>(offsetof(Hash128_t2836532937, ___m_u32_1_1)); }
	inline uint32_t get_m_u32_1_1() const { return ___m_u32_1_1; }
	inline uint32_t* get_address_of_m_u32_1_1() { return &___m_u32_1_1; }
	inline void set_m_u32_1_1(uint32_t value)
	{
		___m_u32_1_1 = value;
	}

	inline static int32_t get_offset_of_m_u32_2_2() { return static_cast<int32_t>(offsetof(Hash128_t2836532937, ___m_u32_2_2)); }
	inline uint32_t get_m_u32_2_2() const { return ___m_u32_2_2; }
	inline uint32_t* get_address_of_m_u32_2_2() { return &___m_u32_2_2; }
	inline void set_m_u32_2_2(uint32_t value)
	{
		___m_u32_2_2 = value;
	}

	inline static int32_t get_offset_of_m_u32_3_3() { return static_cast<int32_t>(offsetof(Hash128_t2836532937, ___m_u32_3_3)); }
	inline uint32_t get_m_u32_3_3() const { return ___m_u32_3_3; }
	inline uint32_t* get_address_of_m_u32_3_3() { return &___m_u32_3_3; }
	inline void set_m_u32_3_3(uint32_t value)
	{
		___m_u32_3_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HASH128_T2836532937_H
#ifndef VOID_T1841601450_H
#define VOID_T1841601450_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Void
struct  Void_t1841601450 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VOID_T1841601450_H
#ifndef PROPERTYATTRIBUTE_T2606999759_H
#define PROPERTYATTRIBUTE_T2606999759_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.PropertyAttribute
struct  PropertyAttribute_t2606999759  : public Attribute_t542643598
{
public:
	// System.Int32 UnityEngine.PropertyAttribute::<order>k__BackingField
	int32_t ___U3CorderU3Ek__BackingField_0;

public:
	inline static int32_t get_offset_of_U3CorderU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(PropertyAttribute_t2606999759, ___U3CorderU3Ek__BackingField_0)); }
	inline int32_t get_U3CorderU3Ek__BackingField_0() const { return ___U3CorderU3Ek__BackingField_0; }
	inline int32_t* get_address_of_U3CorderU3Ek__BackingField_0() { return &___U3CorderU3Ek__BackingField_0; }
	inline void set_U3CorderU3Ek__BackingField_0(int32_t value)
	{
		___U3CorderU3Ek__BackingField_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PROPERTYATTRIBUTE_T2606999759_H
#ifndef INTPTR_T_H
#define INTPTR_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTPTR_T_H
#ifndef UNITYEVENT_1_T2110227463_H
#define UNITYEVENT_1_T2110227463_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Events.UnityEvent`1<System.Int32>
struct  UnityEvent_1_t2110227463  : public UnityEventBase_t828812576
{
public:
	// System.Object[] UnityEngine.Events.UnityEvent`1::m_InvokeArray
	ObjectU5BU5D_t3614634134* ___m_InvokeArray_4;

public:
	inline static int32_t get_offset_of_m_InvokeArray_4() { return static_cast<int32_t>(offsetof(UnityEvent_1_t2110227463, ___m_InvokeArray_4)); }
	inline ObjectU5BU5D_t3614634134* get_m_InvokeArray_4() const { return ___m_InvokeArray_4; }
	inline ObjectU5BU5D_t3614634134** get_address_of_m_InvokeArray_4() { return &___m_InvokeArray_4; }
	inline void set_m_InvokeArray_4(ObjectU5BU5D_t3614634134* value)
	{
		___m_InvokeArray_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_InvokeArray_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYEVENT_1_T2110227463_H
#ifndef UNITYEVENT_2_T2091189821_H
#define UNITYEVENT_2_T2091189821_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Events.UnityEvent`2<System.String,Utage.ButtonEventInfo>
struct  UnityEvent_2_t2091189821  : public UnityEventBase_t828812576
{
public:
	// System.Object[] UnityEngine.Events.UnityEvent`2::m_InvokeArray
	ObjectU5BU5D_t3614634134* ___m_InvokeArray_4;

public:
	inline static int32_t get_offset_of_m_InvokeArray_4() { return static_cast<int32_t>(offsetof(UnityEvent_2_t2091189821, ___m_InvokeArray_4)); }
	inline ObjectU5BU5D_t3614634134* get_m_InvokeArray_4() const { return ___m_InvokeArray_4; }
	inline ObjectU5BU5D_t3614634134** get_address_of_m_InvokeArray_4() { return &___m_InvokeArray_4; }
	inline void set_m_InvokeArray_4(ObjectU5BU5D_t3614634134* value)
	{
		___m_InvokeArray_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_InvokeArray_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYEVENT_2_T2091189821_H
#ifndef UNITYEVENT_2_T1460310953_H
#define UNITYEVENT_2_T1460310953_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Events.UnityEvent`2<System.String,System.Collections.Generic.List`1<Utage.ButtonEventInfo>>
struct  UnityEvent_2_t1460310953  : public UnityEventBase_t828812576
{
public:
	// System.Object[] UnityEngine.Events.UnityEvent`2::m_InvokeArray
	ObjectU5BU5D_t3614634134* ___m_InvokeArray_4;

public:
	inline static int32_t get_offset_of_m_InvokeArray_4() { return static_cast<int32_t>(offsetof(UnityEvent_2_t1460310953, ___m_InvokeArray_4)); }
	inline ObjectU5BU5D_t3614634134* get_m_InvokeArray_4() const { return ___m_InvokeArray_4; }
	inline ObjectU5BU5D_t3614634134** get_address_of_m_InvokeArray_4() { return &___m_InvokeArray_4; }
	inline void set_m_InvokeArray_4(ObjectU5BU5D_t3614634134* value)
	{
		___m_InvokeArray_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_InvokeArray_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYEVENT_2_T1460310953_H
#ifndef VECTOR3_T2243707580_H
#define VECTOR3_T2243707580_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector3
struct  Vector3_t2243707580 
{
public:
	// System.Single UnityEngine.Vector3::x
	float ___x_1;
	// System.Single UnityEngine.Vector3::y
	float ___y_2;
	// System.Single UnityEngine.Vector3::z
	float ___z_3;

public:
	inline static int32_t get_offset_of_x_1() { return static_cast<int32_t>(offsetof(Vector3_t2243707580, ___x_1)); }
	inline float get_x_1() const { return ___x_1; }
	inline float* get_address_of_x_1() { return &___x_1; }
	inline void set_x_1(float value)
	{
		___x_1 = value;
	}

	inline static int32_t get_offset_of_y_2() { return static_cast<int32_t>(offsetof(Vector3_t2243707580, ___y_2)); }
	inline float get_y_2() const { return ___y_2; }
	inline float* get_address_of_y_2() { return &___y_2; }
	inline void set_y_2(float value)
	{
		___y_2 = value;
	}

	inline static int32_t get_offset_of_z_3() { return static_cast<int32_t>(offsetof(Vector3_t2243707580, ___z_3)); }
	inline float get_z_3() const { return ___z_3; }
	inline float* get_address_of_z_3() { return &___z_3; }
	inline void set_z_3(float value)
	{
		___z_3 = value;
	}
};

struct Vector3_t2243707580_StaticFields
{
public:
	// UnityEngine.Vector3 UnityEngine.Vector3::zeroVector
	Vector3_t2243707580  ___zeroVector_4;
	// UnityEngine.Vector3 UnityEngine.Vector3::oneVector
	Vector3_t2243707580  ___oneVector_5;
	// UnityEngine.Vector3 UnityEngine.Vector3::upVector
	Vector3_t2243707580  ___upVector_6;
	// UnityEngine.Vector3 UnityEngine.Vector3::downVector
	Vector3_t2243707580  ___downVector_7;
	// UnityEngine.Vector3 UnityEngine.Vector3::leftVector
	Vector3_t2243707580  ___leftVector_8;
	// UnityEngine.Vector3 UnityEngine.Vector3::rightVector
	Vector3_t2243707580  ___rightVector_9;
	// UnityEngine.Vector3 UnityEngine.Vector3::forwardVector
	Vector3_t2243707580  ___forwardVector_10;
	// UnityEngine.Vector3 UnityEngine.Vector3::backVector
	Vector3_t2243707580  ___backVector_11;
	// UnityEngine.Vector3 UnityEngine.Vector3::positiveInfinityVector
	Vector3_t2243707580  ___positiveInfinityVector_12;
	// UnityEngine.Vector3 UnityEngine.Vector3::negativeInfinityVector
	Vector3_t2243707580  ___negativeInfinityVector_13;

public:
	inline static int32_t get_offset_of_zeroVector_4() { return static_cast<int32_t>(offsetof(Vector3_t2243707580_StaticFields, ___zeroVector_4)); }
	inline Vector3_t2243707580  get_zeroVector_4() const { return ___zeroVector_4; }
	inline Vector3_t2243707580 * get_address_of_zeroVector_4() { return &___zeroVector_4; }
	inline void set_zeroVector_4(Vector3_t2243707580  value)
	{
		___zeroVector_4 = value;
	}

	inline static int32_t get_offset_of_oneVector_5() { return static_cast<int32_t>(offsetof(Vector3_t2243707580_StaticFields, ___oneVector_5)); }
	inline Vector3_t2243707580  get_oneVector_5() const { return ___oneVector_5; }
	inline Vector3_t2243707580 * get_address_of_oneVector_5() { return &___oneVector_5; }
	inline void set_oneVector_5(Vector3_t2243707580  value)
	{
		___oneVector_5 = value;
	}

	inline static int32_t get_offset_of_upVector_6() { return static_cast<int32_t>(offsetof(Vector3_t2243707580_StaticFields, ___upVector_6)); }
	inline Vector3_t2243707580  get_upVector_6() const { return ___upVector_6; }
	inline Vector3_t2243707580 * get_address_of_upVector_6() { return &___upVector_6; }
	inline void set_upVector_6(Vector3_t2243707580  value)
	{
		___upVector_6 = value;
	}

	inline static int32_t get_offset_of_downVector_7() { return static_cast<int32_t>(offsetof(Vector3_t2243707580_StaticFields, ___downVector_7)); }
	inline Vector3_t2243707580  get_downVector_7() const { return ___downVector_7; }
	inline Vector3_t2243707580 * get_address_of_downVector_7() { return &___downVector_7; }
	inline void set_downVector_7(Vector3_t2243707580  value)
	{
		___downVector_7 = value;
	}

	inline static int32_t get_offset_of_leftVector_8() { return static_cast<int32_t>(offsetof(Vector3_t2243707580_StaticFields, ___leftVector_8)); }
	inline Vector3_t2243707580  get_leftVector_8() const { return ___leftVector_8; }
	inline Vector3_t2243707580 * get_address_of_leftVector_8() { return &___leftVector_8; }
	inline void set_leftVector_8(Vector3_t2243707580  value)
	{
		___leftVector_8 = value;
	}

	inline static int32_t get_offset_of_rightVector_9() { return static_cast<int32_t>(offsetof(Vector3_t2243707580_StaticFields, ___rightVector_9)); }
	inline Vector3_t2243707580  get_rightVector_9() const { return ___rightVector_9; }
	inline Vector3_t2243707580 * get_address_of_rightVector_9() { return &___rightVector_9; }
	inline void set_rightVector_9(Vector3_t2243707580  value)
	{
		___rightVector_9 = value;
	}

	inline static int32_t get_offset_of_forwardVector_10() { return static_cast<int32_t>(offsetof(Vector3_t2243707580_StaticFields, ___forwardVector_10)); }
	inline Vector3_t2243707580  get_forwardVector_10() const { return ___forwardVector_10; }
	inline Vector3_t2243707580 * get_address_of_forwardVector_10() { return &___forwardVector_10; }
	inline void set_forwardVector_10(Vector3_t2243707580  value)
	{
		___forwardVector_10 = value;
	}

	inline static int32_t get_offset_of_backVector_11() { return static_cast<int32_t>(offsetof(Vector3_t2243707580_StaticFields, ___backVector_11)); }
	inline Vector3_t2243707580  get_backVector_11() const { return ___backVector_11; }
	inline Vector3_t2243707580 * get_address_of_backVector_11() { return &___backVector_11; }
	inline void set_backVector_11(Vector3_t2243707580  value)
	{
		___backVector_11 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_12() { return static_cast<int32_t>(offsetof(Vector3_t2243707580_StaticFields, ___positiveInfinityVector_12)); }
	inline Vector3_t2243707580  get_positiveInfinityVector_12() const { return ___positiveInfinityVector_12; }
	inline Vector3_t2243707580 * get_address_of_positiveInfinityVector_12() { return &___positiveInfinityVector_12; }
	inline void set_positiveInfinityVector_12(Vector3_t2243707580  value)
	{
		___positiveInfinityVector_12 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_13() { return static_cast<int32_t>(offsetof(Vector3_t2243707580_StaticFields, ___negativeInfinityVector_13)); }
	inline Vector3_t2243707580  get_negativeInfinityVector_13() const { return ___negativeInfinityVector_13; }
	inline Vector3_t2243707580 * get_address_of_negativeInfinityVector_13() { return &___negativeInfinityVector_13; }
	inline void set_negativeInfinityVector_13(Vector3_t2243707580  value)
	{
		___negativeInfinityVector_13 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR3_T2243707580_H
#ifndef UNITYEVENT_1_T2559968630_H
#define UNITYEVENT_1_T2559968630_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Events.UnityEvent`1<Utage.CurveAnimation>
struct  UnityEvent_1_t2559968630  : public UnityEventBase_t828812576
{
public:
	// System.Object[] UnityEngine.Events.UnityEvent`1::m_InvokeArray
	ObjectU5BU5D_t3614634134* ___m_InvokeArray_4;

public:
	inline static int32_t get_offset_of_m_InvokeArray_4() { return static_cast<int32_t>(offsetof(UnityEvent_1_t2559968630, ___m_InvokeArray_4)); }
	inline ObjectU5BU5D_t3614634134* get_m_InvokeArray_4() const { return ___m_InvokeArray_4; }
	inline ObjectU5BU5D_t3614634134** get_address_of_m_InvokeArray_4() { return &___m_InvokeArray_4; }
	inline void set_m_InvokeArray_4(ObjectU5BU5D_t3614634134* value)
	{
		___m_InvokeArray_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_InvokeArray_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYEVENT_1_T2559968630_H
#ifndef UNITYEVENT_3_T1536453934_H
#define UNITYEVENT_3_T1536453934_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Events.UnityEvent`3<System.String,Utage.ButtonEventInfo,Utage.ButtonEventInfo>
struct  UnityEvent_3_t1536453934  : public UnityEventBase_t828812576
{
public:
	// System.Object[] UnityEngine.Events.UnityEvent`3::m_InvokeArray
	ObjectU5BU5D_t3614634134* ___m_InvokeArray_4;

public:
	inline static int32_t get_offset_of_m_InvokeArray_4() { return static_cast<int32_t>(offsetof(UnityEvent_3_t1536453934, ___m_InvokeArray_4)); }
	inline ObjectU5BU5D_t3614634134* get_m_InvokeArray_4() const { return ___m_InvokeArray_4; }
	inline ObjectU5BU5D_t3614634134** get_address_of_m_InvokeArray_4() { return &___m_InvokeArray_4; }
	inline void set_m_InvokeArray_4(ObjectU5BU5D_t3614634134* value)
	{
		___m_InvokeArray_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_InvokeArray_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYEVENT_3_T1536453934_H
#ifndef ENUM_T2459695545_H
#define ENUM_T2459695545_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Enum
struct  Enum_t2459695545  : public ValueType_t3507792607
{
public:

public:
};

struct Enum_t2459695545_StaticFields
{
public:
	// System.Char[] System.Enum::split_char
	CharU5BU5D_t1328083999* ___split_char_0;

public:
	inline static int32_t get_offset_of_split_char_0() { return static_cast<int32_t>(offsetof(Enum_t2459695545_StaticFields, ___split_char_0)); }
	inline CharU5BU5D_t1328083999* get_split_char_0() const { return ___split_char_0; }
	inline CharU5BU5D_t1328083999** get_address_of_split_char_0() { return &___split_char_0; }
	inline void set_split_char_0(CharU5BU5D_t1328083999* value)
	{
		___split_char_0 = value;
		Il2CppCodeGenWriteBarrier((&___split_char_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t2459695545_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t2459695545_marshaled_com
{
};
#endif // ENUM_T2459695545_H
#ifndef UNITYEVENT_1_T2114859947_H
#define UNITYEVENT_1_T2114859947_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Events.UnityEvent`1<System.Single>
struct  UnityEvent_1_t2114859947  : public UnityEventBase_t828812576
{
public:
	// System.Object[] UnityEngine.Events.UnityEvent`1::m_InvokeArray
	ObjectU5BU5D_t3614634134* ___m_InvokeArray_4;

public:
	inline static int32_t get_offset_of_m_InvokeArray_4() { return static_cast<int32_t>(offsetof(UnityEvent_1_t2114859947, ___m_InvokeArray_4)); }
	inline ObjectU5BU5D_t3614634134* get_m_InvokeArray_4() const { return ___m_InvokeArray_4; }
	inline ObjectU5BU5D_t3614634134** get_address_of_m_InvokeArray_4() { return &___m_InvokeArray_4; }
	inline void set_m_InvokeArray_4(ObjectU5BU5D_t3614634134* value)
	{
		___m_InvokeArray_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_InvokeArray_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYEVENT_1_T2114859947_H
#ifndef SOUNDHEADER_T574408063_H
#define SOUNDHEADER_T574408063_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.FileIOManagerBase/SoundHeader
struct  SoundHeader_t574408063 
{
public:
	// System.Int32 Utage.FileIOManagerBase/SoundHeader::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(SoundHeader_t574408063, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SOUNDHEADER_T574408063_H
#ifndef DELEGATE_T3022476291_H
#define DELEGATE_T3022476291_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Delegate
struct  Delegate_t3022476291  : public RuntimeObject
{
public:
	// System.IntPtr System.Delegate::method_ptr
	Il2CppMethodPointer ___method_ptr_0;
	// System.IntPtr System.Delegate::invoke_impl
	intptr_t ___invoke_impl_1;
	// System.Object System.Delegate::m_target
	RuntimeObject * ___m_target_2;
	// System.IntPtr System.Delegate::method
	intptr_t ___method_3;
	// System.IntPtr System.Delegate::delegate_trampoline
	intptr_t ___delegate_trampoline_4;
	// System.IntPtr System.Delegate::method_code
	intptr_t ___method_code_5;
	// System.Reflection.MethodInfo System.Delegate::method_info
	MethodInfo_t * ___method_info_6;
	// System.Reflection.MethodInfo System.Delegate::original_method_info
	MethodInfo_t * ___original_method_info_7;
	// System.DelegateData System.Delegate::data
	DelegateData_t1572802995 * ___data_8;

public:
	inline static int32_t get_offset_of_method_ptr_0() { return static_cast<int32_t>(offsetof(Delegate_t3022476291, ___method_ptr_0)); }
	inline Il2CppMethodPointer get_method_ptr_0() const { return ___method_ptr_0; }
	inline Il2CppMethodPointer* get_address_of_method_ptr_0() { return &___method_ptr_0; }
	inline void set_method_ptr_0(Il2CppMethodPointer value)
	{
		___method_ptr_0 = value;
	}

	inline static int32_t get_offset_of_invoke_impl_1() { return static_cast<int32_t>(offsetof(Delegate_t3022476291, ___invoke_impl_1)); }
	inline intptr_t get_invoke_impl_1() const { return ___invoke_impl_1; }
	inline intptr_t* get_address_of_invoke_impl_1() { return &___invoke_impl_1; }
	inline void set_invoke_impl_1(intptr_t value)
	{
		___invoke_impl_1 = value;
	}

	inline static int32_t get_offset_of_m_target_2() { return static_cast<int32_t>(offsetof(Delegate_t3022476291, ___m_target_2)); }
	inline RuntimeObject * get_m_target_2() const { return ___m_target_2; }
	inline RuntimeObject ** get_address_of_m_target_2() { return &___m_target_2; }
	inline void set_m_target_2(RuntimeObject * value)
	{
		___m_target_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_target_2), value);
	}

	inline static int32_t get_offset_of_method_3() { return static_cast<int32_t>(offsetof(Delegate_t3022476291, ___method_3)); }
	inline intptr_t get_method_3() const { return ___method_3; }
	inline intptr_t* get_address_of_method_3() { return &___method_3; }
	inline void set_method_3(intptr_t value)
	{
		___method_3 = value;
	}

	inline static int32_t get_offset_of_delegate_trampoline_4() { return static_cast<int32_t>(offsetof(Delegate_t3022476291, ___delegate_trampoline_4)); }
	inline intptr_t get_delegate_trampoline_4() const { return ___delegate_trampoline_4; }
	inline intptr_t* get_address_of_delegate_trampoline_4() { return &___delegate_trampoline_4; }
	inline void set_delegate_trampoline_4(intptr_t value)
	{
		___delegate_trampoline_4 = value;
	}

	inline static int32_t get_offset_of_method_code_5() { return static_cast<int32_t>(offsetof(Delegate_t3022476291, ___method_code_5)); }
	inline intptr_t get_method_code_5() const { return ___method_code_5; }
	inline intptr_t* get_address_of_method_code_5() { return &___method_code_5; }
	inline void set_method_code_5(intptr_t value)
	{
		___method_code_5 = value;
	}

	inline static int32_t get_offset_of_method_info_6() { return static_cast<int32_t>(offsetof(Delegate_t3022476291, ___method_info_6)); }
	inline MethodInfo_t * get_method_info_6() const { return ___method_info_6; }
	inline MethodInfo_t ** get_address_of_method_info_6() { return &___method_info_6; }
	inline void set_method_info_6(MethodInfo_t * value)
	{
		___method_info_6 = value;
		Il2CppCodeGenWriteBarrier((&___method_info_6), value);
	}

	inline static int32_t get_offset_of_original_method_info_7() { return static_cast<int32_t>(offsetof(Delegate_t3022476291, ___original_method_info_7)); }
	inline MethodInfo_t * get_original_method_info_7() const { return ___original_method_info_7; }
	inline MethodInfo_t ** get_address_of_original_method_info_7() { return &___original_method_info_7; }
	inline void set_original_method_info_7(MethodInfo_t * value)
	{
		___original_method_info_7 = value;
		Il2CppCodeGenWriteBarrier((&___original_method_info_7), value);
	}

	inline static int32_t get_offset_of_data_8() { return static_cast<int32_t>(offsetof(Delegate_t3022476291, ___data_8)); }
	inline DelegateData_t1572802995 * get_data_8() const { return ___data_8; }
	inline DelegateData_t1572802995 ** get_address_of_data_8() { return &___data_8; }
	inline void set_data_8(DelegateData_t1572802995 * value)
	{
		___data_8 = value;
		Il2CppCodeGenWriteBarrier((&___data_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DELEGATE_T3022476291_H
#ifndef ASSETFILETYPE_T2804304536_H
#define ASSETFILETYPE_T2804304536_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.AssetFileType
struct  AssetFileType_t2804304536 
{
public:
	// System.Int32 Utage.AssetFileType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(AssetFileType_t2804304536, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ASSETFILETYPE_T2804304536_H
#ifndef TYPE_T963772239_H
#define TYPE_T963772239_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.WWWEx/Type
struct  Type_t963772239 
{
public:
	// System.Int32 Utage.WWWEx/Type::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(Type_t963772239, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TYPE_T963772239_H
#ifndef ASSETFILESTRAGETYPE_T3334005126_H
#define ASSETFILESTRAGETYPE_T3334005126_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.AssetFileStrageType
struct  AssetFileStrageType_t3334005126 
{
public:
	// System.Int32 Utage.AssetFileStrageType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(AssetFileStrageType_t3334005126, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ASSETFILESTRAGETYPE_T3334005126_H
#ifndef ASSETFILELOADFLAGS_T3123011941_H
#define ASSETFILELOADFLAGS_T3123011941_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.AssetFileLoadFlags
struct  AssetFileLoadFlags_t3123011941 
{
public:
	// System.Int32 Utage.AssetFileLoadFlags::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(AssetFileLoadFlags_t3123011941, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ASSETFILELOADFLAGS_T3123011941_H
#ifndef ASSETFILELOADPRIORITY_T1290657756_H
#define ASSETFILELOADPRIORITY_T1290657756_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.AssetFileLoadPriority
struct  AssetFileLoadPriority_t1290657756 
{
public:
	// System.Int32 Utage.AssetFileLoadPriority::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(AssetFileLoadPriority_t1290657756, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ASSETFILELOADPRIORITY_T1290657756_H
#ifndef COMPAREFUNCTION_T457874581_H
#define COMPAREFUNCTION_T457874581_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Rendering.CompareFunction
struct  CompareFunction_t457874581 
{
public:
	// System.Int32 UnityEngine.Rendering.CompareFunction::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(CompareFunction_t457874581, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPAREFUNCTION_T457874581_H
#ifndef OPENDIALOGEVENT_T2352564994_H
#define OPENDIALOGEVENT_T2352564994_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.OpenDialogEvent
struct  OpenDialogEvent_t2352564994  : public UnityEvent_2_t1460310953
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OPENDIALOGEVENT_T2352564994_H
#ifndef OPEN1BUTTONDIALOGEVENT_T586865045_H
#define OPEN1BUTTONDIALOGEVENT_T586865045_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.Open1ButtonDialogEvent
struct  Open1ButtonDialogEvent_t586865045  : public UnityEvent_2_t2091189821
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OPEN1BUTTONDIALOGEVENT_T586865045_H
#ifndef OPEN2BUTTONDIALOGEVENT_T1974290772_H
#define OPEN2BUTTONDIALOGEVENT_T1974290772_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.Open2ButtonDialogEvent
struct  Open2ButtonDialogEvent_t1974290772  : public UnityEvent_3_t1536453934
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OPEN2BUTTONDIALOGEVENT_T1974290772_H
#ifndef OPEN3BUTTONDIALOGEVENT_T2400938643_H
#define OPEN3BUTTONDIALOGEVENT_T2400938643_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.Open3ButtonDialogEvent
struct  Open3ButtonDialogEvent_t2400938643  : public UnityEvent_4_t1303696871
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OPEN3BUTTONDIALOGEVENT_T2400938643_H
#ifndef FLOATEVENT_T3725987244_H
#define FLOATEVENT_T3725987244_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.FloatEvent
struct  FloatEvent_t3725987244  : public UnityEvent_1_t2114859947
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FLOATEVENT_T3725987244_H
#ifndef INTEVENT_T3062348593_H
#define INTEVENT_T3062348593_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.IntEvent
struct  IntEvent_t3062348593  : public UnityEvent_1_t2110227463
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTEVENT_T3062348593_H
#ifndef STRINGEVENT_T282042215_H
#define STRINGEVENT_T282042215_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.StringEvent
struct  StringEvent_t282042215  : public UnityEvent_1_t2067570248
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STRINGEVENT_T282042215_H
#ifndef BOOLEVENT_T2313125906_H
#define BOOLEVENT_T2313125906_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.BoolEvent
struct  BoolEvent_t2313125906  : public UnityEvent_1_t3863924733
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BOOLEVENT_T2313125906_H
#ifndef TOKENTYPE_T202952175_H
#define TOKENTYPE_T202952175_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.ExpressionToken/TokenType
struct  TokenType_t202952175 
{
public:
	// System.Int32 Utage.ExpressionToken/TokenType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(TokenType_t202952175, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TOKENTYPE_T202952175_H
#ifndef MOTIONPLAYTYPE_T785574570_H
#define MOTIONPLAYTYPE_T785574570_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.MotionPlayType
struct  MotionPlayType_t785574570 
{
public:
	// System.Int32 Utage.MotionPlayType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(MotionPlayType_t785574570, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MOTIONPLAYTYPE_T785574570_H
#ifndef OVERRIDEPROPERTYDRAWATTRIBUTE_T3814407393_H
#define OVERRIDEPROPERTYDRAWATTRIBUTE_T3814407393_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.OverridePropertyDrawAttribute
struct  OverridePropertyDrawAttribute_t3814407393  : public PropertyAttribute_t2606999759
{
public:
	// System.String Utage.OverridePropertyDrawAttribute::<Function>k__BackingField
	String_t* ___U3CFunctionU3Ek__BackingField_1;

public:
	inline static int32_t get_offset_of_U3CFunctionU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(OverridePropertyDrawAttribute_t3814407393, ___U3CFunctionU3Ek__BackingField_1)); }
	inline String_t* get_U3CFunctionU3Ek__BackingField_1() const { return ___U3CFunctionU3Ek__BackingField_1; }
	inline String_t** get_address_of_U3CFunctionU3Ek__BackingField_1() { return &___U3CFunctionU3Ek__BackingField_1; }
	inline void set_U3CFunctionU3Ek__BackingField_1(String_t* value)
	{
		___U3CFunctionU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CFunctionU3Ek__BackingField_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OVERRIDEPROPERTYDRAWATTRIBUTE_T3814407393_H
#ifndef DIALOGTYPE_T1187455388_H
#define DIALOGTYPE_T1187455388_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.PathDialogAttribute/DialogType
struct  DialogType_t1187455388 
{
public:
	// System.Int32 Utage.PathDialogAttribute/DialogType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(DialogType_t1187455388, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DIALOGTYPE_T1187455388_H
#ifndef STRINGPOPUPATTRIBUTE_T1790424821_H
#define STRINGPOPUPATTRIBUTE_T1790424821_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.StringPopupAttribute
struct  StringPopupAttribute_t1790424821  : public PropertyAttribute_t2606999759
{
public:
	// System.Collections.Generic.List`1<System.String> Utage.StringPopupAttribute::stringList
	List_1_t1398341365 * ___stringList_1;

public:
	inline static int32_t get_offset_of_stringList_1() { return static_cast<int32_t>(offsetof(StringPopupAttribute_t1790424821, ___stringList_1)); }
	inline List_1_t1398341365 * get_stringList_1() const { return ___stringList_1; }
	inline List_1_t1398341365 ** get_address_of_stringList_1() { return &___stringList_1; }
	inline void set_stringList_1(List_1_t1398341365 * value)
	{
		___stringList_1 = value;
		Il2CppCodeGenWriteBarrier((&___stringList_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STRINGPOPUPATTRIBUTE_T1790424821_H
#ifndef STRINGPOPUPFUNCTIONATTRIBUTE_T2619373699_H
#define STRINGPOPUPFUNCTIONATTRIBUTE_T2619373699_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.StringPopupFunctionAttribute
struct  StringPopupFunctionAttribute_t2619373699  : public PropertyAttribute_t2606999759
{
public:
	// System.String Utage.StringPopupFunctionAttribute::<Function>k__BackingField
	String_t* ___U3CFunctionU3Ek__BackingField_1;

public:
	inline static int32_t get_offset_of_U3CFunctionU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(StringPopupFunctionAttribute_t2619373699, ___U3CFunctionU3Ek__BackingField_1)); }
	inline String_t* get_U3CFunctionU3Ek__BackingField_1() const { return ___U3CFunctionU3Ek__BackingField_1; }
	inline String_t** get_address_of_U3CFunctionU3Ek__BackingField_1() { return &___U3CFunctionU3Ek__BackingField_1; }
	inline void set_U3CFunctionU3Ek__BackingField_1(String_t* value)
	{
		___U3CFunctionU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CFunctionU3Ek__BackingField_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STRINGPOPUPFUNCTIONATTRIBUTE_T2619373699_H
#ifndef STRINGPOPUPINDEXEDATTRIBUTE_T3196049692_H
#define STRINGPOPUPINDEXEDATTRIBUTE_T3196049692_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.StringPopupIndexedAttribute
struct  StringPopupIndexedAttribute_t3196049692  : public PropertyAttribute_t2606999759
{
public:
	// System.String Utage.StringPopupIndexedAttribute::<Function>k__BackingField
	String_t* ___U3CFunctionU3Ek__BackingField_1;

public:
	inline static int32_t get_offset_of_U3CFunctionU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(StringPopupIndexedAttribute_t3196049692, ___U3CFunctionU3Ek__BackingField_1)); }
	inline String_t* get_U3CFunctionU3Ek__BackingField_1() const { return ___U3CFunctionU3Ek__BackingField_1; }
	inline String_t** get_address_of_U3CFunctionU3Ek__BackingField_1() { return &___U3CFunctionU3Ek__BackingField_1; }
	inline void set_U3CFunctionU3Ek__BackingField_1(String_t* value)
	{
		___U3CFunctionU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CFunctionU3Ek__BackingField_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STRINGPOPUPINDEXEDATTRIBUTE_T3196049692_H
#ifndef UIEVENTMASK_T3658541192_H
#define UIEVENTMASK_T3658541192_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.UiEventMask
struct  UiEventMask_t3658541192 
{
public:
	// System.Int32 Utage.UiEventMask::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(UiEventMask_t3658541192, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UIEVENTMASK_T3658541192_H
#ifndef ANIMATIONTYPE_T1473548579_H
#define ANIMATIONTYPE_T1473548579_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.UguiAnimation/AnimationType
struct  AnimationType_t1473548579 
{
public:
	// System.Int32 Utage.UguiAnimation/AnimationType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(AnimationType_t1473548579, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ANIMATIONTYPE_T1473548579_H
#ifndef CURVEANIMATIONEVENT_T3593772963_H
#define CURVEANIMATIONEVENT_T3593772963_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.CurveAnimationEvent
struct  CurveAnimationEvent_t3593772963  : public UnityEvent_1_t2559968630
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CURVEANIMATIONEVENT_T3593772963_H
#ifndef NAMINGTYPE_T115011021_H
#define NAMINGTYPE_T115011021_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.MiniAnimationData/Data/NamingType
struct  NamingType_t115011021 
{
public:
	// System.Int32 Utage.MiniAnimationData/Data/NamingType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(NamingType_t115011021, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NAMINGTYPE_T115011021_H
#ifndef ASSETBUNDLETARGETFLAGS_T3506559986_H
#define ASSETBUNDLETARGETFLAGS_T3506559986_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.AssetBundleTargetFlags
struct  AssetBundleTargetFlags_t3506559986 
{
public:
	// System.Int32 Utage.AssetBundleTargetFlags::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(AssetBundleTargetFlags_t3506559986, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ASSETBUNDLETARGETFLAGS_T3506559986_H
#ifndef NOTEDITABLEATTRIBUTE_T3788200865_H
#define NOTEDITABLEATTRIBUTE_T3788200865_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.NotEditableAttribute
struct  NotEditableAttribute_t3788200865  : public PropertyAttribute_t2606999759
{
public:
	// System.String Utage.NotEditableAttribute::<EnablePropertyPath>k__BackingField
	String_t* ___U3CEnablePropertyPathU3Ek__BackingField_1;
	// System.Boolean Utage.NotEditableAttribute::<IsEnableProperty>k__BackingField
	bool ___U3CIsEnablePropertyU3Ek__BackingField_2;

public:
	inline static int32_t get_offset_of_U3CEnablePropertyPathU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(NotEditableAttribute_t3788200865, ___U3CEnablePropertyPathU3Ek__BackingField_1)); }
	inline String_t* get_U3CEnablePropertyPathU3Ek__BackingField_1() const { return ___U3CEnablePropertyPathU3Ek__BackingField_1; }
	inline String_t** get_address_of_U3CEnablePropertyPathU3Ek__BackingField_1() { return &___U3CEnablePropertyPathU3Ek__BackingField_1; }
	inline void set_U3CEnablePropertyPathU3Ek__BackingField_1(String_t* value)
	{
		___U3CEnablePropertyPathU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CEnablePropertyPathU3Ek__BackingField_1), value);
	}

	inline static int32_t get_offset_of_U3CIsEnablePropertyU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(NotEditableAttribute_t3788200865, ___U3CIsEnablePropertyU3Ek__BackingField_2)); }
	inline bool get_U3CIsEnablePropertyU3Ek__BackingField_2() const { return ___U3CIsEnablePropertyU3Ek__BackingField_2; }
	inline bool* get_address_of_U3CIsEnablePropertyU3Ek__BackingField_2() { return &___U3CIsEnablePropertyU3Ek__BackingField_2; }
	inline void set_U3CIsEnablePropertyU3Ek__BackingField_2(bool value)
	{
		___U3CIsEnablePropertyU3Ek__BackingField_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NOTEDITABLEATTRIBUTE_T3788200865_H
#ifndef LOADTYPE_T165610055_H
#define LOADTYPE_T165610055_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.AssetFileManagerSettings/LoadType
struct  LoadType_t165610055 
{
public:
	// System.Int32 Utage.AssetFileManagerSettings/LoadType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(LoadType_t165610055, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LOADTYPE_T165610055_H
#ifndef OBJECT_T1021602117_H
#define OBJECT_T1021602117_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Object
struct  Object_t1021602117  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Object::m_CachedPtr
	intptr_t ___m_CachedPtr_0;

public:
	inline static int32_t get_offset_of_m_CachedPtr_0() { return static_cast<int32_t>(offsetof(Object_t1021602117, ___m_CachedPtr_0)); }
	inline intptr_t get_m_CachedPtr_0() const { return ___m_CachedPtr_0; }
	inline intptr_t* get_address_of_m_CachedPtr_0() { return &___m_CachedPtr_0; }
	inline void set_m_CachedPtr_0(intptr_t value)
	{
		___m_CachedPtr_0 = value;
	}
};

struct Object_t1021602117_StaticFields
{
public:
	// System.Int32 UnityEngine.Object::OffsetOfInstanceIDInCPlusPlusObject
	int32_t ___OffsetOfInstanceIDInCPlusPlusObject_1;

public:
	inline static int32_t get_offset_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return static_cast<int32_t>(offsetof(Object_t1021602117_StaticFields, ___OffsetOfInstanceIDInCPlusPlusObject_1)); }
	inline int32_t get_OffsetOfInstanceIDInCPlusPlusObject_1() const { return ___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline int32_t* get_address_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return &___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline void set_OffsetOfInstanceIDInCPlusPlusObject_1(int32_t value)
	{
		___OffsetOfInstanceIDInCPlusPlusObject_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.Object
struct Object_t1021602117_marshaled_pinvoke
{
	intptr_t ___m_CachedPtr_0;
};
// Native definition for COM marshalling of UnityEngine.Object
struct Object_t1021602117_marshaled_com
{
	intptr_t ___m_CachedPtr_0;
};
#endif // OBJECT_T1021602117_H
#ifndef UNLOADTYPE_T122993117_H
#define UNLOADTYPE_T122993117_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.AssetFileManager/UnloadType
struct  UnloadType_t122993117 
{
public:
	// System.Int32 Utage.AssetFileManager/UnloadType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(UnloadType_t122993117, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNLOADTYPE_T122993117_H
#ifndef ASSETBUNDLEINFO_T1249246306_H
#define ASSETBUNDLEINFO_T1249246306_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.AssetBundleInfo
struct  AssetBundleInfo_t1249246306  : public RuntimeObject
{
public:
	// System.String Utage.AssetBundleInfo::<Url>k__BackingField
	String_t* ___U3CUrlU3Ek__BackingField_0;
	// UnityEngine.Hash128 Utage.AssetBundleInfo::<Hash>k__BackingField
	Hash128_t2836532937  ___U3CHashU3Ek__BackingField_1;
	// System.Int32 Utage.AssetBundleInfo::<Version>k__BackingField
	int32_t ___U3CVersionU3Ek__BackingField_2;

public:
	inline static int32_t get_offset_of_U3CUrlU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(AssetBundleInfo_t1249246306, ___U3CUrlU3Ek__BackingField_0)); }
	inline String_t* get_U3CUrlU3Ek__BackingField_0() const { return ___U3CUrlU3Ek__BackingField_0; }
	inline String_t** get_address_of_U3CUrlU3Ek__BackingField_0() { return &___U3CUrlU3Ek__BackingField_0; }
	inline void set_U3CUrlU3Ek__BackingField_0(String_t* value)
	{
		___U3CUrlU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CUrlU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_U3CHashU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(AssetBundleInfo_t1249246306, ___U3CHashU3Ek__BackingField_1)); }
	inline Hash128_t2836532937  get_U3CHashU3Ek__BackingField_1() const { return ___U3CHashU3Ek__BackingField_1; }
	inline Hash128_t2836532937 * get_address_of_U3CHashU3Ek__BackingField_1() { return &___U3CHashU3Ek__BackingField_1; }
	inline void set_U3CHashU3Ek__BackingField_1(Hash128_t2836532937  value)
	{
		___U3CHashU3Ek__BackingField_1 = value;
	}

	inline static int32_t get_offset_of_U3CVersionU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(AssetBundleInfo_t1249246306, ___U3CVersionU3Ek__BackingField_2)); }
	inline int32_t get_U3CVersionU3Ek__BackingField_2() const { return ___U3CVersionU3Ek__BackingField_2; }
	inline int32_t* get_address_of_U3CVersionU3Ek__BackingField_2() { return &___U3CVersionU3Ek__BackingField_2; }
	inline void set_U3CVersionU3Ek__BackingField_2(int32_t value)
	{
		___U3CVersionU3Ek__BackingField_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ASSETBUNDLEINFO_T1249246306_H
#ifndef U3CFINDU3EC__ANONSTOREY0_T3870382361_H
#define U3CFINDU3EC__ANONSTOREY0_T3870382361_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.AssetFileManagerSettings/<Find>c__AnonStorey0
struct  U3CFindU3Ec__AnonStorey0_t3870382361  : public RuntimeObject
{
public:
	// Utage.AssetFileType Utage.AssetFileManagerSettings/<Find>c__AnonStorey0::type
	int32_t ___type_0;

public:
	inline static int32_t get_offset_of_type_0() { return static_cast<int32_t>(offsetof(U3CFindU3Ec__AnonStorey0_t3870382361, ___type_0)); }
	inline int32_t get_type_0() const { return ___type_0; }
	inline int32_t* get_address_of_type_0() { return &___type_0; }
	inline void set_type_0(int32_t value)
	{
		___type_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CFINDU3EC__ANONSTOREY0_T3870382361_H
#ifndef ASSETFILEMANAGERSETTINGS_T1473658718_H
#define ASSETFILEMANAGERSETTINGS_T1473658718_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.AssetFileManagerSettings
struct  AssetFileManagerSettings_t1473658718  : public RuntimeObject
{
public:
	// Utage.AssetFileManagerSettings/LoadType Utage.AssetFileManagerSettings::loadType
	int32_t ___loadType_0;
	// System.Collections.Generic.List`1<Utage.AssetFileSetting> Utage.AssetFileManagerSettings::fileSettings
	List_1_t1239078948 * ___fileSettings_1;
	// System.Collections.Generic.List`1<Utage.AssetFileSetting> Utage.AssetFileManagerSettings::rebuildFileSettings
	List_1_t1239078948 * ___rebuildFileSettings_2;

public:
	inline static int32_t get_offset_of_loadType_0() { return static_cast<int32_t>(offsetof(AssetFileManagerSettings_t1473658718, ___loadType_0)); }
	inline int32_t get_loadType_0() const { return ___loadType_0; }
	inline int32_t* get_address_of_loadType_0() { return &___loadType_0; }
	inline void set_loadType_0(int32_t value)
	{
		___loadType_0 = value;
	}

	inline static int32_t get_offset_of_fileSettings_1() { return static_cast<int32_t>(offsetof(AssetFileManagerSettings_t1473658718, ___fileSettings_1)); }
	inline List_1_t1239078948 * get_fileSettings_1() const { return ___fileSettings_1; }
	inline List_1_t1239078948 ** get_address_of_fileSettings_1() { return &___fileSettings_1; }
	inline void set_fileSettings_1(List_1_t1239078948 * value)
	{
		___fileSettings_1 = value;
		Il2CppCodeGenWriteBarrier((&___fileSettings_1), value);
	}

	inline static int32_t get_offset_of_rebuildFileSettings_2() { return static_cast<int32_t>(offsetof(AssetFileManagerSettings_t1473658718, ___rebuildFileSettings_2)); }
	inline List_1_t1239078948 * get_rebuildFileSettings_2() const { return ___rebuildFileSettings_2; }
	inline List_1_t1239078948 ** get_address_of_rebuildFileSettings_2() { return &___rebuildFileSettings_2; }
	inline void set_rebuildFileSettings_2(List_1_t1239078948 * value)
	{
		___rebuildFileSettings_2 = value;
		Il2CppCodeGenWriteBarrier((&___rebuildFileSettings_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ASSETFILEMANAGERSETTINGS_T1473658718_H
#ifndef PATHDIALOGATTRIBUTE_T1627133723_H
#define PATHDIALOGATTRIBUTE_T1627133723_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.PathDialogAttribute
struct  PathDialogAttribute_t1627133723  : public PropertyAttribute_t2606999759
{
public:
	// Utage.PathDialogAttribute/DialogType Utage.PathDialogAttribute::<Type>k__BackingField
	int32_t ___U3CTypeU3Ek__BackingField_1;
	// System.String Utage.PathDialogAttribute::<Extention>k__BackingField
	String_t* ___U3CExtentionU3Ek__BackingField_2;

public:
	inline static int32_t get_offset_of_U3CTypeU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(PathDialogAttribute_t1627133723, ___U3CTypeU3Ek__BackingField_1)); }
	inline int32_t get_U3CTypeU3Ek__BackingField_1() const { return ___U3CTypeU3Ek__BackingField_1; }
	inline int32_t* get_address_of_U3CTypeU3Ek__BackingField_1() { return &___U3CTypeU3Ek__BackingField_1; }
	inline void set_U3CTypeU3Ek__BackingField_1(int32_t value)
	{
		___U3CTypeU3Ek__BackingField_1 = value;
	}

	inline static int32_t get_offset_of_U3CExtentionU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(PathDialogAttribute_t1627133723, ___U3CExtentionU3Ek__BackingField_2)); }
	inline String_t* get_U3CExtentionU3Ek__BackingField_2() const { return ___U3CExtentionU3Ek__BackingField_2; }
	inline String_t** get_address_of_U3CExtentionU3Ek__BackingField_2() { return &___U3CExtentionU3Ek__BackingField_2; }
	inline void set_U3CExtentionU3Ek__BackingField_2(String_t* value)
	{
		___U3CExtentionU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CExtentionU3Ek__BackingField_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PATHDIALOGATTRIBUTE_T1627133723_H
#ifndef EXPRESSIONTOKEN_T2745466747_H
#define EXPRESSIONTOKEN_T2745466747_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.ExpressionToken
struct  ExpressionToken_t2745466747  : public RuntimeObject
{
public:
	// System.String Utage.ExpressionToken::name
	String_t* ___name_29;
	// System.Boolean Utage.ExpressionToken::isAlphabet
	bool ___isAlphabet_30;
	// Utage.ExpressionToken/TokenType Utage.ExpressionToken::type
	int32_t ___type_31;
	// System.Int32 Utage.ExpressionToken::priority
	int32_t ___priority_32;
	// System.Object Utage.ExpressionToken::variable
	RuntimeObject * ___variable_33;
	// System.Int32 Utage.ExpressionToken::numFunctionArg
	int32_t ___numFunctionArg_34;

public:
	inline static int32_t get_offset_of_name_29() { return static_cast<int32_t>(offsetof(ExpressionToken_t2745466747, ___name_29)); }
	inline String_t* get_name_29() const { return ___name_29; }
	inline String_t** get_address_of_name_29() { return &___name_29; }
	inline void set_name_29(String_t* value)
	{
		___name_29 = value;
		Il2CppCodeGenWriteBarrier((&___name_29), value);
	}

	inline static int32_t get_offset_of_isAlphabet_30() { return static_cast<int32_t>(offsetof(ExpressionToken_t2745466747, ___isAlphabet_30)); }
	inline bool get_isAlphabet_30() const { return ___isAlphabet_30; }
	inline bool* get_address_of_isAlphabet_30() { return &___isAlphabet_30; }
	inline void set_isAlphabet_30(bool value)
	{
		___isAlphabet_30 = value;
	}

	inline static int32_t get_offset_of_type_31() { return static_cast<int32_t>(offsetof(ExpressionToken_t2745466747, ___type_31)); }
	inline int32_t get_type_31() const { return ___type_31; }
	inline int32_t* get_address_of_type_31() { return &___type_31; }
	inline void set_type_31(int32_t value)
	{
		___type_31 = value;
	}

	inline static int32_t get_offset_of_priority_32() { return static_cast<int32_t>(offsetof(ExpressionToken_t2745466747, ___priority_32)); }
	inline int32_t get_priority_32() const { return ___priority_32; }
	inline int32_t* get_address_of_priority_32() { return &___priority_32; }
	inline void set_priority_32(int32_t value)
	{
		___priority_32 = value;
	}

	inline static int32_t get_offset_of_variable_33() { return static_cast<int32_t>(offsetof(ExpressionToken_t2745466747, ___variable_33)); }
	inline RuntimeObject * get_variable_33() const { return ___variable_33; }
	inline RuntimeObject ** get_address_of_variable_33() { return &___variable_33; }
	inline void set_variable_33(RuntimeObject * value)
	{
		___variable_33 = value;
		Il2CppCodeGenWriteBarrier((&___variable_33), value);
	}

	inline static int32_t get_offset_of_numFunctionArg_34() { return static_cast<int32_t>(offsetof(ExpressionToken_t2745466747, ___numFunctionArg_34)); }
	inline int32_t get_numFunctionArg_34() const { return ___numFunctionArg_34; }
	inline int32_t* get_address_of_numFunctionArg_34() { return &___numFunctionArg_34; }
	inline void set_numFunctionArg_34(int32_t value)
	{
		___numFunctionArg_34 = value;
	}
};

struct ExpressionToken_t2745466747_StaticFields
{
public:
	// Utage.ExpressionToken Utage.ExpressionToken::LpaToken
	ExpressionToken_t2745466747 * ___LpaToken_23;
	// Utage.ExpressionToken Utage.ExpressionToken::RpaToken
	ExpressionToken_t2745466747 * ___RpaToken_24;
	// Utage.ExpressionToken Utage.ExpressionToken::CommaToken
	ExpressionToken_t2745466747 * ___CommaToken_25;
	// Utage.ExpressionToken Utage.ExpressionToken::UniPlus
	ExpressionToken_t2745466747 * ___UniPlus_26;
	// Utage.ExpressionToken Utage.ExpressionToken::UniMinus
	ExpressionToken_t2745466747 * ___UniMinus_27;
	// Utage.ExpressionToken[] Utage.ExpressionToken::OperatorArray
	ExpressionTokenU5BU5D_t142795834* ___OperatorArray_28;
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> Utage.ExpressionToken::<>f__switch$map4
	Dictionary_2_t3986656710 * ___U3CU3Ef__switchU24map4_41;
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> Utage.ExpressionToken::<>f__switch$map5
	Dictionary_2_t3986656710 * ___U3CU3Ef__switchU24map5_42;
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> Utage.ExpressionToken::<>f__switch$map6
	Dictionary_2_t3986656710 * ___U3CU3Ef__switchU24map6_43;
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> Utage.ExpressionToken::<>f__switch$map7
	Dictionary_2_t3986656710 * ___U3CU3Ef__switchU24map7_44;
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> Utage.ExpressionToken::<>f__switch$map8
	Dictionary_2_t3986656710 * ___U3CU3Ef__switchU24map8_45;

public:
	inline static int32_t get_offset_of_LpaToken_23() { return static_cast<int32_t>(offsetof(ExpressionToken_t2745466747_StaticFields, ___LpaToken_23)); }
	inline ExpressionToken_t2745466747 * get_LpaToken_23() const { return ___LpaToken_23; }
	inline ExpressionToken_t2745466747 ** get_address_of_LpaToken_23() { return &___LpaToken_23; }
	inline void set_LpaToken_23(ExpressionToken_t2745466747 * value)
	{
		___LpaToken_23 = value;
		Il2CppCodeGenWriteBarrier((&___LpaToken_23), value);
	}

	inline static int32_t get_offset_of_RpaToken_24() { return static_cast<int32_t>(offsetof(ExpressionToken_t2745466747_StaticFields, ___RpaToken_24)); }
	inline ExpressionToken_t2745466747 * get_RpaToken_24() const { return ___RpaToken_24; }
	inline ExpressionToken_t2745466747 ** get_address_of_RpaToken_24() { return &___RpaToken_24; }
	inline void set_RpaToken_24(ExpressionToken_t2745466747 * value)
	{
		___RpaToken_24 = value;
		Il2CppCodeGenWriteBarrier((&___RpaToken_24), value);
	}

	inline static int32_t get_offset_of_CommaToken_25() { return static_cast<int32_t>(offsetof(ExpressionToken_t2745466747_StaticFields, ___CommaToken_25)); }
	inline ExpressionToken_t2745466747 * get_CommaToken_25() const { return ___CommaToken_25; }
	inline ExpressionToken_t2745466747 ** get_address_of_CommaToken_25() { return &___CommaToken_25; }
	inline void set_CommaToken_25(ExpressionToken_t2745466747 * value)
	{
		___CommaToken_25 = value;
		Il2CppCodeGenWriteBarrier((&___CommaToken_25), value);
	}

	inline static int32_t get_offset_of_UniPlus_26() { return static_cast<int32_t>(offsetof(ExpressionToken_t2745466747_StaticFields, ___UniPlus_26)); }
	inline ExpressionToken_t2745466747 * get_UniPlus_26() const { return ___UniPlus_26; }
	inline ExpressionToken_t2745466747 ** get_address_of_UniPlus_26() { return &___UniPlus_26; }
	inline void set_UniPlus_26(ExpressionToken_t2745466747 * value)
	{
		___UniPlus_26 = value;
		Il2CppCodeGenWriteBarrier((&___UniPlus_26), value);
	}

	inline static int32_t get_offset_of_UniMinus_27() { return static_cast<int32_t>(offsetof(ExpressionToken_t2745466747_StaticFields, ___UniMinus_27)); }
	inline ExpressionToken_t2745466747 * get_UniMinus_27() const { return ___UniMinus_27; }
	inline ExpressionToken_t2745466747 ** get_address_of_UniMinus_27() { return &___UniMinus_27; }
	inline void set_UniMinus_27(ExpressionToken_t2745466747 * value)
	{
		___UniMinus_27 = value;
		Il2CppCodeGenWriteBarrier((&___UniMinus_27), value);
	}

	inline static int32_t get_offset_of_OperatorArray_28() { return static_cast<int32_t>(offsetof(ExpressionToken_t2745466747_StaticFields, ___OperatorArray_28)); }
	inline ExpressionTokenU5BU5D_t142795834* get_OperatorArray_28() const { return ___OperatorArray_28; }
	inline ExpressionTokenU5BU5D_t142795834** get_address_of_OperatorArray_28() { return &___OperatorArray_28; }
	inline void set_OperatorArray_28(ExpressionTokenU5BU5D_t142795834* value)
	{
		___OperatorArray_28 = value;
		Il2CppCodeGenWriteBarrier((&___OperatorArray_28), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__switchU24map4_41() { return static_cast<int32_t>(offsetof(ExpressionToken_t2745466747_StaticFields, ___U3CU3Ef__switchU24map4_41)); }
	inline Dictionary_2_t3986656710 * get_U3CU3Ef__switchU24map4_41() const { return ___U3CU3Ef__switchU24map4_41; }
	inline Dictionary_2_t3986656710 ** get_address_of_U3CU3Ef__switchU24map4_41() { return &___U3CU3Ef__switchU24map4_41; }
	inline void set_U3CU3Ef__switchU24map4_41(Dictionary_2_t3986656710 * value)
	{
		___U3CU3Ef__switchU24map4_41 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__switchU24map4_41), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__switchU24map5_42() { return static_cast<int32_t>(offsetof(ExpressionToken_t2745466747_StaticFields, ___U3CU3Ef__switchU24map5_42)); }
	inline Dictionary_2_t3986656710 * get_U3CU3Ef__switchU24map5_42() const { return ___U3CU3Ef__switchU24map5_42; }
	inline Dictionary_2_t3986656710 ** get_address_of_U3CU3Ef__switchU24map5_42() { return &___U3CU3Ef__switchU24map5_42; }
	inline void set_U3CU3Ef__switchU24map5_42(Dictionary_2_t3986656710 * value)
	{
		___U3CU3Ef__switchU24map5_42 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__switchU24map5_42), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__switchU24map6_43() { return static_cast<int32_t>(offsetof(ExpressionToken_t2745466747_StaticFields, ___U3CU3Ef__switchU24map6_43)); }
	inline Dictionary_2_t3986656710 * get_U3CU3Ef__switchU24map6_43() const { return ___U3CU3Ef__switchU24map6_43; }
	inline Dictionary_2_t3986656710 ** get_address_of_U3CU3Ef__switchU24map6_43() { return &___U3CU3Ef__switchU24map6_43; }
	inline void set_U3CU3Ef__switchU24map6_43(Dictionary_2_t3986656710 * value)
	{
		___U3CU3Ef__switchU24map6_43 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__switchU24map6_43), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__switchU24map7_44() { return static_cast<int32_t>(offsetof(ExpressionToken_t2745466747_StaticFields, ___U3CU3Ef__switchU24map7_44)); }
	inline Dictionary_2_t3986656710 * get_U3CU3Ef__switchU24map7_44() const { return ___U3CU3Ef__switchU24map7_44; }
	inline Dictionary_2_t3986656710 ** get_address_of_U3CU3Ef__switchU24map7_44() { return &___U3CU3Ef__switchU24map7_44; }
	inline void set_U3CU3Ef__switchU24map7_44(Dictionary_2_t3986656710 * value)
	{
		___U3CU3Ef__switchU24map7_44 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__switchU24map7_44), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__switchU24map8_45() { return static_cast<int32_t>(offsetof(ExpressionToken_t2745466747_StaticFields, ___U3CU3Ef__switchU24map8_45)); }
	inline Dictionary_2_t3986656710 * get_U3CU3Ef__switchU24map8_45() const { return ___U3CU3Ef__switchU24map8_45; }
	inline Dictionary_2_t3986656710 ** get_address_of_U3CU3Ef__switchU24map8_45() { return &___U3CU3Ef__switchU24map8_45; }
	inline void set_U3CU3Ef__switchU24map8_45(Dictionary_2_t3986656710 * value)
	{
		___U3CU3Ef__switchU24map8_45 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__switchU24map8_45), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EXPRESSIONTOKEN_T2745466747_H
#ifndef U3CLOADASYNCSUBU3EC__ITERATOR1_T979517335_H
#define U3CLOADASYNCSUBU3EC__ITERATOR1_T979517335_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.AssetFileUtage/<LoadAsyncSub>c__Iterator1
struct  U3CLoadAsyncSubU3Ec__Iterator1_t979517335  : public RuntimeObject
{
public:
	// Utage.AssetFileStrageType Utage.AssetFileUtage/<LoadAsyncSub>c__Iterator1::$locvar0
	int32_t ___U24locvar0_0;
	// System.String Utage.AssetFileUtage/<LoadAsyncSub>c__Iterator1::path
	String_t* ___path_1;
	// System.Action Utage.AssetFileUtage/<LoadAsyncSub>c__Iterator1::onComplete
	Action_t3226471752 * ___onComplete_2;
	// System.Action Utage.AssetFileUtage/<LoadAsyncSub>c__Iterator1::onFailed
	Action_t3226471752 * ___onFailed_3;
	// Utage.AssetFileUtage Utage.AssetFileUtage/<LoadAsyncSub>c__Iterator1::$this
	AssetFileUtage_t3568203916 * ___U24this_4;
	// System.Object Utage.AssetFileUtage/<LoadAsyncSub>c__Iterator1::$current
	RuntimeObject * ___U24current_5;
	// System.Boolean Utage.AssetFileUtage/<LoadAsyncSub>c__Iterator1::$disposing
	bool ___U24disposing_6;
	// System.Int32 Utage.AssetFileUtage/<LoadAsyncSub>c__Iterator1::$PC
	int32_t ___U24PC_7;

public:
	inline static int32_t get_offset_of_U24locvar0_0() { return static_cast<int32_t>(offsetof(U3CLoadAsyncSubU3Ec__Iterator1_t979517335, ___U24locvar0_0)); }
	inline int32_t get_U24locvar0_0() const { return ___U24locvar0_0; }
	inline int32_t* get_address_of_U24locvar0_0() { return &___U24locvar0_0; }
	inline void set_U24locvar0_0(int32_t value)
	{
		___U24locvar0_0 = value;
	}

	inline static int32_t get_offset_of_path_1() { return static_cast<int32_t>(offsetof(U3CLoadAsyncSubU3Ec__Iterator1_t979517335, ___path_1)); }
	inline String_t* get_path_1() const { return ___path_1; }
	inline String_t** get_address_of_path_1() { return &___path_1; }
	inline void set_path_1(String_t* value)
	{
		___path_1 = value;
		Il2CppCodeGenWriteBarrier((&___path_1), value);
	}

	inline static int32_t get_offset_of_onComplete_2() { return static_cast<int32_t>(offsetof(U3CLoadAsyncSubU3Ec__Iterator1_t979517335, ___onComplete_2)); }
	inline Action_t3226471752 * get_onComplete_2() const { return ___onComplete_2; }
	inline Action_t3226471752 ** get_address_of_onComplete_2() { return &___onComplete_2; }
	inline void set_onComplete_2(Action_t3226471752 * value)
	{
		___onComplete_2 = value;
		Il2CppCodeGenWriteBarrier((&___onComplete_2), value);
	}

	inline static int32_t get_offset_of_onFailed_3() { return static_cast<int32_t>(offsetof(U3CLoadAsyncSubU3Ec__Iterator1_t979517335, ___onFailed_3)); }
	inline Action_t3226471752 * get_onFailed_3() const { return ___onFailed_3; }
	inline Action_t3226471752 ** get_address_of_onFailed_3() { return &___onFailed_3; }
	inline void set_onFailed_3(Action_t3226471752 * value)
	{
		___onFailed_3 = value;
		Il2CppCodeGenWriteBarrier((&___onFailed_3), value);
	}

	inline static int32_t get_offset_of_U24this_4() { return static_cast<int32_t>(offsetof(U3CLoadAsyncSubU3Ec__Iterator1_t979517335, ___U24this_4)); }
	inline AssetFileUtage_t3568203916 * get_U24this_4() const { return ___U24this_4; }
	inline AssetFileUtage_t3568203916 ** get_address_of_U24this_4() { return &___U24this_4; }
	inline void set_U24this_4(AssetFileUtage_t3568203916 * value)
	{
		___U24this_4 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_4), value);
	}

	inline static int32_t get_offset_of_U24current_5() { return static_cast<int32_t>(offsetof(U3CLoadAsyncSubU3Ec__Iterator1_t979517335, ___U24current_5)); }
	inline RuntimeObject * get_U24current_5() const { return ___U24current_5; }
	inline RuntimeObject ** get_address_of_U24current_5() { return &___U24current_5; }
	inline void set_U24current_5(RuntimeObject * value)
	{
		___U24current_5 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_5), value);
	}

	inline static int32_t get_offset_of_U24disposing_6() { return static_cast<int32_t>(offsetof(U3CLoadAsyncSubU3Ec__Iterator1_t979517335, ___U24disposing_6)); }
	inline bool get_U24disposing_6() const { return ___U24disposing_6; }
	inline bool* get_address_of_U24disposing_6() { return &___U24disposing_6; }
	inline void set_U24disposing_6(bool value)
	{
		___U24disposing_6 = value;
	}

	inline static int32_t get_offset_of_U24PC_7() { return static_cast<int32_t>(offsetof(U3CLoadAsyncSubU3Ec__Iterator1_t979517335, ___U24PC_7)); }
	inline int32_t get_U24PC_7() const { return ___U24PC_7; }
	inline int32_t* get_address_of_U24PC_7() { return &___U24PC_7; }
	inline void set_U24PC_7(int32_t value)
	{
		___U24PC_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CLOADASYNCSUBU3EC__ITERATOR1_T979517335_H
#ifndef MULTICASTDELEGATE_T3201952435_H
#define MULTICASTDELEGATE_T3201952435_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.MulticastDelegate
struct  MulticastDelegate_t3201952435  : public Delegate_t3022476291
{
public:
	// System.MulticastDelegate System.MulticastDelegate::prev
	MulticastDelegate_t3201952435 * ___prev_9;
	// System.MulticastDelegate System.MulticastDelegate::kpm_next
	MulticastDelegate_t3201952435 * ___kpm_next_10;

public:
	inline static int32_t get_offset_of_prev_9() { return static_cast<int32_t>(offsetof(MulticastDelegate_t3201952435, ___prev_9)); }
	inline MulticastDelegate_t3201952435 * get_prev_9() const { return ___prev_9; }
	inline MulticastDelegate_t3201952435 ** get_address_of_prev_9() { return &___prev_9; }
	inline void set_prev_9(MulticastDelegate_t3201952435 * value)
	{
		___prev_9 = value;
		Il2CppCodeGenWriteBarrier((&___prev_9), value);
	}

	inline static int32_t get_offset_of_kpm_next_10() { return static_cast<int32_t>(offsetof(MulticastDelegate_t3201952435, ___kpm_next_10)); }
	inline MulticastDelegate_t3201952435 * get_kpm_next_10() const { return ___kpm_next_10; }
	inline MulticastDelegate_t3201952435 ** get_address_of_kpm_next_10() { return &___kpm_next_10; }
	inline void set_kpm_next_10(MulticastDelegate_t3201952435 * value)
	{
		___kpm_next_10 = value;
		Il2CppCodeGenWriteBarrier((&___kpm_next_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MULTICASTDELEGATE_T3201952435_H
#ifndef ASSETFILESETTING_T1869957816_H
#define ASSETFILESETTING_T1869957816_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.AssetFileSetting
struct  AssetFileSetting_t1869957816  : public RuntimeObject
{
public:
	// Utage.AssetFileType Utage.AssetFileSetting::fileType
	int32_t ___fileType_0;
	// System.Boolean Utage.AssetFileSetting::isStreamingAssets
	bool ___isStreamingAssets_1;
	// System.Collections.Generic.List`1<System.String> Utage.AssetFileSetting::extensions
	List_1_t1398341365 * ___extensions_2;
	// Utage.AssetFileManagerSettings Utage.AssetFileSetting::settings
	AssetFileManagerSettings_t1473658718 * ___settings_3;

public:
	inline static int32_t get_offset_of_fileType_0() { return static_cast<int32_t>(offsetof(AssetFileSetting_t1869957816, ___fileType_0)); }
	inline int32_t get_fileType_0() const { return ___fileType_0; }
	inline int32_t* get_address_of_fileType_0() { return &___fileType_0; }
	inline void set_fileType_0(int32_t value)
	{
		___fileType_0 = value;
	}

	inline static int32_t get_offset_of_isStreamingAssets_1() { return static_cast<int32_t>(offsetof(AssetFileSetting_t1869957816, ___isStreamingAssets_1)); }
	inline bool get_isStreamingAssets_1() const { return ___isStreamingAssets_1; }
	inline bool* get_address_of_isStreamingAssets_1() { return &___isStreamingAssets_1; }
	inline void set_isStreamingAssets_1(bool value)
	{
		___isStreamingAssets_1 = value;
	}

	inline static int32_t get_offset_of_extensions_2() { return static_cast<int32_t>(offsetof(AssetFileSetting_t1869957816, ___extensions_2)); }
	inline List_1_t1398341365 * get_extensions_2() const { return ___extensions_2; }
	inline List_1_t1398341365 ** get_address_of_extensions_2() { return &___extensions_2; }
	inline void set_extensions_2(List_1_t1398341365 * value)
	{
		___extensions_2 = value;
		Il2CppCodeGenWriteBarrier((&___extensions_2), value);
	}

	inline static int32_t get_offset_of_settings_3() { return static_cast<int32_t>(offsetof(AssetFileSetting_t1869957816, ___settings_3)); }
	inline AssetFileManagerSettings_t1473658718 * get_settings_3() const { return ___settings_3; }
	inline AssetFileManagerSettings_t1473658718 ** get_address_of_settings_3() { return &___settings_3; }
	inline void set_settings_3(AssetFileManagerSettings_t1473658718 * value)
	{
		___settings_3 = value;
		Il2CppCodeGenWriteBarrier((&___settings_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ASSETFILESETTING_T1869957816_H
#ifndef ASSETFILEINFO_T1386031564_H
#define ASSETFILEINFO_T1386031564_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.AssetFileInfo
struct  AssetFileInfo_t1386031564  : public RuntimeObject
{
public:
	// System.String Utage.AssetFileInfo::<FileName>k__BackingField
	String_t* ___U3CFileNameU3Ek__BackingField_0;
	// Utage.AssetBundleInfo Utage.AssetFileInfo::<AssetBundleInfo>k__BackingField
	AssetBundleInfo_t1249246306 * ___U3CAssetBundleInfoU3Ek__BackingField_1;
	// Utage.AssetFileSetting Utage.AssetFileInfo::<Setting>k__BackingField
	AssetFileSetting_t1869957816 * ___U3CSettingU3Ek__BackingField_2;
	// Utage.AssetFileStrageType Utage.AssetFileInfo::<StrageType>k__BackingField
	int32_t ___U3CStrageTypeU3Ek__BackingField_3;

public:
	inline static int32_t get_offset_of_U3CFileNameU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(AssetFileInfo_t1386031564, ___U3CFileNameU3Ek__BackingField_0)); }
	inline String_t* get_U3CFileNameU3Ek__BackingField_0() const { return ___U3CFileNameU3Ek__BackingField_0; }
	inline String_t** get_address_of_U3CFileNameU3Ek__BackingField_0() { return &___U3CFileNameU3Ek__BackingField_0; }
	inline void set_U3CFileNameU3Ek__BackingField_0(String_t* value)
	{
		___U3CFileNameU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CFileNameU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_U3CAssetBundleInfoU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(AssetFileInfo_t1386031564, ___U3CAssetBundleInfoU3Ek__BackingField_1)); }
	inline AssetBundleInfo_t1249246306 * get_U3CAssetBundleInfoU3Ek__BackingField_1() const { return ___U3CAssetBundleInfoU3Ek__BackingField_1; }
	inline AssetBundleInfo_t1249246306 ** get_address_of_U3CAssetBundleInfoU3Ek__BackingField_1() { return &___U3CAssetBundleInfoU3Ek__BackingField_1; }
	inline void set_U3CAssetBundleInfoU3Ek__BackingField_1(AssetBundleInfo_t1249246306 * value)
	{
		___U3CAssetBundleInfoU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CAssetBundleInfoU3Ek__BackingField_1), value);
	}

	inline static int32_t get_offset_of_U3CSettingU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(AssetFileInfo_t1386031564, ___U3CSettingU3Ek__BackingField_2)); }
	inline AssetFileSetting_t1869957816 * get_U3CSettingU3Ek__BackingField_2() const { return ___U3CSettingU3Ek__BackingField_2; }
	inline AssetFileSetting_t1869957816 ** get_address_of_U3CSettingU3Ek__BackingField_2() { return &___U3CSettingU3Ek__BackingField_2; }
	inline void set_U3CSettingU3Ek__BackingField_2(AssetFileSetting_t1869957816 * value)
	{
		___U3CSettingU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CSettingU3Ek__BackingField_2), value);
	}

	inline static int32_t get_offset_of_U3CStrageTypeU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(AssetFileInfo_t1386031564, ___U3CStrageTypeU3Ek__BackingField_3)); }
	inline int32_t get_U3CStrageTypeU3Ek__BackingField_3() const { return ___U3CStrageTypeU3Ek__BackingField_3; }
	inline int32_t* get_address_of_U3CStrageTypeU3Ek__BackingField_3() { return &___U3CStrageTypeU3Ek__BackingField_3; }
	inline void set_U3CStrageTypeU3Ek__BackingField_3(int32_t value)
	{
		___U3CStrageTypeU3Ek__BackingField_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ASSETFILEINFO_T1386031564_H
#ifndef COMPONENT_T3819376471_H
#define COMPONENT_T3819376471_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Component
struct  Component_t3819376471  : public Object_t1021602117
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPONENT_T3819376471_H
#ifndef ASSETFILEBASE_T4101360697_H
#define ASSETFILEBASE_T4101360697_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.AssetFileBase
struct  AssetFileBase_t4101360697  : public RuntimeObject
{
public:
	// Utage.AssetFileManager Utage.AssetFileBase::<FileManager>k__BackingField
	AssetFileManager_t1095395563 * ___U3CFileManagerU3Ek__BackingField_0;
	// Utage.AssetFileInfo Utage.AssetFileBase::<FileInfo>k__BackingField
	AssetFileInfo_t1386031564 * ___U3CFileInfoU3Ek__BackingField_1;
	// Utage.IAssetFileSettingData Utage.AssetFileBase::<SettingData>k__BackingField
	RuntimeObject* ___U3CSettingDataU3Ek__BackingField_2;
	// Utage.AssetFileType Utage.AssetFileBase::<FileType>k__BackingField
	int32_t ___U3CFileTypeU3Ek__BackingField_3;
	// System.Boolean Utage.AssetFileBase::<IsLoadEnd>k__BackingField
	bool ___U3CIsLoadEndU3Ek__BackingField_4;
	// System.Boolean Utage.AssetFileBase::<IsLoadError>k__BackingField
	bool ___U3CIsLoadErrorU3Ek__BackingField_5;
	// System.String Utage.AssetFileBase::<LoadErrorMsg>k__BackingField
	String_t* ___U3CLoadErrorMsgU3Ek__BackingField_6;
	// UnityEngine.TextAsset Utage.AssetFileBase::<Text>k__BackingField
	TextAsset_t3973159845 * ___U3CTextU3Ek__BackingField_7;
	// UnityEngine.Texture2D Utage.AssetFileBase::<Texture>k__BackingField
	Texture2D_t3542995729 * ___U3CTextureU3Ek__BackingField_8;
	// UnityEngine.AudioClip Utage.AssetFileBase::<Sound>k__BackingField
	AudioClip_t1932558630 * ___U3CSoundU3Ek__BackingField_9;
	// UnityEngine.Object Utage.AssetFileBase::<UnityObject>k__BackingField
	Object_t1021602117 * ___U3CUnityObjectU3Ek__BackingField_10;
	// Utage.AssetFileLoadPriority Utage.AssetFileBase::<Priority>k__BackingField
	int32_t ___U3CPriorityU3Ek__BackingField_11;
	// System.Boolean Utage.AssetFileBase::<IgnoreUnload>k__BackingField
	bool ___U3CIgnoreUnloadU3Ek__BackingField_12;
	// System.Collections.Generic.HashSet`1<System.Object> Utage.AssetFileBase::referenceSet
	HashSet_1_t1022910149 * ___referenceSet_13;

public:
	inline static int32_t get_offset_of_U3CFileManagerU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(AssetFileBase_t4101360697, ___U3CFileManagerU3Ek__BackingField_0)); }
	inline AssetFileManager_t1095395563 * get_U3CFileManagerU3Ek__BackingField_0() const { return ___U3CFileManagerU3Ek__BackingField_0; }
	inline AssetFileManager_t1095395563 ** get_address_of_U3CFileManagerU3Ek__BackingField_0() { return &___U3CFileManagerU3Ek__BackingField_0; }
	inline void set_U3CFileManagerU3Ek__BackingField_0(AssetFileManager_t1095395563 * value)
	{
		___U3CFileManagerU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CFileManagerU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_U3CFileInfoU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(AssetFileBase_t4101360697, ___U3CFileInfoU3Ek__BackingField_1)); }
	inline AssetFileInfo_t1386031564 * get_U3CFileInfoU3Ek__BackingField_1() const { return ___U3CFileInfoU3Ek__BackingField_1; }
	inline AssetFileInfo_t1386031564 ** get_address_of_U3CFileInfoU3Ek__BackingField_1() { return &___U3CFileInfoU3Ek__BackingField_1; }
	inline void set_U3CFileInfoU3Ek__BackingField_1(AssetFileInfo_t1386031564 * value)
	{
		___U3CFileInfoU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CFileInfoU3Ek__BackingField_1), value);
	}

	inline static int32_t get_offset_of_U3CSettingDataU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(AssetFileBase_t4101360697, ___U3CSettingDataU3Ek__BackingField_2)); }
	inline RuntimeObject* get_U3CSettingDataU3Ek__BackingField_2() const { return ___U3CSettingDataU3Ek__BackingField_2; }
	inline RuntimeObject** get_address_of_U3CSettingDataU3Ek__BackingField_2() { return &___U3CSettingDataU3Ek__BackingField_2; }
	inline void set_U3CSettingDataU3Ek__BackingField_2(RuntimeObject* value)
	{
		___U3CSettingDataU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CSettingDataU3Ek__BackingField_2), value);
	}

	inline static int32_t get_offset_of_U3CFileTypeU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(AssetFileBase_t4101360697, ___U3CFileTypeU3Ek__BackingField_3)); }
	inline int32_t get_U3CFileTypeU3Ek__BackingField_3() const { return ___U3CFileTypeU3Ek__BackingField_3; }
	inline int32_t* get_address_of_U3CFileTypeU3Ek__BackingField_3() { return &___U3CFileTypeU3Ek__BackingField_3; }
	inline void set_U3CFileTypeU3Ek__BackingField_3(int32_t value)
	{
		___U3CFileTypeU3Ek__BackingField_3 = value;
	}

	inline static int32_t get_offset_of_U3CIsLoadEndU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(AssetFileBase_t4101360697, ___U3CIsLoadEndU3Ek__BackingField_4)); }
	inline bool get_U3CIsLoadEndU3Ek__BackingField_4() const { return ___U3CIsLoadEndU3Ek__BackingField_4; }
	inline bool* get_address_of_U3CIsLoadEndU3Ek__BackingField_4() { return &___U3CIsLoadEndU3Ek__BackingField_4; }
	inline void set_U3CIsLoadEndU3Ek__BackingField_4(bool value)
	{
		___U3CIsLoadEndU3Ek__BackingField_4 = value;
	}

	inline static int32_t get_offset_of_U3CIsLoadErrorU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(AssetFileBase_t4101360697, ___U3CIsLoadErrorU3Ek__BackingField_5)); }
	inline bool get_U3CIsLoadErrorU3Ek__BackingField_5() const { return ___U3CIsLoadErrorU3Ek__BackingField_5; }
	inline bool* get_address_of_U3CIsLoadErrorU3Ek__BackingField_5() { return &___U3CIsLoadErrorU3Ek__BackingField_5; }
	inline void set_U3CIsLoadErrorU3Ek__BackingField_5(bool value)
	{
		___U3CIsLoadErrorU3Ek__BackingField_5 = value;
	}

	inline static int32_t get_offset_of_U3CLoadErrorMsgU3Ek__BackingField_6() { return static_cast<int32_t>(offsetof(AssetFileBase_t4101360697, ___U3CLoadErrorMsgU3Ek__BackingField_6)); }
	inline String_t* get_U3CLoadErrorMsgU3Ek__BackingField_6() const { return ___U3CLoadErrorMsgU3Ek__BackingField_6; }
	inline String_t** get_address_of_U3CLoadErrorMsgU3Ek__BackingField_6() { return &___U3CLoadErrorMsgU3Ek__BackingField_6; }
	inline void set_U3CLoadErrorMsgU3Ek__BackingField_6(String_t* value)
	{
		___U3CLoadErrorMsgU3Ek__BackingField_6 = value;
		Il2CppCodeGenWriteBarrier((&___U3CLoadErrorMsgU3Ek__BackingField_6), value);
	}

	inline static int32_t get_offset_of_U3CTextU3Ek__BackingField_7() { return static_cast<int32_t>(offsetof(AssetFileBase_t4101360697, ___U3CTextU3Ek__BackingField_7)); }
	inline TextAsset_t3973159845 * get_U3CTextU3Ek__BackingField_7() const { return ___U3CTextU3Ek__BackingField_7; }
	inline TextAsset_t3973159845 ** get_address_of_U3CTextU3Ek__BackingField_7() { return &___U3CTextU3Ek__BackingField_7; }
	inline void set_U3CTextU3Ek__BackingField_7(TextAsset_t3973159845 * value)
	{
		___U3CTextU3Ek__BackingField_7 = value;
		Il2CppCodeGenWriteBarrier((&___U3CTextU3Ek__BackingField_7), value);
	}

	inline static int32_t get_offset_of_U3CTextureU3Ek__BackingField_8() { return static_cast<int32_t>(offsetof(AssetFileBase_t4101360697, ___U3CTextureU3Ek__BackingField_8)); }
	inline Texture2D_t3542995729 * get_U3CTextureU3Ek__BackingField_8() const { return ___U3CTextureU3Ek__BackingField_8; }
	inline Texture2D_t3542995729 ** get_address_of_U3CTextureU3Ek__BackingField_8() { return &___U3CTextureU3Ek__BackingField_8; }
	inline void set_U3CTextureU3Ek__BackingField_8(Texture2D_t3542995729 * value)
	{
		___U3CTextureU3Ek__BackingField_8 = value;
		Il2CppCodeGenWriteBarrier((&___U3CTextureU3Ek__BackingField_8), value);
	}

	inline static int32_t get_offset_of_U3CSoundU3Ek__BackingField_9() { return static_cast<int32_t>(offsetof(AssetFileBase_t4101360697, ___U3CSoundU3Ek__BackingField_9)); }
	inline AudioClip_t1932558630 * get_U3CSoundU3Ek__BackingField_9() const { return ___U3CSoundU3Ek__BackingField_9; }
	inline AudioClip_t1932558630 ** get_address_of_U3CSoundU3Ek__BackingField_9() { return &___U3CSoundU3Ek__BackingField_9; }
	inline void set_U3CSoundU3Ek__BackingField_9(AudioClip_t1932558630 * value)
	{
		___U3CSoundU3Ek__BackingField_9 = value;
		Il2CppCodeGenWriteBarrier((&___U3CSoundU3Ek__BackingField_9), value);
	}

	inline static int32_t get_offset_of_U3CUnityObjectU3Ek__BackingField_10() { return static_cast<int32_t>(offsetof(AssetFileBase_t4101360697, ___U3CUnityObjectU3Ek__BackingField_10)); }
	inline Object_t1021602117 * get_U3CUnityObjectU3Ek__BackingField_10() const { return ___U3CUnityObjectU3Ek__BackingField_10; }
	inline Object_t1021602117 ** get_address_of_U3CUnityObjectU3Ek__BackingField_10() { return &___U3CUnityObjectU3Ek__BackingField_10; }
	inline void set_U3CUnityObjectU3Ek__BackingField_10(Object_t1021602117 * value)
	{
		___U3CUnityObjectU3Ek__BackingField_10 = value;
		Il2CppCodeGenWriteBarrier((&___U3CUnityObjectU3Ek__BackingField_10), value);
	}

	inline static int32_t get_offset_of_U3CPriorityU3Ek__BackingField_11() { return static_cast<int32_t>(offsetof(AssetFileBase_t4101360697, ___U3CPriorityU3Ek__BackingField_11)); }
	inline int32_t get_U3CPriorityU3Ek__BackingField_11() const { return ___U3CPriorityU3Ek__BackingField_11; }
	inline int32_t* get_address_of_U3CPriorityU3Ek__BackingField_11() { return &___U3CPriorityU3Ek__BackingField_11; }
	inline void set_U3CPriorityU3Ek__BackingField_11(int32_t value)
	{
		___U3CPriorityU3Ek__BackingField_11 = value;
	}

	inline static int32_t get_offset_of_U3CIgnoreUnloadU3Ek__BackingField_12() { return static_cast<int32_t>(offsetof(AssetFileBase_t4101360697, ___U3CIgnoreUnloadU3Ek__BackingField_12)); }
	inline bool get_U3CIgnoreUnloadU3Ek__BackingField_12() const { return ___U3CIgnoreUnloadU3Ek__BackingField_12; }
	inline bool* get_address_of_U3CIgnoreUnloadU3Ek__BackingField_12() { return &___U3CIgnoreUnloadU3Ek__BackingField_12; }
	inline void set_U3CIgnoreUnloadU3Ek__BackingField_12(bool value)
	{
		___U3CIgnoreUnloadU3Ek__BackingField_12 = value;
	}

	inline static int32_t get_offset_of_referenceSet_13() { return static_cast<int32_t>(offsetof(AssetFileBase_t4101360697, ___referenceSet_13)); }
	inline HashSet_1_t1022910149 * get_referenceSet_13() const { return ___referenceSet_13; }
	inline HashSet_1_t1022910149 ** get_address_of_referenceSet_13() { return &___referenceSet_13; }
	inline void set_referenceSet_13(HashSet_1_t1022910149 * value)
	{
		___referenceSet_13 = value;
		Il2CppCodeGenWriteBarrier((&___referenceSet_13), value);
	}
};

struct AssetFileBase_t4101360697_StaticFields
{
public:
	// System.Predicate`1<System.Object> Utage.AssetFileBase::<>f__am$cache0
	Predicate_1_t1132419410 * ___U3CU3Ef__amU24cache0_14;

public:
	inline static int32_t get_offset_of_U3CU3Ef__amU24cache0_14() { return static_cast<int32_t>(offsetof(AssetFileBase_t4101360697_StaticFields, ___U3CU3Ef__amU24cache0_14)); }
	inline Predicate_1_t1132419410 * get_U3CU3Ef__amU24cache0_14() const { return ___U3CU3Ef__amU24cache0_14; }
	inline Predicate_1_t1132419410 ** get_address_of_U3CU3Ef__amU24cache0_14() { return &___U3CU3Ef__amU24cache0_14; }
	inline void set_U3CU3Ef__amU24cache0_14(Predicate_1_t1132419410 * value)
	{
		___U3CU3Ef__amU24cache0_14 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache0_14), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ASSETFILEBASE_T4101360697_H
#ifndef WWWEX_T1775400116_H
#define WWWEX_T1775400116_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.WWWEx
struct  WWWEx_t1775400116  : public RuntimeObject
{
public:
	// System.String Utage.WWWEx::<Url>k__BackingField
	String_t* ___U3CUrlU3Ek__BackingField_0;
	// UnityEngine.Hash128 Utage.WWWEx::<AssetBundleHash>k__BackingField
	Hash128_t2836532937  ___U3CAssetBundleHashU3Ek__BackingField_1;
	// System.Int32 Utage.WWWEx::<AssetBundleVersion>k__BackingField
	int32_t ___U3CAssetBundleVersionU3Ek__BackingField_2;
	// Utage.WWWEx/Type Utage.WWWEx::<LoadType>k__BackingField
	int32_t ___U3CLoadTypeU3Ek__BackingField_3;
	// System.Int32 Utage.WWWEx::<RetryCount>k__BackingField
	int32_t ___U3CRetryCountU3Ek__BackingField_4;
	// System.Single Utage.WWWEx::<TimeOut>k__BackingField
	float ___U3CTimeOutU3Ek__BackingField_5;
	// System.Single Utage.WWWEx::<Progress>k__BackingField
	float ___U3CProgressU3Ek__BackingField_6;
	// System.Action`1<Utage.WWWEx> Utage.WWWEx::<OnUpdate>k__BackingField
	Action_1_t1577199498 * ___U3COnUpdateU3Ek__BackingField_7;
	// System.Boolean Utage.WWWEx::<IgnoreDebugLog>k__BackingField
	bool ___U3CIgnoreDebugLogU3Ek__BackingField_8;
	// System.Boolean Utage.WWWEx::<StoreBytes>k__BackingField
	bool ___U3CStoreBytesU3Ek__BackingField_9;
	// System.Byte[] Utage.WWWEx::<Bytes>k__BackingField
	ByteU5BU5D_t3397334013* ___U3CBytesU3Ek__BackingField_10;

public:
	inline static int32_t get_offset_of_U3CUrlU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(WWWEx_t1775400116, ___U3CUrlU3Ek__BackingField_0)); }
	inline String_t* get_U3CUrlU3Ek__BackingField_0() const { return ___U3CUrlU3Ek__BackingField_0; }
	inline String_t** get_address_of_U3CUrlU3Ek__BackingField_0() { return &___U3CUrlU3Ek__BackingField_0; }
	inline void set_U3CUrlU3Ek__BackingField_0(String_t* value)
	{
		___U3CUrlU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CUrlU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_U3CAssetBundleHashU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(WWWEx_t1775400116, ___U3CAssetBundleHashU3Ek__BackingField_1)); }
	inline Hash128_t2836532937  get_U3CAssetBundleHashU3Ek__BackingField_1() const { return ___U3CAssetBundleHashU3Ek__BackingField_1; }
	inline Hash128_t2836532937 * get_address_of_U3CAssetBundleHashU3Ek__BackingField_1() { return &___U3CAssetBundleHashU3Ek__BackingField_1; }
	inline void set_U3CAssetBundleHashU3Ek__BackingField_1(Hash128_t2836532937  value)
	{
		___U3CAssetBundleHashU3Ek__BackingField_1 = value;
	}

	inline static int32_t get_offset_of_U3CAssetBundleVersionU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(WWWEx_t1775400116, ___U3CAssetBundleVersionU3Ek__BackingField_2)); }
	inline int32_t get_U3CAssetBundleVersionU3Ek__BackingField_2() const { return ___U3CAssetBundleVersionU3Ek__BackingField_2; }
	inline int32_t* get_address_of_U3CAssetBundleVersionU3Ek__BackingField_2() { return &___U3CAssetBundleVersionU3Ek__BackingField_2; }
	inline void set_U3CAssetBundleVersionU3Ek__BackingField_2(int32_t value)
	{
		___U3CAssetBundleVersionU3Ek__BackingField_2 = value;
	}

	inline static int32_t get_offset_of_U3CLoadTypeU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(WWWEx_t1775400116, ___U3CLoadTypeU3Ek__BackingField_3)); }
	inline int32_t get_U3CLoadTypeU3Ek__BackingField_3() const { return ___U3CLoadTypeU3Ek__BackingField_3; }
	inline int32_t* get_address_of_U3CLoadTypeU3Ek__BackingField_3() { return &___U3CLoadTypeU3Ek__BackingField_3; }
	inline void set_U3CLoadTypeU3Ek__BackingField_3(int32_t value)
	{
		___U3CLoadTypeU3Ek__BackingField_3 = value;
	}

	inline static int32_t get_offset_of_U3CRetryCountU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(WWWEx_t1775400116, ___U3CRetryCountU3Ek__BackingField_4)); }
	inline int32_t get_U3CRetryCountU3Ek__BackingField_4() const { return ___U3CRetryCountU3Ek__BackingField_4; }
	inline int32_t* get_address_of_U3CRetryCountU3Ek__BackingField_4() { return &___U3CRetryCountU3Ek__BackingField_4; }
	inline void set_U3CRetryCountU3Ek__BackingField_4(int32_t value)
	{
		___U3CRetryCountU3Ek__BackingField_4 = value;
	}

	inline static int32_t get_offset_of_U3CTimeOutU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(WWWEx_t1775400116, ___U3CTimeOutU3Ek__BackingField_5)); }
	inline float get_U3CTimeOutU3Ek__BackingField_5() const { return ___U3CTimeOutU3Ek__BackingField_5; }
	inline float* get_address_of_U3CTimeOutU3Ek__BackingField_5() { return &___U3CTimeOutU3Ek__BackingField_5; }
	inline void set_U3CTimeOutU3Ek__BackingField_5(float value)
	{
		___U3CTimeOutU3Ek__BackingField_5 = value;
	}

	inline static int32_t get_offset_of_U3CProgressU3Ek__BackingField_6() { return static_cast<int32_t>(offsetof(WWWEx_t1775400116, ___U3CProgressU3Ek__BackingField_6)); }
	inline float get_U3CProgressU3Ek__BackingField_6() const { return ___U3CProgressU3Ek__BackingField_6; }
	inline float* get_address_of_U3CProgressU3Ek__BackingField_6() { return &___U3CProgressU3Ek__BackingField_6; }
	inline void set_U3CProgressU3Ek__BackingField_6(float value)
	{
		___U3CProgressU3Ek__BackingField_6 = value;
	}

	inline static int32_t get_offset_of_U3COnUpdateU3Ek__BackingField_7() { return static_cast<int32_t>(offsetof(WWWEx_t1775400116, ___U3COnUpdateU3Ek__BackingField_7)); }
	inline Action_1_t1577199498 * get_U3COnUpdateU3Ek__BackingField_7() const { return ___U3COnUpdateU3Ek__BackingField_7; }
	inline Action_1_t1577199498 ** get_address_of_U3COnUpdateU3Ek__BackingField_7() { return &___U3COnUpdateU3Ek__BackingField_7; }
	inline void set_U3COnUpdateU3Ek__BackingField_7(Action_1_t1577199498 * value)
	{
		___U3COnUpdateU3Ek__BackingField_7 = value;
		Il2CppCodeGenWriteBarrier((&___U3COnUpdateU3Ek__BackingField_7), value);
	}

	inline static int32_t get_offset_of_U3CIgnoreDebugLogU3Ek__BackingField_8() { return static_cast<int32_t>(offsetof(WWWEx_t1775400116, ___U3CIgnoreDebugLogU3Ek__BackingField_8)); }
	inline bool get_U3CIgnoreDebugLogU3Ek__BackingField_8() const { return ___U3CIgnoreDebugLogU3Ek__BackingField_8; }
	inline bool* get_address_of_U3CIgnoreDebugLogU3Ek__BackingField_8() { return &___U3CIgnoreDebugLogU3Ek__BackingField_8; }
	inline void set_U3CIgnoreDebugLogU3Ek__BackingField_8(bool value)
	{
		___U3CIgnoreDebugLogU3Ek__BackingField_8 = value;
	}

	inline static int32_t get_offset_of_U3CStoreBytesU3Ek__BackingField_9() { return static_cast<int32_t>(offsetof(WWWEx_t1775400116, ___U3CStoreBytesU3Ek__BackingField_9)); }
	inline bool get_U3CStoreBytesU3Ek__BackingField_9() const { return ___U3CStoreBytesU3Ek__BackingField_9; }
	inline bool* get_address_of_U3CStoreBytesU3Ek__BackingField_9() { return &___U3CStoreBytesU3Ek__BackingField_9; }
	inline void set_U3CStoreBytesU3Ek__BackingField_9(bool value)
	{
		___U3CStoreBytesU3Ek__BackingField_9 = value;
	}

	inline static int32_t get_offset_of_U3CBytesU3Ek__BackingField_10() { return static_cast<int32_t>(offsetof(WWWEx_t1775400116, ___U3CBytesU3Ek__BackingField_10)); }
	inline ByteU5BU5D_t3397334013* get_U3CBytesU3Ek__BackingField_10() const { return ___U3CBytesU3Ek__BackingField_10; }
	inline ByteU5BU5D_t3397334013** get_address_of_U3CBytesU3Ek__BackingField_10() { return &___U3CBytesU3Ek__BackingField_10; }
	inline void set_U3CBytesU3Ek__BackingField_10(ByteU5BU5D_t3397334013* value)
	{
		___U3CBytesU3Ek__BackingField_10 = value;
		Il2CppCodeGenWriteBarrier((&___U3CBytesU3Ek__BackingField_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WWWEX_T1775400116_H
#ifndef FINDASSET_T4163870574_H
#define FINDASSET_T4163870574_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.CustomLoadManager/FindAsset
struct  FindAsset_t4163870574  : public MulticastDelegate_t3201952435
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FINDASSET_T4163870574_H
#ifndef STATICASSETFILE_T628233278_H
#define STATICASSETFILE_T628233278_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.StaticAssetFile
struct  StaticAssetFile_t628233278  : public AssetFileBase_t4101360697
{
public:
	// Utage.StaticAsset Utage.StaticAssetFile::<Asset>k__BackingField
	StaticAsset_t1112757920 * ___U3CAssetU3Ek__BackingField_15;

public:
	inline static int32_t get_offset_of_U3CAssetU3Ek__BackingField_15() { return static_cast<int32_t>(offsetof(StaticAssetFile_t628233278, ___U3CAssetU3Ek__BackingField_15)); }
	inline StaticAsset_t1112757920 * get_U3CAssetU3Ek__BackingField_15() const { return ___U3CAssetU3Ek__BackingField_15; }
	inline StaticAsset_t1112757920 ** get_address_of_U3CAssetU3Ek__BackingField_15() { return &___U3CAssetU3Ek__BackingField_15; }
	inline void set_U3CAssetU3Ek__BackingField_15(StaticAsset_t1112757920 * value)
	{
		___U3CAssetU3Ek__BackingField_15 = value;
		Il2CppCodeGenWriteBarrier((&___U3CAssetU3Ek__BackingField_15), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STATICASSETFILE_T628233278_H
#ifndef ASSETFILEEVENT_T691182924_H
#define ASSETFILEEVENT_T691182924_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.AssetFileEvent
struct  AssetFileEvent_t691182924  : public MulticastDelegate_t3201952435
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ASSETFILEEVENT_T691182924_H
#ifndef ASSETFILEUTAGE_T3568203916_H
#define ASSETFILEUTAGE_T3568203916_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.AssetFileUtage
struct  AssetFileUtage_t3568203916  : public AssetFileBase_t4101360697
{
public:
	// System.String Utage.AssetFileUtage::<LoadPath>k__BackingField
	String_t* ___U3CLoadPathU3Ek__BackingField_15;
	// UnityEngine.AssetBundle Utage.AssetFileUtage::<AssetBundle>k__BackingField
	AssetBundle_t2054978754 * ___U3CAssetBundleU3Ek__BackingField_16;

public:
	inline static int32_t get_offset_of_U3CLoadPathU3Ek__BackingField_15() { return static_cast<int32_t>(offsetof(AssetFileUtage_t3568203916, ___U3CLoadPathU3Ek__BackingField_15)); }
	inline String_t* get_U3CLoadPathU3Ek__BackingField_15() const { return ___U3CLoadPathU3Ek__BackingField_15; }
	inline String_t** get_address_of_U3CLoadPathU3Ek__BackingField_15() { return &___U3CLoadPathU3Ek__BackingField_15; }
	inline void set_U3CLoadPathU3Ek__BackingField_15(String_t* value)
	{
		___U3CLoadPathU3Ek__BackingField_15 = value;
		Il2CppCodeGenWriteBarrier((&___U3CLoadPathU3Ek__BackingField_15), value);
	}

	inline static int32_t get_offset_of_U3CAssetBundleU3Ek__BackingField_16() { return static_cast<int32_t>(offsetof(AssetFileUtage_t3568203916, ___U3CAssetBundleU3Ek__BackingField_16)); }
	inline AssetBundle_t2054978754 * get_U3CAssetBundleU3Ek__BackingField_16() const { return ___U3CAssetBundleU3Ek__BackingField_16; }
	inline AssetBundle_t2054978754 ** get_address_of_U3CAssetBundleU3Ek__BackingField_16() { return &___U3CAssetBundleU3Ek__BackingField_16; }
	inline void set_U3CAssetBundleU3Ek__BackingField_16(AssetBundle_t2054978754 * value)
	{
		___U3CAssetBundleU3Ek__BackingField_16 = value;
		Il2CppCodeGenWriteBarrier((&___U3CAssetBundleU3Ek__BackingField_16), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ASSETFILEUTAGE_T3568203916_H
#ifndef BEHAVIOUR_T955675639_H
#define BEHAVIOUR_T955675639_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Behaviour
struct  Behaviour_t955675639  : public Component_t3819376471
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BEHAVIOUR_T955675639_H
#ifndef ASSETFILELOADCOMPLETE_T3808879259_H
#define ASSETFILELOADCOMPLETE_T3808879259_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.AssetFileLoadComplete
struct  AssetFileLoadComplete_t3808879259  : public MulticastDelegate_t3201952435
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ASSETFILELOADCOMPLETE_T3808879259_H
#ifndef MONOBEHAVIOUR_T1158329972_H
#define MONOBEHAVIOUR_T1158329972_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.MonoBehaviour
struct  MonoBehaviour_t1158329972  : public Behaviour_t955675639
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MONOBEHAVIOUR_T1158329972_H
#ifndef CONVERTFILELISTMANAGER_T2896753090_H
#define CONVERTFILELISTMANAGER_T2896753090_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.ConvertFileListManager
struct  ConvertFileListManager_t2896753090  : public MonoBehaviour_t1158329972
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONVERTFILELISTMANAGER_T2896753090_H
#ifndef FILEIOMANAGERBASE_T4139865030_H
#define FILEIOMANAGERBASE_T4139865030_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.FileIOManagerBase
struct  FileIOManagerBase_t4139865030  : public MonoBehaviour_t1158329972
{
public:

public:
};

struct FileIOManagerBase_t4139865030_StaticFields
{
public:
	// System.Func`3<System.Byte[],System.Byte[],System.Byte[]> Utage.FileIOManagerBase::customEncode
	Func_3_t1164674077 * ___customEncode_3;
	// System.Func`3<System.Byte[],System.Byte[],System.Byte[]> Utage.FileIOManagerBase::customDecode
	Func_3_t1164674077 * ___customDecode_4;
	// System.Action`4<System.Byte[],System.Byte[],System.Int32,System.Int32> Utage.FileIOManagerBase::customEncodeNoCompress
	Action_4_t3159657519 * ___customEncodeNoCompress_5;
	// System.Action`4<System.Byte[],System.Byte[],System.Int32,System.Int32> Utage.FileIOManagerBase::customDecodeNoCompress
	Action_4_t3159657519 * ___customDecodeNoCompress_6;
	// System.Int32[] Utage.FileIOManagerBase::audioHeader
	Int32U5BU5D_t3030399641* ___audioHeader_7;
	// System.Byte[] Utage.FileIOManagerBase::workBufferArray
	ByteU5BU5D_t3397334013* ___workBufferArray_11;
	// System.Int16[] Utage.FileIOManagerBase::audioShortWorkArray
	Int16U5BU5D_t3104283263* ___audioShortWorkArray_12;
	// System.Single[] Utage.FileIOManagerBase::audioSamplesWorkArray
	SingleU5BU5D_t577127397* ___audioSamplesWorkArray_13;
	// System.Func`3<System.Byte[],System.Byte[],System.Byte[]> Utage.FileIOManagerBase::<>f__mg$cache0
	Func_3_t1164674077 * ___U3CU3Ef__mgU24cache0_14;
	// System.Func`3<System.Byte[],System.Byte[],System.Byte[]> Utage.FileIOManagerBase::<>f__mg$cache1
	Func_3_t1164674077 * ___U3CU3Ef__mgU24cache1_15;
	// System.Action`4<System.Byte[],System.Byte[],System.Int32,System.Int32> Utage.FileIOManagerBase::<>f__mg$cache2
	Action_4_t3159657519 * ___U3CU3Ef__mgU24cache2_16;
	// System.Action`4<System.Byte[],System.Byte[],System.Int32,System.Int32> Utage.FileIOManagerBase::<>f__mg$cache3
	Action_4_t3159657519 * ___U3CU3Ef__mgU24cache3_17;

public:
	inline static int32_t get_offset_of_customEncode_3() { return static_cast<int32_t>(offsetof(FileIOManagerBase_t4139865030_StaticFields, ___customEncode_3)); }
	inline Func_3_t1164674077 * get_customEncode_3() const { return ___customEncode_3; }
	inline Func_3_t1164674077 ** get_address_of_customEncode_3() { return &___customEncode_3; }
	inline void set_customEncode_3(Func_3_t1164674077 * value)
	{
		___customEncode_3 = value;
		Il2CppCodeGenWriteBarrier((&___customEncode_3), value);
	}

	inline static int32_t get_offset_of_customDecode_4() { return static_cast<int32_t>(offsetof(FileIOManagerBase_t4139865030_StaticFields, ___customDecode_4)); }
	inline Func_3_t1164674077 * get_customDecode_4() const { return ___customDecode_4; }
	inline Func_3_t1164674077 ** get_address_of_customDecode_4() { return &___customDecode_4; }
	inline void set_customDecode_4(Func_3_t1164674077 * value)
	{
		___customDecode_4 = value;
		Il2CppCodeGenWriteBarrier((&___customDecode_4), value);
	}

	inline static int32_t get_offset_of_customEncodeNoCompress_5() { return static_cast<int32_t>(offsetof(FileIOManagerBase_t4139865030_StaticFields, ___customEncodeNoCompress_5)); }
	inline Action_4_t3159657519 * get_customEncodeNoCompress_5() const { return ___customEncodeNoCompress_5; }
	inline Action_4_t3159657519 ** get_address_of_customEncodeNoCompress_5() { return &___customEncodeNoCompress_5; }
	inline void set_customEncodeNoCompress_5(Action_4_t3159657519 * value)
	{
		___customEncodeNoCompress_5 = value;
		Il2CppCodeGenWriteBarrier((&___customEncodeNoCompress_5), value);
	}

	inline static int32_t get_offset_of_customDecodeNoCompress_6() { return static_cast<int32_t>(offsetof(FileIOManagerBase_t4139865030_StaticFields, ___customDecodeNoCompress_6)); }
	inline Action_4_t3159657519 * get_customDecodeNoCompress_6() const { return ___customDecodeNoCompress_6; }
	inline Action_4_t3159657519 ** get_address_of_customDecodeNoCompress_6() { return &___customDecodeNoCompress_6; }
	inline void set_customDecodeNoCompress_6(Action_4_t3159657519 * value)
	{
		___customDecodeNoCompress_6 = value;
		Il2CppCodeGenWriteBarrier((&___customDecodeNoCompress_6), value);
	}

	inline static int32_t get_offset_of_audioHeader_7() { return static_cast<int32_t>(offsetof(FileIOManagerBase_t4139865030_StaticFields, ___audioHeader_7)); }
	inline Int32U5BU5D_t3030399641* get_audioHeader_7() const { return ___audioHeader_7; }
	inline Int32U5BU5D_t3030399641** get_address_of_audioHeader_7() { return &___audioHeader_7; }
	inline void set_audioHeader_7(Int32U5BU5D_t3030399641* value)
	{
		___audioHeader_7 = value;
		Il2CppCodeGenWriteBarrier((&___audioHeader_7), value);
	}

	inline static int32_t get_offset_of_workBufferArray_11() { return static_cast<int32_t>(offsetof(FileIOManagerBase_t4139865030_StaticFields, ___workBufferArray_11)); }
	inline ByteU5BU5D_t3397334013* get_workBufferArray_11() const { return ___workBufferArray_11; }
	inline ByteU5BU5D_t3397334013** get_address_of_workBufferArray_11() { return &___workBufferArray_11; }
	inline void set_workBufferArray_11(ByteU5BU5D_t3397334013* value)
	{
		___workBufferArray_11 = value;
		Il2CppCodeGenWriteBarrier((&___workBufferArray_11), value);
	}

	inline static int32_t get_offset_of_audioShortWorkArray_12() { return static_cast<int32_t>(offsetof(FileIOManagerBase_t4139865030_StaticFields, ___audioShortWorkArray_12)); }
	inline Int16U5BU5D_t3104283263* get_audioShortWorkArray_12() const { return ___audioShortWorkArray_12; }
	inline Int16U5BU5D_t3104283263** get_address_of_audioShortWorkArray_12() { return &___audioShortWorkArray_12; }
	inline void set_audioShortWorkArray_12(Int16U5BU5D_t3104283263* value)
	{
		___audioShortWorkArray_12 = value;
		Il2CppCodeGenWriteBarrier((&___audioShortWorkArray_12), value);
	}

	inline static int32_t get_offset_of_audioSamplesWorkArray_13() { return static_cast<int32_t>(offsetof(FileIOManagerBase_t4139865030_StaticFields, ___audioSamplesWorkArray_13)); }
	inline SingleU5BU5D_t577127397* get_audioSamplesWorkArray_13() const { return ___audioSamplesWorkArray_13; }
	inline SingleU5BU5D_t577127397** get_address_of_audioSamplesWorkArray_13() { return &___audioSamplesWorkArray_13; }
	inline void set_audioSamplesWorkArray_13(SingleU5BU5D_t577127397* value)
	{
		___audioSamplesWorkArray_13 = value;
		Il2CppCodeGenWriteBarrier((&___audioSamplesWorkArray_13), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache0_14() { return static_cast<int32_t>(offsetof(FileIOManagerBase_t4139865030_StaticFields, ___U3CU3Ef__mgU24cache0_14)); }
	inline Func_3_t1164674077 * get_U3CU3Ef__mgU24cache0_14() const { return ___U3CU3Ef__mgU24cache0_14; }
	inline Func_3_t1164674077 ** get_address_of_U3CU3Ef__mgU24cache0_14() { return &___U3CU3Ef__mgU24cache0_14; }
	inline void set_U3CU3Ef__mgU24cache0_14(Func_3_t1164674077 * value)
	{
		___U3CU3Ef__mgU24cache0_14 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache0_14), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache1_15() { return static_cast<int32_t>(offsetof(FileIOManagerBase_t4139865030_StaticFields, ___U3CU3Ef__mgU24cache1_15)); }
	inline Func_3_t1164674077 * get_U3CU3Ef__mgU24cache1_15() const { return ___U3CU3Ef__mgU24cache1_15; }
	inline Func_3_t1164674077 ** get_address_of_U3CU3Ef__mgU24cache1_15() { return &___U3CU3Ef__mgU24cache1_15; }
	inline void set_U3CU3Ef__mgU24cache1_15(Func_3_t1164674077 * value)
	{
		___U3CU3Ef__mgU24cache1_15 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache1_15), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache2_16() { return static_cast<int32_t>(offsetof(FileIOManagerBase_t4139865030_StaticFields, ___U3CU3Ef__mgU24cache2_16)); }
	inline Action_4_t3159657519 * get_U3CU3Ef__mgU24cache2_16() const { return ___U3CU3Ef__mgU24cache2_16; }
	inline Action_4_t3159657519 ** get_address_of_U3CU3Ef__mgU24cache2_16() { return &___U3CU3Ef__mgU24cache2_16; }
	inline void set_U3CU3Ef__mgU24cache2_16(Action_4_t3159657519 * value)
	{
		___U3CU3Ef__mgU24cache2_16 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache2_16), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache3_17() { return static_cast<int32_t>(offsetof(FileIOManagerBase_t4139865030_StaticFields, ___U3CU3Ef__mgU24cache3_17)); }
	inline Action_4_t3159657519 * get_U3CU3Ef__mgU24cache3_17() const { return ___U3CU3Ef__mgU24cache3_17; }
	inline Action_4_t3159657519 ** get_address_of_U3CU3Ef__mgU24cache3_17() { return &___U3CU3Ef__mgU24cache3_17; }
	inline void set_U3CU3Ef__mgU24cache3_17(Action_4_t3159657519 * value)
	{
		___U3CU3Ef__mgU24cache3_17 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache3_17), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FILEIOMANAGERBASE_T4139865030_H
#ifndef CUSTOMLOADMANAGER_T3941822120_H
#define CUSTOMLOADMANAGER_T3941822120_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.CustomLoadManager
struct  CustomLoadManager_t3941822120  : public MonoBehaviour_t1158329972
{
public:
	// Utage.CustomLoadManager/FindAsset Utage.CustomLoadManager::<OnFindAsset>k__BackingField
	FindAsset_t4163870574 * ___U3COnFindAssetU3Ek__BackingField_2;

public:
	inline static int32_t get_offset_of_U3COnFindAssetU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(CustomLoadManager_t3941822120, ___U3COnFindAssetU3Ek__BackingField_2)); }
	inline FindAsset_t4163870574 * get_U3COnFindAssetU3Ek__BackingField_2() const { return ___U3COnFindAssetU3Ek__BackingField_2; }
	inline FindAsset_t4163870574 ** get_address_of_U3COnFindAssetU3Ek__BackingField_2() { return &___U3COnFindAssetU3Ek__BackingField_2; }
	inline void set_U3COnFindAssetU3Ek__BackingField_2(FindAsset_t4163870574 * value)
	{
		___U3COnFindAssetU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3COnFindAssetU3Ek__BackingField_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CUSTOMLOADMANAGER_T3941822120_H
#ifndef ASSETFILEREFERENCE_T4090268667_H
#define ASSETFILEREFERENCE_T4090268667_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.AssetFileReference
struct  AssetFileReference_t4090268667  : public MonoBehaviour_t1158329972
{
public:
	// Utage.AssetFile Utage.AssetFileReference::file
	RuntimeObject* ___file_2;

public:
	inline static int32_t get_offset_of_file_2() { return static_cast<int32_t>(offsetof(AssetFileReference_t4090268667, ___file_2)); }
	inline RuntimeObject* get_file_2() const { return ___file_2; }
	inline RuntimeObject** get_address_of_file_2() { return &___file_2; }
	inline void set_file_2(RuntimeObject* value)
	{
		___file_2 = value;
		Il2CppCodeGenWriteBarrier((&___file_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ASSETFILEREFERENCE_T4090268667_H
#ifndef ASSETBUNDLEINFOMANAGER_T223439115_H
#define ASSETBUNDLEINFOMANAGER_T223439115_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.AssetBundleInfoManager
struct  AssetBundleInfoManager_t223439115  : public MonoBehaviour_t1158329972
{
public:
	// System.Int32 Utage.AssetBundleInfoManager::retryCount
	int32_t ___retryCount_2;
	// System.Single Utage.AssetBundleInfoManager::timeOut
	float ___timeOut_3;
	// System.Boolean Utage.AssetBundleInfoManager::useCacheManifest
	bool ___useCacheManifest_4;
	// System.String Utage.AssetBundleInfoManager::cacheDirectoryName
	String_t* ___cacheDirectoryName_5;
	// Utage.AssetFileManager Utage.AssetBundleInfoManager::assetFileManager
	AssetFileManager_t1095395563 * ___assetFileManager_6;
	// System.Collections.Generic.Dictionary`2<System.String,Utage.AssetBundleInfo> Utage.AssetBundleInfoManager::dictionary
	Dictionary_2_t3164025568 * ___dictionary_7;

public:
	inline static int32_t get_offset_of_retryCount_2() { return static_cast<int32_t>(offsetof(AssetBundleInfoManager_t223439115, ___retryCount_2)); }
	inline int32_t get_retryCount_2() const { return ___retryCount_2; }
	inline int32_t* get_address_of_retryCount_2() { return &___retryCount_2; }
	inline void set_retryCount_2(int32_t value)
	{
		___retryCount_2 = value;
	}

	inline static int32_t get_offset_of_timeOut_3() { return static_cast<int32_t>(offsetof(AssetBundleInfoManager_t223439115, ___timeOut_3)); }
	inline float get_timeOut_3() const { return ___timeOut_3; }
	inline float* get_address_of_timeOut_3() { return &___timeOut_3; }
	inline void set_timeOut_3(float value)
	{
		___timeOut_3 = value;
	}

	inline static int32_t get_offset_of_useCacheManifest_4() { return static_cast<int32_t>(offsetof(AssetBundleInfoManager_t223439115, ___useCacheManifest_4)); }
	inline bool get_useCacheManifest_4() const { return ___useCacheManifest_4; }
	inline bool* get_address_of_useCacheManifest_4() { return &___useCacheManifest_4; }
	inline void set_useCacheManifest_4(bool value)
	{
		___useCacheManifest_4 = value;
	}

	inline static int32_t get_offset_of_cacheDirectoryName_5() { return static_cast<int32_t>(offsetof(AssetBundleInfoManager_t223439115, ___cacheDirectoryName_5)); }
	inline String_t* get_cacheDirectoryName_5() const { return ___cacheDirectoryName_5; }
	inline String_t** get_address_of_cacheDirectoryName_5() { return &___cacheDirectoryName_5; }
	inline void set_cacheDirectoryName_5(String_t* value)
	{
		___cacheDirectoryName_5 = value;
		Il2CppCodeGenWriteBarrier((&___cacheDirectoryName_5), value);
	}

	inline static int32_t get_offset_of_assetFileManager_6() { return static_cast<int32_t>(offsetof(AssetBundleInfoManager_t223439115, ___assetFileManager_6)); }
	inline AssetFileManager_t1095395563 * get_assetFileManager_6() const { return ___assetFileManager_6; }
	inline AssetFileManager_t1095395563 ** get_address_of_assetFileManager_6() { return &___assetFileManager_6; }
	inline void set_assetFileManager_6(AssetFileManager_t1095395563 * value)
	{
		___assetFileManager_6 = value;
		Il2CppCodeGenWriteBarrier((&___assetFileManager_6), value);
	}

	inline static int32_t get_offset_of_dictionary_7() { return static_cast<int32_t>(offsetof(AssetBundleInfoManager_t223439115, ___dictionary_7)); }
	inline Dictionary_2_t3164025568 * get_dictionary_7() const { return ___dictionary_7; }
	inline Dictionary_2_t3164025568 ** get_address_of_dictionary_7() { return &___dictionary_7; }
	inline void set_dictionary_7(Dictionary_2_t3164025568 * value)
	{
		___dictionary_7 = value;
		Il2CppCodeGenWriteBarrier((&___dictionary_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ASSETBUNDLEINFOMANAGER_T223439115_H
#ifndef ASSETFILEMANAGER_T1095395563_H
#define ASSETFILEMANAGER_T1095395563_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.AssetFileManager
struct  AssetFileManager_t1095395563  : public MonoBehaviour_t1158329972
{
public:
	// Utage.FileIOManager Utage.AssetFileManager::fileIOManager
	FileIOManager_t855502573 * ___fileIOManager_2;
	// System.Boolean Utage.AssetFileManager::enableResourcesLoadAsync
	bool ___enableResourcesLoadAsync_3;
	// System.Single Utage.AssetFileManager::timeOutDownload
	float ___timeOutDownload_4;
	// System.Int32 Utage.AssetFileManager::autoRetryCountOnDonwloadError
	int32_t ___autoRetryCountOnDonwloadError_5;
	// System.Int32 Utage.AssetFileManager::loadFileMax
	int32_t ___loadFileMax_6;
	// Utage.MinMaxInt Utage.AssetFileManager::rangeOfFilesOnMemory
	MinMaxInt_t3259591635 * ___rangeOfFilesOnMemory_7;
	// Utage.AssetFileManager/UnloadType Utage.AssetFileManager::unloadType
	int32_t ___unloadType_8;
	// System.Boolean Utage.AssetFileManager::isOutPutDebugLog
	bool ___isOutPutDebugLog_9;
	// System.Boolean Utage.AssetFileManager::isDebugCacheFileName
	bool ___isDebugCacheFileName_10;
	// System.Boolean Utage.AssetFileManager::isDebugBootDeleteChacheTextAndBinary
	bool ___isDebugBootDeleteChacheTextAndBinary_11;
	// System.Boolean Utage.AssetFileManager::isDebugBootDeleteChacheAll
	bool ___isDebugBootDeleteChacheAll_12;
	// Utage.AssetFileManagerSettings Utage.AssetFileManager::settings
	AssetFileManagerSettings_t1473658718 * ___settings_13;
	// Utage.AssetBundleInfoManager Utage.AssetFileManager::assetBundleInfoManager
	AssetBundleInfoManager_t223439115 * ___assetBundleInfoManager_14;
	// Utage.AssetFileDummyOnLoadError Utage.AssetFileManager::dummyFiles
	AssetFileDummyOnLoadError_t419678579 * ___dummyFiles_15;
	// System.Collections.Generic.List`1<Utage.AssetFileBase> Utage.AssetFileManager::loadingFileList
	List_1_t3470481829 * ___loadingFileList_16;
	// System.Collections.Generic.List`1<Utage.AssetFileBase> Utage.AssetFileManager::loadWaitFileList
	List_1_t3470481829 * ___loadWaitFileList_17;
	// System.Collections.Generic.List`1<Utage.AssetFileBase> Utage.AssetFileManager::usingFileList
	List_1_t3470481829 * ___usingFileList_18;
	// System.Collections.Generic.Dictionary`2<System.String,Utage.AssetFileBase> Utage.AssetFileManager::fileTbl
	Dictionary_2_t1721172663 * ___fileTbl_19;
	// Utage.CustomLoadManager Utage.AssetFileManager::customLoadManager
	CustomLoadManager_t3941822120 * ___customLoadManager_20;
	// Utage.StaticAssetManager Utage.AssetFileManager::staticAssetManager
	StaticAssetManager_t393950057 * ___staticAssetManager_21;
	// System.Action`1<Utage.AssetFile> Utage.AssetFileManager::callbackError
	Action_1_t3184812638 * ___callbackError_22;
	// System.Boolean Utage.AssetFileManager::isWaitingRetry
	bool ___isWaitingRetry_23;
	// System.Boolean Utage.AssetFileManager::unloadingUnusedAssets
	bool ___unloadingUnusedAssets_24;

public:
	inline static int32_t get_offset_of_fileIOManager_2() { return static_cast<int32_t>(offsetof(AssetFileManager_t1095395563, ___fileIOManager_2)); }
	inline FileIOManager_t855502573 * get_fileIOManager_2() const { return ___fileIOManager_2; }
	inline FileIOManager_t855502573 ** get_address_of_fileIOManager_2() { return &___fileIOManager_2; }
	inline void set_fileIOManager_2(FileIOManager_t855502573 * value)
	{
		___fileIOManager_2 = value;
		Il2CppCodeGenWriteBarrier((&___fileIOManager_2), value);
	}

	inline static int32_t get_offset_of_enableResourcesLoadAsync_3() { return static_cast<int32_t>(offsetof(AssetFileManager_t1095395563, ___enableResourcesLoadAsync_3)); }
	inline bool get_enableResourcesLoadAsync_3() const { return ___enableResourcesLoadAsync_3; }
	inline bool* get_address_of_enableResourcesLoadAsync_3() { return &___enableResourcesLoadAsync_3; }
	inline void set_enableResourcesLoadAsync_3(bool value)
	{
		___enableResourcesLoadAsync_3 = value;
	}

	inline static int32_t get_offset_of_timeOutDownload_4() { return static_cast<int32_t>(offsetof(AssetFileManager_t1095395563, ___timeOutDownload_4)); }
	inline float get_timeOutDownload_4() const { return ___timeOutDownload_4; }
	inline float* get_address_of_timeOutDownload_4() { return &___timeOutDownload_4; }
	inline void set_timeOutDownload_4(float value)
	{
		___timeOutDownload_4 = value;
	}

	inline static int32_t get_offset_of_autoRetryCountOnDonwloadError_5() { return static_cast<int32_t>(offsetof(AssetFileManager_t1095395563, ___autoRetryCountOnDonwloadError_5)); }
	inline int32_t get_autoRetryCountOnDonwloadError_5() const { return ___autoRetryCountOnDonwloadError_5; }
	inline int32_t* get_address_of_autoRetryCountOnDonwloadError_5() { return &___autoRetryCountOnDonwloadError_5; }
	inline void set_autoRetryCountOnDonwloadError_5(int32_t value)
	{
		___autoRetryCountOnDonwloadError_5 = value;
	}

	inline static int32_t get_offset_of_loadFileMax_6() { return static_cast<int32_t>(offsetof(AssetFileManager_t1095395563, ___loadFileMax_6)); }
	inline int32_t get_loadFileMax_6() const { return ___loadFileMax_6; }
	inline int32_t* get_address_of_loadFileMax_6() { return &___loadFileMax_6; }
	inline void set_loadFileMax_6(int32_t value)
	{
		___loadFileMax_6 = value;
	}

	inline static int32_t get_offset_of_rangeOfFilesOnMemory_7() { return static_cast<int32_t>(offsetof(AssetFileManager_t1095395563, ___rangeOfFilesOnMemory_7)); }
	inline MinMaxInt_t3259591635 * get_rangeOfFilesOnMemory_7() const { return ___rangeOfFilesOnMemory_7; }
	inline MinMaxInt_t3259591635 ** get_address_of_rangeOfFilesOnMemory_7() { return &___rangeOfFilesOnMemory_7; }
	inline void set_rangeOfFilesOnMemory_7(MinMaxInt_t3259591635 * value)
	{
		___rangeOfFilesOnMemory_7 = value;
		Il2CppCodeGenWriteBarrier((&___rangeOfFilesOnMemory_7), value);
	}

	inline static int32_t get_offset_of_unloadType_8() { return static_cast<int32_t>(offsetof(AssetFileManager_t1095395563, ___unloadType_8)); }
	inline int32_t get_unloadType_8() const { return ___unloadType_8; }
	inline int32_t* get_address_of_unloadType_8() { return &___unloadType_8; }
	inline void set_unloadType_8(int32_t value)
	{
		___unloadType_8 = value;
	}

	inline static int32_t get_offset_of_isOutPutDebugLog_9() { return static_cast<int32_t>(offsetof(AssetFileManager_t1095395563, ___isOutPutDebugLog_9)); }
	inline bool get_isOutPutDebugLog_9() const { return ___isOutPutDebugLog_9; }
	inline bool* get_address_of_isOutPutDebugLog_9() { return &___isOutPutDebugLog_9; }
	inline void set_isOutPutDebugLog_9(bool value)
	{
		___isOutPutDebugLog_9 = value;
	}

	inline static int32_t get_offset_of_isDebugCacheFileName_10() { return static_cast<int32_t>(offsetof(AssetFileManager_t1095395563, ___isDebugCacheFileName_10)); }
	inline bool get_isDebugCacheFileName_10() const { return ___isDebugCacheFileName_10; }
	inline bool* get_address_of_isDebugCacheFileName_10() { return &___isDebugCacheFileName_10; }
	inline void set_isDebugCacheFileName_10(bool value)
	{
		___isDebugCacheFileName_10 = value;
	}

	inline static int32_t get_offset_of_isDebugBootDeleteChacheTextAndBinary_11() { return static_cast<int32_t>(offsetof(AssetFileManager_t1095395563, ___isDebugBootDeleteChacheTextAndBinary_11)); }
	inline bool get_isDebugBootDeleteChacheTextAndBinary_11() const { return ___isDebugBootDeleteChacheTextAndBinary_11; }
	inline bool* get_address_of_isDebugBootDeleteChacheTextAndBinary_11() { return &___isDebugBootDeleteChacheTextAndBinary_11; }
	inline void set_isDebugBootDeleteChacheTextAndBinary_11(bool value)
	{
		___isDebugBootDeleteChacheTextAndBinary_11 = value;
	}

	inline static int32_t get_offset_of_isDebugBootDeleteChacheAll_12() { return static_cast<int32_t>(offsetof(AssetFileManager_t1095395563, ___isDebugBootDeleteChacheAll_12)); }
	inline bool get_isDebugBootDeleteChacheAll_12() const { return ___isDebugBootDeleteChacheAll_12; }
	inline bool* get_address_of_isDebugBootDeleteChacheAll_12() { return &___isDebugBootDeleteChacheAll_12; }
	inline void set_isDebugBootDeleteChacheAll_12(bool value)
	{
		___isDebugBootDeleteChacheAll_12 = value;
	}

	inline static int32_t get_offset_of_settings_13() { return static_cast<int32_t>(offsetof(AssetFileManager_t1095395563, ___settings_13)); }
	inline AssetFileManagerSettings_t1473658718 * get_settings_13() const { return ___settings_13; }
	inline AssetFileManagerSettings_t1473658718 ** get_address_of_settings_13() { return &___settings_13; }
	inline void set_settings_13(AssetFileManagerSettings_t1473658718 * value)
	{
		___settings_13 = value;
		Il2CppCodeGenWriteBarrier((&___settings_13), value);
	}

	inline static int32_t get_offset_of_assetBundleInfoManager_14() { return static_cast<int32_t>(offsetof(AssetFileManager_t1095395563, ___assetBundleInfoManager_14)); }
	inline AssetBundleInfoManager_t223439115 * get_assetBundleInfoManager_14() const { return ___assetBundleInfoManager_14; }
	inline AssetBundleInfoManager_t223439115 ** get_address_of_assetBundleInfoManager_14() { return &___assetBundleInfoManager_14; }
	inline void set_assetBundleInfoManager_14(AssetBundleInfoManager_t223439115 * value)
	{
		___assetBundleInfoManager_14 = value;
		Il2CppCodeGenWriteBarrier((&___assetBundleInfoManager_14), value);
	}

	inline static int32_t get_offset_of_dummyFiles_15() { return static_cast<int32_t>(offsetof(AssetFileManager_t1095395563, ___dummyFiles_15)); }
	inline AssetFileDummyOnLoadError_t419678579 * get_dummyFiles_15() const { return ___dummyFiles_15; }
	inline AssetFileDummyOnLoadError_t419678579 ** get_address_of_dummyFiles_15() { return &___dummyFiles_15; }
	inline void set_dummyFiles_15(AssetFileDummyOnLoadError_t419678579 * value)
	{
		___dummyFiles_15 = value;
		Il2CppCodeGenWriteBarrier((&___dummyFiles_15), value);
	}

	inline static int32_t get_offset_of_loadingFileList_16() { return static_cast<int32_t>(offsetof(AssetFileManager_t1095395563, ___loadingFileList_16)); }
	inline List_1_t3470481829 * get_loadingFileList_16() const { return ___loadingFileList_16; }
	inline List_1_t3470481829 ** get_address_of_loadingFileList_16() { return &___loadingFileList_16; }
	inline void set_loadingFileList_16(List_1_t3470481829 * value)
	{
		___loadingFileList_16 = value;
		Il2CppCodeGenWriteBarrier((&___loadingFileList_16), value);
	}

	inline static int32_t get_offset_of_loadWaitFileList_17() { return static_cast<int32_t>(offsetof(AssetFileManager_t1095395563, ___loadWaitFileList_17)); }
	inline List_1_t3470481829 * get_loadWaitFileList_17() const { return ___loadWaitFileList_17; }
	inline List_1_t3470481829 ** get_address_of_loadWaitFileList_17() { return &___loadWaitFileList_17; }
	inline void set_loadWaitFileList_17(List_1_t3470481829 * value)
	{
		___loadWaitFileList_17 = value;
		Il2CppCodeGenWriteBarrier((&___loadWaitFileList_17), value);
	}

	inline static int32_t get_offset_of_usingFileList_18() { return static_cast<int32_t>(offsetof(AssetFileManager_t1095395563, ___usingFileList_18)); }
	inline List_1_t3470481829 * get_usingFileList_18() const { return ___usingFileList_18; }
	inline List_1_t3470481829 ** get_address_of_usingFileList_18() { return &___usingFileList_18; }
	inline void set_usingFileList_18(List_1_t3470481829 * value)
	{
		___usingFileList_18 = value;
		Il2CppCodeGenWriteBarrier((&___usingFileList_18), value);
	}

	inline static int32_t get_offset_of_fileTbl_19() { return static_cast<int32_t>(offsetof(AssetFileManager_t1095395563, ___fileTbl_19)); }
	inline Dictionary_2_t1721172663 * get_fileTbl_19() const { return ___fileTbl_19; }
	inline Dictionary_2_t1721172663 ** get_address_of_fileTbl_19() { return &___fileTbl_19; }
	inline void set_fileTbl_19(Dictionary_2_t1721172663 * value)
	{
		___fileTbl_19 = value;
		Il2CppCodeGenWriteBarrier((&___fileTbl_19), value);
	}

	inline static int32_t get_offset_of_customLoadManager_20() { return static_cast<int32_t>(offsetof(AssetFileManager_t1095395563, ___customLoadManager_20)); }
	inline CustomLoadManager_t3941822120 * get_customLoadManager_20() const { return ___customLoadManager_20; }
	inline CustomLoadManager_t3941822120 ** get_address_of_customLoadManager_20() { return &___customLoadManager_20; }
	inline void set_customLoadManager_20(CustomLoadManager_t3941822120 * value)
	{
		___customLoadManager_20 = value;
		Il2CppCodeGenWriteBarrier((&___customLoadManager_20), value);
	}

	inline static int32_t get_offset_of_staticAssetManager_21() { return static_cast<int32_t>(offsetof(AssetFileManager_t1095395563, ___staticAssetManager_21)); }
	inline StaticAssetManager_t393950057 * get_staticAssetManager_21() const { return ___staticAssetManager_21; }
	inline StaticAssetManager_t393950057 ** get_address_of_staticAssetManager_21() { return &___staticAssetManager_21; }
	inline void set_staticAssetManager_21(StaticAssetManager_t393950057 * value)
	{
		___staticAssetManager_21 = value;
		Il2CppCodeGenWriteBarrier((&___staticAssetManager_21), value);
	}

	inline static int32_t get_offset_of_callbackError_22() { return static_cast<int32_t>(offsetof(AssetFileManager_t1095395563, ___callbackError_22)); }
	inline Action_1_t3184812638 * get_callbackError_22() const { return ___callbackError_22; }
	inline Action_1_t3184812638 ** get_address_of_callbackError_22() { return &___callbackError_22; }
	inline void set_callbackError_22(Action_1_t3184812638 * value)
	{
		___callbackError_22 = value;
		Il2CppCodeGenWriteBarrier((&___callbackError_22), value);
	}

	inline static int32_t get_offset_of_isWaitingRetry_23() { return static_cast<int32_t>(offsetof(AssetFileManager_t1095395563, ___isWaitingRetry_23)); }
	inline bool get_isWaitingRetry_23() const { return ___isWaitingRetry_23; }
	inline bool* get_address_of_isWaitingRetry_23() { return &___isWaitingRetry_23; }
	inline void set_isWaitingRetry_23(bool value)
	{
		___isWaitingRetry_23 = value;
	}

	inline static int32_t get_offset_of_unloadingUnusedAssets_24() { return static_cast<int32_t>(offsetof(AssetFileManager_t1095395563, ___unloadingUnusedAssets_24)); }
	inline bool get_unloadingUnusedAssets_24() const { return ___unloadingUnusedAssets_24; }
	inline bool* get_address_of_unloadingUnusedAssets_24() { return &___unloadingUnusedAssets_24; }
	inline void set_unloadingUnusedAssets_24(bool value)
	{
		___unloadingUnusedAssets_24 = value;
	}
};

struct AssetFileManager_t1095395563_StaticFields
{
public:
	// System.Boolean Utage.AssetFileManager::isEditorErrorCheck
	bool ___isEditorErrorCheck_25;
	// Utage.AssetFileManager Utage.AssetFileManager::instance
	AssetFileManager_t1095395563 * ___instance_26;

public:
	inline static int32_t get_offset_of_isEditorErrorCheck_25() { return static_cast<int32_t>(offsetof(AssetFileManager_t1095395563_StaticFields, ___isEditorErrorCheck_25)); }
	inline bool get_isEditorErrorCheck_25() const { return ___isEditorErrorCheck_25; }
	inline bool* get_address_of_isEditorErrorCheck_25() { return &___isEditorErrorCheck_25; }
	inline void set_isEditorErrorCheck_25(bool value)
	{
		___isEditorErrorCheck_25 = value;
	}

	inline static int32_t get_offset_of_instance_26() { return static_cast<int32_t>(offsetof(AssetFileManager_t1095395563_StaticFields, ___instance_26)); }
	inline AssetFileManager_t1095395563 * get_instance_26() const { return ___instance_26; }
	inline AssetFileManager_t1095395563 ** get_address_of_instance_26() { return &___instance_26; }
	inline void set_instance_26(AssetFileManager_t1095395563 * value)
	{
		___instance_26 = value;
		Il2CppCodeGenWriteBarrier((&___instance_26), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ASSETFILEMANAGER_T1095395563_H
#ifndef APPLICATIONEVENT_T30752656_H
#define APPLICATIONEVENT_T30752656_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.ApplicationEvent
struct  ApplicationEvent_t30752656  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.Events.UnityEvent Utage.ApplicationEvent::OnScreenSizeChanged
	UnityEvent_t408735097 * ___OnScreenSizeChanged_3;
	// System.Int32 Utage.ApplicationEvent::screenWidth
	int32_t ___screenWidth_4;
	// System.Int32 Utage.ApplicationEvent::screenHeight
	int32_t ___screenHeight_5;

public:
	inline static int32_t get_offset_of_OnScreenSizeChanged_3() { return static_cast<int32_t>(offsetof(ApplicationEvent_t30752656, ___OnScreenSizeChanged_3)); }
	inline UnityEvent_t408735097 * get_OnScreenSizeChanged_3() const { return ___OnScreenSizeChanged_3; }
	inline UnityEvent_t408735097 ** get_address_of_OnScreenSizeChanged_3() { return &___OnScreenSizeChanged_3; }
	inline void set_OnScreenSizeChanged_3(UnityEvent_t408735097 * value)
	{
		___OnScreenSizeChanged_3 = value;
		Il2CppCodeGenWriteBarrier((&___OnScreenSizeChanged_3), value);
	}

	inline static int32_t get_offset_of_screenWidth_4() { return static_cast<int32_t>(offsetof(ApplicationEvent_t30752656, ___screenWidth_4)); }
	inline int32_t get_screenWidth_4() const { return ___screenWidth_4; }
	inline int32_t* get_address_of_screenWidth_4() { return &___screenWidth_4; }
	inline void set_screenWidth_4(int32_t value)
	{
		___screenWidth_4 = value;
	}

	inline static int32_t get_offset_of_screenHeight_5() { return static_cast<int32_t>(offsetof(ApplicationEvent_t30752656, ___screenHeight_5)); }
	inline int32_t get_screenHeight_5() const { return ___screenHeight_5; }
	inline int32_t* get_address_of_screenHeight_5() { return &___screenHeight_5; }
	inline void set_screenHeight_5(int32_t value)
	{
		___screenHeight_5 = value;
	}
};

struct ApplicationEvent_t30752656_StaticFields
{
public:
	// Utage.ApplicationEvent Utage.ApplicationEvent::instance
	ApplicationEvent_t30752656 * ___instance_2;

public:
	inline static int32_t get_offset_of_instance_2() { return static_cast<int32_t>(offsetof(ApplicationEvent_t30752656_StaticFields, ___instance_2)); }
	inline ApplicationEvent_t30752656 * get_instance_2() const { return ___instance_2; }
	inline ApplicationEvent_t30752656 ** get_address_of_instance_2() { return &___instance_2; }
	inline void set_instance_2(ApplicationEvent_t30752656 * value)
	{
		___instance_2 = value;
		Il2CppCodeGenWriteBarrier((&___instance_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // APPLICATIONEVENT_T30752656_H
#ifndef PARTICLESCALER_T814660744_H
#define PARTICLESCALER_T814660744_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.ParticleScaler
struct  ParticleScaler_t814660744  : public MonoBehaviour_t1158329972
{
public:
	// System.Boolean Utage.ParticleScaler::useLocalScale
	bool ___useLocalScale_2;
	// System.Single Utage.ParticleScaler::scale
	float ___scale_3;
	// System.Boolean Utage.ParticleScaler::changeRenderMode
	bool ___changeRenderMode_4;
	// System.Boolean Utage.ParticleScaler::changeGravity
	bool ___changeGravity_5;
	// System.Boolean Utage.ParticleScaler::<HasChanged>k__BackingField
	bool ___U3CHasChangedU3Ek__BackingField_6;
	// System.Boolean Utage.ParticleScaler::<IsInit>k__BackingField
	bool ___U3CIsInitU3Ek__BackingField_7;
	// System.Collections.Generic.Dictionary`2<UnityEngine.ParticleSystem,System.Single> Utage.ParticleScaler::defaultGravities
	Dictionary_2_t3839890066 * ___defaultGravities_8;

public:
	inline static int32_t get_offset_of_useLocalScale_2() { return static_cast<int32_t>(offsetof(ParticleScaler_t814660744, ___useLocalScale_2)); }
	inline bool get_useLocalScale_2() const { return ___useLocalScale_2; }
	inline bool* get_address_of_useLocalScale_2() { return &___useLocalScale_2; }
	inline void set_useLocalScale_2(bool value)
	{
		___useLocalScale_2 = value;
	}

	inline static int32_t get_offset_of_scale_3() { return static_cast<int32_t>(offsetof(ParticleScaler_t814660744, ___scale_3)); }
	inline float get_scale_3() const { return ___scale_3; }
	inline float* get_address_of_scale_3() { return &___scale_3; }
	inline void set_scale_3(float value)
	{
		___scale_3 = value;
	}

	inline static int32_t get_offset_of_changeRenderMode_4() { return static_cast<int32_t>(offsetof(ParticleScaler_t814660744, ___changeRenderMode_4)); }
	inline bool get_changeRenderMode_4() const { return ___changeRenderMode_4; }
	inline bool* get_address_of_changeRenderMode_4() { return &___changeRenderMode_4; }
	inline void set_changeRenderMode_4(bool value)
	{
		___changeRenderMode_4 = value;
	}

	inline static int32_t get_offset_of_changeGravity_5() { return static_cast<int32_t>(offsetof(ParticleScaler_t814660744, ___changeGravity_5)); }
	inline bool get_changeGravity_5() const { return ___changeGravity_5; }
	inline bool* get_address_of_changeGravity_5() { return &___changeGravity_5; }
	inline void set_changeGravity_5(bool value)
	{
		___changeGravity_5 = value;
	}

	inline static int32_t get_offset_of_U3CHasChangedU3Ek__BackingField_6() { return static_cast<int32_t>(offsetof(ParticleScaler_t814660744, ___U3CHasChangedU3Ek__BackingField_6)); }
	inline bool get_U3CHasChangedU3Ek__BackingField_6() const { return ___U3CHasChangedU3Ek__BackingField_6; }
	inline bool* get_address_of_U3CHasChangedU3Ek__BackingField_6() { return &___U3CHasChangedU3Ek__BackingField_6; }
	inline void set_U3CHasChangedU3Ek__BackingField_6(bool value)
	{
		___U3CHasChangedU3Ek__BackingField_6 = value;
	}

	inline static int32_t get_offset_of_U3CIsInitU3Ek__BackingField_7() { return static_cast<int32_t>(offsetof(ParticleScaler_t814660744, ___U3CIsInitU3Ek__BackingField_7)); }
	inline bool get_U3CIsInitU3Ek__BackingField_7() const { return ___U3CIsInitU3Ek__BackingField_7; }
	inline bool* get_address_of_U3CIsInitU3Ek__BackingField_7() { return &___U3CIsInitU3Ek__BackingField_7; }
	inline void set_U3CIsInitU3Ek__BackingField_7(bool value)
	{
		___U3CIsInitU3Ek__BackingField_7 = value;
	}

	inline static int32_t get_offset_of_defaultGravities_8() { return static_cast<int32_t>(offsetof(ParticleScaler_t814660744, ___defaultGravities_8)); }
	inline Dictionary_2_t3839890066 * get_defaultGravities_8() const { return ___defaultGravities_8; }
	inline Dictionary_2_t3839890066 ** get_address_of_defaultGravities_8() { return &___defaultGravities_8; }
	inline void set_defaultGravities_8(Dictionary_2_t3839890066 * value)
	{
		___defaultGravities_8 = value;
		Il2CppCodeGenWriteBarrier((&___defaultGravities_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PARTICLESCALER_T814660744_H
#ifndef MOSAICRENDERER_T4160220697_H
#define MOSAICRENDERER_T4160220697_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.MosaicRenderer
struct  MosaicRenderer_t4160220697  : public MonoBehaviour_t1158329972
{
public:
	// System.Single Utage.MosaicRenderer::mosaicSize
	float ___mosaicSize_2;
	// UnityEngine.Rendering.CompareFunction Utage.MosaicRenderer::zTest
	int32_t ___zTest_3;
	// System.String Utage.MosaicRenderer::sortingLayerName
	String_t* ___sortingLayerName_4;
	// System.Int32 Utage.MosaicRenderer::sortingOrder
	int32_t ___sortingOrder_5;
	// System.Boolean Utage.MosaicRenderer::autoScale
	bool ___autoScale_6;
	// System.Int32 Utage.MosaicRenderer::gameScreenWidth
	int32_t ___gameScreenWidth_7;
	// System.Int32 Utage.MosaicRenderer::gameScreenHeight
	int32_t ___gameScreenHeight_8;

public:
	inline static int32_t get_offset_of_mosaicSize_2() { return static_cast<int32_t>(offsetof(MosaicRenderer_t4160220697, ___mosaicSize_2)); }
	inline float get_mosaicSize_2() const { return ___mosaicSize_2; }
	inline float* get_address_of_mosaicSize_2() { return &___mosaicSize_2; }
	inline void set_mosaicSize_2(float value)
	{
		___mosaicSize_2 = value;
	}

	inline static int32_t get_offset_of_zTest_3() { return static_cast<int32_t>(offsetof(MosaicRenderer_t4160220697, ___zTest_3)); }
	inline int32_t get_zTest_3() const { return ___zTest_3; }
	inline int32_t* get_address_of_zTest_3() { return &___zTest_3; }
	inline void set_zTest_3(int32_t value)
	{
		___zTest_3 = value;
	}

	inline static int32_t get_offset_of_sortingLayerName_4() { return static_cast<int32_t>(offsetof(MosaicRenderer_t4160220697, ___sortingLayerName_4)); }
	inline String_t* get_sortingLayerName_4() const { return ___sortingLayerName_4; }
	inline String_t** get_address_of_sortingLayerName_4() { return &___sortingLayerName_4; }
	inline void set_sortingLayerName_4(String_t* value)
	{
		___sortingLayerName_4 = value;
		Il2CppCodeGenWriteBarrier((&___sortingLayerName_4), value);
	}

	inline static int32_t get_offset_of_sortingOrder_5() { return static_cast<int32_t>(offsetof(MosaicRenderer_t4160220697, ___sortingOrder_5)); }
	inline int32_t get_sortingOrder_5() const { return ___sortingOrder_5; }
	inline int32_t* get_address_of_sortingOrder_5() { return &___sortingOrder_5; }
	inline void set_sortingOrder_5(int32_t value)
	{
		___sortingOrder_5 = value;
	}

	inline static int32_t get_offset_of_autoScale_6() { return static_cast<int32_t>(offsetof(MosaicRenderer_t4160220697, ___autoScale_6)); }
	inline bool get_autoScale_6() const { return ___autoScale_6; }
	inline bool* get_address_of_autoScale_6() { return &___autoScale_6; }
	inline void set_autoScale_6(bool value)
	{
		___autoScale_6 = value;
	}

	inline static int32_t get_offset_of_gameScreenWidth_7() { return static_cast<int32_t>(offsetof(MosaicRenderer_t4160220697, ___gameScreenWidth_7)); }
	inline int32_t get_gameScreenWidth_7() const { return ___gameScreenWidth_7; }
	inline int32_t* get_address_of_gameScreenWidth_7() { return &___gameScreenWidth_7; }
	inline void set_gameScreenWidth_7(int32_t value)
	{
		___gameScreenWidth_7 = value;
	}

	inline static int32_t get_offset_of_gameScreenHeight_8() { return static_cast<int32_t>(offsetof(MosaicRenderer_t4160220697, ___gameScreenHeight_8)); }
	inline int32_t get_gameScreenHeight_8() const { return ___gameScreenHeight_8; }
	inline int32_t* get_address_of_gameScreenHeight_8() { return &___gameScreenHeight_8; }
	inline void set_gameScreenHeight_8(int32_t value)
	{
		___gameScreenHeight_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MOSAICRENDERER_T4160220697_H
#ifndef STATICASSETMANAGER_T393950057_H
#define STATICASSETMANAGER_T393950057_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.StaticAssetManager
struct  StaticAssetManager_t393950057  : public MonoBehaviour_t1158329972
{
public:
	// System.Collections.Generic.List`1<Utage.StaticAsset> Utage.StaticAssetManager::assets
	List_1_t481879052 * ___assets_2;

public:
	inline static int32_t get_offset_of_assets_2() { return static_cast<int32_t>(offsetof(StaticAssetManager_t393950057, ___assets_2)); }
	inline List_1_t481879052 * get_assets_2() const { return ___assets_2; }
	inline List_1_t481879052 ** get_address_of_assets_2() { return &___assets_2; }
	inline void set_assets_2(List_1_t481879052 * value)
	{
		___assets_2 = value;
		Il2CppCodeGenWriteBarrier((&___assets_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STATICASSETMANAGER_T393950057_H
#ifndef LINKTRANSFORM_T2861363074_H
#define LINKTRANSFORM_T2861363074_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.LinkTransform
struct  LinkTransform_t2861363074  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.Transform Utage.LinkTransform::target
	Transform_t3275118058 * ___target_2;
	// System.Boolean Utage.LinkTransform::isInit
	bool ___isInit_3;
	// UnityEngine.Vector3 Utage.LinkTransform::targetPosition
	Vector3_t2243707580  ___targetPosition_4;
	// UnityEngine.Vector3 Utage.LinkTransform::targetScale
	Vector3_t2243707580  ___targetScale_5;
	// UnityEngine.Vector3 Utage.LinkTransform::targetEuler
	Vector3_t2243707580  ___targetEuler_6;
	// UnityEngine.Vector3 Utage.LinkTransform::startPosition
	Vector3_t2243707580  ___startPosition_7;
	// UnityEngine.Vector3 Utage.LinkTransform::startScale
	Vector3_t2243707580  ___startScale_8;
	// UnityEngine.Vector3 Utage.LinkTransform::startEuler
	Vector3_t2243707580  ___startEuler_9;
	// UnityEngine.Transform Utage.LinkTransform::cachedTransform
	Transform_t3275118058 * ___cachedTransform_10;

public:
	inline static int32_t get_offset_of_target_2() { return static_cast<int32_t>(offsetof(LinkTransform_t2861363074, ___target_2)); }
	inline Transform_t3275118058 * get_target_2() const { return ___target_2; }
	inline Transform_t3275118058 ** get_address_of_target_2() { return &___target_2; }
	inline void set_target_2(Transform_t3275118058 * value)
	{
		___target_2 = value;
		Il2CppCodeGenWriteBarrier((&___target_2), value);
	}

	inline static int32_t get_offset_of_isInit_3() { return static_cast<int32_t>(offsetof(LinkTransform_t2861363074, ___isInit_3)); }
	inline bool get_isInit_3() const { return ___isInit_3; }
	inline bool* get_address_of_isInit_3() { return &___isInit_3; }
	inline void set_isInit_3(bool value)
	{
		___isInit_3 = value;
	}

	inline static int32_t get_offset_of_targetPosition_4() { return static_cast<int32_t>(offsetof(LinkTransform_t2861363074, ___targetPosition_4)); }
	inline Vector3_t2243707580  get_targetPosition_4() const { return ___targetPosition_4; }
	inline Vector3_t2243707580 * get_address_of_targetPosition_4() { return &___targetPosition_4; }
	inline void set_targetPosition_4(Vector3_t2243707580  value)
	{
		___targetPosition_4 = value;
	}

	inline static int32_t get_offset_of_targetScale_5() { return static_cast<int32_t>(offsetof(LinkTransform_t2861363074, ___targetScale_5)); }
	inline Vector3_t2243707580  get_targetScale_5() const { return ___targetScale_5; }
	inline Vector3_t2243707580 * get_address_of_targetScale_5() { return &___targetScale_5; }
	inline void set_targetScale_5(Vector3_t2243707580  value)
	{
		___targetScale_5 = value;
	}

	inline static int32_t get_offset_of_targetEuler_6() { return static_cast<int32_t>(offsetof(LinkTransform_t2861363074, ___targetEuler_6)); }
	inline Vector3_t2243707580  get_targetEuler_6() const { return ___targetEuler_6; }
	inline Vector3_t2243707580 * get_address_of_targetEuler_6() { return &___targetEuler_6; }
	inline void set_targetEuler_6(Vector3_t2243707580  value)
	{
		___targetEuler_6 = value;
	}

	inline static int32_t get_offset_of_startPosition_7() { return static_cast<int32_t>(offsetof(LinkTransform_t2861363074, ___startPosition_7)); }
	inline Vector3_t2243707580  get_startPosition_7() const { return ___startPosition_7; }
	inline Vector3_t2243707580 * get_address_of_startPosition_7() { return &___startPosition_7; }
	inline void set_startPosition_7(Vector3_t2243707580  value)
	{
		___startPosition_7 = value;
	}

	inline static int32_t get_offset_of_startScale_8() { return static_cast<int32_t>(offsetof(LinkTransform_t2861363074, ___startScale_8)); }
	inline Vector3_t2243707580  get_startScale_8() const { return ___startScale_8; }
	inline Vector3_t2243707580 * get_address_of_startScale_8() { return &___startScale_8; }
	inline void set_startScale_8(Vector3_t2243707580  value)
	{
		___startScale_8 = value;
	}

	inline static int32_t get_offset_of_startEuler_9() { return static_cast<int32_t>(offsetof(LinkTransform_t2861363074, ___startEuler_9)); }
	inline Vector3_t2243707580  get_startEuler_9() const { return ___startEuler_9; }
	inline Vector3_t2243707580 * get_address_of_startEuler_9() { return &___startEuler_9; }
	inline void set_startEuler_9(Vector3_t2243707580  value)
	{
		___startEuler_9 = value;
	}

	inline static int32_t get_offset_of_cachedTransform_10() { return static_cast<int32_t>(offsetof(LinkTransform_t2861363074, ___cachedTransform_10)); }
	inline Transform_t3275118058 * get_cachedTransform_10() const { return ___cachedTransform_10; }
	inline Transform_t3275118058 ** get_address_of_cachedTransform_10() { return &___cachedTransform_10; }
	inline void set_cachedTransform_10(Transform_t3275118058 * value)
	{
		___cachedTransform_10 = value;
		Il2CppCodeGenWriteBarrier((&___cachedTransform_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LINKTRANSFORM_T2861363074_H
#ifndef CURVEANIMATION_T2521618615_H
#define CURVEANIMATION_T2521618615_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.CurveAnimation
struct  CurveAnimation_t2521618615  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.AnimationCurve Utage.CurveAnimation::curve
	AnimationCurve_t3306541151 * ___curve_2;
	// System.Single Utage.CurveAnimation::delay
	float ___delay_3;
	// System.Single Utage.CurveAnimation::duration
	float ___duration_4;
	// System.Boolean Utage.CurveAnimation::unscaledTime
	bool ___unscaledTime_5;
	// System.Single Utage.CurveAnimation::<Value>k__BackingField
	float ___U3CValueU3Ek__BackingField_6;
	// System.Boolean Utage.CurveAnimation::<IsPlaying>k__BackingField
	bool ___U3CIsPlayingU3Ek__BackingField_7;
	// Utage.CurveAnimationEvent Utage.CurveAnimation::onStart
	CurveAnimationEvent_t3593772963 * ___onStart_8;
	// Utage.CurveAnimationEvent Utage.CurveAnimation::onUpdate
	CurveAnimationEvent_t3593772963 * ___onUpdate_9;
	// Utage.CurveAnimationEvent Utage.CurveAnimation::onComplete
	CurveAnimationEvent_t3593772963 * ___onComplete_10;
	// System.Single Utage.CurveAnimation::<CurrentAnimationTime>k__BackingField
	float ___U3CCurrentAnimationTimeU3Ek__BackingField_11;
	// UnityEngine.Coroutine Utage.CurveAnimation::currentCoroutine
	Coroutine_t2299508840 * ___currentCoroutine_12;

public:
	inline static int32_t get_offset_of_curve_2() { return static_cast<int32_t>(offsetof(CurveAnimation_t2521618615, ___curve_2)); }
	inline AnimationCurve_t3306541151 * get_curve_2() const { return ___curve_2; }
	inline AnimationCurve_t3306541151 ** get_address_of_curve_2() { return &___curve_2; }
	inline void set_curve_2(AnimationCurve_t3306541151 * value)
	{
		___curve_2 = value;
		Il2CppCodeGenWriteBarrier((&___curve_2), value);
	}

	inline static int32_t get_offset_of_delay_3() { return static_cast<int32_t>(offsetof(CurveAnimation_t2521618615, ___delay_3)); }
	inline float get_delay_3() const { return ___delay_3; }
	inline float* get_address_of_delay_3() { return &___delay_3; }
	inline void set_delay_3(float value)
	{
		___delay_3 = value;
	}

	inline static int32_t get_offset_of_duration_4() { return static_cast<int32_t>(offsetof(CurveAnimation_t2521618615, ___duration_4)); }
	inline float get_duration_4() const { return ___duration_4; }
	inline float* get_address_of_duration_4() { return &___duration_4; }
	inline void set_duration_4(float value)
	{
		___duration_4 = value;
	}

	inline static int32_t get_offset_of_unscaledTime_5() { return static_cast<int32_t>(offsetof(CurveAnimation_t2521618615, ___unscaledTime_5)); }
	inline bool get_unscaledTime_5() const { return ___unscaledTime_5; }
	inline bool* get_address_of_unscaledTime_5() { return &___unscaledTime_5; }
	inline void set_unscaledTime_5(bool value)
	{
		___unscaledTime_5 = value;
	}

	inline static int32_t get_offset_of_U3CValueU3Ek__BackingField_6() { return static_cast<int32_t>(offsetof(CurveAnimation_t2521618615, ___U3CValueU3Ek__BackingField_6)); }
	inline float get_U3CValueU3Ek__BackingField_6() const { return ___U3CValueU3Ek__BackingField_6; }
	inline float* get_address_of_U3CValueU3Ek__BackingField_6() { return &___U3CValueU3Ek__BackingField_6; }
	inline void set_U3CValueU3Ek__BackingField_6(float value)
	{
		___U3CValueU3Ek__BackingField_6 = value;
	}

	inline static int32_t get_offset_of_U3CIsPlayingU3Ek__BackingField_7() { return static_cast<int32_t>(offsetof(CurveAnimation_t2521618615, ___U3CIsPlayingU3Ek__BackingField_7)); }
	inline bool get_U3CIsPlayingU3Ek__BackingField_7() const { return ___U3CIsPlayingU3Ek__BackingField_7; }
	inline bool* get_address_of_U3CIsPlayingU3Ek__BackingField_7() { return &___U3CIsPlayingU3Ek__BackingField_7; }
	inline void set_U3CIsPlayingU3Ek__BackingField_7(bool value)
	{
		___U3CIsPlayingU3Ek__BackingField_7 = value;
	}

	inline static int32_t get_offset_of_onStart_8() { return static_cast<int32_t>(offsetof(CurveAnimation_t2521618615, ___onStart_8)); }
	inline CurveAnimationEvent_t3593772963 * get_onStart_8() const { return ___onStart_8; }
	inline CurveAnimationEvent_t3593772963 ** get_address_of_onStart_8() { return &___onStart_8; }
	inline void set_onStart_8(CurveAnimationEvent_t3593772963 * value)
	{
		___onStart_8 = value;
		Il2CppCodeGenWriteBarrier((&___onStart_8), value);
	}

	inline static int32_t get_offset_of_onUpdate_9() { return static_cast<int32_t>(offsetof(CurveAnimation_t2521618615, ___onUpdate_9)); }
	inline CurveAnimationEvent_t3593772963 * get_onUpdate_9() const { return ___onUpdate_9; }
	inline CurveAnimationEvent_t3593772963 ** get_address_of_onUpdate_9() { return &___onUpdate_9; }
	inline void set_onUpdate_9(CurveAnimationEvent_t3593772963 * value)
	{
		___onUpdate_9 = value;
		Il2CppCodeGenWriteBarrier((&___onUpdate_9), value);
	}

	inline static int32_t get_offset_of_onComplete_10() { return static_cast<int32_t>(offsetof(CurveAnimation_t2521618615, ___onComplete_10)); }
	inline CurveAnimationEvent_t3593772963 * get_onComplete_10() const { return ___onComplete_10; }
	inline CurveAnimationEvent_t3593772963 ** get_address_of_onComplete_10() { return &___onComplete_10; }
	inline void set_onComplete_10(CurveAnimationEvent_t3593772963 * value)
	{
		___onComplete_10 = value;
		Il2CppCodeGenWriteBarrier((&___onComplete_10), value);
	}

	inline static int32_t get_offset_of_U3CCurrentAnimationTimeU3Ek__BackingField_11() { return static_cast<int32_t>(offsetof(CurveAnimation_t2521618615, ___U3CCurrentAnimationTimeU3Ek__BackingField_11)); }
	inline float get_U3CCurrentAnimationTimeU3Ek__BackingField_11() const { return ___U3CCurrentAnimationTimeU3Ek__BackingField_11; }
	inline float* get_address_of_U3CCurrentAnimationTimeU3Ek__BackingField_11() { return &___U3CCurrentAnimationTimeU3Ek__BackingField_11; }
	inline void set_U3CCurrentAnimationTimeU3Ek__BackingField_11(float value)
	{
		___U3CCurrentAnimationTimeU3Ek__BackingField_11 = value;
	}

	inline static int32_t get_offset_of_currentCoroutine_12() { return static_cast<int32_t>(offsetof(CurveAnimation_t2521618615, ___currentCoroutine_12)); }
	inline Coroutine_t2299508840 * get_currentCoroutine_12() const { return ___currentCoroutine_12; }
	inline Coroutine_t2299508840 ** get_address_of_currentCoroutine_12() { return &___currentCoroutine_12; }
	inline void set_currentCoroutine_12(Coroutine_t2299508840 * value)
	{
		___currentCoroutine_12 = value;
		Il2CppCodeGenWriteBarrier((&___currentCoroutine_12), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CURVEANIMATION_T2521618615_H
#ifndef PARTICLEAUTOMATICDESTROYER_T1117357552_H
#define PARTICLEAUTOMATICDESTROYER_T1117357552_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.ParticleAutomaticDestroyer
struct  ParticleAutomaticDestroyer_t1117357552  : public MonoBehaviour_t1158329972
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PARTICLEAUTOMATICDESTROYER_T1117357552_H
#ifndef FILEIOMANAGER_T855502573_H
#define FILEIOMANAGER_T855502573_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.FileIOManager
struct  FileIOManager_t855502573  : public FileIOManagerBase_t4139865030
{
public:
	// System.Byte[] Utage.FileIOManager::cryptKeyBytes
	ByteU5BU5D_t3397334013* ___cryptKeyBytes_18;
	// System.String Utage.FileIOManager::cryptKey
	String_t* ___cryptKey_19;

public:
	inline static int32_t get_offset_of_cryptKeyBytes_18() { return static_cast<int32_t>(offsetof(FileIOManager_t855502573, ___cryptKeyBytes_18)); }
	inline ByteU5BU5D_t3397334013* get_cryptKeyBytes_18() const { return ___cryptKeyBytes_18; }
	inline ByteU5BU5D_t3397334013** get_address_of_cryptKeyBytes_18() { return &___cryptKeyBytes_18; }
	inline void set_cryptKeyBytes_18(ByteU5BU5D_t3397334013* value)
	{
		___cryptKeyBytes_18 = value;
		Il2CppCodeGenWriteBarrier((&___cryptKeyBytes_18), value);
	}

	inline static int32_t get_offset_of_cryptKey_19() { return static_cast<int32_t>(offsetof(FileIOManager_t855502573, ___cryptKey_19)); }
	inline String_t* get_cryptKey_19() const { return ___cryptKey_19; }
	inline String_t** get_address_of_cryptKey_19() { return &___cryptKey_19; }
	inline void set_cryptKey_19(String_t* value)
	{
		___cryptKey_19 = value;
		Il2CppCodeGenWriteBarrier((&___cryptKey_19), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FILEIOMANAGER_T855502573_H
#ifndef UGUIANIMATION_T1352362454_H
#define UGUIANIMATION_T1352362454_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.UguiAnimation
struct  UguiAnimation_t1352362454  : public CurveAnimation_t2521618615
{
public:
	// Utage.UiEventMask Utage.UguiAnimation::eventMask
	int32_t ___eventMask_13;
	// Utage.UguiAnimation/AnimationType Utage.UguiAnimation::animationType
	int32_t ___animationType_14;
	// UnityEngine.UI.Graphic Utage.UguiAnimation::targetGraphic
	Graphic_t2426225576 * ___targetGraphic_15;

public:
	inline static int32_t get_offset_of_eventMask_13() { return static_cast<int32_t>(offsetof(UguiAnimation_t1352362454, ___eventMask_13)); }
	inline int32_t get_eventMask_13() const { return ___eventMask_13; }
	inline int32_t* get_address_of_eventMask_13() { return &___eventMask_13; }
	inline void set_eventMask_13(int32_t value)
	{
		___eventMask_13 = value;
	}

	inline static int32_t get_offset_of_animationType_14() { return static_cast<int32_t>(offsetof(UguiAnimation_t1352362454, ___animationType_14)); }
	inline int32_t get_animationType_14() const { return ___animationType_14; }
	inline int32_t* get_address_of_animationType_14() { return &___animationType_14; }
	inline void set_animationType_14(int32_t value)
	{
		___animationType_14 = value;
	}

	inline static int32_t get_offset_of_targetGraphic_15() { return static_cast<int32_t>(offsetof(UguiAnimation_t1352362454, ___targetGraphic_15)); }
	inline Graphic_t2426225576 * get_targetGraphic_15() const { return ___targetGraphic_15; }
	inline Graphic_t2426225576 ** get_address_of_targetGraphic_15() { return &___targetGraphic_15; }
	inline void set_targetGraphic_15(Graphic_t2426225576 * value)
	{
		___targetGraphic_15 = value;
		Il2CppCodeGenWriteBarrier((&___targetGraphic_15), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UGUIANIMATION_T1352362454_H
#ifndef UGUIANIMATIONALPHA_T1630313130_H
#define UGUIANIMATIONALPHA_T1630313130_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.UguiAnimationAlpha
struct  UguiAnimationAlpha_t1630313130  : public UguiAnimation_t1352362454
{
public:
	// System.Single Utage.UguiAnimationAlpha::from
	float ___from_16;
	// System.Single Utage.UguiAnimationAlpha::to
	float ___to_17;
	// System.Single Utage.UguiAnimationAlpha::by
	float ___by_18;
	// System.Single Utage.UguiAnimationAlpha::lerpFrom
	float ___lerpFrom_19;
	// System.Single Utage.UguiAnimationAlpha::lerpTo
	float ___lerpTo_20;

public:
	inline static int32_t get_offset_of_from_16() { return static_cast<int32_t>(offsetof(UguiAnimationAlpha_t1630313130, ___from_16)); }
	inline float get_from_16() const { return ___from_16; }
	inline float* get_address_of_from_16() { return &___from_16; }
	inline void set_from_16(float value)
	{
		___from_16 = value;
	}

	inline static int32_t get_offset_of_to_17() { return static_cast<int32_t>(offsetof(UguiAnimationAlpha_t1630313130, ___to_17)); }
	inline float get_to_17() const { return ___to_17; }
	inline float* get_address_of_to_17() { return &___to_17; }
	inline void set_to_17(float value)
	{
		___to_17 = value;
	}

	inline static int32_t get_offset_of_by_18() { return static_cast<int32_t>(offsetof(UguiAnimationAlpha_t1630313130, ___by_18)); }
	inline float get_by_18() const { return ___by_18; }
	inline float* get_address_of_by_18() { return &___by_18; }
	inline void set_by_18(float value)
	{
		___by_18 = value;
	}

	inline static int32_t get_offset_of_lerpFrom_19() { return static_cast<int32_t>(offsetof(UguiAnimationAlpha_t1630313130, ___lerpFrom_19)); }
	inline float get_lerpFrom_19() const { return ___lerpFrom_19; }
	inline float* get_address_of_lerpFrom_19() { return &___lerpFrom_19; }
	inline void set_lerpFrom_19(float value)
	{
		___lerpFrom_19 = value;
	}

	inline static int32_t get_offset_of_lerpTo_20() { return static_cast<int32_t>(offsetof(UguiAnimationAlpha_t1630313130, ___lerpTo_20)); }
	inline float get_lerpTo_20() const { return ___lerpTo_20; }
	inline float* get_address_of_lerpTo_20() { return &___lerpTo_20; }
	inline void set_lerpTo_20(float value)
	{
		___lerpTo_20 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UGUIANIMATIONALPHA_T1630313130_H





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2600 = { sizeof (NotEditableAttribute_t3788200865), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2600[2] = 
{
	NotEditableAttribute_t3788200865::get_offset_of_U3CEnablePropertyPathU3Ek__BackingField_1(),
	NotEditableAttribute_t3788200865::get_offset_of_U3CIsEnablePropertyU3Ek__BackingField_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2601 = { sizeof (OverridePropertyDrawAttribute_t3814407393), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2601[1] = 
{
	OverridePropertyDrawAttribute_t3814407393::get_offset_of_U3CFunctionU3Ek__BackingField_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2602 = { sizeof (PathDialogAttribute_t1627133723), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2602[2] = 
{
	PathDialogAttribute_t1627133723::get_offset_of_U3CTypeU3Ek__BackingField_1(),
	PathDialogAttribute_t1627133723::get_offset_of_U3CExtentionU3Ek__BackingField_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2603 = { sizeof (DialogType_t1187455388)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2603[3] = 
{
	DialogType_t1187455388::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2604 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2604[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2605 = { sizeof (StringPopupAttribute_t1790424821), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2605[1] = 
{
	StringPopupAttribute_t1790424821::get_offset_of_stringList_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2606 = { sizeof (StringPopupFunctionAttribute_t2619373699), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2606[1] = 
{
	StringPopupFunctionAttribute_t2619373699::get_offset_of_U3CFunctionU3Ek__BackingField_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2607 = { sizeof (StringPopupIndexedAttribute_t3196049692), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2607[1] = 
{
	StringPopupIndexedAttribute_t3196049692::get_offset_of_U3CFunctionU3Ek__BackingField_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2608 = { sizeof (UiEventMask_t3658541192)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2608[18] = 
{
	UiEventMask_t3658541192::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2609 = { sizeof (UguiAnimation_t1352362454), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2609[3] = 
{
	UguiAnimation_t1352362454::get_offset_of_eventMask_13(),
	UguiAnimation_t1352362454::get_offset_of_animationType_14(),
	UguiAnimation_t1352362454::get_offset_of_targetGraphic_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2610 = { sizeof (AnimationType_t1473548579)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2610[5] = 
{
	AnimationType_t1473548579::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2611 = { sizeof (UguiAnimationAlpha_t1630313130), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2611[5] = 
{
	UguiAnimationAlpha_t1630313130::get_offset_of_from_16(),
	UguiAnimationAlpha_t1630313130::get_offset_of_to_17(),
	UguiAnimationAlpha_t1630313130::get_offset_of_by_18(),
	UguiAnimationAlpha_t1630313130::get_offset_of_lerpFrom_19(),
	UguiAnimationAlpha_t1630313130::get_offset_of_lerpTo_20(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2612 = { sizeof (CurveAnimationEvent_t3593772963), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2613 = { sizeof (CurveAnimation_t2521618615), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2613[11] = 
{
	CurveAnimation_t2521618615::get_offset_of_curve_2(),
	CurveAnimation_t2521618615::get_offset_of_delay_3(),
	CurveAnimation_t2521618615::get_offset_of_duration_4(),
	CurveAnimation_t2521618615::get_offset_of_unscaledTime_5(),
	CurveAnimation_t2521618615::get_offset_of_U3CValueU3Ek__BackingField_6(),
	CurveAnimation_t2521618615::get_offset_of_U3CIsPlayingU3Ek__BackingField_7(),
	CurveAnimation_t2521618615::get_offset_of_onStart_8(),
	CurveAnimation_t2521618615::get_offset_of_onUpdate_9(),
	CurveAnimation_t2521618615::get_offset_of_onComplete_10(),
	CurveAnimation_t2521618615::get_offset_of_U3CCurrentAnimationTimeU3Ek__BackingField_11(),
	CurveAnimation_t2521618615::get_offset_of_currentCoroutine_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2614 = { sizeof (U3CCoAnimationU3Ec__Iterator0_t1277845393), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2614[9] = 
{
	U3CCoAnimationU3Ec__Iterator0_t1277845393::get_offset_of_U3CdelayStartTimeU3E__1_0(),
	U3CCoAnimationU3Ec__Iterator0_t1277845393::get_offset_of_U3CendTimeU3E__0_1(),
	U3CCoAnimationU3Ec__Iterator0_t1277845393::get_offset_of_U3CstartTimeU3E__0_2(),
	U3CCoAnimationU3Ec__Iterator0_t1277845393::get_offset_of_onUpdate_3(),
	U3CCoAnimationU3Ec__Iterator0_t1277845393::get_offset_of_onComplete_4(),
	U3CCoAnimationU3Ec__Iterator0_t1277845393::get_offset_of_U24this_5(),
	U3CCoAnimationU3Ec__Iterator0_t1277845393::get_offset_of_U24current_6(),
	U3CCoAnimationU3Ec__Iterator0_t1277845393::get_offset_of_U24disposing_7(),
	U3CCoAnimationU3Ec__Iterator0_t1277845393::get_offset_of_U24PC_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2615 = { sizeof (LinkTransform_t2861363074), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2615[9] = 
{
	LinkTransform_t2861363074::get_offset_of_target_2(),
	LinkTransform_t2861363074::get_offset_of_isInit_3(),
	LinkTransform_t2861363074::get_offset_of_targetPosition_4(),
	LinkTransform_t2861363074::get_offset_of_targetScale_5(),
	LinkTransform_t2861363074::get_offset_of_targetEuler_6(),
	LinkTransform_t2861363074::get_offset_of_startPosition_7(),
	LinkTransform_t2861363074::get_offset_of_startScale_8(),
	LinkTransform_t2861363074::get_offset_of_startEuler_9(),
	LinkTransform_t2861363074::get_offset_of_cachedTransform_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2616 = { sizeof (U3CCoUpdateU3Ec__Iterator0_t418606427), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2616[5] = 
{
	U3CCoUpdateU3Ec__Iterator0_t418606427::get_offset_of_U3CrectTransformU3E__1_0(),
	U3CCoUpdateU3Ec__Iterator0_t418606427::get_offset_of_U24this_1(),
	U3CCoUpdateU3Ec__Iterator0_t418606427::get_offset_of_U24current_2(),
	U3CCoUpdateU3Ec__Iterator0_t418606427::get_offset_of_U24disposing_3(),
	U3CCoUpdateU3Ec__Iterator0_t418606427::get_offset_of_U24PC_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2617 = { sizeof (MiniAnimationData_t521227391), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2617[1] = 
{
	MiniAnimationData_t521227391::get_offset_of_dataList_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2618 = { sizeof (Data_t619954766), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2618[2] = 
{
	Data_t619954766::get_offset_of_duration_0(),
	Data_t619954766::get_offset_of_name_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2619 = { sizeof (NamingType_t115011021)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2619[4] = 
{
	NamingType_t115011021::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2620 = { sizeof (MosaicRenderer_t4160220697), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2620[7] = 
{
	MosaicRenderer_t4160220697::get_offset_of_mosaicSize_2(),
	MosaicRenderer_t4160220697::get_offset_of_zTest_3(),
	MosaicRenderer_t4160220697::get_offset_of_sortingLayerName_4(),
	MosaicRenderer_t4160220697::get_offset_of_sortingOrder_5(),
	MosaicRenderer_t4160220697::get_offset_of_autoScale_6(),
	MosaicRenderer_t4160220697::get_offset_of_gameScreenWidth_7(),
	MosaicRenderer_t4160220697::get_offset_of_gameScreenHeight_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2621 = { sizeof (MotionPlayType_t785574570)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2621[7] = 
{
	MotionPlayType_t785574570::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2622 = { sizeof (MotionPlayTypeUtil_t2936258342), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2623 = { sizeof (ParticleAutomaticDestroyer_t1117357552), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2624 = { sizeof (ParticleScaler_t814660744), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2624[7] = 
{
	ParticleScaler_t814660744::get_offset_of_useLocalScale_2(),
	ParticleScaler_t814660744::get_offset_of_scale_3(),
	ParticleScaler_t814660744::get_offset_of_changeRenderMode_4(),
	ParticleScaler_t814660744::get_offset_of_changeGravity_5(),
	ParticleScaler_t814660744::get_offset_of_U3CHasChangedU3Ek__BackingField_6(),
	ParticleScaler_t814660744::get_offset_of_U3CIsInitU3Ek__BackingField_7(),
	ParticleScaler_t814660744::get_offset_of_defaultGravities_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2625 = { sizeof (ApplicationEvent_t30752656), -1, sizeof(ApplicationEvent_t30752656_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2625[4] = 
{
	ApplicationEvent_t30752656_StaticFields::get_offset_of_instance_2(),
	ApplicationEvent_t30752656::get_offset_of_OnScreenSizeChanged_3(),
	ApplicationEvent_t30752656::get_offset_of_screenWidth_4(),
	ApplicationEvent_t30752656::get_offset_of_screenHeight_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2626 = { sizeof (ButtonEventInfo_t1085784078), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2626[2] = 
{
	ButtonEventInfo_t1085784078::get_offset_of_text_0(),
	ButtonEventInfo_t1085784078::get_offset_of_callBackClicked_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2627 = { sizeof (OpenDialogEvent_t2352564994), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2628 = { sizeof (Open1ButtonDialogEvent_t586865045), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2629 = { sizeof (Open2ButtonDialogEvent_t1974290772), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2630 = { sizeof (Open3ButtonDialogEvent_t2400938643), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2631 = { sizeof (FloatEvent_t3725987244), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2632 = { sizeof (IntEvent_t3062348593), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2633 = { sizeof (StringEvent_t282042215), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2634 = { sizeof (BoolEvent_t2313125906), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2635 = { sizeof (ExpressionCast_t1679256093), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2636 = { sizeof (ExpressionParser_t665800307), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2636[3] = 
{
	ExpressionParser_t665800307::get_offset_of_exp_0(),
	ExpressionParser_t665800307::get_offset_of_errorMsg_1(),
	ExpressionParser_t665800307::get_offset_of_tokens_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2637 = { sizeof (ExpressionToken_t2745466747), -1, sizeof(ExpressionToken_t2745466747_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2637[46] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	ExpressionToken_t2745466747_StaticFields::get_offset_of_LpaToken_23(),
	ExpressionToken_t2745466747_StaticFields::get_offset_of_RpaToken_24(),
	ExpressionToken_t2745466747_StaticFields::get_offset_of_CommaToken_25(),
	ExpressionToken_t2745466747_StaticFields::get_offset_of_UniPlus_26(),
	ExpressionToken_t2745466747_StaticFields::get_offset_of_UniMinus_27(),
	ExpressionToken_t2745466747_StaticFields::get_offset_of_OperatorArray_28(),
	ExpressionToken_t2745466747::get_offset_of_name_29(),
	ExpressionToken_t2745466747::get_offset_of_isAlphabet_30(),
	ExpressionToken_t2745466747::get_offset_of_type_31(),
	ExpressionToken_t2745466747::get_offset_of_priority_32(),
	ExpressionToken_t2745466747::get_offset_of_variable_33(),
	ExpressionToken_t2745466747::get_offset_of_numFunctionArg_34(),
	0,
	0,
	0,
	0,
	0,
	0,
	ExpressionToken_t2745466747_StaticFields::get_offset_of_U3CU3Ef__switchU24map4_41(),
	ExpressionToken_t2745466747_StaticFields::get_offset_of_U3CU3Ef__switchU24map5_42(),
	ExpressionToken_t2745466747_StaticFields::get_offset_of_U3CU3Ef__switchU24map6_43(),
	ExpressionToken_t2745466747_StaticFields::get_offset_of_U3CU3Ef__switchU24map7_44(),
	ExpressionToken_t2745466747_StaticFields::get_offset_of_U3CU3Ef__switchU24map8_45(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2638 = { sizeof (TokenType_t202952175)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2638[10] = 
{
	TokenType_t202952175::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2639 = { sizeof (UtageExtensions_t58130528), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2640 = { sizeof (AssetBundleTargetFlags_t3506559986)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2640[6] = 
{
	AssetBundleTargetFlags_t3506559986::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2641 = { sizeof (AssetBundleHelper_t1847879106), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2642 = { sizeof (AssetBundleInfo_t1249246306), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2642[3] = 
{
	AssetBundleInfo_t1249246306::get_offset_of_U3CUrlU3Ek__BackingField_0(),
	AssetBundleInfo_t1249246306::get_offset_of_U3CHashU3Ek__BackingField_1(),
	AssetBundleInfo_t1249246306::get_offset_of_U3CVersionU3Ek__BackingField_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2643 = { sizeof (AssetBundleInfoManager_t223439115), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2643[7] = 
{
	AssetBundleInfoManager_t223439115::get_offset_of_retryCount_2(),
	AssetBundleInfoManager_t223439115::get_offset_of_timeOut_3(),
	AssetBundleInfoManager_t223439115::get_offset_of_useCacheManifest_4(),
	AssetBundleInfoManager_t223439115::get_offset_of_cacheDirectoryName_5(),
	AssetBundleInfoManager_t223439115::get_offset_of_assetFileManager_6(),
	AssetBundleInfoManager_t223439115::get_offset_of_dictionary_7(),
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2644 = { sizeof (U3CDownloadManifestAsyncU3Ec__AnonStorey0_t3359135894), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2644[6] = 
{
	U3CDownloadManifestAsyncU3Ec__AnonStorey0_t3359135894::get_offset_of_rootUrl_0(),
	U3CDownloadManifestAsyncU3Ec__AnonStorey0_t3359135894::get_offset_of_relativeUrl_1(),
	U3CDownloadManifestAsyncU3Ec__AnonStorey0_t3359135894::get_offset_of_wwwEx_2(),
	U3CDownloadManifestAsyncU3Ec__AnonStorey0_t3359135894::get_offset_of_onComplete_3(),
	U3CDownloadManifestAsyncU3Ec__AnonStorey0_t3359135894::get_offset_of_onFailed_4(),
	U3CDownloadManifestAsyncU3Ec__AnonStorey0_t3359135894::get_offset_of_U24this_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2645 = { sizeof (U3CLoadCacheManifestAsyncU3Ec__AnonStorey1_t1823896243), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2645[4] = 
{
	U3CLoadCacheManifestAsyncU3Ec__AnonStorey1_t1823896243::get_offset_of_rootUrl_0(),
	U3CLoadCacheManifestAsyncU3Ec__AnonStorey1_t1823896243::get_offset_of_onComplete_1(),
	U3CLoadCacheManifestAsyncU3Ec__AnonStorey1_t1823896243::get_offset_of_onFailed_2(),
	U3CLoadCacheManifestAsyncU3Ec__AnonStorey1_t1823896243::get_offset_of_U24this_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2646 = { sizeof (AssetFileManager_t1095395563), -1, sizeof(AssetFileManager_t1095395563_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2646[25] = 
{
	AssetFileManager_t1095395563::get_offset_of_fileIOManager_2(),
	AssetFileManager_t1095395563::get_offset_of_enableResourcesLoadAsync_3(),
	AssetFileManager_t1095395563::get_offset_of_timeOutDownload_4(),
	AssetFileManager_t1095395563::get_offset_of_autoRetryCountOnDonwloadError_5(),
	AssetFileManager_t1095395563::get_offset_of_loadFileMax_6(),
	AssetFileManager_t1095395563::get_offset_of_rangeOfFilesOnMemory_7(),
	AssetFileManager_t1095395563::get_offset_of_unloadType_8(),
	AssetFileManager_t1095395563::get_offset_of_isOutPutDebugLog_9(),
	AssetFileManager_t1095395563::get_offset_of_isDebugCacheFileName_10(),
	AssetFileManager_t1095395563::get_offset_of_isDebugBootDeleteChacheTextAndBinary_11(),
	AssetFileManager_t1095395563::get_offset_of_isDebugBootDeleteChacheAll_12(),
	AssetFileManager_t1095395563::get_offset_of_settings_13(),
	AssetFileManager_t1095395563::get_offset_of_assetBundleInfoManager_14(),
	AssetFileManager_t1095395563::get_offset_of_dummyFiles_15(),
	AssetFileManager_t1095395563::get_offset_of_loadingFileList_16(),
	AssetFileManager_t1095395563::get_offset_of_loadWaitFileList_17(),
	AssetFileManager_t1095395563::get_offset_of_usingFileList_18(),
	AssetFileManager_t1095395563::get_offset_of_fileTbl_19(),
	AssetFileManager_t1095395563::get_offset_of_customLoadManager_20(),
	AssetFileManager_t1095395563::get_offset_of_staticAssetManager_21(),
	AssetFileManager_t1095395563::get_offset_of_callbackError_22(),
	AssetFileManager_t1095395563::get_offset_of_isWaitingRetry_23(),
	AssetFileManager_t1095395563::get_offset_of_unloadingUnusedAssets_24(),
	AssetFileManager_t1095395563_StaticFields::get_offset_of_isEditorErrorCheck_25(),
	AssetFileManager_t1095395563_StaticFields::get_offset_of_instance_26(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2647 = { sizeof (UnloadType_t122993117)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2647[5] = 
{
	UnloadType_t122993117::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2648 = { sizeof (U3CCallbackFileLoadErrorU3Ec__AnonStorey4_t834973050), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2648[2] = 
{
	U3CCallbackFileLoadErrorU3Ec__AnonStorey4_t834973050::get_offset_of_errorFile_0(),
	U3CCallbackFileLoadErrorU3Ec__AnonStorey4_t834973050::get_offset_of_U24this_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2649 = { sizeof (U3CCoWaitRetryU3Ec__Iterator0_t361625508), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2649[5] = 
{
	U3CCoWaitRetryU3Ec__Iterator0_t361625508::get_offset_of_file_0(),
	U3CCoWaitRetryU3Ec__Iterator0_t361625508::get_offset_of_U24this_1(),
	U3CCoWaitRetryU3Ec__Iterator0_t361625508::get_offset_of_U24current_2(),
	U3CCoWaitRetryU3Ec__Iterator0_t361625508::get_offset_of_U24disposing_3(),
	U3CCoWaitRetryU3Ec__Iterator0_t361625508::get_offset_of_U24PC_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2650 = { sizeof (U3CCoLoadWaitU3Ec__Iterator1_t578320303), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2650[6] = 
{
	U3CCoLoadWaitU3Ec__Iterator1_t578320303::get_offset_of_file_0(),
	U3CCoLoadWaitU3Ec__Iterator1_t578320303::get_offset_of_onComplete_1(),
	U3CCoLoadWaitU3Ec__Iterator1_t578320303::get_offset_of_U24this_2(),
	U3CCoLoadWaitU3Ec__Iterator1_t578320303::get_offset_of_U24current_3(),
	U3CCoLoadWaitU3Ec__Iterator1_t578320303::get_offset_of_U24disposing_4(),
	U3CCoLoadWaitU3Ec__Iterator1_t578320303::get_offset_of_U24PC_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2651 = { sizeof (U3CLoadAsyncU3Ec__Iterator2_t109345333), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2651[6] = 
{
	U3CLoadAsyncU3Ec__Iterator2_t109345333::get_offset_of_file_0(),
	U3CLoadAsyncU3Ec__Iterator2_t109345333::get_offset_of_U24this_1(),
	U3CLoadAsyncU3Ec__Iterator2_t109345333::get_offset_of_U24current_2(),
	U3CLoadAsyncU3Ec__Iterator2_t109345333::get_offset_of_U24disposing_3(),
	U3CLoadAsyncU3Ec__Iterator2_t109345333::get_offset_of_U24PC_4(),
	U3CLoadAsyncU3Ec__Iterator2_t109345333::get_offset_of_U24locvar0_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2652 = { sizeof (U3CLoadAsyncU3Ec__AnonStorey5_t3302198958), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2652[2] = 
{
	U3CLoadAsyncU3Ec__AnonStorey5_t3302198958::get_offset_of_file_0(),
	U3CLoadAsyncU3Ec__AnonStorey5_t3302198958::get_offset_of_U3CU3Ef__refU242_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2653 = { sizeof (U3CUnloadUnusedAssetsAsyncU3Ec__Iterator3_t4247547924), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2653[4] = 
{
	U3CUnloadUnusedAssetsAsyncU3Ec__Iterator3_t4247547924::get_offset_of_U24this_0(),
	U3CUnloadUnusedAssetsAsyncU3Ec__Iterator3_t4247547924::get_offset_of_U24current_1(),
	U3CUnloadUnusedAssetsAsyncU3Ec__Iterator3_t4247547924::get_offset_of_U24disposing_2(),
	U3CUnloadUnusedAssetsAsyncU3Ec__Iterator3_t4247547924::get_offset_of_U24PC_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2654 = { sizeof (AssetFileManagerSettings_t1473658718), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2654[3] = 
{
	AssetFileManagerSettings_t1473658718::get_offset_of_loadType_0(),
	AssetFileManagerSettings_t1473658718::get_offset_of_fileSettings_1(),
	AssetFileManagerSettings_t1473658718::get_offset_of_rebuildFileSettings_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2655 = { sizeof (LoadType_t165610055)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2655[5] = 
{
	LoadType_t165610055::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2656 = { sizeof (U3CFindU3Ec__AnonStorey0_t3870382361), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2656[1] = 
{
	U3CFindU3Ec__AnonStorey0_t3870382361::get_offset_of_type_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2657 = { sizeof (U3CFindSettingFromPathU3Ec__AnonStorey1_t1929813237), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2657[1] = 
{
	U3CFindSettingFromPathU3Ec__AnonStorey1_t1929813237::get_offset_of_path_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2658 = { sizeof (AssetFileSetting_t1869957816), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2658[4] = 
{
	AssetFileSetting_t1869957816::get_offset_of_fileType_0(),
	AssetFileSetting_t1869957816::get_offset_of_isStreamingAssets_1(),
	AssetFileSetting_t1869957816::get_offset_of_extensions_2(),
	AssetFileSetting_t1869957816::get_offset_of_settings_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2659 = { sizeof (AssetFileReference_t4090268667), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2659[1] = 
{
	AssetFileReference_t4090268667::get_offset_of_file_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2660 = { sizeof (Compression_t2003203368), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2660[8] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2661 = { sizeof (Node_t1298946215), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2661[3] = 
{
	Node_t1298946215::get_offset_of_mNext_0(),
	Node_t1298946215::get_offset_of_mPrev_1(),
	Node_t1298946215::get_offset_of_mPos_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2662 = { sizeof (Index_t675362613), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2662[3] = 
{
	Index_t675362613::get_offset_of_mNodes_0(),
	Index_t675362613::get_offset_of_mStack_1(),
	Index_t675362613::get_offset_of_mStackPos_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2663 = { sizeof (ConvertFileListManager_t2896753090), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2664 = { sizeof (Crypt_t3650163410), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2665 = { sizeof (CustomLoadManager_t3941822120), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2665[1] = 
{
	CustomLoadManager_t3941822120::get_offset_of_U3COnFindAssetU3Ek__BackingField_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2666 = { sizeof (FindAsset_t4163870574), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2667 = { sizeof (FileIOManager_t855502573), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2667[2] = 
{
	FileIOManager_t855502573::get_offset_of_cryptKeyBytes_18(),
	FileIOManager_t855502573::get_offset_of_cryptKey_19(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2668 = { sizeof (FileIOManagerBase_t4139865030), -1, sizeof(FileIOManagerBase_t4139865030_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2668[16] = 
{
	0,
	FileIOManagerBase_t4139865030_StaticFields::get_offset_of_customEncode_3(),
	FileIOManagerBase_t4139865030_StaticFields::get_offset_of_customDecode_4(),
	FileIOManagerBase_t4139865030_StaticFields::get_offset_of_customEncodeNoCompress_5(),
	FileIOManagerBase_t4139865030_StaticFields::get_offset_of_customDecodeNoCompress_6(),
	FileIOManagerBase_t4139865030_StaticFields::get_offset_of_audioHeader_7(),
	0,
	0,
	0,
	FileIOManagerBase_t4139865030_StaticFields::get_offset_of_workBufferArray_11(),
	FileIOManagerBase_t4139865030_StaticFields::get_offset_of_audioShortWorkArray_12(),
	FileIOManagerBase_t4139865030_StaticFields::get_offset_of_audioSamplesWorkArray_13(),
	FileIOManagerBase_t4139865030_StaticFields::get_offset_of_U3CU3Ef__mgU24cache0_14(),
	FileIOManagerBase_t4139865030_StaticFields::get_offset_of_U3CU3Ef__mgU24cache1_15(),
	FileIOManagerBase_t4139865030_StaticFields::get_offset_of_U3CU3Ef__mgU24cache2_16(),
	FileIOManagerBase_t4139865030_StaticFields::get_offset_of_U3CU3Ef__mgU24cache3_17(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2669 = { sizeof (SoundHeader_t574408063)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2669[5] = 
{
	SoundHeader_t574408063::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2670 = { sizeof (FilePathUtil_t4082793301), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2671 = { sizeof (AssetFileType_t2804304536)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2671[5] = 
{
	AssetFileType_t2804304536::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2672 = { sizeof (AssetFileStrageType_t3334005126)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2672[4] = 
{
	AssetFileStrageType_t3334005126::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2673 = { sizeof (AssetFileLoadFlags_t3123011941)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2673[6] = 
{
	AssetFileLoadFlags_t3123011941::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2674 = { sizeof (AssetFileLoadPriority_t1290657756)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2674[5] = 
{
	AssetFileLoadPriority_t1290657756::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2675 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2676 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2677 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2678 = { sizeof (AssetFileLoadComplete_t3808879259), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2679 = { sizeof (AssetFileEvent_t691182924), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2680 = { sizeof (AssetFileBase_t4101360697), -1, sizeof(AssetFileBase_t4101360697_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2680[15] = 
{
	AssetFileBase_t4101360697::get_offset_of_U3CFileManagerU3Ek__BackingField_0(),
	AssetFileBase_t4101360697::get_offset_of_U3CFileInfoU3Ek__BackingField_1(),
	AssetFileBase_t4101360697::get_offset_of_U3CSettingDataU3Ek__BackingField_2(),
	AssetFileBase_t4101360697::get_offset_of_U3CFileTypeU3Ek__BackingField_3(),
	AssetFileBase_t4101360697::get_offset_of_U3CIsLoadEndU3Ek__BackingField_4(),
	AssetFileBase_t4101360697::get_offset_of_U3CIsLoadErrorU3Ek__BackingField_5(),
	AssetFileBase_t4101360697::get_offset_of_U3CLoadErrorMsgU3Ek__BackingField_6(),
	AssetFileBase_t4101360697::get_offset_of_U3CTextU3Ek__BackingField_7(),
	AssetFileBase_t4101360697::get_offset_of_U3CTextureU3Ek__BackingField_8(),
	AssetFileBase_t4101360697::get_offset_of_U3CSoundU3Ek__BackingField_9(),
	AssetFileBase_t4101360697::get_offset_of_U3CUnityObjectU3Ek__BackingField_10(),
	AssetFileBase_t4101360697::get_offset_of_U3CPriorityU3Ek__BackingField_11(),
	AssetFileBase_t4101360697::get_offset_of_U3CIgnoreUnloadU3Ek__BackingField_12(),
	AssetFileBase_t4101360697::get_offset_of_referenceSet_13(),
	AssetFileBase_t4101360697_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2681 = { sizeof (AssetFileDummyOnLoadError_t419678579), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2681[6] = 
{
	AssetFileDummyOnLoadError_t419678579::get_offset_of_isEnable_0(),
	AssetFileDummyOnLoadError_t419678579::get_offset_of_outputErrorLog_1(),
	AssetFileDummyOnLoadError_t419678579::get_offset_of_texture_2(),
	AssetFileDummyOnLoadError_t419678579::get_offset_of_sound_3(),
	AssetFileDummyOnLoadError_t419678579::get_offset_of_text_4(),
	AssetFileDummyOnLoadError_t419678579::get_offset_of_asset_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2682 = { sizeof (AssetFileUtage_t3568203916), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2682[2] = 
{
	AssetFileUtage_t3568203916::get_offset_of_U3CLoadPathU3Ek__BackingField_15(),
	AssetFileUtage_t3568203916::get_offset_of_U3CAssetBundleU3Ek__BackingField_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2683 = { sizeof (U3CLoadAsyncU3Ec__Iterator0_t2893323214), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2683[7] = 
{
	U3CLoadAsyncU3Ec__Iterator0_t2893323214::get_offset_of_onComplete_0(),
	U3CLoadAsyncU3Ec__Iterator0_t2893323214::get_offset_of_onFailed_1(),
	U3CLoadAsyncU3Ec__Iterator0_t2893323214::get_offset_of_U24this_2(),
	U3CLoadAsyncU3Ec__Iterator0_t2893323214::get_offset_of_U24current_3(),
	U3CLoadAsyncU3Ec__Iterator0_t2893323214::get_offset_of_U24disposing_4(),
	U3CLoadAsyncU3Ec__Iterator0_t2893323214::get_offset_of_U24PC_5(),
	U3CLoadAsyncU3Ec__Iterator0_t2893323214::get_offset_of_U24locvar0_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2684 = { sizeof (U3CLoadAsyncU3Ec__AnonStorey5_t4158068117), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2684[3] = 
{
	U3CLoadAsyncU3Ec__AnonStorey5_t4158068117::get_offset_of_onComplete_0(),
	U3CLoadAsyncU3Ec__AnonStorey5_t4158068117::get_offset_of_onFailed_1(),
	U3CLoadAsyncU3Ec__AnonStorey5_t4158068117::get_offset_of_U3CU3Ef__refU240_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2685 = { sizeof (U3CLoadAsyncSubU3Ec__Iterator1_t979517335), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2685[8] = 
{
	U3CLoadAsyncSubU3Ec__Iterator1_t979517335::get_offset_of_U24locvar0_0(),
	U3CLoadAsyncSubU3Ec__Iterator1_t979517335::get_offset_of_path_1(),
	U3CLoadAsyncSubU3Ec__Iterator1_t979517335::get_offset_of_onComplete_2(),
	U3CLoadAsyncSubU3Ec__Iterator1_t979517335::get_offset_of_onFailed_3(),
	U3CLoadAsyncSubU3Ec__Iterator1_t979517335::get_offset_of_U24this_4(),
	U3CLoadAsyncSubU3Ec__Iterator1_t979517335::get_offset_of_U24current_5(),
	U3CLoadAsyncSubU3Ec__Iterator1_t979517335::get_offset_of_U24disposing_6(),
	U3CLoadAsyncSubU3Ec__Iterator1_t979517335::get_offset_of_U24PC_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2686 = { sizeof (U3CLoadResourceAsyncU3Ec__Iterator2_t3762021256), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2686[8] = 
{
	U3CLoadResourceAsyncU3Ec__Iterator2_t3762021256::get_offset_of_loadPath_0(),
	U3CLoadResourceAsyncU3Ec__Iterator2_t3762021256::get_offset_of_U3CrequestU3E__0_1(),
	U3CLoadResourceAsyncU3Ec__Iterator2_t3762021256::get_offset_of_onComplete_2(),
	U3CLoadResourceAsyncU3Ec__Iterator2_t3762021256::get_offset_of_onFailed_3(),
	U3CLoadResourceAsyncU3Ec__Iterator2_t3762021256::get_offset_of_U24this_4(),
	U3CLoadResourceAsyncU3Ec__Iterator2_t3762021256::get_offset_of_U24current_5(),
	U3CLoadResourceAsyncU3Ec__Iterator2_t3762021256::get_offset_of_U24disposing_6(),
	U3CLoadResourceAsyncU3Ec__Iterator2_t3762021256::get_offset_of_U24PC_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2687 = { sizeof (U3CLoadAssetBundleAsyncU3Ec__Iterator3_t2280417849), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2687[9] = 
{
	U3CLoadAssetBundleAsyncU3Ec__Iterator3_t2280417849::get_offset_of_path_0(),
	U3CLoadAssetBundleAsyncU3Ec__Iterator3_t2280417849::get_offset_of_U3CwwwExU3E__0_1(),
	U3CLoadAssetBundleAsyncU3Ec__Iterator3_t2280417849::get_offset_of_onComplete_2(),
	U3CLoadAssetBundleAsyncU3Ec__Iterator3_t2280417849::get_offset_of_onFailed_3(),
	U3CLoadAssetBundleAsyncU3Ec__Iterator3_t2280417849::get_offset_of_U24this_4(),
	U3CLoadAssetBundleAsyncU3Ec__Iterator3_t2280417849::get_offset_of_U24current_5(),
	U3CLoadAssetBundleAsyncU3Ec__Iterator3_t2280417849::get_offset_of_U24disposing_6(),
	U3CLoadAssetBundleAsyncU3Ec__Iterator3_t2280417849::get_offset_of_U24PC_7(),
	U3CLoadAssetBundleAsyncU3Ec__Iterator3_t2280417849::get_offset_of_U24locvar0_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2688 = { sizeof (U3CLoadAssetBundleAsyncU3Ec__AnonStorey6_t1993755113), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2688[4] = 
{
	U3CLoadAssetBundleAsyncU3Ec__AnonStorey6_t1993755113::get_offset_of_onComplete_0(),
	U3CLoadAssetBundleAsyncU3Ec__AnonStorey6_t1993755113::get_offset_of_assetBundle_1(),
	U3CLoadAssetBundleAsyncU3Ec__AnonStorey6_t1993755113::get_offset_of_onFailed_2(),
	U3CLoadAssetBundleAsyncU3Ec__AnonStorey6_t1993755113::get_offset_of_U3CU3Ef__refU243_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2689 = { sizeof (U3CLoadAssetBundleAsyncU3Ec__Iterator4_t2280417856), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2689[9] = 
{
	U3CLoadAssetBundleAsyncU3Ec__Iterator4_t2280417856::get_offset_of_assetBundle_0(),
	U3CLoadAssetBundleAsyncU3Ec__Iterator4_t2280417856::get_offset_of_U3CrequestU3E__0_1(),
	U3CLoadAssetBundleAsyncU3Ec__Iterator4_t2280417856::get_offset_of_U3CassetsU3E__0_2(),
	U3CLoadAssetBundleAsyncU3Ec__Iterator4_t2280417856::get_offset_of_onFailed_3(),
	U3CLoadAssetBundleAsyncU3Ec__Iterator4_t2280417856::get_offset_of_onComplete_4(),
	U3CLoadAssetBundleAsyncU3Ec__Iterator4_t2280417856::get_offset_of_U24this_5(),
	U3CLoadAssetBundleAsyncU3Ec__Iterator4_t2280417856::get_offset_of_U24current_6(),
	U3CLoadAssetBundleAsyncU3Ec__Iterator4_t2280417856::get_offset_of_U24disposing_7(),
	U3CLoadAssetBundleAsyncU3Ec__Iterator4_t2280417856::get_offset_of_U24PC_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2690 = { sizeof (AssetFileInfo_t1386031564), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2690[4] = 
{
	AssetFileInfo_t1386031564::get_offset_of_U3CFileNameU3Ek__BackingField_0(),
	AssetFileInfo_t1386031564::get_offset_of_U3CAssetBundleInfoU3Ek__BackingField_1(),
	AssetFileInfo_t1386031564::get_offset_of_U3CSettingU3Ek__BackingField_2(),
	AssetFileInfo_t1386031564::get_offset_of_U3CStrageTypeU3Ek__BackingField_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2691 = { sizeof (StaticAssetManager_t393950057), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2691[1] = 
{
	StaticAssetManager_t393950057::get_offset_of_assets_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2692 = { sizeof (U3CFindAssetFileU3Ec__AnonStorey0_t574198236), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2692[1] = 
{
	U3CFindAssetFileU3Ec__AnonStorey0_t574198236::get_offset_of_assetName_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2693 = { sizeof (StaticAsset_t1112757920), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2693[1] = 
{
	StaticAsset_t1112757920::get_offset_of_asset_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2694 = { sizeof (StaticAssetFile_t628233278), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2694[1] = 
{
	StaticAssetFile_t628233278::get_offset_of_U3CAssetU3Ek__BackingField_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2695 = { sizeof (U3CLoadAsyncU3Ec__Iterator0_t1730758704), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2695[4] = 
{
	U3CLoadAsyncU3Ec__Iterator0_t1730758704::get_offset_of_onComplete_0(),
	U3CLoadAsyncU3Ec__Iterator0_t1730758704::get_offset_of_U24current_1(),
	U3CLoadAsyncU3Ec__Iterator0_t1730758704::get_offset_of_U24disposing_2(),
	U3CLoadAsyncU3Ec__Iterator0_t1730758704::get_offset_of_U24PC_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2696 = { sizeof (WWWEx_t1775400116), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2696[11] = 
{
	WWWEx_t1775400116::get_offset_of_U3CUrlU3Ek__BackingField_0(),
	WWWEx_t1775400116::get_offset_of_U3CAssetBundleHashU3Ek__BackingField_1(),
	WWWEx_t1775400116::get_offset_of_U3CAssetBundleVersionU3Ek__BackingField_2(),
	WWWEx_t1775400116::get_offset_of_U3CLoadTypeU3Ek__BackingField_3(),
	WWWEx_t1775400116::get_offset_of_U3CRetryCountU3Ek__BackingField_4(),
	WWWEx_t1775400116::get_offset_of_U3CTimeOutU3Ek__BackingField_5(),
	WWWEx_t1775400116::get_offset_of_U3CProgressU3Ek__BackingField_6(),
	WWWEx_t1775400116::get_offset_of_U3COnUpdateU3Ek__BackingField_7(),
	WWWEx_t1775400116::get_offset_of_U3CIgnoreDebugLogU3Ek__BackingField_8(),
	WWWEx_t1775400116::get_offset_of_U3CStoreBytesU3Ek__BackingField_9(),
	WWWEx_t1775400116::get_offset_of_U3CBytesU3Ek__BackingField_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2697 = { sizeof (Type_t963772239)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2697[3] = 
{
	Type_t963772239::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2698 = { sizeof (U3CLoadAsyncSubU3Ec__Iterator0_t3752071966), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2698[12] = 
{
	U3CLoadAsyncSubU3Ec__Iterator0_t3752071966::get_offset_of_U3CretryU3E__0_0(),
	U3CLoadAsyncSubU3Ec__Iterator0_t3752071966::get_offset_of_U3CwwwU3E__1_1(),
	U3CLoadAsyncSubU3Ec__Iterator0_t3752071966::get_offset_of_U3CtimeU3E__2_2(),
	U3CLoadAsyncSubU3Ec__Iterator0_t3752071966::get_offset_of_U3CisTimeOutU3E__2_3(),
	U3CLoadAsyncSubU3Ec__Iterator0_t3752071966::get_offset_of_retryCount_4(),
	U3CLoadAsyncSubU3Ec__Iterator0_t3752071966::get_offset_of_onTimeOut_5(),
	U3CLoadAsyncSubU3Ec__Iterator0_t3752071966::get_offset_of_onFailed_6(),
	U3CLoadAsyncSubU3Ec__Iterator0_t3752071966::get_offset_of_onCopmlete_7(),
	U3CLoadAsyncSubU3Ec__Iterator0_t3752071966::get_offset_of_U24this_8(),
	U3CLoadAsyncSubU3Ec__Iterator0_t3752071966::get_offset_of_U24current_9(),
	U3CLoadAsyncSubU3Ec__Iterator0_t3752071966::get_offset_of_U24disposing_10(),
	U3CLoadAsyncSubU3Ec__Iterator0_t3752071966::get_offset_of_U24PC_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2699 = { sizeof (U3CLoadAsyncU3Ec__AnonStorey3_t105557981), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2699[3] = 
{
	U3CLoadAsyncU3Ec__AnonStorey3_t105557981::get_offset_of_onComplete_0(),
	U3CLoadAsyncU3Ec__AnonStorey3_t105557981::get_offset_of_onFailed_1(),
	U3CLoadAsyncU3Ec__AnonStorey3_t105557981::get_offset_of_U24this_2(),
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
