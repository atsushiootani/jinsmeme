﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "object-internals.h"

// UnityEngine.ParticleSystem
struct ParticleSystem_t3394631041;
// UnityEngine.AnimationCurve
struct AnimationCurve_t3306541151;
// UnityEngine.Gradient
struct Gradient_t3600583008;
// UnityEngine.ParticleSystemRenderer
struct ParticleSystemRenderer_t892265570;
// System.Char[]
struct CharU5BU5D_t1328083999;
// System.Void
struct Void_t1841601450;
// System.String
struct String_t;

extern RuntimeClass* MinMaxGradient_t3708298175_il2cpp_TypeInfo_var;
extern const uint32_t MainModule_get_startColor_m642758877_MetadataUsageId;
extern RuntimeClass* MinMaxCurve_t122563058_il2cpp_TypeInfo_var;
extern const uint32_t MainModule_get_gravityModifier_m483350030_MetadataUsageId;
struct AnimationCurve_t3306541151_marshaled_pinvoke;
struct AnimationCurve_t3306541151;;
struct AnimationCurve_t3306541151_marshaled_pinvoke;;
extern RuntimeClass* AnimationCurve_t3306541151_il2cpp_TypeInfo_var;
extern const uint32_t MinMaxCurve_t122563058_pinvoke_FromNativeMethodDefinition_MetadataUsageId;
struct AnimationCurve_t3306541151_marshaled_com;
struct AnimationCurve_t3306541151_marshaled_com;;
struct Gradient_t3600583008_marshaled_pinvoke;
struct Gradient_t3600583008;;
struct Gradient_t3600583008_marshaled_pinvoke;;
extern RuntimeClass* Gradient_t3600583008_il2cpp_TypeInfo_var;
extern const uint32_t MinMaxGradient_t3708298175_pinvoke_FromNativeMethodDefinition_MetadataUsageId;
struct Gradient_t3600583008_marshaled_com;
struct Gradient_t3600583008_marshaled_com;;



#ifndef U3CMODULEU3E_T3783534228_H
#define U3CMODULEU3E_T3783534228_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <Module>
struct  U3CModuleU3E_t3783534228 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CMODULEU3E_T3783534228_H
#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
struct Il2CppArrayBounds;
#ifndef RUNTIMEARRAY_H
#define RUNTIMEARRAY_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Array

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEARRAY_H
#ifndef VALUETYPE_T3507792607_H
#define VALUETYPE_T3507792607_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ValueType
struct  ValueType_t3507792607  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t3507792607_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t3507792607_marshaled_com
{
};
#endif // VALUETYPE_T3507792607_H
#ifndef ENUM_T2459695545_H
#define ENUM_T2459695545_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Enum
struct  Enum_t2459695545  : public ValueType_t3507792607
{
public:

public:
};

struct Enum_t2459695545_StaticFields
{
public:
	// System.Char[] System.Enum::split_char
	CharU5BU5D_t1328083999* ___split_char_0;

public:
	inline static int32_t get_offset_of_split_char_0() { return static_cast<int32_t>(offsetof(Enum_t2459695545_StaticFields, ___split_char_0)); }
	inline CharU5BU5D_t1328083999* get_split_char_0() const { return ___split_char_0; }
	inline CharU5BU5D_t1328083999** get_address_of_split_char_0() { return &___split_char_0; }
	inline void set_split_char_0(CharU5BU5D_t1328083999* value)
	{
		___split_char_0 = value;
		Il2CppCodeGenWriteBarrier((&___split_char_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t2459695545_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t2459695545_marshaled_com
{
};
#endif // ENUM_T2459695545_H
#ifndef COLOR_T2020392075_H
#define COLOR_T2020392075_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Color
struct  Color_t2020392075 
{
public:
	// System.Single UnityEngine.Color::r
	float ___r_0;
	// System.Single UnityEngine.Color::g
	float ___g_1;
	// System.Single UnityEngine.Color::b
	float ___b_2;
	// System.Single UnityEngine.Color::a
	float ___a_3;

public:
	inline static int32_t get_offset_of_r_0() { return static_cast<int32_t>(offsetof(Color_t2020392075, ___r_0)); }
	inline float get_r_0() const { return ___r_0; }
	inline float* get_address_of_r_0() { return &___r_0; }
	inline void set_r_0(float value)
	{
		___r_0 = value;
	}

	inline static int32_t get_offset_of_g_1() { return static_cast<int32_t>(offsetof(Color_t2020392075, ___g_1)); }
	inline float get_g_1() const { return ___g_1; }
	inline float* get_address_of_g_1() { return &___g_1; }
	inline void set_g_1(float value)
	{
		___g_1 = value;
	}

	inline static int32_t get_offset_of_b_2() { return static_cast<int32_t>(offsetof(Color_t2020392075, ___b_2)); }
	inline float get_b_2() const { return ___b_2; }
	inline float* get_address_of_b_2() { return &___b_2; }
	inline void set_b_2(float value)
	{
		___b_2 = value;
	}

	inline static int32_t get_offset_of_a_3() { return static_cast<int32_t>(offsetof(Color_t2020392075, ___a_3)); }
	inline float get_a_3() const { return ___a_3; }
	inline float* get_address_of_a_3() { return &___a_3; }
	inline void set_a_3(float value)
	{
		___a_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLOR_T2020392075_H
#ifndef SINGLE_T2076509932_H
#define SINGLE_T2076509932_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Single
struct  Single_t2076509932 
{
public:
	// System.Single System.Single::m_value
	float ___m_value_7;

public:
	inline static int32_t get_offset_of_m_value_7() { return static_cast<int32_t>(offsetof(Single_t2076509932, ___m_value_7)); }
	inline float get_m_value_7() const { return ___m_value_7; }
	inline float* get_address_of_m_value_7() { return &___m_value_7; }
	inline void set_m_value_7(float value)
	{
		___m_value_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SINGLE_T2076509932_H
#ifndef FORCEOVERLIFETIMEMODULE_T3255358877_H
#define FORCEOVERLIFETIMEMODULE_T3255358877_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.ParticleSystem/ForceOverLifetimeModule
struct  ForceOverLifetimeModule_t3255358877 
{
public:
	// UnityEngine.ParticleSystem UnityEngine.ParticleSystem/ForceOverLifetimeModule::m_ParticleSystem
	ParticleSystem_t3394631041 * ___m_ParticleSystem_0;

public:
	inline static int32_t get_offset_of_m_ParticleSystem_0() { return static_cast<int32_t>(offsetof(ForceOverLifetimeModule_t3255358877, ___m_ParticleSystem_0)); }
	inline ParticleSystem_t3394631041 * get_m_ParticleSystem_0() const { return ___m_ParticleSystem_0; }
	inline ParticleSystem_t3394631041 ** get_address_of_m_ParticleSystem_0() { return &___m_ParticleSystem_0; }
	inline void set_m_ParticleSystem_0(ParticleSystem_t3394631041 * value)
	{
		___m_ParticleSystem_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_ParticleSystem_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.ParticleSystem/ForceOverLifetimeModule
struct ForceOverLifetimeModule_t3255358877_marshaled_pinvoke
{
	ParticleSystem_t3394631041 * ___m_ParticleSystem_0;
};
// Native definition for COM marshalling of UnityEngine.ParticleSystem/ForceOverLifetimeModule
struct ForceOverLifetimeModule_t3255358877_marshaled_com
{
	ParticleSystem_t3394631041 * ___m_ParticleSystem_0;
};
#endif // FORCEOVERLIFETIMEMODULE_T3255358877_H
#ifndef VELOCITYOVERLIFETIMEMODULE_T2506587731_H
#define VELOCITYOVERLIFETIMEMODULE_T2506587731_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.ParticleSystem/VelocityOverLifetimeModule
struct  VelocityOverLifetimeModule_t2506587731 
{
public:
	// UnityEngine.ParticleSystem UnityEngine.ParticleSystem/VelocityOverLifetimeModule::m_ParticleSystem
	ParticleSystem_t3394631041 * ___m_ParticleSystem_0;

public:
	inline static int32_t get_offset_of_m_ParticleSystem_0() { return static_cast<int32_t>(offsetof(VelocityOverLifetimeModule_t2506587731, ___m_ParticleSystem_0)); }
	inline ParticleSystem_t3394631041 * get_m_ParticleSystem_0() const { return ___m_ParticleSystem_0; }
	inline ParticleSystem_t3394631041 ** get_address_of_m_ParticleSystem_0() { return &___m_ParticleSystem_0; }
	inline void set_m_ParticleSystem_0(ParticleSystem_t3394631041 * value)
	{
		___m_ParticleSystem_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_ParticleSystem_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.ParticleSystem/VelocityOverLifetimeModule
struct VelocityOverLifetimeModule_t2506587731_marshaled_pinvoke
{
	ParticleSystem_t3394631041 * ___m_ParticleSystem_0;
};
// Native definition for COM marshalling of UnityEngine.ParticleSystem/VelocityOverLifetimeModule
struct VelocityOverLifetimeModule_t2506587731_marshaled_com
{
	ParticleSystem_t3394631041 * ___m_ParticleSystem_0;
};
#endif // VELOCITYOVERLIFETIMEMODULE_T2506587731_H
#ifndef VOID_T1841601450_H
#define VOID_T1841601450_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Void
struct  Void_t1841601450 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VOID_T1841601450_H
#ifndef MAINMODULE_T6751348_H
#define MAINMODULE_T6751348_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.ParticleSystem/MainModule
struct  MainModule_t6751348 
{
public:
	// UnityEngine.ParticleSystem UnityEngine.ParticleSystem/MainModule::m_ParticleSystem
	ParticleSystem_t3394631041 * ___m_ParticleSystem_0;

public:
	inline static int32_t get_offset_of_m_ParticleSystem_0() { return static_cast<int32_t>(offsetof(MainModule_t6751348, ___m_ParticleSystem_0)); }
	inline ParticleSystem_t3394631041 * get_m_ParticleSystem_0() const { return ___m_ParticleSystem_0; }
	inline ParticleSystem_t3394631041 ** get_address_of_m_ParticleSystem_0() { return &___m_ParticleSystem_0; }
	inline void set_m_ParticleSystem_0(ParticleSystem_t3394631041 * value)
	{
		___m_ParticleSystem_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_ParticleSystem_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.ParticleSystem/MainModule
struct MainModule_t6751348_marshaled_pinvoke
{
	ParticleSystem_t3394631041 * ___m_ParticleSystem_0;
};
// Native definition for COM marshalling of UnityEngine.ParticleSystem/MainModule
struct MainModule_t6751348_marshaled_com
{
	ParticleSystem_t3394631041 * ___m_ParticleSystem_0;
};
#endif // MAINMODULE_T6751348_H
#ifndef INTPTR_T_H
#define INTPTR_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTPTR_T_H
#ifndef BOOLEAN_T3825574718_H
#define BOOLEAN_T3825574718_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Boolean
struct  Boolean_t3825574718 
{
public:
	// System.Boolean System.Boolean::m_value
	bool ___m_value_2;

public:
	inline static int32_t get_offset_of_m_value_2() { return static_cast<int32_t>(offsetof(Boolean_t3825574718, ___m_value_2)); }
	inline bool get_m_value_2() const { return ___m_value_2; }
	inline bool* get_address_of_m_value_2() { return &___m_value_2; }
	inline void set_m_value_2(bool value)
	{
		___m_value_2 = value;
	}
};

struct Boolean_t3825574718_StaticFields
{
public:
	// System.String System.Boolean::FalseString
	String_t* ___FalseString_0;
	// System.String System.Boolean::TrueString
	String_t* ___TrueString_1;

public:
	inline static int32_t get_offset_of_FalseString_0() { return static_cast<int32_t>(offsetof(Boolean_t3825574718_StaticFields, ___FalseString_0)); }
	inline String_t* get_FalseString_0() const { return ___FalseString_0; }
	inline String_t** get_address_of_FalseString_0() { return &___FalseString_0; }
	inline void set_FalseString_0(String_t* value)
	{
		___FalseString_0 = value;
		Il2CppCodeGenWriteBarrier((&___FalseString_0), value);
	}

	inline static int32_t get_offset_of_TrueString_1() { return static_cast<int32_t>(offsetof(Boolean_t3825574718_StaticFields, ___TrueString_1)); }
	inline String_t* get_TrueString_1() const { return ___TrueString_1; }
	inline String_t** get_address_of_TrueString_1() { return &___TrueString_1; }
	inline void set_TrueString_1(String_t* value)
	{
		___TrueString_1 = value;
		Il2CppCodeGenWriteBarrier((&___TrueString_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BOOLEAN_T3825574718_H
#ifndef GRADIENT_T3600583008_H
#define GRADIENT_T3600583008_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Gradient
struct  Gradient_t3600583008  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Gradient::m_Ptr
	intptr_t ___m_Ptr_0;

public:
	inline static int32_t get_offset_of_m_Ptr_0() { return static_cast<int32_t>(offsetof(Gradient_t3600583008, ___m_Ptr_0)); }
	inline intptr_t get_m_Ptr_0() const { return ___m_Ptr_0; }
	inline intptr_t* get_address_of_m_Ptr_0() { return &___m_Ptr_0; }
	inline void set_m_Ptr_0(intptr_t value)
	{
		___m_Ptr_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.Gradient
struct Gradient_t3600583008_marshaled_pinvoke
{
	intptr_t ___m_Ptr_0;
};
// Native definition for COM marshalling of UnityEngine.Gradient
struct Gradient_t3600583008_marshaled_com
{
	intptr_t ___m_Ptr_0;
};
#endif // GRADIENT_T3600583008_H
#ifndef PARTICLESYSTEMRENDERMODE_T1131601630_H
#define PARTICLESYSTEMRENDERMODE_T1131601630_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.ParticleSystemRenderMode
struct  ParticleSystemRenderMode_t1131601630 
{
public:
	// System.Int32 UnityEngine.ParticleSystemRenderMode::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(ParticleSystemRenderMode_t1131601630, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PARTICLESYSTEMRENDERMODE_T1131601630_H
#ifndef PARTICLESYSTEMGRADIENTMODE_T4205168402_H
#define PARTICLESYSTEMGRADIENTMODE_T4205168402_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.ParticleSystemGradientMode
struct  ParticleSystemGradientMode_t4205168402 
{
public:
	// System.Int32 UnityEngine.ParticleSystemGradientMode::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(ParticleSystemGradientMode_t4205168402, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PARTICLESYSTEMGRADIENTMODE_T4205168402_H
#ifndef PARTICLESYSTEMSIMULATIONSPACE_T3554150940_H
#define PARTICLESYSTEMSIMULATIONSPACE_T3554150940_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.ParticleSystemSimulationSpace
struct  ParticleSystemSimulationSpace_t3554150940 
{
public:
	// System.Int32 UnityEngine.ParticleSystemSimulationSpace::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(ParticleSystemSimulationSpace_t3554150940, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PARTICLESYSTEMSIMULATIONSPACE_T3554150940_H
#ifndef OBJECT_T1021602117_H
#define OBJECT_T1021602117_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Object
struct  Object_t1021602117  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Object::m_CachedPtr
	intptr_t ___m_CachedPtr_0;

public:
	inline static int32_t get_offset_of_m_CachedPtr_0() { return static_cast<int32_t>(offsetof(Object_t1021602117, ___m_CachedPtr_0)); }
	inline intptr_t get_m_CachedPtr_0() const { return ___m_CachedPtr_0; }
	inline intptr_t* get_address_of_m_CachedPtr_0() { return &___m_CachedPtr_0; }
	inline void set_m_CachedPtr_0(intptr_t value)
	{
		___m_CachedPtr_0 = value;
	}
};

struct Object_t1021602117_StaticFields
{
public:
	// System.Int32 UnityEngine.Object::OffsetOfInstanceIDInCPlusPlusObject
	int32_t ___OffsetOfInstanceIDInCPlusPlusObject_1;

public:
	inline static int32_t get_offset_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return static_cast<int32_t>(offsetof(Object_t1021602117_StaticFields, ___OffsetOfInstanceIDInCPlusPlusObject_1)); }
	inline int32_t get_OffsetOfInstanceIDInCPlusPlusObject_1() const { return ___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline int32_t* get_address_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return &___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline void set_OffsetOfInstanceIDInCPlusPlusObject_1(int32_t value)
	{
		___OffsetOfInstanceIDInCPlusPlusObject_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.Object
struct Object_t1021602117_marshaled_pinvoke
{
	intptr_t ___m_CachedPtr_0;
};
// Native definition for COM marshalling of UnityEngine.Object
struct Object_t1021602117_marshaled_com
{
	intptr_t ___m_CachedPtr_0;
};
#endif // OBJECT_T1021602117_H
#ifndef PARTICLESYSTEMSCALINGMODE_T366697231_H
#define PARTICLESYSTEMSCALINGMODE_T366697231_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.ParticleSystemScalingMode
struct  ParticleSystemScalingMode_t366697231 
{
public:
	// System.Int32 UnityEngine.ParticleSystemScalingMode::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(ParticleSystemScalingMode_t366697231, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PARTICLESYSTEMSCALINGMODE_T366697231_H
#ifndef ANIMATIONCURVE_T3306541151_H
#define ANIMATIONCURVE_T3306541151_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.AnimationCurve
struct  AnimationCurve_t3306541151  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.AnimationCurve::m_Ptr
	intptr_t ___m_Ptr_0;

public:
	inline static int32_t get_offset_of_m_Ptr_0() { return static_cast<int32_t>(offsetof(AnimationCurve_t3306541151, ___m_Ptr_0)); }
	inline intptr_t get_m_Ptr_0() const { return ___m_Ptr_0; }
	inline intptr_t* get_address_of_m_Ptr_0() { return &___m_Ptr_0; }
	inline void set_m_Ptr_0(intptr_t value)
	{
		___m_Ptr_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.AnimationCurve
struct AnimationCurve_t3306541151_marshaled_pinvoke
{
	intptr_t ___m_Ptr_0;
};
// Native definition for COM marshalling of UnityEngine.AnimationCurve
struct AnimationCurve_t3306541151_marshaled_com
{
	intptr_t ___m_Ptr_0;
};
#endif // ANIMATIONCURVE_T3306541151_H
#ifndef PARTICLESYSTEMCURVEMODE_T1659312557_H
#define PARTICLESYSTEMCURVEMODE_T1659312557_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.ParticleSystemCurveMode
struct  ParticleSystemCurveMode_t1659312557 
{
public:
	// System.Int32 UnityEngine.ParticleSystemCurveMode::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(ParticleSystemCurveMode_t1659312557, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PARTICLESYSTEMCURVEMODE_T1659312557_H
#ifndef MINMAXGRADIENT_T3708298175_H
#define MINMAXGRADIENT_T3708298175_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.ParticleSystem/MinMaxGradient
struct  MinMaxGradient_t3708298175 
{
public:
	// UnityEngine.ParticleSystemGradientMode UnityEngine.ParticleSystem/MinMaxGradient::m_Mode
	int32_t ___m_Mode_0;
	// UnityEngine.Gradient UnityEngine.ParticleSystem/MinMaxGradient::m_GradientMin
	Gradient_t3600583008 * ___m_GradientMin_1;
	// UnityEngine.Gradient UnityEngine.ParticleSystem/MinMaxGradient::m_GradientMax
	Gradient_t3600583008 * ___m_GradientMax_2;
	// UnityEngine.Color UnityEngine.ParticleSystem/MinMaxGradient::m_ColorMin
	Color_t2020392075  ___m_ColorMin_3;
	// UnityEngine.Color UnityEngine.ParticleSystem/MinMaxGradient::m_ColorMax
	Color_t2020392075  ___m_ColorMax_4;

public:
	inline static int32_t get_offset_of_m_Mode_0() { return static_cast<int32_t>(offsetof(MinMaxGradient_t3708298175, ___m_Mode_0)); }
	inline int32_t get_m_Mode_0() const { return ___m_Mode_0; }
	inline int32_t* get_address_of_m_Mode_0() { return &___m_Mode_0; }
	inline void set_m_Mode_0(int32_t value)
	{
		___m_Mode_0 = value;
	}

	inline static int32_t get_offset_of_m_GradientMin_1() { return static_cast<int32_t>(offsetof(MinMaxGradient_t3708298175, ___m_GradientMin_1)); }
	inline Gradient_t3600583008 * get_m_GradientMin_1() const { return ___m_GradientMin_1; }
	inline Gradient_t3600583008 ** get_address_of_m_GradientMin_1() { return &___m_GradientMin_1; }
	inline void set_m_GradientMin_1(Gradient_t3600583008 * value)
	{
		___m_GradientMin_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_GradientMin_1), value);
	}

	inline static int32_t get_offset_of_m_GradientMax_2() { return static_cast<int32_t>(offsetof(MinMaxGradient_t3708298175, ___m_GradientMax_2)); }
	inline Gradient_t3600583008 * get_m_GradientMax_2() const { return ___m_GradientMax_2; }
	inline Gradient_t3600583008 ** get_address_of_m_GradientMax_2() { return &___m_GradientMax_2; }
	inline void set_m_GradientMax_2(Gradient_t3600583008 * value)
	{
		___m_GradientMax_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_GradientMax_2), value);
	}

	inline static int32_t get_offset_of_m_ColorMin_3() { return static_cast<int32_t>(offsetof(MinMaxGradient_t3708298175, ___m_ColorMin_3)); }
	inline Color_t2020392075  get_m_ColorMin_3() const { return ___m_ColorMin_3; }
	inline Color_t2020392075 * get_address_of_m_ColorMin_3() { return &___m_ColorMin_3; }
	inline void set_m_ColorMin_3(Color_t2020392075  value)
	{
		___m_ColorMin_3 = value;
	}

	inline static int32_t get_offset_of_m_ColorMax_4() { return static_cast<int32_t>(offsetof(MinMaxGradient_t3708298175, ___m_ColorMax_4)); }
	inline Color_t2020392075  get_m_ColorMax_4() const { return ___m_ColorMax_4; }
	inline Color_t2020392075 * get_address_of_m_ColorMax_4() { return &___m_ColorMax_4; }
	inline void set_m_ColorMax_4(Color_t2020392075  value)
	{
		___m_ColorMax_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.ParticleSystem/MinMaxGradient
struct MinMaxGradient_t3708298175_marshaled_pinvoke
{
	int32_t ___m_Mode_0;
	Gradient_t3600583008_marshaled_pinvoke ___m_GradientMin_1;
	Gradient_t3600583008_marshaled_pinvoke ___m_GradientMax_2;
	Color_t2020392075  ___m_ColorMin_3;
	Color_t2020392075  ___m_ColorMax_4;
};
// Native definition for COM marshalling of UnityEngine.ParticleSystem/MinMaxGradient
struct MinMaxGradient_t3708298175_marshaled_com
{
	int32_t ___m_Mode_0;
	Gradient_t3600583008_marshaled_com* ___m_GradientMin_1;
	Gradient_t3600583008_marshaled_com* ___m_GradientMax_2;
	Color_t2020392075  ___m_ColorMin_3;
	Color_t2020392075  ___m_ColorMax_4;
};
#endif // MINMAXGRADIENT_T3708298175_H
#ifndef COMPONENT_T3819376471_H
#define COMPONENT_T3819376471_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Component
struct  Component_t3819376471  : public Object_t1021602117
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPONENT_T3819376471_H
#ifndef MINMAXCURVE_T122563058_H
#define MINMAXCURVE_T122563058_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.ParticleSystem/MinMaxCurve
struct  MinMaxCurve_t122563058 
{
public:
	// UnityEngine.ParticleSystemCurveMode UnityEngine.ParticleSystem/MinMaxCurve::m_Mode
	int32_t ___m_Mode_0;
	// System.Single UnityEngine.ParticleSystem/MinMaxCurve::m_CurveMultiplier
	float ___m_CurveMultiplier_1;
	// UnityEngine.AnimationCurve UnityEngine.ParticleSystem/MinMaxCurve::m_CurveMin
	AnimationCurve_t3306541151 * ___m_CurveMin_2;
	// UnityEngine.AnimationCurve UnityEngine.ParticleSystem/MinMaxCurve::m_CurveMax
	AnimationCurve_t3306541151 * ___m_CurveMax_3;
	// System.Single UnityEngine.ParticleSystem/MinMaxCurve::m_ConstantMin
	float ___m_ConstantMin_4;
	// System.Single UnityEngine.ParticleSystem/MinMaxCurve::m_ConstantMax
	float ___m_ConstantMax_5;

public:
	inline static int32_t get_offset_of_m_Mode_0() { return static_cast<int32_t>(offsetof(MinMaxCurve_t122563058, ___m_Mode_0)); }
	inline int32_t get_m_Mode_0() const { return ___m_Mode_0; }
	inline int32_t* get_address_of_m_Mode_0() { return &___m_Mode_0; }
	inline void set_m_Mode_0(int32_t value)
	{
		___m_Mode_0 = value;
	}

	inline static int32_t get_offset_of_m_CurveMultiplier_1() { return static_cast<int32_t>(offsetof(MinMaxCurve_t122563058, ___m_CurveMultiplier_1)); }
	inline float get_m_CurveMultiplier_1() const { return ___m_CurveMultiplier_1; }
	inline float* get_address_of_m_CurveMultiplier_1() { return &___m_CurveMultiplier_1; }
	inline void set_m_CurveMultiplier_1(float value)
	{
		___m_CurveMultiplier_1 = value;
	}

	inline static int32_t get_offset_of_m_CurveMin_2() { return static_cast<int32_t>(offsetof(MinMaxCurve_t122563058, ___m_CurveMin_2)); }
	inline AnimationCurve_t3306541151 * get_m_CurveMin_2() const { return ___m_CurveMin_2; }
	inline AnimationCurve_t3306541151 ** get_address_of_m_CurveMin_2() { return &___m_CurveMin_2; }
	inline void set_m_CurveMin_2(AnimationCurve_t3306541151 * value)
	{
		___m_CurveMin_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_CurveMin_2), value);
	}

	inline static int32_t get_offset_of_m_CurveMax_3() { return static_cast<int32_t>(offsetof(MinMaxCurve_t122563058, ___m_CurveMax_3)); }
	inline AnimationCurve_t3306541151 * get_m_CurveMax_3() const { return ___m_CurveMax_3; }
	inline AnimationCurve_t3306541151 ** get_address_of_m_CurveMax_3() { return &___m_CurveMax_3; }
	inline void set_m_CurveMax_3(AnimationCurve_t3306541151 * value)
	{
		___m_CurveMax_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_CurveMax_3), value);
	}

	inline static int32_t get_offset_of_m_ConstantMin_4() { return static_cast<int32_t>(offsetof(MinMaxCurve_t122563058, ___m_ConstantMin_4)); }
	inline float get_m_ConstantMin_4() const { return ___m_ConstantMin_4; }
	inline float* get_address_of_m_ConstantMin_4() { return &___m_ConstantMin_4; }
	inline void set_m_ConstantMin_4(float value)
	{
		___m_ConstantMin_4 = value;
	}

	inline static int32_t get_offset_of_m_ConstantMax_5() { return static_cast<int32_t>(offsetof(MinMaxCurve_t122563058, ___m_ConstantMax_5)); }
	inline float get_m_ConstantMax_5() const { return ___m_ConstantMax_5; }
	inline float* get_address_of_m_ConstantMax_5() { return &___m_ConstantMax_5; }
	inline void set_m_ConstantMax_5(float value)
	{
		___m_ConstantMax_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.ParticleSystem/MinMaxCurve
struct MinMaxCurve_t122563058_marshaled_pinvoke
{
	int32_t ___m_Mode_0;
	float ___m_CurveMultiplier_1;
	AnimationCurve_t3306541151_marshaled_pinvoke ___m_CurveMin_2;
	AnimationCurve_t3306541151_marshaled_pinvoke ___m_CurveMax_3;
	float ___m_ConstantMin_4;
	float ___m_ConstantMax_5;
};
// Native definition for COM marshalling of UnityEngine.ParticleSystem/MinMaxCurve
struct MinMaxCurve_t122563058_marshaled_com
{
	int32_t ___m_Mode_0;
	float ___m_CurveMultiplier_1;
	AnimationCurve_t3306541151_marshaled_com* ___m_CurveMin_2;
	AnimationCurve_t3306541151_marshaled_com* ___m_CurveMax_3;
	float ___m_ConstantMin_4;
	float ___m_ConstantMax_5;
};
#endif // MINMAXCURVE_T122563058_H
#ifndef PARTICLESYSTEM_T3394631041_H
#define PARTICLESYSTEM_T3394631041_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.ParticleSystem
struct  ParticleSystem_t3394631041  : public Component_t3819376471
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PARTICLESYSTEM_T3394631041_H
#ifndef RENDERER_T257310565_H
#define RENDERER_T257310565_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Renderer
struct  Renderer_t257310565  : public Component_t3819376471
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RENDERER_T257310565_H
#ifndef PARTICLESYSTEMRENDERER_T892265570_H
#define PARTICLESYSTEMRENDERER_T892265570_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.ParticleSystemRenderer
struct  ParticleSystemRenderer_t892265570  : public Renderer_t257310565
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PARTICLESYSTEMRENDERER_T892265570_H

extern "C" void AnimationCurve_t3306541151_marshal_pinvoke(const AnimationCurve_t3306541151& unmarshaled, AnimationCurve_t3306541151_marshaled_pinvoke& marshaled);
extern "C" void AnimationCurve_t3306541151_marshal_pinvoke_back(const AnimationCurve_t3306541151_marshaled_pinvoke& marshaled, AnimationCurve_t3306541151& unmarshaled);
extern "C" void AnimationCurve_t3306541151_marshal_pinvoke_cleanup(AnimationCurve_t3306541151_marshaled_pinvoke& marshaled);
extern "C" void AnimationCurve_t3306541151_marshal_com(const AnimationCurve_t3306541151& unmarshaled, AnimationCurve_t3306541151_marshaled_com& marshaled);
extern "C" void AnimationCurve_t3306541151_marshal_com_back(const AnimationCurve_t3306541151_marshaled_com& marshaled, AnimationCurve_t3306541151& unmarshaled);
extern "C" void AnimationCurve_t3306541151_marshal_com_cleanup(AnimationCurve_t3306541151_marshaled_com& marshaled);
extern "C" void Gradient_t3600583008_marshal_pinvoke(const Gradient_t3600583008& unmarshaled, Gradient_t3600583008_marshaled_pinvoke& marshaled);
extern "C" void Gradient_t3600583008_marshal_pinvoke_back(const Gradient_t3600583008_marshaled_pinvoke& marshaled, Gradient_t3600583008& unmarshaled);
extern "C" void Gradient_t3600583008_marshal_pinvoke_cleanup(Gradient_t3600583008_marshaled_pinvoke& marshaled);
extern "C" void Gradient_t3600583008_marshal_com(const Gradient_t3600583008& unmarshaled, Gradient_t3600583008_marshaled_com& marshaled);
extern "C" void Gradient_t3600583008_marshal_com_back(const Gradient_t3600583008_marshaled_com& marshaled, Gradient_t3600583008& unmarshaled);
extern "C" void Gradient_t3600583008_marshal_com_cleanup(Gradient_t3600583008_marshaled_com& marshaled);


// System.Void UnityEngine.ParticleSystem/MainModule::.ctor(UnityEngine.ParticleSystem)
extern "C"  void MainModule__ctor_m724825857 (MainModule_t6751348 * __this, ParticleSystem_t3394631041 * ___particleSystem0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.ParticleSystem/VelocityOverLifetimeModule::.ctor(UnityEngine.ParticleSystem)
extern "C"  void VelocityOverLifetimeModule__ctor_m4227621638 (VelocityOverLifetimeModule_t2506587731 * __this, ParticleSystem_t3394631041 * ___particleSystem0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.ParticleSystem/ForceOverLifetimeModule::.ctor(UnityEngine.ParticleSystem)
extern "C"  void ForceOverLifetimeModule__ctor_m3383247588 (ForceOverLifetimeModule_t3255358877 * __this, ParticleSystem_t3394631041 * ___particleSystem0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.ParticleSystem/ForceOverLifetimeModule::GetEnabled(UnityEngine.ParticleSystem)
extern "C"  bool ForceOverLifetimeModule_GetEnabled_m1888273613 (RuntimeObject * __this /* static, unused */, ParticleSystem_t3394631041 * ___system0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.ParticleSystem/ForceOverLifetimeModule::get_enabled()
extern "C"  bool ForceOverLifetimeModule_get_enabled_m3992229176 (ForceOverLifetimeModule_t3255358877 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.ParticleSystem/ForceOverLifetimeModule::SetWorldSpace(UnityEngine.ParticleSystem,System.Boolean)
extern "C"  void ForceOverLifetimeModule_SetWorldSpace_m3704240577 (RuntimeObject * __this /* static, unused */, ParticleSystem_t3394631041 * ___system0, bool ___value1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.ParticleSystem/ForceOverLifetimeModule::set_space(UnityEngine.ParticleSystemSimulationSpace)
extern "C"  void ForceOverLifetimeModule_set_space_m297890352 (ForceOverLifetimeModule_t3255358877 * __this, int32_t ___value0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.ParticleSystem/MainModule::SetStartColor(UnityEngine.ParticleSystem,UnityEngine.ParticleSystem/MinMaxGradient&)
extern "C"  void MainModule_SetStartColor_m2492347281 (RuntimeObject * __this /* static, unused */, ParticleSystem_t3394631041 * ___system0, MinMaxGradient_t3708298175 * ___gradient1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.ParticleSystem/MainModule::set_startColor(UnityEngine.ParticleSystem/MinMaxGradient)
extern "C"  void MainModule_set_startColor_m3844897932 (MainModule_t6751348 * __this, MinMaxGradient_t3708298175  ___value0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.ParticleSystem/MainModule::GetStartColor(UnityEngine.ParticleSystem,UnityEngine.ParticleSystem/MinMaxGradient&)
extern "C"  void MainModule_GetStartColor_m2015758365 (RuntimeObject * __this /* static, unused */, ParticleSystem_t3394631041 * ___system0, MinMaxGradient_t3708298175 * ___gradient1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.ParticleSystem/MinMaxGradient UnityEngine.ParticleSystem/MainModule::get_startColor()
extern "C"  MinMaxGradient_t3708298175  MainModule_get_startColor_m642758877 (MainModule_t6751348 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.ParticleSystem/MainModule::SetGravityModifier(UnityEngine.ParticleSystem,UnityEngine.ParticleSystem/MinMaxCurve&)
extern "C"  void MainModule_SetGravityModifier_m2001849420 (RuntimeObject * __this /* static, unused */, ParticleSystem_t3394631041 * ___system0, MinMaxCurve_t122563058 * ___curve1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.ParticleSystem/MainModule::set_gravityModifier(UnityEngine.ParticleSystem/MinMaxCurve)
extern "C"  void MainModule_set_gravityModifier_m676251203 (MainModule_t6751348 * __this, MinMaxCurve_t122563058  ___value0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.ParticleSystem/MainModule::GetGravityModifier(UnityEngine.ParticleSystem,UnityEngine.ParticleSystem/MinMaxCurve&)
extern "C"  void MainModule_GetGravityModifier_m4089386048 (RuntimeObject * __this /* static, unused */, ParticleSystem_t3394631041 * ___system0, MinMaxCurve_t122563058 * ___curve1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.ParticleSystem/MinMaxCurve UnityEngine.ParticleSystem/MainModule::get_gravityModifier()
extern "C"  MinMaxCurve_t122563058  MainModule_get_gravityModifier_m483350030 (MainModule_t6751348 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.ParticleSystem/MainModule::SetScalingMode(UnityEngine.ParticleSystem,UnityEngine.ParticleSystemScalingMode)
extern "C"  void MainModule_SetScalingMode_m10537077 (RuntimeObject * __this /* static, unused */, ParticleSystem_t3394631041 * ___system0, int32_t ___value1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.ParticleSystem/MainModule::set_scalingMode(UnityEngine.ParticleSystemScalingMode)
extern "C"  void MainModule_set_scalingMode_m595996162 (MainModule_t6751348 * __this, int32_t ___value0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.AnimationCurve::.ctor()
extern "C"  void AnimationCurve__ctor_m3808882547 (AnimationCurve_t3306541151 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.ParticleSystem/MinMaxCurve::.ctor(System.Single)
extern "C"  void MinMaxCurve__ctor_m3111284754 (MinMaxCurve_t122563058 * __this, float ___constant0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.ParticleSystem/MinMaxCurve::get_constant()
extern "C"  float MinMaxCurve_get_constant_m2300889686 (MinMaxCurve_t122563058 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Gradient::.ctor()
extern "C"  void Gradient__ctor_m1008579686 (Gradient_t3600583008 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Color UnityEngine.Color::get_black()
extern "C"  Color_t2020392075  Color_get_black_m455352838 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.ParticleSystem/MinMaxGradient::.ctor(UnityEngine.Color)
extern "C"  void MinMaxGradient__ctor_m2960432810 (MinMaxGradient_t3708298175 * __this, Color_t2020392075  ___color0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Color UnityEngine.ParticleSystem/MinMaxGradient::get_color()
extern "C"  Color_t2020392075  MinMaxGradient_get_color_m1448527895 (MinMaxGradient_t3708298175 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.ParticleSystem/VelocityOverLifetimeModule::GetEnabled(UnityEngine.ParticleSystem)
extern "C"  bool VelocityOverLifetimeModule_GetEnabled_m1730484311 (RuntimeObject * __this /* static, unused */, ParticleSystem_t3394631041 * ___system0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.ParticleSystem/VelocityOverLifetimeModule::get_enabled()
extern "C"  bool VelocityOverLifetimeModule_get_enabled_m230085458 (VelocityOverLifetimeModule_t2506587731 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.ParticleSystem/VelocityOverLifetimeModule::SetWorldSpace(UnityEngine.ParticleSystem,System.Boolean)
extern "C"  void VelocityOverLifetimeModule_SetWorldSpace_m1687038599 (RuntimeObject * __this /* static, unused */, ParticleSystem_t3394631041 * ___system0, bool ___value1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.ParticleSystem/VelocityOverLifetimeModule::set_space(UnityEngine.ParticleSystemSimulationSpace)
extern "C"  void VelocityOverLifetimeModule_set_space_m1646074934 (VelocityOverLifetimeModule_t2506587731 * __this, int32_t ___value0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Boolean UnityEngine.ParticleSystem::get_isPlaying()
extern "C"  bool ParticleSystem_get_isPlaying_m312053940 (ParticleSystem_t3394631041 * __this, const RuntimeMethod* method)
{
	typedef bool (*ParticleSystem_get_isPlaying_m312053940_ftn) (ParticleSystem_t3394631041 *);
	static ParticleSystem_get_isPlaying_m312053940_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (ParticleSystem_get_isPlaying_m312053940_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.ParticleSystem::get_isPlaying()");
	bool retVal = _il2cpp_icall_func(__this);
	return retVal;
}
// UnityEngine.ParticleSystem/MainModule UnityEngine.ParticleSystem::get_main()
extern "C"  MainModule_t6751348  ParticleSystem_get_main_m2275307502 (ParticleSystem_t3394631041 * __this, const RuntimeMethod* method)
{
	MainModule_t6751348  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		MainModule_t6751348  L_0;
		memset(&L_0, 0, sizeof(L_0));
		MainModule__ctor_m724825857((&L_0), __this, /*hidden argument*/NULL);
		V_0 = L_0;
		goto IL_000d;
	}

IL_000d:
	{
		MainModule_t6751348  L_1 = V_0;
		return L_1;
	}
}
// UnityEngine.ParticleSystem/VelocityOverLifetimeModule UnityEngine.ParticleSystem::get_velocityOverLifetime()
extern "C"  VelocityOverLifetimeModule_t2506587731  ParticleSystem_get_velocityOverLifetime_m384692328 (ParticleSystem_t3394631041 * __this, const RuntimeMethod* method)
{
	VelocityOverLifetimeModule_t2506587731  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		VelocityOverLifetimeModule_t2506587731  L_0;
		memset(&L_0, 0, sizeof(L_0));
		VelocityOverLifetimeModule__ctor_m4227621638((&L_0), __this, /*hidden argument*/NULL);
		V_0 = L_0;
		goto IL_000d;
	}

IL_000d:
	{
		VelocityOverLifetimeModule_t2506587731  L_1 = V_0;
		return L_1;
	}
}
// UnityEngine.ParticleSystem/ForceOverLifetimeModule UnityEngine.ParticleSystem::get_forceOverLifetime()
extern "C"  ForceOverLifetimeModule_t3255358877  ParticleSystem_get_forceOverLifetime_m3333174386 (ParticleSystem_t3394631041 * __this, const RuntimeMethod* method)
{
	ForceOverLifetimeModule_t3255358877  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		ForceOverLifetimeModule_t3255358877  L_0;
		memset(&L_0, 0, sizeof(L_0));
		ForceOverLifetimeModule__ctor_m3383247588((&L_0), __this, /*hidden argument*/NULL);
		V_0 = L_0;
		goto IL_000d;
	}

IL_000d:
	{
		ForceOverLifetimeModule_t3255358877  L_1 = V_0;
		return L_1;
	}
}
// Conversion methods for marshalling of: UnityEngine.ParticleSystem/ForceOverLifetimeModule
extern "C" void ForceOverLifetimeModule_t3255358877_marshal_pinvoke(const ForceOverLifetimeModule_t3255358877& unmarshaled, ForceOverLifetimeModule_t3255358877_marshaled_pinvoke& marshaled)
{
	Il2CppCodeGenException* ___m_ParticleSystem_0Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'm_ParticleSystem' of type 'ForceOverLifetimeModule': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___m_ParticleSystem_0Exception);
}
extern "C" void ForceOverLifetimeModule_t3255358877_marshal_pinvoke_back(const ForceOverLifetimeModule_t3255358877_marshaled_pinvoke& marshaled, ForceOverLifetimeModule_t3255358877& unmarshaled)
{
	Il2CppCodeGenException* ___m_ParticleSystem_0Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'm_ParticleSystem' of type 'ForceOverLifetimeModule': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___m_ParticleSystem_0Exception);
}
// Conversion method for clean up from marshalling of: UnityEngine.ParticleSystem/ForceOverLifetimeModule
extern "C" void ForceOverLifetimeModule_t3255358877_marshal_pinvoke_cleanup(ForceOverLifetimeModule_t3255358877_marshaled_pinvoke& marshaled)
{
}
// Conversion methods for marshalling of: UnityEngine.ParticleSystem/ForceOverLifetimeModule
extern "C" void ForceOverLifetimeModule_t3255358877_marshal_com(const ForceOverLifetimeModule_t3255358877& unmarshaled, ForceOverLifetimeModule_t3255358877_marshaled_com& marshaled)
{
	Il2CppCodeGenException* ___m_ParticleSystem_0Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'm_ParticleSystem' of type 'ForceOverLifetimeModule': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___m_ParticleSystem_0Exception);
}
extern "C" void ForceOverLifetimeModule_t3255358877_marshal_com_back(const ForceOverLifetimeModule_t3255358877_marshaled_com& marshaled, ForceOverLifetimeModule_t3255358877& unmarshaled)
{
	Il2CppCodeGenException* ___m_ParticleSystem_0Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'm_ParticleSystem' of type 'ForceOverLifetimeModule': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___m_ParticleSystem_0Exception);
}
// Conversion method for clean up from marshalling of: UnityEngine.ParticleSystem/ForceOverLifetimeModule
extern "C" void ForceOverLifetimeModule_t3255358877_marshal_com_cleanup(ForceOverLifetimeModule_t3255358877_marshaled_com& marshaled)
{
}
// System.Void UnityEngine.ParticleSystem/ForceOverLifetimeModule::.ctor(UnityEngine.ParticleSystem)
extern "C"  void ForceOverLifetimeModule__ctor_m3383247588 (ForceOverLifetimeModule_t3255358877 * __this, ParticleSystem_t3394631041 * ___particleSystem0, const RuntimeMethod* method)
{
	{
		ParticleSystem_t3394631041 * L_0 = ___particleSystem0;
		__this->set_m_ParticleSystem_0(L_0);
		return;
	}
}
extern "C"  void ForceOverLifetimeModule__ctor_m3383247588_AdjustorThunk (RuntimeObject * __this, ParticleSystem_t3394631041 * ___particleSystem0, const RuntimeMethod* method)
{
	ForceOverLifetimeModule_t3255358877 * _thisAdjusted = reinterpret_cast<ForceOverLifetimeModule_t3255358877 *>(__this + 1);
	ForceOverLifetimeModule__ctor_m3383247588(_thisAdjusted, ___particleSystem0, method);
}
// System.Boolean UnityEngine.ParticleSystem/ForceOverLifetimeModule::get_enabled()
extern "C"  bool ForceOverLifetimeModule_get_enabled_m3992229176 (ForceOverLifetimeModule_t3255358877 * __this, const RuntimeMethod* method)
{
	bool V_0 = false;
	{
		ParticleSystem_t3394631041 * L_0 = __this->get_m_ParticleSystem_0();
		bool L_1 = ForceOverLifetimeModule_GetEnabled_m1888273613(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		goto IL_0012;
	}

IL_0012:
	{
		bool L_2 = V_0;
		return L_2;
	}
}
extern "C"  bool ForceOverLifetimeModule_get_enabled_m3992229176_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	ForceOverLifetimeModule_t3255358877 * _thisAdjusted = reinterpret_cast<ForceOverLifetimeModule_t3255358877 *>(__this + 1);
	return ForceOverLifetimeModule_get_enabled_m3992229176(_thisAdjusted, method);
}
// System.Void UnityEngine.ParticleSystem/ForceOverLifetimeModule::set_space(UnityEngine.ParticleSystemSimulationSpace)
extern "C"  void ForceOverLifetimeModule_set_space_m297890352 (ForceOverLifetimeModule_t3255358877 * __this, int32_t ___value0, const RuntimeMethod* method)
{
	{
		ParticleSystem_t3394631041 * L_0 = __this->get_m_ParticleSystem_0();
		int32_t L_1 = ___value0;
		ForceOverLifetimeModule_SetWorldSpace_m3704240577(NULL /*static, unused*/, L_0, (bool)((((int32_t)L_1) == ((int32_t)1))? 1 : 0), /*hidden argument*/NULL);
		return;
	}
}
extern "C"  void ForceOverLifetimeModule_set_space_m297890352_AdjustorThunk (RuntimeObject * __this, int32_t ___value0, const RuntimeMethod* method)
{
	ForceOverLifetimeModule_t3255358877 * _thisAdjusted = reinterpret_cast<ForceOverLifetimeModule_t3255358877 *>(__this + 1);
	ForceOverLifetimeModule_set_space_m297890352(_thisAdjusted, ___value0, method);
}
// System.Boolean UnityEngine.ParticleSystem/ForceOverLifetimeModule::GetEnabled(UnityEngine.ParticleSystem)
extern "C"  bool ForceOverLifetimeModule_GetEnabled_m1888273613 (RuntimeObject * __this /* static, unused */, ParticleSystem_t3394631041 * ___system0, const RuntimeMethod* method)
{
	typedef bool (*ForceOverLifetimeModule_GetEnabled_m1888273613_ftn) (ParticleSystem_t3394631041 *);
	static ForceOverLifetimeModule_GetEnabled_m1888273613_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (ForceOverLifetimeModule_GetEnabled_m1888273613_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.ParticleSystem/ForceOverLifetimeModule::GetEnabled(UnityEngine.ParticleSystem)");
	bool retVal = _il2cpp_icall_func(___system0);
	return retVal;
}
// System.Void UnityEngine.ParticleSystem/ForceOverLifetimeModule::SetWorldSpace(UnityEngine.ParticleSystem,System.Boolean)
extern "C"  void ForceOverLifetimeModule_SetWorldSpace_m3704240577 (RuntimeObject * __this /* static, unused */, ParticleSystem_t3394631041 * ___system0, bool ___value1, const RuntimeMethod* method)
{
	typedef void (*ForceOverLifetimeModule_SetWorldSpace_m3704240577_ftn) (ParticleSystem_t3394631041 *, bool);
	static ForceOverLifetimeModule_SetWorldSpace_m3704240577_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (ForceOverLifetimeModule_SetWorldSpace_m3704240577_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.ParticleSystem/ForceOverLifetimeModule::SetWorldSpace(UnityEngine.ParticleSystem,System.Boolean)");
	_il2cpp_icall_func(___system0, ___value1);
}
// Conversion methods for marshalling of: UnityEngine.ParticleSystem/MainModule
extern "C" void MainModule_t6751348_marshal_pinvoke(const MainModule_t6751348& unmarshaled, MainModule_t6751348_marshaled_pinvoke& marshaled)
{
	Il2CppCodeGenException* ___m_ParticleSystem_0Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'm_ParticleSystem' of type 'MainModule': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___m_ParticleSystem_0Exception);
}
extern "C" void MainModule_t6751348_marshal_pinvoke_back(const MainModule_t6751348_marshaled_pinvoke& marshaled, MainModule_t6751348& unmarshaled)
{
	Il2CppCodeGenException* ___m_ParticleSystem_0Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'm_ParticleSystem' of type 'MainModule': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___m_ParticleSystem_0Exception);
}
// Conversion method for clean up from marshalling of: UnityEngine.ParticleSystem/MainModule
extern "C" void MainModule_t6751348_marshal_pinvoke_cleanup(MainModule_t6751348_marshaled_pinvoke& marshaled)
{
}
// Conversion methods for marshalling of: UnityEngine.ParticleSystem/MainModule
extern "C" void MainModule_t6751348_marshal_com(const MainModule_t6751348& unmarshaled, MainModule_t6751348_marshaled_com& marshaled)
{
	Il2CppCodeGenException* ___m_ParticleSystem_0Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'm_ParticleSystem' of type 'MainModule': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___m_ParticleSystem_0Exception);
}
extern "C" void MainModule_t6751348_marshal_com_back(const MainModule_t6751348_marshaled_com& marshaled, MainModule_t6751348& unmarshaled)
{
	Il2CppCodeGenException* ___m_ParticleSystem_0Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'm_ParticleSystem' of type 'MainModule': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___m_ParticleSystem_0Exception);
}
// Conversion method for clean up from marshalling of: UnityEngine.ParticleSystem/MainModule
extern "C" void MainModule_t6751348_marshal_com_cleanup(MainModule_t6751348_marshaled_com& marshaled)
{
}
// System.Void UnityEngine.ParticleSystem/MainModule::.ctor(UnityEngine.ParticleSystem)
extern "C"  void MainModule__ctor_m724825857 (MainModule_t6751348 * __this, ParticleSystem_t3394631041 * ___particleSystem0, const RuntimeMethod* method)
{
	{
		ParticleSystem_t3394631041 * L_0 = ___particleSystem0;
		__this->set_m_ParticleSystem_0(L_0);
		return;
	}
}
extern "C"  void MainModule__ctor_m724825857_AdjustorThunk (RuntimeObject * __this, ParticleSystem_t3394631041 * ___particleSystem0, const RuntimeMethod* method)
{
	MainModule_t6751348 * _thisAdjusted = reinterpret_cast<MainModule_t6751348 *>(__this + 1);
	MainModule__ctor_m724825857(_thisAdjusted, ___particleSystem0, method);
}
// System.Void UnityEngine.ParticleSystem/MainModule::set_startColor(UnityEngine.ParticleSystem/MinMaxGradient)
extern "C"  void MainModule_set_startColor_m3844897932 (MainModule_t6751348 * __this, MinMaxGradient_t3708298175  ___value0, const RuntimeMethod* method)
{
	{
		ParticleSystem_t3394631041 * L_0 = __this->get_m_ParticleSystem_0();
		MainModule_SetStartColor_m2492347281(NULL /*static, unused*/, L_0, (&___value0), /*hidden argument*/NULL);
		return;
	}
}
extern "C"  void MainModule_set_startColor_m3844897932_AdjustorThunk (RuntimeObject * __this, MinMaxGradient_t3708298175  ___value0, const RuntimeMethod* method)
{
	MainModule_t6751348 * _thisAdjusted = reinterpret_cast<MainModule_t6751348 *>(__this + 1);
	MainModule_set_startColor_m3844897932(_thisAdjusted, ___value0, method);
}
// UnityEngine.ParticleSystem/MinMaxGradient UnityEngine.ParticleSystem/MainModule::get_startColor()
extern "C"  MinMaxGradient_t3708298175  MainModule_get_startColor_m642758877 (MainModule_t6751348 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (MainModule_get_startColor_m642758877_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	MinMaxGradient_t3708298175  V_0;
	memset(&V_0, 0, sizeof(V_0));
	MinMaxGradient_t3708298175  V_1;
	memset(&V_1, 0, sizeof(V_1));
	{
		Initobj (MinMaxGradient_t3708298175_il2cpp_TypeInfo_var, (&V_0));
		ParticleSystem_t3394631041 * L_0 = __this->get_m_ParticleSystem_0();
		MainModule_GetStartColor_m2015758365(NULL /*static, unused*/, L_0, (&V_0), /*hidden argument*/NULL);
		MinMaxGradient_t3708298175  L_1 = V_0;
		V_1 = L_1;
		goto IL_001d;
	}

IL_001d:
	{
		MinMaxGradient_t3708298175  L_2 = V_1;
		return L_2;
	}
}
extern "C"  MinMaxGradient_t3708298175  MainModule_get_startColor_m642758877_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	MainModule_t6751348 * _thisAdjusted = reinterpret_cast<MainModule_t6751348 *>(__this + 1);
	return MainModule_get_startColor_m642758877(_thisAdjusted, method);
}
// System.Void UnityEngine.ParticleSystem/MainModule::set_gravityModifier(UnityEngine.ParticleSystem/MinMaxCurve)
extern "C"  void MainModule_set_gravityModifier_m676251203 (MainModule_t6751348 * __this, MinMaxCurve_t122563058  ___value0, const RuntimeMethod* method)
{
	{
		ParticleSystem_t3394631041 * L_0 = __this->get_m_ParticleSystem_0();
		MainModule_SetGravityModifier_m2001849420(NULL /*static, unused*/, L_0, (&___value0), /*hidden argument*/NULL);
		return;
	}
}
extern "C"  void MainModule_set_gravityModifier_m676251203_AdjustorThunk (RuntimeObject * __this, MinMaxCurve_t122563058  ___value0, const RuntimeMethod* method)
{
	MainModule_t6751348 * _thisAdjusted = reinterpret_cast<MainModule_t6751348 *>(__this + 1);
	MainModule_set_gravityModifier_m676251203(_thisAdjusted, ___value0, method);
}
// UnityEngine.ParticleSystem/MinMaxCurve UnityEngine.ParticleSystem/MainModule::get_gravityModifier()
extern "C"  MinMaxCurve_t122563058  MainModule_get_gravityModifier_m483350030 (MainModule_t6751348 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (MainModule_get_gravityModifier_m483350030_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	MinMaxCurve_t122563058  V_0;
	memset(&V_0, 0, sizeof(V_0));
	MinMaxCurve_t122563058  V_1;
	memset(&V_1, 0, sizeof(V_1));
	{
		Initobj (MinMaxCurve_t122563058_il2cpp_TypeInfo_var, (&V_0));
		ParticleSystem_t3394631041 * L_0 = __this->get_m_ParticleSystem_0();
		MainModule_GetGravityModifier_m4089386048(NULL /*static, unused*/, L_0, (&V_0), /*hidden argument*/NULL);
		MinMaxCurve_t122563058  L_1 = V_0;
		V_1 = L_1;
		goto IL_001d;
	}

IL_001d:
	{
		MinMaxCurve_t122563058  L_2 = V_1;
		return L_2;
	}
}
extern "C"  MinMaxCurve_t122563058  MainModule_get_gravityModifier_m483350030_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	MainModule_t6751348 * _thisAdjusted = reinterpret_cast<MainModule_t6751348 *>(__this + 1);
	return MainModule_get_gravityModifier_m483350030(_thisAdjusted, method);
}
// System.Void UnityEngine.ParticleSystem/MainModule::set_scalingMode(UnityEngine.ParticleSystemScalingMode)
extern "C"  void MainModule_set_scalingMode_m595996162 (MainModule_t6751348 * __this, int32_t ___value0, const RuntimeMethod* method)
{
	{
		ParticleSystem_t3394631041 * L_0 = __this->get_m_ParticleSystem_0();
		int32_t L_1 = ___value0;
		MainModule_SetScalingMode_m10537077(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
extern "C"  void MainModule_set_scalingMode_m595996162_AdjustorThunk (RuntimeObject * __this, int32_t ___value0, const RuntimeMethod* method)
{
	MainModule_t6751348 * _thisAdjusted = reinterpret_cast<MainModule_t6751348 *>(__this + 1);
	MainModule_set_scalingMode_m595996162(_thisAdjusted, ___value0, method);
}
// System.Void UnityEngine.ParticleSystem/MainModule::SetStartColor(UnityEngine.ParticleSystem,UnityEngine.ParticleSystem/MinMaxGradient&)
extern "C"  void MainModule_SetStartColor_m2492347281 (RuntimeObject * __this /* static, unused */, ParticleSystem_t3394631041 * ___system0, MinMaxGradient_t3708298175 * ___gradient1, const RuntimeMethod* method)
{
	typedef void (*MainModule_SetStartColor_m2492347281_ftn) (ParticleSystem_t3394631041 *, MinMaxGradient_t3708298175 *);
	static MainModule_SetStartColor_m2492347281_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (MainModule_SetStartColor_m2492347281_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.ParticleSystem/MainModule::SetStartColor(UnityEngine.ParticleSystem,UnityEngine.ParticleSystem/MinMaxGradient&)");
	_il2cpp_icall_func(___system0, ___gradient1);
}
// System.Void UnityEngine.ParticleSystem/MainModule::GetStartColor(UnityEngine.ParticleSystem,UnityEngine.ParticleSystem/MinMaxGradient&)
extern "C"  void MainModule_GetStartColor_m2015758365 (RuntimeObject * __this /* static, unused */, ParticleSystem_t3394631041 * ___system0, MinMaxGradient_t3708298175 * ___gradient1, const RuntimeMethod* method)
{
	typedef void (*MainModule_GetStartColor_m2015758365_ftn) (ParticleSystem_t3394631041 *, MinMaxGradient_t3708298175 *);
	static MainModule_GetStartColor_m2015758365_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (MainModule_GetStartColor_m2015758365_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.ParticleSystem/MainModule::GetStartColor(UnityEngine.ParticleSystem,UnityEngine.ParticleSystem/MinMaxGradient&)");
	_il2cpp_icall_func(___system0, ___gradient1);
}
// System.Void UnityEngine.ParticleSystem/MainModule::SetGravityModifier(UnityEngine.ParticleSystem,UnityEngine.ParticleSystem/MinMaxCurve&)
extern "C"  void MainModule_SetGravityModifier_m2001849420 (RuntimeObject * __this /* static, unused */, ParticleSystem_t3394631041 * ___system0, MinMaxCurve_t122563058 * ___curve1, const RuntimeMethod* method)
{
	typedef void (*MainModule_SetGravityModifier_m2001849420_ftn) (ParticleSystem_t3394631041 *, MinMaxCurve_t122563058 *);
	static MainModule_SetGravityModifier_m2001849420_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (MainModule_SetGravityModifier_m2001849420_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.ParticleSystem/MainModule::SetGravityModifier(UnityEngine.ParticleSystem,UnityEngine.ParticleSystem/MinMaxCurve&)");
	_il2cpp_icall_func(___system0, ___curve1);
}
// System.Void UnityEngine.ParticleSystem/MainModule::GetGravityModifier(UnityEngine.ParticleSystem,UnityEngine.ParticleSystem/MinMaxCurve&)
extern "C"  void MainModule_GetGravityModifier_m4089386048 (RuntimeObject * __this /* static, unused */, ParticleSystem_t3394631041 * ___system0, MinMaxCurve_t122563058 * ___curve1, const RuntimeMethod* method)
{
	typedef void (*MainModule_GetGravityModifier_m4089386048_ftn) (ParticleSystem_t3394631041 *, MinMaxCurve_t122563058 *);
	static MainModule_GetGravityModifier_m4089386048_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (MainModule_GetGravityModifier_m4089386048_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.ParticleSystem/MainModule::GetGravityModifier(UnityEngine.ParticleSystem,UnityEngine.ParticleSystem/MinMaxCurve&)");
	_il2cpp_icall_func(___system0, ___curve1);
}
// System.Void UnityEngine.ParticleSystem/MainModule::SetScalingMode(UnityEngine.ParticleSystem,UnityEngine.ParticleSystemScalingMode)
extern "C"  void MainModule_SetScalingMode_m10537077 (RuntimeObject * __this /* static, unused */, ParticleSystem_t3394631041 * ___system0, int32_t ___value1, const RuntimeMethod* method)
{
	typedef void (*MainModule_SetScalingMode_m10537077_ftn) (ParticleSystem_t3394631041 *, int32_t);
	static MainModule_SetScalingMode_m10537077_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (MainModule_SetScalingMode_m10537077_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.ParticleSystem/MainModule::SetScalingMode(UnityEngine.ParticleSystem,UnityEngine.ParticleSystemScalingMode)");
	_il2cpp_icall_func(___system0, ___value1);
}




// Conversion methods for marshalling of: UnityEngine.ParticleSystem/MinMaxCurve
extern "C" void MinMaxCurve_t122563058_marshal_pinvoke(const MinMaxCurve_t122563058& unmarshaled, MinMaxCurve_t122563058_marshaled_pinvoke& marshaled)
{
	marshaled.___m_Mode_0 = unmarshaled.get_m_Mode_0();
	marshaled.___m_CurveMultiplier_1 = unmarshaled.get_m_CurveMultiplier_1();
	if (unmarshaled.get_m_CurveMin_2() != NULL) AnimationCurve_t3306541151_marshal_pinvoke(*unmarshaled.get_m_CurveMin_2(), marshaled.___m_CurveMin_2);
	if (unmarshaled.get_m_CurveMax_3() != NULL) AnimationCurve_t3306541151_marshal_pinvoke(*unmarshaled.get_m_CurveMax_3(), marshaled.___m_CurveMax_3);
	marshaled.___m_ConstantMin_4 = unmarshaled.get_m_ConstantMin_4();
	marshaled.___m_ConstantMax_5 = unmarshaled.get_m_ConstantMax_5();
}
extern "C" void MinMaxCurve_t122563058_marshal_pinvoke_back(const MinMaxCurve_t122563058_marshaled_pinvoke& marshaled, MinMaxCurve_t122563058& unmarshaled)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (MinMaxCurve_t122563058_pinvoke_FromNativeMethodDefinition_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t unmarshaled_m_Mode_temp_0 = 0;
	unmarshaled_m_Mode_temp_0 = marshaled.___m_Mode_0;
	unmarshaled.set_m_Mode_0(unmarshaled_m_Mode_temp_0);
	float unmarshaled_m_CurveMultiplier_temp_1 = 0.0f;
	unmarshaled_m_CurveMultiplier_temp_1 = marshaled.___m_CurveMultiplier_1;
	unmarshaled.set_m_CurveMultiplier_1(unmarshaled_m_CurveMultiplier_temp_1);
	unmarshaled.set_m_CurveMin_2((AnimationCurve_t3306541151 *)il2cpp_codegen_object_new(AnimationCurve_t3306541151_il2cpp_TypeInfo_var));
	AnimationCurve__ctor_m3808882547(unmarshaled.get_m_CurveMin_2(), NULL);
	AnimationCurve_t3306541151_marshal_pinvoke_back(marshaled.___m_CurveMin_2, *unmarshaled.get_m_CurveMin_2());
	unmarshaled.set_m_CurveMax_3((AnimationCurve_t3306541151 *)il2cpp_codegen_object_new(AnimationCurve_t3306541151_il2cpp_TypeInfo_var));
	AnimationCurve__ctor_m3808882547(unmarshaled.get_m_CurveMax_3(), NULL);
	AnimationCurve_t3306541151_marshal_pinvoke_back(marshaled.___m_CurveMax_3, *unmarshaled.get_m_CurveMax_3());
	float unmarshaled_m_ConstantMin_temp_4 = 0.0f;
	unmarshaled_m_ConstantMin_temp_4 = marshaled.___m_ConstantMin_4;
	unmarshaled.set_m_ConstantMin_4(unmarshaled_m_ConstantMin_temp_4);
	float unmarshaled_m_ConstantMax_temp_5 = 0.0f;
	unmarshaled_m_ConstantMax_temp_5 = marshaled.___m_ConstantMax_5;
	unmarshaled.set_m_ConstantMax_5(unmarshaled_m_ConstantMax_temp_5);
}
// Conversion method for clean up from marshalling of: UnityEngine.ParticleSystem/MinMaxCurve
extern "C" void MinMaxCurve_t122563058_marshal_pinvoke_cleanup(MinMaxCurve_t122563058_marshaled_pinvoke& marshaled)
{
	AnimationCurve_t3306541151_marshal_pinvoke_cleanup(marshaled.___m_CurveMin_2);
	AnimationCurve_t3306541151_marshal_pinvoke_cleanup(marshaled.___m_CurveMax_3);
}




// Conversion methods for marshalling of: UnityEngine.ParticleSystem/MinMaxCurve
extern "C" void MinMaxCurve_t122563058_marshal_com(const MinMaxCurve_t122563058& unmarshaled, MinMaxCurve_t122563058_marshaled_com& marshaled)
{
	marshaled.___m_Mode_0 = unmarshaled.get_m_Mode_0();
	marshaled.___m_CurveMultiplier_1 = unmarshaled.get_m_CurveMultiplier_1();
	if (unmarshaled.get_m_CurveMin_2() != NULL) AnimationCurve_t3306541151_marshal_com(*unmarshaled.get_m_CurveMin_2(), *marshaled.___m_CurveMin_2);
	if (unmarshaled.get_m_CurveMax_3() != NULL) AnimationCurve_t3306541151_marshal_com(*unmarshaled.get_m_CurveMax_3(), *marshaled.___m_CurveMax_3);
	marshaled.___m_ConstantMin_4 = unmarshaled.get_m_ConstantMin_4();
	marshaled.___m_ConstantMax_5 = unmarshaled.get_m_ConstantMax_5();
}
extern "C" void MinMaxCurve_t122563058_marshal_com_back(const MinMaxCurve_t122563058_marshaled_com& marshaled, MinMaxCurve_t122563058& unmarshaled)
{
	int32_t unmarshaled_m_Mode_temp_0 = 0;
	unmarshaled_m_Mode_temp_0 = marshaled.___m_Mode_0;
	unmarshaled.set_m_Mode_0(unmarshaled_m_Mode_temp_0);
	float unmarshaled_m_CurveMultiplier_temp_1 = 0.0f;
	unmarshaled_m_CurveMultiplier_temp_1 = marshaled.___m_CurveMultiplier_1;
	unmarshaled.set_m_CurveMultiplier_1(unmarshaled_m_CurveMultiplier_temp_1);
	if (unmarshaled.get_m_CurveMin_2() != NULL)
	{
		AnimationCurve__ctor_m3808882547(unmarshaled.get_m_CurveMin_2(), NULL);
		AnimationCurve_t3306541151_marshal_com_back(*marshaled.___m_CurveMin_2, *unmarshaled.get_m_CurveMin_2());
	}
	if (unmarshaled.get_m_CurveMax_3() != NULL)
	{
		AnimationCurve__ctor_m3808882547(unmarshaled.get_m_CurveMax_3(), NULL);
		AnimationCurve_t3306541151_marshal_com_back(*marshaled.___m_CurveMax_3, *unmarshaled.get_m_CurveMax_3());
	}
	float unmarshaled_m_ConstantMin_temp_4 = 0.0f;
	unmarshaled_m_ConstantMin_temp_4 = marshaled.___m_ConstantMin_4;
	unmarshaled.set_m_ConstantMin_4(unmarshaled_m_ConstantMin_temp_4);
	float unmarshaled_m_ConstantMax_temp_5 = 0.0f;
	unmarshaled_m_ConstantMax_temp_5 = marshaled.___m_ConstantMax_5;
	unmarshaled.set_m_ConstantMax_5(unmarshaled_m_ConstantMax_temp_5);
}
// Conversion method for clean up from marshalling of: UnityEngine.ParticleSystem/MinMaxCurve
extern "C" void MinMaxCurve_t122563058_marshal_com_cleanup(MinMaxCurve_t122563058_marshaled_com& marshaled)
{
	if (&(*marshaled.___m_CurveMin_2) != NULL) AnimationCurve_t3306541151_marshal_com_cleanup(*marshaled.___m_CurveMin_2);
	if (&(*marshaled.___m_CurveMax_3) != NULL) AnimationCurve_t3306541151_marshal_com_cleanup(*marshaled.___m_CurveMax_3);
}
// System.Void UnityEngine.ParticleSystem/MinMaxCurve::.ctor(System.Single)
extern "C"  void MinMaxCurve__ctor_m3111284754 (MinMaxCurve_t122563058 * __this, float ___constant0, const RuntimeMethod* method)
{
	{
		__this->set_m_Mode_0(0);
		__this->set_m_CurveMultiplier_1((0.0f));
		__this->set_m_CurveMin_2((AnimationCurve_t3306541151 *)NULL);
		__this->set_m_CurveMax_3((AnimationCurve_t3306541151 *)NULL);
		__this->set_m_ConstantMin_4((0.0f));
		float L_0 = ___constant0;
		__this->set_m_ConstantMax_5(L_0);
		return;
	}
}
extern "C"  void MinMaxCurve__ctor_m3111284754_AdjustorThunk (RuntimeObject * __this, float ___constant0, const RuntimeMethod* method)
{
	MinMaxCurve_t122563058 * _thisAdjusted = reinterpret_cast<MinMaxCurve_t122563058 *>(__this + 1);
	MinMaxCurve__ctor_m3111284754(_thisAdjusted, ___constant0, method);
}
// System.Single UnityEngine.ParticleSystem/MinMaxCurve::get_constant()
extern "C"  float MinMaxCurve_get_constant_m2300889686 (MinMaxCurve_t122563058 * __this, const RuntimeMethod* method)
{
	float V_0 = 0.0f;
	{
		float L_0 = __this->get_m_ConstantMax_5();
		V_0 = L_0;
		goto IL_000d;
	}

IL_000d:
	{
		float L_1 = V_0;
		return L_1;
	}
}
extern "C"  float MinMaxCurve_get_constant_m2300889686_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	MinMaxCurve_t122563058 * _thisAdjusted = reinterpret_cast<MinMaxCurve_t122563058 *>(__this + 1);
	return MinMaxCurve_get_constant_m2300889686(_thisAdjusted, method);
}
// UnityEngine.ParticleSystem/MinMaxCurve UnityEngine.ParticleSystem/MinMaxCurve::op_Implicit(System.Single)
extern "C"  MinMaxCurve_t122563058  MinMaxCurve_op_Implicit_m4185724230 (RuntimeObject * __this /* static, unused */, float ___constant0, const RuntimeMethod* method)
{
	MinMaxCurve_t122563058  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		float L_0 = ___constant0;
		MinMaxCurve_t122563058  L_1;
		memset(&L_1, 0, sizeof(L_1));
		MinMaxCurve__ctor_m3111284754((&L_1), L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		goto IL_000d;
	}

IL_000d:
	{
		MinMaxCurve_t122563058  L_2 = V_0;
		return L_2;
	}
}




// Conversion methods for marshalling of: UnityEngine.ParticleSystem/MinMaxGradient
extern "C" void MinMaxGradient_t3708298175_marshal_pinvoke(const MinMaxGradient_t3708298175& unmarshaled, MinMaxGradient_t3708298175_marshaled_pinvoke& marshaled)
{
	marshaled.___m_Mode_0 = unmarshaled.get_m_Mode_0();
	if (unmarshaled.get_m_GradientMin_1() != NULL) Gradient_t3600583008_marshal_pinvoke(*unmarshaled.get_m_GradientMin_1(), marshaled.___m_GradientMin_1);
	if (unmarshaled.get_m_GradientMax_2() != NULL) Gradient_t3600583008_marshal_pinvoke(*unmarshaled.get_m_GradientMax_2(), marshaled.___m_GradientMax_2);
	marshaled.___m_ColorMin_3 = unmarshaled.get_m_ColorMin_3();
	marshaled.___m_ColorMax_4 = unmarshaled.get_m_ColorMax_4();
}
extern "C" void MinMaxGradient_t3708298175_marshal_pinvoke_back(const MinMaxGradient_t3708298175_marshaled_pinvoke& marshaled, MinMaxGradient_t3708298175& unmarshaled)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (MinMaxGradient_t3708298175_pinvoke_FromNativeMethodDefinition_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t unmarshaled_m_Mode_temp_0 = 0;
	unmarshaled_m_Mode_temp_0 = marshaled.___m_Mode_0;
	unmarshaled.set_m_Mode_0(unmarshaled_m_Mode_temp_0);
	unmarshaled.set_m_GradientMin_1((Gradient_t3600583008 *)il2cpp_codegen_object_new(Gradient_t3600583008_il2cpp_TypeInfo_var));
	Gradient__ctor_m1008579686(unmarshaled.get_m_GradientMin_1(), NULL);
	Gradient_t3600583008_marshal_pinvoke_back(marshaled.___m_GradientMin_1, *unmarshaled.get_m_GradientMin_1());
	unmarshaled.set_m_GradientMax_2((Gradient_t3600583008 *)il2cpp_codegen_object_new(Gradient_t3600583008_il2cpp_TypeInfo_var));
	Gradient__ctor_m1008579686(unmarshaled.get_m_GradientMax_2(), NULL);
	Gradient_t3600583008_marshal_pinvoke_back(marshaled.___m_GradientMax_2, *unmarshaled.get_m_GradientMax_2());
	Color_t2020392075  unmarshaled_m_ColorMin_temp_3;
	memset(&unmarshaled_m_ColorMin_temp_3, 0, sizeof(unmarshaled_m_ColorMin_temp_3));
	unmarshaled_m_ColorMin_temp_3 = marshaled.___m_ColorMin_3;
	unmarshaled.set_m_ColorMin_3(unmarshaled_m_ColorMin_temp_3);
	Color_t2020392075  unmarshaled_m_ColorMax_temp_4;
	memset(&unmarshaled_m_ColorMax_temp_4, 0, sizeof(unmarshaled_m_ColorMax_temp_4));
	unmarshaled_m_ColorMax_temp_4 = marshaled.___m_ColorMax_4;
	unmarshaled.set_m_ColorMax_4(unmarshaled_m_ColorMax_temp_4);
}
// Conversion method for clean up from marshalling of: UnityEngine.ParticleSystem/MinMaxGradient
extern "C" void MinMaxGradient_t3708298175_marshal_pinvoke_cleanup(MinMaxGradient_t3708298175_marshaled_pinvoke& marshaled)
{
	Gradient_t3600583008_marshal_pinvoke_cleanup(marshaled.___m_GradientMin_1);
	Gradient_t3600583008_marshal_pinvoke_cleanup(marshaled.___m_GradientMax_2);
}




// Conversion methods for marshalling of: UnityEngine.ParticleSystem/MinMaxGradient
extern "C" void MinMaxGradient_t3708298175_marshal_com(const MinMaxGradient_t3708298175& unmarshaled, MinMaxGradient_t3708298175_marshaled_com& marshaled)
{
	marshaled.___m_Mode_0 = unmarshaled.get_m_Mode_0();
	if (unmarshaled.get_m_GradientMin_1() != NULL) Gradient_t3600583008_marshal_com(*unmarshaled.get_m_GradientMin_1(), *marshaled.___m_GradientMin_1);
	if (unmarshaled.get_m_GradientMax_2() != NULL) Gradient_t3600583008_marshal_com(*unmarshaled.get_m_GradientMax_2(), *marshaled.___m_GradientMax_2);
	marshaled.___m_ColorMin_3 = unmarshaled.get_m_ColorMin_3();
	marshaled.___m_ColorMax_4 = unmarshaled.get_m_ColorMax_4();
}
extern "C" void MinMaxGradient_t3708298175_marshal_com_back(const MinMaxGradient_t3708298175_marshaled_com& marshaled, MinMaxGradient_t3708298175& unmarshaled)
{
	int32_t unmarshaled_m_Mode_temp_0 = 0;
	unmarshaled_m_Mode_temp_0 = marshaled.___m_Mode_0;
	unmarshaled.set_m_Mode_0(unmarshaled_m_Mode_temp_0);
	if (unmarshaled.get_m_GradientMin_1() != NULL)
	{
		Gradient__ctor_m1008579686(unmarshaled.get_m_GradientMin_1(), NULL);
		Gradient_t3600583008_marshal_com_back(*marshaled.___m_GradientMin_1, *unmarshaled.get_m_GradientMin_1());
	}
	if (unmarshaled.get_m_GradientMax_2() != NULL)
	{
		Gradient__ctor_m1008579686(unmarshaled.get_m_GradientMax_2(), NULL);
		Gradient_t3600583008_marshal_com_back(*marshaled.___m_GradientMax_2, *unmarshaled.get_m_GradientMax_2());
	}
	Color_t2020392075  unmarshaled_m_ColorMin_temp_3;
	memset(&unmarshaled_m_ColorMin_temp_3, 0, sizeof(unmarshaled_m_ColorMin_temp_3));
	unmarshaled_m_ColorMin_temp_3 = marshaled.___m_ColorMin_3;
	unmarshaled.set_m_ColorMin_3(unmarshaled_m_ColorMin_temp_3);
	Color_t2020392075  unmarshaled_m_ColorMax_temp_4;
	memset(&unmarshaled_m_ColorMax_temp_4, 0, sizeof(unmarshaled_m_ColorMax_temp_4));
	unmarshaled_m_ColorMax_temp_4 = marshaled.___m_ColorMax_4;
	unmarshaled.set_m_ColorMax_4(unmarshaled_m_ColorMax_temp_4);
}
// Conversion method for clean up from marshalling of: UnityEngine.ParticleSystem/MinMaxGradient
extern "C" void MinMaxGradient_t3708298175_marshal_com_cleanup(MinMaxGradient_t3708298175_marshaled_com& marshaled)
{
	if (&(*marshaled.___m_GradientMin_1) != NULL) Gradient_t3600583008_marshal_com_cleanup(*marshaled.___m_GradientMin_1);
	if (&(*marshaled.___m_GradientMax_2) != NULL) Gradient_t3600583008_marshal_com_cleanup(*marshaled.___m_GradientMax_2);
}
// System.Void UnityEngine.ParticleSystem/MinMaxGradient::.ctor(UnityEngine.Color)
extern "C"  void MinMaxGradient__ctor_m2960432810 (MinMaxGradient_t3708298175 * __this, Color_t2020392075  ___color0, const RuntimeMethod* method)
{
	{
		__this->set_m_Mode_0(0);
		__this->set_m_GradientMin_1((Gradient_t3600583008 *)NULL);
		__this->set_m_GradientMax_2((Gradient_t3600583008 *)NULL);
		Color_t2020392075  L_0 = Color_get_black_m455352838(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set_m_ColorMin_3(L_0);
		Color_t2020392075  L_1 = ___color0;
		__this->set_m_ColorMax_4(L_1);
		return;
	}
}
extern "C"  void MinMaxGradient__ctor_m2960432810_AdjustorThunk (RuntimeObject * __this, Color_t2020392075  ___color0, const RuntimeMethod* method)
{
	MinMaxGradient_t3708298175 * _thisAdjusted = reinterpret_cast<MinMaxGradient_t3708298175 *>(__this + 1);
	MinMaxGradient__ctor_m2960432810(_thisAdjusted, ___color0, method);
}
// UnityEngine.Color UnityEngine.ParticleSystem/MinMaxGradient::get_color()
extern "C"  Color_t2020392075  MinMaxGradient_get_color_m1448527895 (MinMaxGradient_t3708298175 * __this, const RuntimeMethod* method)
{
	Color_t2020392075  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Color_t2020392075  L_0 = __this->get_m_ColorMax_4();
		V_0 = L_0;
		goto IL_000d;
	}

IL_000d:
	{
		Color_t2020392075  L_1 = V_0;
		return L_1;
	}
}
extern "C"  Color_t2020392075  MinMaxGradient_get_color_m1448527895_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	MinMaxGradient_t3708298175 * _thisAdjusted = reinterpret_cast<MinMaxGradient_t3708298175 *>(__this + 1);
	return MinMaxGradient_get_color_m1448527895(_thisAdjusted, method);
}
// UnityEngine.ParticleSystem/MinMaxGradient UnityEngine.ParticleSystem/MinMaxGradient::op_Implicit(UnityEngine.Color)
extern "C"  MinMaxGradient_t3708298175  MinMaxGradient_op_Implicit_m3183052829 (RuntimeObject * __this /* static, unused */, Color_t2020392075  ___color0, const RuntimeMethod* method)
{
	MinMaxGradient_t3708298175  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Color_t2020392075  L_0 = ___color0;
		MinMaxGradient_t3708298175  L_1;
		memset(&L_1, 0, sizeof(L_1));
		MinMaxGradient__ctor_m2960432810((&L_1), L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		goto IL_000d;
	}

IL_000d:
	{
		MinMaxGradient_t3708298175  L_2 = V_0;
		return L_2;
	}
}
// Conversion methods for marshalling of: UnityEngine.ParticleSystem/VelocityOverLifetimeModule
extern "C" void VelocityOverLifetimeModule_t2506587731_marshal_pinvoke(const VelocityOverLifetimeModule_t2506587731& unmarshaled, VelocityOverLifetimeModule_t2506587731_marshaled_pinvoke& marshaled)
{
	Il2CppCodeGenException* ___m_ParticleSystem_0Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'm_ParticleSystem' of type 'VelocityOverLifetimeModule': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___m_ParticleSystem_0Exception);
}
extern "C" void VelocityOverLifetimeModule_t2506587731_marshal_pinvoke_back(const VelocityOverLifetimeModule_t2506587731_marshaled_pinvoke& marshaled, VelocityOverLifetimeModule_t2506587731& unmarshaled)
{
	Il2CppCodeGenException* ___m_ParticleSystem_0Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'm_ParticleSystem' of type 'VelocityOverLifetimeModule': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___m_ParticleSystem_0Exception);
}
// Conversion method for clean up from marshalling of: UnityEngine.ParticleSystem/VelocityOverLifetimeModule
extern "C" void VelocityOverLifetimeModule_t2506587731_marshal_pinvoke_cleanup(VelocityOverLifetimeModule_t2506587731_marshaled_pinvoke& marshaled)
{
}
// Conversion methods for marshalling of: UnityEngine.ParticleSystem/VelocityOverLifetimeModule
extern "C" void VelocityOverLifetimeModule_t2506587731_marshal_com(const VelocityOverLifetimeModule_t2506587731& unmarshaled, VelocityOverLifetimeModule_t2506587731_marshaled_com& marshaled)
{
	Il2CppCodeGenException* ___m_ParticleSystem_0Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'm_ParticleSystem' of type 'VelocityOverLifetimeModule': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___m_ParticleSystem_0Exception);
}
extern "C" void VelocityOverLifetimeModule_t2506587731_marshal_com_back(const VelocityOverLifetimeModule_t2506587731_marshaled_com& marshaled, VelocityOverLifetimeModule_t2506587731& unmarshaled)
{
	Il2CppCodeGenException* ___m_ParticleSystem_0Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'm_ParticleSystem' of type 'VelocityOverLifetimeModule': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___m_ParticleSystem_0Exception);
}
// Conversion method for clean up from marshalling of: UnityEngine.ParticleSystem/VelocityOverLifetimeModule
extern "C" void VelocityOverLifetimeModule_t2506587731_marshal_com_cleanup(VelocityOverLifetimeModule_t2506587731_marshaled_com& marshaled)
{
}
// System.Void UnityEngine.ParticleSystem/VelocityOverLifetimeModule::.ctor(UnityEngine.ParticleSystem)
extern "C"  void VelocityOverLifetimeModule__ctor_m4227621638 (VelocityOverLifetimeModule_t2506587731 * __this, ParticleSystem_t3394631041 * ___particleSystem0, const RuntimeMethod* method)
{
	{
		ParticleSystem_t3394631041 * L_0 = ___particleSystem0;
		__this->set_m_ParticleSystem_0(L_0);
		return;
	}
}
extern "C"  void VelocityOverLifetimeModule__ctor_m4227621638_AdjustorThunk (RuntimeObject * __this, ParticleSystem_t3394631041 * ___particleSystem0, const RuntimeMethod* method)
{
	VelocityOverLifetimeModule_t2506587731 * _thisAdjusted = reinterpret_cast<VelocityOverLifetimeModule_t2506587731 *>(__this + 1);
	VelocityOverLifetimeModule__ctor_m4227621638(_thisAdjusted, ___particleSystem0, method);
}
// System.Boolean UnityEngine.ParticleSystem/VelocityOverLifetimeModule::get_enabled()
extern "C"  bool VelocityOverLifetimeModule_get_enabled_m230085458 (VelocityOverLifetimeModule_t2506587731 * __this, const RuntimeMethod* method)
{
	bool V_0 = false;
	{
		ParticleSystem_t3394631041 * L_0 = __this->get_m_ParticleSystem_0();
		bool L_1 = VelocityOverLifetimeModule_GetEnabled_m1730484311(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		goto IL_0012;
	}

IL_0012:
	{
		bool L_2 = V_0;
		return L_2;
	}
}
extern "C"  bool VelocityOverLifetimeModule_get_enabled_m230085458_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	VelocityOverLifetimeModule_t2506587731 * _thisAdjusted = reinterpret_cast<VelocityOverLifetimeModule_t2506587731 *>(__this + 1);
	return VelocityOverLifetimeModule_get_enabled_m230085458(_thisAdjusted, method);
}
// System.Void UnityEngine.ParticleSystem/VelocityOverLifetimeModule::set_space(UnityEngine.ParticleSystemSimulationSpace)
extern "C"  void VelocityOverLifetimeModule_set_space_m1646074934 (VelocityOverLifetimeModule_t2506587731 * __this, int32_t ___value0, const RuntimeMethod* method)
{
	{
		ParticleSystem_t3394631041 * L_0 = __this->get_m_ParticleSystem_0();
		int32_t L_1 = ___value0;
		VelocityOverLifetimeModule_SetWorldSpace_m1687038599(NULL /*static, unused*/, L_0, (bool)((((int32_t)L_1) == ((int32_t)1))? 1 : 0), /*hidden argument*/NULL);
		return;
	}
}
extern "C"  void VelocityOverLifetimeModule_set_space_m1646074934_AdjustorThunk (RuntimeObject * __this, int32_t ___value0, const RuntimeMethod* method)
{
	VelocityOverLifetimeModule_t2506587731 * _thisAdjusted = reinterpret_cast<VelocityOverLifetimeModule_t2506587731 *>(__this + 1);
	VelocityOverLifetimeModule_set_space_m1646074934(_thisAdjusted, ___value0, method);
}
// System.Boolean UnityEngine.ParticleSystem/VelocityOverLifetimeModule::GetEnabled(UnityEngine.ParticleSystem)
extern "C"  bool VelocityOverLifetimeModule_GetEnabled_m1730484311 (RuntimeObject * __this /* static, unused */, ParticleSystem_t3394631041 * ___system0, const RuntimeMethod* method)
{
	typedef bool (*VelocityOverLifetimeModule_GetEnabled_m1730484311_ftn) (ParticleSystem_t3394631041 *);
	static VelocityOverLifetimeModule_GetEnabled_m1730484311_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (VelocityOverLifetimeModule_GetEnabled_m1730484311_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.ParticleSystem/VelocityOverLifetimeModule::GetEnabled(UnityEngine.ParticleSystem)");
	bool retVal = _il2cpp_icall_func(___system0);
	return retVal;
}
// System.Void UnityEngine.ParticleSystem/VelocityOverLifetimeModule::SetWorldSpace(UnityEngine.ParticleSystem,System.Boolean)
extern "C"  void VelocityOverLifetimeModule_SetWorldSpace_m1687038599 (RuntimeObject * __this /* static, unused */, ParticleSystem_t3394631041 * ___system0, bool ___value1, const RuntimeMethod* method)
{
	typedef void (*VelocityOverLifetimeModule_SetWorldSpace_m1687038599_ftn) (ParticleSystem_t3394631041 *, bool);
	static VelocityOverLifetimeModule_SetWorldSpace_m1687038599_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (VelocityOverLifetimeModule_SetWorldSpace_m1687038599_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.ParticleSystem/VelocityOverLifetimeModule::SetWorldSpace(UnityEngine.ParticleSystem,System.Boolean)");
	_il2cpp_icall_func(___system0, ___value1);
}
// UnityEngine.ParticleSystemRenderMode UnityEngine.ParticleSystemRenderer::get_renderMode()
extern "C"  int32_t ParticleSystemRenderer_get_renderMode_m854634928 (ParticleSystemRenderer_t892265570 * __this, const RuntimeMethod* method)
{
	typedef int32_t (*ParticleSystemRenderer_get_renderMode_m854634928_ftn) (ParticleSystemRenderer_t892265570 *);
	static ParticleSystemRenderer_get_renderMode_m854634928_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (ParticleSystemRenderer_get_renderMode_m854634928_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.ParticleSystemRenderer::get_renderMode()");
	int32_t retVal = _il2cpp_icall_func(__this);
	return retVal;
}
// System.Void UnityEngine.ParticleSystemRenderer::set_renderMode(UnityEngine.ParticleSystemRenderMode)
extern "C"  void ParticleSystemRenderer_set_renderMode_m561359963 (ParticleSystemRenderer_t892265570 * __this, int32_t ___value0, const RuntimeMethod* method)
{
	typedef void (*ParticleSystemRenderer_set_renderMode_m561359963_ftn) (ParticleSystemRenderer_t892265570 *, int32_t);
	static ParticleSystemRenderer_set_renderMode_m561359963_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (ParticleSystemRenderer_set_renderMode_m561359963_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.ParticleSystemRenderer::set_renderMode(UnityEngine.ParticleSystemRenderMode)");
	_il2cpp_icall_func(__this, ___value0);
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
