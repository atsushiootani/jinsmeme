﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "object-internals.h"

// System.Action
struct Action_t3226471752;
// Utage.UguiCrossFadeRawImage
struct UguiCrossFadeRawImage_t1509295035;
// Utage.Timer
struct Timer_t2904185433;
// Utage.UguiTransition
struct UguiTransition_t3998485683;
// System.String
struct String_t;
// Utage.SoundAudioPlayer
struct SoundAudioPlayer_t3829317720;
// Utage.SoundAudio
struct SoundAudio_t3759608863;
// System.Action`2<UnityEngine.WWW,UnityEngine.AssetBundle>
struct Action_2_t1479210752;
// System.Action`1<UnityEngine.WWW>
struct Action_1_t2721744421;
// Utage.WWWEx
struct WWWEx_t1775400116;
// System.Collections.Generic.List`1<UnityEngine.Sprite>
struct List_1_t3973682211;
// System.Collections.Generic.HashSet`1<System.String>
struct HashSet_1_t362681087;
// Utage.SystemUiDebugMenu
struct SystemUiDebugMenu_t1631343349;
// Utage.StringGrid
struct StringGrid_t1872153679;
// System.String[]
struct StringU5BU5D_t1642385972;
// Utage.CharData/CustomCharaInfo
struct CustomCharaInfo_t997213109;
// Utage.WaitTimer
struct WaitTimer_t2776916572;
// System.Collections.Generic.Dictionary`2<System.String,Utage.SoundGroup>
struct Dictionary_2_t3930869258;
// Utage.SoundManager
struct SoundManager_t1265124084;
// System.Collections.Generic.List`1<Utage.CharData>
struct List_1_t3547192402;
// System.Func`2<System.String,System.Object>
struct Func_2_t853256019;
// System.Collections.Generic.Dictionary`2<System.String,System.Int32>
struct Dictionary_2_t3986656710;
// Utage.TextParser
struct TextParser_t4249442530;
// System.Collections.Generic.List`1<Utage.ImageEffectUtil/ImageEffectPattern>
struct List_1_t2525152917;
// System.Type
struct Type_t;
// UnityEngine.Shader[]
struct ShaderU5BU5D_t1916779366;
// System.Collections.Generic.List`1<Utage.StringGridDictionaryKeyValue>
struct List_1_t1589136695;
// System.Collections.Generic.Dictionary`2<System.String,Utage.StringGridDictionaryKeyValue>
struct Dictionary_2_t4134794825;
// System.Collections.Generic.List`1<Utage.AvatarPattern/PartternData>
struct List_1_t2549837370;
// System.Collections.Generic.List`1<System.String>
struct List_1_t1398341365;
// Utage.AvatarData/Category
struct Category_t2702308564;
// UnityEngine.Events.InvokableCallList
struct InvokableCallList_t2295673753;
// UnityEngine.Events.PersistentCallGroup
struct PersistentCallGroup_t339478082;
// System.Void
struct Void_t1841601450;
// System.Collections.Generic.List`1<Utage.MiniAnimationData/Data>
struct List_1_t4284043194;
// Utage.MiniAnimationData/Data
struct Data_t619954766;
// System.Char[]
struct CharU5BU5D_t1328083999;
// System.Object[]
struct ObjectU5BU5D_t3614634134;
// UnityEngine.Events.UnityAction`1<Utage.WaitTimer>
struct UnityAction_1_t4143502323;
// Utage.EyeBlinkAvatar
struct EyeBlinkAvatar_t3311504802;
// UnityEngine.AudioClip
struct AudioClip_t1932558630;
// Utage.AssetFile
struct AssetFile_t3383013256;
// System.Collections.Generic.List`1<Utage.StringGridRow>
struct List_1_t3562358329;
// System.Collections.Generic.Dictionary`2<System.String,System.Object>
struct Dictionary_2_t309261261;
// System.String[][]
struct StringU5BU5DU5BU5D_t2190260861;
// System.Collections.Generic.List`1<Utage.AvatarData/Category>
struct List_1_t2071429696;
// Utage.MinMaxFloat
struct MinMaxFloat_t1425080750;
// Utage.MiniAnimationData
struct MiniAnimationData_t521227391;
// Utage.SystemUiDialog2Button
struct SystemUiDialog2Button_t2800622875;
// Utage.SystemUiDialog1Button
struct SystemUiDialog1Button_t2800515000;
// Utage.SystemUiDialog3Button
struct SystemUiDialog3Button_t2800582650;
// Utage.IndicatorIcon
struct IndicatorIcon_t1982044218;
// Utage.AvatarData
struct AvatarData_t1681718915;
// Utage.AvatarPattern
struct AvatarPattern_t4052248955;
// UnityEngine.RectTransform
struct RectTransform_t3349966182;
// UnityEngine.Material
struct Material_t193706927;
// UnityEngine.Events.UnityEvent
struct UnityEvent_t408735097;
// System.Collections.Generic.List`1<Utage.SoundManager/TaggedMasterVolume>
struct List_1_t2034408517;
// Utage.SoundManager/SoundManagerEvent
struct SoundManagerEvent_t3645924073;
// Utage.SoundManagerSystemInterface
struct SoundManagerSystemInterface_t75645720;
// Utage.TimerEvent
struct TimerEvent_t3049894269;
// System.Action`1<Utage.Timer>
struct Action_1_t2705984815;
// UnityEngine.UI.Text
struct Text_t356221433;
// UnityEngine.GameObject
struct GameObject_t1756533147;
// Utage.UguiLocalize
struct UguiLocalize_t1715753735;
// System.Collections.Generic.List`1<System.Object>
struct List_1_t2058570427;
// System.Collections.Generic.List`1<UnityEngine.Material>
struct List_1_t3857795355;
// UnityEngine.AudioSource
struct AudioSource_t1135106623;
// Utage.SoundData
struct SoundData_t3887839289;
// Utage.LinearValue
struct LinearValue_t662057272;
// System.Single[]
struct SingleU5BU5D_t577127397;
// Utage.SoundGroup
struct SoundGroup_t2016089996;
// System.Collections.Generic.List`1<Utage.SoundAudio>
struct List_1_t3128729995;
// System.Predicate`1<Utage.SoundAudio>
struct Predicate_1_t2202578978;
// UnityEngine.UI.Graphic
struct Graphic_t2426225576;
// UnityEngine.Texture
struct Texture_t2243626319;
// Utage.SoundManagerSystem
struct SoundManagerSystem_t1680680183;
// System.Collections.Generic.Dictionary`2<System.String,Utage.SoundAudioPlayer>
struct Dictionary_2_t1449129686;
// System.Collections.Generic.List`1<Utage.SoundGroup>
struct List_1_t1385211128;
// System.Predicate`1<Utage.SoundGroup>
struct Predicate_1_t459060111;
// UnityEngine.Shader
struct Shader_t2430389951;
// Utage.AvatarImage
struct AvatarImage_t1946614104;
// Utage.DicingTextureData
struct DicingTextureData_t752335795;
// Utage.LetterBoxCamera
struct LetterBoxCamera_t3507617684;
// UnityEngine.RenderTexture
struct RenderTexture_t2666733923;
// UnityEngine.Texture2D
struct Texture2D_t3542995729;




#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
#ifndef U3CCROSSFADEU3EC__ANONSTOREY0_T3339215237_H
#define U3CCROSSFADEU3EC__ANONSTOREY0_T3339215237_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.UguiCrossFadeRawImage/<CrossFade>c__AnonStorey0
struct  U3CCrossFadeU3Ec__AnonStorey0_t3339215237  : public RuntimeObject
{
public:
	// System.Action Utage.UguiCrossFadeRawImage/<CrossFade>c__AnonStorey0::onComplete
	Action_t3226471752 * ___onComplete_0;
	// Utage.UguiCrossFadeRawImage Utage.UguiCrossFadeRawImage/<CrossFade>c__AnonStorey0::$this
	UguiCrossFadeRawImage_t1509295035 * ___U24this_1;

public:
	inline static int32_t get_offset_of_onComplete_0() { return static_cast<int32_t>(offsetof(U3CCrossFadeU3Ec__AnonStorey0_t3339215237, ___onComplete_0)); }
	inline Action_t3226471752 * get_onComplete_0() const { return ___onComplete_0; }
	inline Action_t3226471752 ** get_address_of_onComplete_0() { return &___onComplete_0; }
	inline void set_onComplete_0(Action_t3226471752 * value)
	{
		___onComplete_0 = value;
		Il2CppCodeGenWriteBarrier((&___onComplete_0), value);
	}

	inline static int32_t get_offset_of_U24this_1() { return static_cast<int32_t>(offsetof(U3CCrossFadeU3Ec__AnonStorey0_t3339215237, ___U24this_1)); }
	inline UguiCrossFadeRawImage_t1509295035 * get_U24this_1() const { return ___U24this_1; }
	inline UguiCrossFadeRawImage_t1509295035 ** get_address_of_U24this_1() { return &___U24this_1; }
	inline void set_U24this_1(UguiCrossFadeRawImage_t1509295035 * value)
	{
		___U24this_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CCROSSFADEU3EC__ANONSTOREY0_T3339215237_H
#ifndef U3CRULEFADEINU3EC__ANONSTOREY0_T2664374724_H
#define U3CRULEFADEINU3EC__ANONSTOREY0_T2664374724_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.UguiTransition/<RuleFadeIn>c__AnonStorey0
struct  U3CRuleFadeInU3Ec__AnonStorey0_t2664374724  : public RuntimeObject
{
public:
	// Utage.Timer Utage.UguiTransition/<RuleFadeIn>c__AnonStorey0::timer
	Timer_t2904185433 * ___timer_0;
	// System.Action Utage.UguiTransition/<RuleFadeIn>c__AnonStorey0::onComplete
	Action_t3226471752 * ___onComplete_1;
	// Utage.UguiTransition Utage.UguiTransition/<RuleFadeIn>c__AnonStorey0::$this
	UguiTransition_t3998485683 * ___U24this_2;

public:
	inline static int32_t get_offset_of_timer_0() { return static_cast<int32_t>(offsetof(U3CRuleFadeInU3Ec__AnonStorey0_t2664374724, ___timer_0)); }
	inline Timer_t2904185433 * get_timer_0() const { return ___timer_0; }
	inline Timer_t2904185433 ** get_address_of_timer_0() { return &___timer_0; }
	inline void set_timer_0(Timer_t2904185433 * value)
	{
		___timer_0 = value;
		Il2CppCodeGenWriteBarrier((&___timer_0), value);
	}

	inline static int32_t get_offset_of_onComplete_1() { return static_cast<int32_t>(offsetof(U3CRuleFadeInU3Ec__AnonStorey0_t2664374724, ___onComplete_1)); }
	inline Action_t3226471752 * get_onComplete_1() const { return ___onComplete_1; }
	inline Action_t3226471752 ** get_address_of_onComplete_1() { return &___onComplete_1; }
	inline void set_onComplete_1(Action_t3226471752 * value)
	{
		___onComplete_1 = value;
		Il2CppCodeGenWriteBarrier((&___onComplete_1), value);
	}

	inline static int32_t get_offset_of_U24this_2() { return static_cast<int32_t>(offsetof(U3CRuleFadeInU3Ec__AnonStorey0_t2664374724, ___U24this_2)); }
	inline UguiTransition_t3998485683 * get_U24this_2() const { return ___U24this_2; }
	inline UguiTransition_t3998485683 ** get_address_of_U24this_2() { return &___U24this_2; }
	inline void set_U24this_2(UguiTransition_t3998485683 * value)
	{
		___U24this_2 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CRULEFADEINU3EC__ANONSTOREY0_T2664374724_H
#ifndef U3CREADU3EC__ANONSTOREY0_T1030251424_H
#define U3CREADU3EC__ANONSTOREY0_T1030251424_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.SoundAudioPlayer/<Read>c__AnonStorey0
struct  U3CReadU3Ec__AnonStorey0_t1030251424  : public RuntimeObject
{
public:
	// System.String Utage.SoundAudioPlayer/<Read>c__AnonStorey0::audioName
	String_t* ___audioName_0;
	// Utage.SoundAudioPlayer Utage.SoundAudioPlayer/<Read>c__AnonStorey0::$this
	SoundAudioPlayer_t3829317720 * ___U24this_1;

public:
	inline static int32_t get_offset_of_audioName_0() { return static_cast<int32_t>(offsetof(U3CReadU3Ec__AnonStorey0_t1030251424, ___audioName_0)); }
	inline String_t* get_audioName_0() const { return ___audioName_0; }
	inline String_t** get_address_of_audioName_0() { return &___audioName_0; }
	inline void set_audioName_0(String_t* value)
	{
		___audioName_0 = value;
		Il2CppCodeGenWriteBarrier((&___audioName_0), value);
	}

	inline static int32_t get_offset_of_U24this_1() { return static_cast<int32_t>(offsetof(U3CReadU3Ec__AnonStorey0_t1030251424, ___U24this_1)); }
	inline SoundAudioPlayer_t3829317720 * get_U24this_1() const { return ___U24this_1; }
	inline SoundAudioPlayer_t3829317720 ** get_address_of_U24this_1() { return &___U24this_1; }
	inline void set_U24this_1(SoundAudioPlayer_t3829317720 * value)
	{
		___U24this_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CREADU3EC__ANONSTOREY0_T1030251424_H
#ifndef U3CRULEFADEOUTU3EC__ANONSTOREY1_T2281953346_H
#define U3CRULEFADEOUTU3EC__ANONSTOREY1_T2281953346_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.UguiTransition/<RuleFadeOut>c__AnonStorey1
struct  U3CRuleFadeOutU3Ec__AnonStorey1_t2281953346  : public RuntimeObject
{
public:
	// Utage.Timer Utage.UguiTransition/<RuleFadeOut>c__AnonStorey1::timer
	Timer_t2904185433 * ___timer_0;
	// System.Action Utage.UguiTransition/<RuleFadeOut>c__AnonStorey1::onComplete
	Action_t3226471752 * ___onComplete_1;
	// Utage.UguiTransition Utage.UguiTransition/<RuleFadeOut>c__AnonStorey1::$this
	UguiTransition_t3998485683 * ___U24this_2;

public:
	inline static int32_t get_offset_of_timer_0() { return static_cast<int32_t>(offsetof(U3CRuleFadeOutU3Ec__AnonStorey1_t2281953346, ___timer_0)); }
	inline Timer_t2904185433 * get_timer_0() const { return ___timer_0; }
	inline Timer_t2904185433 ** get_address_of_timer_0() { return &___timer_0; }
	inline void set_timer_0(Timer_t2904185433 * value)
	{
		___timer_0 = value;
		Il2CppCodeGenWriteBarrier((&___timer_0), value);
	}

	inline static int32_t get_offset_of_onComplete_1() { return static_cast<int32_t>(offsetof(U3CRuleFadeOutU3Ec__AnonStorey1_t2281953346, ___onComplete_1)); }
	inline Action_t3226471752 * get_onComplete_1() const { return ___onComplete_1; }
	inline Action_t3226471752 ** get_address_of_onComplete_1() { return &___onComplete_1; }
	inline void set_onComplete_1(Action_t3226471752 * value)
	{
		___onComplete_1 = value;
		Il2CppCodeGenWriteBarrier((&___onComplete_1), value);
	}

	inline static int32_t get_offset_of_U24this_2() { return static_cast<int32_t>(offsetof(U3CRuleFadeOutU3Ec__AnonStorey1_t2281953346, ___U24this_2)); }
	inline UguiTransition_t3998485683 * get_U24this_2() const { return ___U24this_2; }
	inline UguiTransition_t3998485683 ** get_address_of_U24this_2() { return &___U24this_2; }
	inline void set_U24this_2(UguiTransition_t3998485683 * value)
	{
		___U24this_2 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CRULEFADEOUTU3EC__ANONSTOREY1_T2281953346_H
#ifndef U3CCOWAITDELAYU3EC__ITERATOR0_T1369630889_H
#define U3CCOWAITDELAYU3EC__ITERATOR0_T1369630889_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.SoundAudio/<CoWaitDelay>c__Iterator0
struct  U3CCoWaitDelayU3Ec__Iterator0_t1369630889  : public RuntimeObject
{
public:
	// System.Single Utage.SoundAudio/<CoWaitDelay>c__Iterator0::delay
	float ___delay_0;
	// System.Single Utage.SoundAudio/<CoWaitDelay>c__Iterator0::fadeInTime
	float ___fadeInTime_1;
	// Utage.SoundAudio Utage.SoundAudio/<CoWaitDelay>c__Iterator0::$this
	SoundAudio_t3759608863 * ___U24this_2;
	// System.Object Utage.SoundAudio/<CoWaitDelay>c__Iterator0::$current
	RuntimeObject * ___U24current_3;
	// System.Boolean Utage.SoundAudio/<CoWaitDelay>c__Iterator0::$disposing
	bool ___U24disposing_4;
	// System.Int32 Utage.SoundAudio/<CoWaitDelay>c__Iterator0::$PC
	int32_t ___U24PC_5;

public:
	inline static int32_t get_offset_of_delay_0() { return static_cast<int32_t>(offsetof(U3CCoWaitDelayU3Ec__Iterator0_t1369630889, ___delay_0)); }
	inline float get_delay_0() const { return ___delay_0; }
	inline float* get_address_of_delay_0() { return &___delay_0; }
	inline void set_delay_0(float value)
	{
		___delay_0 = value;
	}

	inline static int32_t get_offset_of_fadeInTime_1() { return static_cast<int32_t>(offsetof(U3CCoWaitDelayU3Ec__Iterator0_t1369630889, ___fadeInTime_1)); }
	inline float get_fadeInTime_1() const { return ___fadeInTime_1; }
	inline float* get_address_of_fadeInTime_1() { return &___fadeInTime_1; }
	inline void set_fadeInTime_1(float value)
	{
		___fadeInTime_1 = value;
	}

	inline static int32_t get_offset_of_U24this_2() { return static_cast<int32_t>(offsetof(U3CCoWaitDelayU3Ec__Iterator0_t1369630889, ___U24this_2)); }
	inline SoundAudio_t3759608863 * get_U24this_2() const { return ___U24this_2; }
	inline SoundAudio_t3759608863 ** get_address_of_U24this_2() { return &___U24this_2; }
	inline void set_U24this_2(SoundAudio_t3759608863 * value)
	{
		___U24this_2 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_2), value);
	}

	inline static int32_t get_offset_of_U24current_3() { return static_cast<int32_t>(offsetof(U3CCoWaitDelayU3Ec__Iterator0_t1369630889, ___U24current_3)); }
	inline RuntimeObject * get_U24current_3() const { return ___U24current_3; }
	inline RuntimeObject ** get_address_of_U24current_3() { return &___U24current_3; }
	inline void set_U24current_3(RuntimeObject * value)
	{
		___U24current_3 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_3), value);
	}

	inline static int32_t get_offset_of_U24disposing_4() { return static_cast<int32_t>(offsetof(U3CCoWaitDelayU3Ec__Iterator0_t1369630889, ___U24disposing_4)); }
	inline bool get_U24disposing_4() const { return ___U24disposing_4; }
	inline bool* get_address_of_U24disposing_4() { return &___U24disposing_4; }
	inline void set_U24disposing_4(bool value)
	{
		___U24disposing_4 = value;
	}

	inline static int32_t get_offset_of_U24PC_5() { return static_cast<int32_t>(offsetof(U3CCoWaitDelayU3Ec__Iterator0_t1369630889, ___U24PC_5)); }
	inline int32_t get_U24PC_5() const { return ___U24PC_5; }
	inline int32_t* get_address_of_U24PC_5() { return &___U24PC_5; }
	inline void set_U24PC_5(int32_t value)
	{
		___U24PC_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CCOWAITDELAYU3EC__ITERATOR0_T1369630889_H
#ifndef U3CLOADASSETBUNDLEASYNCU3EC__ANONSTOREY4_T2077293016_H
#define U3CLOADASSETBUNDLEASYNCU3EC__ANONSTOREY4_T2077293016_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.WWWEx/<LoadAssetBundleAsync>c__AnonStorey4
struct  U3CLoadAssetBundleAsyncU3Ec__AnonStorey4_t2077293016  : public RuntimeObject
{
public:
	// System.Action`2<UnityEngine.WWW,UnityEngine.AssetBundle> Utage.WWWEx/<LoadAssetBundleAsync>c__AnonStorey4::onComplete
	Action_2_t1479210752 * ___onComplete_0;
	// System.Action`1<UnityEngine.WWW> Utage.WWWEx/<LoadAssetBundleAsync>c__AnonStorey4::onFailed
	Action_1_t2721744421 * ___onFailed_1;
	// Utage.WWWEx Utage.WWWEx/<LoadAssetBundleAsync>c__AnonStorey4::$this
	WWWEx_t1775400116 * ___U24this_2;

public:
	inline static int32_t get_offset_of_onComplete_0() { return static_cast<int32_t>(offsetof(U3CLoadAssetBundleAsyncU3Ec__AnonStorey4_t2077293016, ___onComplete_0)); }
	inline Action_2_t1479210752 * get_onComplete_0() const { return ___onComplete_0; }
	inline Action_2_t1479210752 ** get_address_of_onComplete_0() { return &___onComplete_0; }
	inline void set_onComplete_0(Action_2_t1479210752 * value)
	{
		___onComplete_0 = value;
		Il2CppCodeGenWriteBarrier((&___onComplete_0), value);
	}

	inline static int32_t get_offset_of_onFailed_1() { return static_cast<int32_t>(offsetof(U3CLoadAssetBundleAsyncU3Ec__AnonStorey4_t2077293016, ___onFailed_1)); }
	inline Action_1_t2721744421 * get_onFailed_1() const { return ___onFailed_1; }
	inline Action_1_t2721744421 ** get_address_of_onFailed_1() { return &___onFailed_1; }
	inline void set_onFailed_1(Action_1_t2721744421 * value)
	{
		___onFailed_1 = value;
		Il2CppCodeGenWriteBarrier((&___onFailed_1), value);
	}

	inline static int32_t get_offset_of_U24this_2() { return static_cast<int32_t>(offsetof(U3CLoadAssetBundleAsyncU3Ec__AnonStorey4_t2077293016, ___U24this_2)); }
	inline WWWEx_t1775400116 * get_U24this_2() const { return ___U24this_2; }
	inline WWWEx_t1775400116 ** get_address_of_U24this_2() { return &___U24this_2; }
	inline void set_U24this_2(WWWEx_t1775400116 * value)
	{
		___U24this_2 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CLOADASSETBUNDLEASYNCU3EC__ANONSTOREY4_T2077293016_H
#ifndef CATEGORY_T2702308564_H
#define CATEGORY_T2702308564_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.AvatarData/Category
struct  Category_t2702308564  : public RuntimeObject
{
public:
	// System.String Utage.AvatarData/Category::name
	String_t* ___name_0;
	// System.Int32 Utage.AvatarData/Category::sortOrder
	int32_t ___sortOrder_1;
	// System.String Utage.AvatarData/Category::tag
	String_t* ___tag_2;
	// System.Collections.Generic.List`1<UnityEngine.Sprite> Utage.AvatarData/Category::sprites
	List_1_t3973682211 * ___sprites_3;

public:
	inline static int32_t get_offset_of_name_0() { return static_cast<int32_t>(offsetof(Category_t2702308564, ___name_0)); }
	inline String_t* get_name_0() const { return ___name_0; }
	inline String_t** get_address_of_name_0() { return &___name_0; }
	inline void set_name_0(String_t* value)
	{
		___name_0 = value;
		Il2CppCodeGenWriteBarrier((&___name_0), value);
	}

	inline static int32_t get_offset_of_sortOrder_1() { return static_cast<int32_t>(offsetof(Category_t2702308564, ___sortOrder_1)); }
	inline int32_t get_sortOrder_1() const { return ___sortOrder_1; }
	inline int32_t* get_address_of_sortOrder_1() { return &___sortOrder_1; }
	inline void set_sortOrder_1(int32_t value)
	{
		___sortOrder_1 = value;
	}

	inline static int32_t get_offset_of_tag_2() { return static_cast<int32_t>(offsetof(Category_t2702308564, ___tag_2)); }
	inline String_t* get_tag_2() const { return ___tag_2; }
	inline String_t** get_address_of_tag_2() { return &___tag_2; }
	inline void set_tag_2(String_t* value)
	{
		___tag_2 = value;
		Il2CppCodeGenWriteBarrier((&___tag_2), value);
	}

	inline static int32_t get_offset_of_sprites_3() { return static_cast<int32_t>(offsetof(Category_t2702308564, ___sprites_3)); }
	inline List_1_t3973682211 * get_sprites_3() const { return ___sprites_3; }
	inline List_1_t3973682211 ** get_address_of_sprites_3() { return &___sprites_3; }
	inline void set_sprites_3(List_1_t3973682211 * value)
	{
		___sprites_3 = value;
		Il2CppCodeGenWriteBarrier((&___sprites_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CATEGORY_T2702308564_H
#ifndef U3CGETALLPATTERNNAMESU3EC__ANONSTOREY0_T275193013_H
#define U3CGETALLPATTERNNAMESU3EC__ANONSTOREY0_T275193013_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.AvatarData/Category/<GetAllPatternNames>c__AnonStorey0
struct  U3CGetAllPatternNamesU3Ec__AnonStorey0_t275193013  : public RuntimeObject
{
public:
	// System.Collections.Generic.HashSet`1<System.String> Utage.AvatarData/Category/<GetAllPatternNames>c__AnonStorey0::set
	HashSet_1_t362681087 * ___set_0;

public:
	inline static int32_t get_offset_of_set_0() { return static_cast<int32_t>(offsetof(U3CGetAllPatternNamesU3Ec__AnonStorey0_t275193013, ___set_0)); }
	inline HashSet_1_t362681087 * get_set_0() const { return ___set_0; }
	inline HashSet_1_t362681087 ** get_address_of_set_0() { return &___set_0; }
	inline void set_set_0(HashSet_1_t362681087 * value)
	{
		___set_0 = value;
		Il2CppCodeGenWriteBarrier((&___set_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CGETALLPATTERNNAMESU3EC__ANONSTOREY0_T275193013_H
#ifndef SHADERMANAGER_T3833453370_H
#define SHADERMANAGER_T3833453370_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.ShaderManager
struct  ShaderManager_t3833453370  : public RuntimeObject
{
public:

public:
};

struct ShaderManager_t3833453370_StaticFields
{
public:
	// System.String Utage.ShaderManager::ColorFade
	String_t* ___ColorFade_0;
	// System.String Utage.ShaderManager::BloomName
	String_t* ___BloomName_1;
	// System.String Utage.ShaderManager::BlurName
	String_t* ___BlurName_2;
	// System.String Utage.ShaderManager::MosaicName
	String_t* ___MosaicName_3;
	// System.String Utage.ShaderManager::ColorCorrectionRampName
	String_t* ___ColorCorrectionRampName_4;
	// System.String Utage.ShaderManager::GrayScaleName
	String_t* ___GrayScaleName_5;
	// System.String Utage.ShaderManager::MotionBlurName
	String_t* ___MotionBlurName_6;
	// System.String Utage.ShaderManager::NoiseAndGrainName
	String_t* ___NoiseAndGrainName_7;
	// System.String Utage.ShaderManager::BlendModesOverlayName
	String_t* ___BlendModesOverlayName_8;
	// System.String Utage.ShaderManager::SepiatoneName
	String_t* ___SepiatoneName_9;
	// System.String Utage.ShaderManager::NegaPosiName
	String_t* ___NegaPosiName_10;
	// System.String Utage.ShaderManager::FisheyeName
	String_t* ___FisheyeName_11;
	// System.String Utage.ShaderManager::TwirlName
	String_t* ___TwirlName_12;
	// System.String Utage.ShaderManager::VortexName
	String_t* ___VortexName_13;

public:
	inline static int32_t get_offset_of_ColorFade_0() { return static_cast<int32_t>(offsetof(ShaderManager_t3833453370_StaticFields, ___ColorFade_0)); }
	inline String_t* get_ColorFade_0() const { return ___ColorFade_0; }
	inline String_t** get_address_of_ColorFade_0() { return &___ColorFade_0; }
	inline void set_ColorFade_0(String_t* value)
	{
		___ColorFade_0 = value;
		Il2CppCodeGenWriteBarrier((&___ColorFade_0), value);
	}

	inline static int32_t get_offset_of_BloomName_1() { return static_cast<int32_t>(offsetof(ShaderManager_t3833453370_StaticFields, ___BloomName_1)); }
	inline String_t* get_BloomName_1() const { return ___BloomName_1; }
	inline String_t** get_address_of_BloomName_1() { return &___BloomName_1; }
	inline void set_BloomName_1(String_t* value)
	{
		___BloomName_1 = value;
		Il2CppCodeGenWriteBarrier((&___BloomName_1), value);
	}

	inline static int32_t get_offset_of_BlurName_2() { return static_cast<int32_t>(offsetof(ShaderManager_t3833453370_StaticFields, ___BlurName_2)); }
	inline String_t* get_BlurName_2() const { return ___BlurName_2; }
	inline String_t** get_address_of_BlurName_2() { return &___BlurName_2; }
	inline void set_BlurName_2(String_t* value)
	{
		___BlurName_2 = value;
		Il2CppCodeGenWriteBarrier((&___BlurName_2), value);
	}

	inline static int32_t get_offset_of_MosaicName_3() { return static_cast<int32_t>(offsetof(ShaderManager_t3833453370_StaticFields, ___MosaicName_3)); }
	inline String_t* get_MosaicName_3() const { return ___MosaicName_3; }
	inline String_t** get_address_of_MosaicName_3() { return &___MosaicName_3; }
	inline void set_MosaicName_3(String_t* value)
	{
		___MosaicName_3 = value;
		Il2CppCodeGenWriteBarrier((&___MosaicName_3), value);
	}

	inline static int32_t get_offset_of_ColorCorrectionRampName_4() { return static_cast<int32_t>(offsetof(ShaderManager_t3833453370_StaticFields, ___ColorCorrectionRampName_4)); }
	inline String_t* get_ColorCorrectionRampName_4() const { return ___ColorCorrectionRampName_4; }
	inline String_t** get_address_of_ColorCorrectionRampName_4() { return &___ColorCorrectionRampName_4; }
	inline void set_ColorCorrectionRampName_4(String_t* value)
	{
		___ColorCorrectionRampName_4 = value;
		Il2CppCodeGenWriteBarrier((&___ColorCorrectionRampName_4), value);
	}

	inline static int32_t get_offset_of_GrayScaleName_5() { return static_cast<int32_t>(offsetof(ShaderManager_t3833453370_StaticFields, ___GrayScaleName_5)); }
	inline String_t* get_GrayScaleName_5() const { return ___GrayScaleName_5; }
	inline String_t** get_address_of_GrayScaleName_5() { return &___GrayScaleName_5; }
	inline void set_GrayScaleName_5(String_t* value)
	{
		___GrayScaleName_5 = value;
		Il2CppCodeGenWriteBarrier((&___GrayScaleName_5), value);
	}

	inline static int32_t get_offset_of_MotionBlurName_6() { return static_cast<int32_t>(offsetof(ShaderManager_t3833453370_StaticFields, ___MotionBlurName_6)); }
	inline String_t* get_MotionBlurName_6() const { return ___MotionBlurName_6; }
	inline String_t** get_address_of_MotionBlurName_6() { return &___MotionBlurName_6; }
	inline void set_MotionBlurName_6(String_t* value)
	{
		___MotionBlurName_6 = value;
		Il2CppCodeGenWriteBarrier((&___MotionBlurName_6), value);
	}

	inline static int32_t get_offset_of_NoiseAndGrainName_7() { return static_cast<int32_t>(offsetof(ShaderManager_t3833453370_StaticFields, ___NoiseAndGrainName_7)); }
	inline String_t* get_NoiseAndGrainName_7() const { return ___NoiseAndGrainName_7; }
	inline String_t** get_address_of_NoiseAndGrainName_7() { return &___NoiseAndGrainName_7; }
	inline void set_NoiseAndGrainName_7(String_t* value)
	{
		___NoiseAndGrainName_7 = value;
		Il2CppCodeGenWriteBarrier((&___NoiseAndGrainName_7), value);
	}

	inline static int32_t get_offset_of_BlendModesOverlayName_8() { return static_cast<int32_t>(offsetof(ShaderManager_t3833453370_StaticFields, ___BlendModesOverlayName_8)); }
	inline String_t* get_BlendModesOverlayName_8() const { return ___BlendModesOverlayName_8; }
	inline String_t** get_address_of_BlendModesOverlayName_8() { return &___BlendModesOverlayName_8; }
	inline void set_BlendModesOverlayName_8(String_t* value)
	{
		___BlendModesOverlayName_8 = value;
		Il2CppCodeGenWriteBarrier((&___BlendModesOverlayName_8), value);
	}

	inline static int32_t get_offset_of_SepiatoneName_9() { return static_cast<int32_t>(offsetof(ShaderManager_t3833453370_StaticFields, ___SepiatoneName_9)); }
	inline String_t* get_SepiatoneName_9() const { return ___SepiatoneName_9; }
	inline String_t** get_address_of_SepiatoneName_9() { return &___SepiatoneName_9; }
	inline void set_SepiatoneName_9(String_t* value)
	{
		___SepiatoneName_9 = value;
		Il2CppCodeGenWriteBarrier((&___SepiatoneName_9), value);
	}

	inline static int32_t get_offset_of_NegaPosiName_10() { return static_cast<int32_t>(offsetof(ShaderManager_t3833453370_StaticFields, ___NegaPosiName_10)); }
	inline String_t* get_NegaPosiName_10() const { return ___NegaPosiName_10; }
	inline String_t** get_address_of_NegaPosiName_10() { return &___NegaPosiName_10; }
	inline void set_NegaPosiName_10(String_t* value)
	{
		___NegaPosiName_10 = value;
		Il2CppCodeGenWriteBarrier((&___NegaPosiName_10), value);
	}

	inline static int32_t get_offset_of_FisheyeName_11() { return static_cast<int32_t>(offsetof(ShaderManager_t3833453370_StaticFields, ___FisheyeName_11)); }
	inline String_t* get_FisheyeName_11() const { return ___FisheyeName_11; }
	inline String_t** get_address_of_FisheyeName_11() { return &___FisheyeName_11; }
	inline void set_FisheyeName_11(String_t* value)
	{
		___FisheyeName_11 = value;
		Il2CppCodeGenWriteBarrier((&___FisheyeName_11), value);
	}

	inline static int32_t get_offset_of_TwirlName_12() { return static_cast<int32_t>(offsetof(ShaderManager_t3833453370_StaticFields, ___TwirlName_12)); }
	inline String_t* get_TwirlName_12() const { return ___TwirlName_12; }
	inline String_t** get_address_of_TwirlName_12() { return &___TwirlName_12; }
	inline void set_TwirlName_12(String_t* value)
	{
		___TwirlName_12 = value;
		Il2CppCodeGenWriteBarrier((&___TwirlName_12), value);
	}

	inline static int32_t get_offset_of_VortexName_13() { return static_cast<int32_t>(offsetof(ShaderManager_t3833453370_StaticFields, ___VortexName_13)); }
	inline String_t* get_VortexName_13() const { return ___VortexName_13; }
	inline String_t** get_address_of_VortexName_13() { return &___VortexName_13; }
	inline void set_VortexName_13(String_t* value)
	{
		___VortexName_13 = value;
		Il2CppCodeGenWriteBarrier((&___VortexName_13), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SHADERMANAGER_T3833453370_H
#ifndef EASING_T1940169557_H
#define EASING_T1940169557_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.Easing
struct  Easing_t1940169557  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EASING_T1940169557_H
#ifndef U3CGETSPRITEU3EC__ANONSTOREY1_T1236054666_H
#define U3CGETSPRITEU3EC__ANONSTOREY1_T1236054666_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.AvatarData/Category/<GetSprite>c__AnonStorey1
struct  U3CGetSpriteU3Ec__AnonStorey1_t1236054666  : public RuntimeObject
{
public:
	// System.String Utage.AvatarData/Category/<GetSprite>c__AnonStorey1::pattern
	String_t* ___pattern_0;

public:
	inline static int32_t get_offset_of_pattern_0() { return static_cast<int32_t>(offsetof(U3CGetSpriteU3Ec__AnonStorey1_t1236054666, ___pattern_0)); }
	inline String_t* get_pattern_0() const { return ___pattern_0; }
	inline String_t** get_address_of_pattern_0() { return &___pattern_0; }
	inline void set_pattern_0(String_t* value)
	{
		___pattern_0 = value;
		Il2CppCodeGenWriteBarrier((&___pattern_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CGETSPRITEU3EC__ANONSTOREY1_T1236054666_H
#ifndef TAGGEDMASTERVOLUME_T2665287385_H
#define TAGGEDMASTERVOLUME_T2665287385_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.SoundManager/TaggedMasterVolume
struct  TaggedMasterVolume_t2665287385  : public RuntimeObject
{
public:
	// System.String Utage.SoundManager/TaggedMasterVolume::tag
	String_t* ___tag_0;
	// System.Single Utage.SoundManager/TaggedMasterVolume::volume
	float ___volume_1;

public:
	inline static int32_t get_offset_of_tag_0() { return static_cast<int32_t>(offsetof(TaggedMasterVolume_t2665287385, ___tag_0)); }
	inline String_t* get_tag_0() const { return ___tag_0; }
	inline String_t** get_address_of_tag_0() { return &___tag_0; }
	inline void set_tag_0(String_t* value)
	{
		___tag_0 = value;
		Il2CppCodeGenWriteBarrier((&___tag_0), value);
	}

	inline static int32_t get_offset_of_volume_1() { return static_cast<int32_t>(offsetof(TaggedMasterVolume_t2665287385, ___volume_1)); }
	inline float get_volume_1() const { return ___volume_1; }
	inline float* get_address_of_volume_1() { return &___volume_1; }
	inline void set_volume_1(float value)
	{
		___volume_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TAGGEDMASTERVOLUME_T2665287385_H
#ifndef U3CCOGAMEEXITU3EC__ITERATOR0_T3048488661_H
#define U3CCOGAMEEXITU3EC__ITERATOR0_T3048488661_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.SystemUi/<CoGameExit>c__Iterator0
struct  U3CCoGameExitU3Ec__Iterator0_t3048488661  : public RuntimeObject
{
public:
	// System.Object Utage.SystemUi/<CoGameExit>c__Iterator0::$current
	RuntimeObject * ___U24current_0;
	// System.Boolean Utage.SystemUi/<CoGameExit>c__Iterator0::$disposing
	bool ___U24disposing_1;
	// System.Int32 Utage.SystemUi/<CoGameExit>c__Iterator0::$PC
	int32_t ___U24PC_2;

public:
	inline static int32_t get_offset_of_U24current_0() { return static_cast<int32_t>(offsetof(U3CCoGameExitU3Ec__Iterator0_t3048488661, ___U24current_0)); }
	inline RuntimeObject * get_U24current_0() const { return ___U24current_0; }
	inline RuntimeObject ** get_address_of_U24current_0() { return &___U24current_0; }
	inline void set_U24current_0(RuntimeObject * value)
	{
		___U24current_0 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_0), value);
	}

	inline static int32_t get_offset_of_U24disposing_1() { return static_cast<int32_t>(offsetof(U3CCoGameExitU3Ec__Iterator0_t3048488661, ___U24disposing_1)); }
	inline bool get_U24disposing_1() const { return ___U24disposing_1; }
	inline bool* get_address_of_U24disposing_1() { return &___U24disposing_1; }
	inline void set_U24disposing_1(bool value)
	{
		___U24disposing_1 = value;
	}

	inline static int32_t get_offset_of_U24PC_2() { return static_cast<int32_t>(offsetof(U3CCoGameExitU3Ec__Iterator0_t3048488661, ___U24PC_2)); }
	inline int32_t get_U24PC_2() const { return ___U24PC_2; }
	inline int32_t* get_address_of_U24PC_2() { return &___U24PC_2; }
	inline void set_U24PC_2(int32_t value)
	{
		___U24PC_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CCOGAMEEXITU3EC__ITERATOR0_T3048488661_H
#ifndef U3CCOUPDATEINFOU3EC__ITERATOR0_T3399782156_H
#define U3CCOUPDATEINFOU3EC__ITERATOR0_T3399782156_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.SystemUiDebugMenu/<CoUpdateInfo>c__Iterator0
struct  U3CCoUpdateInfoU3Ec__Iterator0_t3399782156  : public RuntimeObject
{
public:
	// Utage.SystemUiDebugMenu Utage.SystemUiDebugMenu/<CoUpdateInfo>c__Iterator0::$this
	SystemUiDebugMenu_t1631343349 * ___U24this_0;
	// System.Object Utage.SystemUiDebugMenu/<CoUpdateInfo>c__Iterator0::$current
	RuntimeObject * ___U24current_1;
	// System.Boolean Utage.SystemUiDebugMenu/<CoUpdateInfo>c__Iterator0::$disposing
	bool ___U24disposing_2;
	// System.Int32 Utage.SystemUiDebugMenu/<CoUpdateInfo>c__Iterator0::$PC
	int32_t ___U24PC_3;

public:
	inline static int32_t get_offset_of_U24this_0() { return static_cast<int32_t>(offsetof(U3CCoUpdateInfoU3Ec__Iterator0_t3399782156, ___U24this_0)); }
	inline SystemUiDebugMenu_t1631343349 * get_U24this_0() const { return ___U24this_0; }
	inline SystemUiDebugMenu_t1631343349 ** get_address_of_U24this_0() { return &___U24this_0; }
	inline void set_U24this_0(SystemUiDebugMenu_t1631343349 * value)
	{
		___U24this_0 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_0), value);
	}

	inline static int32_t get_offset_of_U24current_1() { return static_cast<int32_t>(offsetof(U3CCoUpdateInfoU3Ec__Iterator0_t3399782156, ___U24current_1)); }
	inline RuntimeObject * get_U24current_1() const { return ___U24current_1; }
	inline RuntimeObject ** get_address_of_U24current_1() { return &___U24current_1; }
	inline void set_U24current_1(RuntimeObject * value)
	{
		___U24current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_1), value);
	}

	inline static int32_t get_offset_of_U24disposing_2() { return static_cast<int32_t>(offsetof(U3CCoUpdateInfoU3Ec__Iterator0_t3399782156, ___U24disposing_2)); }
	inline bool get_U24disposing_2() const { return ___U24disposing_2; }
	inline bool* get_address_of_U24disposing_2() { return &___U24disposing_2; }
	inline void set_U24disposing_2(bool value)
	{
		___U24disposing_2 = value;
	}

	inline static int32_t get_offset_of_U24PC_3() { return static_cast<int32_t>(offsetof(U3CCoUpdateInfoU3Ec__Iterator0_t3399782156, ___U24PC_3)); }
	inline int32_t get_U24PC_3() const { return ___U24PC_3; }
	inline int32_t* get_address_of_U24PC_3() { return &___U24PC_3; }
	inline void set_U24PC_3(int32_t value)
	{
		___U24PC_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CCOUPDATEINFOU3EC__ITERATOR0_T3399782156_H
#ifndef U3CCOUPDATELOGU3EC__ITERATOR1_T3573605669_H
#define U3CCOUPDATELOGU3EC__ITERATOR1_T3573605669_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.SystemUiDebugMenu/<CoUpdateLog>c__Iterator1
struct  U3CCoUpdateLogU3Ec__Iterator1_t3573605669  : public RuntimeObject
{
public:
	// Utage.SystemUiDebugMenu Utage.SystemUiDebugMenu/<CoUpdateLog>c__Iterator1::$this
	SystemUiDebugMenu_t1631343349 * ___U24this_0;
	// System.Object Utage.SystemUiDebugMenu/<CoUpdateLog>c__Iterator1::$current
	RuntimeObject * ___U24current_1;
	// System.Boolean Utage.SystemUiDebugMenu/<CoUpdateLog>c__Iterator1::$disposing
	bool ___U24disposing_2;
	// System.Int32 Utage.SystemUiDebugMenu/<CoUpdateLog>c__Iterator1::$PC
	int32_t ___U24PC_3;

public:
	inline static int32_t get_offset_of_U24this_0() { return static_cast<int32_t>(offsetof(U3CCoUpdateLogU3Ec__Iterator1_t3573605669, ___U24this_0)); }
	inline SystemUiDebugMenu_t1631343349 * get_U24this_0() const { return ___U24this_0; }
	inline SystemUiDebugMenu_t1631343349 ** get_address_of_U24this_0() { return &___U24this_0; }
	inline void set_U24this_0(SystemUiDebugMenu_t1631343349 * value)
	{
		___U24this_0 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_0), value);
	}

	inline static int32_t get_offset_of_U24current_1() { return static_cast<int32_t>(offsetof(U3CCoUpdateLogU3Ec__Iterator1_t3573605669, ___U24current_1)); }
	inline RuntimeObject * get_U24current_1() const { return ___U24current_1; }
	inline RuntimeObject ** get_address_of_U24current_1() { return &___U24current_1; }
	inline void set_U24current_1(RuntimeObject * value)
	{
		___U24current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_1), value);
	}

	inline static int32_t get_offset_of_U24disposing_2() { return static_cast<int32_t>(offsetof(U3CCoUpdateLogU3Ec__Iterator1_t3573605669, ___U24disposing_2)); }
	inline bool get_U24disposing_2() const { return ___U24disposing_2; }
	inline bool* get_address_of_U24disposing_2() { return &___U24disposing_2; }
	inline void set_U24disposing_2(bool value)
	{
		___U24disposing_2 = value;
	}

	inline static int32_t get_offset_of_U24PC_3() { return static_cast<int32_t>(offsetof(U3CCoUpdateLogU3Ec__Iterator1_t3573605669, ___U24PC_3)); }
	inline int32_t get_U24PC_3() const { return ___U24PC_3; }
	inline int32_t* get_address_of_U24PC_3() { return &___U24PC_3; }
	inline void set_U24PC_3(int32_t value)
	{
		___U24PC_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CCOUPDATELOGU3EC__ITERATOR1_T3573605669_H
#ifndef STRINGGRIDROW_T4193237197_H
#define STRINGGRIDROW_T4193237197_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.StringGridRow
struct  StringGridRow_t4193237197  : public RuntimeObject
{
public:
	// Utage.StringGrid Utage.StringGridRow::grid
	StringGrid_t1872153679 * ___grid_0;
	// System.Int32 Utage.StringGridRow::rowIndex
	int32_t ___rowIndex_1;
	// System.Int32 Utage.StringGridRow::debugIndex
	int32_t ___debugIndex_2;
	// System.String[] Utage.StringGridRow::strings
	StringU5BU5D_t1642385972* ___strings_3;
	// System.Boolean Utage.StringGridRow::isEmpty
	bool ___isEmpty_4;
	// System.Boolean Utage.StringGridRow::isCommentOut
	bool ___isCommentOut_5;
	// System.String Utage.StringGridRow::debugInfo
	String_t* ___debugInfo_6;

public:
	inline static int32_t get_offset_of_grid_0() { return static_cast<int32_t>(offsetof(StringGridRow_t4193237197, ___grid_0)); }
	inline StringGrid_t1872153679 * get_grid_0() const { return ___grid_0; }
	inline StringGrid_t1872153679 ** get_address_of_grid_0() { return &___grid_0; }
	inline void set_grid_0(StringGrid_t1872153679 * value)
	{
		___grid_0 = value;
		Il2CppCodeGenWriteBarrier((&___grid_0), value);
	}

	inline static int32_t get_offset_of_rowIndex_1() { return static_cast<int32_t>(offsetof(StringGridRow_t4193237197, ___rowIndex_1)); }
	inline int32_t get_rowIndex_1() const { return ___rowIndex_1; }
	inline int32_t* get_address_of_rowIndex_1() { return &___rowIndex_1; }
	inline void set_rowIndex_1(int32_t value)
	{
		___rowIndex_1 = value;
	}

	inline static int32_t get_offset_of_debugIndex_2() { return static_cast<int32_t>(offsetof(StringGridRow_t4193237197, ___debugIndex_2)); }
	inline int32_t get_debugIndex_2() const { return ___debugIndex_2; }
	inline int32_t* get_address_of_debugIndex_2() { return &___debugIndex_2; }
	inline void set_debugIndex_2(int32_t value)
	{
		___debugIndex_2 = value;
	}

	inline static int32_t get_offset_of_strings_3() { return static_cast<int32_t>(offsetof(StringGridRow_t4193237197, ___strings_3)); }
	inline StringU5BU5D_t1642385972* get_strings_3() const { return ___strings_3; }
	inline StringU5BU5D_t1642385972** get_address_of_strings_3() { return &___strings_3; }
	inline void set_strings_3(StringU5BU5D_t1642385972* value)
	{
		___strings_3 = value;
		Il2CppCodeGenWriteBarrier((&___strings_3), value);
	}

	inline static int32_t get_offset_of_isEmpty_4() { return static_cast<int32_t>(offsetof(StringGridRow_t4193237197, ___isEmpty_4)); }
	inline bool get_isEmpty_4() const { return ___isEmpty_4; }
	inline bool* get_address_of_isEmpty_4() { return &___isEmpty_4; }
	inline void set_isEmpty_4(bool value)
	{
		___isEmpty_4 = value;
	}

	inline static int32_t get_offset_of_isCommentOut_5() { return static_cast<int32_t>(offsetof(StringGridRow_t4193237197, ___isCommentOut_5)); }
	inline bool get_isCommentOut_5() const { return ___isCommentOut_5; }
	inline bool* get_address_of_isCommentOut_5() { return &___isCommentOut_5; }
	inline void set_isCommentOut_5(bool value)
	{
		___isCommentOut_5 = value;
	}

	inline static int32_t get_offset_of_debugInfo_6() { return static_cast<int32_t>(offsetof(StringGridRow_t4193237197, ___debugInfo_6)); }
	inline String_t* get_debugInfo_6() const { return ___debugInfo_6; }
	inline String_t** get_address_of_debugInfo_6() { return &___debugInfo_6; }
	inline void set_debugInfo_6(String_t* value)
	{
		___debugInfo_6 = value;
		Il2CppCodeGenWriteBarrier((&___debugInfo_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STRINGGRIDROW_T4193237197_H
#ifndef U3CCOUPDATEMENUU3EC__ITERATOR2_T1895229701_H
#define U3CCOUPDATEMENUU3EC__ITERATOR2_T1895229701_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.SystemUiDebugMenu/<CoUpdateMenu>c__Iterator2
struct  U3CCoUpdateMenuU3Ec__Iterator2_t1895229701  : public RuntimeObject
{
public:
	// Utage.SystemUiDebugMenu Utage.SystemUiDebugMenu/<CoUpdateMenu>c__Iterator2::$this
	SystemUiDebugMenu_t1631343349 * ___U24this_0;
	// System.Object Utage.SystemUiDebugMenu/<CoUpdateMenu>c__Iterator2::$current
	RuntimeObject * ___U24current_1;
	// System.Boolean Utage.SystemUiDebugMenu/<CoUpdateMenu>c__Iterator2::$disposing
	bool ___U24disposing_2;
	// System.Int32 Utage.SystemUiDebugMenu/<CoUpdateMenu>c__Iterator2::$PC
	int32_t ___U24PC_3;

public:
	inline static int32_t get_offset_of_U24this_0() { return static_cast<int32_t>(offsetof(U3CCoUpdateMenuU3Ec__Iterator2_t1895229701, ___U24this_0)); }
	inline SystemUiDebugMenu_t1631343349 * get_U24this_0() const { return ___U24this_0; }
	inline SystemUiDebugMenu_t1631343349 ** get_address_of_U24this_0() { return &___U24this_0; }
	inline void set_U24this_0(SystemUiDebugMenu_t1631343349 * value)
	{
		___U24this_0 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_0), value);
	}

	inline static int32_t get_offset_of_U24current_1() { return static_cast<int32_t>(offsetof(U3CCoUpdateMenuU3Ec__Iterator2_t1895229701, ___U24current_1)); }
	inline RuntimeObject * get_U24current_1() const { return ___U24current_1; }
	inline RuntimeObject ** get_address_of_U24current_1() { return &___U24current_1; }
	inline void set_U24current_1(RuntimeObject * value)
	{
		___U24current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_1), value);
	}

	inline static int32_t get_offset_of_U24disposing_2() { return static_cast<int32_t>(offsetof(U3CCoUpdateMenuU3Ec__Iterator2_t1895229701, ___U24disposing_2)); }
	inline bool get_U24disposing_2() const { return ___U24disposing_2; }
	inline bool* get_address_of_U24disposing_2() { return &___U24disposing_2; }
	inline void set_U24disposing_2(bool value)
	{
		___U24disposing_2 = value;
	}

	inline static int32_t get_offset_of_U24PC_3() { return static_cast<int32_t>(offsetof(U3CCoUpdateMenuU3Ec__Iterator2_t1895229701, ___U24PC_3)); }
	inline int32_t get_U24PC_3() const { return ___U24PC_3; }
	inline int32_t* get_address_of_U24PC_3() { return &___U24PC_3; }
	inline void set_U24PC_3(int32_t value)
	{
		___U24PC_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CCOUPDATEMENUU3EC__ITERATOR2_T1895229701_H
#ifndef CHARDATA_T4178071270_H
#define CHARDATA_T4178071270_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.CharData
struct  CharData_t4178071270  : public RuntimeObject
{
public:
	// System.Char Utage.CharData::c
	Il2CppChar ___c_1;
	// System.Int32 Utage.CharData::unityRitchTextIndex
	int32_t ___unityRitchTextIndex_2;
	// Utage.CharData/CustomCharaInfo Utage.CharData::customInfo
	CustomCharaInfo_t997213109 * ___customInfo_3;

public:
	inline static int32_t get_offset_of_c_1() { return static_cast<int32_t>(offsetof(CharData_t4178071270, ___c_1)); }
	inline Il2CppChar get_c_1() const { return ___c_1; }
	inline Il2CppChar* get_address_of_c_1() { return &___c_1; }
	inline void set_c_1(Il2CppChar value)
	{
		___c_1 = value;
	}

	inline static int32_t get_offset_of_unityRitchTextIndex_2() { return static_cast<int32_t>(offsetof(CharData_t4178071270, ___unityRitchTextIndex_2)); }
	inline int32_t get_unityRitchTextIndex_2() const { return ___unityRitchTextIndex_2; }
	inline int32_t* get_address_of_unityRitchTextIndex_2() { return &___unityRitchTextIndex_2; }
	inline void set_unityRitchTextIndex_2(int32_t value)
	{
		___unityRitchTextIndex_2 = value;
	}

	inline static int32_t get_offset_of_customInfo_3() { return static_cast<int32_t>(offsetof(CharData_t4178071270, ___customInfo_3)); }
	inline CustomCharaInfo_t997213109 * get_customInfo_3() const { return ___customInfo_3; }
	inline CustomCharaInfo_t997213109 ** get_address_of_customInfo_3() { return &___customInfo_3; }
	inline void set_customInfo_3(CustomCharaInfo_t997213109 * value)
	{
		___customInfo_3 = value;
		Il2CppCodeGenWriteBarrier((&___customInfo_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CHARDATA_T4178071270_H
#ifndef U3CCOTIMERU3EC__ITERATOR0_T3483898792_H
#define U3CCOTIMERU3EC__ITERATOR0_T3483898792_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.Timer/<CoTimer>c__Iterator0
struct  U3CCoTimerU3Ec__Iterator0_t3483898792  : public RuntimeObject
{
public:
	// System.Single Utage.Timer/<CoTimer>c__Iterator0::duration
	float ___duration_0;
	// System.Single Utage.Timer/<CoTimer>c__Iterator0::delay
	float ___delay_1;
	// Utage.WaitTimer Utage.Timer/<CoTimer>c__Iterator0::<timer>__0
	WaitTimer_t2776916572 * ___U3CtimerU3E__0_2;
	// Utage.Timer Utage.Timer/<CoTimer>c__Iterator0::$this
	Timer_t2904185433 * ___U24this_3;
	// System.Object Utage.Timer/<CoTimer>c__Iterator0::$current
	RuntimeObject * ___U24current_4;
	// System.Boolean Utage.Timer/<CoTimer>c__Iterator0::$disposing
	bool ___U24disposing_5;
	// System.Int32 Utage.Timer/<CoTimer>c__Iterator0::$PC
	int32_t ___U24PC_6;

public:
	inline static int32_t get_offset_of_duration_0() { return static_cast<int32_t>(offsetof(U3CCoTimerU3Ec__Iterator0_t3483898792, ___duration_0)); }
	inline float get_duration_0() const { return ___duration_0; }
	inline float* get_address_of_duration_0() { return &___duration_0; }
	inline void set_duration_0(float value)
	{
		___duration_0 = value;
	}

	inline static int32_t get_offset_of_delay_1() { return static_cast<int32_t>(offsetof(U3CCoTimerU3Ec__Iterator0_t3483898792, ___delay_1)); }
	inline float get_delay_1() const { return ___delay_1; }
	inline float* get_address_of_delay_1() { return &___delay_1; }
	inline void set_delay_1(float value)
	{
		___delay_1 = value;
	}

	inline static int32_t get_offset_of_U3CtimerU3E__0_2() { return static_cast<int32_t>(offsetof(U3CCoTimerU3Ec__Iterator0_t3483898792, ___U3CtimerU3E__0_2)); }
	inline WaitTimer_t2776916572 * get_U3CtimerU3E__0_2() const { return ___U3CtimerU3E__0_2; }
	inline WaitTimer_t2776916572 ** get_address_of_U3CtimerU3E__0_2() { return &___U3CtimerU3E__0_2; }
	inline void set_U3CtimerU3E__0_2(WaitTimer_t2776916572 * value)
	{
		___U3CtimerU3E__0_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CtimerU3E__0_2), value);
	}

	inline static int32_t get_offset_of_U24this_3() { return static_cast<int32_t>(offsetof(U3CCoTimerU3Ec__Iterator0_t3483898792, ___U24this_3)); }
	inline Timer_t2904185433 * get_U24this_3() const { return ___U24this_3; }
	inline Timer_t2904185433 ** get_address_of_U24this_3() { return &___U24this_3; }
	inline void set_U24this_3(Timer_t2904185433 * value)
	{
		___U24this_3 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_3), value);
	}

	inline static int32_t get_offset_of_U24current_4() { return static_cast<int32_t>(offsetof(U3CCoTimerU3Ec__Iterator0_t3483898792, ___U24current_4)); }
	inline RuntimeObject * get_U24current_4() const { return ___U24current_4; }
	inline RuntimeObject ** get_address_of_U24current_4() { return &___U24current_4; }
	inline void set_U24current_4(RuntimeObject * value)
	{
		___U24current_4 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_4), value);
	}

	inline static int32_t get_offset_of_U24disposing_5() { return static_cast<int32_t>(offsetof(U3CCoTimerU3Ec__Iterator0_t3483898792, ___U24disposing_5)); }
	inline bool get_U24disposing_5() const { return ___U24disposing_5; }
	inline bool* get_address_of_U24disposing_5() { return &___U24disposing_5; }
	inline void set_U24disposing_5(bool value)
	{
		___U24disposing_5 = value;
	}

	inline static int32_t get_offset_of_U24PC_6() { return static_cast<int32_t>(offsetof(U3CCoTimerU3Ec__Iterator0_t3483898792, ___U24PC_6)); }
	inline int32_t get_U24PC_6() const { return ___U24PC_6; }
	inline int32_t* get_address_of_U24PC_6() { return &___U24PC_6; }
	inline void set_U24PC_6(int32_t value)
	{
		___U24PC_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CCOTIMERU3EC__ITERATOR0_T3483898792_H
#ifndef U3CSETTAGGEDMASTERVOLUMEU3EC__ANONSTOREY0_T1588545342_H
#define U3CSETTAGGEDMASTERVOLUMEU3EC__ANONSTOREY0_T1588545342_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.SoundManager/<SetTaggedMasterVolume>c__AnonStorey0
struct  U3CSetTaggedMasterVolumeU3Ec__AnonStorey0_t1588545342  : public RuntimeObject
{
public:
	// System.String Utage.SoundManager/<SetTaggedMasterVolume>c__AnonStorey0::tag
	String_t* ___tag_0;

public:
	inline static int32_t get_offset_of_tag_0() { return static_cast<int32_t>(offsetof(U3CSetTaggedMasterVolumeU3Ec__AnonStorey0_t1588545342, ___tag_0)); }
	inline String_t* get_tag_0() const { return ___tag_0; }
	inline String_t** get_address_of_tag_0() { return &___tag_0; }
	inline void set_tag_0(String_t* value)
	{
		___tag_0 = value;
		Il2CppCodeGenWriteBarrier((&___tag_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CSETTAGGEDMASTERVOLUMEU3EC__ANONSTOREY0_T1588545342_H
#ifndef U3CPLAYVOICEU3EC__ANONSTOREY1_T1527014591_H
#define U3CPLAYVOICEU3EC__ANONSTOREY1_T1527014591_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.SoundManager/<PlayVoice>c__AnonStorey1
struct  U3CPlayVoiceU3Ec__AnonStorey1_t1527014591  : public RuntimeObject
{
public:
	// System.String Utage.SoundManager/<PlayVoice>c__AnonStorey1::characterLabel
	String_t* ___characterLabel_0;

public:
	inline static int32_t get_offset_of_characterLabel_0() { return static_cast<int32_t>(offsetof(U3CPlayVoiceU3Ec__AnonStorey1_t1527014591, ___characterLabel_0)); }
	inline String_t* get_characterLabel_0() const { return ___characterLabel_0; }
	inline String_t** get_address_of_characterLabel_0() { return &___characterLabel_0; }
	inline void set_characterLabel_0(String_t* value)
	{
		___characterLabel_0 = value;
		Il2CppCodeGenWriteBarrier((&___characterLabel_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CPLAYVOICEU3EC__ANONSTOREY1_T1527014591_H
#ifndef SOUNDMANAGERSYSTEM_T1680680183_H
#define SOUNDMANAGERSYSTEM_T1680680183_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.SoundManagerSystem
struct  SoundManagerSystem_t1680680183  : public RuntimeObject
{
public:
	// System.Collections.Generic.Dictionary`2<System.String,Utage.SoundGroup> Utage.SoundManagerSystem::groups
	Dictionary_2_t3930869258 * ___groups_1;
	// Utage.SoundManager Utage.SoundManagerSystem::<SoundManager>k__BackingField
	SoundManager_t1265124084 * ___U3CSoundManagerU3Ek__BackingField_2;

public:
	inline static int32_t get_offset_of_groups_1() { return static_cast<int32_t>(offsetof(SoundManagerSystem_t1680680183, ___groups_1)); }
	inline Dictionary_2_t3930869258 * get_groups_1() const { return ___groups_1; }
	inline Dictionary_2_t3930869258 ** get_address_of_groups_1() { return &___groups_1; }
	inline void set_groups_1(Dictionary_2_t3930869258 * value)
	{
		___groups_1 = value;
		Il2CppCodeGenWriteBarrier((&___groups_1), value);
	}

	inline static int32_t get_offset_of_U3CSoundManagerU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(SoundManagerSystem_t1680680183, ___U3CSoundManagerU3Ek__BackingField_2)); }
	inline SoundManager_t1265124084 * get_U3CSoundManagerU3Ek__BackingField_2() const { return ___U3CSoundManagerU3Ek__BackingField_2; }
	inline SoundManager_t1265124084 ** get_address_of_U3CSoundManagerU3Ek__BackingField_2() { return &___U3CSoundManagerU3Ek__BackingField_2; }
	inline void set_U3CSoundManagerU3Ek__BackingField_2(SoundManager_t1265124084 * value)
	{
		___U3CSoundManagerU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CSoundManagerU3Ek__BackingField_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SOUNDMANAGERSYSTEM_T1680680183_H
#ifndef TEXTPARSER_T4249442530_H
#define TEXTPARSER_T4249442530_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.TextParser
struct  TextParser_t4249442530  : public RuntimeObject
{
public:
	// System.Collections.Generic.List`1<Utage.CharData> Utage.TextParser::charList
	List_1_t3547192402 * ___charList_3;
	// System.String Utage.TextParser::errorMsg
	String_t* ___errorMsg_5;
	// System.String Utage.TextParser::originalText
	String_t* ___originalText_6;
	// System.String Utage.TextParser::noneMetaString
	String_t* ___noneMetaString_7;
	// System.Int32 Utage.TextParser::currentTextIndex
	int32_t ___currentTextIndex_8;
	// Utage.CharData/CustomCharaInfo Utage.TextParser::customInfo
	CustomCharaInfo_t997213109 * ___customInfo_9;
	// System.Boolean Utage.TextParser::isParseParamOnly
	bool ___isParseParamOnly_10;

public:
	inline static int32_t get_offset_of_charList_3() { return static_cast<int32_t>(offsetof(TextParser_t4249442530, ___charList_3)); }
	inline List_1_t3547192402 * get_charList_3() const { return ___charList_3; }
	inline List_1_t3547192402 ** get_address_of_charList_3() { return &___charList_3; }
	inline void set_charList_3(List_1_t3547192402 * value)
	{
		___charList_3 = value;
		Il2CppCodeGenWriteBarrier((&___charList_3), value);
	}

	inline static int32_t get_offset_of_errorMsg_5() { return static_cast<int32_t>(offsetof(TextParser_t4249442530, ___errorMsg_5)); }
	inline String_t* get_errorMsg_5() const { return ___errorMsg_5; }
	inline String_t** get_address_of_errorMsg_5() { return &___errorMsg_5; }
	inline void set_errorMsg_5(String_t* value)
	{
		___errorMsg_5 = value;
		Il2CppCodeGenWriteBarrier((&___errorMsg_5), value);
	}

	inline static int32_t get_offset_of_originalText_6() { return static_cast<int32_t>(offsetof(TextParser_t4249442530, ___originalText_6)); }
	inline String_t* get_originalText_6() const { return ___originalText_6; }
	inline String_t** get_address_of_originalText_6() { return &___originalText_6; }
	inline void set_originalText_6(String_t* value)
	{
		___originalText_6 = value;
		Il2CppCodeGenWriteBarrier((&___originalText_6), value);
	}

	inline static int32_t get_offset_of_noneMetaString_7() { return static_cast<int32_t>(offsetof(TextParser_t4249442530, ___noneMetaString_7)); }
	inline String_t* get_noneMetaString_7() const { return ___noneMetaString_7; }
	inline String_t** get_address_of_noneMetaString_7() { return &___noneMetaString_7; }
	inline void set_noneMetaString_7(String_t* value)
	{
		___noneMetaString_7 = value;
		Il2CppCodeGenWriteBarrier((&___noneMetaString_7), value);
	}

	inline static int32_t get_offset_of_currentTextIndex_8() { return static_cast<int32_t>(offsetof(TextParser_t4249442530, ___currentTextIndex_8)); }
	inline int32_t get_currentTextIndex_8() const { return ___currentTextIndex_8; }
	inline int32_t* get_address_of_currentTextIndex_8() { return &___currentTextIndex_8; }
	inline void set_currentTextIndex_8(int32_t value)
	{
		___currentTextIndex_8 = value;
	}

	inline static int32_t get_offset_of_customInfo_9() { return static_cast<int32_t>(offsetof(TextParser_t4249442530, ___customInfo_9)); }
	inline CustomCharaInfo_t997213109 * get_customInfo_9() const { return ___customInfo_9; }
	inline CustomCharaInfo_t997213109 ** get_address_of_customInfo_9() { return &___customInfo_9; }
	inline void set_customInfo_9(CustomCharaInfo_t997213109 * value)
	{
		___customInfo_9 = value;
		Il2CppCodeGenWriteBarrier((&___customInfo_9), value);
	}

	inline static int32_t get_offset_of_isParseParamOnly_10() { return static_cast<int32_t>(offsetof(TextParser_t4249442530, ___isParseParamOnly_10)); }
	inline bool get_isParseParamOnly_10() const { return ___isParseParamOnly_10; }
	inline bool* get_address_of_isParseParamOnly_10() { return &___isParseParamOnly_10; }
	inline void set_isParseParamOnly_10(bool value)
	{
		___isParseParamOnly_10 = value;
	}
};

struct TextParser_t4249442530_StaticFields
{
public:
	// System.Func`2<System.String,System.Object> Utage.TextParser::CallbackCalcExpression
	Func_2_t853256019 * ___CallbackCalcExpression_4;
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> Utage.TextParser::<>f__switch$mapA
	Dictionary_2_t3986656710 * ___U3CU3Ef__switchU24mapA_11;

public:
	inline static int32_t get_offset_of_CallbackCalcExpression_4() { return static_cast<int32_t>(offsetof(TextParser_t4249442530_StaticFields, ___CallbackCalcExpression_4)); }
	inline Func_2_t853256019 * get_CallbackCalcExpression_4() const { return ___CallbackCalcExpression_4; }
	inline Func_2_t853256019 ** get_address_of_CallbackCalcExpression_4() { return &___CallbackCalcExpression_4; }
	inline void set_CallbackCalcExpression_4(Func_2_t853256019 * value)
	{
		___CallbackCalcExpression_4 = value;
		Il2CppCodeGenWriteBarrier((&___CallbackCalcExpression_4), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__switchU24mapA_11() { return static_cast<int32_t>(offsetof(TextParser_t4249442530_StaticFields, ___U3CU3Ef__switchU24mapA_11)); }
	inline Dictionary_2_t3986656710 * get_U3CU3Ef__switchU24mapA_11() const { return ___U3CU3Ef__switchU24mapA_11; }
	inline Dictionary_2_t3986656710 ** get_address_of_U3CU3Ef__switchU24mapA_11() { return &___U3CU3Ef__switchU24mapA_11; }
	inline void set_U3CU3Ef__switchU24mapA_11(Dictionary_2_t3986656710 * value)
	{
		___U3CU3Ef__switchU24mapA_11 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__switchU24mapA_11), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TEXTPARSER_T4249442530_H
#ifndef TEXTDATA_T603454315_H
#define TEXTDATA_T603454315_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.TextData
struct  TextData_t603454315  : public RuntimeObject
{
public:
	// Utage.TextParser Utage.TextData::<ParsedText>k__BackingField
	TextParser_t4249442530 * ___U3CParsedTextU3Ek__BackingField_0;
	// System.Boolean Utage.TextData::<ContainsSpeedTag>k__BackingField
	bool ___U3CContainsSpeedTagU3Ek__BackingField_1;
	// System.Boolean Utage.TextData::<IsNoWaitAll>k__BackingField
	bool ___U3CIsNoWaitAllU3Ek__BackingField_2;
	// System.String Utage.TextData::unityRitchText
	String_t* ___unityRitchText_3;

public:
	inline static int32_t get_offset_of_U3CParsedTextU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(TextData_t603454315, ___U3CParsedTextU3Ek__BackingField_0)); }
	inline TextParser_t4249442530 * get_U3CParsedTextU3Ek__BackingField_0() const { return ___U3CParsedTextU3Ek__BackingField_0; }
	inline TextParser_t4249442530 ** get_address_of_U3CParsedTextU3Ek__BackingField_0() { return &___U3CParsedTextU3Ek__BackingField_0; }
	inline void set_U3CParsedTextU3Ek__BackingField_0(TextParser_t4249442530 * value)
	{
		___U3CParsedTextU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CParsedTextU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_U3CContainsSpeedTagU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(TextData_t603454315, ___U3CContainsSpeedTagU3Ek__BackingField_1)); }
	inline bool get_U3CContainsSpeedTagU3Ek__BackingField_1() const { return ___U3CContainsSpeedTagU3Ek__BackingField_1; }
	inline bool* get_address_of_U3CContainsSpeedTagU3Ek__BackingField_1() { return &___U3CContainsSpeedTagU3Ek__BackingField_1; }
	inline void set_U3CContainsSpeedTagU3Ek__BackingField_1(bool value)
	{
		___U3CContainsSpeedTagU3Ek__BackingField_1 = value;
	}

	inline static int32_t get_offset_of_U3CIsNoWaitAllU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(TextData_t603454315, ___U3CIsNoWaitAllU3Ek__BackingField_2)); }
	inline bool get_U3CIsNoWaitAllU3Ek__BackingField_2() const { return ___U3CIsNoWaitAllU3Ek__BackingField_2; }
	inline bool* get_address_of_U3CIsNoWaitAllU3Ek__BackingField_2() { return &___U3CIsNoWaitAllU3Ek__BackingField_2; }
	inline void set_U3CIsNoWaitAllU3Ek__BackingField_2(bool value)
	{
		___U3CIsNoWaitAllU3Ek__BackingField_2 = value;
	}

	inline static int32_t get_offset_of_unityRitchText_3() { return static_cast<int32_t>(offsetof(TextData_t603454315, ___unityRitchText_3)); }
	inline String_t* get_unityRitchText_3() const { return ___unityRitchText_3; }
	inline String_t** get_address_of_unityRitchText_3() { return &___unityRitchText_3; }
	inline void set_unityRitchText_3(String_t* value)
	{
		___unityRitchText_3 = value;
		Il2CppCodeGenWriteBarrier((&___unityRitchText_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TEXTDATA_T603454315_H
#ifndef U3CMAKESORTEDSPRITESU3EC__ANONSTOREY0_T1816920928_H
#define U3CMAKESORTEDSPRITESU3EC__ANONSTOREY0_T1816920928_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.AvatarData/<MakeSortedSprites>c__AnonStorey0
struct  U3CMakeSortedSpritesU3Ec__AnonStorey0_t1816920928  : public RuntimeObject
{
public:
	// System.String Utage.AvatarData/<MakeSortedSprites>c__AnonStorey0::optionPattern
	String_t* ___optionPattern_0;

public:
	inline static int32_t get_offset_of_optionPattern_0() { return static_cast<int32_t>(offsetof(U3CMakeSortedSpritesU3Ec__AnonStorey0_t1816920928, ___optionPattern_0)); }
	inline String_t* get_optionPattern_0() const { return ___optionPattern_0; }
	inline String_t** get_address_of_optionPattern_0() { return &___optionPattern_0; }
	inline void set_optionPattern_0(String_t* value)
	{
		___optionPattern_0 = value;
		Il2CppCodeGenWriteBarrier((&___optionPattern_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CMAKESORTEDSPRITESU3EC__ANONSTOREY0_T1816920928_H
#ifndef IMAGEEFFECTUTIL_T378442258_H
#define IMAGEEFFECTUTIL_T378442258_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.ImageEffectUtil
struct  ImageEffectUtil_t378442258  : public RuntimeObject
{
public:

public:
};

struct ImageEffectUtil_t378442258_StaticFields
{
public:
	// System.Collections.Generic.List`1<Utage.ImageEffectUtil/ImageEffectPattern> Utage.ImageEffectUtil::patterns
	List_1_t2525152917 * ___patterns_0;

public:
	inline static int32_t get_offset_of_patterns_0() { return static_cast<int32_t>(offsetof(ImageEffectUtil_t378442258_StaticFields, ___patterns_0)); }
	inline List_1_t2525152917 * get_patterns_0() const { return ___patterns_0; }
	inline List_1_t2525152917 ** get_address_of_patterns_0() { return &___patterns_0; }
	inline void set_patterns_0(List_1_t2525152917 * value)
	{
		___patterns_0 = value;
		Il2CppCodeGenWriteBarrier((&___patterns_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IMAGEEFFECTUTIL_T378442258_H
#ifndef SERIALIZABLEDICTIONARYKEYVALUE_T1605592419_H
#define SERIALIZABLEDICTIONARYKEYVALUE_T1605592419_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.SerializableDictionaryKeyValue
struct  SerializableDictionaryKeyValue_t1605592419  : public RuntimeObject
{
public:
	// System.String Utage.SerializableDictionaryKeyValue::key
	String_t* ___key_0;

public:
	inline static int32_t get_offset_of_key_0() { return static_cast<int32_t>(offsetof(SerializableDictionaryKeyValue_t1605592419, ___key_0)); }
	inline String_t* get_key_0() const { return ___key_0; }
	inline String_t** get_address_of_key_0() { return &___key_0; }
	inline void set_key_0(String_t* value)
	{
		___key_0 = value;
		Il2CppCodeGenWriteBarrier((&___key_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SERIALIZABLEDICTIONARYKEYVALUE_T1605592419_H
#ifndef IMAGEEFFECTPATTERN_T3156031785_H
#define IMAGEEFFECTPATTERN_T3156031785_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.ImageEffectUtil/ImageEffectPattern
struct  ImageEffectPattern_t3156031785  : public RuntimeObject
{
public:
	// System.String Utage.ImageEffectUtil/ImageEffectPattern::type
	String_t* ___type_0;
	// System.Type Utage.ImageEffectUtil/ImageEffectPattern::componentType
	Type_t * ___componentType_1;
	// UnityEngine.Shader[] Utage.ImageEffectUtil/ImageEffectPattern::shaders
	ShaderU5BU5D_t1916779366* ___shaders_2;

public:
	inline static int32_t get_offset_of_type_0() { return static_cast<int32_t>(offsetof(ImageEffectPattern_t3156031785, ___type_0)); }
	inline String_t* get_type_0() const { return ___type_0; }
	inline String_t** get_address_of_type_0() { return &___type_0; }
	inline void set_type_0(String_t* value)
	{
		___type_0 = value;
		Il2CppCodeGenWriteBarrier((&___type_0), value);
	}

	inline static int32_t get_offset_of_componentType_1() { return static_cast<int32_t>(offsetof(ImageEffectPattern_t3156031785, ___componentType_1)); }
	inline Type_t * get_componentType_1() const { return ___componentType_1; }
	inline Type_t ** get_address_of_componentType_1() { return &___componentType_1; }
	inline void set_componentType_1(Type_t * value)
	{
		___componentType_1 = value;
		Il2CppCodeGenWriteBarrier((&___componentType_1), value);
	}

	inline static int32_t get_offset_of_shaders_2() { return static_cast<int32_t>(offsetof(ImageEffectPattern_t3156031785, ___shaders_2)); }
	inline ShaderU5BU5D_t1916779366* get_shaders_2() const { return ___shaders_2; }
	inline ShaderU5BU5D_t1916779366** get_address_of_shaders_2() { return &___shaders_2; }
	inline void set_shaders_2(ShaderU5BU5D_t1916779366* value)
	{
		___shaders_2 = value;
		Il2CppCodeGenWriteBarrier((&___shaders_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IMAGEEFFECTPATTERN_T3156031785_H
#ifndef SERIALIZABLEDICTIONARY_1_T4187208014_H
#define SERIALIZABLEDICTIONARY_1_T4187208014_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.SerializableDictionary`1<Utage.StringGridDictionaryKeyValue>
struct  SerializableDictionary_1_t4187208014  : public RuntimeObject
{
public:
	// System.Collections.Generic.List`1<T> Utage.SerializableDictionary`1::list
	List_1_t1589136695 * ___list_0;
	// System.Collections.Generic.Dictionary`2<System.String,T> Utage.SerializableDictionary`1::dictionary
	Dictionary_2_t4134794825 * ___dictionary_1;

public:
	inline static int32_t get_offset_of_list_0() { return static_cast<int32_t>(offsetof(SerializableDictionary_1_t4187208014, ___list_0)); }
	inline List_1_t1589136695 * get_list_0() const { return ___list_0; }
	inline List_1_t1589136695 ** get_address_of_list_0() { return &___list_0; }
	inline void set_list_0(List_1_t1589136695 * value)
	{
		___list_0 = value;
		Il2CppCodeGenWriteBarrier((&___list_0), value);
	}

	inline static int32_t get_offset_of_dictionary_1() { return static_cast<int32_t>(offsetof(SerializableDictionary_1_t4187208014, ___dictionary_1)); }
	inline Dictionary_2_t4134794825 * get_dictionary_1() const { return ___dictionary_1; }
	inline Dictionary_2_t4134794825 ** get_address_of_dictionary_1() { return &___dictionary_1; }
	inline void set_dictionary_1(Dictionary_2_t4134794825 * value)
	{
		___dictionary_1 = value;
		Il2CppCodeGenWriteBarrier((&___dictionary_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SERIALIZABLEDICTIONARY_1_T4187208014_H
#ifndef CUSTOMYIELDINSTRUCTION_T1786092740_H
#define CUSTOMYIELDINSTRUCTION_T1786092740_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.CustomYieldInstruction
struct  CustomYieldInstruction_t1786092740  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CUSTOMYIELDINSTRUCTION_T1786092740_H
#ifndef U3CTRYPARSEU3EC__ANONSTOREY0_T2065039102_H
#define U3CTRYPARSEU3EC__ANONSTOREY0_T2065039102_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.ImageEffectUtil/<TryParse>c__AnonStorey0
struct  U3CTryParseU3Ec__AnonStorey0_t2065039102  : public RuntimeObject
{
public:
	// System.String Utage.ImageEffectUtil/<TryParse>c__AnonStorey0::type
	String_t* ___type_0;

public:
	inline static int32_t get_offset_of_type_0() { return static_cast<int32_t>(offsetof(U3CTryParseU3Ec__AnonStorey0_t2065039102, ___type_0)); }
	inline String_t* get_type_0() const { return ___type_0; }
	inline String_t** get_address_of_type_0() { return &___type_0; }
	inline void set_type_0(String_t* value)
	{
		___type_0 = value;
		Il2CppCodeGenWriteBarrier((&___type_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CTRYPARSEU3EC__ANONSTOREY0_T2065039102_H
#ifndef U3CTOIMAGEEFFECTTYPEU3EC__ANONSTOREY1_T491534822_H
#define U3CTOIMAGEEFFECTTYPEU3EC__ANONSTOREY1_T491534822_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.ImageEffectUtil/<ToImageEffectType>c__AnonStorey1
struct  U3CToImageEffectTypeU3Ec__AnonStorey1_t491534822  : public RuntimeObject
{
public:
	// System.Type Utage.ImageEffectUtil/<ToImageEffectType>c__AnonStorey1::ComponentType
	Type_t * ___ComponentType_0;

public:
	inline static int32_t get_offset_of_ComponentType_0() { return static_cast<int32_t>(offsetof(U3CToImageEffectTypeU3Ec__AnonStorey1_t491534822, ___ComponentType_0)); }
	inline Type_t * get_ComponentType_0() const { return ___ComponentType_0; }
	inline Type_t ** get_address_of_ComponentType_0() { return &___ComponentType_0; }
	inline void set_ComponentType_0(Type_t * value)
	{
		___ComponentType_0 = value;
		Il2CppCodeGenWriteBarrier((&___ComponentType_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CTOIMAGEEFFECTTYPEU3EC__ANONSTOREY1_T491534822_H
#ifndef VALUETYPE_T3507792607_H
#define VALUETYPE_T3507792607_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ValueType
struct  ValueType_t3507792607  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t3507792607_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t3507792607_marshaled_com
{
};
#endif // VALUETYPE_T3507792607_H
#ifndef AVATARPATTERN_T4052248955_H
#define AVATARPATTERN_T4052248955_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.AvatarPattern
struct  AvatarPattern_t4052248955  : public RuntimeObject
{
public:
	// System.Collections.Generic.List`1<Utage.AvatarPattern/PartternData> Utage.AvatarPattern::avatarPatternDataList
	List_1_t2549837370 * ___avatarPatternDataList_0;
	// System.Collections.Generic.List`1<System.String> Utage.AvatarPattern::optionPatternNameList
	List_1_t1398341365 * ___optionPatternNameList_1;

public:
	inline static int32_t get_offset_of_avatarPatternDataList_0() { return static_cast<int32_t>(offsetof(AvatarPattern_t4052248955, ___avatarPatternDataList_0)); }
	inline List_1_t2549837370 * get_avatarPatternDataList_0() const { return ___avatarPatternDataList_0; }
	inline List_1_t2549837370 ** get_address_of_avatarPatternDataList_0() { return &___avatarPatternDataList_0; }
	inline void set_avatarPatternDataList_0(List_1_t2549837370 * value)
	{
		___avatarPatternDataList_0 = value;
		Il2CppCodeGenWriteBarrier((&___avatarPatternDataList_0), value);
	}

	inline static int32_t get_offset_of_optionPatternNameList_1() { return static_cast<int32_t>(offsetof(AvatarPattern_t4052248955, ___optionPatternNameList_1)); }
	inline List_1_t1398341365 * get_optionPatternNameList_1() const { return ___optionPatternNameList_1; }
	inline List_1_t1398341365 ** get_address_of_optionPatternNameList_1() { return &___optionPatternNameList_1; }
	inline void set_optionPatternNameList_1(List_1_t1398341365 * value)
	{
		___optionPatternNameList_1 = value;
		Il2CppCodeGenWriteBarrier((&___optionPatternNameList_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AVATARPATTERN_T4052248955_H
#ifndef PARTTERNDATA_T3180716238_H
#define PARTTERNDATA_T3180716238_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.AvatarPattern/PartternData
struct  PartternData_t3180716238  : public RuntimeObject
{
public:
	// System.String Utage.AvatarPattern/PartternData::tag
	String_t* ___tag_0;
	// System.String Utage.AvatarPattern/PartternData::patternName
	String_t* ___patternName_1;

public:
	inline static int32_t get_offset_of_tag_0() { return static_cast<int32_t>(offsetof(PartternData_t3180716238, ___tag_0)); }
	inline String_t* get_tag_0() const { return ___tag_0; }
	inline String_t** get_address_of_tag_0() { return &___tag_0; }
	inline void set_tag_0(String_t* value)
	{
		___tag_0 = value;
		Il2CppCodeGenWriteBarrier((&___tag_0), value);
	}

	inline static int32_t get_offset_of_patternName_1() { return static_cast<int32_t>(offsetof(PartternData_t3180716238, ___patternName_1)); }
	inline String_t* get_patternName_1() const { return ___patternName_1; }
	inline String_t** get_address_of_patternName_1() { return &___patternName_1; }
	inline void set_patternName_1(String_t* value)
	{
		___patternName_1 = value;
		Il2CppCodeGenWriteBarrier((&___patternName_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PARTTERNDATA_T3180716238_H
#ifndef U3CSETPATTERNNAMEU3EC__ANONSTOREY0_T1376057134_H
#define U3CSETPATTERNNAMEU3EC__ANONSTOREY0_T1376057134_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.AvatarPattern/<SetPatternName>c__AnonStorey0
struct  U3CSetPatternNameU3Ec__AnonStorey0_t1376057134  : public RuntimeObject
{
public:
	// System.String Utage.AvatarPattern/<SetPatternName>c__AnonStorey0::tag
	String_t* ___tag_0;

public:
	inline static int32_t get_offset_of_tag_0() { return static_cast<int32_t>(offsetof(U3CSetPatternNameU3Ec__AnonStorey0_t1376057134, ___tag_0)); }
	inline String_t* get_tag_0() const { return ___tag_0; }
	inline String_t** get_address_of_tag_0() { return &___tag_0; }
	inline void set_tag_0(String_t* value)
	{
		___tag_0 = value;
		Il2CppCodeGenWriteBarrier((&___tag_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CSETPATTERNNAMEU3EC__ANONSTOREY0_T1376057134_H
#ifndef U3CGETPATTERNNAMEU3EC__ANONSTOREY1_T263092357_H
#define U3CGETPATTERNNAMEU3EC__ANONSTOREY1_T263092357_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.AvatarPattern/<GetPatternName>c__AnonStorey1
struct  U3CGetPatternNameU3Ec__AnonStorey1_t263092357  : public RuntimeObject
{
public:
	// System.String Utage.AvatarPattern/<GetPatternName>c__AnonStorey1::tag
	String_t* ___tag_0;

public:
	inline static int32_t get_offset_of_tag_0() { return static_cast<int32_t>(offsetof(U3CGetPatternNameU3Ec__AnonStorey1_t263092357, ___tag_0)); }
	inline String_t* get_tag_0() const { return ___tag_0; }
	inline String_t** get_address_of_tag_0() { return &___tag_0; }
	inline void set_tag_0(String_t* value)
	{
		___tag_0 = value;
		Il2CppCodeGenWriteBarrier((&___tag_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CGETPATTERNNAMEU3EC__ANONSTOREY1_T263092357_H
#ifndef U3CREBUILDU3EC__ANONSTOREY3_T628083871_H
#define U3CREBUILDU3EC__ANONSTOREY3_T628083871_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.AvatarPattern/<Rebuild>c__AnonStorey3
struct  U3CRebuildU3Ec__AnonStorey3_t628083871  : public RuntimeObject
{
public:
	// Utage.AvatarData/Category Utage.AvatarPattern/<Rebuild>c__AnonStorey3::category
	Category_t2702308564 * ___category_0;

public:
	inline static int32_t get_offset_of_category_0() { return static_cast<int32_t>(offsetof(U3CRebuildU3Ec__AnonStorey3_t628083871, ___category_0)); }
	inline Category_t2702308564 * get_category_0() const { return ___category_0; }
	inline Category_t2702308564 ** get_address_of_category_0() { return &___category_0; }
	inline void set_category_0(Category_t2702308564 * value)
	{
		___category_0 = value;
		Il2CppCodeGenWriteBarrier((&___category_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CREBUILDU3EC__ANONSTOREY3_T628083871_H
#ifndef ATTRIBUTE_T542643598_H
#define ATTRIBUTE_T542643598_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Attribute
struct  Attribute_t542643598  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ATTRIBUTE_T542643598_H
#ifndef UNITYEVENTBASE_T828812576_H
#define UNITYEVENTBASE_T828812576_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Events.UnityEventBase
struct  UnityEventBase_t828812576  : public RuntimeObject
{
public:
	// UnityEngine.Events.InvokableCallList UnityEngine.Events.UnityEventBase::m_Calls
	InvokableCallList_t2295673753 * ___m_Calls_0;
	// UnityEngine.Events.PersistentCallGroup UnityEngine.Events.UnityEventBase::m_PersistentCalls
	PersistentCallGroup_t339478082 * ___m_PersistentCalls_1;
	// System.String UnityEngine.Events.UnityEventBase::m_TypeName
	String_t* ___m_TypeName_2;
	// System.Boolean UnityEngine.Events.UnityEventBase::m_CallsDirty
	bool ___m_CallsDirty_3;

public:
	inline static int32_t get_offset_of_m_Calls_0() { return static_cast<int32_t>(offsetof(UnityEventBase_t828812576, ___m_Calls_0)); }
	inline InvokableCallList_t2295673753 * get_m_Calls_0() const { return ___m_Calls_0; }
	inline InvokableCallList_t2295673753 ** get_address_of_m_Calls_0() { return &___m_Calls_0; }
	inline void set_m_Calls_0(InvokableCallList_t2295673753 * value)
	{
		___m_Calls_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_Calls_0), value);
	}

	inline static int32_t get_offset_of_m_PersistentCalls_1() { return static_cast<int32_t>(offsetof(UnityEventBase_t828812576, ___m_PersistentCalls_1)); }
	inline PersistentCallGroup_t339478082 * get_m_PersistentCalls_1() const { return ___m_PersistentCalls_1; }
	inline PersistentCallGroup_t339478082 ** get_address_of_m_PersistentCalls_1() { return &___m_PersistentCalls_1; }
	inline void set_m_PersistentCalls_1(PersistentCallGroup_t339478082 * value)
	{
		___m_PersistentCalls_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_PersistentCalls_1), value);
	}

	inline static int32_t get_offset_of_m_TypeName_2() { return static_cast<int32_t>(offsetof(UnityEventBase_t828812576, ___m_TypeName_2)); }
	inline String_t* get_m_TypeName_2() const { return ___m_TypeName_2; }
	inline String_t** get_address_of_m_TypeName_2() { return &___m_TypeName_2; }
	inline void set_m_TypeName_2(String_t* value)
	{
		___m_TypeName_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_TypeName_2), value);
	}

	inline static int32_t get_offset_of_m_CallsDirty_3() { return static_cast<int32_t>(offsetof(UnityEventBase_t828812576, ___m_CallsDirty_3)); }
	inline bool get_m_CallsDirty_3() const { return ___m_CallsDirty_3; }
	inline bool* get_address_of_m_CallsDirty_3() { return &___m_CallsDirty_3; }
	inline void set_m_CallsDirty_3(bool value)
	{
		___m_CallsDirty_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYEVENTBASE_T828812576_H
#ifndef INTPTR_T_H
#define INTPTR_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTPTR_T_H
#ifndef ENUMERATOR_T3818772868_H
#define ENUMERATOR_T3818772868_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.List`1/Enumerator<Utage.MiniAnimationData/Data>
struct  Enumerator_t3818772868 
{
public:
	// System.Collections.Generic.List`1<T> System.Collections.Generic.List`1/Enumerator::l
	List_1_t4284043194 * ___l_0;
	// System.Int32 System.Collections.Generic.List`1/Enumerator::next
	int32_t ___next_1;
	// System.Int32 System.Collections.Generic.List`1/Enumerator::ver
	int32_t ___ver_2;
	// T System.Collections.Generic.List`1/Enumerator::current
	Data_t619954766 * ___current_3;

public:
	inline static int32_t get_offset_of_l_0() { return static_cast<int32_t>(offsetof(Enumerator_t3818772868, ___l_0)); }
	inline List_1_t4284043194 * get_l_0() const { return ___l_0; }
	inline List_1_t4284043194 ** get_address_of_l_0() { return &___l_0; }
	inline void set_l_0(List_1_t4284043194 * value)
	{
		___l_0 = value;
		Il2CppCodeGenWriteBarrier((&___l_0), value);
	}

	inline static int32_t get_offset_of_next_1() { return static_cast<int32_t>(offsetof(Enumerator_t3818772868, ___next_1)); }
	inline int32_t get_next_1() const { return ___next_1; }
	inline int32_t* get_address_of_next_1() { return &___next_1; }
	inline void set_next_1(int32_t value)
	{
		___next_1 = value;
	}

	inline static int32_t get_offset_of_ver_2() { return static_cast<int32_t>(offsetof(Enumerator_t3818772868, ___ver_2)); }
	inline int32_t get_ver_2() const { return ___ver_2; }
	inline int32_t* get_address_of_ver_2() { return &___ver_2; }
	inline void set_ver_2(int32_t value)
	{
		___ver_2 = value;
	}

	inline static int32_t get_offset_of_current_3() { return static_cast<int32_t>(offsetof(Enumerator_t3818772868, ___current_3)); }
	inline Data_t619954766 * get_current_3() const { return ___current_3; }
	inline Data_t619954766 ** get_address_of_current_3() { return &___current_3; }
	inline void set_current_3(Data_t619954766 * value)
	{
		___current_3 = value;
		Il2CppCodeGenWriteBarrier((&___current_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENUMERATOR_T3818772868_H
#ifndef ENUM_T2459695545_H
#define ENUM_T2459695545_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Enum
struct  Enum_t2459695545  : public ValueType_t3507792607
{
public:

public:
};

struct Enum_t2459695545_StaticFields
{
public:
	// System.Char[] System.Enum::split_char
	CharU5BU5D_t1328083999* ___split_char_0;

public:
	inline static int32_t get_offset_of_split_char_0() { return static_cast<int32_t>(offsetof(Enum_t2459695545_StaticFields, ___split_char_0)); }
	inline CharU5BU5D_t1328083999* get_split_char_0() const { return ___split_char_0; }
	inline CharU5BU5D_t1328083999** get_address_of_split_char_0() { return &___split_char_0; }
	inline void set_split_char_0(CharU5BU5D_t1328083999* value)
	{
		___split_char_0 = value;
		Il2CppCodeGenWriteBarrier((&___split_char_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t2459695545_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t2459695545_marshaled_com
{
};
#endif // ENUM_T2459695545_H
#ifndef COLOR_T2020392075_H
#define COLOR_T2020392075_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Color
struct  Color_t2020392075 
{
public:
	// System.Single UnityEngine.Color::r
	float ___r_0;
	// System.Single UnityEngine.Color::g
	float ___g_1;
	// System.Single UnityEngine.Color::b
	float ___b_2;
	// System.Single UnityEngine.Color::a
	float ___a_3;

public:
	inline static int32_t get_offset_of_r_0() { return static_cast<int32_t>(offsetof(Color_t2020392075, ___r_0)); }
	inline float get_r_0() const { return ___r_0; }
	inline float* get_address_of_r_0() { return &___r_0; }
	inline void set_r_0(float value)
	{
		___r_0 = value;
	}

	inline static int32_t get_offset_of_g_1() { return static_cast<int32_t>(offsetof(Color_t2020392075, ___g_1)); }
	inline float get_g_1() const { return ___g_1; }
	inline float* get_address_of_g_1() { return &___g_1; }
	inline void set_g_1(float value)
	{
		___g_1 = value;
	}

	inline static int32_t get_offset_of_b_2() { return static_cast<int32_t>(offsetof(Color_t2020392075, ___b_2)); }
	inline float get_b_2() const { return ___b_2; }
	inline float* get_address_of_b_2() { return &___b_2; }
	inline void set_b_2(float value)
	{
		___b_2 = value;
	}

	inline static int32_t get_offset_of_a_3() { return static_cast<int32_t>(offsetof(Color_t2020392075, ___a_3)); }
	inline float get_a_3() const { return ___a_3; }
	inline float* get_address_of_a_3() { return &___a_3; }
	inline void set_a_3(float value)
	{
		___a_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLOR_T2020392075_H
#ifndef VECTOR2_T2243707579_H
#define VECTOR2_T2243707579_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector2
struct  Vector2_t2243707579 
{
public:
	// System.Single UnityEngine.Vector2::x
	float ___x_0;
	// System.Single UnityEngine.Vector2::y
	float ___y_1;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Vector2_t2243707579, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Vector2_t2243707579, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}
};

struct Vector2_t2243707579_StaticFields
{
public:
	// UnityEngine.Vector2 UnityEngine.Vector2::zeroVector
	Vector2_t2243707579  ___zeroVector_2;
	// UnityEngine.Vector2 UnityEngine.Vector2::oneVector
	Vector2_t2243707579  ___oneVector_3;
	// UnityEngine.Vector2 UnityEngine.Vector2::upVector
	Vector2_t2243707579  ___upVector_4;
	// UnityEngine.Vector2 UnityEngine.Vector2::downVector
	Vector2_t2243707579  ___downVector_5;
	// UnityEngine.Vector2 UnityEngine.Vector2::leftVector
	Vector2_t2243707579  ___leftVector_6;
	// UnityEngine.Vector2 UnityEngine.Vector2::rightVector
	Vector2_t2243707579  ___rightVector_7;
	// UnityEngine.Vector2 UnityEngine.Vector2::positiveInfinityVector
	Vector2_t2243707579  ___positiveInfinityVector_8;
	// UnityEngine.Vector2 UnityEngine.Vector2::negativeInfinityVector
	Vector2_t2243707579  ___negativeInfinityVector_9;

public:
	inline static int32_t get_offset_of_zeroVector_2() { return static_cast<int32_t>(offsetof(Vector2_t2243707579_StaticFields, ___zeroVector_2)); }
	inline Vector2_t2243707579  get_zeroVector_2() const { return ___zeroVector_2; }
	inline Vector2_t2243707579 * get_address_of_zeroVector_2() { return &___zeroVector_2; }
	inline void set_zeroVector_2(Vector2_t2243707579  value)
	{
		___zeroVector_2 = value;
	}

	inline static int32_t get_offset_of_oneVector_3() { return static_cast<int32_t>(offsetof(Vector2_t2243707579_StaticFields, ___oneVector_3)); }
	inline Vector2_t2243707579  get_oneVector_3() const { return ___oneVector_3; }
	inline Vector2_t2243707579 * get_address_of_oneVector_3() { return &___oneVector_3; }
	inline void set_oneVector_3(Vector2_t2243707579  value)
	{
		___oneVector_3 = value;
	}

	inline static int32_t get_offset_of_upVector_4() { return static_cast<int32_t>(offsetof(Vector2_t2243707579_StaticFields, ___upVector_4)); }
	inline Vector2_t2243707579  get_upVector_4() const { return ___upVector_4; }
	inline Vector2_t2243707579 * get_address_of_upVector_4() { return &___upVector_4; }
	inline void set_upVector_4(Vector2_t2243707579  value)
	{
		___upVector_4 = value;
	}

	inline static int32_t get_offset_of_downVector_5() { return static_cast<int32_t>(offsetof(Vector2_t2243707579_StaticFields, ___downVector_5)); }
	inline Vector2_t2243707579  get_downVector_5() const { return ___downVector_5; }
	inline Vector2_t2243707579 * get_address_of_downVector_5() { return &___downVector_5; }
	inline void set_downVector_5(Vector2_t2243707579  value)
	{
		___downVector_5 = value;
	}

	inline static int32_t get_offset_of_leftVector_6() { return static_cast<int32_t>(offsetof(Vector2_t2243707579_StaticFields, ___leftVector_6)); }
	inline Vector2_t2243707579  get_leftVector_6() const { return ___leftVector_6; }
	inline Vector2_t2243707579 * get_address_of_leftVector_6() { return &___leftVector_6; }
	inline void set_leftVector_6(Vector2_t2243707579  value)
	{
		___leftVector_6 = value;
	}

	inline static int32_t get_offset_of_rightVector_7() { return static_cast<int32_t>(offsetof(Vector2_t2243707579_StaticFields, ___rightVector_7)); }
	inline Vector2_t2243707579  get_rightVector_7() const { return ___rightVector_7; }
	inline Vector2_t2243707579 * get_address_of_rightVector_7() { return &___rightVector_7; }
	inline void set_rightVector_7(Vector2_t2243707579  value)
	{
		___rightVector_7 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_8() { return static_cast<int32_t>(offsetof(Vector2_t2243707579_StaticFields, ___positiveInfinityVector_8)); }
	inline Vector2_t2243707579  get_positiveInfinityVector_8() const { return ___positiveInfinityVector_8; }
	inline Vector2_t2243707579 * get_address_of_positiveInfinityVector_8() { return &___positiveInfinityVector_8; }
	inline void set_positiveInfinityVector_8(Vector2_t2243707579  value)
	{
		___positiveInfinityVector_8 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_9() { return static_cast<int32_t>(offsetof(Vector2_t2243707579_StaticFields, ___negativeInfinityVector_9)); }
	inline Vector2_t2243707579  get_negativeInfinityVector_9() const { return ___negativeInfinityVector_9; }
	inline Vector2_t2243707579 * get_address_of_negativeInfinityVector_9() { return &___negativeInfinityVector_9; }
	inline void set_negativeInfinityVector_9(Vector2_t2243707579  value)
	{
		___negativeInfinityVector_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR2_T2243707579_H
#ifndef UNITYEVENT_1_T1303474099_H
#define UNITYEVENT_1_T1303474099_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Events.UnityEvent`1<Utage.SoundManager>
struct  UnityEvent_1_t1303474099  : public UnityEventBase_t828812576
{
public:
	// System.Object[] UnityEngine.Events.UnityEvent`1::m_InvokeArray
	ObjectU5BU5D_t3614634134* ___m_InvokeArray_4;

public:
	inline static int32_t get_offset_of_m_InvokeArray_4() { return static_cast<int32_t>(offsetof(UnityEvent_1_t1303474099, ___m_InvokeArray_4)); }
	inline ObjectU5BU5D_t3614634134* get_m_InvokeArray_4() const { return ___m_InvokeArray_4; }
	inline ObjectU5BU5D_t3614634134** get_address_of_m_InvokeArray_4() { return &___m_InvokeArray_4; }
	inline void set_m_InvokeArray_4(ObjectU5BU5D_t3614634134* value)
	{
		___m_InvokeArray_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_InvokeArray_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYEVENT_1_T1303474099_H
#ifndef WAITTIMER_T2776916572_H
#define WAITTIMER_T2776916572_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.WaitTimer
struct  WaitTimer_t2776916572  : public CustomYieldInstruction_t1786092740
{
public:
	// System.Single Utage.WaitTimer::duration
	float ___duration_0;
	// System.Single Utage.WaitTimer::delay
	float ___delay_1;
	// System.Single Utage.WaitTimer::initTime
	float ___initTime_2;
	// System.Boolean Utage.WaitTimer::isStarted
	bool ___isStarted_3;
	// System.Single Utage.WaitTimer::<Time>k__BackingField
	float ___U3CTimeU3Ek__BackingField_4;
	// System.Single Utage.WaitTimer::<Time01>k__BackingField
	float ___U3CTime01U3Ek__BackingField_5;
	// UnityEngine.Events.UnityAction`1<Utage.WaitTimer> Utage.WaitTimer::onStart
	UnityAction_1_t4143502323 * ___onStart_6;
	// UnityEngine.Events.UnityAction`1<Utage.WaitTimer> Utage.WaitTimer::onUpdate
	UnityAction_1_t4143502323 * ___onUpdate_7;
	// UnityEngine.Events.UnityAction`1<Utage.WaitTimer> Utage.WaitTimer::onComplete
	UnityAction_1_t4143502323 * ___onComplete_8;

public:
	inline static int32_t get_offset_of_duration_0() { return static_cast<int32_t>(offsetof(WaitTimer_t2776916572, ___duration_0)); }
	inline float get_duration_0() const { return ___duration_0; }
	inline float* get_address_of_duration_0() { return &___duration_0; }
	inline void set_duration_0(float value)
	{
		___duration_0 = value;
	}

	inline static int32_t get_offset_of_delay_1() { return static_cast<int32_t>(offsetof(WaitTimer_t2776916572, ___delay_1)); }
	inline float get_delay_1() const { return ___delay_1; }
	inline float* get_address_of_delay_1() { return &___delay_1; }
	inline void set_delay_1(float value)
	{
		___delay_1 = value;
	}

	inline static int32_t get_offset_of_initTime_2() { return static_cast<int32_t>(offsetof(WaitTimer_t2776916572, ___initTime_2)); }
	inline float get_initTime_2() const { return ___initTime_2; }
	inline float* get_address_of_initTime_2() { return &___initTime_2; }
	inline void set_initTime_2(float value)
	{
		___initTime_2 = value;
	}

	inline static int32_t get_offset_of_isStarted_3() { return static_cast<int32_t>(offsetof(WaitTimer_t2776916572, ___isStarted_3)); }
	inline bool get_isStarted_3() const { return ___isStarted_3; }
	inline bool* get_address_of_isStarted_3() { return &___isStarted_3; }
	inline void set_isStarted_3(bool value)
	{
		___isStarted_3 = value;
	}

	inline static int32_t get_offset_of_U3CTimeU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(WaitTimer_t2776916572, ___U3CTimeU3Ek__BackingField_4)); }
	inline float get_U3CTimeU3Ek__BackingField_4() const { return ___U3CTimeU3Ek__BackingField_4; }
	inline float* get_address_of_U3CTimeU3Ek__BackingField_4() { return &___U3CTimeU3Ek__BackingField_4; }
	inline void set_U3CTimeU3Ek__BackingField_4(float value)
	{
		___U3CTimeU3Ek__BackingField_4 = value;
	}

	inline static int32_t get_offset_of_U3CTime01U3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(WaitTimer_t2776916572, ___U3CTime01U3Ek__BackingField_5)); }
	inline float get_U3CTime01U3Ek__BackingField_5() const { return ___U3CTime01U3Ek__BackingField_5; }
	inline float* get_address_of_U3CTime01U3Ek__BackingField_5() { return &___U3CTime01U3Ek__BackingField_5; }
	inline void set_U3CTime01U3Ek__BackingField_5(float value)
	{
		___U3CTime01U3Ek__BackingField_5 = value;
	}

	inline static int32_t get_offset_of_onStart_6() { return static_cast<int32_t>(offsetof(WaitTimer_t2776916572, ___onStart_6)); }
	inline UnityAction_1_t4143502323 * get_onStart_6() const { return ___onStart_6; }
	inline UnityAction_1_t4143502323 ** get_address_of_onStart_6() { return &___onStart_6; }
	inline void set_onStart_6(UnityAction_1_t4143502323 * value)
	{
		___onStart_6 = value;
		Il2CppCodeGenWriteBarrier((&___onStart_6), value);
	}

	inline static int32_t get_offset_of_onUpdate_7() { return static_cast<int32_t>(offsetof(WaitTimer_t2776916572, ___onUpdate_7)); }
	inline UnityAction_1_t4143502323 * get_onUpdate_7() const { return ___onUpdate_7; }
	inline UnityAction_1_t4143502323 ** get_address_of_onUpdate_7() { return &___onUpdate_7; }
	inline void set_onUpdate_7(UnityAction_1_t4143502323 * value)
	{
		___onUpdate_7 = value;
		Il2CppCodeGenWriteBarrier((&___onUpdate_7), value);
	}

	inline static int32_t get_offset_of_onComplete_8() { return static_cast<int32_t>(offsetof(WaitTimer_t2776916572, ___onComplete_8)); }
	inline UnityAction_1_t4143502323 * get_onComplete_8() const { return ___onComplete_8; }
	inline UnityAction_1_t4143502323 ** get_address_of_onComplete_8() { return &___onComplete_8; }
	inline void set_onComplete_8(UnityAction_1_t4143502323 * value)
	{
		___onComplete_8 = value;
		Il2CppCodeGenWriteBarrier((&___onComplete_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WAITTIMER_T2776916572_H
#ifndef UNITYEVENT_1_T2942535448_H
#define UNITYEVENT_1_T2942535448_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Events.UnityEvent`1<Utage.Timer>
struct  UnityEvent_1_t2942535448  : public UnityEventBase_t828812576
{
public:
	// System.Object[] UnityEngine.Events.UnityEvent`1::m_InvokeArray
	ObjectU5BU5D_t3614634134* ___m_InvokeArray_4;

public:
	inline static int32_t get_offset_of_m_InvokeArray_4() { return static_cast<int32_t>(offsetof(UnityEvent_1_t2942535448, ___m_InvokeArray_4)); }
	inline ObjectU5BU5D_t3614634134* get_m_InvokeArray_4() const { return ___m_InvokeArray_4; }
	inline ObjectU5BU5D_t3614634134** get_address_of_m_InvokeArray_4() { return &___m_InvokeArray_4; }
	inline void set_m_InvokeArray_4(ObjectU5BU5D_t3614634134* value)
	{
		___m_InvokeArray_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_InvokeArray_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYEVENT_1_T2942535448_H
#ifndef KEYVALUEPAIR_2_T1744001932_H
#define KEYVALUEPAIR_2_T1744001932_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.KeyValuePair`2<System.String,System.Int32>
struct  KeyValuePair_2_t1744001932 
{
public:
	// TKey System.Collections.Generic.KeyValuePair`2::key
	String_t* ___key_0;
	// TValue System.Collections.Generic.KeyValuePair`2::value
	int32_t ___value_1;

public:
	inline static int32_t get_offset_of_key_0() { return static_cast<int32_t>(offsetof(KeyValuePair_2_t1744001932, ___key_0)); }
	inline String_t* get_key_0() const { return ___key_0; }
	inline String_t** get_address_of_key_0() { return &___key_0; }
	inline void set_key_0(String_t* value)
	{
		___key_0 = value;
		Il2CppCodeGenWriteBarrier((&___key_0), value);
	}

	inline static int32_t get_offset_of_value_1() { return static_cast<int32_t>(offsetof(KeyValuePair_2_t1744001932, ___value_1)); }
	inline int32_t get_value_1() const { return ___value_1; }
	inline int32_t* get_address_of_value_1() { return &___value_1; }
	inline void set_value_1(int32_t value)
	{
		___value_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // KEYVALUEPAIR_2_T1744001932_H
#ifndef PROPERTYATTRIBUTE_T2606999759_H
#define PROPERTYATTRIBUTE_T2606999759_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.PropertyAttribute
struct  PropertyAttribute_t2606999759  : public Attribute_t542643598
{
public:
	// System.Int32 UnityEngine.PropertyAttribute::<order>k__BackingField
	int32_t ___U3CorderU3Ek__BackingField_0;

public:
	inline static int32_t get_offset_of_U3CorderU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(PropertyAttribute_t2606999759, ___U3CorderU3Ek__BackingField_0)); }
	inline int32_t get_U3CorderU3Ek__BackingField_0() const { return ___U3CorderU3Ek__BackingField_0; }
	inline int32_t* get_address_of_U3CorderU3Ek__BackingField_0() { return &___U3CorderU3Ek__BackingField_0; }
	inline void set_U3CorderU3Ek__BackingField_0(int32_t value)
	{
		___U3CorderU3Ek__BackingField_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PROPERTYATTRIBUTE_T2606999759_H
#ifndef STRINGGRIDDICTIONARY_T1396150451_H
#define STRINGGRIDDICTIONARY_T1396150451_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.StringGridDictionary
struct  StringGridDictionary_t1396150451  : public SerializableDictionary_1_t4187208014
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STRINGGRIDDICTIONARY_T1396150451_H
#ifndef STRINGGRIDDICTIONARYKEYVALUE_T2220015563_H
#define STRINGGRIDDICTIONARYKEYVALUE_T2220015563_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.StringGridDictionaryKeyValue
struct  StringGridDictionaryKeyValue_t2220015563  : public SerializableDictionaryKeyValue_t1605592419
{
public:
	// Utage.StringGrid Utage.StringGridDictionaryKeyValue::grid
	StringGrid_t1872153679 * ___grid_1;

public:
	inline static int32_t get_offset_of_grid_1() { return static_cast<int32_t>(offsetof(StringGridDictionaryKeyValue_t2220015563, ___grid_1)); }
	inline StringGrid_t1872153679 * get_grid_1() const { return ___grid_1; }
	inline StringGrid_t1872153679 ** get_address_of_grid_1() { return &___grid_1; }
	inline void set_grid_1(StringGrid_t1872153679 * value)
	{
		___grid_1 = value;
		Il2CppCodeGenWriteBarrier((&___grid_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STRINGGRIDDICTIONARYKEYVALUE_T2220015563_H
#ifndef TIMEREVENT_T3049894269_H
#define TIMEREVENT_T3049894269_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.TimerEvent
struct  TimerEvent_t3049894269  : public UnityEvent_1_t2942535448
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TIMEREVENT_T3049894269_H
#ifndef SOUNDMANAGEREVENT_T3645924073_H
#define SOUNDMANAGEREVENT_T3645924073_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.SoundManager/SoundManagerEvent
struct  SoundManagerEvent_t3645924073  : public UnityEvent_1_t1303474099
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SOUNDMANAGEREVENT_T3645924073_H
#ifndef U3CCOEYEBLINKU3EC__ITERATOR0_T822695095_H
#define U3CCOEYEBLINKU3EC__ITERATOR0_T822695095_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.EyeBlinkAvatar/<CoEyeBlink>c__Iterator0
struct  U3CCoEyeBlinkU3Ec__Iterator0_t822695095  : public RuntimeObject
{
public:
	// System.String Utage.EyeBlinkAvatar/<CoEyeBlink>c__Iterator0::<pattern>__0
	String_t* ___U3CpatternU3E__0_0;
	// System.Collections.Generic.List`1/Enumerator<Utage.MiniAnimationData/Data> Utage.EyeBlinkAvatar/<CoEyeBlink>c__Iterator0::$locvar0
	Enumerator_t3818772868  ___U24locvar0_1;
	// Utage.MiniAnimationData/Data Utage.EyeBlinkAvatar/<CoEyeBlink>c__Iterator0::<data>__1
	Data_t619954766 * ___U3CdataU3E__1_2;
	// System.Action Utage.EyeBlinkAvatar/<CoEyeBlink>c__Iterator0::onComplete
	Action_t3226471752 * ___onComplete_3;
	// Utage.EyeBlinkAvatar Utage.EyeBlinkAvatar/<CoEyeBlink>c__Iterator0::$this
	EyeBlinkAvatar_t3311504802 * ___U24this_4;
	// System.Object Utage.EyeBlinkAvatar/<CoEyeBlink>c__Iterator0::$current
	RuntimeObject * ___U24current_5;
	// System.Boolean Utage.EyeBlinkAvatar/<CoEyeBlink>c__Iterator0::$disposing
	bool ___U24disposing_6;
	// System.Int32 Utage.EyeBlinkAvatar/<CoEyeBlink>c__Iterator0::$PC
	int32_t ___U24PC_7;

public:
	inline static int32_t get_offset_of_U3CpatternU3E__0_0() { return static_cast<int32_t>(offsetof(U3CCoEyeBlinkU3Ec__Iterator0_t822695095, ___U3CpatternU3E__0_0)); }
	inline String_t* get_U3CpatternU3E__0_0() const { return ___U3CpatternU3E__0_0; }
	inline String_t** get_address_of_U3CpatternU3E__0_0() { return &___U3CpatternU3E__0_0; }
	inline void set_U3CpatternU3E__0_0(String_t* value)
	{
		___U3CpatternU3E__0_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CpatternU3E__0_0), value);
	}

	inline static int32_t get_offset_of_U24locvar0_1() { return static_cast<int32_t>(offsetof(U3CCoEyeBlinkU3Ec__Iterator0_t822695095, ___U24locvar0_1)); }
	inline Enumerator_t3818772868  get_U24locvar0_1() const { return ___U24locvar0_1; }
	inline Enumerator_t3818772868 * get_address_of_U24locvar0_1() { return &___U24locvar0_1; }
	inline void set_U24locvar0_1(Enumerator_t3818772868  value)
	{
		___U24locvar0_1 = value;
	}

	inline static int32_t get_offset_of_U3CdataU3E__1_2() { return static_cast<int32_t>(offsetof(U3CCoEyeBlinkU3Ec__Iterator0_t822695095, ___U3CdataU3E__1_2)); }
	inline Data_t619954766 * get_U3CdataU3E__1_2() const { return ___U3CdataU3E__1_2; }
	inline Data_t619954766 ** get_address_of_U3CdataU3E__1_2() { return &___U3CdataU3E__1_2; }
	inline void set_U3CdataU3E__1_2(Data_t619954766 * value)
	{
		___U3CdataU3E__1_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CdataU3E__1_2), value);
	}

	inline static int32_t get_offset_of_onComplete_3() { return static_cast<int32_t>(offsetof(U3CCoEyeBlinkU3Ec__Iterator0_t822695095, ___onComplete_3)); }
	inline Action_t3226471752 * get_onComplete_3() const { return ___onComplete_3; }
	inline Action_t3226471752 ** get_address_of_onComplete_3() { return &___onComplete_3; }
	inline void set_onComplete_3(Action_t3226471752 * value)
	{
		___onComplete_3 = value;
		Il2CppCodeGenWriteBarrier((&___onComplete_3), value);
	}

	inline static int32_t get_offset_of_U24this_4() { return static_cast<int32_t>(offsetof(U3CCoEyeBlinkU3Ec__Iterator0_t822695095, ___U24this_4)); }
	inline EyeBlinkAvatar_t3311504802 * get_U24this_4() const { return ___U24this_4; }
	inline EyeBlinkAvatar_t3311504802 ** get_address_of_U24this_4() { return &___U24this_4; }
	inline void set_U24this_4(EyeBlinkAvatar_t3311504802 * value)
	{
		___U24this_4 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_4), value);
	}

	inline static int32_t get_offset_of_U24current_5() { return static_cast<int32_t>(offsetof(U3CCoEyeBlinkU3Ec__Iterator0_t822695095, ___U24current_5)); }
	inline RuntimeObject * get_U24current_5() const { return ___U24current_5; }
	inline RuntimeObject ** get_address_of_U24current_5() { return &___U24current_5; }
	inline void set_U24current_5(RuntimeObject * value)
	{
		___U24current_5 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_5), value);
	}

	inline static int32_t get_offset_of_U24disposing_6() { return static_cast<int32_t>(offsetof(U3CCoEyeBlinkU3Ec__Iterator0_t822695095, ___U24disposing_6)); }
	inline bool get_U24disposing_6() const { return ___U24disposing_6; }
	inline bool* get_address_of_U24disposing_6() { return &___U24disposing_6; }
	inline void set_U24disposing_6(bool value)
	{
		___U24disposing_6 = value;
	}

	inline static int32_t get_offset_of_U24PC_7() { return static_cast<int32_t>(offsetof(U3CCoEyeBlinkU3Ec__Iterator0_t822695095, ___U24PC_7)); }
	inline int32_t get_U24PC_7() const { return ___U24PC_7; }
	inline int32_t* get_address_of_U24PC_7() { return &___U24PC_7; }
	inline void set_U24PC_7(int32_t value)
	{
		___U24PC_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CCOEYEBLINKU3EC__ITERATOR0_T822695095_H
#ifndef EASETYPE_T1880589884_H
#define EASETYPE_T1880589884_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.EaseType
struct  EaseType_t1880589884 
{
public:
	// System.Int32 Utage.EaseType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(EaseType_t1880589884, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EASETYPE_T1880589884_H
#ifndef NOVELAVATARPATTERNATTRIBUTE_T3689869613_H
#define NOVELAVATARPATTERNATTRIBUTE_T3689869613_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.NovelAvatarPatternAttribute
struct  NovelAvatarPatternAttribute_t3689869613  : public PropertyAttribute_t2606999759
{
public:
	// System.String Utage.NovelAvatarPatternAttribute::<Function>k__BackingField
	String_t* ___U3CFunctionU3Ek__BackingField_1;

public:
	inline static int32_t get_offset_of_U3CFunctionU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(NovelAvatarPatternAttribute_t3689869613, ___U3CFunctionU3Ek__BackingField_1)); }
	inline String_t* get_U3CFunctionU3Ek__BackingField_1() const { return ___U3CFunctionU3Ek__BackingField_1; }
	inline String_t** get_address_of_U3CFunctionU3Ek__BackingField_1() { return &___U3CFunctionU3Ek__BackingField_1; }
	inline void set_U3CFunctionU3Ek__BackingField_1(String_t* value)
	{
		___U3CFunctionU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CFunctionU3Ek__BackingField_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NOVELAVATARPATTERNATTRIBUTE_T3689869613_H
#ifndef SOUNDSTREAMSTATUS_T3426717699_H
#define SOUNDSTREAMSTATUS_T3426717699_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.SoundAudio/SoundStreamStatus
struct  SoundStreamStatus_t3426717699 
{
public:
	// System.Int32 Utage.SoundAudio/SoundStreamStatus::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(SoundStreamStatus_t3426717699, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SOUNDSTREAMSTATUS_T3426717699_H
#ifndef IMAGEEFFECTTYPE_T2892257590_H
#define IMAGEEFFECTTYPE_T2892257590_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.ImageEffectType
struct  ImageEffectType_t2892257590 
{
public:
	// System.Int32 Utage.ImageEffectType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(ImageEffectType_t2892257590, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IMAGEEFFECTTYPE_T2892257590_H
#ifndef U3CSETPATTERNU3EC__ANONSTOREY2_T2684688205_H
#define U3CSETPATTERNU3EC__ANONSTOREY2_T2684688205_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.AvatarPattern/<SetPattern>c__AnonStorey2
struct  U3CSetPatternU3Ec__AnonStorey2_t2684688205  : public RuntimeObject
{
public:
	// System.Collections.Generic.KeyValuePair`2<System.String,System.Int32> Utage.AvatarPattern/<SetPattern>c__AnonStorey2::keyValue
	KeyValuePair_2_t1744001932  ___keyValue_0;

public:
	inline static int32_t get_offset_of_keyValue_0() { return static_cast<int32_t>(offsetof(U3CSetPatternU3Ec__AnonStorey2_t2684688205, ___keyValue_0)); }
	inline KeyValuePair_2_t1744001932  get_keyValue_0() const { return ___keyValue_0; }
	inline KeyValuePair_2_t1744001932 * get_address_of_keyValue_0() { return &___keyValue_0; }
	inline void set_keyValue_0(KeyValuePair_2_t1744001932  value)
	{
		___keyValue_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CSETPATTERNU3EC__ANONSTOREY2_T2684688205_H
#ifndef OBJECT_T1021602117_H
#define OBJECT_T1021602117_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Object
struct  Object_t1021602117  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Object::m_CachedPtr
	intptr_t ___m_CachedPtr_0;

public:
	inline static int32_t get_offset_of_m_CachedPtr_0() { return static_cast<int32_t>(offsetof(Object_t1021602117, ___m_CachedPtr_0)); }
	inline intptr_t get_m_CachedPtr_0() const { return ___m_CachedPtr_0; }
	inline intptr_t* get_address_of_m_CachedPtr_0() { return &___m_CachedPtr_0; }
	inline void set_m_CachedPtr_0(intptr_t value)
	{
		___m_CachedPtr_0 = value;
	}
};

struct Object_t1021602117_StaticFields
{
public:
	// System.Int32 UnityEngine.Object::OffsetOfInstanceIDInCPlusPlusObject
	int32_t ___OffsetOfInstanceIDInCPlusPlusObject_1;

public:
	inline static int32_t get_offset_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return static_cast<int32_t>(offsetof(Object_t1021602117_StaticFields, ___OffsetOfInstanceIDInCPlusPlusObject_1)); }
	inline int32_t get_OffsetOfInstanceIDInCPlusPlusObject_1() const { return ___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline int32_t* get_address_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return &___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline void set_OffsetOfInstanceIDInCPlusPlusObject_1(int32_t value)
	{
		___OffsetOfInstanceIDInCPlusPlusObject_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.Object
struct Object_t1021602117_marshaled_pinvoke
{
	intptr_t ___m_CachedPtr_0;
};
// Native definition for COM marshalling of UnityEngine.Object
struct Object_t1021602117_marshaled_com
{
	intptr_t ___m_CachedPtr_0;
};
#endif // OBJECT_T1021602117_H
#ifndef OVERLAYBLENDMODE_T2265955091_H
#define OVERLAYBLENDMODE_T2265955091_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.ScreenOverlay/OverlayBlendMode
struct  OverlayBlendMode_t2265955091 
{
public:
	// System.Int32 Utage.ScreenOverlay/OverlayBlendMode::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(OverlayBlendMode_t2265955091, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OVERLAYBLENDMODE_T2265955091_H
#ifndef RESOLUTION_T93968648_H
#define RESOLUTION_T93968648_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.Bloom/Resolution
struct  Resolution_t93968648 
{
public:
	// System.Int32 Utage.Bloom/Resolution::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(Resolution_t93968648, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RESOLUTION_T93968648_H
#ifndef SOUNDPLAYMODE_T4141625668_H
#define SOUNDPLAYMODE_T4141625668_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.SoundPlayMode
struct  SoundPlayMode_t4141625668 
{
public:
	// System.Int32 Utage.SoundPlayMode::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(SoundPlayMode_t4141625668, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SOUNDPLAYMODE_T4141625668_H
#ifndef BLURTYPE_T3188843521_H
#define BLURTYPE_T3188843521_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.Blur/BlurType
struct  BlurType_t3188843521 
{
public:
	// System.Int32 Utage.Blur/BlurType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(BlurType_t3188843521, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BLURTYPE_T3188843521_H
#ifndef LOOPTYPE_T1490651981_H
#define LOOPTYPE_T1490651981_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// iTween/LoopType
struct  LoopType_t1490651981 
{
public:
	// System.Int32 iTween/LoopType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(LoopType_t1490651981, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LOOPTYPE_T1490651981_H
#ifndef MODE_T75426569_H
#define MODE_T75426569_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.SystemUiDebugMenu/Mode
struct  Mode_t75426569 
{
public:
	// System.Int32 Utage.SystemUiDebugMenu/Mode::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(Mode_t75426569, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MODE_T75426569_H
#ifndef BLURTYPE_T3344687199_H
#define BLURTYPE_T3344687199_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.Bloom/BlurType
struct  BlurType_t3344687199 
{
public:
	// System.Int32 Utage.Bloom/BlurType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(BlurType_t3344687199, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BLURTYPE_T3344687199_H
#ifndef ITWEENTYPE_T3322595176_H
#define ITWEENTYPE_T3322595176_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.iTweenType
struct  iTweenType_t3322595176 
{
public:
	// System.Int32 Utage.iTweenType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(iTweenType_t3322595176, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ITWEENTYPE_T3322595176_H
#ifndef CSVTYPE_T1603973182_H
#define CSVTYPE_T1603973182_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.CsvType
struct  CsvType_t1603973182 
{
public:
	// System.Int32 Utage.CsvType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(CsvType_t1603973182, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CSVTYPE_T1603973182_H
#ifndef HITEVENTTYPE_T2243784412_H
#define HITEVENTTYPE_T2243784412_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.CharData/HitEventType
struct  HitEventType_t2243784412 
{
public:
	// System.Int32 Utage.CharData/HitEventType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(HitEventType_t2243784412, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HITEVENTTYPE_T2243784412_H
#ifndef CUSTOMCHARAINFO_T997213109_H
#define CUSTOMCHARAINFO_T997213109_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.CharData/CustomCharaInfo
struct  CustomCharaInfo_t997213109  : public RuntimeObject
{
public:
	// System.Boolean Utage.CharData/CustomCharaInfo::<IsColor>k__BackingField
	bool ___U3CIsColorU3Ek__BackingField_0;
	// UnityEngine.Color Utage.CharData/CustomCharaInfo::color
	Color_t2020392075  ___color_1;
	// System.String Utage.CharData/CustomCharaInfo::colorStr
	String_t* ___colorStr_2;
	// System.Boolean Utage.CharData/CustomCharaInfo::<IsSize>k__BackingField
	bool ___U3CIsSizeU3Ek__BackingField_3;
	// System.Int32 Utage.CharData/CustomCharaInfo::size
	int32_t ___size_4;
	// System.Boolean Utage.CharData/CustomCharaInfo::<IsBold>k__BackingField
	bool ___U3CIsBoldU3Ek__BackingField_5;
	// System.Boolean Utage.CharData/CustomCharaInfo::<IsItalic>k__BackingField
	bool ___U3CIsItalicU3Ek__BackingField_6;
	// System.Boolean Utage.CharData/CustomCharaInfo::<IsDoubleWord>k__BackingField
	bool ___U3CIsDoubleWordU3Ek__BackingField_7;
	// System.Boolean Utage.CharData/CustomCharaInfo::<IsRubyTop>k__BackingField
	bool ___U3CIsRubyTopU3Ek__BackingField_8;
	// System.Boolean Utage.CharData/CustomCharaInfo::<IsRuby>k__BackingField
	bool ___U3CIsRubyU3Ek__BackingField_9;
	// System.String Utage.CharData/CustomCharaInfo::rubyStr
	String_t* ___rubyStr_10;
	// System.Boolean Utage.CharData/CustomCharaInfo::<IsEmphasisMark>k__BackingField
	bool ___U3CIsEmphasisMarkU3Ek__BackingField_11;
	// System.Boolean Utage.CharData/CustomCharaInfo::<IsSuperScript>k__BackingField
	bool ___U3CIsSuperScriptU3Ek__BackingField_12;
	// System.Boolean Utage.CharData/CustomCharaInfo::<IsSubScript>k__BackingField
	bool ___U3CIsSubScriptU3Ek__BackingField_13;
	// System.Boolean Utage.CharData/CustomCharaInfo::<IsUnderLineTop>k__BackingField
	bool ___U3CIsUnderLineTopU3Ek__BackingField_14;
	// System.Boolean Utage.CharData/CustomCharaInfo::<IsUnderLine>k__BackingField
	bool ___U3CIsUnderLineU3Ek__BackingField_15;
	// System.Boolean Utage.CharData/CustomCharaInfo::<IsStrikeTop>k__BackingField
	bool ___U3CIsStrikeTopU3Ek__BackingField_16;
	// System.Boolean Utage.CharData/CustomCharaInfo::<IsStrike>k__BackingField
	bool ___U3CIsStrikeU3Ek__BackingField_17;
	// System.Boolean Utage.CharData/CustomCharaInfo::<IsGroupTop>k__BackingField
	bool ___U3CIsGroupTopU3Ek__BackingField_18;
	// System.Boolean Utage.CharData/CustomCharaInfo::<IsGroup>k__BackingField
	bool ___U3CIsGroupU3Ek__BackingField_19;
	// System.Boolean Utage.CharData/CustomCharaInfo::<IsEmoji>k__BackingField
	bool ___U3CIsEmojiU3Ek__BackingField_20;
	// System.String Utage.CharData/CustomCharaInfo::<EmojiKey>k__BackingField
	String_t* ___U3CEmojiKeyU3Ek__BackingField_21;
	// System.Boolean Utage.CharData/CustomCharaInfo::<IsDash>k__BackingField
	bool ___U3CIsDashU3Ek__BackingField_22;
	// System.Int32 Utage.CharData/CustomCharaInfo::<DashSize>k__BackingField
	int32_t ___U3CDashSizeU3Ek__BackingField_23;
	// System.Boolean Utage.CharData/CustomCharaInfo::<IsSpace>k__BackingField
	bool ___U3CIsSpaceU3Ek__BackingField_24;
	// System.Int32 Utage.CharData/CustomCharaInfo::<SpaceSize>k__BackingField
	int32_t ___U3CSpaceSizeU3Ek__BackingField_25;
	// System.Boolean Utage.CharData/CustomCharaInfo::<IsSpeed>k__BackingField
	bool ___U3CIsSpeedU3Ek__BackingField_26;
	// System.Single Utage.CharData/CustomCharaInfo::speed
	float ___speed_27;
	// System.Boolean Utage.CharData/CustomCharaInfo::<IsInterval>k__BackingField
	bool ___U3CIsIntervalU3Ek__BackingField_28;
	// System.Single Utage.CharData/CustomCharaInfo::Interval
	float ___Interval_29;
	// System.Boolean Utage.CharData/CustomCharaInfo::<IsHitEventTop>k__BackingField
	bool ___U3CIsHitEventTopU3Ek__BackingField_30;
	// System.Boolean Utage.CharData/CustomCharaInfo::<IsHitEvent>k__BackingField
	bool ___U3CIsHitEventU3Ek__BackingField_31;
	// System.String Utage.CharData/CustomCharaInfo::<HitEventArg>k__BackingField
	String_t* ___U3CHitEventArgU3Ek__BackingField_32;
	// Utage.CharData/HitEventType Utage.CharData/CustomCharaInfo::<HitEventType>k__BackingField
	int32_t ___U3CHitEventTypeU3Ek__BackingField_33;

public:
	inline static int32_t get_offset_of_U3CIsColorU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(CustomCharaInfo_t997213109, ___U3CIsColorU3Ek__BackingField_0)); }
	inline bool get_U3CIsColorU3Ek__BackingField_0() const { return ___U3CIsColorU3Ek__BackingField_0; }
	inline bool* get_address_of_U3CIsColorU3Ek__BackingField_0() { return &___U3CIsColorU3Ek__BackingField_0; }
	inline void set_U3CIsColorU3Ek__BackingField_0(bool value)
	{
		___U3CIsColorU3Ek__BackingField_0 = value;
	}

	inline static int32_t get_offset_of_color_1() { return static_cast<int32_t>(offsetof(CustomCharaInfo_t997213109, ___color_1)); }
	inline Color_t2020392075  get_color_1() const { return ___color_1; }
	inline Color_t2020392075 * get_address_of_color_1() { return &___color_1; }
	inline void set_color_1(Color_t2020392075  value)
	{
		___color_1 = value;
	}

	inline static int32_t get_offset_of_colorStr_2() { return static_cast<int32_t>(offsetof(CustomCharaInfo_t997213109, ___colorStr_2)); }
	inline String_t* get_colorStr_2() const { return ___colorStr_2; }
	inline String_t** get_address_of_colorStr_2() { return &___colorStr_2; }
	inline void set_colorStr_2(String_t* value)
	{
		___colorStr_2 = value;
		Il2CppCodeGenWriteBarrier((&___colorStr_2), value);
	}

	inline static int32_t get_offset_of_U3CIsSizeU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(CustomCharaInfo_t997213109, ___U3CIsSizeU3Ek__BackingField_3)); }
	inline bool get_U3CIsSizeU3Ek__BackingField_3() const { return ___U3CIsSizeU3Ek__BackingField_3; }
	inline bool* get_address_of_U3CIsSizeU3Ek__BackingField_3() { return &___U3CIsSizeU3Ek__BackingField_3; }
	inline void set_U3CIsSizeU3Ek__BackingField_3(bool value)
	{
		___U3CIsSizeU3Ek__BackingField_3 = value;
	}

	inline static int32_t get_offset_of_size_4() { return static_cast<int32_t>(offsetof(CustomCharaInfo_t997213109, ___size_4)); }
	inline int32_t get_size_4() const { return ___size_4; }
	inline int32_t* get_address_of_size_4() { return &___size_4; }
	inline void set_size_4(int32_t value)
	{
		___size_4 = value;
	}

	inline static int32_t get_offset_of_U3CIsBoldU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(CustomCharaInfo_t997213109, ___U3CIsBoldU3Ek__BackingField_5)); }
	inline bool get_U3CIsBoldU3Ek__BackingField_5() const { return ___U3CIsBoldU3Ek__BackingField_5; }
	inline bool* get_address_of_U3CIsBoldU3Ek__BackingField_5() { return &___U3CIsBoldU3Ek__BackingField_5; }
	inline void set_U3CIsBoldU3Ek__BackingField_5(bool value)
	{
		___U3CIsBoldU3Ek__BackingField_5 = value;
	}

	inline static int32_t get_offset_of_U3CIsItalicU3Ek__BackingField_6() { return static_cast<int32_t>(offsetof(CustomCharaInfo_t997213109, ___U3CIsItalicU3Ek__BackingField_6)); }
	inline bool get_U3CIsItalicU3Ek__BackingField_6() const { return ___U3CIsItalicU3Ek__BackingField_6; }
	inline bool* get_address_of_U3CIsItalicU3Ek__BackingField_6() { return &___U3CIsItalicU3Ek__BackingField_6; }
	inline void set_U3CIsItalicU3Ek__BackingField_6(bool value)
	{
		___U3CIsItalicU3Ek__BackingField_6 = value;
	}

	inline static int32_t get_offset_of_U3CIsDoubleWordU3Ek__BackingField_7() { return static_cast<int32_t>(offsetof(CustomCharaInfo_t997213109, ___U3CIsDoubleWordU3Ek__BackingField_7)); }
	inline bool get_U3CIsDoubleWordU3Ek__BackingField_7() const { return ___U3CIsDoubleWordU3Ek__BackingField_7; }
	inline bool* get_address_of_U3CIsDoubleWordU3Ek__BackingField_7() { return &___U3CIsDoubleWordU3Ek__BackingField_7; }
	inline void set_U3CIsDoubleWordU3Ek__BackingField_7(bool value)
	{
		___U3CIsDoubleWordU3Ek__BackingField_7 = value;
	}

	inline static int32_t get_offset_of_U3CIsRubyTopU3Ek__BackingField_8() { return static_cast<int32_t>(offsetof(CustomCharaInfo_t997213109, ___U3CIsRubyTopU3Ek__BackingField_8)); }
	inline bool get_U3CIsRubyTopU3Ek__BackingField_8() const { return ___U3CIsRubyTopU3Ek__BackingField_8; }
	inline bool* get_address_of_U3CIsRubyTopU3Ek__BackingField_8() { return &___U3CIsRubyTopU3Ek__BackingField_8; }
	inline void set_U3CIsRubyTopU3Ek__BackingField_8(bool value)
	{
		___U3CIsRubyTopU3Ek__BackingField_8 = value;
	}

	inline static int32_t get_offset_of_U3CIsRubyU3Ek__BackingField_9() { return static_cast<int32_t>(offsetof(CustomCharaInfo_t997213109, ___U3CIsRubyU3Ek__BackingField_9)); }
	inline bool get_U3CIsRubyU3Ek__BackingField_9() const { return ___U3CIsRubyU3Ek__BackingField_9; }
	inline bool* get_address_of_U3CIsRubyU3Ek__BackingField_9() { return &___U3CIsRubyU3Ek__BackingField_9; }
	inline void set_U3CIsRubyU3Ek__BackingField_9(bool value)
	{
		___U3CIsRubyU3Ek__BackingField_9 = value;
	}

	inline static int32_t get_offset_of_rubyStr_10() { return static_cast<int32_t>(offsetof(CustomCharaInfo_t997213109, ___rubyStr_10)); }
	inline String_t* get_rubyStr_10() const { return ___rubyStr_10; }
	inline String_t** get_address_of_rubyStr_10() { return &___rubyStr_10; }
	inline void set_rubyStr_10(String_t* value)
	{
		___rubyStr_10 = value;
		Il2CppCodeGenWriteBarrier((&___rubyStr_10), value);
	}

	inline static int32_t get_offset_of_U3CIsEmphasisMarkU3Ek__BackingField_11() { return static_cast<int32_t>(offsetof(CustomCharaInfo_t997213109, ___U3CIsEmphasisMarkU3Ek__BackingField_11)); }
	inline bool get_U3CIsEmphasisMarkU3Ek__BackingField_11() const { return ___U3CIsEmphasisMarkU3Ek__BackingField_11; }
	inline bool* get_address_of_U3CIsEmphasisMarkU3Ek__BackingField_11() { return &___U3CIsEmphasisMarkU3Ek__BackingField_11; }
	inline void set_U3CIsEmphasisMarkU3Ek__BackingField_11(bool value)
	{
		___U3CIsEmphasisMarkU3Ek__BackingField_11 = value;
	}

	inline static int32_t get_offset_of_U3CIsSuperScriptU3Ek__BackingField_12() { return static_cast<int32_t>(offsetof(CustomCharaInfo_t997213109, ___U3CIsSuperScriptU3Ek__BackingField_12)); }
	inline bool get_U3CIsSuperScriptU3Ek__BackingField_12() const { return ___U3CIsSuperScriptU3Ek__BackingField_12; }
	inline bool* get_address_of_U3CIsSuperScriptU3Ek__BackingField_12() { return &___U3CIsSuperScriptU3Ek__BackingField_12; }
	inline void set_U3CIsSuperScriptU3Ek__BackingField_12(bool value)
	{
		___U3CIsSuperScriptU3Ek__BackingField_12 = value;
	}

	inline static int32_t get_offset_of_U3CIsSubScriptU3Ek__BackingField_13() { return static_cast<int32_t>(offsetof(CustomCharaInfo_t997213109, ___U3CIsSubScriptU3Ek__BackingField_13)); }
	inline bool get_U3CIsSubScriptU3Ek__BackingField_13() const { return ___U3CIsSubScriptU3Ek__BackingField_13; }
	inline bool* get_address_of_U3CIsSubScriptU3Ek__BackingField_13() { return &___U3CIsSubScriptU3Ek__BackingField_13; }
	inline void set_U3CIsSubScriptU3Ek__BackingField_13(bool value)
	{
		___U3CIsSubScriptU3Ek__BackingField_13 = value;
	}

	inline static int32_t get_offset_of_U3CIsUnderLineTopU3Ek__BackingField_14() { return static_cast<int32_t>(offsetof(CustomCharaInfo_t997213109, ___U3CIsUnderLineTopU3Ek__BackingField_14)); }
	inline bool get_U3CIsUnderLineTopU3Ek__BackingField_14() const { return ___U3CIsUnderLineTopU3Ek__BackingField_14; }
	inline bool* get_address_of_U3CIsUnderLineTopU3Ek__BackingField_14() { return &___U3CIsUnderLineTopU3Ek__BackingField_14; }
	inline void set_U3CIsUnderLineTopU3Ek__BackingField_14(bool value)
	{
		___U3CIsUnderLineTopU3Ek__BackingField_14 = value;
	}

	inline static int32_t get_offset_of_U3CIsUnderLineU3Ek__BackingField_15() { return static_cast<int32_t>(offsetof(CustomCharaInfo_t997213109, ___U3CIsUnderLineU3Ek__BackingField_15)); }
	inline bool get_U3CIsUnderLineU3Ek__BackingField_15() const { return ___U3CIsUnderLineU3Ek__BackingField_15; }
	inline bool* get_address_of_U3CIsUnderLineU3Ek__BackingField_15() { return &___U3CIsUnderLineU3Ek__BackingField_15; }
	inline void set_U3CIsUnderLineU3Ek__BackingField_15(bool value)
	{
		___U3CIsUnderLineU3Ek__BackingField_15 = value;
	}

	inline static int32_t get_offset_of_U3CIsStrikeTopU3Ek__BackingField_16() { return static_cast<int32_t>(offsetof(CustomCharaInfo_t997213109, ___U3CIsStrikeTopU3Ek__BackingField_16)); }
	inline bool get_U3CIsStrikeTopU3Ek__BackingField_16() const { return ___U3CIsStrikeTopU3Ek__BackingField_16; }
	inline bool* get_address_of_U3CIsStrikeTopU3Ek__BackingField_16() { return &___U3CIsStrikeTopU3Ek__BackingField_16; }
	inline void set_U3CIsStrikeTopU3Ek__BackingField_16(bool value)
	{
		___U3CIsStrikeTopU3Ek__BackingField_16 = value;
	}

	inline static int32_t get_offset_of_U3CIsStrikeU3Ek__BackingField_17() { return static_cast<int32_t>(offsetof(CustomCharaInfo_t997213109, ___U3CIsStrikeU3Ek__BackingField_17)); }
	inline bool get_U3CIsStrikeU3Ek__BackingField_17() const { return ___U3CIsStrikeU3Ek__BackingField_17; }
	inline bool* get_address_of_U3CIsStrikeU3Ek__BackingField_17() { return &___U3CIsStrikeU3Ek__BackingField_17; }
	inline void set_U3CIsStrikeU3Ek__BackingField_17(bool value)
	{
		___U3CIsStrikeU3Ek__BackingField_17 = value;
	}

	inline static int32_t get_offset_of_U3CIsGroupTopU3Ek__BackingField_18() { return static_cast<int32_t>(offsetof(CustomCharaInfo_t997213109, ___U3CIsGroupTopU3Ek__BackingField_18)); }
	inline bool get_U3CIsGroupTopU3Ek__BackingField_18() const { return ___U3CIsGroupTopU3Ek__BackingField_18; }
	inline bool* get_address_of_U3CIsGroupTopU3Ek__BackingField_18() { return &___U3CIsGroupTopU3Ek__BackingField_18; }
	inline void set_U3CIsGroupTopU3Ek__BackingField_18(bool value)
	{
		___U3CIsGroupTopU3Ek__BackingField_18 = value;
	}

	inline static int32_t get_offset_of_U3CIsGroupU3Ek__BackingField_19() { return static_cast<int32_t>(offsetof(CustomCharaInfo_t997213109, ___U3CIsGroupU3Ek__BackingField_19)); }
	inline bool get_U3CIsGroupU3Ek__BackingField_19() const { return ___U3CIsGroupU3Ek__BackingField_19; }
	inline bool* get_address_of_U3CIsGroupU3Ek__BackingField_19() { return &___U3CIsGroupU3Ek__BackingField_19; }
	inline void set_U3CIsGroupU3Ek__BackingField_19(bool value)
	{
		___U3CIsGroupU3Ek__BackingField_19 = value;
	}

	inline static int32_t get_offset_of_U3CIsEmojiU3Ek__BackingField_20() { return static_cast<int32_t>(offsetof(CustomCharaInfo_t997213109, ___U3CIsEmojiU3Ek__BackingField_20)); }
	inline bool get_U3CIsEmojiU3Ek__BackingField_20() const { return ___U3CIsEmojiU3Ek__BackingField_20; }
	inline bool* get_address_of_U3CIsEmojiU3Ek__BackingField_20() { return &___U3CIsEmojiU3Ek__BackingField_20; }
	inline void set_U3CIsEmojiU3Ek__BackingField_20(bool value)
	{
		___U3CIsEmojiU3Ek__BackingField_20 = value;
	}

	inline static int32_t get_offset_of_U3CEmojiKeyU3Ek__BackingField_21() { return static_cast<int32_t>(offsetof(CustomCharaInfo_t997213109, ___U3CEmojiKeyU3Ek__BackingField_21)); }
	inline String_t* get_U3CEmojiKeyU3Ek__BackingField_21() const { return ___U3CEmojiKeyU3Ek__BackingField_21; }
	inline String_t** get_address_of_U3CEmojiKeyU3Ek__BackingField_21() { return &___U3CEmojiKeyU3Ek__BackingField_21; }
	inline void set_U3CEmojiKeyU3Ek__BackingField_21(String_t* value)
	{
		___U3CEmojiKeyU3Ek__BackingField_21 = value;
		Il2CppCodeGenWriteBarrier((&___U3CEmojiKeyU3Ek__BackingField_21), value);
	}

	inline static int32_t get_offset_of_U3CIsDashU3Ek__BackingField_22() { return static_cast<int32_t>(offsetof(CustomCharaInfo_t997213109, ___U3CIsDashU3Ek__BackingField_22)); }
	inline bool get_U3CIsDashU3Ek__BackingField_22() const { return ___U3CIsDashU3Ek__BackingField_22; }
	inline bool* get_address_of_U3CIsDashU3Ek__BackingField_22() { return &___U3CIsDashU3Ek__BackingField_22; }
	inline void set_U3CIsDashU3Ek__BackingField_22(bool value)
	{
		___U3CIsDashU3Ek__BackingField_22 = value;
	}

	inline static int32_t get_offset_of_U3CDashSizeU3Ek__BackingField_23() { return static_cast<int32_t>(offsetof(CustomCharaInfo_t997213109, ___U3CDashSizeU3Ek__BackingField_23)); }
	inline int32_t get_U3CDashSizeU3Ek__BackingField_23() const { return ___U3CDashSizeU3Ek__BackingField_23; }
	inline int32_t* get_address_of_U3CDashSizeU3Ek__BackingField_23() { return &___U3CDashSizeU3Ek__BackingField_23; }
	inline void set_U3CDashSizeU3Ek__BackingField_23(int32_t value)
	{
		___U3CDashSizeU3Ek__BackingField_23 = value;
	}

	inline static int32_t get_offset_of_U3CIsSpaceU3Ek__BackingField_24() { return static_cast<int32_t>(offsetof(CustomCharaInfo_t997213109, ___U3CIsSpaceU3Ek__BackingField_24)); }
	inline bool get_U3CIsSpaceU3Ek__BackingField_24() const { return ___U3CIsSpaceU3Ek__BackingField_24; }
	inline bool* get_address_of_U3CIsSpaceU3Ek__BackingField_24() { return &___U3CIsSpaceU3Ek__BackingField_24; }
	inline void set_U3CIsSpaceU3Ek__BackingField_24(bool value)
	{
		___U3CIsSpaceU3Ek__BackingField_24 = value;
	}

	inline static int32_t get_offset_of_U3CSpaceSizeU3Ek__BackingField_25() { return static_cast<int32_t>(offsetof(CustomCharaInfo_t997213109, ___U3CSpaceSizeU3Ek__BackingField_25)); }
	inline int32_t get_U3CSpaceSizeU3Ek__BackingField_25() const { return ___U3CSpaceSizeU3Ek__BackingField_25; }
	inline int32_t* get_address_of_U3CSpaceSizeU3Ek__BackingField_25() { return &___U3CSpaceSizeU3Ek__BackingField_25; }
	inline void set_U3CSpaceSizeU3Ek__BackingField_25(int32_t value)
	{
		___U3CSpaceSizeU3Ek__BackingField_25 = value;
	}

	inline static int32_t get_offset_of_U3CIsSpeedU3Ek__BackingField_26() { return static_cast<int32_t>(offsetof(CustomCharaInfo_t997213109, ___U3CIsSpeedU3Ek__BackingField_26)); }
	inline bool get_U3CIsSpeedU3Ek__BackingField_26() const { return ___U3CIsSpeedU3Ek__BackingField_26; }
	inline bool* get_address_of_U3CIsSpeedU3Ek__BackingField_26() { return &___U3CIsSpeedU3Ek__BackingField_26; }
	inline void set_U3CIsSpeedU3Ek__BackingField_26(bool value)
	{
		___U3CIsSpeedU3Ek__BackingField_26 = value;
	}

	inline static int32_t get_offset_of_speed_27() { return static_cast<int32_t>(offsetof(CustomCharaInfo_t997213109, ___speed_27)); }
	inline float get_speed_27() const { return ___speed_27; }
	inline float* get_address_of_speed_27() { return &___speed_27; }
	inline void set_speed_27(float value)
	{
		___speed_27 = value;
	}

	inline static int32_t get_offset_of_U3CIsIntervalU3Ek__BackingField_28() { return static_cast<int32_t>(offsetof(CustomCharaInfo_t997213109, ___U3CIsIntervalU3Ek__BackingField_28)); }
	inline bool get_U3CIsIntervalU3Ek__BackingField_28() const { return ___U3CIsIntervalU3Ek__BackingField_28; }
	inline bool* get_address_of_U3CIsIntervalU3Ek__BackingField_28() { return &___U3CIsIntervalU3Ek__BackingField_28; }
	inline void set_U3CIsIntervalU3Ek__BackingField_28(bool value)
	{
		___U3CIsIntervalU3Ek__BackingField_28 = value;
	}

	inline static int32_t get_offset_of_Interval_29() { return static_cast<int32_t>(offsetof(CustomCharaInfo_t997213109, ___Interval_29)); }
	inline float get_Interval_29() const { return ___Interval_29; }
	inline float* get_address_of_Interval_29() { return &___Interval_29; }
	inline void set_Interval_29(float value)
	{
		___Interval_29 = value;
	}

	inline static int32_t get_offset_of_U3CIsHitEventTopU3Ek__BackingField_30() { return static_cast<int32_t>(offsetof(CustomCharaInfo_t997213109, ___U3CIsHitEventTopU3Ek__BackingField_30)); }
	inline bool get_U3CIsHitEventTopU3Ek__BackingField_30() const { return ___U3CIsHitEventTopU3Ek__BackingField_30; }
	inline bool* get_address_of_U3CIsHitEventTopU3Ek__BackingField_30() { return &___U3CIsHitEventTopU3Ek__BackingField_30; }
	inline void set_U3CIsHitEventTopU3Ek__BackingField_30(bool value)
	{
		___U3CIsHitEventTopU3Ek__BackingField_30 = value;
	}

	inline static int32_t get_offset_of_U3CIsHitEventU3Ek__BackingField_31() { return static_cast<int32_t>(offsetof(CustomCharaInfo_t997213109, ___U3CIsHitEventU3Ek__BackingField_31)); }
	inline bool get_U3CIsHitEventU3Ek__BackingField_31() const { return ___U3CIsHitEventU3Ek__BackingField_31; }
	inline bool* get_address_of_U3CIsHitEventU3Ek__BackingField_31() { return &___U3CIsHitEventU3Ek__BackingField_31; }
	inline void set_U3CIsHitEventU3Ek__BackingField_31(bool value)
	{
		___U3CIsHitEventU3Ek__BackingField_31 = value;
	}

	inline static int32_t get_offset_of_U3CHitEventArgU3Ek__BackingField_32() { return static_cast<int32_t>(offsetof(CustomCharaInfo_t997213109, ___U3CHitEventArgU3Ek__BackingField_32)); }
	inline String_t* get_U3CHitEventArgU3Ek__BackingField_32() const { return ___U3CHitEventArgU3Ek__BackingField_32; }
	inline String_t** get_address_of_U3CHitEventArgU3Ek__BackingField_32() { return &___U3CHitEventArgU3Ek__BackingField_32; }
	inline void set_U3CHitEventArgU3Ek__BackingField_32(String_t* value)
	{
		___U3CHitEventArgU3Ek__BackingField_32 = value;
		Il2CppCodeGenWriteBarrier((&___U3CHitEventArgU3Ek__BackingField_32), value);
	}

	inline static int32_t get_offset_of_U3CHitEventTypeU3Ek__BackingField_33() { return static_cast<int32_t>(offsetof(CustomCharaInfo_t997213109, ___U3CHitEventTypeU3Ek__BackingField_33)); }
	inline int32_t get_U3CHitEventTypeU3Ek__BackingField_33() const { return ___U3CHitEventTypeU3Ek__BackingField_33; }
	inline int32_t* get_address_of_U3CHitEventTypeU3Ek__BackingField_33() { return &___U3CHitEventTypeU3Ek__BackingField_33; }
	inline void set_U3CHitEventTypeU3Ek__BackingField_33(int32_t value)
	{
		___U3CHitEventTypeU3Ek__BackingField_33 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CUSTOMCHARAINFO_T997213109_H
#ifndef SOUNDDATA_T3887839289_H
#define SOUNDDATA_T3887839289_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.SoundData
struct  SoundData_t3887839289  : public RuntimeObject
{
public:
	// UnityEngine.AudioClip Utage.SoundData::clip
	AudioClip_t1932558630 * ___clip_0;
	// Utage.AssetFile Utage.SoundData::<File>k__BackingField
	RuntimeObject* ___U3CFileU3Ek__BackingField_1;
	// Utage.SoundPlayMode Utage.SoundData::<PlayMode>k__BackingField
	int32_t ___U3CPlayModeU3Ek__BackingField_2;
	// System.Boolean Utage.SoundData::<IsLoop>k__BackingField
	bool ___U3CIsLoopU3Ek__BackingField_3;
	// System.Single Utage.SoundData::<PlayVolume>k__BackingField
	float ___U3CPlayVolumeU3Ek__BackingField_4;
	// System.Single Utage.SoundData::<ResourceVolume>k__BackingField
	float ___U3CResourceVolumeU3Ek__BackingField_5;
	// System.Single Utage.SoundData::<IntroTime>k__BackingField
	float ___U3CIntroTimeU3Ek__BackingField_6;
	// System.String Utage.SoundData::<Tag>k__BackingField
	String_t* ___U3CTagU3Ek__BackingField_7;

public:
	inline static int32_t get_offset_of_clip_0() { return static_cast<int32_t>(offsetof(SoundData_t3887839289, ___clip_0)); }
	inline AudioClip_t1932558630 * get_clip_0() const { return ___clip_0; }
	inline AudioClip_t1932558630 ** get_address_of_clip_0() { return &___clip_0; }
	inline void set_clip_0(AudioClip_t1932558630 * value)
	{
		___clip_0 = value;
		Il2CppCodeGenWriteBarrier((&___clip_0), value);
	}

	inline static int32_t get_offset_of_U3CFileU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(SoundData_t3887839289, ___U3CFileU3Ek__BackingField_1)); }
	inline RuntimeObject* get_U3CFileU3Ek__BackingField_1() const { return ___U3CFileU3Ek__BackingField_1; }
	inline RuntimeObject** get_address_of_U3CFileU3Ek__BackingField_1() { return &___U3CFileU3Ek__BackingField_1; }
	inline void set_U3CFileU3Ek__BackingField_1(RuntimeObject* value)
	{
		___U3CFileU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CFileU3Ek__BackingField_1), value);
	}

	inline static int32_t get_offset_of_U3CPlayModeU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(SoundData_t3887839289, ___U3CPlayModeU3Ek__BackingField_2)); }
	inline int32_t get_U3CPlayModeU3Ek__BackingField_2() const { return ___U3CPlayModeU3Ek__BackingField_2; }
	inline int32_t* get_address_of_U3CPlayModeU3Ek__BackingField_2() { return &___U3CPlayModeU3Ek__BackingField_2; }
	inline void set_U3CPlayModeU3Ek__BackingField_2(int32_t value)
	{
		___U3CPlayModeU3Ek__BackingField_2 = value;
	}

	inline static int32_t get_offset_of_U3CIsLoopU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(SoundData_t3887839289, ___U3CIsLoopU3Ek__BackingField_3)); }
	inline bool get_U3CIsLoopU3Ek__BackingField_3() const { return ___U3CIsLoopU3Ek__BackingField_3; }
	inline bool* get_address_of_U3CIsLoopU3Ek__BackingField_3() { return &___U3CIsLoopU3Ek__BackingField_3; }
	inline void set_U3CIsLoopU3Ek__BackingField_3(bool value)
	{
		___U3CIsLoopU3Ek__BackingField_3 = value;
	}

	inline static int32_t get_offset_of_U3CPlayVolumeU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(SoundData_t3887839289, ___U3CPlayVolumeU3Ek__BackingField_4)); }
	inline float get_U3CPlayVolumeU3Ek__BackingField_4() const { return ___U3CPlayVolumeU3Ek__BackingField_4; }
	inline float* get_address_of_U3CPlayVolumeU3Ek__BackingField_4() { return &___U3CPlayVolumeU3Ek__BackingField_4; }
	inline void set_U3CPlayVolumeU3Ek__BackingField_4(float value)
	{
		___U3CPlayVolumeU3Ek__BackingField_4 = value;
	}

	inline static int32_t get_offset_of_U3CResourceVolumeU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(SoundData_t3887839289, ___U3CResourceVolumeU3Ek__BackingField_5)); }
	inline float get_U3CResourceVolumeU3Ek__BackingField_5() const { return ___U3CResourceVolumeU3Ek__BackingField_5; }
	inline float* get_address_of_U3CResourceVolumeU3Ek__BackingField_5() { return &___U3CResourceVolumeU3Ek__BackingField_5; }
	inline void set_U3CResourceVolumeU3Ek__BackingField_5(float value)
	{
		___U3CResourceVolumeU3Ek__BackingField_5 = value;
	}

	inline static int32_t get_offset_of_U3CIntroTimeU3Ek__BackingField_6() { return static_cast<int32_t>(offsetof(SoundData_t3887839289, ___U3CIntroTimeU3Ek__BackingField_6)); }
	inline float get_U3CIntroTimeU3Ek__BackingField_6() const { return ___U3CIntroTimeU3Ek__BackingField_6; }
	inline float* get_address_of_U3CIntroTimeU3Ek__BackingField_6() { return &___U3CIntroTimeU3Ek__BackingField_6; }
	inline void set_U3CIntroTimeU3Ek__BackingField_6(float value)
	{
		___U3CIntroTimeU3Ek__BackingField_6 = value;
	}

	inline static int32_t get_offset_of_U3CTagU3Ek__BackingField_7() { return static_cast<int32_t>(offsetof(SoundData_t3887839289, ___U3CTagU3Ek__BackingField_7)); }
	inline String_t* get_U3CTagU3Ek__BackingField_7() const { return ___U3CTagU3Ek__BackingField_7; }
	inline String_t** get_address_of_U3CTagU3Ek__BackingField_7() { return &___U3CTagU3Ek__BackingField_7; }
	inline void set_U3CTagU3Ek__BackingField_7(String_t* value)
	{
		___U3CTagU3Ek__BackingField_7 = value;
		Il2CppCodeGenWriteBarrier((&___U3CTagU3Ek__BackingField_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SOUNDDATA_T3887839289_H
#ifndef STRINGGRID_T1872153679_H
#define STRINGGRID_T1872153679_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.StringGrid
struct  StringGrid_t1872153679  : public RuntimeObject
{
public:
	// System.Collections.Generic.List`1<Utage.StringGridRow> Utage.StringGrid::rows
	List_1_t3562358329 * ___rows_0;
	// System.String Utage.StringGrid::name
	String_t* ___name_1;
	// System.String Utage.StringGrid::sheetName
	String_t* ___sheetName_2;
	// Utage.CsvType Utage.StringGrid::type
	int32_t ___type_3;
	// System.Int32 Utage.StringGrid::textLength
	int32_t ___textLength_4;
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> Utage.StringGrid::columnIndexTbl
	Dictionary_2_t3986656710 * ___columnIndexTbl_5;
	// System.Int32 Utage.StringGrid::headerRow
	int32_t ___headerRow_6;

public:
	inline static int32_t get_offset_of_rows_0() { return static_cast<int32_t>(offsetof(StringGrid_t1872153679, ___rows_0)); }
	inline List_1_t3562358329 * get_rows_0() const { return ___rows_0; }
	inline List_1_t3562358329 ** get_address_of_rows_0() { return &___rows_0; }
	inline void set_rows_0(List_1_t3562358329 * value)
	{
		___rows_0 = value;
		Il2CppCodeGenWriteBarrier((&___rows_0), value);
	}

	inline static int32_t get_offset_of_name_1() { return static_cast<int32_t>(offsetof(StringGrid_t1872153679, ___name_1)); }
	inline String_t* get_name_1() const { return ___name_1; }
	inline String_t** get_address_of_name_1() { return &___name_1; }
	inline void set_name_1(String_t* value)
	{
		___name_1 = value;
		Il2CppCodeGenWriteBarrier((&___name_1), value);
	}

	inline static int32_t get_offset_of_sheetName_2() { return static_cast<int32_t>(offsetof(StringGrid_t1872153679, ___sheetName_2)); }
	inline String_t* get_sheetName_2() const { return ___sheetName_2; }
	inline String_t** get_address_of_sheetName_2() { return &___sheetName_2; }
	inline void set_sheetName_2(String_t* value)
	{
		___sheetName_2 = value;
		Il2CppCodeGenWriteBarrier((&___sheetName_2), value);
	}

	inline static int32_t get_offset_of_type_3() { return static_cast<int32_t>(offsetof(StringGrid_t1872153679, ___type_3)); }
	inline int32_t get_type_3() const { return ___type_3; }
	inline int32_t* get_address_of_type_3() { return &___type_3; }
	inline void set_type_3(int32_t value)
	{
		___type_3 = value;
	}

	inline static int32_t get_offset_of_textLength_4() { return static_cast<int32_t>(offsetof(StringGrid_t1872153679, ___textLength_4)); }
	inline int32_t get_textLength_4() const { return ___textLength_4; }
	inline int32_t* get_address_of_textLength_4() { return &___textLength_4; }
	inline void set_textLength_4(int32_t value)
	{
		___textLength_4 = value;
	}

	inline static int32_t get_offset_of_columnIndexTbl_5() { return static_cast<int32_t>(offsetof(StringGrid_t1872153679, ___columnIndexTbl_5)); }
	inline Dictionary_2_t3986656710 * get_columnIndexTbl_5() const { return ___columnIndexTbl_5; }
	inline Dictionary_2_t3986656710 ** get_address_of_columnIndexTbl_5() { return &___columnIndexTbl_5; }
	inline void set_columnIndexTbl_5(Dictionary_2_t3986656710 * value)
	{
		___columnIndexTbl_5 = value;
		Il2CppCodeGenWriteBarrier((&___columnIndexTbl_5), value);
	}

	inline static int32_t get_offset_of_headerRow_6() { return static_cast<int32_t>(offsetof(StringGrid_t1872153679, ___headerRow_6)); }
	inline int32_t get_headerRow_6() const { return ___headerRow_6; }
	inline int32_t* get_address_of_headerRow_6() { return &___headerRow_6; }
	inline void set_headerRow_6(int32_t value)
	{
		___headerRow_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STRINGGRID_T1872153679_H
#ifndef SCRIPTABLEOBJECT_T1975622470_H
#define SCRIPTABLEOBJECT_T1975622470_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.ScriptableObject
struct  ScriptableObject_t1975622470  : public Object_t1021602117
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.ScriptableObject
struct ScriptableObject_t1975622470_marshaled_pinvoke : public Object_t1021602117_marshaled_pinvoke
{
};
// Native definition for COM marshalling of UnityEngine.ScriptableObject
struct ScriptableObject_t1975622470_marshaled_com : public Object_t1021602117_marshaled_com
{
};
#endif // SCRIPTABLEOBJECT_T1975622470_H
#ifndef COMPONENT_T3819376471_H
#define COMPONENT_T3819376471_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Component
struct  Component_t3819376471  : public Object_t1021602117
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPONENT_T3819376471_H
#ifndef ITWEENDATA_T1904063128_H
#define ITWEENDATA_T1904063128_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.iTweenData
struct  iTweenData_t1904063128  : public RuntimeObject
{
public:
	// Utage.iTweenType Utage.iTweenData::type
	int32_t ___type_15;
	// iTween/LoopType Utage.iTweenData::loopType
	int32_t ___loopType_16;
	// System.Int32 Utage.iTweenData::loopCount
	int32_t ___loopCount_17;
	// System.Collections.Generic.Dictionary`2<System.String,System.Object> Utage.iTweenData::hashObjects
	Dictionary_2_t309261261 * ___hashObjects_18;
	// System.String Utage.iTweenData::errorMsg
	String_t* ___errorMsg_19;
	// System.String Utage.iTweenData::strType
	String_t* ___strType_20;
	// System.String Utage.iTweenData::strArg
	String_t* ___strArg_21;
	// System.String Utage.iTweenData::strEaseType
	String_t* ___strEaseType_22;
	// System.String Utage.iTweenData::strLoopType
	String_t* ___strLoopType_23;
	// System.Boolean Utage.iTweenData::isDynamic
	bool ___isDynamic_25;

public:
	inline static int32_t get_offset_of_type_15() { return static_cast<int32_t>(offsetof(iTweenData_t1904063128, ___type_15)); }
	inline int32_t get_type_15() const { return ___type_15; }
	inline int32_t* get_address_of_type_15() { return &___type_15; }
	inline void set_type_15(int32_t value)
	{
		___type_15 = value;
	}

	inline static int32_t get_offset_of_loopType_16() { return static_cast<int32_t>(offsetof(iTweenData_t1904063128, ___loopType_16)); }
	inline int32_t get_loopType_16() const { return ___loopType_16; }
	inline int32_t* get_address_of_loopType_16() { return &___loopType_16; }
	inline void set_loopType_16(int32_t value)
	{
		___loopType_16 = value;
	}

	inline static int32_t get_offset_of_loopCount_17() { return static_cast<int32_t>(offsetof(iTweenData_t1904063128, ___loopCount_17)); }
	inline int32_t get_loopCount_17() const { return ___loopCount_17; }
	inline int32_t* get_address_of_loopCount_17() { return &___loopCount_17; }
	inline void set_loopCount_17(int32_t value)
	{
		___loopCount_17 = value;
	}

	inline static int32_t get_offset_of_hashObjects_18() { return static_cast<int32_t>(offsetof(iTweenData_t1904063128, ___hashObjects_18)); }
	inline Dictionary_2_t309261261 * get_hashObjects_18() const { return ___hashObjects_18; }
	inline Dictionary_2_t309261261 ** get_address_of_hashObjects_18() { return &___hashObjects_18; }
	inline void set_hashObjects_18(Dictionary_2_t309261261 * value)
	{
		___hashObjects_18 = value;
		Il2CppCodeGenWriteBarrier((&___hashObjects_18), value);
	}

	inline static int32_t get_offset_of_errorMsg_19() { return static_cast<int32_t>(offsetof(iTweenData_t1904063128, ___errorMsg_19)); }
	inline String_t* get_errorMsg_19() const { return ___errorMsg_19; }
	inline String_t** get_address_of_errorMsg_19() { return &___errorMsg_19; }
	inline void set_errorMsg_19(String_t* value)
	{
		___errorMsg_19 = value;
		Il2CppCodeGenWriteBarrier((&___errorMsg_19), value);
	}

	inline static int32_t get_offset_of_strType_20() { return static_cast<int32_t>(offsetof(iTweenData_t1904063128, ___strType_20)); }
	inline String_t* get_strType_20() const { return ___strType_20; }
	inline String_t** get_address_of_strType_20() { return &___strType_20; }
	inline void set_strType_20(String_t* value)
	{
		___strType_20 = value;
		Il2CppCodeGenWriteBarrier((&___strType_20), value);
	}

	inline static int32_t get_offset_of_strArg_21() { return static_cast<int32_t>(offsetof(iTweenData_t1904063128, ___strArg_21)); }
	inline String_t* get_strArg_21() const { return ___strArg_21; }
	inline String_t** get_address_of_strArg_21() { return &___strArg_21; }
	inline void set_strArg_21(String_t* value)
	{
		___strArg_21 = value;
		Il2CppCodeGenWriteBarrier((&___strArg_21), value);
	}

	inline static int32_t get_offset_of_strEaseType_22() { return static_cast<int32_t>(offsetof(iTweenData_t1904063128, ___strEaseType_22)); }
	inline String_t* get_strEaseType_22() const { return ___strEaseType_22; }
	inline String_t** get_address_of_strEaseType_22() { return &___strEaseType_22; }
	inline void set_strEaseType_22(String_t* value)
	{
		___strEaseType_22 = value;
		Il2CppCodeGenWriteBarrier((&___strEaseType_22), value);
	}

	inline static int32_t get_offset_of_strLoopType_23() { return static_cast<int32_t>(offsetof(iTweenData_t1904063128, ___strLoopType_23)); }
	inline String_t* get_strLoopType_23() const { return ___strLoopType_23; }
	inline String_t** get_address_of_strLoopType_23() { return &___strLoopType_23; }
	inline void set_strLoopType_23(String_t* value)
	{
		___strLoopType_23 = value;
		Il2CppCodeGenWriteBarrier((&___strLoopType_23), value);
	}

	inline static int32_t get_offset_of_isDynamic_25() { return static_cast<int32_t>(offsetof(iTweenData_t1904063128, ___isDynamic_25)); }
	inline bool get_isDynamic_25() const { return ___isDynamic_25; }
	inline bool* get_address_of_isDynamic_25() { return &___isDynamic_25; }
	inline void set_isDynamic_25(bool value)
	{
		___isDynamic_25 = value;
	}
};

struct iTweenData_t1904063128_StaticFields
{
public:
	// System.Func`2<System.String,System.Object> Utage.iTweenData::CallbackGetValue
	Func_2_t853256019 * ___CallbackGetValue_24;
	// System.String[][] Utage.iTweenData::ArgTbl
	StringU5BU5DU5BU5D_t2190260861* ___ArgTbl_26;
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> Utage.iTweenData::<>f__switch$map9
	Dictionary_2_t3986656710 * ___U3CU3Ef__switchU24map9_27;

public:
	inline static int32_t get_offset_of_CallbackGetValue_24() { return static_cast<int32_t>(offsetof(iTweenData_t1904063128_StaticFields, ___CallbackGetValue_24)); }
	inline Func_2_t853256019 * get_CallbackGetValue_24() const { return ___CallbackGetValue_24; }
	inline Func_2_t853256019 ** get_address_of_CallbackGetValue_24() { return &___CallbackGetValue_24; }
	inline void set_CallbackGetValue_24(Func_2_t853256019 * value)
	{
		___CallbackGetValue_24 = value;
		Il2CppCodeGenWriteBarrier((&___CallbackGetValue_24), value);
	}

	inline static int32_t get_offset_of_ArgTbl_26() { return static_cast<int32_t>(offsetof(iTweenData_t1904063128_StaticFields, ___ArgTbl_26)); }
	inline StringU5BU5DU5BU5D_t2190260861* get_ArgTbl_26() const { return ___ArgTbl_26; }
	inline StringU5BU5DU5BU5D_t2190260861** get_address_of_ArgTbl_26() { return &___ArgTbl_26; }
	inline void set_ArgTbl_26(StringU5BU5DU5BU5D_t2190260861* value)
	{
		___ArgTbl_26 = value;
		Il2CppCodeGenWriteBarrier((&___ArgTbl_26), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__switchU24map9_27() { return static_cast<int32_t>(offsetof(iTweenData_t1904063128_StaticFields, ___U3CU3Ef__switchU24map9_27)); }
	inline Dictionary_2_t3986656710 * get_U3CU3Ef__switchU24map9_27() const { return ___U3CU3Ef__switchU24map9_27; }
	inline Dictionary_2_t3986656710 ** get_address_of_U3CU3Ef__switchU24map9_27() { return &___U3CU3Ef__switchU24map9_27; }
	inline void set_U3CU3Ef__switchU24map9_27(Dictionary_2_t3986656710 * value)
	{
		___U3CU3Ef__switchU24map9_27 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__switchU24map9_27), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ITWEENDATA_T1904063128_H
#ifndef BEHAVIOUR_T955675639_H
#define BEHAVIOUR_T955675639_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Behaviour
struct  Behaviour_t955675639  : public Component_t3819376471
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BEHAVIOUR_T955675639_H
#ifndef AVATARDATA_T1681718915_H
#define AVATARDATA_T1681718915_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.AvatarData
struct  AvatarData_t1681718915  : public ScriptableObject_t1975622470
{
public:
	// System.Collections.Generic.List`1<Utage.AvatarData/Category> Utage.AvatarData::categories
	List_1_t2071429696 * ___categories_2;
	// System.String Utage.AvatarData::optionTag
	String_t* ___optionTag_3;
	// UnityEngine.Vector2 Utage.AvatarData::size
	Vector2_t2243707579  ___size_4;

public:
	inline static int32_t get_offset_of_categories_2() { return static_cast<int32_t>(offsetof(AvatarData_t1681718915, ___categories_2)); }
	inline List_1_t2071429696 * get_categories_2() const { return ___categories_2; }
	inline List_1_t2071429696 ** get_address_of_categories_2() { return &___categories_2; }
	inline void set_categories_2(List_1_t2071429696 * value)
	{
		___categories_2 = value;
		Il2CppCodeGenWriteBarrier((&___categories_2), value);
	}

	inline static int32_t get_offset_of_optionTag_3() { return static_cast<int32_t>(offsetof(AvatarData_t1681718915, ___optionTag_3)); }
	inline String_t* get_optionTag_3() const { return ___optionTag_3; }
	inline String_t** get_address_of_optionTag_3() { return &___optionTag_3; }
	inline void set_optionTag_3(String_t* value)
	{
		___optionTag_3 = value;
		Il2CppCodeGenWriteBarrier((&___optionTag_3), value);
	}

	inline static int32_t get_offset_of_size_4() { return static_cast<int32_t>(offsetof(AvatarData_t1681718915, ___size_4)); }
	inline Vector2_t2243707579  get_size_4() const { return ___size_4; }
	inline Vector2_t2243707579 * get_address_of_size_4() { return &___size_4; }
	inline void set_size_4(Vector2_t2243707579  value)
	{
		___size_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AVATARDATA_T1681718915_H
#ifndef MONOBEHAVIOUR_T1158329972_H
#define MONOBEHAVIOUR_T1158329972_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.MonoBehaviour
struct  MonoBehaviour_t1158329972  : public Behaviour_t955675639
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MONOBEHAVIOUR_T1158329972_H
#ifndef EYEBLINKBASE_T1662968566_H
#define EYEBLINKBASE_T1662968566_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.EyeBlinkBase
struct  EyeBlinkBase_t1662968566  : public MonoBehaviour_t1158329972
{
public:
	// Utage.MinMaxFloat Utage.EyeBlinkBase::intervalTime
	MinMaxFloat_t1425080750 * ___intervalTime_2;
	// System.Single Utage.EyeBlinkBase::randomDoubleEyeBlink
	float ___randomDoubleEyeBlink_3;
	// System.Single Utage.EyeBlinkBase::intervalDoubleEyeBlink
	float ___intervalDoubleEyeBlink_4;
	// System.String Utage.EyeBlinkBase::eyeTag
	String_t* ___eyeTag_5;
	// Utage.MiniAnimationData Utage.EyeBlinkBase::animationData
	MiniAnimationData_t521227391 * ___animationData_6;

public:
	inline static int32_t get_offset_of_intervalTime_2() { return static_cast<int32_t>(offsetof(EyeBlinkBase_t1662968566, ___intervalTime_2)); }
	inline MinMaxFloat_t1425080750 * get_intervalTime_2() const { return ___intervalTime_2; }
	inline MinMaxFloat_t1425080750 ** get_address_of_intervalTime_2() { return &___intervalTime_2; }
	inline void set_intervalTime_2(MinMaxFloat_t1425080750 * value)
	{
		___intervalTime_2 = value;
		Il2CppCodeGenWriteBarrier((&___intervalTime_2), value);
	}

	inline static int32_t get_offset_of_randomDoubleEyeBlink_3() { return static_cast<int32_t>(offsetof(EyeBlinkBase_t1662968566, ___randomDoubleEyeBlink_3)); }
	inline float get_randomDoubleEyeBlink_3() const { return ___randomDoubleEyeBlink_3; }
	inline float* get_address_of_randomDoubleEyeBlink_3() { return &___randomDoubleEyeBlink_3; }
	inline void set_randomDoubleEyeBlink_3(float value)
	{
		___randomDoubleEyeBlink_3 = value;
	}

	inline static int32_t get_offset_of_intervalDoubleEyeBlink_4() { return static_cast<int32_t>(offsetof(EyeBlinkBase_t1662968566, ___intervalDoubleEyeBlink_4)); }
	inline float get_intervalDoubleEyeBlink_4() const { return ___intervalDoubleEyeBlink_4; }
	inline float* get_address_of_intervalDoubleEyeBlink_4() { return &___intervalDoubleEyeBlink_4; }
	inline void set_intervalDoubleEyeBlink_4(float value)
	{
		___intervalDoubleEyeBlink_4 = value;
	}

	inline static int32_t get_offset_of_eyeTag_5() { return static_cast<int32_t>(offsetof(EyeBlinkBase_t1662968566, ___eyeTag_5)); }
	inline String_t* get_eyeTag_5() const { return ___eyeTag_5; }
	inline String_t** get_address_of_eyeTag_5() { return &___eyeTag_5; }
	inline void set_eyeTag_5(String_t* value)
	{
		___eyeTag_5 = value;
		Il2CppCodeGenWriteBarrier((&___eyeTag_5), value);
	}

	inline static int32_t get_offset_of_animationData_6() { return static_cast<int32_t>(offsetof(EyeBlinkBase_t1662968566, ___animationData_6)); }
	inline MiniAnimationData_t521227391 * get_animationData_6() const { return ___animationData_6; }
	inline MiniAnimationData_t521227391 ** get_address_of_animationData_6() { return &___animationData_6; }
	inline void set_animationData_6(MiniAnimationData_t521227391 * value)
	{
		___animationData_6 = value;
		Il2CppCodeGenWriteBarrier((&___animationData_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EYEBLINKBASE_T1662968566_H
#ifndef SYSTEMUI_T2405527951_H
#define SYSTEMUI_T2405527951_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.SystemUi
struct  SystemUi_t2405527951  : public MonoBehaviour_t1158329972
{
public:
	// Utage.SystemUiDialog2Button Utage.SystemUi::dialogGameExit
	SystemUiDialog2Button_t2800622875 * ___dialogGameExit_3;
	// Utage.SystemUiDialog1Button Utage.SystemUi::dialog1Button
	SystemUiDialog1Button_t2800515000 * ___dialog1Button_4;
	// Utage.SystemUiDialog2Button Utage.SystemUi::dialog2Button
	SystemUiDialog2Button_t2800622875 * ___dialog2Button_5;
	// Utage.SystemUiDialog3Button Utage.SystemUi::dialog3Button
	SystemUiDialog3Button_t2800582650 * ___dialog3Button_6;
	// Utage.IndicatorIcon Utage.SystemUi::indicator
	IndicatorIcon_t1982044218 * ___indicator_7;
	// System.Boolean Utage.SystemUi::isEnableInputEscape
	bool ___isEnableInputEscape_8;

public:
	inline static int32_t get_offset_of_dialogGameExit_3() { return static_cast<int32_t>(offsetof(SystemUi_t2405527951, ___dialogGameExit_3)); }
	inline SystemUiDialog2Button_t2800622875 * get_dialogGameExit_3() const { return ___dialogGameExit_3; }
	inline SystemUiDialog2Button_t2800622875 ** get_address_of_dialogGameExit_3() { return &___dialogGameExit_3; }
	inline void set_dialogGameExit_3(SystemUiDialog2Button_t2800622875 * value)
	{
		___dialogGameExit_3 = value;
		Il2CppCodeGenWriteBarrier((&___dialogGameExit_3), value);
	}

	inline static int32_t get_offset_of_dialog1Button_4() { return static_cast<int32_t>(offsetof(SystemUi_t2405527951, ___dialog1Button_4)); }
	inline SystemUiDialog1Button_t2800515000 * get_dialog1Button_4() const { return ___dialog1Button_4; }
	inline SystemUiDialog1Button_t2800515000 ** get_address_of_dialog1Button_4() { return &___dialog1Button_4; }
	inline void set_dialog1Button_4(SystemUiDialog1Button_t2800515000 * value)
	{
		___dialog1Button_4 = value;
		Il2CppCodeGenWriteBarrier((&___dialog1Button_4), value);
	}

	inline static int32_t get_offset_of_dialog2Button_5() { return static_cast<int32_t>(offsetof(SystemUi_t2405527951, ___dialog2Button_5)); }
	inline SystemUiDialog2Button_t2800622875 * get_dialog2Button_5() const { return ___dialog2Button_5; }
	inline SystemUiDialog2Button_t2800622875 ** get_address_of_dialog2Button_5() { return &___dialog2Button_5; }
	inline void set_dialog2Button_5(SystemUiDialog2Button_t2800622875 * value)
	{
		___dialog2Button_5 = value;
		Il2CppCodeGenWriteBarrier((&___dialog2Button_5), value);
	}

	inline static int32_t get_offset_of_dialog3Button_6() { return static_cast<int32_t>(offsetof(SystemUi_t2405527951, ___dialog3Button_6)); }
	inline SystemUiDialog3Button_t2800582650 * get_dialog3Button_6() const { return ___dialog3Button_6; }
	inline SystemUiDialog3Button_t2800582650 ** get_address_of_dialog3Button_6() { return &___dialog3Button_6; }
	inline void set_dialog3Button_6(SystemUiDialog3Button_t2800582650 * value)
	{
		___dialog3Button_6 = value;
		Il2CppCodeGenWriteBarrier((&___dialog3Button_6), value);
	}

	inline static int32_t get_offset_of_indicator_7() { return static_cast<int32_t>(offsetof(SystemUi_t2405527951, ___indicator_7)); }
	inline IndicatorIcon_t1982044218 * get_indicator_7() const { return ___indicator_7; }
	inline IndicatorIcon_t1982044218 ** get_address_of_indicator_7() { return &___indicator_7; }
	inline void set_indicator_7(IndicatorIcon_t1982044218 * value)
	{
		___indicator_7 = value;
		Il2CppCodeGenWriteBarrier((&___indicator_7), value);
	}

	inline static int32_t get_offset_of_isEnableInputEscape_8() { return static_cast<int32_t>(offsetof(SystemUi_t2405527951, ___isEnableInputEscape_8)); }
	inline bool get_isEnableInputEscape_8() const { return ___isEnableInputEscape_8; }
	inline bool* get_address_of_isEnableInputEscape_8() { return &___isEnableInputEscape_8; }
	inline void set_isEnableInputEscape_8(bool value)
	{
		___isEnableInputEscape_8 = value;
	}
};

struct SystemUi_t2405527951_StaticFields
{
public:
	// Utage.SystemUi Utage.SystemUi::instance
	SystemUi_t2405527951 * ___instance_2;

public:
	inline static int32_t get_offset_of_instance_2() { return static_cast<int32_t>(offsetof(SystemUi_t2405527951_StaticFields, ___instance_2)); }
	inline SystemUi_t2405527951 * get_instance_2() const { return ___instance_2; }
	inline SystemUi_t2405527951 ** get_address_of_instance_2() { return &___instance_2; }
	inline void set_instance_2(SystemUi_t2405527951 * value)
	{
		___instance_2 = value;
		Il2CppCodeGenWriteBarrier((&___instance_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SYSTEMUI_T2405527951_H
#ifndef AVATARIMAGE_T1946614104_H
#define AVATARIMAGE_T1946614104_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.AvatarImage
struct  AvatarImage_t1946614104  : public MonoBehaviour_t1158329972
{
public:
	// Utage.AvatarData Utage.AvatarImage::avatarData
	AvatarData_t1681718915 * ___avatarData_2;
	// Utage.AvatarPattern Utage.AvatarImage::avatarPattern
	AvatarPattern_t4052248955 * ___avatarPattern_3;
	// UnityEngine.RectTransform Utage.AvatarImage::cachedRectTransform
	RectTransform_t3349966182 * ___cachedRectTransform_4;
	// UnityEngine.Material Utage.AvatarImage::material
	Material_t193706927 * ___material_5;
	// UnityEngine.Events.UnityEvent Utage.AvatarImage::OnPostRefresh
	UnityEvent_t408735097 * ___OnPostRefresh_6;
	// System.Boolean Utage.AvatarImage::flipX
	bool ___flipX_7;
	// System.Boolean Utage.AvatarImage::flipY
	bool ___flipY_8;
	// UnityEngine.RectTransform Utage.AvatarImage::rootChildren
	RectTransform_t3349966182 * ___rootChildren_9;
	// System.Boolean Utage.AvatarImage::<HasChanged>k__BackingField
	bool ___U3CHasChangedU3Ek__BackingField_10;

public:
	inline static int32_t get_offset_of_avatarData_2() { return static_cast<int32_t>(offsetof(AvatarImage_t1946614104, ___avatarData_2)); }
	inline AvatarData_t1681718915 * get_avatarData_2() const { return ___avatarData_2; }
	inline AvatarData_t1681718915 ** get_address_of_avatarData_2() { return &___avatarData_2; }
	inline void set_avatarData_2(AvatarData_t1681718915 * value)
	{
		___avatarData_2 = value;
		Il2CppCodeGenWriteBarrier((&___avatarData_2), value);
	}

	inline static int32_t get_offset_of_avatarPattern_3() { return static_cast<int32_t>(offsetof(AvatarImage_t1946614104, ___avatarPattern_3)); }
	inline AvatarPattern_t4052248955 * get_avatarPattern_3() const { return ___avatarPattern_3; }
	inline AvatarPattern_t4052248955 ** get_address_of_avatarPattern_3() { return &___avatarPattern_3; }
	inline void set_avatarPattern_3(AvatarPattern_t4052248955 * value)
	{
		___avatarPattern_3 = value;
		Il2CppCodeGenWriteBarrier((&___avatarPattern_3), value);
	}

	inline static int32_t get_offset_of_cachedRectTransform_4() { return static_cast<int32_t>(offsetof(AvatarImage_t1946614104, ___cachedRectTransform_4)); }
	inline RectTransform_t3349966182 * get_cachedRectTransform_4() const { return ___cachedRectTransform_4; }
	inline RectTransform_t3349966182 ** get_address_of_cachedRectTransform_4() { return &___cachedRectTransform_4; }
	inline void set_cachedRectTransform_4(RectTransform_t3349966182 * value)
	{
		___cachedRectTransform_4 = value;
		Il2CppCodeGenWriteBarrier((&___cachedRectTransform_4), value);
	}

	inline static int32_t get_offset_of_material_5() { return static_cast<int32_t>(offsetof(AvatarImage_t1946614104, ___material_5)); }
	inline Material_t193706927 * get_material_5() const { return ___material_5; }
	inline Material_t193706927 ** get_address_of_material_5() { return &___material_5; }
	inline void set_material_5(Material_t193706927 * value)
	{
		___material_5 = value;
		Il2CppCodeGenWriteBarrier((&___material_5), value);
	}

	inline static int32_t get_offset_of_OnPostRefresh_6() { return static_cast<int32_t>(offsetof(AvatarImage_t1946614104, ___OnPostRefresh_6)); }
	inline UnityEvent_t408735097 * get_OnPostRefresh_6() const { return ___OnPostRefresh_6; }
	inline UnityEvent_t408735097 ** get_address_of_OnPostRefresh_6() { return &___OnPostRefresh_6; }
	inline void set_OnPostRefresh_6(UnityEvent_t408735097 * value)
	{
		___OnPostRefresh_6 = value;
		Il2CppCodeGenWriteBarrier((&___OnPostRefresh_6), value);
	}

	inline static int32_t get_offset_of_flipX_7() { return static_cast<int32_t>(offsetof(AvatarImage_t1946614104, ___flipX_7)); }
	inline bool get_flipX_7() const { return ___flipX_7; }
	inline bool* get_address_of_flipX_7() { return &___flipX_7; }
	inline void set_flipX_7(bool value)
	{
		___flipX_7 = value;
	}

	inline static int32_t get_offset_of_flipY_8() { return static_cast<int32_t>(offsetof(AvatarImage_t1946614104, ___flipY_8)); }
	inline bool get_flipY_8() const { return ___flipY_8; }
	inline bool* get_address_of_flipY_8() { return &___flipY_8; }
	inline void set_flipY_8(bool value)
	{
		___flipY_8 = value;
	}

	inline static int32_t get_offset_of_rootChildren_9() { return static_cast<int32_t>(offsetof(AvatarImage_t1946614104, ___rootChildren_9)); }
	inline RectTransform_t3349966182 * get_rootChildren_9() const { return ___rootChildren_9; }
	inline RectTransform_t3349966182 ** get_address_of_rootChildren_9() { return &___rootChildren_9; }
	inline void set_rootChildren_9(RectTransform_t3349966182 * value)
	{
		___rootChildren_9 = value;
		Il2CppCodeGenWriteBarrier((&___rootChildren_9), value);
	}

	inline static int32_t get_offset_of_U3CHasChangedU3Ek__BackingField_10() { return static_cast<int32_t>(offsetof(AvatarImage_t1946614104, ___U3CHasChangedU3Ek__BackingField_10)); }
	inline bool get_U3CHasChangedU3Ek__BackingField_10() const { return ___U3CHasChangedU3Ek__BackingField_10; }
	inline bool* get_address_of_U3CHasChangedU3Ek__BackingField_10() { return &___U3CHasChangedU3Ek__BackingField_10; }
	inline void set_U3CHasChangedU3Ek__BackingField_10(bool value)
	{
		___U3CHasChangedU3Ek__BackingField_10 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AVATARIMAGE_T1946614104_H
#ifndef SOUNDMANAGER_T1265124084_H
#define SOUNDMANAGER_T1265124084_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.SoundManager
struct  SoundManager_t1265124084  : public MonoBehaviour_t1158329972
{
public:
	// System.Single Utage.SoundManager::masterVolume
	float ___masterVolume_7;
	// System.Collections.Generic.List`1<Utage.SoundManager/TaggedMasterVolume> Utage.SoundManager::taggedMasterVolumes
	List_1_t2034408517 * ___taggedMasterVolumes_8;
	// System.Single Utage.SoundManager::duckVolume
	float ___duckVolume_10;
	// System.Single Utage.SoundManager::duckFadeTime
	float ___duckFadeTime_11;
	// System.Single Utage.SoundManager::defaultFadeTime
	float ___defaultFadeTime_12;
	// System.Single Utage.SoundManager::defaultVoiceFadeTime
	float ___defaultVoiceFadeTime_13;
	// System.Single Utage.SoundManager::defaultVolume
	float ___defaultVolume_14;
	// System.String Utage.SoundManager::<CurrentVoiceCharacterLabel>k__BackingField
	String_t* ___U3CCurrentVoiceCharacterLabelU3Ek__BackingField_15;
	// Utage.SoundPlayMode Utage.SoundManager::voicePlayMode
	int32_t ___voicePlayMode_16;
	// Utage.SoundManager/SoundManagerEvent Utage.SoundManager::onCreateSoundSystem
	SoundManagerEvent_t3645924073 * ___onCreateSoundSystem_17;
	// Utage.SoundManagerSystemInterface Utage.SoundManager::system
	RuntimeObject* ___system_18;

public:
	inline static int32_t get_offset_of_masterVolume_7() { return static_cast<int32_t>(offsetof(SoundManager_t1265124084, ___masterVolume_7)); }
	inline float get_masterVolume_7() const { return ___masterVolume_7; }
	inline float* get_address_of_masterVolume_7() { return &___masterVolume_7; }
	inline void set_masterVolume_7(float value)
	{
		___masterVolume_7 = value;
	}

	inline static int32_t get_offset_of_taggedMasterVolumes_8() { return static_cast<int32_t>(offsetof(SoundManager_t1265124084, ___taggedMasterVolumes_8)); }
	inline List_1_t2034408517 * get_taggedMasterVolumes_8() const { return ___taggedMasterVolumes_8; }
	inline List_1_t2034408517 ** get_address_of_taggedMasterVolumes_8() { return &___taggedMasterVolumes_8; }
	inline void set_taggedMasterVolumes_8(List_1_t2034408517 * value)
	{
		___taggedMasterVolumes_8 = value;
		Il2CppCodeGenWriteBarrier((&___taggedMasterVolumes_8), value);
	}

	inline static int32_t get_offset_of_duckVolume_10() { return static_cast<int32_t>(offsetof(SoundManager_t1265124084, ___duckVolume_10)); }
	inline float get_duckVolume_10() const { return ___duckVolume_10; }
	inline float* get_address_of_duckVolume_10() { return &___duckVolume_10; }
	inline void set_duckVolume_10(float value)
	{
		___duckVolume_10 = value;
	}

	inline static int32_t get_offset_of_duckFadeTime_11() { return static_cast<int32_t>(offsetof(SoundManager_t1265124084, ___duckFadeTime_11)); }
	inline float get_duckFadeTime_11() const { return ___duckFadeTime_11; }
	inline float* get_address_of_duckFadeTime_11() { return &___duckFadeTime_11; }
	inline void set_duckFadeTime_11(float value)
	{
		___duckFadeTime_11 = value;
	}

	inline static int32_t get_offset_of_defaultFadeTime_12() { return static_cast<int32_t>(offsetof(SoundManager_t1265124084, ___defaultFadeTime_12)); }
	inline float get_defaultFadeTime_12() const { return ___defaultFadeTime_12; }
	inline float* get_address_of_defaultFadeTime_12() { return &___defaultFadeTime_12; }
	inline void set_defaultFadeTime_12(float value)
	{
		___defaultFadeTime_12 = value;
	}

	inline static int32_t get_offset_of_defaultVoiceFadeTime_13() { return static_cast<int32_t>(offsetof(SoundManager_t1265124084, ___defaultVoiceFadeTime_13)); }
	inline float get_defaultVoiceFadeTime_13() const { return ___defaultVoiceFadeTime_13; }
	inline float* get_address_of_defaultVoiceFadeTime_13() { return &___defaultVoiceFadeTime_13; }
	inline void set_defaultVoiceFadeTime_13(float value)
	{
		___defaultVoiceFadeTime_13 = value;
	}

	inline static int32_t get_offset_of_defaultVolume_14() { return static_cast<int32_t>(offsetof(SoundManager_t1265124084, ___defaultVolume_14)); }
	inline float get_defaultVolume_14() const { return ___defaultVolume_14; }
	inline float* get_address_of_defaultVolume_14() { return &___defaultVolume_14; }
	inline void set_defaultVolume_14(float value)
	{
		___defaultVolume_14 = value;
	}

	inline static int32_t get_offset_of_U3CCurrentVoiceCharacterLabelU3Ek__BackingField_15() { return static_cast<int32_t>(offsetof(SoundManager_t1265124084, ___U3CCurrentVoiceCharacterLabelU3Ek__BackingField_15)); }
	inline String_t* get_U3CCurrentVoiceCharacterLabelU3Ek__BackingField_15() const { return ___U3CCurrentVoiceCharacterLabelU3Ek__BackingField_15; }
	inline String_t** get_address_of_U3CCurrentVoiceCharacterLabelU3Ek__BackingField_15() { return &___U3CCurrentVoiceCharacterLabelU3Ek__BackingField_15; }
	inline void set_U3CCurrentVoiceCharacterLabelU3Ek__BackingField_15(String_t* value)
	{
		___U3CCurrentVoiceCharacterLabelU3Ek__BackingField_15 = value;
		Il2CppCodeGenWriteBarrier((&___U3CCurrentVoiceCharacterLabelU3Ek__BackingField_15), value);
	}

	inline static int32_t get_offset_of_voicePlayMode_16() { return static_cast<int32_t>(offsetof(SoundManager_t1265124084, ___voicePlayMode_16)); }
	inline int32_t get_voicePlayMode_16() const { return ___voicePlayMode_16; }
	inline int32_t* get_address_of_voicePlayMode_16() { return &___voicePlayMode_16; }
	inline void set_voicePlayMode_16(int32_t value)
	{
		___voicePlayMode_16 = value;
	}

	inline static int32_t get_offset_of_onCreateSoundSystem_17() { return static_cast<int32_t>(offsetof(SoundManager_t1265124084, ___onCreateSoundSystem_17)); }
	inline SoundManagerEvent_t3645924073 * get_onCreateSoundSystem_17() const { return ___onCreateSoundSystem_17; }
	inline SoundManagerEvent_t3645924073 ** get_address_of_onCreateSoundSystem_17() { return &___onCreateSoundSystem_17; }
	inline void set_onCreateSoundSystem_17(SoundManagerEvent_t3645924073 * value)
	{
		___onCreateSoundSystem_17 = value;
		Il2CppCodeGenWriteBarrier((&___onCreateSoundSystem_17), value);
	}

	inline static int32_t get_offset_of_system_18() { return static_cast<int32_t>(offsetof(SoundManager_t1265124084, ___system_18)); }
	inline RuntimeObject* get_system_18() const { return ___system_18; }
	inline RuntimeObject** get_address_of_system_18() { return &___system_18; }
	inline void set_system_18(RuntimeObject* value)
	{
		___system_18 = value;
		Il2CppCodeGenWriteBarrier((&___system_18), value);
	}
};

struct SoundManager_t1265124084_StaticFields
{
public:
	// Utage.SoundManager Utage.SoundManager::instance
	SoundManager_t1265124084 * ___instance_6;

public:
	inline static int32_t get_offset_of_instance_6() { return static_cast<int32_t>(offsetof(SoundManager_t1265124084_StaticFields, ___instance_6)); }
	inline SoundManager_t1265124084 * get_instance_6() const { return ___instance_6; }
	inline SoundManager_t1265124084 ** get_address_of_instance_6() { return &___instance_6; }
	inline void set_instance_6(SoundManager_t1265124084 * value)
	{
		___instance_6 = value;
		Il2CppCodeGenWriteBarrier((&___instance_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SOUNDMANAGER_T1265124084_H
#ifndef TIMER_T2904185433_H
#define TIMER_T2904185433_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.Timer
struct  Timer_t2904185433  : public MonoBehaviour_t1158329972
{
public:
	// System.Single Utage.Timer::duration
	float ___duration_2;
	// System.Single Utage.Timer::delay
	float ___delay_3;
	// System.Single Utage.Timer::time
	float ___time_4;
	// System.Single Utage.Timer::time01
	float ___time01_5;
	// Utage.TimerEvent Utage.Timer::onStart
	TimerEvent_t3049894269 * ___onStart_6;
	// Utage.TimerEvent Utage.Timer::onUpdate
	TimerEvent_t3049894269 * ___onUpdate_7;
	// Utage.TimerEvent Utage.Timer::onComplete
	TimerEvent_t3049894269 * ___onComplete_8;
	// System.Boolean Utage.Timer::autoDestroy
	bool ___autoDestroy_9;
	// System.Boolean Utage.Timer::autoStart
	bool ___autoStart_10;
	// System.Action`1<Utage.Timer> Utage.Timer::callbackUpdate
	Action_1_t2705984815 * ___callbackUpdate_11;
	// System.Action`1<Utage.Timer> Utage.Timer::callbackComplete
	Action_1_t2705984815 * ___callbackComplete_12;

public:
	inline static int32_t get_offset_of_duration_2() { return static_cast<int32_t>(offsetof(Timer_t2904185433, ___duration_2)); }
	inline float get_duration_2() const { return ___duration_2; }
	inline float* get_address_of_duration_2() { return &___duration_2; }
	inline void set_duration_2(float value)
	{
		___duration_2 = value;
	}

	inline static int32_t get_offset_of_delay_3() { return static_cast<int32_t>(offsetof(Timer_t2904185433, ___delay_3)); }
	inline float get_delay_3() const { return ___delay_3; }
	inline float* get_address_of_delay_3() { return &___delay_3; }
	inline void set_delay_3(float value)
	{
		___delay_3 = value;
	}

	inline static int32_t get_offset_of_time_4() { return static_cast<int32_t>(offsetof(Timer_t2904185433, ___time_4)); }
	inline float get_time_4() const { return ___time_4; }
	inline float* get_address_of_time_4() { return &___time_4; }
	inline void set_time_4(float value)
	{
		___time_4 = value;
	}

	inline static int32_t get_offset_of_time01_5() { return static_cast<int32_t>(offsetof(Timer_t2904185433, ___time01_5)); }
	inline float get_time01_5() const { return ___time01_5; }
	inline float* get_address_of_time01_5() { return &___time01_5; }
	inline void set_time01_5(float value)
	{
		___time01_5 = value;
	}

	inline static int32_t get_offset_of_onStart_6() { return static_cast<int32_t>(offsetof(Timer_t2904185433, ___onStart_6)); }
	inline TimerEvent_t3049894269 * get_onStart_6() const { return ___onStart_6; }
	inline TimerEvent_t3049894269 ** get_address_of_onStart_6() { return &___onStart_6; }
	inline void set_onStart_6(TimerEvent_t3049894269 * value)
	{
		___onStart_6 = value;
		Il2CppCodeGenWriteBarrier((&___onStart_6), value);
	}

	inline static int32_t get_offset_of_onUpdate_7() { return static_cast<int32_t>(offsetof(Timer_t2904185433, ___onUpdate_7)); }
	inline TimerEvent_t3049894269 * get_onUpdate_7() const { return ___onUpdate_7; }
	inline TimerEvent_t3049894269 ** get_address_of_onUpdate_7() { return &___onUpdate_7; }
	inline void set_onUpdate_7(TimerEvent_t3049894269 * value)
	{
		___onUpdate_7 = value;
		Il2CppCodeGenWriteBarrier((&___onUpdate_7), value);
	}

	inline static int32_t get_offset_of_onComplete_8() { return static_cast<int32_t>(offsetof(Timer_t2904185433, ___onComplete_8)); }
	inline TimerEvent_t3049894269 * get_onComplete_8() const { return ___onComplete_8; }
	inline TimerEvent_t3049894269 ** get_address_of_onComplete_8() { return &___onComplete_8; }
	inline void set_onComplete_8(TimerEvent_t3049894269 * value)
	{
		___onComplete_8 = value;
		Il2CppCodeGenWriteBarrier((&___onComplete_8), value);
	}

	inline static int32_t get_offset_of_autoDestroy_9() { return static_cast<int32_t>(offsetof(Timer_t2904185433, ___autoDestroy_9)); }
	inline bool get_autoDestroy_9() const { return ___autoDestroy_9; }
	inline bool* get_address_of_autoDestroy_9() { return &___autoDestroy_9; }
	inline void set_autoDestroy_9(bool value)
	{
		___autoDestroy_9 = value;
	}

	inline static int32_t get_offset_of_autoStart_10() { return static_cast<int32_t>(offsetof(Timer_t2904185433, ___autoStart_10)); }
	inline bool get_autoStart_10() const { return ___autoStart_10; }
	inline bool* get_address_of_autoStart_10() { return &___autoStart_10; }
	inline void set_autoStart_10(bool value)
	{
		___autoStart_10 = value;
	}

	inline static int32_t get_offset_of_callbackUpdate_11() { return static_cast<int32_t>(offsetof(Timer_t2904185433, ___callbackUpdate_11)); }
	inline Action_1_t2705984815 * get_callbackUpdate_11() const { return ___callbackUpdate_11; }
	inline Action_1_t2705984815 ** get_address_of_callbackUpdate_11() { return &___callbackUpdate_11; }
	inline void set_callbackUpdate_11(Action_1_t2705984815 * value)
	{
		___callbackUpdate_11 = value;
		Il2CppCodeGenWriteBarrier((&___callbackUpdate_11), value);
	}

	inline static int32_t get_offset_of_callbackComplete_12() { return static_cast<int32_t>(offsetof(Timer_t2904185433, ___callbackComplete_12)); }
	inline Action_1_t2705984815 * get_callbackComplete_12() const { return ___callbackComplete_12; }
	inline Action_1_t2705984815 ** get_address_of_callbackComplete_12() { return &___callbackComplete_12; }
	inline void set_callbackComplete_12(Action_1_t2705984815 * value)
	{
		___callbackComplete_12 = value;
		Il2CppCodeGenWriteBarrier((&___callbackComplete_12), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TIMER_T2904185433_H
#ifndef SYSTEMUIDIALOG1BUTTON_T2800515000_H
#define SYSTEMUIDIALOG1BUTTON_T2800515000_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.SystemUiDialog1Button
struct  SystemUiDialog1Button_t2800515000  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.UI.Text Utage.SystemUiDialog1Button::titleText
	Text_t356221433 * ___titleText_2;
	// UnityEngine.UI.Text Utage.SystemUiDialog1Button::button1Text
	Text_t356221433 * ___button1Text_3;
	// UnityEngine.Events.UnityEvent Utage.SystemUiDialog1Button::OnClickButton1
	UnityEvent_t408735097 * ___OnClickButton1_4;

public:
	inline static int32_t get_offset_of_titleText_2() { return static_cast<int32_t>(offsetof(SystemUiDialog1Button_t2800515000, ___titleText_2)); }
	inline Text_t356221433 * get_titleText_2() const { return ___titleText_2; }
	inline Text_t356221433 ** get_address_of_titleText_2() { return &___titleText_2; }
	inline void set_titleText_2(Text_t356221433 * value)
	{
		___titleText_2 = value;
		Il2CppCodeGenWriteBarrier((&___titleText_2), value);
	}

	inline static int32_t get_offset_of_button1Text_3() { return static_cast<int32_t>(offsetof(SystemUiDialog1Button_t2800515000, ___button1Text_3)); }
	inline Text_t356221433 * get_button1Text_3() const { return ___button1Text_3; }
	inline Text_t356221433 ** get_address_of_button1Text_3() { return &___button1Text_3; }
	inline void set_button1Text_3(Text_t356221433 * value)
	{
		___button1Text_3 = value;
		Il2CppCodeGenWriteBarrier((&___button1Text_3), value);
	}

	inline static int32_t get_offset_of_OnClickButton1_4() { return static_cast<int32_t>(offsetof(SystemUiDialog1Button_t2800515000, ___OnClickButton1_4)); }
	inline UnityEvent_t408735097 * get_OnClickButton1_4() const { return ___OnClickButton1_4; }
	inline UnityEvent_t408735097 ** get_address_of_OnClickButton1_4() { return &___OnClickButton1_4; }
	inline void set_OnClickButton1_4(UnityEvent_t408735097 * value)
	{
		___OnClickButton1_4 = value;
		Il2CppCodeGenWriteBarrier((&___OnClickButton1_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SYSTEMUIDIALOG1BUTTON_T2800515000_H
#ifndef DEBUGPAUSEEDITOR_T649960972_H
#define DEBUGPAUSEEDITOR_T649960972_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.DebugPauseEditor
struct  DebugPauseEditor_t649960972  : public MonoBehaviour_t1158329972
{
public:
	// System.Boolean Utage.DebugPauseEditor::isPauseOnMouseDown
	bool ___isPauseOnMouseDown_2;
	// System.Boolean Utage.DebugPauseEditor::isPauseOnMouseUp
	bool ___isPauseOnMouseUp_3;
	// System.Single Utage.DebugPauseEditor::timeScale
	float ___timeScale_4;

public:
	inline static int32_t get_offset_of_isPauseOnMouseDown_2() { return static_cast<int32_t>(offsetof(DebugPauseEditor_t649960972, ___isPauseOnMouseDown_2)); }
	inline bool get_isPauseOnMouseDown_2() const { return ___isPauseOnMouseDown_2; }
	inline bool* get_address_of_isPauseOnMouseDown_2() { return &___isPauseOnMouseDown_2; }
	inline void set_isPauseOnMouseDown_2(bool value)
	{
		___isPauseOnMouseDown_2 = value;
	}

	inline static int32_t get_offset_of_isPauseOnMouseUp_3() { return static_cast<int32_t>(offsetof(DebugPauseEditor_t649960972, ___isPauseOnMouseUp_3)); }
	inline bool get_isPauseOnMouseUp_3() const { return ___isPauseOnMouseUp_3; }
	inline bool* get_address_of_isPauseOnMouseUp_3() { return &___isPauseOnMouseUp_3; }
	inline void set_isPauseOnMouseUp_3(bool value)
	{
		___isPauseOnMouseUp_3 = value;
	}

	inline static int32_t get_offset_of_timeScale_4() { return static_cast<int32_t>(offsetof(DebugPauseEditor_t649960972, ___timeScale_4)); }
	inline float get_timeScale_4() const { return ___timeScale_4; }
	inline float* get_address_of_timeScale_4() { return &___timeScale_4; }
	inline void set_timeScale_4(float value)
	{
		___timeScale_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DEBUGPAUSEEDITOR_T649960972_H
#ifndef DEBUGPRINT_T1518728472_H
#define DEBUGPRINT_T1518728472_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.DebugPrint
struct  DebugPrint_t1518728472  : public MonoBehaviour_t1158329972
{
public:
	// System.Collections.Generic.List`1<System.String> Utage.DebugPrint::logList
	List_1_t1398341365 * ___logList_3;
	// System.Single Utage.DebugPrint::oldTime
	float ___oldTime_4;
	// System.Int32 Utage.DebugPrint::frame
	int32_t ___frame_5;
	// System.Single Utage.DebugPrint::frameRate
	float ___frameRate_6;
	// System.Single Utage.DebugPrint::memSizeSystem
	float ___memSizeSystem_8;
	// System.Single Utage.DebugPrint::memSizeGraphic
	float ___memSizeGraphic_9;
	// System.Single Utage.DebugPrint::memSizeUsedHeap
	float ___memSizeUsedHeap_10;
	// System.Single Utage.DebugPrint::memSizeGC
	float ___memSizeGC_11;
	// System.Single Utage.DebugPrint::memSizeMonoHeap
	float ___memSizeMonoHeap_12;
	// System.Single Utage.DebugPrint::memSizeMonoUsedHeap
	float ___memSizeMonoUsedHeap_13;

public:
	inline static int32_t get_offset_of_logList_3() { return static_cast<int32_t>(offsetof(DebugPrint_t1518728472, ___logList_3)); }
	inline List_1_t1398341365 * get_logList_3() const { return ___logList_3; }
	inline List_1_t1398341365 ** get_address_of_logList_3() { return &___logList_3; }
	inline void set_logList_3(List_1_t1398341365 * value)
	{
		___logList_3 = value;
		Il2CppCodeGenWriteBarrier((&___logList_3), value);
	}

	inline static int32_t get_offset_of_oldTime_4() { return static_cast<int32_t>(offsetof(DebugPrint_t1518728472, ___oldTime_4)); }
	inline float get_oldTime_4() const { return ___oldTime_4; }
	inline float* get_address_of_oldTime_4() { return &___oldTime_4; }
	inline void set_oldTime_4(float value)
	{
		___oldTime_4 = value;
	}

	inline static int32_t get_offset_of_frame_5() { return static_cast<int32_t>(offsetof(DebugPrint_t1518728472, ___frame_5)); }
	inline int32_t get_frame_5() const { return ___frame_5; }
	inline int32_t* get_address_of_frame_5() { return &___frame_5; }
	inline void set_frame_5(int32_t value)
	{
		___frame_5 = value;
	}

	inline static int32_t get_offset_of_frameRate_6() { return static_cast<int32_t>(offsetof(DebugPrint_t1518728472, ___frameRate_6)); }
	inline float get_frameRate_6() const { return ___frameRate_6; }
	inline float* get_address_of_frameRate_6() { return &___frameRate_6; }
	inline void set_frameRate_6(float value)
	{
		___frameRate_6 = value;
	}

	inline static int32_t get_offset_of_memSizeSystem_8() { return static_cast<int32_t>(offsetof(DebugPrint_t1518728472, ___memSizeSystem_8)); }
	inline float get_memSizeSystem_8() const { return ___memSizeSystem_8; }
	inline float* get_address_of_memSizeSystem_8() { return &___memSizeSystem_8; }
	inline void set_memSizeSystem_8(float value)
	{
		___memSizeSystem_8 = value;
	}

	inline static int32_t get_offset_of_memSizeGraphic_9() { return static_cast<int32_t>(offsetof(DebugPrint_t1518728472, ___memSizeGraphic_9)); }
	inline float get_memSizeGraphic_9() const { return ___memSizeGraphic_9; }
	inline float* get_address_of_memSizeGraphic_9() { return &___memSizeGraphic_9; }
	inline void set_memSizeGraphic_9(float value)
	{
		___memSizeGraphic_9 = value;
	}

	inline static int32_t get_offset_of_memSizeUsedHeap_10() { return static_cast<int32_t>(offsetof(DebugPrint_t1518728472, ___memSizeUsedHeap_10)); }
	inline float get_memSizeUsedHeap_10() const { return ___memSizeUsedHeap_10; }
	inline float* get_address_of_memSizeUsedHeap_10() { return &___memSizeUsedHeap_10; }
	inline void set_memSizeUsedHeap_10(float value)
	{
		___memSizeUsedHeap_10 = value;
	}

	inline static int32_t get_offset_of_memSizeGC_11() { return static_cast<int32_t>(offsetof(DebugPrint_t1518728472, ___memSizeGC_11)); }
	inline float get_memSizeGC_11() const { return ___memSizeGC_11; }
	inline float* get_address_of_memSizeGC_11() { return &___memSizeGC_11; }
	inline void set_memSizeGC_11(float value)
	{
		___memSizeGC_11 = value;
	}

	inline static int32_t get_offset_of_memSizeMonoHeap_12() { return static_cast<int32_t>(offsetof(DebugPrint_t1518728472, ___memSizeMonoHeap_12)); }
	inline float get_memSizeMonoHeap_12() const { return ___memSizeMonoHeap_12; }
	inline float* get_address_of_memSizeMonoHeap_12() { return &___memSizeMonoHeap_12; }
	inline void set_memSizeMonoHeap_12(float value)
	{
		___memSizeMonoHeap_12 = value;
	}

	inline static int32_t get_offset_of_memSizeMonoUsedHeap_13() { return static_cast<int32_t>(offsetof(DebugPrint_t1518728472, ___memSizeMonoUsedHeap_13)); }
	inline float get_memSizeMonoUsedHeap_13() const { return ___memSizeMonoUsedHeap_13; }
	inline float* get_address_of_memSizeMonoUsedHeap_13() { return &___memSizeMonoUsedHeap_13; }
	inline void set_memSizeMonoUsedHeap_13(float value)
	{
		___memSizeMonoUsedHeap_13 = value;
	}
};

struct DebugPrint_t1518728472_StaticFields
{
public:
	// Utage.DebugPrint Utage.DebugPrint::instance
	DebugPrint_t1518728472 * ___instance_2;

public:
	inline static int32_t get_offset_of_instance_2() { return static_cast<int32_t>(offsetof(DebugPrint_t1518728472_StaticFields, ___instance_2)); }
	inline DebugPrint_t1518728472 * get_instance_2() const { return ___instance_2; }
	inline DebugPrint_t1518728472 ** get_address_of_instance_2() { return &___instance_2; }
	inline void set_instance_2(DebugPrint_t1518728472 * value)
	{
		___instance_2 = value;
		Il2CppCodeGenWriteBarrier((&___instance_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DEBUGPRINT_T1518728472_H
#ifndef SYSTEMUIDEBUGMENU_T1631343349_H
#define SYSTEMUIDEBUGMENU_T1631343349_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.SystemUiDebugMenu
struct  SystemUiDebugMenu_t1631343349  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.GameObject Utage.SystemUiDebugMenu::buttonRoot
	GameObject_t1756533147 * ___buttonRoot_2;
	// UnityEngine.GameObject Utage.SystemUiDebugMenu::buttonViewRoot
	GameObject_t1756533147 * ___buttonViewRoot_3;
	// Utage.UguiLocalize Utage.SystemUiDebugMenu::buttonText
	UguiLocalize_t1715753735 * ___buttonText_4;
	// UnityEngine.GameObject Utage.SystemUiDebugMenu::debugInfo
	GameObject_t1756533147 * ___debugInfo_5;
	// UnityEngine.UI.Text Utage.SystemUiDebugMenu::debugInfoText
	Text_t356221433 * ___debugInfoText_6;
	// UnityEngine.GameObject Utage.SystemUiDebugMenu::debugLog
	GameObject_t1756533147 * ___debugLog_7;
	// UnityEngine.UI.Text Utage.SystemUiDebugMenu::debugLogText
	Text_t356221433 * ___debugLogText_8;
	// System.Boolean Utage.SystemUiDebugMenu::autoUpdateLogText
	bool ___autoUpdateLogText_9;
	// UnityEngine.GameObject Utage.SystemUiDebugMenu::rootDebugMenu
	GameObject_t1756533147 * ___rootDebugMenu_10;
	// UnityEngine.GameObject Utage.SystemUiDebugMenu::targetDeleteAllSaveData
	GameObject_t1756533147 * ___targetDeleteAllSaveData_11;
	// System.Boolean Utage.SystemUiDebugMenu::enabeReleaseBuild
	bool ___enabeReleaseBuild_12;
	// Utage.SystemUiDebugMenu/Mode Utage.SystemUiDebugMenu::currentMode
	int32_t ___currentMode_13;

public:
	inline static int32_t get_offset_of_buttonRoot_2() { return static_cast<int32_t>(offsetof(SystemUiDebugMenu_t1631343349, ___buttonRoot_2)); }
	inline GameObject_t1756533147 * get_buttonRoot_2() const { return ___buttonRoot_2; }
	inline GameObject_t1756533147 ** get_address_of_buttonRoot_2() { return &___buttonRoot_2; }
	inline void set_buttonRoot_2(GameObject_t1756533147 * value)
	{
		___buttonRoot_2 = value;
		Il2CppCodeGenWriteBarrier((&___buttonRoot_2), value);
	}

	inline static int32_t get_offset_of_buttonViewRoot_3() { return static_cast<int32_t>(offsetof(SystemUiDebugMenu_t1631343349, ___buttonViewRoot_3)); }
	inline GameObject_t1756533147 * get_buttonViewRoot_3() const { return ___buttonViewRoot_3; }
	inline GameObject_t1756533147 ** get_address_of_buttonViewRoot_3() { return &___buttonViewRoot_3; }
	inline void set_buttonViewRoot_3(GameObject_t1756533147 * value)
	{
		___buttonViewRoot_3 = value;
		Il2CppCodeGenWriteBarrier((&___buttonViewRoot_3), value);
	}

	inline static int32_t get_offset_of_buttonText_4() { return static_cast<int32_t>(offsetof(SystemUiDebugMenu_t1631343349, ___buttonText_4)); }
	inline UguiLocalize_t1715753735 * get_buttonText_4() const { return ___buttonText_4; }
	inline UguiLocalize_t1715753735 ** get_address_of_buttonText_4() { return &___buttonText_4; }
	inline void set_buttonText_4(UguiLocalize_t1715753735 * value)
	{
		___buttonText_4 = value;
		Il2CppCodeGenWriteBarrier((&___buttonText_4), value);
	}

	inline static int32_t get_offset_of_debugInfo_5() { return static_cast<int32_t>(offsetof(SystemUiDebugMenu_t1631343349, ___debugInfo_5)); }
	inline GameObject_t1756533147 * get_debugInfo_5() const { return ___debugInfo_5; }
	inline GameObject_t1756533147 ** get_address_of_debugInfo_5() { return &___debugInfo_5; }
	inline void set_debugInfo_5(GameObject_t1756533147 * value)
	{
		___debugInfo_5 = value;
		Il2CppCodeGenWriteBarrier((&___debugInfo_5), value);
	}

	inline static int32_t get_offset_of_debugInfoText_6() { return static_cast<int32_t>(offsetof(SystemUiDebugMenu_t1631343349, ___debugInfoText_6)); }
	inline Text_t356221433 * get_debugInfoText_6() const { return ___debugInfoText_6; }
	inline Text_t356221433 ** get_address_of_debugInfoText_6() { return &___debugInfoText_6; }
	inline void set_debugInfoText_6(Text_t356221433 * value)
	{
		___debugInfoText_6 = value;
		Il2CppCodeGenWriteBarrier((&___debugInfoText_6), value);
	}

	inline static int32_t get_offset_of_debugLog_7() { return static_cast<int32_t>(offsetof(SystemUiDebugMenu_t1631343349, ___debugLog_7)); }
	inline GameObject_t1756533147 * get_debugLog_7() const { return ___debugLog_7; }
	inline GameObject_t1756533147 ** get_address_of_debugLog_7() { return &___debugLog_7; }
	inline void set_debugLog_7(GameObject_t1756533147 * value)
	{
		___debugLog_7 = value;
		Il2CppCodeGenWriteBarrier((&___debugLog_7), value);
	}

	inline static int32_t get_offset_of_debugLogText_8() { return static_cast<int32_t>(offsetof(SystemUiDebugMenu_t1631343349, ___debugLogText_8)); }
	inline Text_t356221433 * get_debugLogText_8() const { return ___debugLogText_8; }
	inline Text_t356221433 ** get_address_of_debugLogText_8() { return &___debugLogText_8; }
	inline void set_debugLogText_8(Text_t356221433 * value)
	{
		___debugLogText_8 = value;
		Il2CppCodeGenWriteBarrier((&___debugLogText_8), value);
	}

	inline static int32_t get_offset_of_autoUpdateLogText_9() { return static_cast<int32_t>(offsetof(SystemUiDebugMenu_t1631343349, ___autoUpdateLogText_9)); }
	inline bool get_autoUpdateLogText_9() const { return ___autoUpdateLogText_9; }
	inline bool* get_address_of_autoUpdateLogText_9() { return &___autoUpdateLogText_9; }
	inline void set_autoUpdateLogText_9(bool value)
	{
		___autoUpdateLogText_9 = value;
	}

	inline static int32_t get_offset_of_rootDebugMenu_10() { return static_cast<int32_t>(offsetof(SystemUiDebugMenu_t1631343349, ___rootDebugMenu_10)); }
	inline GameObject_t1756533147 * get_rootDebugMenu_10() const { return ___rootDebugMenu_10; }
	inline GameObject_t1756533147 ** get_address_of_rootDebugMenu_10() { return &___rootDebugMenu_10; }
	inline void set_rootDebugMenu_10(GameObject_t1756533147 * value)
	{
		___rootDebugMenu_10 = value;
		Il2CppCodeGenWriteBarrier((&___rootDebugMenu_10), value);
	}

	inline static int32_t get_offset_of_targetDeleteAllSaveData_11() { return static_cast<int32_t>(offsetof(SystemUiDebugMenu_t1631343349, ___targetDeleteAllSaveData_11)); }
	inline GameObject_t1756533147 * get_targetDeleteAllSaveData_11() const { return ___targetDeleteAllSaveData_11; }
	inline GameObject_t1756533147 ** get_address_of_targetDeleteAllSaveData_11() { return &___targetDeleteAllSaveData_11; }
	inline void set_targetDeleteAllSaveData_11(GameObject_t1756533147 * value)
	{
		___targetDeleteAllSaveData_11 = value;
		Il2CppCodeGenWriteBarrier((&___targetDeleteAllSaveData_11), value);
	}

	inline static int32_t get_offset_of_enabeReleaseBuild_12() { return static_cast<int32_t>(offsetof(SystemUiDebugMenu_t1631343349, ___enabeReleaseBuild_12)); }
	inline bool get_enabeReleaseBuild_12() const { return ___enabeReleaseBuild_12; }
	inline bool* get_address_of_enabeReleaseBuild_12() { return &___enabeReleaseBuild_12; }
	inline void set_enabeReleaseBuild_12(bool value)
	{
		___enabeReleaseBuild_12 = value;
	}

	inline static int32_t get_offset_of_currentMode_13() { return static_cast<int32_t>(offsetof(SystemUiDebugMenu_t1631343349, ___currentMode_13)); }
	inline int32_t get_currentMode_13() const { return ___currentMode_13; }
	inline int32_t* get_address_of_currentMode_13() { return &___currentMode_13; }
	inline void set_currentMode_13(int32_t value)
	{
		___currentMode_13 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SYSTEMUIDEBUGMENU_T1631343349_H
#ifndef INDICATORICON_T1982044218_H
#define INDICATORICON_T1982044218_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.IndicatorIcon
struct  IndicatorIcon_t1982044218  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.GameObject Utage.IndicatorIcon::icon
	GameObject_t1756533147 * ___icon_2;
	// System.Single Utage.IndicatorIcon::animTime
	float ___animTime_3;
	// System.Single Utage.IndicatorIcon::animRotZ
	float ___animRotZ_4;
	// System.Boolean Utage.IndicatorIcon::isDeviceIndicator
	bool ___isDeviceIndicator_5;
	// System.Boolean Utage.IndicatorIcon::isStarting
	bool ___isStarting_6;
	// System.Single Utage.IndicatorIcon::rotZ
	float ___rotZ_7;
	// System.Collections.Generic.List`1<System.Object> Utage.IndicatorIcon::objList
	List_1_t2058570427 * ___objList_8;

public:
	inline static int32_t get_offset_of_icon_2() { return static_cast<int32_t>(offsetof(IndicatorIcon_t1982044218, ___icon_2)); }
	inline GameObject_t1756533147 * get_icon_2() const { return ___icon_2; }
	inline GameObject_t1756533147 ** get_address_of_icon_2() { return &___icon_2; }
	inline void set_icon_2(GameObject_t1756533147 * value)
	{
		___icon_2 = value;
		Il2CppCodeGenWriteBarrier((&___icon_2), value);
	}

	inline static int32_t get_offset_of_animTime_3() { return static_cast<int32_t>(offsetof(IndicatorIcon_t1982044218, ___animTime_3)); }
	inline float get_animTime_3() const { return ___animTime_3; }
	inline float* get_address_of_animTime_3() { return &___animTime_3; }
	inline void set_animTime_3(float value)
	{
		___animTime_3 = value;
	}

	inline static int32_t get_offset_of_animRotZ_4() { return static_cast<int32_t>(offsetof(IndicatorIcon_t1982044218, ___animRotZ_4)); }
	inline float get_animRotZ_4() const { return ___animRotZ_4; }
	inline float* get_address_of_animRotZ_4() { return &___animRotZ_4; }
	inline void set_animRotZ_4(float value)
	{
		___animRotZ_4 = value;
	}

	inline static int32_t get_offset_of_isDeviceIndicator_5() { return static_cast<int32_t>(offsetof(IndicatorIcon_t1982044218, ___isDeviceIndicator_5)); }
	inline bool get_isDeviceIndicator_5() const { return ___isDeviceIndicator_5; }
	inline bool* get_address_of_isDeviceIndicator_5() { return &___isDeviceIndicator_5; }
	inline void set_isDeviceIndicator_5(bool value)
	{
		___isDeviceIndicator_5 = value;
	}

	inline static int32_t get_offset_of_isStarting_6() { return static_cast<int32_t>(offsetof(IndicatorIcon_t1982044218, ___isStarting_6)); }
	inline bool get_isStarting_6() const { return ___isStarting_6; }
	inline bool* get_address_of_isStarting_6() { return &___isStarting_6; }
	inline void set_isStarting_6(bool value)
	{
		___isStarting_6 = value;
	}

	inline static int32_t get_offset_of_rotZ_7() { return static_cast<int32_t>(offsetof(IndicatorIcon_t1982044218, ___rotZ_7)); }
	inline float get_rotZ_7() const { return ___rotZ_7; }
	inline float* get_address_of_rotZ_7() { return &___rotZ_7; }
	inline void set_rotZ_7(float value)
	{
		___rotZ_7 = value;
	}

	inline static int32_t get_offset_of_objList_8() { return static_cast<int32_t>(offsetof(IndicatorIcon_t1982044218, ___objList_8)); }
	inline List_1_t2058570427 * get_objList_8() const { return ___objList_8; }
	inline List_1_t2058570427 ** get_address_of_objList_8() { return &___objList_8; }
	inline void set_objList_8(List_1_t2058570427 * value)
	{
		___objList_8 = value;
		Il2CppCodeGenWriteBarrier((&___objList_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INDICATORICON_T1982044218_H
#ifndef IMAGEEFFECTBASE_T432402543_H
#define IMAGEEFFECTBASE_T432402543_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.ImageEffectBase
struct  ImageEffectBase_t432402543  : public MonoBehaviour_t1158329972
{
public:
	// System.Collections.Generic.List`1<UnityEngine.Material> Utage.ImageEffectBase::createdMaterials
	List_1_t3857795355 * ___createdMaterials_2;

public:
	inline static int32_t get_offset_of_createdMaterials_2() { return static_cast<int32_t>(offsetof(ImageEffectBase_t432402543, ___createdMaterials_2)); }
	inline List_1_t3857795355 * get_createdMaterials_2() const { return ___createdMaterials_2; }
	inline List_1_t3857795355 ** get_address_of_createdMaterials_2() { return &___createdMaterials_2; }
	inline void set_createdMaterials_2(List_1_t3857795355 * value)
	{
		___createdMaterials_2 = value;
		Il2CppCodeGenWriteBarrier((&___createdMaterials_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IMAGEEFFECTBASE_T432402543_H
#ifndef SOUNDAUDIO_T3759608863_H
#define SOUNDAUDIO_T3759608863_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.SoundAudio
struct  SoundAudio_t3759608863  : public MonoBehaviour_t1158329972
{
public:
	// Utage.SoundAudio/SoundStreamStatus Utage.SoundAudio::status
	int32_t ___status_2;
	// UnityEngine.AudioSource Utage.SoundAudio::<AudioSource>k__BackingField
	AudioSource_t1135106623 * ___U3CAudioSourceU3Ek__BackingField_3;
	// UnityEngine.AudioSource Utage.SoundAudio::<AudioSourceForIntroLoop>k__BackingField
	AudioSource_t1135106623 * ___U3CAudioSourceForIntroLoopU3Ek__BackingField_4;
	// UnityEngine.AudioSource Utage.SoundAudio::<Audio0>k__BackingField
	AudioSource_t1135106623 * ___U3CAudio0U3Ek__BackingField_5;
	// UnityEngine.AudioSource Utage.SoundAudio::<Audio1>k__BackingField
	AudioSource_t1135106623 * ___U3CAudio1U3Ek__BackingField_6;
	// Utage.SoundData Utage.SoundAudio::<Data>k__BackingField
	SoundData_t3887839289 * ___U3CDataU3Ek__BackingField_7;
	// Utage.SoundAudioPlayer Utage.SoundAudio::<Player>k__BackingField
	SoundAudioPlayer_t3829317720 * ___U3CPlayerU3Ek__BackingField_8;
	// System.Boolean Utage.SoundAudio::<IsLoading>k__BackingField
	bool ___U3CIsLoadingU3Ek__BackingField_9;
	// Utage.LinearValue Utage.SoundAudio::fadeValue
	LinearValue_t662057272 * ___fadeValue_10;

public:
	inline static int32_t get_offset_of_status_2() { return static_cast<int32_t>(offsetof(SoundAudio_t3759608863, ___status_2)); }
	inline int32_t get_status_2() const { return ___status_2; }
	inline int32_t* get_address_of_status_2() { return &___status_2; }
	inline void set_status_2(int32_t value)
	{
		___status_2 = value;
	}

	inline static int32_t get_offset_of_U3CAudioSourceU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(SoundAudio_t3759608863, ___U3CAudioSourceU3Ek__BackingField_3)); }
	inline AudioSource_t1135106623 * get_U3CAudioSourceU3Ek__BackingField_3() const { return ___U3CAudioSourceU3Ek__BackingField_3; }
	inline AudioSource_t1135106623 ** get_address_of_U3CAudioSourceU3Ek__BackingField_3() { return &___U3CAudioSourceU3Ek__BackingField_3; }
	inline void set_U3CAudioSourceU3Ek__BackingField_3(AudioSource_t1135106623 * value)
	{
		___U3CAudioSourceU3Ek__BackingField_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CAudioSourceU3Ek__BackingField_3), value);
	}

	inline static int32_t get_offset_of_U3CAudioSourceForIntroLoopU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(SoundAudio_t3759608863, ___U3CAudioSourceForIntroLoopU3Ek__BackingField_4)); }
	inline AudioSource_t1135106623 * get_U3CAudioSourceForIntroLoopU3Ek__BackingField_4() const { return ___U3CAudioSourceForIntroLoopU3Ek__BackingField_4; }
	inline AudioSource_t1135106623 ** get_address_of_U3CAudioSourceForIntroLoopU3Ek__BackingField_4() { return &___U3CAudioSourceForIntroLoopU3Ek__BackingField_4; }
	inline void set_U3CAudioSourceForIntroLoopU3Ek__BackingField_4(AudioSource_t1135106623 * value)
	{
		___U3CAudioSourceForIntroLoopU3Ek__BackingField_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CAudioSourceForIntroLoopU3Ek__BackingField_4), value);
	}

	inline static int32_t get_offset_of_U3CAudio0U3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(SoundAudio_t3759608863, ___U3CAudio0U3Ek__BackingField_5)); }
	inline AudioSource_t1135106623 * get_U3CAudio0U3Ek__BackingField_5() const { return ___U3CAudio0U3Ek__BackingField_5; }
	inline AudioSource_t1135106623 ** get_address_of_U3CAudio0U3Ek__BackingField_5() { return &___U3CAudio0U3Ek__BackingField_5; }
	inline void set_U3CAudio0U3Ek__BackingField_5(AudioSource_t1135106623 * value)
	{
		___U3CAudio0U3Ek__BackingField_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3CAudio0U3Ek__BackingField_5), value);
	}

	inline static int32_t get_offset_of_U3CAudio1U3Ek__BackingField_6() { return static_cast<int32_t>(offsetof(SoundAudio_t3759608863, ___U3CAudio1U3Ek__BackingField_6)); }
	inline AudioSource_t1135106623 * get_U3CAudio1U3Ek__BackingField_6() const { return ___U3CAudio1U3Ek__BackingField_6; }
	inline AudioSource_t1135106623 ** get_address_of_U3CAudio1U3Ek__BackingField_6() { return &___U3CAudio1U3Ek__BackingField_6; }
	inline void set_U3CAudio1U3Ek__BackingField_6(AudioSource_t1135106623 * value)
	{
		___U3CAudio1U3Ek__BackingField_6 = value;
		Il2CppCodeGenWriteBarrier((&___U3CAudio1U3Ek__BackingField_6), value);
	}

	inline static int32_t get_offset_of_U3CDataU3Ek__BackingField_7() { return static_cast<int32_t>(offsetof(SoundAudio_t3759608863, ___U3CDataU3Ek__BackingField_7)); }
	inline SoundData_t3887839289 * get_U3CDataU3Ek__BackingField_7() const { return ___U3CDataU3Ek__BackingField_7; }
	inline SoundData_t3887839289 ** get_address_of_U3CDataU3Ek__BackingField_7() { return &___U3CDataU3Ek__BackingField_7; }
	inline void set_U3CDataU3Ek__BackingField_7(SoundData_t3887839289 * value)
	{
		___U3CDataU3Ek__BackingField_7 = value;
		Il2CppCodeGenWriteBarrier((&___U3CDataU3Ek__BackingField_7), value);
	}

	inline static int32_t get_offset_of_U3CPlayerU3Ek__BackingField_8() { return static_cast<int32_t>(offsetof(SoundAudio_t3759608863, ___U3CPlayerU3Ek__BackingField_8)); }
	inline SoundAudioPlayer_t3829317720 * get_U3CPlayerU3Ek__BackingField_8() const { return ___U3CPlayerU3Ek__BackingField_8; }
	inline SoundAudioPlayer_t3829317720 ** get_address_of_U3CPlayerU3Ek__BackingField_8() { return &___U3CPlayerU3Ek__BackingField_8; }
	inline void set_U3CPlayerU3Ek__BackingField_8(SoundAudioPlayer_t3829317720 * value)
	{
		___U3CPlayerU3Ek__BackingField_8 = value;
		Il2CppCodeGenWriteBarrier((&___U3CPlayerU3Ek__BackingField_8), value);
	}

	inline static int32_t get_offset_of_U3CIsLoadingU3Ek__BackingField_9() { return static_cast<int32_t>(offsetof(SoundAudio_t3759608863, ___U3CIsLoadingU3Ek__BackingField_9)); }
	inline bool get_U3CIsLoadingU3Ek__BackingField_9() const { return ___U3CIsLoadingU3Ek__BackingField_9; }
	inline bool* get_address_of_U3CIsLoadingU3Ek__BackingField_9() { return &___U3CIsLoadingU3Ek__BackingField_9; }
	inline void set_U3CIsLoadingU3Ek__BackingField_9(bool value)
	{
		___U3CIsLoadingU3Ek__BackingField_9 = value;
	}

	inline static int32_t get_offset_of_fadeValue_10() { return static_cast<int32_t>(offsetof(SoundAudio_t3759608863, ___fadeValue_10)); }
	inline LinearValue_t662057272 * get_fadeValue_10() const { return ___fadeValue_10; }
	inline LinearValue_t662057272 ** get_address_of_fadeValue_10() { return &___fadeValue_10; }
	inline void set_fadeValue_10(LinearValue_t662057272 * value)
	{
		___fadeValue_10 = value;
		Il2CppCodeGenWriteBarrier((&___fadeValue_10), value);
	}
};

struct SoundAudio_t3759608863_StaticFields
{
public:
	// System.Single[] Utage.SoundAudio::waveData
	SingleU5BU5D_t577127397* ___waveData_13;

public:
	inline static int32_t get_offset_of_waveData_13() { return static_cast<int32_t>(offsetof(SoundAudio_t3759608863_StaticFields, ___waveData_13)); }
	inline SingleU5BU5D_t577127397* get_waveData_13() const { return ___waveData_13; }
	inline SingleU5BU5D_t577127397** get_address_of_waveData_13() { return &___waveData_13; }
	inline void set_waveData_13(SingleU5BU5D_t577127397* value)
	{
		___waveData_13 = value;
		Il2CppCodeGenWriteBarrier((&___waveData_13), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SOUNDAUDIO_T3759608863_H
#ifndef SOUNDAUDIOPLAYER_T3829317720_H
#define SOUNDAUDIOPLAYER_T3829317720_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.SoundAudioPlayer
struct  SoundAudioPlayer_t3829317720  : public MonoBehaviour_t1158329972
{
public:
	// System.String Utage.SoundAudioPlayer::<Label>k__BackingField
	String_t* ___U3CLabelU3Ek__BackingField_2;
	// Utage.SoundGroup Utage.SoundAudioPlayer::<Group>k__BackingField
	SoundGroup_t2016089996 * ___U3CGroupU3Ek__BackingField_3;
	// Utage.SoundAudio Utage.SoundAudioPlayer::<Audio>k__BackingField
	SoundAudio_t3759608863 * ___U3CAudioU3Ek__BackingField_4;
	// Utage.SoundAudio Utage.SoundAudioPlayer::<FadeOutAudio>k__BackingField
	SoundAudio_t3759608863 * ___U3CFadeOutAudioU3Ek__BackingField_5;
	// System.Collections.Generic.List`1<Utage.SoundAudio> Utage.SoundAudioPlayer::<AudioList>k__BackingField
	List_1_t3128729995 * ___U3CAudioListU3Ek__BackingField_6;
	// System.Collections.Generic.List`1<Utage.SoundAudio> Utage.SoundAudioPlayer::<CurrentFrameAudioList>k__BackingField
	List_1_t3128729995 * ___U3CCurrentFrameAudioListU3Ek__BackingField_7;

public:
	inline static int32_t get_offset_of_U3CLabelU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(SoundAudioPlayer_t3829317720, ___U3CLabelU3Ek__BackingField_2)); }
	inline String_t* get_U3CLabelU3Ek__BackingField_2() const { return ___U3CLabelU3Ek__BackingField_2; }
	inline String_t** get_address_of_U3CLabelU3Ek__BackingField_2() { return &___U3CLabelU3Ek__BackingField_2; }
	inline void set_U3CLabelU3Ek__BackingField_2(String_t* value)
	{
		___U3CLabelU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CLabelU3Ek__BackingField_2), value);
	}

	inline static int32_t get_offset_of_U3CGroupU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(SoundAudioPlayer_t3829317720, ___U3CGroupU3Ek__BackingField_3)); }
	inline SoundGroup_t2016089996 * get_U3CGroupU3Ek__BackingField_3() const { return ___U3CGroupU3Ek__BackingField_3; }
	inline SoundGroup_t2016089996 ** get_address_of_U3CGroupU3Ek__BackingField_3() { return &___U3CGroupU3Ek__BackingField_3; }
	inline void set_U3CGroupU3Ek__BackingField_3(SoundGroup_t2016089996 * value)
	{
		___U3CGroupU3Ek__BackingField_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CGroupU3Ek__BackingField_3), value);
	}

	inline static int32_t get_offset_of_U3CAudioU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(SoundAudioPlayer_t3829317720, ___U3CAudioU3Ek__BackingField_4)); }
	inline SoundAudio_t3759608863 * get_U3CAudioU3Ek__BackingField_4() const { return ___U3CAudioU3Ek__BackingField_4; }
	inline SoundAudio_t3759608863 ** get_address_of_U3CAudioU3Ek__BackingField_4() { return &___U3CAudioU3Ek__BackingField_4; }
	inline void set_U3CAudioU3Ek__BackingField_4(SoundAudio_t3759608863 * value)
	{
		___U3CAudioU3Ek__BackingField_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CAudioU3Ek__BackingField_4), value);
	}

	inline static int32_t get_offset_of_U3CFadeOutAudioU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(SoundAudioPlayer_t3829317720, ___U3CFadeOutAudioU3Ek__BackingField_5)); }
	inline SoundAudio_t3759608863 * get_U3CFadeOutAudioU3Ek__BackingField_5() const { return ___U3CFadeOutAudioU3Ek__BackingField_5; }
	inline SoundAudio_t3759608863 ** get_address_of_U3CFadeOutAudioU3Ek__BackingField_5() { return &___U3CFadeOutAudioU3Ek__BackingField_5; }
	inline void set_U3CFadeOutAudioU3Ek__BackingField_5(SoundAudio_t3759608863 * value)
	{
		___U3CFadeOutAudioU3Ek__BackingField_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3CFadeOutAudioU3Ek__BackingField_5), value);
	}

	inline static int32_t get_offset_of_U3CAudioListU3Ek__BackingField_6() { return static_cast<int32_t>(offsetof(SoundAudioPlayer_t3829317720, ___U3CAudioListU3Ek__BackingField_6)); }
	inline List_1_t3128729995 * get_U3CAudioListU3Ek__BackingField_6() const { return ___U3CAudioListU3Ek__BackingField_6; }
	inline List_1_t3128729995 ** get_address_of_U3CAudioListU3Ek__BackingField_6() { return &___U3CAudioListU3Ek__BackingField_6; }
	inline void set_U3CAudioListU3Ek__BackingField_6(List_1_t3128729995 * value)
	{
		___U3CAudioListU3Ek__BackingField_6 = value;
		Il2CppCodeGenWriteBarrier((&___U3CAudioListU3Ek__BackingField_6), value);
	}

	inline static int32_t get_offset_of_U3CCurrentFrameAudioListU3Ek__BackingField_7() { return static_cast<int32_t>(offsetof(SoundAudioPlayer_t3829317720, ___U3CCurrentFrameAudioListU3Ek__BackingField_7)); }
	inline List_1_t3128729995 * get_U3CCurrentFrameAudioListU3Ek__BackingField_7() const { return ___U3CCurrentFrameAudioListU3Ek__BackingField_7; }
	inline List_1_t3128729995 ** get_address_of_U3CCurrentFrameAudioListU3Ek__BackingField_7() { return &___U3CCurrentFrameAudioListU3Ek__BackingField_7; }
	inline void set_U3CCurrentFrameAudioListU3Ek__BackingField_7(List_1_t3128729995 * value)
	{
		___U3CCurrentFrameAudioListU3Ek__BackingField_7 = value;
		Il2CppCodeGenWriteBarrier((&___U3CCurrentFrameAudioListU3Ek__BackingField_7), value);
	}
};

struct SoundAudioPlayer_t3829317720_StaticFields
{
public:
	// System.Predicate`1<Utage.SoundAudio> Utage.SoundAudioPlayer::<>f__am$cache0
	Predicate_1_t2202578978 * ___U3CU3Ef__amU24cache0_9;

public:
	inline static int32_t get_offset_of_U3CU3Ef__amU24cache0_9() { return static_cast<int32_t>(offsetof(SoundAudioPlayer_t3829317720_StaticFields, ___U3CU3Ef__amU24cache0_9)); }
	inline Predicate_1_t2202578978 * get_U3CU3Ef__amU24cache0_9() const { return ___U3CU3Ef__amU24cache0_9; }
	inline Predicate_1_t2202578978 ** get_address_of_U3CU3Ef__amU24cache0_9() { return &___U3CU3Ef__amU24cache0_9; }
	inline void set_U3CU3Ef__amU24cache0_9(Predicate_1_t2202578978 * value)
	{
		___U3CU3Ef__amU24cache0_9 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache0_9), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SOUNDAUDIOPLAYER_T3829317720_H
#ifndef UGUITRANSITION_T3998485683_H
#define UGUITRANSITION_T3998485683_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.UguiTransition
struct  UguiTransition_t3998485683  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.UI.Graphic Utage.UguiTransition::target
	Graphic_t2426225576 * ___target_2;
	// UnityEngine.Texture Utage.UguiTransition::ruleTexture
	Texture_t2243626319 * ___ruleTexture_3;
	// System.Single Utage.UguiTransition::strengh
	float ___strengh_4;
	// System.Single Utage.UguiTransition::vague
	float ___vague_5;
	// System.Boolean Utage.UguiTransition::<IsPremultipliedAlpha>k__BackingField
	bool ___U3CIsPremultipliedAlphaU3Ek__BackingField_6;
	// UnityEngine.Material Utage.UguiTransition::<DefaultMaterial>k__BackingField
	Material_t193706927 * ___U3CDefaultMaterialU3Ek__BackingField_7;

public:
	inline static int32_t get_offset_of_target_2() { return static_cast<int32_t>(offsetof(UguiTransition_t3998485683, ___target_2)); }
	inline Graphic_t2426225576 * get_target_2() const { return ___target_2; }
	inline Graphic_t2426225576 ** get_address_of_target_2() { return &___target_2; }
	inline void set_target_2(Graphic_t2426225576 * value)
	{
		___target_2 = value;
		Il2CppCodeGenWriteBarrier((&___target_2), value);
	}

	inline static int32_t get_offset_of_ruleTexture_3() { return static_cast<int32_t>(offsetof(UguiTransition_t3998485683, ___ruleTexture_3)); }
	inline Texture_t2243626319 * get_ruleTexture_3() const { return ___ruleTexture_3; }
	inline Texture_t2243626319 ** get_address_of_ruleTexture_3() { return &___ruleTexture_3; }
	inline void set_ruleTexture_3(Texture_t2243626319 * value)
	{
		___ruleTexture_3 = value;
		Il2CppCodeGenWriteBarrier((&___ruleTexture_3), value);
	}

	inline static int32_t get_offset_of_strengh_4() { return static_cast<int32_t>(offsetof(UguiTransition_t3998485683, ___strengh_4)); }
	inline float get_strengh_4() const { return ___strengh_4; }
	inline float* get_address_of_strengh_4() { return &___strengh_4; }
	inline void set_strengh_4(float value)
	{
		___strengh_4 = value;
	}

	inline static int32_t get_offset_of_vague_5() { return static_cast<int32_t>(offsetof(UguiTransition_t3998485683, ___vague_5)); }
	inline float get_vague_5() const { return ___vague_5; }
	inline float* get_address_of_vague_5() { return &___vague_5; }
	inline void set_vague_5(float value)
	{
		___vague_5 = value;
	}

	inline static int32_t get_offset_of_U3CIsPremultipliedAlphaU3Ek__BackingField_6() { return static_cast<int32_t>(offsetof(UguiTransition_t3998485683, ___U3CIsPremultipliedAlphaU3Ek__BackingField_6)); }
	inline bool get_U3CIsPremultipliedAlphaU3Ek__BackingField_6() const { return ___U3CIsPremultipliedAlphaU3Ek__BackingField_6; }
	inline bool* get_address_of_U3CIsPremultipliedAlphaU3Ek__BackingField_6() { return &___U3CIsPremultipliedAlphaU3Ek__BackingField_6; }
	inline void set_U3CIsPremultipliedAlphaU3Ek__BackingField_6(bool value)
	{
		___U3CIsPremultipliedAlphaU3Ek__BackingField_6 = value;
	}

	inline static int32_t get_offset_of_U3CDefaultMaterialU3Ek__BackingField_7() { return static_cast<int32_t>(offsetof(UguiTransition_t3998485683, ___U3CDefaultMaterialU3Ek__BackingField_7)); }
	inline Material_t193706927 * get_U3CDefaultMaterialU3Ek__BackingField_7() const { return ___U3CDefaultMaterialU3Ek__BackingField_7; }
	inline Material_t193706927 ** get_address_of_U3CDefaultMaterialU3Ek__BackingField_7() { return &___U3CDefaultMaterialU3Ek__BackingField_7; }
	inline void set_U3CDefaultMaterialU3Ek__BackingField_7(Material_t193706927 * value)
	{
		___U3CDefaultMaterialU3Ek__BackingField_7 = value;
		Il2CppCodeGenWriteBarrier((&___U3CDefaultMaterialU3Ek__BackingField_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UGUITRANSITION_T3998485683_H
#ifndef UIBEHAVIOUR_T3960014691_H
#define UIBEHAVIOUR_T3960014691_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.EventSystems.UIBehaviour
struct  UIBehaviour_t3960014691  : public MonoBehaviour_t1158329972
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UIBEHAVIOUR_T3960014691_H
#ifndef SOUNDGROUP_T2016089996_H
#define SOUNDGROUP_T2016089996_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.SoundGroup
struct  SoundGroup_t2016089996  : public MonoBehaviour_t1158329972
{
public:
	// Utage.SoundManagerSystem Utage.SoundGroup::<SoundManagerSystem>k__BackingField
	SoundManagerSystem_t1680680183 * ___U3CSoundManagerSystemU3Ek__BackingField_2;
	// System.Collections.Generic.Dictionary`2<System.String,Utage.SoundAudioPlayer> Utage.SoundGroup::playerList
	Dictionary_2_t1449129686 * ___playerList_3;
	// System.Boolean Utage.SoundGroup::multiPlay
	bool ___multiPlay_4;
	// System.Boolean Utage.SoundGroup::autoDestoryPlayer
	bool ___autoDestoryPlayer_5;
	// System.Single Utage.SoundGroup::masterVolume
	float ___masterVolume_6;
	// System.Single Utage.SoundGroup::groupVolume
	float ___groupVolume_7;
	// System.Collections.Generic.List`1<Utage.SoundGroup> Utage.SoundGroup::duckGroups
	List_1_t1385211128 * ___duckGroups_8;
	// System.Single Utage.SoundGroup::<DuckVolume>k__BackingField
	float ___U3CDuckVolumeU3Ek__BackingField_9;
	// System.Single Utage.SoundGroup::duckVelocity
	float ___duckVelocity_10;

public:
	inline static int32_t get_offset_of_U3CSoundManagerSystemU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(SoundGroup_t2016089996, ___U3CSoundManagerSystemU3Ek__BackingField_2)); }
	inline SoundManagerSystem_t1680680183 * get_U3CSoundManagerSystemU3Ek__BackingField_2() const { return ___U3CSoundManagerSystemU3Ek__BackingField_2; }
	inline SoundManagerSystem_t1680680183 ** get_address_of_U3CSoundManagerSystemU3Ek__BackingField_2() { return &___U3CSoundManagerSystemU3Ek__BackingField_2; }
	inline void set_U3CSoundManagerSystemU3Ek__BackingField_2(SoundManagerSystem_t1680680183 * value)
	{
		___U3CSoundManagerSystemU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CSoundManagerSystemU3Ek__BackingField_2), value);
	}

	inline static int32_t get_offset_of_playerList_3() { return static_cast<int32_t>(offsetof(SoundGroup_t2016089996, ___playerList_3)); }
	inline Dictionary_2_t1449129686 * get_playerList_3() const { return ___playerList_3; }
	inline Dictionary_2_t1449129686 ** get_address_of_playerList_3() { return &___playerList_3; }
	inline void set_playerList_3(Dictionary_2_t1449129686 * value)
	{
		___playerList_3 = value;
		Il2CppCodeGenWriteBarrier((&___playerList_3), value);
	}

	inline static int32_t get_offset_of_multiPlay_4() { return static_cast<int32_t>(offsetof(SoundGroup_t2016089996, ___multiPlay_4)); }
	inline bool get_multiPlay_4() const { return ___multiPlay_4; }
	inline bool* get_address_of_multiPlay_4() { return &___multiPlay_4; }
	inline void set_multiPlay_4(bool value)
	{
		___multiPlay_4 = value;
	}

	inline static int32_t get_offset_of_autoDestoryPlayer_5() { return static_cast<int32_t>(offsetof(SoundGroup_t2016089996, ___autoDestoryPlayer_5)); }
	inline bool get_autoDestoryPlayer_5() const { return ___autoDestoryPlayer_5; }
	inline bool* get_address_of_autoDestoryPlayer_5() { return &___autoDestoryPlayer_5; }
	inline void set_autoDestoryPlayer_5(bool value)
	{
		___autoDestoryPlayer_5 = value;
	}

	inline static int32_t get_offset_of_masterVolume_6() { return static_cast<int32_t>(offsetof(SoundGroup_t2016089996, ___masterVolume_6)); }
	inline float get_masterVolume_6() const { return ___masterVolume_6; }
	inline float* get_address_of_masterVolume_6() { return &___masterVolume_6; }
	inline void set_masterVolume_6(float value)
	{
		___masterVolume_6 = value;
	}

	inline static int32_t get_offset_of_groupVolume_7() { return static_cast<int32_t>(offsetof(SoundGroup_t2016089996, ___groupVolume_7)); }
	inline float get_groupVolume_7() const { return ___groupVolume_7; }
	inline float* get_address_of_groupVolume_7() { return &___groupVolume_7; }
	inline void set_groupVolume_7(float value)
	{
		___groupVolume_7 = value;
	}

	inline static int32_t get_offset_of_duckGroups_8() { return static_cast<int32_t>(offsetof(SoundGroup_t2016089996, ___duckGroups_8)); }
	inline List_1_t1385211128 * get_duckGroups_8() const { return ___duckGroups_8; }
	inline List_1_t1385211128 ** get_address_of_duckGroups_8() { return &___duckGroups_8; }
	inline void set_duckGroups_8(List_1_t1385211128 * value)
	{
		___duckGroups_8 = value;
		Il2CppCodeGenWriteBarrier((&___duckGroups_8), value);
	}

	inline static int32_t get_offset_of_U3CDuckVolumeU3Ek__BackingField_9() { return static_cast<int32_t>(offsetof(SoundGroup_t2016089996, ___U3CDuckVolumeU3Ek__BackingField_9)); }
	inline float get_U3CDuckVolumeU3Ek__BackingField_9() const { return ___U3CDuckVolumeU3Ek__BackingField_9; }
	inline float* get_address_of_U3CDuckVolumeU3Ek__BackingField_9() { return &___U3CDuckVolumeU3Ek__BackingField_9; }
	inline void set_U3CDuckVolumeU3Ek__BackingField_9(float value)
	{
		___U3CDuckVolumeU3Ek__BackingField_9 = value;
	}

	inline static int32_t get_offset_of_duckVelocity_10() { return static_cast<int32_t>(offsetof(SoundGroup_t2016089996, ___duckVelocity_10)); }
	inline float get_duckVelocity_10() const { return ___duckVelocity_10; }
	inline float* get_address_of_duckVelocity_10() { return &___duckVelocity_10; }
	inline void set_duckVelocity_10(float value)
	{
		___duckVelocity_10 = value;
	}
};

struct SoundGroup_t2016089996_StaticFields
{
public:
	// System.Predicate`1<Utage.SoundGroup> Utage.SoundGroup::<>f__am$cache0
	Predicate_1_t459060111 * ___U3CU3Ef__amU24cache0_13;

public:
	inline static int32_t get_offset_of_U3CU3Ef__amU24cache0_13() { return static_cast<int32_t>(offsetof(SoundGroup_t2016089996_StaticFields, ___U3CU3Ef__amU24cache0_13)); }
	inline Predicate_1_t459060111 * get_U3CU3Ef__amU24cache0_13() const { return ___U3CU3Ef__amU24cache0_13; }
	inline Predicate_1_t459060111 ** get_address_of_U3CU3Ef__amU24cache0_13() { return &___U3CU3Ef__amU24cache0_13; }
	inline void set_U3CU3Ef__amU24cache0_13(Predicate_1_t459060111 * value)
	{
		___U3CU3Ef__amU24cache0_13 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache0_13), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SOUNDGROUP_T2016089996_H
#ifndef UGUICROSSFADERAWIMAGE_T1509295035_H
#define UGUICROSSFADERAWIMAGE_T1509295035_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.UguiCrossFadeRawImage
struct  UguiCrossFadeRawImage_t1509295035  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.Texture Utage.UguiCrossFadeRawImage::fadeTexture
	Texture_t2243626319 * ___fadeTexture_2;
	// System.Single Utage.UguiCrossFadeRawImage::strengh
	float ___strengh_3;
	// UnityEngine.UI.Graphic Utage.UguiCrossFadeRawImage::target
	Graphic_t2426225576 * ___target_4;
	// Utage.Timer Utage.UguiCrossFadeRawImage::timer
	Timer_t2904185433 * ___timer_5;
	// UnityEngine.Material Utage.UguiCrossFadeRawImage::lastMaterial
	Material_t193706927 * ___lastMaterial_6;
	// UnityEngine.Material Utage.UguiCrossFadeRawImage::corssFadeMaterial
	Material_t193706927 * ___corssFadeMaterial_7;

public:
	inline static int32_t get_offset_of_fadeTexture_2() { return static_cast<int32_t>(offsetof(UguiCrossFadeRawImage_t1509295035, ___fadeTexture_2)); }
	inline Texture_t2243626319 * get_fadeTexture_2() const { return ___fadeTexture_2; }
	inline Texture_t2243626319 ** get_address_of_fadeTexture_2() { return &___fadeTexture_2; }
	inline void set_fadeTexture_2(Texture_t2243626319 * value)
	{
		___fadeTexture_2 = value;
		Il2CppCodeGenWriteBarrier((&___fadeTexture_2), value);
	}

	inline static int32_t get_offset_of_strengh_3() { return static_cast<int32_t>(offsetof(UguiCrossFadeRawImage_t1509295035, ___strengh_3)); }
	inline float get_strengh_3() const { return ___strengh_3; }
	inline float* get_address_of_strengh_3() { return &___strengh_3; }
	inline void set_strengh_3(float value)
	{
		___strengh_3 = value;
	}

	inline static int32_t get_offset_of_target_4() { return static_cast<int32_t>(offsetof(UguiCrossFadeRawImage_t1509295035, ___target_4)); }
	inline Graphic_t2426225576 * get_target_4() const { return ___target_4; }
	inline Graphic_t2426225576 ** get_address_of_target_4() { return &___target_4; }
	inline void set_target_4(Graphic_t2426225576 * value)
	{
		___target_4 = value;
		Il2CppCodeGenWriteBarrier((&___target_4), value);
	}

	inline static int32_t get_offset_of_timer_5() { return static_cast<int32_t>(offsetof(UguiCrossFadeRawImage_t1509295035, ___timer_5)); }
	inline Timer_t2904185433 * get_timer_5() const { return ___timer_5; }
	inline Timer_t2904185433 ** get_address_of_timer_5() { return &___timer_5; }
	inline void set_timer_5(Timer_t2904185433 * value)
	{
		___timer_5 = value;
		Il2CppCodeGenWriteBarrier((&___timer_5), value);
	}

	inline static int32_t get_offset_of_lastMaterial_6() { return static_cast<int32_t>(offsetof(UguiCrossFadeRawImage_t1509295035, ___lastMaterial_6)); }
	inline Material_t193706927 * get_lastMaterial_6() const { return ___lastMaterial_6; }
	inline Material_t193706927 ** get_address_of_lastMaterial_6() { return &___lastMaterial_6; }
	inline void set_lastMaterial_6(Material_t193706927 * value)
	{
		___lastMaterial_6 = value;
		Il2CppCodeGenWriteBarrier((&___lastMaterial_6), value);
	}

	inline static int32_t get_offset_of_corssFadeMaterial_7() { return static_cast<int32_t>(offsetof(UguiCrossFadeRawImage_t1509295035, ___corssFadeMaterial_7)); }
	inline Material_t193706927 * get_corssFadeMaterial_7() const { return ___corssFadeMaterial_7; }
	inline Material_t193706927 ** get_address_of_corssFadeMaterial_7() { return &___corssFadeMaterial_7; }
	inline void set_corssFadeMaterial_7(Material_t193706927 * value)
	{
		___corssFadeMaterial_7 = value;
		Il2CppCodeGenWriteBarrier((&___corssFadeMaterial_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UGUICROSSFADERAWIMAGE_T1509295035_H
#ifndef BASEMESHEFFECT_T1728560551_H
#define BASEMESHEFFECT_T1728560551_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.BaseMeshEffect
struct  BaseMeshEffect_t1728560551  : public UIBehaviour_t3960014691
{
public:
	// UnityEngine.UI.Graphic UnityEngine.UI.BaseMeshEffect::m_Graphic
	Graphic_t2426225576 * ___m_Graphic_2;

public:
	inline static int32_t get_offset_of_m_Graphic_2() { return static_cast<int32_t>(offsetof(BaseMeshEffect_t1728560551, ___m_Graphic_2)); }
	inline Graphic_t2426225576 * get_m_Graphic_2() const { return ___m_Graphic_2; }
	inline Graphic_t2426225576 ** get_address_of_m_Graphic_2() { return &___m_Graphic_2; }
	inline void set_m_Graphic_2(Graphic_t2426225576 * value)
	{
		___m_Graphic_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_Graphic_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BASEMESHEFFECT_T1728560551_H
#ifndef IMAGEEFFECTSINGELSHADERBASE_T892515528_H
#define IMAGEEFFECTSINGELSHADERBASE_T892515528_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.ImageEffectSingelShaderBase
struct  ImageEffectSingelShaderBase_t892515528  : public ImageEffectBase_t432402543
{
public:
	// UnityEngine.Shader Utage.ImageEffectSingelShaderBase::shader
	Shader_t2430389951 * ___shader_4;
	// UnityEngine.Material Utage.ImageEffectSingelShaderBase::<Material>k__BackingField
	Material_t193706927 * ___U3CMaterialU3Ek__BackingField_5;
	// UnityEngine.Shader Utage.ImageEffectSingelShaderBase::tmpShader
	Shader_t2430389951 * ___tmpShader_6;

public:
	inline static int32_t get_offset_of_shader_4() { return static_cast<int32_t>(offsetof(ImageEffectSingelShaderBase_t892515528, ___shader_4)); }
	inline Shader_t2430389951 * get_shader_4() const { return ___shader_4; }
	inline Shader_t2430389951 ** get_address_of_shader_4() { return &___shader_4; }
	inline void set_shader_4(Shader_t2430389951 * value)
	{
		___shader_4 = value;
		Il2CppCodeGenWriteBarrier((&___shader_4), value);
	}

	inline static int32_t get_offset_of_U3CMaterialU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(ImageEffectSingelShaderBase_t892515528, ___U3CMaterialU3Ek__BackingField_5)); }
	inline Material_t193706927 * get_U3CMaterialU3Ek__BackingField_5() const { return ___U3CMaterialU3Ek__BackingField_5; }
	inline Material_t193706927 ** get_address_of_U3CMaterialU3Ek__BackingField_5() { return &___U3CMaterialU3Ek__BackingField_5; }
	inline void set_U3CMaterialU3Ek__BackingField_5(Material_t193706927 * value)
	{
		___U3CMaterialU3Ek__BackingField_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3CMaterialU3Ek__BackingField_5), value);
	}

	inline static int32_t get_offset_of_tmpShader_6() { return static_cast<int32_t>(offsetof(ImageEffectSingelShaderBase_t892515528, ___tmpShader_6)); }
	inline Shader_t2430389951 * get_tmpShader_6() const { return ___tmpShader_6; }
	inline Shader_t2430389951 ** get_address_of_tmpShader_6() { return &___tmpShader_6; }
	inline void set_tmpShader_6(Shader_t2430389951 * value)
	{
		___tmpShader_6 = value;
		Il2CppCodeGenWriteBarrier((&___tmpShader_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IMAGEEFFECTSINGELSHADERBASE_T892515528_H
#ifndef EYEBLINKAVATAR_T3311504802_H
#define EYEBLINKAVATAR_T3311504802_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.EyeBlinkAvatar
struct  EyeBlinkAvatar_t3311504802  : public EyeBlinkBase_t1662968566
{
public:
	// Utage.AvatarImage Utage.EyeBlinkAvatar::avator
	AvatarImage_t1946614104 * ___avator_7;

public:
	inline static int32_t get_offset_of_avator_7() { return static_cast<int32_t>(offsetof(EyeBlinkAvatar_t3311504802, ___avator_7)); }
	inline AvatarImage_t1946614104 * get_avator_7() const { return ___avator_7; }
	inline AvatarImage_t1946614104 ** get_address_of_avator_7() { return &___avator_7; }
	inline void set_avator_7(AvatarImage_t1946614104 * value)
	{
		___avator_7 = value;
		Il2CppCodeGenWriteBarrier((&___avator_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EYEBLINKAVATAR_T3311504802_H
#ifndef UGUICROSSFADEDICING_T2659796554_H
#define UGUICROSSFADEDICING_T2659796554_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.UguiCrossFadeDicing
struct  UguiCrossFadeDicing_t2659796554  : public UguiCrossFadeRawImage_t1509295035
{
public:
	// Utage.DicingTextureData Utage.UguiCrossFadeDicing::fadePatternData
	DicingTextureData_t752335795 * ___fadePatternData_8;

public:
	inline static int32_t get_offset_of_fadePatternData_8() { return static_cast<int32_t>(offsetof(UguiCrossFadeDicing_t2659796554, ___fadePatternData_8)); }
	inline DicingTextureData_t752335795 * get_fadePatternData_8() const { return ___fadePatternData_8; }
	inline DicingTextureData_t752335795 ** get_address_of_fadePatternData_8() { return &___fadePatternData_8; }
	inline void set_fadePatternData_8(DicingTextureData_t752335795 * value)
	{
		___fadePatternData_8 = value;
		Il2CppCodeGenWriteBarrier((&___fadePatternData_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UGUICROSSFADEDICING_T2659796554_H
#ifndef SYSTEMUIDIALOG2BUTTON_T2800622875_H
#define SYSTEMUIDIALOG2BUTTON_T2800622875_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.SystemUiDialog2Button
struct  SystemUiDialog2Button_t2800622875  : public SystemUiDialog1Button_t2800515000
{
public:
	// UnityEngine.UI.Text Utage.SystemUiDialog2Button::button2Text
	Text_t356221433 * ___button2Text_5;
	// UnityEngine.Events.UnityEvent Utage.SystemUiDialog2Button::OnClickButton2
	UnityEvent_t408735097 * ___OnClickButton2_6;

public:
	inline static int32_t get_offset_of_button2Text_5() { return static_cast<int32_t>(offsetof(SystemUiDialog2Button_t2800622875, ___button2Text_5)); }
	inline Text_t356221433 * get_button2Text_5() const { return ___button2Text_5; }
	inline Text_t356221433 ** get_address_of_button2Text_5() { return &___button2Text_5; }
	inline void set_button2Text_5(Text_t356221433 * value)
	{
		___button2Text_5 = value;
		Il2CppCodeGenWriteBarrier((&___button2Text_5), value);
	}

	inline static int32_t get_offset_of_OnClickButton2_6() { return static_cast<int32_t>(offsetof(SystemUiDialog2Button_t2800622875, ___OnClickButton2_6)); }
	inline UnityEvent_t408735097 * get_OnClickButton2_6() const { return ___OnClickButton2_6; }
	inline UnityEvent_t408735097 ** get_address_of_OnClickButton2_6() { return &___OnClickButton2_6; }
	inline void set_OnClickButton2_6(UnityEvent_t408735097 * value)
	{
		___OnClickButton2_6 = value;
		Il2CppCodeGenWriteBarrier((&___OnClickButton2_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SYSTEMUIDIALOG2BUTTON_T2800622875_H
#ifndef BLOOM_T2447217777_H
#define BLOOM_T2447217777_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.Bloom
struct  Bloom_t2447217777  : public ImageEffectSingelShaderBase_t892515528
{
public:
	// System.Single Utage.Bloom::threshold
	float ___threshold_7;
	// System.Single Utage.Bloom::intensity
	float ___intensity_8;
	// System.Single Utage.Bloom::blurSize
	float ___blurSize_9;
	// Utage.Bloom/Resolution Utage.Bloom::resolution
	int32_t ___resolution_10;
	// System.Int32 Utage.Bloom::blurIterations
	int32_t ___blurIterations_11;
	// Utage.Bloom/BlurType Utage.Bloom::blurType
	int32_t ___blurType_12;

public:
	inline static int32_t get_offset_of_threshold_7() { return static_cast<int32_t>(offsetof(Bloom_t2447217777, ___threshold_7)); }
	inline float get_threshold_7() const { return ___threshold_7; }
	inline float* get_address_of_threshold_7() { return &___threshold_7; }
	inline void set_threshold_7(float value)
	{
		___threshold_7 = value;
	}

	inline static int32_t get_offset_of_intensity_8() { return static_cast<int32_t>(offsetof(Bloom_t2447217777, ___intensity_8)); }
	inline float get_intensity_8() const { return ___intensity_8; }
	inline float* get_address_of_intensity_8() { return &___intensity_8; }
	inline void set_intensity_8(float value)
	{
		___intensity_8 = value;
	}

	inline static int32_t get_offset_of_blurSize_9() { return static_cast<int32_t>(offsetof(Bloom_t2447217777, ___blurSize_9)); }
	inline float get_blurSize_9() const { return ___blurSize_9; }
	inline float* get_address_of_blurSize_9() { return &___blurSize_9; }
	inline void set_blurSize_9(float value)
	{
		___blurSize_9 = value;
	}

	inline static int32_t get_offset_of_resolution_10() { return static_cast<int32_t>(offsetof(Bloom_t2447217777, ___resolution_10)); }
	inline int32_t get_resolution_10() const { return ___resolution_10; }
	inline int32_t* get_address_of_resolution_10() { return &___resolution_10; }
	inline void set_resolution_10(int32_t value)
	{
		___resolution_10 = value;
	}

	inline static int32_t get_offset_of_blurIterations_11() { return static_cast<int32_t>(offsetof(Bloom_t2447217777, ___blurIterations_11)); }
	inline int32_t get_blurIterations_11() const { return ___blurIterations_11; }
	inline int32_t* get_address_of_blurIterations_11() { return &___blurIterations_11; }
	inline void set_blurIterations_11(int32_t value)
	{
		___blurIterations_11 = value;
	}

	inline static int32_t get_offset_of_blurType_12() { return static_cast<int32_t>(offsetof(Bloom_t2447217777, ___blurType_12)); }
	inline int32_t get_blurType_12() const { return ___blurType_12; }
	inline int32_t* get_address_of_blurType_12() { return &___blurType_12; }
	inline void set_blurType_12(int32_t value)
	{
		___blurType_12 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BLOOM_T2447217777_H
#ifndef SEPIATONE_T4118711512_H
#define SEPIATONE_T4118711512_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.SepiaTone
struct  SepiaTone_t4118711512  : public ImageEffectSingelShaderBase_t892515528
{
public:
	// System.Single Utage.SepiaTone::strength
	float ___strength_7;

public:
	inline static int32_t get_offset_of_strength_7() { return static_cast<int32_t>(offsetof(SepiaTone_t4118711512, ___strength_7)); }
	inline float get_strength_7() const { return ___strength_7; }
	inline float* get_address_of_strength_7() { return &___strength_7; }
	inline void set_strength_7(float value)
	{
		___strength_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SEPIATONE_T4118711512_H
#ifndef TWIRL_T4073632908_H
#define TWIRL_T4073632908_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.Twirl
struct  Twirl_t4073632908  : public ImageEffectSingelShaderBase_t892515528
{
public:
	// UnityEngine.Vector2 Utage.Twirl::radius
	Vector2_t2243707579  ___radius_7;
	// System.Single Utage.Twirl::angle
	float ___angle_8;
	// UnityEngine.Vector2 Utage.Twirl::center
	Vector2_t2243707579  ___center_9;

public:
	inline static int32_t get_offset_of_radius_7() { return static_cast<int32_t>(offsetof(Twirl_t4073632908, ___radius_7)); }
	inline Vector2_t2243707579  get_radius_7() const { return ___radius_7; }
	inline Vector2_t2243707579 * get_address_of_radius_7() { return &___radius_7; }
	inline void set_radius_7(Vector2_t2243707579  value)
	{
		___radius_7 = value;
	}

	inline static int32_t get_offset_of_angle_8() { return static_cast<int32_t>(offsetof(Twirl_t4073632908, ___angle_8)); }
	inline float get_angle_8() const { return ___angle_8; }
	inline float* get_address_of_angle_8() { return &___angle_8; }
	inline void set_angle_8(float value)
	{
		___angle_8 = value;
	}

	inline static int32_t get_offset_of_center_9() { return static_cast<int32_t>(offsetof(Twirl_t4073632908, ___center_9)); }
	inline Vector2_t2243707579  get_center_9() const { return ___center_9; }
	inline Vector2_t2243707579 * get_address_of_center_9() { return &___center_9; }
	inline void set_center_9(Vector2_t2243707579  value)
	{
		___center_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TWIRL_T4073632908_H
#ifndef SHADOW_T4269599528_H
#define SHADOW_T4269599528_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Shadow
struct  Shadow_t4269599528  : public BaseMeshEffect_t1728560551
{
public:
	// UnityEngine.Color UnityEngine.UI.Shadow::m_EffectColor
	Color_t2020392075  ___m_EffectColor_3;
	// UnityEngine.Vector2 UnityEngine.UI.Shadow::m_EffectDistance
	Vector2_t2243707579  ___m_EffectDistance_4;
	// System.Boolean UnityEngine.UI.Shadow::m_UseGraphicAlpha
	bool ___m_UseGraphicAlpha_5;

public:
	inline static int32_t get_offset_of_m_EffectColor_3() { return static_cast<int32_t>(offsetof(Shadow_t4269599528, ___m_EffectColor_3)); }
	inline Color_t2020392075  get_m_EffectColor_3() const { return ___m_EffectColor_3; }
	inline Color_t2020392075 * get_address_of_m_EffectColor_3() { return &___m_EffectColor_3; }
	inline void set_m_EffectColor_3(Color_t2020392075  value)
	{
		___m_EffectColor_3 = value;
	}

	inline static int32_t get_offset_of_m_EffectDistance_4() { return static_cast<int32_t>(offsetof(Shadow_t4269599528, ___m_EffectDistance_4)); }
	inline Vector2_t2243707579  get_m_EffectDistance_4() const { return ___m_EffectDistance_4; }
	inline Vector2_t2243707579 * get_address_of_m_EffectDistance_4() { return &___m_EffectDistance_4; }
	inline void set_m_EffectDistance_4(Vector2_t2243707579  value)
	{
		___m_EffectDistance_4 = value;
	}

	inline static int32_t get_offset_of_m_UseGraphicAlpha_5() { return static_cast<int32_t>(offsetof(Shadow_t4269599528, ___m_UseGraphicAlpha_5)); }
	inline bool get_m_UseGraphicAlpha_5() const { return ___m_UseGraphicAlpha_5; }
	inline bool* get_address_of_m_UseGraphicAlpha_5() { return &___m_UseGraphicAlpha_5; }
	inline void set_m_UseGraphicAlpha_5(bool value)
	{
		___m_UseGraphicAlpha_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SHADOW_T4269599528_H
#ifndef UGUIFLIP_T3801343075_H
#define UGUIFLIP_T3801343075_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.UguiFlip
struct  UguiFlip_t3801343075  : public BaseMeshEffect_t1728560551
{
public:
	// System.Boolean Utage.UguiFlip::flipX
	bool ___flipX_3;
	// System.Boolean Utage.UguiFlip::flipY
	bool ___flipY_4;

public:
	inline static int32_t get_offset_of_flipX_3() { return static_cast<int32_t>(offsetof(UguiFlip_t3801343075, ___flipX_3)); }
	inline bool get_flipX_3() const { return ___flipX_3; }
	inline bool* get_address_of_flipX_3() { return &___flipX_3; }
	inline void set_flipX_3(bool value)
	{
		___flipX_3 = value;
	}

	inline static int32_t get_offset_of_flipY_4() { return static_cast<int32_t>(offsetof(UguiFlip_t3801343075, ___flipY_4)); }
	inline bool get_flipY_4() const { return ___flipY_4; }
	inline bool* get_address_of_flipY_4() { return &___flipY_4; }
	inline void set_flipY_4(bool value)
	{
		___flipY_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UGUIFLIP_T3801343075_H
#ifndef SYSTEMUIDIALOG3BUTTON_T2800582650_H
#define SYSTEMUIDIALOG3BUTTON_T2800582650_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.SystemUiDialog3Button
struct  SystemUiDialog3Button_t2800582650  : public SystemUiDialog2Button_t2800622875
{
public:
	// UnityEngine.UI.Text Utage.SystemUiDialog3Button::button3Text
	Text_t356221433 * ___button3Text_7;
	// UnityEngine.Events.UnityEvent Utage.SystemUiDialog3Button::OnClickButton3
	UnityEvent_t408735097 * ___OnClickButton3_8;

public:
	inline static int32_t get_offset_of_button3Text_7() { return static_cast<int32_t>(offsetof(SystemUiDialog3Button_t2800582650, ___button3Text_7)); }
	inline Text_t356221433 * get_button3Text_7() const { return ___button3Text_7; }
	inline Text_t356221433 ** get_address_of_button3Text_7() { return &___button3Text_7; }
	inline void set_button3Text_7(Text_t356221433 * value)
	{
		___button3Text_7 = value;
		Il2CppCodeGenWriteBarrier((&___button3Text_7), value);
	}

	inline static int32_t get_offset_of_OnClickButton3_8() { return static_cast<int32_t>(offsetof(SystemUiDialog3Button_t2800582650, ___OnClickButton3_8)); }
	inline UnityEvent_t408735097 * get_OnClickButton3_8() const { return ___OnClickButton3_8; }
	inline UnityEvent_t408735097 ** get_address_of_OnClickButton3_8() { return &___OnClickButton3_8; }
	inline void set_OnClickButton3_8(UnityEvent_t408735097 * value)
	{
		___OnClickButton3_8 = value;
		Il2CppCodeGenWriteBarrier((&___OnClickButton3_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SYSTEMUIDIALOG3BUTTON_T2800582650_H
#ifndef FISHEYE_T1506725015_H
#define FISHEYE_T1506725015_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.FishEye
struct  FishEye_t1506725015  : public ImageEffectSingelShaderBase_t892515528
{
public:
	// System.Single Utage.FishEye::strengthX
	float ___strengthX_7;
	// System.Single Utage.FishEye::strengthY
	float ___strengthY_8;

public:
	inline static int32_t get_offset_of_strengthX_7() { return static_cast<int32_t>(offsetof(FishEye_t1506725015, ___strengthX_7)); }
	inline float get_strengthX_7() const { return ___strengthX_7; }
	inline float* get_address_of_strengthX_7() { return &___strengthX_7; }
	inline void set_strengthX_7(float value)
	{
		___strengthX_7 = value;
	}

	inline static int32_t get_offset_of_strengthY_8() { return static_cast<int32_t>(offsetof(FishEye_t1506725015, ___strengthY_8)); }
	inline float get_strengthY_8() const { return ___strengthY_8; }
	inline float* get_address_of_strengthY_8() { return &___strengthY_8; }
	inline void set_strengthY_8(float value)
	{
		___strengthY_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FISHEYE_T1506725015_H
#ifndef COLORFADE_T3112942085_H
#define COLORFADE_T3112942085_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.ColorFade
struct  ColorFade_t3112942085  : public ImageEffectSingelShaderBase_t892515528
{
public:
	// System.Single Utage.ColorFade::strength
	float ___strength_7;
	// UnityEngine.Color Utage.ColorFade::color
	Color_t2020392075  ___color_8;

public:
	inline static int32_t get_offset_of_strength_7() { return static_cast<int32_t>(offsetof(ColorFade_t3112942085, ___strength_7)); }
	inline float get_strength_7() const { return ___strength_7; }
	inline float* get_address_of_strength_7() { return &___strength_7; }
	inline void set_strength_7(float value)
	{
		___strength_7 = value;
	}

	inline static int32_t get_offset_of_color_8() { return static_cast<int32_t>(offsetof(ColorFade_t3112942085, ___color_8)); }
	inline Color_t2020392075  get_color_8() const { return ___color_8; }
	inline Color_t2020392075 * get_address_of_color_8() { return &___color_8; }
	inline void set_color_8(Color_t2020392075  value)
	{
		___color_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLORFADE_T3112942085_H
#ifndef GRAYSCALE_T423910279_H
#define GRAYSCALE_T423910279_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.Grayscale
struct  Grayscale_t423910279  : public ImageEffectSingelShaderBase_t892515528
{
public:
	// System.Single Utage.Grayscale::strength
	float ___strength_7;
	// UnityEngine.Texture Utage.Grayscale::textureRamp
	Texture_t2243626319 * ___textureRamp_8;
	// System.Single Utage.Grayscale::rampOffset
	float ___rampOffset_9;
	// UnityEngine.Texture Utage.Grayscale::tmpTextureRamp
	Texture_t2243626319 * ___tmpTextureRamp_10;

public:
	inline static int32_t get_offset_of_strength_7() { return static_cast<int32_t>(offsetof(Grayscale_t423910279, ___strength_7)); }
	inline float get_strength_7() const { return ___strength_7; }
	inline float* get_address_of_strength_7() { return &___strength_7; }
	inline void set_strength_7(float value)
	{
		___strength_7 = value;
	}

	inline static int32_t get_offset_of_textureRamp_8() { return static_cast<int32_t>(offsetof(Grayscale_t423910279, ___textureRamp_8)); }
	inline Texture_t2243626319 * get_textureRamp_8() const { return ___textureRamp_8; }
	inline Texture_t2243626319 ** get_address_of_textureRamp_8() { return &___textureRamp_8; }
	inline void set_textureRamp_8(Texture_t2243626319 * value)
	{
		___textureRamp_8 = value;
		Il2CppCodeGenWriteBarrier((&___textureRamp_8), value);
	}

	inline static int32_t get_offset_of_rampOffset_9() { return static_cast<int32_t>(offsetof(Grayscale_t423910279, ___rampOffset_9)); }
	inline float get_rampOffset_9() const { return ___rampOffset_9; }
	inline float* get_address_of_rampOffset_9() { return &___rampOffset_9; }
	inline void set_rampOffset_9(float value)
	{
		___rampOffset_9 = value;
	}

	inline static int32_t get_offset_of_tmpTextureRamp_10() { return static_cast<int32_t>(offsetof(Grayscale_t423910279, ___tmpTextureRamp_10)); }
	inline Texture_t2243626319 * get_tmpTextureRamp_10() const { return ___tmpTextureRamp_10; }
	inline Texture_t2243626319 ** get_address_of_tmpTextureRamp_10() { return &___tmpTextureRamp_10; }
	inline void set_tmpTextureRamp_10(Texture_t2243626319 * value)
	{
		___tmpTextureRamp_10 = value;
		Il2CppCodeGenWriteBarrier((&___tmpTextureRamp_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GRAYSCALE_T423910279_H
#ifndef BLUR_T1183174167_H
#define BLUR_T1183174167_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.Blur
struct  Blur_t1183174167  : public ImageEffectSingelShaderBase_t892515528
{
public:
	// System.Int32 Utage.Blur::downsample
	int32_t ___downsample_7;
	// System.Single Utage.Blur::blurSize
	float ___blurSize_8;
	// System.Int32 Utage.Blur::blurIterations
	int32_t ___blurIterations_9;
	// Utage.Blur/BlurType Utage.Blur::blurType
	int32_t ___blurType_10;

public:
	inline static int32_t get_offset_of_downsample_7() { return static_cast<int32_t>(offsetof(Blur_t1183174167, ___downsample_7)); }
	inline int32_t get_downsample_7() const { return ___downsample_7; }
	inline int32_t* get_address_of_downsample_7() { return &___downsample_7; }
	inline void set_downsample_7(int32_t value)
	{
		___downsample_7 = value;
	}

	inline static int32_t get_offset_of_blurSize_8() { return static_cast<int32_t>(offsetof(Blur_t1183174167, ___blurSize_8)); }
	inline float get_blurSize_8() const { return ___blurSize_8; }
	inline float* get_address_of_blurSize_8() { return &___blurSize_8; }
	inline void set_blurSize_8(float value)
	{
		___blurSize_8 = value;
	}

	inline static int32_t get_offset_of_blurIterations_9() { return static_cast<int32_t>(offsetof(Blur_t1183174167, ___blurIterations_9)); }
	inline int32_t get_blurIterations_9() const { return ___blurIterations_9; }
	inline int32_t* get_address_of_blurIterations_9() { return &___blurIterations_9; }
	inline void set_blurIterations_9(int32_t value)
	{
		___blurIterations_9 = value;
	}

	inline static int32_t get_offset_of_blurType_10() { return static_cast<int32_t>(offsetof(Blur_t1183174167, ___blurType_10)); }
	inline int32_t get_blurType_10() const { return ___blurType_10; }
	inline int32_t* get_address_of_blurType_10() { return &___blurType_10; }
	inline void set_blurType_10(int32_t value)
	{
		___blurType_10 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BLUR_T1183174167_H
#ifndef MOSAIC_T1809987202_H
#define MOSAIC_T1809987202_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.Mosaic
struct  Mosaic_t1809987202  : public ImageEffectSingelShaderBase_t892515528
{
public:
	// System.Single Utage.Mosaic::size
	float ___size_7;
	// Utage.LetterBoxCamera Utage.Mosaic::letterBoxCamera
	LetterBoxCamera_t3507617684 * ___letterBoxCamera_8;

public:
	inline static int32_t get_offset_of_size_7() { return static_cast<int32_t>(offsetof(Mosaic_t1809987202, ___size_7)); }
	inline float get_size_7() const { return ___size_7; }
	inline float* get_address_of_size_7() { return &___size_7; }
	inline void set_size_7(float value)
	{
		___size_7 = value;
	}

	inline static int32_t get_offset_of_letterBoxCamera_8() { return static_cast<int32_t>(offsetof(Mosaic_t1809987202, ___letterBoxCamera_8)); }
	inline LetterBoxCamera_t3507617684 * get_letterBoxCamera_8() const { return ___letterBoxCamera_8; }
	inline LetterBoxCamera_t3507617684 ** get_address_of_letterBoxCamera_8() { return &___letterBoxCamera_8; }
	inline void set_letterBoxCamera_8(LetterBoxCamera_t3507617684 * value)
	{
		___letterBoxCamera_8 = value;
		Il2CppCodeGenWriteBarrier((&___letterBoxCamera_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MOSAIC_T1809987202_H
#ifndef VORTEX_T2475109170_H
#define VORTEX_T2475109170_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.Vortex
struct  Vortex_t2475109170  : public ImageEffectSingelShaderBase_t892515528
{
public:
	// UnityEngine.Vector2 Utage.Vortex::radius
	Vector2_t2243707579  ___radius_7;
	// System.Single Utage.Vortex::angle
	float ___angle_8;
	// UnityEngine.Vector2 Utage.Vortex::center
	Vector2_t2243707579  ___center_9;

public:
	inline static int32_t get_offset_of_radius_7() { return static_cast<int32_t>(offsetof(Vortex_t2475109170, ___radius_7)); }
	inline Vector2_t2243707579  get_radius_7() const { return ___radius_7; }
	inline Vector2_t2243707579 * get_address_of_radius_7() { return &___radius_7; }
	inline void set_radius_7(Vector2_t2243707579  value)
	{
		___radius_7 = value;
	}

	inline static int32_t get_offset_of_angle_8() { return static_cast<int32_t>(offsetof(Vortex_t2475109170, ___angle_8)); }
	inline float get_angle_8() const { return ___angle_8; }
	inline float* get_address_of_angle_8() { return &___angle_8; }
	inline void set_angle_8(float value)
	{
		___angle_8 = value;
	}

	inline static int32_t get_offset_of_center_9() { return static_cast<int32_t>(offsetof(Vortex_t2475109170, ___center_9)); }
	inline Vector2_t2243707579  get_center_9() const { return ___center_9; }
	inline Vector2_t2243707579 * get_address_of_center_9() { return &___center_9; }
	inline void set_center_9(Vector2_t2243707579  value)
	{
		___center_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VORTEX_T2475109170_H
#ifndef MOTIONBLUR_T2547438417_H
#define MOTIONBLUR_T2547438417_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.MotionBlur
struct  MotionBlur_t2547438417  : public ImageEffectSingelShaderBase_t892515528
{
public:
	// System.Single Utage.MotionBlur::blurAmount
	float ___blurAmount_7;
	// System.Boolean Utage.MotionBlur::extraBlur
	bool ___extraBlur_8;
	// UnityEngine.RenderTexture Utage.MotionBlur::accumTexture
	RenderTexture_t2666733923 * ___accumTexture_9;

public:
	inline static int32_t get_offset_of_blurAmount_7() { return static_cast<int32_t>(offsetof(MotionBlur_t2547438417, ___blurAmount_7)); }
	inline float get_blurAmount_7() const { return ___blurAmount_7; }
	inline float* get_address_of_blurAmount_7() { return &___blurAmount_7; }
	inline void set_blurAmount_7(float value)
	{
		___blurAmount_7 = value;
	}

	inline static int32_t get_offset_of_extraBlur_8() { return static_cast<int32_t>(offsetof(MotionBlur_t2547438417, ___extraBlur_8)); }
	inline bool get_extraBlur_8() const { return ___extraBlur_8; }
	inline bool* get_address_of_extraBlur_8() { return &___extraBlur_8; }
	inline void set_extraBlur_8(bool value)
	{
		___extraBlur_8 = value;
	}

	inline static int32_t get_offset_of_accumTexture_9() { return static_cast<int32_t>(offsetof(MotionBlur_t2547438417, ___accumTexture_9)); }
	inline RenderTexture_t2666733923 * get_accumTexture_9() const { return ___accumTexture_9; }
	inline RenderTexture_t2666733923 ** get_address_of_accumTexture_9() { return &___accumTexture_9; }
	inline void set_accumTexture_9(RenderTexture_t2666733923 * value)
	{
		___accumTexture_9 = value;
		Il2CppCodeGenWriteBarrier((&___accumTexture_9), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MOTIONBLUR_T2547438417_H
#ifndef NEGAPOSI_T3335502518_H
#define NEGAPOSI_T3335502518_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.NegaPosi
struct  NegaPosi_t3335502518  : public ImageEffectSingelShaderBase_t892515528
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NEGAPOSI_T3335502518_H
#ifndef SCREENOVERLAY_T1155121678_H
#define SCREENOVERLAY_T1155121678_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.ScreenOverlay
struct  ScreenOverlay_t1155121678  : public ImageEffectSingelShaderBase_t892515528
{
public:
	// Utage.ScreenOverlay/OverlayBlendMode Utage.ScreenOverlay::blendMode
	int32_t ___blendMode_7;
	// System.Single Utage.ScreenOverlay::intensity
	float ___intensity_8;
	// UnityEngine.Texture2D Utage.ScreenOverlay::texture
	Texture2D_t3542995729 * ___texture_9;

public:
	inline static int32_t get_offset_of_blendMode_7() { return static_cast<int32_t>(offsetof(ScreenOverlay_t1155121678, ___blendMode_7)); }
	inline int32_t get_blendMode_7() const { return ___blendMode_7; }
	inline int32_t* get_address_of_blendMode_7() { return &___blendMode_7; }
	inline void set_blendMode_7(int32_t value)
	{
		___blendMode_7 = value;
	}

	inline static int32_t get_offset_of_intensity_8() { return static_cast<int32_t>(offsetof(ScreenOverlay_t1155121678, ___intensity_8)); }
	inline float get_intensity_8() const { return ___intensity_8; }
	inline float* get_address_of_intensity_8() { return &___intensity_8; }
	inline void set_intensity_8(float value)
	{
		___intensity_8 = value;
	}

	inline static int32_t get_offset_of_texture_9() { return static_cast<int32_t>(offsetof(ScreenOverlay_t1155121678, ___texture_9)); }
	inline Texture2D_t3542995729 * get_texture_9() const { return ___texture_9; }
	inline Texture2D_t3542995729 ** get_address_of_texture_9() { return &___texture_9; }
	inline void set_texture_9(Texture2D_t3542995729 * value)
	{
		___texture_9 = value;
		Il2CppCodeGenWriteBarrier((&___texture_9), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SCREENOVERLAY_T1155121678_H
#ifndef OUTLINE_T1417504278_H
#define OUTLINE_T1417504278_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Outline
struct  Outline_t1417504278  : public Shadow_t4269599528
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OUTLINE_T1417504278_H
#ifndef UGUIRICHOUTLINE_T1106552936_H
#define UGUIRICHOUTLINE_T1106552936_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.UguiRichOutline
struct  UguiRichOutline_t1106552936  : public Outline_t1417504278
{
public:
	// System.Int32 Utage.UguiRichOutline::copyCount
	int32_t ___copyCount_7;

public:
	inline static int32_t get_offset_of_copyCount_7() { return static_cast<int32_t>(offsetof(UguiRichOutline_t1106552936, ___copyCount_7)); }
	inline int32_t get_copyCount_7() const { return ___copyCount_7; }
	inline int32_t* get_address_of_copyCount_7() { return &___copyCount_7; }
	inline void set_copyCount_7(int32_t value)
	{
		___copyCount_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UGUIRICHOUTLINE_T1106552936_H





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2700 = { sizeof (U3CLoadAssetBundleAsyncU3Ec__AnonStorey4_t2077293016), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2700[3] = 
{
	U3CLoadAssetBundleAsyncU3Ec__AnonStorey4_t2077293016::get_offset_of_onComplete_0(),
	U3CLoadAssetBundleAsyncU3Ec__AnonStorey4_t2077293016::get_offset_of_onFailed_1(),
	U3CLoadAssetBundleAsyncU3Ec__AnonStorey4_t2077293016::get_offset_of_U24this_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2701 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2701[4] = 
{
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2702 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2702[11] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2703 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2703[3] = 
{
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2704 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2704[10] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2705 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2705[3] = 
{
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2706 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2707 = { sizeof (ImageEffectBase_t432402543), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2707[2] = 
{
	ImageEffectBase_t432402543::get_offset_of_createdMaterials_2(),
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2708 = { sizeof (ImageEffectSingelShaderBase_t892515528), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2708[3] = 
{
	ImageEffectSingelShaderBase_t892515528::get_offset_of_shader_4(),
	ImageEffectSingelShaderBase_t892515528::get_offset_of_U3CMaterialU3Ek__BackingField_5(),
	ImageEffectSingelShaderBase_t892515528::get_offset_of_tmpShader_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2709 = { sizeof (ImageEffectType_t2892257590)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2709[14] = 
{
	ImageEffectType_t2892257590::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2710 = { sizeof (ImageEffectUtil_t378442258), -1, sizeof(ImageEffectUtil_t378442258_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2710[1] = 
{
	ImageEffectUtil_t378442258_StaticFields::get_offset_of_patterns_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2711 = { sizeof (ImageEffectPattern_t3156031785), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2711[3] = 
{
	ImageEffectPattern_t3156031785::get_offset_of_type_0(),
	ImageEffectPattern_t3156031785::get_offset_of_componentType_1(),
	ImageEffectPattern_t3156031785::get_offset_of_shaders_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2712 = { sizeof (U3CTryParseU3Ec__AnonStorey0_t2065039102), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2712[1] = 
{
	U3CTryParseU3Ec__AnonStorey0_t2065039102::get_offset_of_type_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2713 = { sizeof (U3CToImageEffectTypeU3Ec__AnonStorey1_t491534822), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2713[1] = 
{
	U3CToImageEffectTypeU3Ec__AnonStorey1_t491534822::get_offset_of_ComponentType_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2714 = { sizeof (Bloom_t2447217777), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2714[6] = 
{
	Bloom_t2447217777::get_offset_of_threshold_7(),
	Bloom_t2447217777::get_offset_of_intensity_8(),
	Bloom_t2447217777::get_offset_of_blurSize_9(),
	Bloom_t2447217777::get_offset_of_resolution_10(),
	Bloom_t2447217777::get_offset_of_blurIterations_11(),
	Bloom_t2447217777::get_offset_of_blurType_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2715 = { sizeof (Resolution_t93968648)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2715[3] = 
{
	Resolution_t93968648::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2716 = { sizeof (BlurType_t3344687199)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2716[3] = 
{
	BlurType_t3344687199::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2717 = { sizeof (Blur_t1183174167), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2717[4] = 
{
	Blur_t1183174167::get_offset_of_downsample_7(),
	Blur_t1183174167::get_offset_of_blurSize_8(),
	Blur_t1183174167::get_offset_of_blurIterations_9(),
	Blur_t1183174167::get_offset_of_blurType_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2718 = { sizeof (BlurType_t3188843521)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2718[3] = 
{
	BlurType_t3188843521::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2719 = { sizeof (ColorFade_t3112942085), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2719[2] = 
{
	ColorFade_t3112942085::get_offset_of_strength_7(),
	ColorFade_t3112942085::get_offset_of_color_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2720 = { sizeof (FishEye_t1506725015), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2720[2] = 
{
	FishEye_t1506725015::get_offset_of_strengthX_7(),
	FishEye_t1506725015::get_offset_of_strengthY_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2721 = { sizeof (Grayscale_t423910279), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2721[4] = 
{
	Grayscale_t423910279::get_offset_of_strength_7(),
	Grayscale_t423910279::get_offset_of_textureRamp_8(),
	Grayscale_t423910279::get_offset_of_rampOffset_9(),
	Grayscale_t423910279::get_offset_of_tmpTextureRamp_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2722 = { sizeof (Mosaic_t1809987202), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2722[2] = 
{
	Mosaic_t1809987202::get_offset_of_size_7(),
	Mosaic_t1809987202::get_offset_of_letterBoxCamera_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2723 = { sizeof (MotionBlur_t2547438417), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2723[3] = 
{
	MotionBlur_t2547438417::get_offset_of_blurAmount_7(),
	MotionBlur_t2547438417::get_offset_of_extraBlur_8(),
	MotionBlur_t2547438417::get_offset_of_accumTexture_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2724 = { sizeof (NegaPosi_t3335502518), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2725 = { sizeof (ScreenOverlay_t1155121678), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2725[3] = 
{
	ScreenOverlay_t1155121678::get_offset_of_blendMode_7(),
	ScreenOverlay_t1155121678::get_offset_of_intensity_8(),
	ScreenOverlay_t1155121678::get_offset_of_texture_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2726 = { sizeof (OverlayBlendMode_t2265955091)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2726[6] = 
{
	OverlayBlendMode_t2265955091::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2727 = { sizeof (SepiaTone_t4118711512), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2727[1] = 
{
	SepiaTone_t4118711512::get_offset_of_strength_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2728 = { sizeof (Twirl_t4073632908), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2728[3] = 
{
	Twirl_t4073632908::get_offset_of_radius_7(),
	Twirl_t4073632908::get_offset_of_angle_8(),
	Twirl_t4073632908::get_offset_of_center_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2729 = { sizeof (Vortex_t2475109170), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2729[3] = 
{
	Vortex_t2475109170::get_offset_of_radius_7(),
	Vortex_t2475109170::get_offset_of_angle_8(),
	Vortex_t2475109170::get_offset_of_center_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2730 = { sizeof (iTweenType_t3322595176)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2730[23] = 
{
	iTweenType_t3322595176::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2731 = { sizeof (iTweenData_t1904063128), -1, sizeof(iTweenData_t1904063128_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2731[28] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	iTweenData_t1904063128::get_offset_of_type_15(),
	iTweenData_t1904063128::get_offset_of_loopType_16(),
	iTweenData_t1904063128::get_offset_of_loopCount_17(),
	iTweenData_t1904063128::get_offset_of_hashObjects_18(),
	iTweenData_t1904063128::get_offset_of_errorMsg_19(),
	iTweenData_t1904063128::get_offset_of_strType_20(),
	iTweenData_t1904063128::get_offset_of_strArg_21(),
	iTweenData_t1904063128::get_offset_of_strEaseType_22(),
	iTweenData_t1904063128::get_offset_of_strLoopType_23(),
	iTweenData_t1904063128_StaticFields::get_offset_of_CallbackGetValue_24(),
	iTweenData_t1904063128::get_offset_of_isDynamic_25(),
	iTweenData_t1904063128_StaticFields::get_offset_of_ArgTbl_26(),
	iTweenData_t1904063128_StaticFields::get_offset_of_U3CU3Ef__switchU24map9_27(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2732 = { sizeof (EaseType_t1880589884)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2732[30] = 
{
	EaseType_t1880589884::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2733 = { sizeof (Easing_t1940169557), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2734 = { sizeof (ShaderManager_t3833453370), -1, sizeof(ShaderManager_t3833453370_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2734[14] = 
{
	ShaderManager_t3833453370_StaticFields::get_offset_of_ColorFade_0(),
	ShaderManager_t3833453370_StaticFields::get_offset_of_BloomName_1(),
	ShaderManager_t3833453370_StaticFields::get_offset_of_BlurName_2(),
	ShaderManager_t3833453370_StaticFields::get_offset_of_MosaicName_3(),
	ShaderManager_t3833453370_StaticFields::get_offset_of_ColorCorrectionRampName_4(),
	ShaderManager_t3833453370_StaticFields::get_offset_of_GrayScaleName_5(),
	ShaderManager_t3833453370_StaticFields::get_offset_of_MotionBlurName_6(),
	ShaderManager_t3833453370_StaticFields::get_offset_of_NoiseAndGrainName_7(),
	ShaderManager_t3833453370_StaticFields::get_offset_of_BlendModesOverlayName_8(),
	ShaderManager_t3833453370_StaticFields::get_offset_of_SepiatoneName_9(),
	ShaderManager_t3833453370_StaticFields::get_offset_of_NegaPosiName_10(),
	ShaderManager_t3833453370_StaticFields::get_offset_of_FisheyeName_11(),
	ShaderManager_t3833453370_StaticFields::get_offset_of_TwirlName_12(),
	ShaderManager_t3833453370_StaticFields::get_offset_of_VortexName_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2735 = { sizeof (SoundAudio_t3759608863), -1, sizeof(SoundAudio_t3759608863_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2735[12] = 
{
	SoundAudio_t3759608863::get_offset_of_status_2(),
	SoundAudio_t3759608863::get_offset_of_U3CAudioSourceU3Ek__BackingField_3(),
	SoundAudio_t3759608863::get_offset_of_U3CAudioSourceForIntroLoopU3Ek__BackingField_4(),
	SoundAudio_t3759608863::get_offset_of_U3CAudio0U3Ek__BackingField_5(),
	SoundAudio_t3759608863::get_offset_of_U3CAudio1U3Ek__BackingField_6(),
	SoundAudio_t3759608863::get_offset_of_U3CDataU3Ek__BackingField_7(),
	SoundAudio_t3759608863::get_offset_of_U3CPlayerU3Ek__BackingField_8(),
	SoundAudio_t3759608863::get_offset_of_U3CIsLoadingU3Ek__BackingField_9(),
	SoundAudio_t3759608863::get_offset_of_fadeValue_10(),
	0,
	0,
	SoundAudio_t3759608863_StaticFields::get_offset_of_waveData_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2736 = { sizeof (SoundStreamStatus_t3426717699)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2736[5] = 
{
	SoundStreamStatus_t3426717699::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2737 = { sizeof (U3CCoWaitDelayU3Ec__Iterator0_t1369630889), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2737[6] = 
{
	U3CCoWaitDelayU3Ec__Iterator0_t1369630889::get_offset_of_delay_0(),
	U3CCoWaitDelayU3Ec__Iterator0_t1369630889::get_offset_of_fadeInTime_1(),
	U3CCoWaitDelayU3Ec__Iterator0_t1369630889::get_offset_of_U24this_2(),
	U3CCoWaitDelayU3Ec__Iterator0_t1369630889::get_offset_of_U24current_3(),
	U3CCoWaitDelayU3Ec__Iterator0_t1369630889::get_offset_of_U24disposing_4(),
	U3CCoWaitDelayU3Ec__Iterator0_t1369630889::get_offset_of_U24PC_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2738 = { sizeof (SoundAudioPlayer_t3829317720), -1, sizeof(SoundAudioPlayer_t3829317720_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2738[8] = 
{
	SoundAudioPlayer_t3829317720::get_offset_of_U3CLabelU3Ek__BackingField_2(),
	SoundAudioPlayer_t3829317720::get_offset_of_U3CGroupU3Ek__BackingField_3(),
	SoundAudioPlayer_t3829317720::get_offset_of_U3CAudioU3Ek__BackingField_4(),
	SoundAudioPlayer_t3829317720::get_offset_of_U3CFadeOutAudioU3Ek__BackingField_5(),
	SoundAudioPlayer_t3829317720::get_offset_of_U3CAudioListU3Ek__BackingField_6(),
	SoundAudioPlayer_t3829317720::get_offset_of_U3CCurrentFrameAudioListU3Ek__BackingField_7(),
	0,
	SoundAudioPlayer_t3829317720_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2739 = { sizeof (U3CReadU3Ec__AnonStorey0_t1030251424), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2739[2] = 
{
	U3CReadU3Ec__AnonStorey0_t1030251424::get_offset_of_audioName_0(),
	U3CReadU3Ec__AnonStorey0_t1030251424::get_offset_of_U24this_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2740 = { sizeof (SoundData_t3887839289), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2740[9] = 
{
	SoundData_t3887839289::get_offset_of_clip_0(),
	SoundData_t3887839289::get_offset_of_U3CFileU3Ek__BackingField_1(),
	SoundData_t3887839289::get_offset_of_U3CPlayModeU3Ek__BackingField_2(),
	SoundData_t3887839289::get_offset_of_U3CIsLoopU3Ek__BackingField_3(),
	SoundData_t3887839289::get_offset_of_U3CPlayVolumeU3Ek__BackingField_4(),
	SoundData_t3887839289::get_offset_of_U3CResourceVolumeU3Ek__BackingField_5(),
	SoundData_t3887839289::get_offset_of_U3CIntroTimeU3Ek__BackingField_6(),
	SoundData_t3887839289::get_offset_of_U3CTagU3Ek__BackingField_7(),
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2741 = { sizeof (SoundGroup_t2016089996), -1, sizeof(SoundGroup_t2016089996_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2741[12] = 
{
	SoundGroup_t2016089996::get_offset_of_U3CSoundManagerSystemU3Ek__BackingField_2(),
	SoundGroup_t2016089996::get_offset_of_playerList_3(),
	SoundGroup_t2016089996::get_offset_of_multiPlay_4(),
	SoundGroup_t2016089996::get_offset_of_autoDestoryPlayer_5(),
	SoundGroup_t2016089996::get_offset_of_masterVolume_6(),
	SoundGroup_t2016089996::get_offset_of_groupVolume_7(),
	SoundGroup_t2016089996::get_offset_of_duckGroups_8(),
	SoundGroup_t2016089996::get_offset_of_U3CDuckVolumeU3Ek__BackingField_9(),
	SoundGroup_t2016089996::get_offset_of_duckVelocity_10(),
	0,
	0,
	SoundGroup_t2016089996_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2742 = { sizeof (SoundManager_t1265124084), -1, sizeof(SoundManager_t1265124084_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2742[17] = 
{
	0,
	0,
	0,
	0,
	SoundManager_t1265124084_StaticFields::get_offset_of_instance_6(),
	SoundManager_t1265124084::get_offset_of_masterVolume_7(),
	SoundManager_t1265124084::get_offset_of_taggedMasterVolumes_8(),
	0,
	SoundManager_t1265124084::get_offset_of_duckVolume_10(),
	SoundManager_t1265124084::get_offset_of_duckFadeTime_11(),
	SoundManager_t1265124084::get_offset_of_defaultFadeTime_12(),
	SoundManager_t1265124084::get_offset_of_defaultVoiceFadeTime_13(),
	SoundManager_t1265124084::get_offset_of_defaultVolume_14(),
	SoundManager_t1265124084::get_offset_of_U3CCurrentVoiceCharacterLabelU3Ek__BackingField_15(),
	SoundManager_t1265124084::get_offset_of_voicePlayMode_16(),
	SoundManager_t1265124084::get_offset_of_onCreateSoundSystem_17(),
	SoundManager_t1265124084::get_offset_of_system_18(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2743 = { sizeof (TaggedMasterVolume_t2665287385), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2743[2] = 
{
	TaggedMasterVolume_t2665287385::get_offset_of_tag_0(),
	TaggedMasterVolume_t2665287385::get_offset_of_volume_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2744 = { sizeof (SoundManagerEvent_t3645924073), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2745 = { sizeof (U3CSetTaggedMasterVolumeU3Ec__AnonStorey0_t1588545342), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2745[1] = 
{
	U3CSetTaggedMasterVolumeU3Ec__AnonStorey0_t1588545342::get_offset_of_tag_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2746 = { sizeof (U3CPlayVoiceU3Ec__AnonStorey1_t1527014591), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2746[1] = 
{
	U3CPlayVoiceU3Ec__AnonStorey1_t1527014591::get_offset_of_characterLabel_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2747 = { sizeof (SoundManagerSystem_t1680680183), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2747[4] = 
{
	0,
	SoundManagerSystem_t1680680183::get_offset_of_groups_1(),
	SoundManagerSystem_t1680680183::get_offset_of_U3CSoundManagerU3Ek__BackingField_2(),
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2748 = { sizeof (SoundPlayMode_t4141625668)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2748[4] = 
{
	SoundPlayMode_t4141625668::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2749 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2750 = { sizeof (CsvType_t1603973182)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2750[3] = 
{
	CsvType_t1603973182::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2751 = { sizeof (StringGrid_t1872153679), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2751[7] = 
{
	StringGrid_t1872153679::get_offset_of_rows_0(),
	StringGrid_t1872153679::get_offset_of_name_1(),
	StringGrid_t1872153679::get_offset_of_sheetName_2(),
	StringGrid_t1872153679::get_offset_of_type_3(),
	StringGrid_t1872153679::get_offset_of_textLength_4(),
	StringGrid_t1872153679::get_offset_of_columnIndexTbl_5(),
	StringGrid_t1872153679::get_offset_of_headerRow_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2752 = { sizeof (StringGridDictionaryKeyValue_t2220015563), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2752[1] = 
{
	StringGridDictionaryKeyValue_t2220015563::get_offset_of_grid_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2753 = { sizeof (StringGridDictionary_t1396150451), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2754 = { sizeof (StringGridRow_t4193237197), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2754[7] = 
{
	StringGridRow_t4193237197::get_offset_of_grid_0(),
	StringGridRow_t4193237197::get_offset_of_rowIndex_1(),
	StringGridRow_t4193237197::get_offset_of_debugIndex_2(),
	StringGridRow_t4193237197::get_offset_of_strings_3(),
	StringGridRow_t4193237197::get_offset_of_isEmpty_4(),
	StringGridRow_t4193237197::get_offset_of_isCommentOut_5(),
	StringGridRow_t4193237197::get_offset_of_debugInfo_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2755 = { sizeof (DebugPauseEditor_t649960972), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2755[3] = 
{
	DebugPauseEditor_t649960972::get_offset_of_isPauseOnMouseDown_2(),
	DebugPauseEditor_t649960972::get_offset_of_isPauseOnMouseUp_3(),
	DebugPauseEditor_t649960972::get_offset_of_timeScale_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2756 = { sizeof (DebugPrint_t1518728472), -1, sizeof(DebugPrint_t1518728472_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2756[12] = 
{
	DebugPrint_t1518728472_StaticFields::get_offset_of_instance_2(),
	DebugPrint_t1518728472::get_offset_of_logList_3(),
	DebugPrint_t1518728472::get_offset_of_oldTime_4(),
	DebugPrint_t1518728472::get_offset_of_frame_5(),
	DebugPrint_t1518728472::get_offset_of_frameRate_6(),
	0,
	DebugPrint_t1518728472::get_offset_of_memSizeSystem_8(),
	DebugPrint_t1518728472::get_offset_of_memSizeGraphic_9(),
	DebugPrint_t1518728472::get_offset_of_memSizeUsedHeap_10(),
	DebugPrint_t1518728472::get_offset_of_memSizeGC_11(),
	DebugPrint_t1518728472::get_offset_of_memSizeMonoHeap_12(),
	DebugPrint_t1518728472::get_offset_of_memSizeMonoUsedHeap_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2757 = { sizeof (IndicatorIcon_t1982044218), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2757[7] = 
{
	IndicatorIcon_t1982044218::get_offset_of_icon_2(),
	IndicatorIcon_t1982044218::get_offset_of_animTime_3(),
	IndicatorIcon_t1982044218::get_offset_of_animRotZ_4(),
	IndicatorIcon_t1982044218::get_offset_of_isDeviceIndicator_5(),
	IndicatorIcon_t1982044218::get_offset_of_isStarting_6(),
	IndicatorIcon_t1982044218::get_offset_of_rotZ_7(),
	IndicatorIcon_t1982044218::get_offset_of_objList_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2758 = { sizeof (SystemUi_t2405527951), -1, sizeof(SystemUi_t2405527951_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2758[7] = 
{
	SystemUi_t2405527951_StaticFields::get_offset_of_instance_2(),
	SystemUi_t2405527951::get_offset_of_dialogGameExit_3(),
	SystemUi_t2405527951::get_offset_of_dialog1Button_4(),
	SystemUi_t2405527951::get_offset_of_dialog2Button_5(),
	SystemUi_t2405527951::get_offset_of_dialog3Button_6(),
	SystemUi_t2405527951::get_offset_of_indicator_7(),
	SystemUi_t2405527951::get_offset_of_isEnableInputEscape_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2759 = { sizeof (U3CCoGameExitU3Ec__Iterator0_t3048488661), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2759[3] = 
{
	U3CCoGameExitU3Ec__Iterator0_t3048488661::get_offset_of_U24current_0(),
	U3CCoGameExitU3Ec__Iterator0_t3048488661::get_offset_of_U24disposing_1(),
	U3CCoGameExitU3Ec__Iterator0_t3048488661::get_offset_of_U24PC_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2760 = { sizeof (SystemUiDebugMenu_t1631343349), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2760[12] = 
{
	SystemUiDebugMenu_t1631343349::get_offset_of_buttonRoot_2(),
	SystemUiDebugMenu_t1631343349::get_offset_of_buttonViewRoot_3(),
	SystemUiDebugMenu_t1631343349::get_offset_of_buttonText_4(),
	SystemUiDebugMenu_t1631343349::get_offset_of_debugInfo_5(),
	SystemUiDebugMenu_t1631343349::get_offset_of_debugInfoText_6(),
	SystemUiDebugMenu_t1631343349::get_offset_of_debugLog_7(),
	SystemUiDebugMenu_t1631343349::get_offset_of_debugLogText_8(),
	SystemUiDebugMenu_t1631343349::get_offset_of_autoUpdateLogText_9(),
	SystemUiDebugMenu_t1631343349::get_offset_of_rootDebugMenu_10(),
	SystemUiDebugMenu_t1631343349::get_offset_of_targetDeleteAllSaveData_11(),
	SystemUiDebugMenu_t1631343349::get_offset_of_enabeReleaseBuild_12(),
	SystemUiDebugMenu_t1631343349::get_offset_of_currentMode_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2761 = { sizeof (Mode_t75426569)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2761[6] = 
{
	Mode_t75426569::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2762 = { sizeof (U3CCoUpdateInfoU3Ec__Iterator0_t3399782156), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2762[4] = 
{
	U3CCoUpdateInfoU3Ec__Iterator0_t3399782156::get_offset_of_U24this_0(),
	U3CCoUpdateInfoU3Ec__Iterator0_t3399782156::get_offset_of_U24current_1(),
	U3CCoUpdateInfoU3Ec__Iterator0_t3399782156::get_offset_of_U24disposing_2(),
	U3CCoUpdateInfoU3Ec__Iterator0_t3399782156::get_offset_of_U24PC_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2763 = { sizeof (U3CCoUpdateLogU3Ec__Iterator1_t3573605669), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2763[4] = 
{
	U3CCoUpdateLogU3Ec__Iterator1_t3573605669::get_offset_of_U24this_0(),
	U3CCoUpdateLogU3Ec__Iterator1_t3573605669::get_offset_of_U24current_1(),
	U3CCoUpdateLogU3Ec__Iterator1_t3573605669::get_offset_of_U24disposing_2(),
	U3CCoUpdateLogU3Ec__Iterator1_t3573605669::get_offset_of_U24PC_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2764 = { sizeof (U3CCoUpdateMenuU3Ec__Iterator2_t1895229701), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2764[4] = 
{
	U3CCoUpdateMenuU3Ec__Iterator2_t1895229701::get_offset_of_U24this_0(),
	U3CCoUpdateMenuU3Ec__Iterator2_t1895229701::get_offset_of_U24current_1(),
	U3CCoUpdateMenuU3Ec__Iterator2_t1895229701::get_offset_of_U24disposing_2(),
	U3CCoUpdateMenuU3Ec__Iterator2_t1895229701::get_offset_of_U24PC_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2765 = { sizeof (SystemUiDialog1Button_t2800515000), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2765[3] = 
{
	SystemUiDialog1Button_t2800515000::get_offset_of_titleText_2(),
	SystemUiDialog1Button_t2800515000::get_offset_of_button1Text_3(),
	SystemUiDialog1Button_t2800515000::get_offset_of_OnClickButton1_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2766 = { sizeof (SystemUiDialog2Button_t2800622875), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2766[2] = 
{
	SystemUiDialog2Button_t2800622875::get_offset_of_button2Text_5(),
	SystemUiDialog2Button_t2800622875::get_offset_of_OnClickButton2_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2767 = { sizeof (SystemUiDialog3Button_t2800582650), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2767[2] = 
{
	SystemUiDialog3Button_t2800582650::get_offset_of_button3Text_7(),
	SystemUiDialog3Button_t2800582650::get_offset_of_OnClickButton3_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2768 = { sizeof (CharData_t4178071270), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2768[4] = 
{
	0,
	CharData_t4178071270::get_offset_of_c_1(),
	CharData_t4178071270::get_offset_of_unityRitchTextIndex_2(),
	CharData_t4178071270::get_offset_of_customInfo_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2769 = { sizeof (HitEventType_t2243784412)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2769[4] = 
{
	HitEventType_t2243784412::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2770 = { sizeof (CustomCharaInfo_t997213109), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2770[34] = 
{
	CustomCharaInfo_t997213109::get_offset_of_U3CIsColorU3Ek__BackingField_0(),
	CustomCharaInfo_t997213109::get_offset_of_color_1(),
	CustomCharaInfo_t997213109::get_offset_of_colorStr_2(),
	CustomCharaInfo_t997213109::get_offset_of_U3CIsSizeU3Ek__BackingField_3(),
	CustomCharaInfo_t997213109::get_offset_of_size_4(),
	CustomCharaInfo_t997213109::get_offset_of_U3CIsBoldU3Ek__BackingField_5(),
	CustomCharaInfo_t997213109::get_offset_of_U3CIsItalicU3Ek__BackingField_6(),
	CustomCharaInfo_t997213109::get_offset_of_U3CIsDoubleWordU3Ek__BackingField_7(),
	CustomCharaInfo_t997213109::get_offset_of_U3CIsRubyTopU3Ek__BackingField_8(),
	CustomCharaInfo_t997213109::get_offset_of_U3CIsRubyU3Ek__BackingField_9(),
	CustomCharaInfo_t997213109::get_offset_of_rubyStr_10(),
	CustomCharaInfo_t997213109::get_offset_of_U3CIsEmphasisMarkU3Ek__BackingField_11(),
	CustomCharaInfo_t997213109::get_offset_of_U3CIsSuperScriptU3Ek__BackingField_12(),
	CustomCharaInfo_t997213109::get_offset_of_U3CIsSubScriptU3Ek__BackingField_13(),
	CustomCharaInfo_t997213109::get_offset_of_U3CIsUnderLineTopU3Ek__BackingField_14(),
	CustomCharaInfo_t997213109::get_offset_of_U3CIsUnderLineU3Ek__BackingField_15(),
	CustomCharaInfo_t997213109::get_offset_of_U3CIsStrikeTopU3Ek__BackingField_16(),
	CustomCharaInfo_t997213109::get_offset_of_U3CIsStrikeU3Ek__BackingField_17(),
	CustomCharaInfo_t997213109::get_offset_of_U3CIsGroupTopU3Ek__BackingField_18(),
	CustomCharaInfo_t997213109::get_offset_of_U3CIsGroupU3Ek__BackingField_19(),
	CustomCharaInfo_t997213109::get_offset_of_U3CIsEmojiU3Ek__BackingField_20(),
	CustomCharaInfo_t997213109::get_offset_of_U3CEmojiKeyU3Ek__BackingField_21(),
	CustomCharaInfo_t997213109::get_offset_of_U3CIsDashU3Ek__BackingField_22(),
	CustomCharaInfo_t997213109::get_offset_of_U3CDashSizeU3Ek__BackingField_23(),
	CustomCharaInfo_t997213109::get_offset_of_U3CIsSpaceU3Ek__BackingField_24(),
	CustomCharaInfo_t997213109::get_offset_of_U3CSpaceSizeU3Ek__BackingField_25(),
	CustomCharaInfo_t997213109::get_offset_of_U3CIsSpeedU3Ek__BackingField_26(),
	CustomCharaInfo_t997213109::get_offset_of_speed_27(),
	CustomCharaInfo_t997213109::get_offset_of_U3CIsIntervalU3Ek__BackingField_28(),
	CustomCharaInfo_t997213109::get_offset_of_Interval_29(),
	CustomCharaInfo_t997213109::get_offset_of_U3CIsHitEventTopU3Ek__BackingField_30(),
	CustomCharaInfo_t997213109::get_offset_of_U3CIsHitEventU3Ek__BackingField_31(),
	CustomCharaInfo_t997213109::get_offset_of_U3CHitEventArgU3Ek__BackingField_32(),
	CustomCharaInfo_t997213109::get_offset_of_U3CHitEventTypeU3Ek__BackingField_33(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2771 = { sizeof (TextData_t603454315), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2771[8] = 
{
	TextData_t603454315::get_offset_of_U3CParsedTextU3Ek__BackingField_0(),
	TextData_t603454315::get_offset_of_U3CContainsSpeedTagU3Ek__BackingField_1(),
	TextData_t603454315::get_offset_of_U3CIsNoWaitAllU3Ek__BackingField_2(),
	TextData_t603454315::get_offset_of_unityRitchText_3(),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2772 = { sizeof (TextParser_t4249442530), -1, sizeof(TextParser_t4249442530_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2772[12] = 
{
	0,
	0,
	0,
	TextParser_t4249442530::get_offset_of_charList_3(),
	TextParser_t4249442530_StaticFields::get_offset_of_CallbackCalcExpression_4(),
	TextParser_t4249442530::get_offset_of_errorMsg_5(),
	TextParser_t4249442530::get_offset_of_originalText_6(),
	TextParser_t4249442530::get_offset_of_noneMetaString_7(),
	TextParser_t4249442530::get_offset_of_currentTextIndex_8(),
	TextParser_t4249442530::get_offset_of_customInfo_9(),
	TextParser_t4249442530::get_offset_of_isParseParamOnly_10(),
	TextParser_t4249442530_StaticFields::get_offset_of_U3CU3Ef__switchU24mapA_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2773 = { sizeof (TimerEvent_t3049894269), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2774 = { sizeof (Timer_t2904185433), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2774[11] = 
{
	Timer_t2904185433::get_offset_of_duration_2(),
	Timer_t2904185433::get_offset_of_delay_3(),
	Timer_t2904185433::get_offset_of_time_4(),
	Timer_t2904185433::get_offset_of_time01_5(),
	Timer_t2904185433::get_offset_of_onStart_6(),
	Timer_t2904185433::get_offset_of_onUpdate_7(),
	Timer_t2904185433::get_offset_of_onComplete_8(),
	Timer_t2904185433::get_offset_of_autoDestroy_9(),
	Timer_t2904185433::get_offset_of_autoStart_10(),
	Timer_t2904185433::get_offset_of_callbackUpdate_11(),
	Timer_t2904185433::get_offset_of_callbackComplete_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2775 = { sizeof (U3CCoTimerU3Ec__Iterator0_t3483898792), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2775[7] = 
{
	U3CCoTimerU3Ec__Iterator0_t3483898792::get_offset_of_duration_0(),
	U3CCoTimerU3Ec__Iterator0_t3483898792::get_offset_of_delay_1(),
	U3CCoTimerU3Ec__Iterator0_t3483898792::get_offset_of_U3CtimerU3E__0_2(),
	U3CCoTimerU3Ec__Iterator0_t3483898792::get_offset_of_U24this_3(),
	U3CCoTimerU3Ec__Iterator0_t3483898792::get_offset_of_U24current_4(),
	U3CCoTimerU3Ec__Iterator0_t3483898792::get_offset_of_U24disposing_5(),
	U3CCoTimerU3Ec__Iterator0_t3483898792::get_offset_of_U24PC_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2776 = { sizeof (WaitTimer_t2776916572), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2776[9] = 
{
	WaitTimer_t2776916572::get_offset_of_duration_0(),
	WaitTimer_t2776916572::get_offset_of_delay_1(),
	WaitTimer_t2776916572::get_offset_of_initTime_2(),
	WaitTimer_t2776916572::get_offset_of_isStarted_3(),
	WaitTimer_t2776916572::get_offset_of_U3CTimeU3Ek__BackingField_4(),
	WaitTimer_t2776916572::get_offset_of_U3CTime01U3Ek__BackingField_5(),
	WaitTimer_t2776916572::get_offset_of_onStart_6(),
	WaitTimer_t2776916572::get_offset_of_onUpdate_7(),
	WaitTimer_t2776916572::get_offset_of_onComplete_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2777 = { sizeof (UguiCrossFadeDicing_t2659796554), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2777[1] = 
{
	UguiCrossFadeDicing_t2659796554::get_offset_of_fadePatternData_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2778 = { sizeof (UguiCrossFadeRawImage_t1509295035), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2778[6] = 
{
	UguiCrossFadeRawImage_t1509295035::get_offset_of_fadeTexture_2(),
	UguiCrossFadeRawImage_t1509295035::get_offset_of_strengh_3(),
	UguiCrossFadeRawImage_t1509295035::get_offset_of_target_4(),
	UguiCrossFadeRawImage_t1509295035::get_offset_of_timer_5(),
	UguiCrossFadeRawImage_t1509295035::get_offset_of_lastMaterial_6(),
	UguiCrossFadeRawImage_t1509295035::get_offset_of_corssFadeMaterial_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2779 = { sizeof (U3CCrossFadeU3Ec__AnonStorey0_t3339215237), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2779[2] = 
{
	U3CCrossFadeU3Ec__AnonStorey0_t3339215237::get_offset_of_onComplete_0(),
	U3CCrossFadeU3Ec__AnonStorey0_t3339215237::get_offset_of_U24this_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2780 = { sizeof (UguiFlip_t3801343075), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2780[2] = 
{
	UguiFlip_t3801343075::get_offset_of_flipX_3(),
	UguiFlip_t3801343075::get_offset_of_flipY_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2781 = { sizeof (UguiRichOutline_t1106552936), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2781[1] = 
{
	UguiRichOutline_t1106552936::get_offset_of_copyCount_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2782 = { sizeof (UguiTransition_t3998485683), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2782[6] = 
{
	UguiTransition_t3998485683::get_offset_of_target_2(),
	UguiTransition_t3998485683::get_offset_of_ruleTexture_3(),
	UguiTransition_t3998485683::get_offset_of_strengh_4(),
	UguiTransition_t3998485683::get_offset_of_vague_5(),
	UguiTransition_t3998485683::get_offset_of_U3CIsPremultipliedAlphaU3Ek__BackingField_6(),
	UguiTransition_t3998485683::get_offset_of_U3CDefaultMaterialU3Ek__BackingField_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2783 = { sizeof (U3CRuleFadeInU3Ec__AnonStorey0_t2664374724), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2783[3] = 
{
	U3CRuleFadeInU3Ec__AnonStorey0_t2664374724::get_offset_of_timer_0(),
	U3CRuleFadeInU3Ec__AnonStorey0_t2664374724::get_offset_of_onComplete_1(),
	U3CRuleFadeInU3Ec__AnonStorey0_t2664374724::get_offset_of_U24this_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2784 = { sizeof (U3CRuleFadeOutU3Ec__AnonStorey1_t2281953346), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2784[3] = 
{
	U3CRuleFadeOutU3Ec__AnonStorey1_t2281953346::get_offset_of_timer_0(),
	U3CRuleFadeOutU3Ec__AnonStorey1_t2281953346::get_offset_of_onComplete_1(),
	U3CRuleFadeOutU3Ec__AnonStorey1_t2281953346::get_offset_of_U24this_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2785 = { sizeof (AvatarData_t1681718915), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2785[3] = 
{
	AvatarData_t1681718915::get_offset_of_categories_2(),
	AvatarData_t1681718915::get_offset_of_optionTag_3(),
	AvatarData_t1681718915::get_offset_of_size_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2786 = { sizeof (Category_t2702308564), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2786[4] = 
{
	Category_t2702308564::get_offset_of_name_0(),
	Category_t2702308564::get_offset_of_sortOrder_1(),
	Category_t2702308564::get_offset_of_tag_2(),
	Category_t2702308564::get_offset_of_sprites_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2787 = { sizeof (U3CGetAllPatternNamesU3Ec__AnonStorey0_t275193013), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2787[1] = 
{
	U3CGetAllPatternNamesU3Ec__AnonStorey0_t275193013::get_offset_of_set_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2788 = { sizeof (U3CGetSpriteU3Ec__AnonStorey1_t1236054666), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2788[1] = 
{
	U3CGetSpriteU3Ec__AnonStorey1_t1236054666::get_offset_of_pattern_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2789 = { sizeof (U3CMakeSortedSpritesU3Ec__AnonStorey0_t1816920928), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2789[1] = 
{
	U3CMakeSortedSpritesU3Ec__AnonStorey0_t1816920928::get_offset_of_optionPattern_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2790 = { sizeof (AvatarImage_t1946614104), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2790[9] = 
{
	AvatarImage_t1946614104::get_offset_of_avatarData_2(),
	AvatarImage_t1946614104::get_offset_of_avatarPattern_3(),
	AvatarImage_t1946614104::get_offset_of_cachedRectTransform_4(),
	AvatarImage_t1946614104::get_offset_of_material_5(),
	AvatarImage_t1946614104::get_offset_of_OnPostRefresh_6(),
	AvatarImage_t1946614104::get_offset_of_flipX_7(),
	AvatarImage_t1946614104::get_offset_of_flipY_8(),
	AvatarImage_t1946614104::get_offset_of_rootChildren_9(),
	AvatarImage_t1946614104::get_offset_of_U3CHasChangedU3Ek__BackingField_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2791 = { sizeof (AvatarPattern_t4052248955), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2791[2] = 
{
	AvatarPattern_t4052248955::get_offset_of_avatarPatternDataList_0(),
	AvatarPattern_t4052248955::get_offset_of_optionPatternNameList_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2792 = { sizeof (PartternData_t3180716238), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2792[2] = 
{
	PartternData_t3180716238::get_offset_of_tag_0(),
	PartternData_t3180716238::get_offset_of_patternName_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2793 = { sizeof (U3CSetPatternNameU3Ec__AnonStorey0_t1376057134), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2793[1] = 
{
	U3CSetPatternNameU3Ec__AnonStorey0_t1376057134::get_offset_of_tag_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2794 = { sizeof (U3CGetPatternNameU3Ec__AnonStorey1_t263092357), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2794[1] = 
{
	U3CGetPatternNameU3Ec__AnonStorey1_t263092357::get_offset_of_tag_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2795 = { sizeof (U3CSetPatternU3Ec__AnonStorey2_t2684688205), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2795[1] = 
{
	U3CSetPatternU3Ec__AnonStorey2_t2684688205::get_offset_of_keyValue_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2796 = { sizeof (U3CRebuildU3Ec__AnonStorey3_t628083871), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2796[1] = 
{
	U3CRebuildU3Ec__AnonStorey3_t628083871::get_offset_of_category_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2797 = { sizeof (NovelAvatarPatternAttribute_t3689869613), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2797[1] = 
{
	NovelAvatarPatternAttribute_t3689869613::get_offset_of_U3CFunctionU3Ek__BackingField_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2798 = { sizeof (EyeBlinkAvatar_t3311504802), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2798[1] = 
{
	EyeBlinkAvatar_t3311504802::get_offset_of_avator_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2799 = { sizeof (U3CCoEyeBlinkU3Ec__Iterator0_t822695095), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2799[8] = 
{
	U3CCoEyeBlinkU3Ec__Iterator0_t822695095::get_offset_of_U3CpatternU3E__0_0(),
	U3CCoEyeBlinkU3Ec__Iterator0_t822695095::get_offset_of_U24locvar0_1(),
	U3CCoEyeBlinkU3Ec__Iterator0_t822695095::get_offset_of_U3CdataU3E__1_2(),
	U3CCoEyeBlinkU3Ec__Iterator0_t822695095::get_offset_of_onComplete_3(),
	U3CCoEyeBlinkU3Ec__Iterator0_t822695095::get_offset_of_U24this_4(),
	U3CCoEyeBlinkU3Ec__Iterator0_t822695095::get_offset_of_U24current_5(),
	U3CCoEyeBlinkU3Ec__Iterator0_t822695095::get_offset_of_U24disposing_6(),
	U3CCoEyeBlinkU3Ec__Iterator0_t822695095::get_offset_of_U24PC_7(),
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
