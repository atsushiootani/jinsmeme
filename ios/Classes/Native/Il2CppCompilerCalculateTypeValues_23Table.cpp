﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "object-internals.h"

// Utage.AdvSettingDataManager
struct AdvSettingDataManager_t931476416;
// Utage.AdvTextureSettingData
struct AdvTextureSettingData_t2976012266;
// System.Collections.Generic.Dictionary`2<System.String,Utage.AdvParamData>
struct Dictionary_2_t3372211046;
// System.String
struct String_t;
// Utage.AdvParamManager/<Read>c__AnonStorey3
struct U3CReadU3Ec__AnonStorey3_t2127335072;
// Utage.AdvGraphicInfoList
struct AdvGraphicInfoList_t3537398639;
// System.Collections.Generic.List`1<Utage.AdvTextureSettingData>
struct List_1_t2345133398;
// Utage.AdvGallerySaveData
struct AdvGallerySaveData_t1012686890;
// Utage.AdvBootSetting/DefaultDirInfo
struct DefaultDirInfo_t3178678002;
// Utage.AdvParticleSettingData
struct AdvParticleSettingData_t1603520623;
// Utage.AdvPage
struct AdvPage_t2715018132;
// UnityEngine.AudioClip
struct AudioClip_t1932558630;
// Utage.AdvGraphicLayer
struct AdvGraphicLayer_t3630223822;
// Utage.AdvParamStructTbl/<Read>c__AnonStorey3
struct U3CReadU3Ec__AnonStorey3_t881395170;
// Utage.AdvCharacterSettingData
struct AdvCharacterSettingData_t1671945242;
// System.Collections.Generic.Dictionary`2<System.String,Utage.AdvParamStruct>
struct Dictionary_2_t3811166195;
// Utage.AdvChapterData
struct AdvChapterData_t685691324;
// Utage.StringGridRow
struct StringGridRow_t4193237197;
// Utage.AdvVideoSettingData
struct AdvVideoSettingData_t2742317570;
// System.String[]
struct StringU5BU5D_t1642385972;
// Utage.AdvCommand
struct AdvCommand_t2859960984;
// System.Collections.Generic.List`1<Utage.StringGrid>
struct List_1_t1241274811;
// UnityEngine.AnimationClip
struct AnimationClip_t3510324950;
// System.Text.RegularExpressions.Regex
struct Regex_t1803876613;
// System.Collections.Generic.Dictionary`2<System.String,System.Int32>
struct Dictionary_2_t3986656710;
// Utage.AdvImportScenarios
struct AdvImportScenarios_t1226385437;
// Utage.AdvBootSetting
struct AdvBootSetting_t3720085301;
// Utage.AdvCharacterSetting
struct AdvCharacterSetting_t2712258826;
// Utage.AdvTextureSetting
struct AdvTextureSetting_t2060921594;
// Utage.AdvSoundSetting
struct AdvSoundSetting_t229690022;
// Utage.AdvLayerSetting
struct AdvLayerSetting_t3281222136;
// Utage.AdvParamManager
struct AdvParamManager_t1816006425;
// Utage.AdvSceneGallerySetting
struct AdvSceneGallerySetting_t2241589337;
// Utage.AdvLocalizeSetting
struct AdvLocalizeSetting_t467665552;
// Utage.AdvAnimationSetting
struct AdvAnimationSetting_t2232720937;
// Utage.AdvEyeBlinkSetting
struct AdvEyeBlinkSetting_t1976982958;
// Utage.AdvLipSynchSetting
struct AdvLipSynchSetting_t289134997;
// Utage.AdvParticleSetting
struct AdvParticleSetting_t588078687;
// Utage.AdvVideoSetting
struct AdvVideoSetting_t3960366418;
// System.Collections.Generic.List`1<Utage.IAdvSetting>
struct List_1_t4076707138;
// System.Action`1<Utage.IAdvSetting>
struct Action_1_t214418092;
// Utage.AdvEntityData
struct AdvEntityData_t1465354496;
// System.Collections.Generic.List`1<Utage.AssetFile>
struct List_1_t2752134388;
// Utage.AdvScenarioThread
struct AdvScenarioThread_t1270526825;
// System.Collections.Generic.Dictionary`2<System.String,Utage.AdvParamStructTbl>
struct Dictionary_2_t2908846465;
// Utage.AdvParamManager/IoInerface
struct IoInerface_t3434613093;
// Utage.AdvGraphicInfo
struct AdvGraphicInfo_t3545565645;
// Utage.AdvScenarioData
struct AdvScenarioData_t2546512739;
// System.Collections.Generic.List`1<Utage.AdvSceneGallerySettingData>
struct List_1_t649724181;
// System.Collections.Generic.Dictionary`2<System.String,Utage.AdvSceneGallerySettingData>
struct Dictionary_2_t3195382311;
// System.Collections.Generic.List`1<Utage.AdvSoundSettingData>
struct List_1_t2504512354;
// System.Collections.Generic.Dictionary`2<System.String,Utage.AdvSoundSettingData>
struct Dictionary_2_t755203188;
// System.Collections.Generic.Dictionary`2<System.String,Utage.AdvTextureSettingData>
struct Dictionary_2_t595824232;
// System.Collections.Generic.List`1<Utage.AdvVideoSettingData>
struct List_1_t2111438702;
// System.Collections.Generic.Dictionary`2<System.String,Utage.AdvVideoSettingData>
struct Dictionary_2_t362129536;
// Utage.AdvParamStructTbl
struct AdvParamStructTbl_t994067203;
// Utage.AdvParamStruct
struct AdvParamStruct_t1896386933;
// System.Void
struct Void_t1841601450;
// System.Collections.Generic.List`1<Utage.AdvParticleSettingData>
struct List_1_t972641755;
// System.Collections.Generic.Dictionary`2<System.String,Utage.AdvParticleSettingData>
struct Dictionary_2_t3518299885;
// System.Char[]
struct CharU5BU5D_t1328083999;
// System.Collections.Generic.List`1<Utage.AdvCharacterSettingData>
struct List_1_t1041066374;
// System.Collections.Generic.Dictionary`2<System.String,Utage.AdvCharacterSettingData>
struct Dictionary_2_t3586724504;
// System.Collections.Generic.List`1<Utage.AdvEyeBlinkData>
struct List_1_t2656469090;
// System.Collections.Generic.Dictionary`2<System.String,Utage.AdvEyeBlinkData>
struct Dictionary_2_t907159924;
// Utage.AdvEngine
struct AdvEngine_t1176753927;
// System.Collections.Generic.List`1<Utage.AdvLayerSettingData>
struct List_1_t1277893492;
// System.Collections.Generic.Dictionary`2<System.String,Utage.AdvLayerSettingData>
struct Dictionary_2_t3823551622;
// System.Collections.Generic.List`1<Utage.AdvLipSynchData>
struct List_1_t459569395;
// System.Collections.Generic.Dictionary`2<System.String,Utage.AdvLipSynchData>
struct Dictionary_2_t3005227525;
// System.Collections.Generic.List`1<Utage.AdvAnimationData>
struct List_1_t2800688647;
// Utage.AdvCharacterSettingData/ParseCustomFileTypeRootDir
struct ParseCustomFileTypeRootDir_t3762798398;
// Utage.AdvCharacterSettingData/IconInfo
struct IconInfo_t3287921618;
// Utage.MiniAnimationData
struct MiniAnimationData_t521227391;
// Utage.AdvEffectColor
struct AdvEffectColor_t2285992559;
// Utage.AdvCharacterGrayOutController
struct AdvCharacterGrayOutController_t895965523;
// Utage.DictionaryString
struct DictionaryString_t3376238631;
// Utage.AdvParamManager/<Write>c__AnonStorey1
struct U3CWriteU3Ec__AnonStorey1_t894999319;
// System.Collections.Generic.Dictionary`2<System.String,Utage.AdvScenarioData>
struct Dictionary_2_t166324705;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// System.DelegateData
struct DelegateData_t1572802995;
// Utage.AdvParamStructTbl/<Write>c__AnonStorey1
struct U3CWriteU3Ec__AnonStorey1_t2299255853;
// System.Collections.Generic.List`1<Utage.StringGridRow>
struct List_1_t3562358329;
// Utage.AssetFile
struct AssetFile_t3383013256;
// Utage.AdvLayerSettingData/RectSetting
struct RectSetting_t1088783529;
// Utage.AdvTextureSettingData/ParseCustomFileTypeRootDir
struct ParseCustomFileTypeRootDir_t2850020050;
// System.Collections.Generic.List`1<System.Int32>
struct List_1_t1440998580;
// System.Collections.Generic.List`1<Utage.AdvEntityData>
struct List_1_t834475628;
// System.Collections.Generic.List`1<Utage.AdvChapterData>
struct List_1_t54812456;
// System.Collections.Generic.List`1<Utage.AdvImportScenarioSheet>
struct List_1_t2918946017;
// System.IAsyncResult
struct IAsyncResult_t1999651008;
// System.AsyncCallback
struct AsyncCallback_t163412349;
// System.Collections.Generic.List`1<Utage.AdvImportBook>
struct List_1_t3327163157;
// Utage.AdvDataManager
struct AdvDataManager_t2481232830;
// Utage.AdvMacroManager
struct AdvMacroManager_t3793610292;
// UnityEngine.GameObject
struct GameObject_t1756533147;
// UnityEngine.UI.Text
struct Text_t356221433;
// Utage.AdvSelectionTimeLimit
struct AdvSelectionTimeLimit_t2136628673;
// Utage.AdvUguiSelection
struct AdvUguiSelection_t2632218991;
// System.Collections.Generic.List`1<System.String>
struct List_1_t1398341365;
// System.Collections.Generic.List`1<Utage.AdvGraphicLayer>
struct List_1_t2999344954;
// System.Collections.Generic.Dictionary`2<Utage.AdvScenarioPageData,System.Int32>
struct Dictionary_2_t1287095029;
// Utage.iTweenData
struct iTweenData_t1904063128;
// System.Collections.Hashtable
struct Hashtable_t909839986;
// System.Action`1<Utage.AdvITweenPlayer>
struct Action_1_t1793357254;
// System.Collections.Generic.List`1<Utage.AdvITweenPlayer>
struct List_1_t1360679004;
// Utage.AdvUguiMessageWindowManager
struct AdvUguiMessageWindowManager_t1210998287;
// System.Collections.Generic.List`1<UnityEngine.Texture2D>
struct List_1_t2912116861;
// System.Action
struct Action_t3226471752;
// UnityEngine.Animation
struct Animation_t2068071072;
// UnityEngine.Animator
struct Animator_t69676727;
// System.Collections.Generic.List`1<Utage.AdvTextSound/SoundInfo>
struct List_1_t2907673085;




#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
#ifndef U3CBOOTINITU3EC__ANONSTOREY0_T1781737112_H
#define U3CBOOTINITU3EC__ANONSTOREY0_T1781737112_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.AdvTextureSettingData/<BootInit>c__AnonStorey0
struct  U3CBootInitU3Ec__AnonStorey0_t1781737112  : public RuntimeObject
{
public:
	// Utage.AdvSettingDataManager Utage.AdvTextureSettingData/<BootInit>c__AnonStorey0::dataManager
	AdvSettingDataManager_t931476416 * ___dataManager_0;
	// Utage.AdvTextureSettingData Utage.AdvTextureSettingData/<BootInit>c__AnonStorey0::$this
	AdvTextureSettingData_t2976012266 * ___U24this_1;

public:
	inline static int32_t get_offset_of_dataManager_0() { return static_cast<int32_t>(offsetof(U3CBootInitU3Ec__AnonStorey0_t1781737112, ___dataManager_0)); }
	inline AdvSettingDataManager_t931476416 * get_dataManager_0() const { return ___dataManager_0; }
	inline AdvSettingDataManager_t931476416 ** get_address_of_dataManager_0() { return &___dataManager_0; }
	inline void set_dataManager_0(AdvSettingDataManager_t931476416 * value)
	{
		___dataManager_0 = value;
		Il2CppCodeGenWriteBarrier((&___dataManager_0), value);
	}

	inline static int32_t get_offset_of_U24this_1() { return static_cast<int32_t>(offsetof(U3CBootInitU3Ec__AnonStorey0_t1781737112, ___U24this_1)); }
	inline AdvTextureSettingData_t2976012266 * get_U24this_1() const { return ___U24this_1; }
	inline AdvTextureSettingData_t2976012266 ** get_address_of_U24this_1() { return &___U24this_1; }
	inline void set_U24this_1(AdvTextureSettingData_t2976012266 * value)
	{
		___U24this_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CBOOTINITU3EC__ANONSTOREY0_T1781737112_H
#ifndef ADVPARAMSTRUCT_T1896386933_H
#define ADVPARAMSTRUCT_T1896386933_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.AdvParamStruct
struct  AdvParamStruct_t1896386933  : public RuntimeObject
{
public:
	// System.Collections.Generic.Dictionary`2<System.String,Utage.AdvParamData> Utage.AdvParamStruct::tbl
	Dictionary_2_t3372211046 * ___tbl_0;

public:
	inline static int32_t get_offset_of_tbl_0() { return static_cast<int32_t>(offsetof(AdvParamStruct_t1896386933, ___tbl_0)); }
	inline Dictionary_2_t3372211046 * get_tbl_0() const { return ___tbl_0; }
	inline Dictionary_2_t3372211046 ** get_address_of_tbl_0() { return &___tbl_0; }
	inline void set_tbl_0(Dictionary_2_t3372211046 * value)
	{
		___tbl_0 = value;
		Il2CppCodeGenWriteBarrier((&___tbl_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ADVPARAMSTRUCT_T1896386933_H
#ifndef U3CREADU3EC__ANONSTOREY2_T561251131_H
#define U3CREADU3EC__ANONSTOREY2_T561251131_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.AdvParamManager/<Read>c__AnonStorey2
struct  U3CReadU3Ec__AnonStorey2_t561251131  : public RuntimeObject
{
public:
	// System.String Utage.AdvParamManager/<Read>c__AnonStorey2::key
	String_t* ___key_0;
	// Utage.AdvParamManager/<Read>c__AnonStorey3 Utage.AdvParamManager/<Read>c__AnonStorey2::<>f__ref$3
	U3CReadU3Ec__AnonStorey3_t2127335072 * ___U3CU3Ef__refU243_1;

public:
	inline static int32_t get_offset_of_key_0() { return static_cast<int32_t>(offsetof(U3CReadU3Ec__AnonStorey2_t561251131, ___key_0)); }
	inline String_t* get_key_0() const { return ___key_0; }
	inline String_t** get_address_of_key_0() { return &___key_0; }
	inline void set_key_0(String_t* value)
	{
		___key_0 = value;
		Il2CppCodeGenWriteBarrier((&___key_0), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__refU243_1() { return static_cast<int32_t>(offsetof(U3CReadU3Ec__AnonStorey2_t561251131, ___U3CU3Ef__refU243_1)); }
	inline U3CReadU3Ec__AnonStorey3_t2127335072 * get_U3CU3Ef__refU243_1() const { return ___U3CU3Ef__refU243_1; }
	inline U3CReadU3Ec__AnonStorey3_t2127335072 ** get_address_of_U3CU3Ef__refU243_1() { return &___U3CU3Ef__refU243_1; }
	inline void set_U3CU3Ef__refU243_1(U3CReadU3Ec__AnonStorey3_t2127335072 * value)
	{
		___U3CU3Ef__refU243_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__refU243_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CREADU3EC__ANONSTOREY2_T561251131_H
#ifndef U3CCREATEU3EC__ANONSTOREY0_T3183710506_H
#define U3CCREATEU3EC__ANONSTOREY0_T3183710506_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.AdvCharacterInfo/<Create>c__AnonStorey0
struct  U3CCreateU3Ec__AnonStorey0_t3183710506  : public RuntimeObject
{
public:
	// System.Boolean Utage.AdvCharacterInfo/<Create>c__AnonStorey0::isHide
	bool ___isHide_0;
	// System.String Utage.AdvCharacterInfo/<Create>c__AnonStorey0::characterLabel
	String_t* ___characterLabel_1;
	// System.String Utage.AdvCharacterInfo/<Create>c__AnonStorey0::erroMsg
	String_t* ___erroMsg_2;

public:
	inline static int32_t get_offset_of_isHide_0() { return static_cast<int32_t>(offsetof(U3CCreateU3Ec__AnonStorey0_t3183710506, ___isHide_0)); }
	inline bool get_isHide_0() const { return ___isHide_0; }
	inline bool* get_address_of_isHide_0() { return &___isHide_0; }
	inline void set_isHide_0(bool value)
	{
		___isHide_0 = value;
	}

	inline static int32_t get_offset_of_characterLabel_1() { return static_cast<int32_t>(offsetof(U3CCreateU3Ec__AnonStorey0_t3183710506, ___characterLabel_1)); }
	inline String_t* get_characterLabel_1() const { return ___characterLabel_1; }
	inline String_t** get_address_of_characterLabel_1() { return &___characterLabel_1; }
	inline void set_characterLabel_1(String_t* value)
	{
		___characterLabel_1 = value;
		Il2CppCodeGenWriteBarrier((&___characterLabel_1), value);
	}

	inline static int32_t get_offset_of_erroMsg_2() { return static_cast<int32_t>(offsetof(U3CCreateU3Ec__AnonStorey0_t3183710506, ___erroMsg_2)); }
	inline String_t* get_erroMsg_2() const { return ___erroMsg_2; }
	inline String_t** get_address_of_erroMsg_2() { return &___erroMsg_2; }
	inline void set_erroMsg_2(String_t* value)
	{
		___erroMsg_2 = value;
		Il2CppCodeGenWriteBarrier((&___erroMsg_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CCREATEU3EC__ANONSTOREY0_T3183710506_H
#ifndef ADVCHARACTERINFO_T1582765630_H
#define ADVCHARACTERINFO_T1582765630_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.AdvCharacterInfo
struct  AdvCharacterInfo_t1582765630  : public RuntimeObject
{
public:
	// System.String Utage.AdvCharacterInfo::<Label>k__BackingField
	String_t* ___U3CLabelU3Ek__BackingField_0;
	// System.String Utage.AdvCharacterInfo::<NameText>k__BackingField
	String_t* ___U3CNameTextU3Ek__BackingField_1;
	// System.String Utage.AdvCharacterInfo::<Pattern>k__BackingField
	String_t* ___U3CPatternU3Ek__BackingField_2;
	// System.Boolean Utage.AdvCharacterInfo::<IsHide>k__BackingField
	bool ___U3CIsHideU3Ek__BackingField_3;
	// Utage.AdvGraphicInfoList Utage.AdvCharacterInfo::<Graphic>k__BackingField
	AdvGraphicInfoList_t3537398639 * ___U3CGraphicU3Ek__BackingField_4;

public:
	inline static int32_t get_offset_of_U3CLabelU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(AdvCharacterInfo_t1582765630, ___U3CLabelU3Ek__BackingField_0)); }
	inline String_t* get_U3CLabelU3Ek__BackingField_0() const { return ___U3CLabelU3Ek__BackingField_0; }
	inline String_t** get_address_of_U3CLabelU3Ek__BackingField_0() { return &___U3CLabelU3Ek__BackingField_0; }
	inline void set_U3CLabelU3Ek__BackingField_0(String_t* value)
	{
		___U3CLabelU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CLabelU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_U3CNameTextU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(AdvCharacterInfo_t1582765630, ___U3CNameTextU3Ek__BackingField_1)); }
	inline String_t* get_U3CNameTextU3Ek__BackingField_1() const { return ___U3CNameTextU3Ek__BackingField_1; }
	inline String_t** get_address_of_U3CNameTextU3Ek__BackingField_1() { return &___U3CNameTextU3Ek__BackingField_1; }
	inline void set_U3CNameTextU3Ek__BackingField_1(String_t* value)
	{
		___U3CNameTextU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CNameTextU3Ek__BackingField_1), value);
	}

	inline static int32_t get_offset_of_U3CPatternU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(AdvCharacterInfo_t1582765630, ___U3CPatternU3Ek__BackingField_2)); }
	inline String_t* get_U3CPatternU3Ek__BackingField_2() const { return ___U3CPatternU3Ek__BackingField_2; }
	inline String_t** get_address_of_U3CPatternU3Ek__BackingField_2() { return &___U3CPatternU3Ek__BackingField_2; }
	inline void set_U3CPatternU3Ek__BackingField_2(String_t* value)
	{
		___U3CPatternU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CPatternU3Ek__BackingField_2), value);
	}

	inline static int32_t get_offset_of_U3CIsHideU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(AdvCharacterInfo_t1582765630, ___U3CIsHideU3Ek__BackingField_3)); }
	inline bool get_U3CIsHideU3Ek__BackingField_3() const { return ___U3CIsHideU3Ek__BackingField_3; }
	inline bool* get_address_of_U3CIsHideU3Ek__BackingField_3() { return &___U3CIsHideU3Ek__BackingField_3; }
	inline void set_U3CIsHideU3Ek__BackingField_3(bool value)
	{
		___U3CIsHideU3Ek__BackingField_3 = value;
	}

	inline static int32_t get_offset_of_U3CGraphicU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(AdvCharacterInfo_t1582765630, ___U3CGraphicU3Ek__BackingField_4)); }
	inline AdvGraphicInfoList_t3537398639 * get_U3CGraphicU3Ek__BackingField_4() const { return ___U3CGraphicU3Ek__BackingField_4; }
	inline AdvGraphicInfoList_t3537398639 ** get_address_of_U3CGraphicU3Ek__BackingField_4() { return &___U3CGraphicU3Ek__BackingField_4; }
	inline void set_U3CGraphicU3Ek__BackingField_4(AdvGraphicInfoList_t3537398639 * value)
	{
		___U3CGraphicU3Ek__BackingField_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CGraphicU3Ek__BackingField_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ADVCHARACTERINFO_T1582765630_H
#ifndef ADVCGGALLERYDATA_T4276096361_H
#define ADVCGGALLERYDATA_T4276096361_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.AdvCgGalleryData
struct  AdvCgGalleryData_t4276096361  : public RuntimeObject
{
public:
	// System.Collections.Generic.List`1<Utage.AdvTextureSettingData> Utage.AdvCgGalleryData::list
	List_1_t2345133398 * ___list_0;
	// System.String Utage.AdvCgGalleryData::thumbnailPath
	String_t* ___thumbnailPath_1;
	// Utage.AdvGallerySaveData Utage.AdvCgGalleryData::saveData
	AdvGallerySaveData_t1012686890 * ___saveData_2;

public:
	inline static int32_t get_offset_of_list_0() { return static_cast<int32_t>(offsetof(AdvCgGalleryData_t4276096361, ___list_0)); }
	inline List_1_t2345133398 * get_list_0() const { return ___list_0; }
	inline List_1_t2345133398 ** get_address_of_list_0() { return &___list_0; }
	inline void set_list_0(List_1_t2345133398 * value)
	{
		___list_0 = value;
		Il2CppCodeGenWriteBarrier((&___list_0), value);
	}

	inline static int32_t get_offset_of_thumbnailPath_1() { return static_cast<int32_t>(offsetof(AdvCgGalleryData_t4276096361, ___thumbnailPath_1)); }
	inline String_t* get_thumbnailPath_1() const { return ___thumbnailPath_1; }
	inline String_t** get_address_of_thumbnailPath_1() { return &___thumbnailPath_1; }
	inline void set_thumbnailPath_1(String_t* value)
	{
		___thumbnailPath_1 = value;
		Il2CppCodeGenWriteBarrier((&___thumbnailPath_1), value);
	}

	inline static int32_t get_offset_of_saveData_2() { return static_cast<int32_t>(offsetof(AdvCgGalleryData_t4276096361, ___saveData_2)); }
	inline AdvGallerySaveData_t1012686890 * get_saveData_2() const { return ___saveData_2; }
	inline AdvGallerySaveData_t1012686890 ** get_address_of_saveData_2() { return &___saveData_2; }
	inline void set_saveData_2(AdvGallerySaveData_t1012686890 * value)
	{
		___saveData_2 = value;
		Il2CppCodeGenWriteBarrier((&___saveData_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ADVCGGALLERYDATA_T4276096361_H
#ifndef DEFAULTDIRINFO_T3178678002_H
#define DEFAULTDIRINFO_T3178678002_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.AdvBootSetting/DefaultDirInfo
struct  DefaultDirInfo_t3178678002  : public RuntimeObject
{
public:
	// System.String Utage.AdvBootSetting/DefaultDirInfo::defaultDir
	String_t* ___defaultDir_0;
	// System.String Utage.AdvBootSetting/DefaultDirInfo::defaultExt
	String_t* ___defaultExt_1;

public:
	inline static int32_t get_offset_of_defaultDir_0() { return static_cast<int32_t>(offsetof(DefaultDirInfo_t3178678002, ___defaultDir_0)); }
	inline String_t* get_defaultDir_0() const { return ___defaultDir_0; }
	inline String_t** get_address_of_defaultDir_0() { return &___defaultDir_0; }
	inline void set_defaultDir_0(String_t* value)
	{
		___defaultDir_0 = value;
		Il2CppCodeGenWriteBarrier((&___defaultDir_0), value);
	}

	inline static int32_t get_offset_of_defaultExt_1() { return static_cast<int32_t>(offsetof(DefaultDirInfo_t3178678002, ___defaultExt_1)); }
	inline String_t* get_defaultExt_1() const { return ___defaultExt_1; }
	inline String_t** get_address_of_defaultExt_1() { return &___defaultExt_1; }
	inline void set_defaultExt_1(String_t* value)
	{
		___defaultExt_1 = value;
		Il2CppCodeGenWriteBarrier((&___defaultExt_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DEFAULTDIRINFO_T3178678002_H
#ifndef ADVBOOTSETTING_T3720085301_H
#define ADVBOOTSETTING_T3720085301_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.AdvBootSetting
struct  AdvBootSetting_t3720085301  : public RuntimeObject
{
public:
	// System.String Utage.AdvBootSetting::<ResourceDir>k__BackingField
	String_t* ___U3CResourceDirU3Ek__BackingField_0;
	// Utage.AdvBootSetting/DefaultDirInfo Utage.AdvBootSetting::characterDirInfo
	DefaultDirInfo_t3178678002 * ___characterDirInfo_1;
	// Utage.AdvBootSetting/DefaultDirInfo Utage.AdvBootSetting::bgDirInfo
	DefaultDirInfo_t3178678002 * ___bgDirInfo_2;
	// Utage.AdvBootSetting/DefaultDirInfo Utage.AdvBootSetting::eventDirInfo
	DefaultDirInfo_t3178678002 * ___eventDirInfo_3;
	// Utage.AdvBootSetting/DefaultDirInfo Utage.AdvBootSetting::spriteDirInfo
	DefaultDirInfo_t3178678002 * ___spriteDirInfo_4;
	// Utage.AdvBootSetting/DefaultDirInfo Utage.AdvBootSetting::thumbnailDirInfo
	DefaultDirInfo_t3178678002 * ___thumbnailDirInfo_5;
	// Utage.AdvBootSetting/DefaultDirInfo Utage.AdvBootSetting::bgmDirInfo
	DefaultDirInfo_t3178678002 * ___bgmDirInfo_6;
	// Utage.AdvBootSetting/DefaultDirInfo Utage.AdvBootSetting::seDirInfo
	DefaultDirInfo_t3178678002 * ___seDirInfo_7;
	// Utage.AdvBootSetting/DefaultDirInfo Utage.AdvBootSetting::ambienceDirInfo
	DefaultDirInfo_t3178678002 * ___ambienceDirInfo_8;
	// Utage.AdvBootSetting/DefaultDirInfo Utage.AdvBootSetting::voiceDirInfo
	DefaultDirInfo_t3178678002 * ___voiceDirInfo_9;
	// Utage.AdvBootSetting/DefaultDirInfo Utage.AdvBootSetting::particleDirInfo
	DefaultDirInfo_t3178678002 * ___particleDirInfo_10;
	// Utage.AdvBootSetting/DefaultDirInfo Utage.AdvBootSetting::videoDirInfo
	DefaultDirInfo_t3178678002 * ___videoDirInfo_11;

public:
	inline static int32_t get_offset_of_U3CResourceDirU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(AdvBootSetting_t3720085301, ___U3CResourceDirU3Ek__BackingField_0)); }
	inline String_t* get_U3CResourceDirU3Ek__BackingField_0() const { return ___U3CResourceDirU3Ek__BackingField_0; }
	inline String_t** get_address_of_U3CResourceDirU3Ek__BackingField_0() { return &___U3CResourceDirU3Ek__BackingField_0; }
	inline void set_U3CResourceDirU3Ek__BackingField_0(String_t* value)
	{
		___U3CResourceDirU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CResourceDirU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_characterDirInfo_1() { return static_cast<int32_t>(offsetof(AdvBootSetting_t3720085301, ___characterDirInfo_1)); }
	inline DefaultDirInfo_t3178678002 * get_characterDirInfo_1() const { return ___characterDirInfo_1; }
	inline DefaultDirInfo_t3178678002 ** get_address_of_characterDirInfo_1() { return &___characterDirInfo_1; }
	inline void set_characterDirInfo_1(DefaultDirInfo_t3178678002 * value)
	{
		___characterDirInfo_1 = value;
		Il2CppCodeGenWriteBarrier((&___characterDirInfo_1), value);
	}

	inline static int32_t get_offset_of_bgDirInfo_2() { return static_cast<int32_t>(offsetof(AdvBootSetting_t3720085301, ___bgDirInfo_2)); }
	inline DefaultDirInfo_t3178678002 * get_bgDirInfo_2() const { return ___bgDirInfo_2; }
	inline DefaultDirInfo_t3178678002 ** get_address_of_bgDirInfo_2() { return &___bgDirInfo_2; }
	inline void set_bgDirInfo_2(DefaultDirInfo_t3178678002 * value)
	{
		___bgDirInfo_2 = value;
		Il2CppCodeGenWriteBarrier((&___bgDirInfo_2), value);
	}

	inline static int32_t get_offset_of_eventDirInfo_3() { return static_cast<int32_t>(offsetof(AdvBootSetting_t3720085301, ___eventDirInfo_3)); }
	inline DefaultDirInfo_t3178678002 * get_eventDirInfo_3() const { return ___eventDirInfo_3; }
	inline DefaultDirInfo_t3178678002 ** get_address_of_eventDirInfo_3() { return &___eventDirInfo_3; }
	inline void set_eventDirInfo_3(DefaultDirInfo_t3178678002 * value)
	{
		___eventDirInfo_3 = value;
		Il2CppCodeGenWriteBarrier((&___eventDirInfo_3), value);
	}

	inline static int32_t get_offset_of_spriteDirInfo_4() { return static_cast<int32_t>(offsetof(AdvBootSetting_t3720085301, ___spriteDirInfo_4)); }
	inline DefaultDirInfo_t3178678002 * get_spriteDirInfo_4() const { return ___spriteDirInfo_4; }
	inline DefaultDirInfo_t3178678002 ** get_address_of_spriteDirInfo_4() { return &___spriteDirInfo_4; }
	inline void set_spriteDirInfo_4(DefaultDirInfo_t3178678002 * value)
	{
		___spriteDirInfo_4 = value;
		Il2CppCodeGenWriteBarrier((&___spriteDirInfo_4), value);
	}

	inline static int32_t get_offset_of_thumbnailDirInfo_5() { return static_cast<int32_t>(offsetof(AdvBootSetting_t3720085301, ___thumbnailDirInfo_5)); }
	inline DefaultDirInfo_t3178678002 * get_thumbnailDirInfo_5() const { return ___thumbnailDirInfo_5; }
	inline DefaultDirInfo_t3178678002 ** get_address_of_thumbnailDirInfo_5() { return &___thumbnailDirInfo_5; }
	inline void set_thumbnailDirInfo_5(DefaultDirInfo_t3178678002 * value)
	{
		___thumbnailDirInfo_5 = value;
		Il2CppCodeGenWriteBarrier((&___thumbnailDirInfo_5), value);
	}

	inline static int32_t get_offset_of_bgmDirInfo_6() { return static_cast<int32_t>(offsetof(AdvBootSetting_t3720085301, ___bgmDirInfo_6)); }
	inline DefaultDirInfo_t3178678002 * get_bgmDirInfo_6() const { return ___bgmDirInfo_6; }
	inline DefaultDirInfo_t3178678002 ** get_address_of_bgmDirInfo_6() { return &___bgmDirInfo_6; }
	inline void set_bgmDirInfo_6(DefaultDirInfo_t3178678002 * value)
	{
		___bgmDirInfo_6 = value;
		Il2CppCodeGenWriteBarrier((&___bgmDirInfo_6), value);
	}

	inline static int32_t get_offset_of_seDirInfo_7() { return static_cast<int32_t>(offsetof(AdvBootSetting_t3720085301, ___seDirInfo_7)); }
	inline DefaultDirInfo_t3178678002 * get_seDirInfo_7() const { return ___seDirInfo_7; }
	inline DefaultDirInfo_t3178678002 ** get_address_of_seDirInfo_7() { return &___seDirInfo_7; }
	inline void set_seDirInfo_7(DefaultDirInfo_t3178678002 * value)
	{
		___seDirInfo_7 = value;
		Il2CppCodeGenWriteBarrier((&___seDirInfo_7), value);
	}

	inline static int32_t get_offset_of_ambienceDirInfo_8() { return static_cast<int32_t>(offsetof(AdvBootSetting_t3720085301, ___ambienceDirInfo_8)); }
	inline DefaultDirInfo_t3178678002 * get_ambienceDirInfo_8() const { return ___ambienceDirInfo_8; }
	inline DefaultDirInfo_t3178678002 ** get_address_of_ambienceDirInfo_8() { return &___ambienceDirInfo_8; }
	inline void set_ambienceDirInfo_8(DefaultDirInfo_t3178678002 * value)
	{
		___ambienceDirInfo_8 = value;
		Il2CppCodeGenWriteBarrier((&___ambienceDirInfo_8), value);
	}

	inline static int32_t get_offset_of_voiceDirInfo_9() { return static_cast<int32_t>(offsetof(AdvBootSetting_t3720085301, ___voiceDirInfo_9)); }
	inline DefaultDirInfo_t3178678002 * get_voiceDirInfo_9() const { return ___voiceDirInfo_9; }
	inline DefaultDirInfo_t3178678002 ** get_address_of_voiceDirInfo_9() { return &___voiceDirInfo_9; }
	inline void set_voiceDirInfo_9(DefaultDirInfo_t3178678002 * value)
	{
		___voiceDirInfo_9 = value;
		Il2CppCodeGenWriteBarrier((&___voiceDirInfo_9), value);
	}

	inline static int32_t get_offset_of_particleDirInfo_10() { return static_cast<int32_t>(offsetof(AdvBootSetting_t3720085301, ___particleDirInfo_10)); }
	inline DefaultDirInfo_t3178678002 * get_particleDirInfo_10() const { return ___particleDirInfo_10; }
	inline DefaultDirInfo_t3178678002 ** get_address_of_particleDirInfo_10() { return &___particleDirInfo_10; }
	inline void set_particleDirInfo_10(DefaultDirInfo_t3178678002 * value)
	{
		___particleDirInfo_10 = value;
		Il2CppCodeGenWriteBarrier((&___particleDirInfo_10), value);
	}

	inline static int32_t get_offset_of_videoDirInfo_11() { return static_cast<int32_t>(offsetof(AdvBootSetting_t3720085301, ___videoDirInfo_11)); }
	inline DefaultDirInfo_t3178678002 * get_videoDirInfo_11() const { return ___videoDirInfo_11; }
	inline DefaultDirInfo_t3178678002 ** get_address_of_videoDirInfo_11() { return &___videoDirInfo_11; }
	inline void set_videoDirInfo_11(DefaultDirInfo_t3178678002 * value)
	{
		___videoDirInfo_11 = value;
		Il2CppCodeGenWriteBarrier((&___videoDirInfo_11), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ADVBOOTSETTING_T3720085301_H
#ifndef U3CBOOTINITU3EC__ANONSTOREY0_T2032018241_H
#define U3CBOOTINITU3EC__ANONSTOREY0_T2032018241_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.AdvParticleSettingData/<BootInit>c__AnonStorey0
struct  U3CBootInitU3Ec__AnonStorey0_t2032018241  : public RuntimeObject
{
public:
	// Utage.AdvSettingDataManager Utage.AdvParticleSettingData/<BootInit>c__AnonStorey0::dataManager
	AdvSettingDataManager_t931476416 * ___dataManager_0;
	// Utage.AdvParticleSettingData Utage.AdvParticleSettingData/<BootInit>c__AnonStorey0::$this
	AdvParticleSettingData_t1603520623 * ___U24this_1;

public:
	inline static int32_t get_offset_of_dataManager_0() { return static_cast<int32_t>(offsetof(U3CBootInitU3Ec__AnonStorey0_t2032018241, ___dataManager_0)); }
	inline AdvSettingDataManager_t931476416 * get_dataManager_0() const { return ___dataManager_0; }
	inline AdvSettingDataManager_t931476416 ** get_address_of_dataManager_0() { return &___dataManager_0; }
	inline void set_dataManager_0(AdvSettingDataManager_t931476416 * value)
	{
		___dataManager_0 = value;
		Il2CppCodeGenWriteBarrier((&___dataManager_0), value);
	}

	inline static int32_t get_offset_of_U24this_1() { return static_cast<int32_t>(offsetof(U3CBootInitU3Ec__AnonStorey0_t2032018241, ___U24this_1)); }
	inline AdvParticleSettingData_t1603520623 * get_U24this_1() const { return ___U24this_1; }
	inline AdvParticleSettingData_t1603520623 ** get_address_of_U24this_1() { return &___U24this_1; }
	inline void set_U24this_1(AdvParticleSettingData_t1603520623 * value)
	{
		___U24this_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CBOOTINITU3EC__ANONSTOREY0_T2032018241_H
#ifndef U3CGETSEU3EC__ANONSTOREY0_T3599616041_H
#define U3CGETSEU3EC__ANONSTOREY0_T3599616041_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.AdvTextSound/<GetSe>c__AnonStorey0
struct  U3CGetSeU3Ec__AnonStorey0_t3599616041  : public RuntimeObject
{
public:
	// Utage.AdvPage Utage.AdvTextSound/<GetSe>c__AnonStorey0::page
	AdvPage_t2715018132 * ___page_0;

public:
	inline static int32_t get_offset_of_page_0() { return static_cast<int32_t>(offsetof(U3CGetSeU3Ec__AnonStorey0_t3599616041, ___page_0)); }
	inline AdvPage_t2715018132 * get_page_0() const { return ___page_0; }
	inline AdvPage_t2715018132 ** get_address_of_page_0() { return &___page_0; }
	inline void set_page_0(AdvPage_t2715018132 * value)
	{
		___page_0 = value;
		Il2CppCodeGenWriteBarrier((&___page_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CGETSEU3EC__ANONSTOREY0_T3599616041_H
#ifndef SOUNDINFO_T3538551953_H
#define SOUNDINFO_T3538551953_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.AdvTextSound/SoundInfo
struct  SoundInfo_t3538551953  : public RuntimeObject
{
public:
	// System.String Utage.AdvTextSound/SoundInfo::key
	String_t* ___key_0;
	// UnityEngine.AudioClip Utage.AdvTextSound/SoundInfo::sound
	AudioClip_t1932558630 * ___sound_1;

public:
	inline static int32_t get_offset_of_key_0() { return static_cast<int32_t>(offsetof(SoundInfo_t3538551953, ___key_0)); }
	inline String_t* get_key_0() const { return ___key_0; }
	inline String_t** get_address_of_key_0() { return &___key_0; }
	inline void set_key_0(String_t* value)
	{
		___key_0 = value;
		Il2CppCodeGenWriteBarrier((&___key_0), value);
	}

	inline static int32_t get_offset_of_sound_1() { return static_cast<int32_t>(offsetof(SoundInfo_t3538551953, ___sound_1)); }
	inline AudioClip_t1932558630 * get_sound_1() const { return ___sound_1; }
	inline AudioClip_t1932558630 ** get_address_of_sound_1() { return &___sound_1; }
	inline void set_sound_1(AudioClip_t1932558630 * value)
	{
		___sound_1 = value;
		Il2CppCodeGenWriteBarrier((&___sound_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SOUNDINFO_T3538551953_H
#ifndef U3CISLIGHTINGCHARACTERU3EC__ANONSTOREY1_T623611741_H
#define U3CISLIGHTINGCHARACTERU3EC__ANONSTOREY1_T623611741_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.AdvCharacterGrayOutController/<IsLightingCharacter>c__AnonStorey1
struct  U3CIsLightingCharacterU3Ec__AnonStorey1_t623611741  : public RuntimeObject
{
public:
	// Utage.AdvGraphicLayer Utage.AdvCharacterGrayOutController/<IsLightingCharacter>c__AnonStorey1::layer
	AdvGraphicLayer_t3630223822 * ___layer_0;

public:
	inline static int32_t get_offset_of_layer_0() { return static_cast<int32_t>(offsetof(U3CIsLightingCharacterU3Ec__AnonStorey1_t623611741, ___layer_0)); }
	inline AdvGraphicLayer_t3630223822 * get_layer_0() const { return ___layer_0; }
	inline AdvGraphicLayer_t3630223822 ** get_address_of_layer_0() { return &___layer_0; }
	inline void set_layer_0(AdvGraphicLayer_t3630223822 * value)
	{
		___layer_0 = value;
		Il2CppCodeGenWriteBarrier((&___layer_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CISLIGHTINGCHARACTERU3EC__ANONSTOREY1_T623611741_H
#ifndef U3CFINDRULETEXTUREU3EC__ANONSTOREY0_T4067206823_H
#define U3CFINDRULETEXTUREU3EC__ANONSTOREY0_T4067206823_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.AdvEffectManager/<FindRuleTexture>c__AnonStorey0
struct  U3CFindRuleTextureU3Ec__AnonStorey0_t4067206823  : public RuntimeObject
{
public:
	// System.String Utage.AdvEffectManager/<FindRuleTexture>c__AnonStorey0::name
	String_t* ___name_0;

public:
	inline static int32_t get_offset_of_name_0() { return static_cast<int32_t>(offsetof(U3CFindRuleTextureU3Ec__AnonStorey0_t4067206823, ___name_0)); }
	inline String_t* get_name_0() const { return ___name_0; }
	inline String_t** get_address_of_name_0() { return &___name_0; }
	inline void set_name_0(String_t* value)
	{
		___name_0 = value;
		Il2CppCodeGenWriteBarrier((&___name_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CFINDRULETEXTUREU3EC__ANONSTOREY0_T4067206823_H
#ifndef U3CREADU3EC__ANONSTOREY2_T3610278525_H
#define U3CREADU3EC__ANONSTOREY2_T3610278525_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.AdvParamStructTbl/<Read>c__AnonStorey2
struct  U3CReadU3Ec__AnonStorey2_t3610278525  : public RuntimeObject
{
public:
	// System.String Utage.AdvParamStructTbl/<Read>c__AnonStorey2::key
	String_t* ___key_0;
	// Utage.AdvParamStructTbl/<Read>c__AnonStorey3 Utage.AdvParamStructTbl/<Read>c__AnonStorey2::<>f__ref$3
	U3CReadU3Ec__AnonStorey3_t881395170 * ___U3CU3Ef__refU243_1;

public:
	inline static int32_t get_offset_of_key_0() { return static_cast<int32_t>(offsetof(U3CReadU3Ec__AnonStorey2_t3610278525, ___key_0)); }
	inline String_t* get_key_0() const { return ___key_0; }
	inline String_t** get_address_of_key_0() { return &___key_0; }
	inline void set_key_0(String_t* value)
	{
		___key_0 = value;
		Il2CppCodeGenWriteBarrier((&___key_0), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__refU243_1() { return static_cast<int32_t>(offsetof(U3CReadU3Ec__AnonStorey2_t3610278525, ___U3CU3Ef__refU243_1)); }
	inline U3CReadU3Ec__AnonStorey3_t881395170 * get_U3CU3Ef__refU243_1() const { return ___U3CU3Ef__refU243_1; }
	inline U3CReadU3Ec__AnonStorey3_t881395170 ** get_address_of_U3CU3Ef__refU243_1() { return &___U3CU3Ef__refU243_1; }
	inline void set_U3CU3Ef__refU243_1(U3CReadU3Ec__AnonStorey3_t881395170 * value)
	{
		___U3CU3Ef__refU243_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__refU243_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CREADU3EC__ANONSTOREY2_T3610278525_H
#ifndef U3CBOOTINITU3EC__ANONSTOREY0_T2996028236_H
#define U3CBOOTINITU3EC__ANONSTOREY0_T2996028236_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.AdvCharacterSettingData/<BootInit>c__AnonStorey0
struct  U3CBootInitU3Ec__AnonStorey0_t2996028236  : public RuntimeObject
{
public:
	// Utage.AdvSettingDataManager Utage.AdvCharacterSettingData/<BootInit>c__AnonStorey0::dataManager
	AdvSettingDataManager_t931476416 * ___dataManager_0;
	// Utage.AdvCharacterSettingData Utage.AdvCharacterSettingData/<BootInit>c__AnonStorey0::$this
	AdvCharacterSettingData_t1671945242 * ___U24this_1;

public:
	inline static int32_t get_offset_of_dataManager_0() { return static_cast<int32_t>(offsetof(U3CBootInitU3Ec__AnonStorey0_t2996028236, ___dataManager_0)); }
	inline AdvSettingDataManager_t931476416 * get_dataManager_0() const { return ___dataManager_0; }
	inline AdvSettingDataManager_t931476416 ** get_address_of_dataManager_0() { return &___dataManager_0; }
	inline void set_dataManager_0(AdvSettingDataManager_t931476416 * value)
	{
		___dataManager_0 = value;
		Il2CppCodeGenWriteBarrier((&___dataManager_0), value);
	}

	inline static int32_t get_offset_of_U24this_1() { return static_cast<int32_t>(offsetof(U3CBootInitU3Ec__AnonStorey0_t2996028236, ___U24this_1)); }
	inline AdvCharacterSettingData_t1671945242 * get_U24this_1() const { return ___U24this_1; }
	inline AdvCharacterSettingData_t1671945242 ** get_address_of_U24this_1() { return &___U24this_1; }
	inline void set_U24this_1(AdvCharacterSettingData_t1671945242 * value)
	{
		___U24this_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CBOOTINITU3EC__ANONSTOREY0_T2996028236_H
#ifndef ADVPARAMSTRUCTTBL_T994067203_H
#define ADVPARAMSTRUCTTBL_T994067203_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.AdvParamStructTbl
struct  AdvParamStructTbl_t994067203  : public RuntimeObject
{
public:
	// System.Collections.Generic.Dictionary`2<System.String,Utage.AdvParamStruct> Utage.AdvParamStructTbl::tbl
	Dictionary_2_t3811166195 * ___tbl_0;

public:
	inline static int32_t get_offset_of_tbl_0() { return static_cast<int32_t>(offsetof(AdvParamStructTbl_t994067203, ___tbl_0)); }
	inline Dictionary_2_t3811166195 * get_tbl_0() const { return ___tbl_0; }
	inline Dictionary_2_t3811166195 ** get_address_of_tbl_0() { return &___tbl_0; }
	inline void set_tbl_0(Dictionary_2_t3811166195 * value)
	{
		___tbl_0 = value;
		Il2CppCodeGenWriteBarrier((&___tbl_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ADVPARAMSTRUCTTBL_T994067203_H
#ifndef U3CFINDU3EC__ANONSTOREY0_T2022056998_H
#define U3CFINDU3EC__ANONSTOREY0_T2022056998_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.AdvAnimationSetting/<Find>c__AnonStorey0
struct  U3CFindU3Ec__AnonStorey0_t2022056998  : public RuntimeObject
{
public:
	// System.String Utage.AdvAnimationSetting/<Find>c__AnonStorey0::name
	String_t* ___name_0;

public:
	inline static int32_t get_offset_of_name_0() { return static_cast<int32_t>(offsetof(U3CFindU3Ec__AnonStorey0_t2022056998, ___name_0)); }
	inline String_t* get_name_0() const { return ___name_0; }
	inline String_t** get_address_of_name_0() { return &___name_0; }
	inline void set_name_0(String_t* value)
	{
		___name_0 = value;
		Il2CppCodeGenWriteBarrier((&___name_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CFINDU3EC__ANONSTOREY0_T2022056998_H
#ifndef U3CTRYADDCHAPTERU3EC__ANONSTOREY0_T2946455296_H
#define U3CTRYADDCHAPTERU3EC__ANONSTOREY0_T2946455296_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.AdvImportScenarios/<TryAddChapter>c__AnonStorey0
struct  U3CTryAddChapterU3Ec__AnonStorey0_t2946455296  : public RuntimeObject
{
public:
	// Utage.AdvChapterData Utage.AdvImportScenarios/<TryAddChapter>c__AnonStorey0::chapterData
	AdvChapterData_t685691324 * ___chapterData_0;

public:
	inline static int32_t get_offset_of_chapterData_0() { return static_cast<int32_t>(offsetof(U3CTryAddChapterU3Ec__AnonStorey0_t2946455296, ___chapterData_0)); }
	inline AdvChapterData_t685691324 * get_chapterData_0() const { return ___chapterData_0; }
	inline AdvChapterData_t685691324 ** get_address_of_chapterData_0() { return &___chapterData_0; }
	inline void set_chapterData_0(AdvChapterData_t685691324 * value)
	{
		___chapterData_0 = value;
		Il2CppCodeGenWriteBarrier((&___chapterData_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CTRYADDCHAPTERU3EC__ANONSTOREY0_T2946455296_H
#ifndef ADVVOICESETTING_T1784182553_H
#define ADVVOICESETTING_T1784182553_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.AdvVoiceSetting
struct  AdvVoiceSetting_t1784182553  : public RuntimeObject
{
public:
	// Utage.StringGridRow Utage.AdvVoiceSetting::<RowData>k__BackingField
	StringGridRow_t4193237197 * ___U3CRowDataU3Ek__BackingField_0;

public:
	inline static int32_t get_offset_of_U3CRowDataU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(AdvVoiceSetting_t1784182553, ___U3CRowDataU3Ek__BackingField_0)); }
	inline StringGridRow_t4193237197 * get_U3CRowDataU3Ek__BackingField_0() const { return ___U3CRowDataU3Ek__BackingField_0; }
	inline StringGridRow_t4193237197 ** get_address_of_U3CRowDataU3Ek__BackingField_0() { return &___U3CRowDataU3Ek__BackingField_0; }
	inline void set_U3CRowDataU3Ek__BackingField_0(StringGridRow_t4193237197 * value)
	{
		___U3CRowDataU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CRowDataU3Ek__BackingField_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ADVVOICESETTING_T1784182553_H
#ifndef U3CBOOTINITU3EC__ANONSTOREY0_T2854516436_H
#define U3CBOOTINITU3EC__ANONSTOREY0_T2854516436_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.AdvVideoSettingData/<BootInit>c__AnonStorey0
struct  U3CBootInitU3Ec__AnonStorey0_t2854516436  : public RuntimeObject
{
public:
	// Utage.AdvSettingDataManager Utage.AdvVideoSettingData/<BootInit>c__AnonStorey0::dataManager
	AdvSettingDataManager_t931476416 * ___dataManager_0;
	// Utage.AdvVideoSettingData Utage.AdvVideoSettingData/<BootInit>c__AnonStorey0::$this
	AdvVideoSettingData_t2742317570 * ___U24this_1;

public:
	inline static int32_t get_offset_of_dataManager_0() { return static_cast<int32_t>(offsetof(U3CBootInitU3Ec__AnonStorey0_t2854516436, ___dataManager_0)); }
	inline AdvSettingDataManager_t931476416 * get_dataManager_0() const { return ___dataManager_0; }
	inline AdvSettingDataManager_t931476416 ** get_address_of_dataManager_0() { return &___dataManager_0; }
	inline void set_dataManager_0(AdvSettingDataManager_t931476416 * value)
	{
		___dataManager_0 = value;
		Il2CppCodeGenWriteBarrier((&___dataManager_0), value);
	}

	inline static int32_t get_offset_of_U24this_1() { return static_cast<int32_t>(offsetof(U3CBootInitU3Ec__AnonStorey0_t2854516436, ___U24this_1)); }
	inline AdvVideoSettingData_t2742317570 * get_U24this_1() const { return ___U24this_1; }
	inline AdvVideoSettingData_t2742317570 ** get_address_of_U24this_1() { return &___U24this_1; }
	inline void set_U24this_1(AdvVideoSettingData_t2742317570 * value)
	{
		___U24this_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CBOOTINITU3EC__ANONSTOREY0_T2854516436_H
#ifndef ADVCOLUMNNAMEEXTENTISON_T2488705969_H
#define ADVCOLUMNNAMEEXTENTISON_T2488705969_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.AdvColumnNameExtentison
struct  AdvColumnNameExtentison_t2488705969  : public RuntimeObject
{
public:

public:
};

struct AdvColumnNameExtentison_t2488705969_StaticFields
{
public:
	// System.String[] Utage.AdvColumnNameExtentison::names
	StringU5BU5D_t1642385972* ___names_0;

public:
	inline static int32_t get_offset_of_names_0() { return static_cast<int32_t>(offsetof(AdvColumnNameExtentison_t2488705969_StaticFields, ___names_0)); }
	inline StringU5BU5D_t1642385972* get_names_0() const { return ___names_0; }
	inline StringU5BU5D_t1642385972** get_address_of_names_0() { return &___names_0; }
	inline void set_names_0(StringU5BU5D_t1642385972* value)
	{
		___names_0 = value;
		Il2CppCodeGenWriteBarrier((&___names_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ADVCOLUMNNAMEEXTENTISON_T2488705969_H
#ifndef VALUETYPE_T3507792607_H
#define VALUETYPE_T3507792607_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ValueType
struct  ValueType_t3507792607  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t3507792607_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t3507792607_marshaled_com
{
};
#endif // VALUETYPE_T3507792607_H
#ifndef ADVCOMMANDSETTING_T396459972_H
#define ADVCOMMANDSETTING_T396459972_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.AdvCommandSetting
struct  AdvCommandSetting_t396459972  : public RuntimeObject
{
public:
	// Utage.AdvCommand Utage.AdvCommandSetting::<Command>k__BackingField
	AdvCommand_t2859960984 * ___U3CCommandU3Ek__BackingField_0;

public:
	inline static int32_t get_offset_of_U3CCommandU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(AdvCommandSetting_t396459972, ___U3CCommandU3Ek__BackingField_0)); }
	inline AdvCommand_t2859960984 * get_U3CCommandU3Ek__BackingField_0() const { return ___U3CCommandU3Ek__BackingField_0; }
	inline AdvCommand_t2859960984 ** get_address_of_U3CCommandU3Ek__BackingField_0() { return &___U3CCommandU3Ek__BackingField_0; }
	inline void set_U3CCommandU3Ek__BackingField_0(AdvCommand_t2859960984 * value)
	{
		___U3CCommandU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CCommandU3Ek__BackingField_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ADVCOMMANDSETTING_T396459972_H
#ifndef ADVSETTINGDICTINOAYITEMBASE_T203751633_H
#define ADVSETTINGDICTINOAYITEMBASE_T203751633_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.AdvSettingDictinoayItemBase
struct  AdvSettingDictinoayItemBase_t203751633  : public RuntimeObject
{
public:
	// System.String Utage.AdvSettingDictinoayItemBase::key
	String_t* ___key_0;
	// Utage.StringGridRow Utage.AdvSettingDictinoayItemBase::<RowData>k__BackingField
	StringGridRow_t4193237197 * ___U3CRowDataU3Ek__BackingField_1;

public:
	inline static int32_t get_offset_of_key_0() { return static_cast<int32_t>(offsetof(AdvSettingDictinoayItemBase_t203751633, ___key_0)); }
	inline String_t* get_key_0() const { return ___key_0; }
	inline String_t** get_address_of_key_0() { return &___key_0; }
	inline void set_key_0(String_t* value)
	{
		___key_0 = value;
		Il2CppCodeGenWriteBarrier((&___key_0), value);
	}

	inline static int32_t get_offset_of_U3CRowDataU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(AdvSettingDictinoayItemBase_t203751633, ___U3CRowDataU3Ek__BackingField_1)); }
	inline StringGridRow_t4193237197 * get_U3CRowDataU3Ek__BackingField_1() const { return ___U3CRowDataU3Ek__BackingField_1; }
	inline StringGridRow_t4193237197 ** get_address_of_U3CRowDataU3Ek__BackingField_1() { return &___U3CRowDataU3Ek__BackingField_1; }
	inline void set_U3CRowDataU3Ek__BackingField_1(StringGridRow_t4193237197 * value)
	{
		___U3CRowDataU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CRowDataU3Ek__BackingField_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ADVSETTINGDICTINOAYITEMBASE_T203751633_H
#ifndef ADVSETTINGBASE_T3183557912_H
#define ADVSETTINGBASE_T3183557912_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.AdvSettingBase
struct  AdvSettingBase_t3183557912  : public RuntimeObject
{
public:
	// System.Collections.Generic.List`1<Utage.StringGrid> Utage.AdvSettingBase::gridList
	List_1_t1241274811 * ___gridList_0;

public:
	inline static int32_t get_offset_of_gridList_0() { return static_cast<int32_t>(offsetof(AdvSettingBase_t3183557912, ___gridList_0)); }
	inline List_1_t1241274811 * get_gridList_0() const { return ___gridList_0; }
	inline List_1_t1241274811 ** get_address_of_gridList_0() { return &___gridList_0; }
	inline void set_gridList_0(List_1_t1241274811 * value)
	{
		___gridList_0 = value;
		Il2CppCodeGenWriteBarrier((&___gridList_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ADVSETTINGBASE_T3183557912_H
#ifndef ADVANIMATIONDATA_T3431567515_H
#define ADVANIMATIONDATA_T3431567515_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.AdvAnimationData
struct  AdvAnimationData_t3431567515  : public RuntimeObject
{
public:
	// UnityEngine.AnimationClip Utage.AdvAnimationData::<Clip>k__BackingField
	AnimationClip_t3510324950 * ___U3CClipU3Ek__BackingField_0;

public:
	inline static int32_t get_offset_of_U3CClipU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(AdvAnimationData_t3431567515, ___U3CClipU3Ek__BackingField_0)); }
	inline AnimationClip_t3510324950 * get_U3CClipU3Ek__BackingField_0() const { return ___U3CClipU3Ek__BackingField_0; }
	inline AnimationClip_t3510324950 ** get_address_of_U3CClipU3Ek__BackingField_0() { return &___U3CClipU3Ek__BackingField_0; }
	inline void set_U3CClipU3Ek__BackingField_0(AnimationClip_t3510324950 * value)
	{
		___U3CClipU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CClipU3Ek__BackingField_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ADVANIMATIONDATA_T3431567515_H
#ifndef ADVSHEETPARSER_T3206602165_H
#define ADVSHEETPARSER_T3206602165_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.AdvSheetParser
struct  AdvSheetParser_t3206602165  : public RuntimeObject
{
public:

public:
};

struct AdvSheetParser_t3206602165_StaticFields
{
public:
	// System.Text.RegularExpressions.Regex Utage.AdvSheetParser::SheetNameRegex
	Regex_t1803876613 * ___SheetNameRegex_12;
	// System.Text.RegularExpressions.Regex Utage.AdvSheetParser::AnimationSheetNameRegix
	Regex_t1803876613 * ___AnimationSheetNameRegix_13;
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> Utage.AdvSheetParser::<>f__switch$map1
	Dictionary_2_t3986656710 * ___U3CU3Ef__switchU24map1_14;
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> Utage.AdvSheetParser::<>f__switch$map2
	Dictionary_2_t3986656710 * ___U3CU3Ef__switchU24map2_15;

public:
	inline static int32_t get_offset_of_SheetNameRegex_12() { return static_cast<int32_t>(offsetof(AdvSheetParser_t3206602165_StaticFields, ___SheetNameRegex_12)); }
	inline Regex_t1803876613 * get_SheetNameRegex_12() const { return ___SheetNameRegex_12; }
	inline Regex_t1803876613 ** get_address_of_SheetNameRegex_12() { return &___SheetNameRegex_12; }
	inline void set_SheetNameRegex_12(Regex_t1803876613 * value)
	{
		___SheetNameRegex_12 = value;
		Il2CppCodeGenWriteBarrier((&___SheetNameRegex_12), value);
	}

	inline static int32_t get_offset_of_AnimationSheetNameRegix_13() { return static_cast<int32_t>(offsetof(AdvSheetParser_t3206602165_StaticFields, ___AnimationSheetNameRegix_13)); }
	inline Regex_t1803876613 * get_AnimationSheetNameRegix_13() const { return ___AnimationSheetNameRegix_13; }
	inline Regex_t1803876613 ** get_address_of_AnimationSheetNameRegix_13() { return &___AnimationSheetNameRegix_13; }
	inline void set_AnimationSheetNameRegix_13(Regex_t1803876613 * value)
	{
		___AnimationSheetNameRegix_13 = value;
		Il2CppCodeGenWriteBarrier((&___AnimationSheetNameRegix_13), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__switchU24map1_14() { return static_cast<int32_t>(offsetof(AdvSheetParser_t3206602165_StaticFields, ___U3CU3Ef__switchU24map1_14)); }
	inline Dictionary_2_t3986656710 * get_U3CU3Ef__switchU24map1_14() const { return ___U3CU3Ef__switchU24map1_14; }
	inline Dictionary_2_t3986656710 ** get_address_of_U3CU3Ef__switchU24map1_14() { return &___U3CU3Ef__switchU24map1_14; }
	inline void set_U3CU3Ef__switchU24map1_14(Dictionary_2_t3986656710 * value)
	{
		___U3CU3Ef__switchU24map1_14 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__switchU24map1_14), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__switchU24map2_15() { return static_cast<int32_t>(offsetof(AdvSheetParser_t3206602165_StaticFields, ___U3CU3Ef__switchU24map2_15)); }
	inline Dictionary_2_t3986656710 * get_U3CU3Ef__switchU24map2_15() const { return ___U3CU3Ef__switchU24map2_15; }
	inline Dictionary_2_t3986656710 ** get_address_of_U3CU3Ef__switchU24map2_15() { return &___U3CU3Ef__switchU24map2_15; }
	inline void set_U3CU3Ef__switchU24map2_15(Dictionary_2_t3986656710 * value)
	{
		___U3CU3Ef__switchU24map2_15 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__switchU24map2_15), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ADVSHEETPARSER_T3206602165_H
#ifndef ADVSETTINGDATAMANAGER_T931476416_H
#define ADVSETTINGDATAMANAGER_T931476416_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.AdvSettingDataManager
struct  AdvSettingDataManager_t931476416  : public RuntimeObject
{
public:
	// Utage.AdvImportScenarios Utage.AdvSettingDataManager::<ImportedScenarios>k__BackingField
	AdvImportScenarios_t1226385437 * ___U3CImportedScenariosU3Ek__BackingField_0;
	// Utage.AdvBootSetting Utage.AdvSettingDataManager::bootSetting
	AdvBootSetting_t3720085301 * ___bootSetting_1;
	// Utage.AdvCharacterSetting Utage.AdvSettingDataManager::characterSetting
	AdvCharacterSetting_t2712258826 * ___characterSetting_2;
	// Utage.AdvTextureSetting Utage.AdvSettingDataManager::textureSetting
	AdvTextureSetting_t2060921594 * ___textureSetting_3;
	// Utage.AdvSoundSetting Utage.AdvSettingDataManager::soundSetting
	AdvSoundSetting_t229690022 * ___soundSetting_4;
	// Utage.AdvLayerSetting Utage.AdvSettingDataManager::layerSetting
	AdvLayerSetting_t3281222136 * ___layerSetting_5;
	// Utage.AdvParamManager Utage.AdvSettingDataManager::defaultParam
	AdvParamManager_t1816006425 * ___defaultParam_6;
	// Utage.AdvSceneGallerySetting Utage.AdvSettingDataManager::sceneGallerySetting
	AdvSceneGallerySetting_t2241589337 * ___sceneGallerySetting_7;
	// Utage.AdvLocalizeSetting Utage.AdvSettingDataManager::localizeSetting
	AdvLocalizeSetting_t467665552 * ___localizeSetting_8;
	// Utage.AdvAnimationSetting Utage.AdvSettingDataManager::animationSetting
	AdvAnimationSetting_t2232720937 * ___animationSetting_9;
	// Utage.AdvEyeBlinkSetting Utage.AdvSettingDataManager::eyeBlinkSetting
	AdvEyeBlinkSetting_t1976982958 * ___eyeBlinkSetting_10;
	// Utage.AdvLipSynchSetting Utage.AdvSettingDataManager::lipSynchSetting
	AdvLipSynchSetting_t289134997 * ___lipSynchSetting_11;
	// Utage.AdvParticleSetting Utage.AdvSettingDataManager::advParticleSetting
	AdvParticleSetting_t588078687 * ___advParticleSetting_12;
	// Utage.AdvVideoSetting Utage.AdvSettingDataManager::videoSetting
	AdvVideoSetting_t3960366418 * ___videoSetting_13;
	// System.Collections.Generic.List`1<Utage.IAdvSetting> Utage.AdvSettingDataManager::settingDataList
	List_1_t4076707138 * ___settingDataList_14;

public:
	inline static int32_t get_offset_of_U3CImportedScenariosU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(AdvSettingDataManager_t931476416, ___U3CImportedScenariosU3Ek__BackingField_0)); }
	inline AdvImportScenarios_t1226385437 * get_U3CImportedScenariosU3Ek__BackingField_0() const { return ___U3CImportedScenariosU3Ek__BackingField_0; }
	inline AdvImportScenarios_t1226385437 ** get_address_of_U3CImportedScenariosU3Ek__BackingField_0() { return &___U3CImportedScenariosU3Ek__BackingField_0; }
	inline void set_U3CImportedScenariosU3Ek__BackingField_0(AdvImportScenarios_t1226385437 * value)
	{
		___U3CImportedScenariosU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CImportedScenariosU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_bootSetting_1() { return static_cast<int32_t>(offsetof(AdvSettingDataManager_t931476416, ___bootSetting_1)); }
	inline AdvBootSetting_t3720085301 * get_bootSetting_1() const { return ___bootSetting_1; }
	inline AdvBootSetting_t3720085301 ** get_address_of_bootSetting_1() { return &___bootSetting_1; }
	inline void set_bootSetting_1(AdvBootSetting_t3720085301 * value)
	{
		___bootSetting_1 = value;
		Il2CppCodeGenWriteBarrier((&___bootSetting_1), value);
	}

	inline static int32_t get_offset_of_characterSetting_2() { return static_cast<int32_t>(offsetof(AdvSettingDataManager_t931476416, ___characterSetting_2)); }
	inline AdvCharacterSetting_t2712258826 * get_characterSetting_2() const { return ___characterSetting_2; }
	inline AdvCharacterSetting_t2712258826 ** get_address_of_characterSetting_2() { return &___characterSetting_2; }
	inline void set_characterSetting_2(AdvCharacterSetting_t2712258826 * value)
	{
		___characterSetting_2 = value;
		Il2CppCodeGenWriteBarrier((&___characterSetting_2), value);
	}

	inline static int32_t get_offset_of_textureSetting_3() { return static_cast<int32_t>(offsetof(AdvSettingDataManager_t931476416, ___textureSetting_3)); }
	inline AdvTextureSetting_t2060921594 * get_textureSetting_3() const { return ___textureSetting_3; }
	inline AdvTextureSetting_t2060921594 ** get_address_of_textureSetting_3() { return &___textureSetting_3; }
	inline void set_textureSetting_3(AdvTextureSetting_t2060921594 * value)
	{
		___textureSetting_3 = value;
		Il2CppCodeGenWriteBarrier((&___textureSetting_3), value);
	}

	inline static int32_t get_offset_of_soundSetting_4() { return static_cast<int32_t>(offsetof(AdvSettingDataManager_t931476416, ___soundSetting_4)); }
	inline AdvSoundSetting_t229690022 * get_soundSetting_4() const { return ___soundSetting_4; }
	inline AdvSoundSetting_t229690022 ** get_address_of_soundSetting_4() { return &___soundSetting_4; }
	inline void set_soundSetting_4(AdvSoundSetting_t229690022 * value)
	{
		___soundSetting_4 = value;
		Il2CppCodeGenWriteBarrier((&___soundSetting_4), value);
	}

	inline static int32_t get_offset_of_layerSetting_5() { return static_cast<int32_t>(offsetof(AdvSettingDataManager_t931476416, ___layerSetting_5)); }
	inline AdvLayerSetting_t3281222136 * get_layerSetting_5() const { return ___layerSetting_5; }
	inline AdvLayerSetting_t3281222136 ** get_address_of_layerSetting_5() { return &___layerSetting_5; }
	inline void set_layerSetting_5(AdvLayerSetting_t3281222136 * value)
	{
		___layerSetting_5 = value;
		Il2CppCodeGenWriteBarrier((&___layerSetting_5), value);
	}

	inline static int32_t get_offset_of_defaultParam_6() { return static_cast<int32_t>(offsetof(AdvSettingDataManager_t931476416, ___defaultParam_6)); }
	inline AdvParamManager_t1816006425 * get_defaultParam_6() const { return ___defaultParam_6; }
	inline AdvParamManager_t1816006425 ** get_address_of_defaultParam_6() { return &___defaultParam_6; }
	inline void set_defaultParam_6(AdvParamManager_t1816006425 * value)
	{
		___defaultParam_6 = value;
		Il2CppCodeGenWriteBarrier((&___defaultParam_6), value);
	}

	inline static int32_t get_offset_of_sceneGallerySetting_7() { return static_cast<int32_t>(offsetof(AdvSettingDataManager_t931476416, ___sceneGallerySetting_7)); }
	inline AdvSceneGallerySetting_t2241589337 * get_sceneGallerySetting_7() const { return ___sceneGallerySetting_7; }
	inline AdvSceneGallerySetting_t2241589337 ** get_address_of_sceneGallerySetting_7() { return &___sceneGallerySetting_7; }
	inline void set_sceneGallerySetting_7(AdvSceneGallerySetting_t2241589337 * value)
	{
		___sceneGallerySetting_7 = value;
		Il2CppCodeGenWriteBarrier((&___sceneGallerySetting_7), value);
	}

	inline static int32_t get_offset_of_localizeSetting_8() { return static_cast<int32_t>(offsetof(AdvSettingDataManager_t931476416, ___localizeSetting_8)); }
	inline AdvLocalizeSetting_t467665552 * get_localizeSetting_8() const { return ___localizeSetting_8; }
	inline AdvLocalizeSetting_t467665552 ** get_address_of_localizeSetting_8() { return &___localizeSetting_8; }
	inline void set_localizeSetting_8(AdvLocalizeSetting_t467665552 * value)
	{
		___localizeSetting_8 = value;
		Il2CppCodeGenWriteBarrier((&___localizeSetting_8), value);
	}

	inline static int32_t get_offset_of_animationSetting_9() { return static_cast<int32_t>(offsetof(AdvSettingDataManager_t931476416, ___animationSetting_9)); }
	inline AdvAnimationSetting_t2232720937 * get_animationSetting_9() const { return ___animationSetting_9; }
	inline AdvAnimationSetting_t2232720937 ** get_address_of_animationSetting_9() { return &___animationSetting_9; }
	inline void set_animationSetting_9(AdvAnimationSetting_t2232720937 * value)
	{
		___animationSetting_9 = value;
		Il2CppCodeGenWriteBarrier((&___animationSetting_9), value);
	}

	inline static int32_t get_offset_of_eyeBlinkSetting_10() { return static_cast<int32_t>(offsetof(AdvSettingDataManager_t931476416, ___eyeBlinkSetting_10)); }
	inline AdvEyeBlinkSetting_t1976982958 * get_eyeBlinkSetting_10() const { return ___eyeBlinkSetting_10; }
	inline AdvEyeBlinkSetting_t1976982958 ** get_address_of_eyeBlinkSetting_10() { return &___eyeBlinkSetting_10; }
	inline void set_eyeBlinkSetting_10(AdvEyeBlinkSetting_t1976982958 * value)
	{
		___eyeBlinkSetting_10 = value;
		Il2CppCodeGenWriteBarrier((&___eyeBlinkSetting_10), value);
	}

	inline static int32_t get_offset_of_lipSynchSetting_11() { return static_cast<int32_t>(offsetof(AdvSettingDataManager_t931476416, ___lipSynchSetting_11)); }
	inline AdvLipSynchSetting_t289134997 * get_lipSynchSetting_11() const { return ___lipSynchSetting_11; }
	inline AdvLipSynchSetting_t289134997 ** get_address_of_lipSynchSetting_11() { return &___lipSynchSetting_11; }
	inline void set_lipSynchSetting_11(AdvLipSynchSetting_t289134997 * value)
	{
		___lipSynchSetting_11 = value;
		Il2CppCodeGenWriteBarrier((&___lipSynchSetting_11), value);
	}

	inline static int32_t get_offset_of_advParticleSetting_12() { return static_cast<int32_t>(offsetof(AdvSettingDataManager_t931476416, ___advParticleSetting_12)); }
	inline AdvParticleSetting_t588078687 * get_advParticleSetting_12() const { return ___advParticleSetting_12; }
	inline AdvParticleSetting_t588078687 ** get_address_of_advParticleSetting_12() { return &___advParticleSetting_12; }
	inline void set_advParticleSetting_12(AdvParticleSetting_t588078687 * value)
	{
		___advParticleSetting_12 = value;
		Il2CppCodeGenWriteBarrier((&___advParticleSetting_12), value);
	}

	inline static int32_t get_offset_of_videoSetting_13() { return static_cast<int32_t>(offsetof(AdvSettingDataManager_t931476416, ___videoSetting_13)); }
	inline AdvVideoSetting_t3960366418 * get_videoSetting_13() const { return ___videoSetting_13; }
	inline AdvVideoSetting_t3960366418 ** get_address_of_videoSetting_13() { return &___videoSetting_13; }
	inline void set_videoSetting_13(AdvVideoSetting_t3960366418 * value)
	{
		___videoSetting_13 = value;
		Il2CppCodeGenWriteBarrier((&___videoSetting_13), value);
	}

	inline static int32_t get_offset_of_settingDataList_14() { return static_cast<int32_t>(offsetof(AdvSettingDataManager_t931476416, ___settingDataList_14)); }
	inline List_1_t4076707138 * get_settingDataList_14() const { return ___settingDataList_14; }
	inline List_1_t4076707138 ** get_address_of_settingDataList_14() { return &___settingDataList_14; }
	inline void set_settingDataList_14(List_1_t4076707138 * value)
	{
		___settingDataList_14 = value;
		Il2CppCodeGenWriteBarrier((&___settingDataList_14), value);
	}
};

struct AdvSettingDataManager_t931476416_StaticFields
{
public:
	// System.Action`1<Utage.IAdvSetting> Utage.AdvSettingDataManager::<>f__am$cache0
	Action_1_t214418092 * ___U3CU3Ef__amU24cache0_15;

public:
	inline static int32_t get_offset_of_U3CU3Ef__amU24cache0_15() { return static_cast<int32_t>(offsetof(AdvSettingDataManager_t931476416_StaticFields, ___U3CU3Ef__amU24cache0_15)); }
	inline Action_1_t214418092 * get_U3CU3Ef__amU24cache0_15() const { return ___U3CU3Ef__amU24cache0_15; }
	inline Action_1_t214418092 ** get_address_of_U3CU3Ef__amU24cache0_15() { return &___U3CU3Ef__amU24cache0_15; }
	inline void set_U3CU3Ef__amU24cache0_15(Action_1_t214418092 * value)
	{
		___U3CU3Ef__amU24cache0_15 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache0_15), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ADVSETTINGDATAMANAGER_T931476416_H
#ifndef ADVPARSER_T1629086016_H
#define ADVPARSER_T1629086016_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.AdvParser
struct  AdvParser_t1629086016  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ADVPARSER_T1629086016_H
#ifndef STRINGGRIDROWMACROED_T3994913000_H
#define STRINGGRIDROWMACROED_T3994913000_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.AdvImportScenarioSheet/StringGridRowMacroed
struct  StringGridRowMacroed_t3994913000  : public RuntimeObject
{
public:
	// Utage.StringGridRow Utage.AdvImportScenarioSheet/StringGridRowMacroed::row
	StringGridRow_t4193237197 * ___row_0;
	// Utage.AdvEntityData Utage.AdvImportScenarioSheet/StringGridRowMacroed::entityData
	AdvEntityData_t1465354496 * ___entityData_1;

public:
	inline static int32_t get_offset_of_row_0() { return static_cast<int32_t>(offsetof(StringGridRowMacroed_t3994913000, ___row_0)); }
	inline StringGridRow_t4193237197 * get_row_0() const { return ___row_0; }
	inline StringGridRow_t4193237197 ** get_address_of_row_0() { return &___row_0; }
	inline void set_row_0(StringGridRow_t4193237197 * value)
	{
		___row_0 = value;
		Il2CppCodeGenWriteBarrier((&___row_0), value);
	}

	inline static int32_t get_offset_of_entityData_1() { return static_cast<int32_t>(offsetof(StringGridRowMacroed_t3994913000, ___entityData_1)); }
	inline AdvEntityData_t1465354496 * get_entityData_1() const { return ___entityData_1; }
	inline AdvEntityData_t1465354496 ** get_address_of_entityData_1() { return &___entityData_1; }
	inline void set_entityData_1(AdvEntityData_t1465354496 * value)
	{
		___entityData_1 = value;
		Il2CppCodeGenWriteBarrier((&___entityData_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STRINGGRIDROWMACROED_T3994913000_H
#ifndef ADVCOMMAND_T2859960984_H
#define ADVCOMMAND_T2859960984_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.AdvCommand
struct  AdvCommand_t2859960984  : public RuntimeObject
{
public:
	// Utage.StringGridRow Utage.AdvCommand::<RowData>k__BackingField
	StringGridRow_t4193237197 * ___U3CRowDataU3Ek__BackingField_2;
	// Utage.AdvEntityData Utage.AdvCommand::<EntityData>k__BackingField
	AdvEntityData_t1465354496 * ___U3CEntityDataU3Ek__BackingField_3;
	// System.String Utage.AdvCommand::<Id>k__BackingField
	String_t* ___U3CIdU3Ek__BackingField_4;
	// System.Collections.Generic.List`1<Utage.AssetFile> Utage.AdvCommand::loadFileList
	List_1_t2752134388 * ___loadFileList_5;
	// Utage.AdvScenarioThread Utage.AdvCommand::<CurrentTread>k__BackingField
	AdvScenarioThread_t1270526825 * ___U3CCurrentTreadU3Ek__BackingField_6;

public:
	inline static int32_t get_offset_of_U3CRowDataU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(AdvCommand_t2859960984, ___U3CRowDataU3Ek__BackingField_2)); }
	inline StringGridRow_t4193237197 * get_U3CRowDataU3Ek__BackingField_2() const { return ___U3CRowDataU3Ek__BackingField_2; }
	inline StringGridRow_t4193237197 ** get_address_of_U3CRowDataU3Ek__BackingField_2() { return &___U3CRowDataU3Ek__BackingField_2; }
	inline void set_U3CRowDataU3Ek__BackingField_2(StringGridRow_t4193237197 * value)
	{
		___U3CRowDataU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CRowDataU3Ek__BackingField_2), value);
	}

	inline static int32_t get_offset_of_U3CEntityDataU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(AdvCommand_t2859960984, ___U3CEntityDataU3Ek__BackingField_3)); }
	inline AdvEntityData_t1465354496 * get_U3CEntityDataU3Ek__BackingField_3() const { return ___U3CEntityDataU3Ek__BackingField_3; }
	inline AdvEntityData_t1465354496 ** get_address_of_U3CEntityDataU3Ek__BackingField_3() { return &___U3CEntityDataU3Ek__BackingField_3; }
	inline void set_U3CEntityDataU3Ek__BackingField_3(AdvEntityData_t1465354496 * value)
	{
		___U3CEntityDataU3Ek__BackingField_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CEntityDataU3Ek__BackingField_3), value);
	}

	inline static int32_t get_offset_of_U3CIdU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(AdvCommand_t2859960984, ___U3CIdU3Ek__BackingField_4)); }
	inline String_t* get_U3CIdU3Ek__BackingField_4() const { return ___U3CIdU3Ek__BackingField_4; }
	inline String_t** get_address_of_U3CIdU3Ek__BackingField_4() { return &___U3CIdU3Ek__BackingField_4; }
	inline void set_U3CIdU3Ek__BackingField_4(String_t* value)
	{
		___U3CIdU3Ek__BackingField_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CIdU3Ek__BackingField_4), value);
	}

	inline static int32_t get_offset_of_loadFileList_5() { return static_cast<int32_t>(offsetof(AdvCommand_t2859960984, ___loadFileList_5)); }
	inline List_1_t2752134388 * get_loadFileList_5() const { return ___loadFileList_5; }
	inline List_1_t2752134388 ** get_address_of_loadFileList_5() { return &___loadFileList_5; }
	inline void set_loadFileList_5(List_1_t2752134388 * value)
	{
		___loadFileList_5 = value;
		Il2CppCodeGenWriteBarrier((&___loadFileList_5), value);
	}

	inline static int32_t get_offset_of_U3CCurrentTreadU3Ek__BackingField_6() { return static_cast<int32_t>(offsetof(AdvCommand_t2859960984, ___U3CCurrentTreadU3Ek__BackingField_6)); }
	inline AdvScenarioThread_t1270526825 * get_U3CCurrentTreadU3Ek__BackingField_6() const { return ___U3CCurrentTreadU3Ek__BackingField_6; }
	inline AdvScenarioThread_t1270526825 ** get_address_of_U3CCurrentTreadU3Ek__BackingField_6() { return &___U3CCurrentTreadU3Ek__BackingField_6; }
	inline void set_U3CCurrentTreadU3Ek__BackingField_6(AdvScenarioThread_t1270526825 * value)
	{
		___U3CCurrentTreadU3Ek__BackingField_6 = value;
		Il2CppCodeGenWriteBarrier((&___U3CCurrentTreadU3Ek__BackingField_6), value);
	}
};

struct AdvCommand_t2859960984_StaticFields
{
public:
	// System.Boolean Utage.AdvCommand::isEditorErrorCheck
	bool ___isEditorErrorCheck_0;
	// System.Boolean Utage.AdvCommand::<IsEditorErrorCheckWaitType>k__BackingField
	bool ___U3CIsEditorErrorCheckWaitTypeU3Ek__BackingField_1;

public:
	inline static int32_t get_offset_of_isEditorErrorCheck_0() { return static_cast<int32_t>(offsetof(AdvCommand_t2859960984_StaticFields, ___isEditorErrorCheck_0)); }
	inline bool get_isEditorErrorCheck_0() const { return ___isEditorErrorCheck_0; }
	inline bool* get_address_of_isEditorErrorCheck_0() { return &___isEditorErrorCheck_0; }
	inline void set_isEditorErrorCheck_0(bool value)
	{
		___isEditorErrorCheck_0 = value;
	}

	inline static int32_t get_offset_of_U3CIsEditorErrorCheckWaitTypeU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(AdvCommand_t2859960984_StaticFields, ___U3CIsEditorErrorCheckWaitTypeU3Ek__BackingField_1)); }
	inline bool get_U3CIsEditorErrorCheckWaitTypeU3Ek__BackingField_1() const { return ___U3CIsEditorErrorCheckWaitTypeU3Ek__BackingField_1; }
	inline bool* get_address_of_U3CIsEditorErrorCheckWaitTypeU3Ek__BackingField_1() { return &___U3CIsEditorErrorCheckWaitTypeU3Ek__BackingField_1; }
	inline void set_U3CIsEditorErrorCheckWaitTypeU3Ek__BackingField_1(bool value)
	{
		___U3CIsEditorErrorCheckWaitTypeU3Ek__BackingField_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ADVCOMMAND_T2859960984_H
#ifndef ADVPARAMMANAGER_T1816006425_H
#define ADVPARAMMANAGER_T1816006425_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.AdvParamManager
struct  AdvParamManager_t1816006425  : public AdvSettingBase_t3183557912
{
public:
	// System.Boolean Utage.AdvParamManager::<IsInit>k__BackingField
	bool ___U3CIsInitU3Ek__BackingField_4;
	// System.Collections.Generic.Dictionary`2<System.String,Utage.AdvParamStructTbl> Utage.AdvParamManager::structTbl
	Dictionary_2_t2908846465 * ___structTbl_5;
	// System.Boolean Utage.AdvParamManager::<HasChangedSystemParam>k__BackingField
	bool ___U3CHasChangedSystemParamU3Ek__BackingField_6;
	// Utage.AdvParamManager Utage.AdvParamManager::<DefaultParameter>k__BackingField
	AdvParamManager_t1816006425 * ___U3CDefaultParameterU3Ek__BackingField_7;
	// Utage.AdvParamManager/IoInerface Utage.AdvParamManager::systemData
	IoInerface_t3434613093 * ___systemData_9;
	// Utage.AdvParamManager/IoInerface Utage.AdvParamManager::defaultData
	IoInerface_t3434613093 * ___defaultData_10;

public:
	inline static int32_t get_offset_of_U3CIsInitU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(AdvParamManager_t1816006425, ___U3CIsInitU3Ek__BackingField_4)); }
	inline bool get_U3CIsInitU3Ek__BackingField_4() const { return ___U3CIsInitU3Ek__BackingField_4; }
	inline bool* get_address_of_U3CIsInitU3Ek__BackingField_4() { return &___U3CIsInitU3Ek__BackingField_4; }
	inline void set_U3CIsInitU3Ek__BackingField_4(bool value)
	{
		___U3CIsInitU3Ek__BackingField_4 = value;
	}

	inline static int32_t get_offset_of_structTbl_5() { return static_cast<int32_t>(offsetof(AdvParamManager_t1816006425, ___structTbl_5)); }
	inline Dictionary_2_t2908846465 * get_structTbl_5() const { return ___structTbl_5; }
	inline Dictionary_2_t2908846465 ** get_address_of_structTbl_5() { return &___structTbl_5; }
	inline void set_structTbl_5(Dictionary_2_t2908846465 * value)
	{
		___structTbl_5 = value;
		Il2CppCodeGenWriteBarrier((&___structTbl_5), value);
	}

	inline static int32_t get_offset_of_U3CHasChangedSystemParamU3Ek__BackingField_6() { return static_cast<int32_t>(offsetof(AdvParamManager_t1816006425, ___U3CHasChangedSystemParamU3Ek__BackingField_6)); }
	inline bool get_U3CHasChangedSystemParamU3Ek__BackingField_6() const { return ___U3CHasChangedSystemParamU3Ek__BackingField_6; }
	inline bool* get_address_of_U3CHasChangedSystemParamU3Ek__BackingField_6() { return &___U3CHasChangedSystemParamU3Ek__BackingField_6; }
	inline void set_U3CHasChangedSystemParamU3Ek__BackingField_6(bool value)
	{
		___U3CHasChangedSystemParamU3Ek__BackingField_6 = value;
	}

	inline static int32_t get_offset_of_U3CDefaultParameterU3Ek__BackingField_7() { return static_cast<int32_t>(offsetof(AdvParamManager_t1816006425, ___U3CDefaultParameterU3Ek__BackingField_7)); }
	inline AdvParamManager_t1816006425 * get_U3CDefaultParameterU3Ek__BackingField_7() const { return ___U3CDefaultParameterU3Ek__BackingField_7; }
	inline AdvParamManager_t1816006425 ** get_address_of_U3CDefaultParameterU3Ek__BackingField_7() { return &___U3CDefaultParameterU3Ek__BackingField_7; }
	inline void set_U3CDefaultParameterU3Ek__BackingField_7(AdvParamManager_t1816006425 * value)
	{
		___U3CDefaultParameterU3Ek__BackingField_7 = value;
		Il2CppCodeGenWriteBarrier((&___U3CDefaultParameterU3Ek__BackingField_7), value);
	}

	inline static int32_t get_offset_of_systemData_9() { return static_cast<int32_t>(offsetof(AdvParamManager_t1816006425, ___systemData_9)); }
	inline IoInerface_t3434613093 * get_systemData_9() const { return ___systemData_9; }
	inline IoInerface_t3434613093 ** get_address_of_systemData_9() { return &___systemData_9; }
	inline void set_systemData_9(IoInerface_t3434613093 * value)
	{
		___systemData_9 = value;
		Il2CppCodeGenWriteBarrier((&___systemData_9), value);
	}

	inline static int32_t get_offset_of_defaultData_10() { return static_cast<int32_t>(offsetof(AdvParamManager_t1816006425, ___defaultData_10)); }
	inline IoInerface_t3434613093 * get_defaultData_10() const { return ___defaultData_10; }
	inline IoInerface_t3434613093 ** get_address_of_defaultData_10() { return &___defaultData_10; }
	inline void set_defaultData_10(IoInerface_t3434613093 * value)
	{
		___defaultData_10 = value;
		Il2CppCodeGenWriteBarrier((&___defaultData_10), value);
	}
};

struct AdvParamManager_t1816006425_StaticFields
{
public:
	// System.Text.RegularExpressions.Regex Utage.AdvParamManager::KeyRegix
	Regex_t1803876613 * ___KeyRegix_3;

public:
	inline static int32_t get_offset_of_KeyRegix_3() { return static_cast<int32_t>(offsetof(AdvParamManager_t1816006425_StaticFields, ___KeyRegix_3)); }
	inline Regex_t1803876613 * get_KeyRegix_3() const { return ___KeyRegix_3; }
	inline Regex_t1803876613 ** get_address_of_KeyRegix_3() { return &___KeyRegix_3; }
	inline void set_KeyRegix_3(Regex_t1803876613 * value)
	{
		___KeyRegix_3 = value;
		Il2CppCodeGenWriteBarrier((&___KeyRegix_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ADVPARAMMANAGER_T1816006425_H
#ifndef ADVVIDEOSETTINGDATA_T2742317570_H
#define ADVVIDEOSETTINGDATA_T2742317570_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.AdvVideoSettingData
struct  AdvVideoSettingData_t2742317570  : public AdvSettingDictinoayItemBase_t203751633
{
public:
	// Utage.AdvGraphicInfo Utage.AdvVideoSettingData::graphic
	AdvGraphicInfo_t3545565645 * ___graphic_2;

public:
	inline static int32_t get_offset_of_graphic_2() { return static_cast<int32_t>(offsetof(AdvVideoSettingData_t2742317570, ___graphic_2)); }
	inline AdvGraphicInfo_t3545565645 * get_graphic_2() const { return ___graphic_2; }
	inline AdvGraphicInfo_t3545565645 ** get_address_of_graphic_2() { return &___graphic_2; }
	inline void set_graphic_2(AdvGraphicInfo_t3545565645 * value)
	{
		___graphic_2 = value;
		Il2CppCodeGenWriteBarrier((&___graphic_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ADVVIDEOSETTINGDATA_T2742317570_H
#ifndef KEYVALUEPAIR_2_T2218637223_H
#define KEYVALUEPAIR_2_T2218637223_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.KeyValuePair`2<System.String,Utage.AdvScenarioData>
struct  KeyValuePair_2_t2218637223 
{
public:
	// TKey System.Collections.Generic.KeyValuePair`2::key
	String_t* ___key_0;
	// TValue System.Collections.Generic.KeyValuePair`2::value
	AdvScenarioData_t2546512739 * ___value_1;

public:
	inline static int32_t get_offset_of_key_0() { return static_cast<int32_t>(offsetof(KeyValuePair_2_t2218637223, ___key_0)); }
	inline String_t* get_key_0() const { return ___key_0; }
	inline String_t** get_address_of_key_0() { return &___key_0; }
	inline void set_key_0(String_t* value)
	{
		___key_0 = value;
		Il2CppCodeGenWriteBarrier((&___key_0), value);
	}

	inline static int32_t get_offset_of_value_1() { return static_cast<int32_t>(offsetof(KeyValuePair_2_t2218637223, ___value_1)); }
	inline AdvScenarioData_t2546512739 * get_value_1() const { return ___value_1; }
	inline AdvScenarioData_t2546512739 ** get_address_of_value_1() { return &___value_1; }
	inline void set_value_1(AdvScenarioData_t2546512739 * value)
	{
		___value_1 = value;
		Il2CppCodeGenWriteBarrier((&___value_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // KEYVALUEPAIR_2_T2218637223_H
#ifndef ADVSETTINGDATADICTINOAYBASE_1_T1027612555_H
#define ADVSETTINGDATADICTINOAYBASE_1_T1027612555_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.AdvSettingDataDictinoayBase`1<Utage.AdvSceneGallerySettingData>
struct  AdvSettingDataDictinoayBase_1_t1027612555  : public AdvSettingBase_t3183557912
{
public:
	// System.Collections.Generic.List`1<T> Utage.AdvSettingDataDictinoayBase`1::<List>k__BackingField
	List_1_t649724181 * ___U3CListU3Ek__BackingField_1;
	// System.Collections.Generic.Dictionary`2<System.String,T> Utage.AdvSettingDataDictinoayBase`1::<Dictionary>k__BackingField
	Dictionary_2_t3195382311 * ___U3CDictionaryU3Ek__BackingField_2;

public:
	inline static int32_t get_offset_of_U3CListU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(AdvSettingDataDictinoayBase_1_t1027612555, ___U3CListU3Ek__BackingField_1)); }
	inline List_1_t649724181 * get_U3CListU3Ek__BackingField_1() const { return ___U3CListU3Ek__BackingField_1; }
	inline List_1_t649724181 ** get_address_of_U3CListU3Ek__BackingField_1() { return &___U3CListU3Ek__BackingField_1; }
	inline void set_U3CListU3Ek__BackingField_1(List_1_t649724181 * value)
	{
		___U3CListU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CListU3Ek__BackingField_1), value);
	}

	inline static int32_t get_offset_of_U3CDictionaryU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(AdvSettingDataDictinoayBase_1_t1027612555, ___U3CDictionaryU3Ek__BackingField_2)); }
	inline Dictionary_2_t3195382311 * get_U3CDictionaryU3Ek__BackingField_2() const { return ___U3CDictionaryU3Ek__BackingField_2; }
	inline Dictionary_2_t3195382311 ** get_address_of_U3CDictionaryU3Ek__BackingField_2() { return &___U3CDictionaryU3Ek__BackingField_2; }
	inline void set_U3CDictionaryU3Ek__BackingField_2(Dictionary_2_t3195382311 * value)
	{
		___U3CDictionaryU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CDictionaryU3Ek__BackingField_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ADVSETTINGDATADICTINOAYBASE_1_T1027612555_H
#ifndef ADVSETTINGDATADICTINOAYBASE_1_T2882400728_H
#define ADVSETTINGDATADICTINOAYBASE_1_T2882400728_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.AdvSettingDataDictinoayBase`1<Utage.AdvSoundSettingData>
struct  AdvSettingDataDictinoayBase_1_t2882400728  : public AdvSettingBase_t3183557912
{
public:
	// System.Collections.Generic.List`1<T> Utage.AdvSettingDataDictinoayBase`1::<List>k__BackingField
	List_1_t2504512354 * ___U3CListU3Ek__BackingField_1;
	// System.Collections.Generic.Dictionary`2<System.String,T> Utage.AdvSettingDataDictinoayBase`1::<Dictionary>k__BackingField
	Dictionary_2_t755203188 * ___U3CDictionaryU3Ek__BackingField_2;

public:
	inline static int32_t get_offset_of_U3CListU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(AdvSettingDataDictinoayBase_1_t2882400728, ___U3CListU3Ek__BackingField_1)); }
	inline List_1_t2504512354 * get_U3CListU3Ek__BackingField_1() const { return ___U3CListU3Ek__BackingField_1; }
	inline List_1_t2504512354 ** get_address_of_U3CListU3Ek__BackingField_1() { return &___U3CListU3Ek__BackingField_1; }
	inline void set_U3CListU3Ek__BackingField_1(List_1_t2504512354 * value)
	{
		___U3CListU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CListU3Ek__BackingField_1), value);
	}

	inline static int32_t get_offset_of_U3CDictionaryU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(AdvSettingDataDictinoayBase_1_t2882400728, ___U3CDictionaryU3Ek__BackingField_2)); }
	inline Dictionary_2_t755203188 * get_U3CDictionaryU3Ek__BackingField_2() const { return ___U3CDictionaryU3Ek__BackingField_2; }
	inline Dictionary_2_t755203188 ** get_address_of_U3CDictionaryU3Ek__BackingField_2() { return &___U3CDictionaryU3Ek__BackingField_2; }
	inline void set_U3CDictionaryU3Ek__BackingField_2(Dictionary_2_t755203188 * value)
	{
		___U3CDictionaryU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CDictionaryU3Ek__BackingField_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ADVSETTINGDATADICTINOAYBASE_1_T2882400728_H
#ifndef ADVSETTINGDATADICTINOAYBASE_1_T2723021772_H
#define ADVSETTINGDATADICTINOAYBASE_1_T2723021772_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.AdvSettingDataDictinoayBase`1<Utage.AdvTextureSettingData>
struct  AdvSettingDataDictinoayBase_1_t2723021772  : public AdvSettingBase_t3183557912
{
public:
	// System.Collections.Generic.List`1<T> Utage.AdvSettingDataDictinoayBase`1::<List>k__BackingField
	List_1_t2345133398 * ___U3CListU3Ek__BackingField_1;
	// System.Collections.Generic.Dictionary`2<System.String,T> Utage.AdvSettingDataDictinoayBase`1::<Dictionary>k__BackingField
	Dictionary_2_t595824232 * ___U3CDictionaryU3Ek__BackingField_2;

public:
	inline static int32_t get_offset_of_U3CListU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(AdvSettingDataDictinoayBase_1_t2723021772, ___U3CListU3Ek__BackingField_1)); }
	inline List_1_t2345133398 * get_U3CListU3Ek__BackingField_1() const { return ___U3CListU3Ek__BackingField_1; }
	inline List_1_t2345133398 ** get_address_of_U3CListU3Ek__BackingField_1() { return &___U3CListU3Ek__BackingField_1; }
	inline void set_U3CListU3Ek__BackingField_1(List_1_t2345133398 * value)
	{
		___U3CListU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CListU3Ek__BackingField_1), value);
	}

	inline static int32_t get_offset_of_U3CDictionaryU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(AdvSettingDataDictinoayBase_1_t2723021772, ___U3CDictionaryU3Ek__BackingField_2)); }
	inline Dictionary_2_t595824232 * get_U3CDictionaryU3Ek__BackingField_2() const { return ___U3CDictionaryU3Ek__BackingField_2; }
	inline Dictionary_2_t595824232 ** get_address_of_U3CDictionaryU3Ek__BackingField_2() { return &___U3CDictionaryU3Ek__BackingField_2; }
	inline void set_U3CDictionaryU3Ek__BackingField_2(Dictionary_2_t595824232 * value)
	{
		___U3CDictionaryU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CDictionaryU3Ek__BackingField_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ADVSETTINGDATADICTINOAYBASE_1_T2723021772_H
#ifndef ADVSETTINGDATADICTINOAYBASE_1_T2489327076_H
#define ADVSETTINGDATADICTINOAYBASE_1_T2489327076_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.AdvSettingDataDictinoayBase`1<Utage.AdvVideoSettingData>
struct  AdvSettingDataDictinoayBase_1_t2489327076  : public AdvSettingBase_t3183557912
{
public:
	// System.Collections.Generic.List`1<T> Utage.AdvSettingDataDictinoayBase`1::<List>k__BackingField
	List_1_t2111438702 * ___U3CListU3Ek__BackingField_1;
	// System.Collections.Generic.Dictionary`2<System.String,T> Utage.AdvSettingDataDictinoayBase`1::<Dictionary>k__BackingField
	Dictionary_2_t362129536 * ___U3CDictionaryU3Ek__BackingField_2;

public:
	inline static int32_t get_offset_of_U3CListU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(AdvSettingDataDictinoayBase_1_t2489327076, ___U3CListU3Ek__BackingField_1)); }
	inline List_1_t2111438702 * get_U3CListU3Ek__BackingField_1() const { return ___U3CListU3Ek__BackingField_1; }
	inline List_1_t2111438702 ** get_address_of_U3CListU3Ek__BackingField_1() { return &___U3CListU3Ek__BackingField_1; }
	inline void set_U3CListU3Ek__BackingField_1(List_1_t2111438702 * value)
	{
		___U3CListU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CListU3Ek__BackingField_1), value);
	}

	inline static int32_t get_offset_of_U3CDictionaryU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(AdvSettingDataDictinoayBase_1_t2489327076, ___U3CDictionaryU3Ek__BackingField_2)); }
	inline Dictionary_2_t362129536 * get_U3CDictionaryU3Ek__BackingField_2() const { return ___U3CDictionaryU3Ek__BackingField_2; }
	inline Dictionary_2_t362129536 ** get_address_of_U3CDictionaryU3Ek__BackingField_2() { return &___U3CDictionaryU3Ek__BackingField_2; }
	inline void set_U3CDictionaryU3Ek__BackingField_2(Dictionary_2_t362129536 * value)
	{
		___U3CDictionaryU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CDictionaryU3Ek__BackingField_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ADVSETTINGDATADICTINOAYBASE_1_T2489327076_H
#ifndef KEYVALUEPAIR_2_T666191687_H
#define KEYVALUEPAIR_2_T666191687_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.KeyValuePair`2<System.String,Utage.AdvParamStructTbl>
struct  KeyValuePair_2_t666191687 
{
public:
	// TKey System.Collections.Generic.KeyValuePair`2::key
	String_t* ___key_0;
	// TValue System.Collections.Generic.KeyValuePair`2::value
	AdvParamStructTbl_t994067203 * ___value_1;

public:
	inline static int32_t get_offset_of_key_0() { return static_cast<int32_t>(offsetof(KeyValuePair_2_t666191687, ___key_0)); }
	inline String_t* get_key_0() const { return ___key_0; }
	inline String_t** get_address_of_key_0() { return &___key_0; }
	inline void set_key_0(String_t* value)
	{
		___key_0 = value;
		Il2CppCodeGenWriteBarrier((&___key_0), value);
	}

	inline static int32_t get_offset_of_value_1() { return static_cast<int32_t>(offsetof(KeyValuePair_2_t666191687, ___value_1)); }
	inline AdvParamStructTbl_t994067203 * get_value_1() const { return ___value_1; }
	inline AdvParamStructTbl_t994067203 ** get_address_of_value_1() { return &___value_1; }
	inline void set_value_1(AdvParamStructTbl_t994067203 * value)
	{
		___value_1 = value;
		Il2CppCodeGenWriteBarrier((&___value_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // KEYVALUEPAIR_2_T666191687_H
#ifndef KEYVALUEPAIR_2_T1568511417_H
#define KEYVALUEPAIR_2_T1568511417_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.KeyValuePair`2<System.String,Utage.AdvParamStruct>
struct  KeyValuePair_2_t1568511417 
{
public:
	// TKey System.Collections.Generic.KeyValuePair`2::key
	String_t* ___key_0;
	// TValue System.Collections.Generic.KeyValuePair`2::value
	AdvParamStruct_t1896386933 * ___value_1;

public:
	inline static int32_t get_offset_of_key_0() { return static_cast<int32_t>(offsetof(KeyValuePair_2_t1568511417, ___key_0)); }
	inline String_t* get_key_0() const { return ___key_0; }
	inline String_t** get_address_of_key_0() { return &___key_0; }
	inline void set_key_0(String_t* value)
	{
		___key_0 = value;
		Il2CppCodeGenWriteBarrier((&___key_0), value);
	}

	inline static int32_t get_offset_of_value_1() { return static_cast<int32_t>(offsetof(KeyValuePair_2_t1568511417, ___value_1)); }
	inline AdvParamStruct_t1896386933 * get_value_1() const { return ___value_1; }
	inline AdvParamStruct_t1896386933 ** get_address_of_value_1() { return &___value_1; }
	inline void set_value_1(AdvParamStruct_t1896386933 * value)
	{
		___value_1 = value;
		Il2CppCodeGenWriteBarrier((&___value_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // KEYVALUEPAIR_2_T1568511417_H
#ifndef COLOR_T2020392075_H
#define COLOR_T2020392075_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Color
struct  Color_t2020392075 
{
public:
	// System.Single UnityEngine.Color::r
	float ___r_0;
	// System.Single UnityEngine.Color::g
	float ___g_1;
	// System.Single UnityEngine.Color::b
	float ___b_2;
	// System.Single UnityEngine.Color::a
	float ___a_3;

public:
	inline static int32_t get_offset_of_r_0() { return static_cast<int32_t>(offsetof(Color_t2020392075, ___r_0)); }
	inline float get_r_0() const { return ___r_0; }
	inline float* get_address_of_r_0() { return &___r_0; }
	inline void set_r_0(float value)
	{
		___r_0 = value;
	}

	inline static int32_t get_offset_of_g_1() { return static_cast<int32_t>(offsetof(Color_t2020392075, ___g_1)); }
	inline float get_g_1() const { return ___g_1; }
	inline float* get_address_of_g_1() { return &___g_1; }
	inline void set_g_1(float value)
	{
		___g_1 = value;
	}

	inline static int32_t get_offset_of_b_2() { return static_cast<int32_t>(offsetof(Color_t2020392075, ___b_2)); }
	inline float get_b_2() const { return ___b_2; }
	inline float* get_address_of_b_2() { return &___b_2; }
	inline void set_b_2(float value)
	{
		___b_2 = value;
	}

	inline static int32_t get_offset_of_a_3() { return static_cast<int32_t>(offsetof(Color_t2020392075, ___a_3)); }
	inline float get_a_3() const { return ___a_3; }
	inline float* get_address_of_a_3() { return &___a_3; }
	inline void set_a_3(float value)
	{
		___a_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLOR_T2020392075_H
#ifndef INTPTR_T_H
#define INTPTR_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTPTR_T_H
#ifndef ADVSETTINGDATADICTINOAYBASE_1_T1350530129_H
#define ADVSETTINGDATADICTINOAYBASE_1_T1350530129_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.AdvSettingDataDictinoayBase`1<Utage.AdvParticleSettingData>
struct  AdvSettingDataDictinoayBase_1_t1350530129  : public AdvSettingBase_t3183557912
{
public:
	// System.Collections.Generic.List`1<T> Utage.AdvSettingDataDictinoayBase`1::<List>k__BackingField
	List_1_t972641755 * ___U3CListU3Ek__BackingField_1;
	// System.Collections.Generic.Dictionary`2<System.String,T> Utage.AdvSettingDataDictinoayBase`1::<Dictionary>k__BackingField
	Dictionary_2_t3518299885 * ___U3CDictionaryU3Ek__BackingField_2;

public:
	inline static int32_t get_offset_of_U3CListU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(AdvSettingDataDictinoayBase_1_t1350530129, ___U3CListU3Ek__BackingField_1)); }
	inline List_1_t972641755 * get_U3CListU3Ek__BackingField_1() const { return ___U3CListU3Ek__BackingField_1; }
	inline List_1_t972641755 ** get_address_of_U3CListU3Ek__BackingField_1() { return &___U3CListU3Ek__BackingField_1; }
	inline void set_U3CListU3Ek__BackingField_1(List_1_t972641755 * value)
	{
		___U3CListU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CListU3Ek__BackingField_1), value);
	}

	inline static int32_t get_offset_of_U3CDictionaryU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(AdvSettingDataDictinoayBase_1_t1350530129, ___U3CDictionaryU3Ek__BackingField_2)); }
	inline Dictionary_2_t3518299885 * get_U3CDictionaryU3Ek__BackingField_2() const { return ___U3CDictionaryU3Ek__BackingField_2; }
	inline Dictionary_2_t3518299885 ** get_address_of_U3CDictionaryU3Ek__BackingField_2() { return &___U3CDictionaryU3Ek__BackingField_2; }
	inline void set_U3CDictionaryU3Ek__BackingField_2(Dictionary_2_t3518299885 * value)
	{
		___U3CDictionaryU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CDictionaryU3Ek__BackingField_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ADVSETTINGDATADICTINOAYBASE_1_T1350530129_H
#ifndef ENUM_T2459695545_H
#define ENUM_T2459695545_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Enum
struct  Enum_t2459695545  : public ValueType_t3507792607
{
public:

public:
};

struct Enum_t2459695545_StaticFields
{
public:
	// System.Char[] System.Enum::split_char
	CharU5BU5D_t1328083999* ___split_char_0;

public:
	inline static int32_t get_offset_of_split_char_0() { return static_cast<int32_t>(offsetof(Enum_t2459695545_StaticFields, ___split_char_0)); }
	inline CharU5BU5D_t1328083999* get_split_char_0() const { return ___split_char_0; }
	inline CharU5BU5D_t1328083999** get_address_of_split_char_0() { return &___split_char_0; }
	inline void set_split_char_0(CharU5BU5D_t1328083999* value)
	{
		___split_char_0 = value;
		Il2CppCodeGenWriteBarrier((&___split_char_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t2459695545_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t2459695545_marshaled_com
{
};
#endif // ENUM_T2459695545_H
#ifndef VOID_T1841601450_H
#define VOID_T1841601450_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Void
struct  Void_t1841601450 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VOID_T1841601450_H
#ifndef RECT_T3681755626_H
#define RECT_T3681755626_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Rect
struct  Rect_t3681755626 
{
public:
	// System.Single UnityEngine.Rect::m_XMin
	float ___m_XMin_0;
	// System.Single UnityEngine.Rect::m_YMin
	float ___m_YMin_1;
	// System.Single UnityEngine.Rect::m_Width
	float ___m_Width_2;
	// System.Single UnityEngine.Rect::m_Height
	float ___m_Height_3;

public:
	inline static int32_t get_offset_of_m_XMin_0() { return static_cast<int32_t>(offsetof(Rect_t3681755626, ___m_XMin_0)); }
	inline float get_m_XMin_0() const { return ___m_XMin_0; }
	inline float* get_address_of_m_XMin_0() { return &___m_XMin_0; }
	inline void set_m_XMin_0(float value)
	{
		___m_XMin_0 = value;
	}

	inline static int32_t get_offset_of_m_YMin_1() { return static_cast<int32_t>(offsetof(Rect_t3681755626, ___m_YMin_1)); }
	inline float get_m_YMin_1() const { return ___m_YMin_1; }
	inline float* get_address_of_m_YMin_1() { return &___m_YMin_1; }
	inline void set_m_YMin_1(float value)
	{
		___m_YMin_1 = value;
	}

	inline static int32_t get_offset_of_m_Width_2() { return static_cast<int32_t>(offsetof(Rect_t3681755626, ___m_Width_2)); }
	inline float get_m_Width_2() const { return ___m_Width_2; }
	inline float* get_address_of_m_Width_2() { return &___m_Width_2; }
	inline void set_m_Width_2(float value)
	{
		___m_Width_2 = value;
	}

	inline static int32_t get_offset_of_m_Height_3() { return static_cast<int32_t>(offsetof(Rect_t3681755626, ___m_Height_3)); }
	inline float get_m_Height_3() const { return ___m_Height_3; }
	inline float* get_address_of_m_Height_3() { return &___m_Height_3; }
	inline void set_m_Height_3(float value)
	{
		___m_Height_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RECT_T3681755626_H
#ifndef ADVSETTINGDATADICTINOAYBASE_1_T1418954748_H
#define ADVSETTINGDATADICTINOAYBASE_1_T1418954748_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.AdvSettingDataDictinoayBase`1<Utage.AdvCharacterSettingData>
struct  AdvSettingDataDictinoayBase_1_t1418954748  : public AdvSettingBase_t3183557912
{
public:
	// System.Collections.Generic.List`1<T> Utage.AdvSettingDataDictinoayBase`1::<List>k__BackingField
	List_1_t1041066374 * ___U3CListU3Ek__BackingField_1;
	// System.Collections.Generic.Dictionary`2<System.String,T> Utage.AdvSettingDataDictinoayBase`1::<Dictionary>k__BackingField
	Dictionary_2_t3586724504 * ___U3CDictionaryU3Ek__BackingField_2;

public:
	inline static int32_t get_offset_of_U3CListU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(AdvSettingDataDictinoayBase_1_t1418954748, ___U3CListU3Ek__BackingField_1)); }
	inline List_1_t1041066374 * get_U3CListU3Ek__BackingField_1() const { return ___U3CListU3Ek__BackingField_1; }
	inline List_1_t1041066374 ** get_address_of_U3CListU3Ek__BackingField_1() { return &___U3CListU3Ek__BackingField_1; }
	inline void set_U3CListU3Ek__BackingField_1(List_1_t1041066374 * value)
	{
		___U3CListU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CListU3Ek__BackingField_1), value);
	}

	inline static int32_t get_offset_of_U3CDictionaryU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(AdvSettingDataDictinoayBase_1_t1418954748, ___U3CDictionaryU3Ek__BackingField_2)); }
	inline Dictionary_2_t3586724504 * get_U3CDictionaryU3Ek__BackingField_2() const { return ___U3CDictionaryU3Ek__BackingField_2; }
	inline Dictionary_2_t3586724504 ** get_address_of_U3CDictionaryU3Ek__BackingField_2() { return &___U3CDictionaryU3Ek__BackingField_2; }
	inline void set_U3CDictionaryU3Ek__BackingField_2(Dictionary_2_t3586724504 * value)
	{
		___U3CDictionaryU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CDictionaryU3Ek__BackingField_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ADVSETTINGDATADICTINOAYBASE_1_T1418954748_H
#ifndef ADVSETTINGDATADICTINOAYBASE_1_T3034357464_H
#define ADVSETTINGDATADICTINOAYBASE_1_T3034357464_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.AdvSettingDataDictinoayBase`1<Utage.AdvEyeBlinkData>
struct  AdvSettingDataDictinoayBase_1_t3034357464  : public AdvSettingBase_t3183557912
{
public:
	// System.Collections.Generic.List`1<T> Utage.AdvSettingDataDictinoayBase`1::<List>k__BackingField
	List_1_t2656469090 * ___U3CListU3Ek__BackingField_1;
	// System.Collections.Generic.Dictionary`2<System.String,T> Utage.AdvSettingDataDictinoayBase`1::<Dictionary>k__BackingField
	Dictionary_2_t907159924 * ___U3CDictionaryU3Ek__BackingField_2;

public:
	inline static int32_t get_offset_of_U3CListU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(AdvSettingDataDictinoayBase_1_t3034357464, ___U3CListU3Ek__BackingField_1)); }
	inline List_1_t2656469090 * get_U3CListU3Ek__BackingField_1() const { return ___U3CListU3Ek__BackingField_1; }
	inline List_1_t2656469090 ** get_address_of_U3CListU3Ek__BackingField_1() { return &___U3CListU3Ek__BackingField_1; }
	inline void set_U3CListU3Ek__BackingField_1(List_1_t2656469090 * value)
	{
		___U3CListU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CListU3Ek__BackingField_1), value);
	}

	inline static int32_t get_offset_of_U3CDictionaryU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(AdvSettingDataDictinoayBase_1_t3034357464, ___U3CDictionaryU3Ek__BackingField_2)); }
	inline Dictionary_2_t907159924 * get_U3CDictionaryU3Ek__BackingField_2() const { return ___U3CDictionaryU3Ek__BackingField_2; }
	inline Dictionary_2_t907159924 ** get_address_of_U3CDictionaryU3Ek__BackingField_2() { return &___U3CDictionaryU3Ek__BackingField_2; }
	inline void set_U3CDictionaryU3Ek__BackingField_2(Dictionary_2_t907159924 * value)
	{
		___U3CDictionaryU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CDictionaryU3Ek__BackingField_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ADVSETTINGDATADICTINOAYBASE_1_T3034357464_H
#ifndef VECTOR3_T2243707580_H
#define VECTOR3_T2243707580_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector3
struct  Vector3_t2243707580 
{
public:
	// System.Single UnityEngine.Vector3::x
	float ___x_1;
	// System.Single UnityEngine.Vector3::y
	float ___y_2;
	// System.Single UnityEngine.Vector3::z
	float ___z_3;

public:
	inline static int32_t get_offset_of_x_1() { return static_cast<int32_t>(offsetof(Vector3_t2243707580, ___x_1)); }
	inline float get_x_1() const { return ___x_1; }
	inline float* get_address_of_x_1() { return &___x_1; }
	inline void set_x_1(float value)
	{
		___x_1 = value;
	}

	inline static int32_t get_offset_of_y_2() { return static_cast<int32_t>(offsetof(Vector3_t2243707580, ___y_2)); }
	inline float get_y_2() const { return ___y_2; }
	inline float* get_address_of_y_2() { return &___y_2; }
	inline void set_y_2(float value)
	{
		___y_2 = value;
	}

	inline static int32_t get_offset_of_z_3() { return static_cast<int32_t>(offsetof(Vector3_t2243707580, ___z_3)); }
	inline float get_z_3() const { return ___z_3; }
	inline float* get_address_of_z_3() { return &___z_3; }
	inline void set_z_3(float value)
	{
		___z_3 = value;
	}
};

struct Vector3_t2243707580_StaticFields
{
public:
	// UnityEngine.Vector3 UnityEngine.Vector3::zeroVector
	Vector3_t2243707580  ___zeroVector_4;
	// UnityEngine.Vector3 UnityEngine.Vector3::oneVector
	Vector3_t2243707580  ___oneVector_5;
	// UnityEngine.Vector3 UnityEngine.Vector3::upVector
	Vector3_t2243707580  ___upVector_6;
	// UnityEngine.Vector3 UnityEngine.Vector3::downVector
	Vector3_t2243707580  ___downVector_7;
	// UnityEngine.Vector3 UnityEngine.Vector3::leftVector
	Vector3_t2243707580  ___leftVector_8;
	// UnityEngine.Vector3 UnityEngine.Vector3::rightVector
	Vector3_t2243707580  ___rightVector_9;
	// UnityEngine.Vector3 UnityEngine.Vector3::forwardVector
	Vector3_t2243707580  ___forwardVector_10;
	// UnityEngine.Vector3 UnityEngine.Vector3::backVector
	Vector3_t2243707580  ___backVector_11;
	// UnityEngine.Vector3 UnityEngine.Vector3::positiveInfinityVector
	Vector3_t2243707580  ___positiveInfinityVector_12;
	// UnityEngine.Vector3 UnityEngine.Vector3::negativeInfinityVector
	Vector3_t2243707580  ___negativeInfinityVector_13;

public:
	inline static int32_t get_offset_of_zeroVector_4() { return static_cast<int32_t>(offsetof(Vector3_t2243707580_StaticFields, ___zeroVector_4)); }
	inline Vector3_t2243707580  get_zeroVector_4() const { return ___zeroVector_4; }
	inline Vector3_t2243707580 * get_address_of_zeroVector_4() { return &___zeroVector_4; }
	inline void set_zeroVector_4(Vector3_t2243707580  value)
	{
		___zeroVector_4 = value;
	}

	inline static int32_t get_offset_of_oneVector_5() { return static_cast<int32_t>(offsetof(Vector3_t2243707580_StaticFields, ___oneVector_5)); }
	inline Vector3_t2243707580  get_oneVector_5() const { return ___oneVector_5; }
	inline Vector3_t2243707580 * get_address_of_oneVector_5() { return &___oneVector_5; }
	inline void set_oneVector_5(Vector3_t2243707580  value)
	{
		___oneVector_5 = value;
	}

	inline static int32_t get_offset_of_upVector_6() { return static_cast<int32_t>(offsetof(Vector3_t2243707580_StaticFields, ___upVector_6)); }
	inline Vector3_t2243707580  get_upVector_6() const { return ___upVector_6; }
	inline Vector3_t2243707580 * get_address_of_upVector_6() { return &___upVector_6; }
	inline void set_upVector_6(Vector3_t2243707580  value)
	{
		___upVector_6 = value;
	}

	inline static int32_t get_offset_of_downVector_7() { return static_cast<int32_t>(offsetof(Vector3_t2243707580_StaticFields, ___downVector_7)); }
	inline Vector3_t2243707580  get_downVector_7() const { return ___downVector_7; }
	inline Vector3_t2243707580 * get_address_of_downVector_7() { return &___downVector_7; }
	inline void set_downVector_7(Vector3_t2243707580  value)
	{
		___downVector_7 = value;
	}

	inline static int32_t get_offset_of_leftVector_8() { return static_cast<int32_t>(offsetof(Vector3_t2243707580_StaticFields, ___leftVector_8)); }
	inline Vector3_t2243707580  get_leftVector_8() const { return ___leftVector_8; }
	inline Vector3_t2243707580 * get_address_of_leftVector_8() { return &___leftVector_8; }
	inline void set_leftVector_8(Vector3_t2243707580  value)
	{
		___leftVector_8 = value;
	}

	inline static int32_t get_offset_of_rightVector_9() { return static_cast<int32_t>(offsetof(Vector3_t2243707580_StaticFields, ___rightVector_9)); }
	inline Vector3_t2243707580  get_rightVector_9() const { return ___rightVector_9; }
	inline Vector3_t2243707580 * get_address_of_rightVector_9() { return &___rightVector_9; }
	inline void set_rightVector_9(Vector3_t2243707580  value)
	{
		___rightVector_9 = value;
	}

	inline static int32_t get_offset_of_forwardVector_10() { return static_cast<int32_t>(offsetof(Vector3_t2243707580_StaticFields, ___forwardVector_10)); }
	inline Vector3_t2243707580  get_forwardVector_10() const { return ___forwardVector_10; }
	inline Vector3_t2243707580 * get_address_of_forwardVector_10() { return &___forwardVector_10; }
	inline void set_forwardVector_10(Vector3_t2243707580  value)
	{
		___forwardVector_10 = value;
	}

	inline static int32_t get_offset_of_backVector_11() { return static_cast<int32_t>(offsetof(Vector3_t2243707580_StaticFields, ___backVector_11)); }
	inline Vector3_t2243707580  get_backVector_11() const { return ___backVector_11; }
	inline Vector3_t2243707580 * get_address_of_backVector_11() { return &___backVector_11; }
	inline void set_backVector_11(Vector3_t2243707580  value)
	{
		___backVector_11 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_12() { return static_cast<int32_t>(offsetof(Vector3_t2243707580_StaticFields, ___positiveInfinityVector_12)); }
	inline Vector3_t2243707580  get_positiveInfinityVector_12() const { return ___positiveInfinityVector_12; }
	inline Vector3_t2243707580 * get_address_of_positiveInfinityVector_12() { return &___positiveInfinityVector_12; }
	inline void set_positiveInfinityVector_12(Vector3_t2243707580  value)
	{
		___positiveInfinityVector_12 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_13() { return static_cast<int32_t>(offsetof(Vector3_t2243707580_StaticFields, ___negativeInfinityVector_13)); }
	inline Vector3_t2243707580  get_negativeInfinityVector_13() const { return ___negativeInfinityVector_13; }
	inline Vector3_t2243707580 * get_address_of_negativeInfinityVector_13() { return &___negativeInfinityVector_13; }
	inline void set_negativeInfinityVector_13(Vector3_t2243707580  value)
	{
		___negativeInfinityVector_13 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR3_T2243707580_H
#ifndef ADVCOMMANDSENDMESSAGEBYNAME_T3179247017_H
#define ADVCOMMANDSENDMESSAGEBYNAME_T3179247017_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.AdvCommandSendMessageByName
struct  AdvCommandSendMessageByName_t3179247017  : public AdvCommand_t2859960984
{
public:
	// System.Boolean Utage.AdvCommandSendMessageByName::<IsWait>k__BackingField
	bool ___U3CIsWaitU3Ek__BackingField_7;
	// Utage.AdvEngine Utage.AdvCommandSendMessageByName::<Engine>k__BackingField
	AdvEngine_t1176753927 * ___U3CEngineU3Ek__BackingField_8;

public:
	inline static int32_t get_offset_of_U3CIsWaitU3Ek__BackingField_7() { return static_cast<int32_t>(offsetof(AdvCommandSendMessageByName_t3179247017, ___U3CIsWaitU3Ek__BackingField_7)); }
	inline bool get_U3CIsWaitU3Ek__BackingField_7() const { return ___U3CIsWaitU3Ek__BackingField_7; }
	inline bool* get_address_of_U3CIsWaitU3Ek__BackingField_7() { return &___U3CIsWaitU3Ek__BackingField_7; }
	inline void set_U3CIsWaitU3Ek__BackingField_7(bool value)
	{
		___U3CIsWaitU3Ek__BackingField_7 = value;
	}

	inline static int32_t get_offset_of_U3CEngineU3Ek__BackingField_8() { return static_cast<int32_t>(offsetof(AdvCommandSendMessageByName_t3179247017, ___U3CEngineU3Ek__BackingField_8)); }
	inline AdvEngine_t1176753927 * get_U3CEngineU3Ek__BackingField_8() const { return ___U3CEngineU3Ek__BackingField_8; }
	inline AdvEngine_t1176753927 ** get_address_of_U3CEngineU3Ek__BackingField_8() { return &___U3CEngineU3Ek__BackingField_8; }
	inline void set_U3CEngineU3Ek__BackingField_8(AdvEngine_t1176753927 * value)
	{
		___U3CEngineU3Ek__BackingField_8 = value;
		Il2CppCodeGenWriteBarrier((&___U3CEngineU3Ek__BackingField_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ADVCOMMANDSENDMESSAGEBYNAME_T3179247017_H
#ifndef ADVSETTINGDATADICTINOAYBASE_1_T1655781866_H
#define ADVSETTINGDATADICTINOAYBASE_1_T1655781866_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.AdvSettingDataDictinoayBase`1<Utage.AdvLayerSettingData>
struct  AdvSettingDataDictinoayBase_1_t1655781866  : public AdvSettingBase_t3183557912
{
public:
	// System.Collections.Generic.List`1<T> Utage.AdvSettingDataDictinoayBase`1::<List>k__BackingField
	List_1_t1277893492 * ___U3CListU3Ek__BackingField_1;
	// System.Collections.Generic.Dictionary`2<System.String,T> Utage.AdvSettingDataDictinoayBase`1::<Dictionary>k__BackingField
	Dictionary_2_t3823551622 * ___U3CDictionaryU3Ek__BackingField_2;

public:
	inline static int32_t get_offset_of_U3CListU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(AdvSettingDataDictinoayBase_1_t1655781866, ___U3CListU3Ek__BackingField_1)); }
	inline List_1_t1277893492 * get_U3CListU3Ek__BackingField_1() const { return ___U3CListU3Ek__BackingField_1; }
	inline List_1_t1277893492 ** get_address_of_U3CListU3Ek__BackingField_1() { return &___U3CListU3Ek__BackingField_1; }
	inline void set_U3CListU3Ek__BackingField_1(List_1_t1277893492 * value)
	{
		___U3CListU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CListU3Ek__BackingField_1), value);
	}

	inline static int32_t get_offset_of_U3CDictionaryU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(AdvSettingDataDictinoayBase_1_t1655781866, ___U3CDictionaryU3Ek__BackingField_2)); }
	inline Dictionary_2_t3823551622 * get_U3CDictionaryU3Ek__BackingField_2() const { return ___U3CDictionaryU3Ek__BackingField_2; }
	inline Dictionary_2_t3823551622 ** get_address_of_U3CDictionaryU3Ek__BackingField_2() { return &___U3CDictionaryU3Ek__BackingField_2; }
	inline void set_U3CDictionaryU3Ek__BackingField_2(Dictionary_2_t3823551622 * value)
	{
		___U3CDictionaryU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CDictionaryU3Ek__BackingField_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ADVSETTINGDATADICTINOAYBASE_1_T1655781866_H
#ifndef ADVSETTINGDATADICTINOAYBASE_1_T837457769_H
#define ADVSETTINGDATADICTINOAYBASE_1_T837457769_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.AdvSettingDataDictinoayBase`1<Utage.AdvLipSynchData>
struct  AdvSettingDataDictinoayBase_1_t837457769  : public AdvSettingBase_t3183557912
{
public:
	// System.Collections.Generic.List`1<T> Utage.AdvSettingDataDictinoayBase`1::<List>k__BackingField
	List_1_t459569395 * ___U3CListU3Ek__BackingField_1;
	// System.Collections.Generic.Dictionary`2<System.String,T> Utage.AdvSettingDataDictinoayBase`1::<Dictionary>k__BackingField
	Dictionary_2_t3005227525 * ___U3CDictionaryU3Ek__BackingField_2;

public:
	inline static int32_t get_offset_of_U3CListU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(AdvSettingDataDictinoayBase_1_t837457769, ___U3CListU3Ek__BackingField_1)); }
	inline List_1_t459569395 * get_U3CListU3Ek__BackingField_1() const { return ___U3CListU3Ek__BackingField_1; }
	inline List_1_t459569395 ** get_address_of_U3CListU3Ek__BackingField_1() { return &___U3CListU3Ek__BackingField_1; }
	inline void set_U3CListU3Ek__BackingField_1(List_1_t459569395 * value)
	{
		___U3CListU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CListU3Ek__BackingField_1), value);
	}

	inline static int32_t get_offset_of_U3CDictionaryU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(AdvSettingDataDictinoayBase_1_t837457769, ___U3CDictionaryU3Ek__BackingField_2)); }
	inline Dictionary_2_t3005227525 * get_U3CDictionaryU3Ek__BackingField_2() const { return ___U3CDictionaryU3Ek__BackingField_2; }
	inline Dictionary_2_t3005227525 ** get_address_of_U3CDictionaryU3Ek__BackingField_2() { return &___U3CDictionaryU3Ek__BackingField_2; }
	inline void set_U3CDictionaryU3Ek__BackingField_2(Dictionary_2_t3005227525 * value)
	{
		___U3CDictionaryU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CDictionaryU3Ek__BackingField_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ADVSETTINGDATADICTINOAYBASE_1_T837457769_H
#ifndef VECTOR2_T2243707579_H
#define VECTOR2_T2243707579_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector2
struct  Vector2_t2243707579 
{
public:
	// System.Single UnityEngine.Vector2::x
	float ___x_0;
	// System.Single UnityEngine.Vector2::y
	float ___y_1;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Vector2_t2243707579, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Vector2_t2243707579, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}
};

struct Vector2_t2243707579_StaticFields
{
public:
	// UnityEngine.Vector2 UnityEngine.Vector2::zeroVector
	Vector2_t2243707579  ___zeroVector_2;
	// UnityEngine.Vector2 UnityEngine.Vector2::oneVector
	Vector2_t2243707579  ___oneVector_3;
	// UnityEngine.Vector2 UnityEngine.Vector2::upVector
	Vector2_t2243707579  ___upVector_4;
	// UnityEngine.Vector2 UnityEngine.Vector2::downVector
	Vector2_t2243707579  ___downVector_5;
	// UnityEngine.Vector2 UnityEngine.Vector2::leftVector
	Vector2_t2243707579  ___leftVector_6;
	// UnityEngine.Vector2 UnityEngine.Vector2::rightVector
	Vector2_t2243707579  ___rightVector_7;
	// UnityEngine.Vector2 UnityEngine.Vector2::positiveInfinityVector
	Vector2_t2243707579  ___positiveInfinityVector_8;
	// UnityEngine.Vector2 UnityEngine.Vector2::negativeInfinityVector
	Vector2_t2243707579  ___negativeInfinityVector_9;

public:
	inline static int32_t get_offset_of_zeroVector_2() { return static_cast<int32_t>(offsetof(Vector2_t2243707579_StaticFields, ___zeroVector_2)); }
	inline Vector2_t2243707579  get_zeroVector_2() const { return ___zeroVector_2; }
	inline Vector2_t2243707579 * get_address_of_zeroVector_2() { return &___zeroVector_2; }
	inline void set_zeroVector_2(Vector2_t2243707579  value)
	{
		___zeroVector_2 = value;
	}

	inline static int32_t get_offset_of_oneVector_3() { return static_cast<int32_t>(offsetof(Vector2_t2243707579_StaticFields, ___oneVector_3)); }
	inline Vector2_t2243707579  get_oneVector_3() const { return ___oneVector_3; }
	inline Vector2_t2243707579 * get_address_of_oneVector_3() { return &___oneVector_3; }
	inline void set_oneVector_3(Vector2_t2243707579  value)
	{
		___oneVector_3 = value;
	}

	inline static int32_t get_offset_of_upVector_4() { return static_cast<int32_t>(offsetof(Vector2_t2243707579_StaticFields, ___upVector_4)); }
	inline Vector2_t2243707579  get_upVector_4() const { return ___upVector_4; }
	inline Vector2_t2243707579 * get_address_of_upVector_4() { return &___upVector_4; }
	inline void set_upVector_4(Vector2_t2243707579  value)
	{
		___upVector_4 = value;
	}

	inline static int32_t get_offset_of_downVector_5() { return static_cast<int32_t>(offsetof(Vector2_t2243707579_StaticFields, ___downVector_5)); }
	inline Vector2_t2243707579  get_downVector_5() const { return ___downVector_5; }
	inline Vector2_t2243707579 * get_address_of_downVector_5() { return &___downVector_5; }
	inline void set_downVector_5(Vector2_t2243707579  value)
	{
		___downVector_5 = value;
	}

	inline static int32_t get_offset_of_leftVector_6() { return static_cast<int32_t>(offsetof(Vector2_t2243707579_StaticFields, ___leftVector_6)); }
	inline Vector2_t2243707579  get_leftVector_6() const { return ___leftVector_6; }
	inline Vector2_t2243707579 * get_address_of_leftVector_6() { return &___leftVector_6; }
	inline void set_leftVector_6(Vector2_t2243707579  value)
	{
		___leftVector_6 = value;
	}

	inline static int32_t get_offset_of_rightVector_7() { return static_cast<int32_t>(offsetof(Vector2_t2243707579_StaticFields, ___rightVector_7)); }
	inline Vector2_t2243707579  get_rightVector_7() const { return ___rightVector_7; }
	inline Vector2_t2243707579 * get_address_of_rightVector_7() { return &___rightVector_7; }
	inline void set_rightVector_7(Vector2_t2243707579  value)
	{
		___rightVector_7 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_8() { return static_cast<int32_t>(offsetof(Vector2_t2243707579_StaticFields, ___positiveInfinityVector_8)); }
	inline Vector2_t2243707579  get_positiveInfinityVector_8() const { return ___positiveInfinityVector_8; }
	inline Vector2_t2243707579 * get_address_of_positiveInfinityVector_8() { return &___positiveInfinityVector_8; }
	inline void set_positiveInfinityVector_8(Vector2_t2243707579  value)
	{
		___positiveInfinityVector_8 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_9() { return static_cast<int32_t>(offsetof(Vector2_t2243707579_StaticFields, ___negativeInfinityVector_9)); }
	inline Vector2_t2243707579  get_negativeInfinityVector_9() const { return ___negativeInfinityVector_9; }
	inline Vector2_t2243707579 * get_address_of_negativeInfinityVector_9() { return &___negativeInfinityVector_9; }
	inline void set_negativeInfinityVector_9(Vector2_t2243707579  value)
	{
		___negativeInfinityVector_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR2_T2243707579_H
#ifndef ADVSCENEGALLERYSETTINGDATA_T1280603049_H
#define ADVSCENEGALLERYSETTINGDATA_T1280603049_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.AdvSceneGallerySettingData
struct  AdvSceneGallerySettingData_t1280603049  : public AdvSettingDictinoayItemBase_t203751633
{
public:
	// System.String Utage.AdvSceneGallerySettingData::title
	String_t* ___title_2;
	// System.String Utage.AdvSceneGallerySettingData::category
	String_t* ___category_3;
	// System.String Utage.AdvSceneGallerySettingData::thumbnailName
	String_t* ___thumbnailName_4;
	// System.String Utage.AdvSceneGallerySettingData::thumbnailPath
	String_t* ___thumbnailPath_5;
	// System.Int32 Utage.AdvSceneGallerySettingData::thumbnailVersion
	int32_t ___thumbnailVersion_6;

public:
	inline static int32_t get_offset_of_title_2() { return static_cast<int32_t>(offsetof(AdvSceneGallerySettingData_t1280603049, ___title_2)); }
	inline String_t* get_title_2() const { return ___title_2; }
	inline String_t** get_address_of_title_2() { return &___title_2; }
	inline void set_title_2(String_t* value)
	{
		___title_2 = value;
		Il2CppCodeGenWriteBarrier((&___title_2), value);
	}

	inline static int32_t get_offset_of_category_3() { return static_cast<int32_t>(offsetof(AdvSceneGallerySettingData_t1280603049, ___category_3)); }
	inline String_t* get_category_3() const { return ___category_3; }
	inline String_t** get_address_of_category_3() { return &___category_3; }
	inline void set_category_3(String_t* value)
	{
		___category_3 = value;
		Il2CppCodeGenWriteBarrier((&___category_3), value);
	}

	inline static int32_t get_offset_of_thumbnailName_4() { return static_cast<int32_t>(offsetof(AdvSceneGallerySettingData_t1280603049, ___thumbnailName_4)); }
	inline String_t* get_thumbnailName_4() const { return ___thumbnailName_4; }
	inline String_t** get_address_of_thumbnailName_4() { return &___thumbnailName_4; }
	inline void set_thumbnailName_4(String_t* value)
	{
		___thumbnailName_4 = value;
		Il2CppCodeGenWriteBarrier((&___thumbnailName_4), value);
	}

	inline static int32_t get_offset_of_thumbnailPath_5() { return static_cast<int32_t>(offsetof(AdvSceneGallerySettingData_t1280603049, ___thumbnailPath_5)); }
	inline String_t* get_thumbnailPath_5() const { return ___thumbnailPath_5; }
	inline String_t** get_address_of_thumbnailPath_5() { return &___thumbnailPath_5; }
	inline void set_thumbnailPath_5(String_t* value)
	{
		___thumbnailPath_5 = value;
		Il2CppCodeGenWriteBarrier((&___thumbnailPath_5), value);
	}

	inline static int32_t get_offset_of_thumbnailVersion_6() { return static_cast<int32_t>(offsetof(AdvSceneGallerySettingData_t1280603049, ___thumbnailVersion_6)); }
	inline int32_t get_thumbnailVersion_6() const { return ___thumbnailVersion_6; }
	inline int32_t* get_address_of_thumbnailVersion_6() { return &___thumbnailVersion_6; }
	inline void set_thumbnailVersion_6(int32_t value)
	{
		___thumbnailVersion_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ADVSCENEGALLERYSETTINGDATA_T1280603049_H
#ifndef ADVPARTICLESETTINGDATA_T1603520623_H
#define ADVPARTICLESETTINGDATA_T1603520623_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.AdvParticleSettingData
struct  AdvParticleSettingData_t1603520623  : public AdvSettingDictinoayItemBase_t203751633
{
public:
	// Utage.AdvGraphicInfo Utage.AdvParticleSettingData::graphic
	AdvGraphicInfo_t3545565645 * ___graphic_2;

public:
	inline static int32_t get_offset_of_graphic_2() { return static_cast<int32_t>(offsetof(AdvParticleSettingData_t1603520623, ___graphic_2)); }
	inline AdvGraphicInfo_t3545565645 * get_graphic_2() const { return ___graphic_2; }
	inline AdvGraphicInfo_t3545565645 ** get_address_of_graphic_2() { return &___graphic_2; }
	inline void set_graphic_2(AdvGraphicInfo_t3545565645 * value)
	{
		___graphic_2 = value;
		Il2CppCodeGenWriteBarrier((&___graphic_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ADVPARTICLESETTINGDATA_T1603520623_H
#ifndef ADVLOCALIZESETTING_T467665552_H
#define ADVLOCALIZESETTING_T467665552_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.AdvLocalizeSetting
struct  AdvLocalizeSetting_t467665552  : public AdvSettingBase_t3183557912
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ADVLOCALIZESETTING_T467665552_H
#ifndef ADVANIMATIONSETTING_T2232720937_H
#define ADVANIMATIONSETTING_T2232720937_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.AdvAnimationSetting
struct  AdvAnimationSetting_t2232720937  : public AdvSettingBase_t3183557912
{
public:
	// System.Collections.Generic.List`1<Utage.AdvAnimationData> Utage.AdvAnimationSetting::DataList
	List_1_t2800688647 * ___DataList_1;

public:
	inline static int32_t get_offset_of_DataList_1() { return static_cast<int32_t>(offsetof(AdvAnimationSetting_t2232720937, ___DataList_1)); }
	inline List_1_t2800688647 * get_DataList_1() const { return ___DataList_1; }
	inline List_1_t2800688647 ** get_address_of_DataList_1() { return &___DataList_1; }
	inline void set_DataList_1(List_1_t2800688647 * value)
	{
		___DataList_1 = value;
		Il2CppCodeGenWriteBarrier((&___DataList_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ADVANIMATIONSETTING_T2232720937_H
#ifndef ADVCHARACTERSETTINGDATA_T1671945242_H
#define ADVCHARACTERSETTINGDATA_T1671945242_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.AdvCharacterSettingData
struct  AdvCharacterSettingData_t1671945242  : public AdvSettingDictinoayItemBase_t203751633
{
public:
	// System.String Utage.AdvCharacterSettingData::name
	String_t* ___name_3;
	// System.String Utage.AdvCharacterSettingData::pattern
	String_t* ___pattern_4;
	// System.String Utage.AdvCharacterSettingData::nameText
	String_t* ___nameText_5;
	// Utage.AdvGraphicInfoList Utage.AdvCharacterSettingData::graphic
	AdvGraphicInfoList_t3537398639 * ___graphic_6;
	// Utage.AdvCharacterSettingData/IconInfo Utage.AdvCharacterSettingData::<Icon>k__BackingField
	IconInfo_t3287921618 * ___U3CIconU3Ek__BackingField_7;

public:
	inline static int32_t get_offset_of_name_3() { return static_cast<int32_t>(offsetof(AdvCharacterSettingData_t1671945242, ___name_3)); }
	inline String_t* get_name_3() const { return ___name_3; }
	inline String_t** get_address_of_name_3() { return &___name_3; }
	inline void set_name_3(String_t* value)
	{
		___name_3 = value;
		Il2CppCodeGenWriteBarrier((&___name_3), value);
	}

	inline static int32_t get_offset_of_pattern_4() { return static_cast<int32_t>(offsetof(AdvCharacterSettingData_t1671945242, ___pattern_4)); }
	inline String_t* get_pattern_4() const { return ___pattern_4; }
	inline String_t** get_address_of_pattern_4() { return &___pattern_4; }
	inline void set_pattern_4(String_t* value)
	{
		___pattern_4 = value;
		Il2CppCodeGenWriteBarrier((&___pattern_4), value);
	}

	inline static int32_t get_offset_of_nameText_5() { return static_cast<int32_t>(offsetof(AdvCharacterSettingData_t1671945242, ___nameText_5)); }
	inline String_t* get_nameText_5() const { return ___nameText_5; }
	inline String_t** get_address_of_nameText_5() { return &___nameText_5; }
	inline void set_nameText_5(String_t* value)
	{
		___nameText_5 = value;
		Il2CppCodeGenWriteBarrier((&___nameText_5), value);
	}

	inline static int32_t get_offset_of_graphic_6() { return static_cast<int32_t>(offsetof(AdvCharacterSettingData_t1671945242, ___graphic_6)); }
	inline AdvGraphicInfoList_t3537398639 * get_graphic_6() const { return ___graphic_6; }
	inline AdvGraphicInfoList_t3537398639 ** get_address_of_graphic_6() { return &___graphic_6; }
	inline void set_graphic_6(AdvGraphicInfoList_t3537398639 * value)
	{
		___graphic_6 = value;
		Il2CppCodeGenWriteBarrier((&___graphic_6), value);
	}

	inline static int32_t get_offset_of_U3CIconU3Ek__BackingField_7() { return static_cast<int32_t>(offsetof(AdvCharacterSettingData_t1671945242, ___U3CIconU3Ek__BackingField_7)); }
	inline IconInfo_t3287921618 * get_U3CIconU3Ek__BackingField_7() const { return ___U3CIconU3Ek__BackingField_7; }
	inline IconInfo_t3287921618 ** get_address_of_U3CIconU3Ek__BackingField_7() { return &___U3CIconU3Ek__BackingField_7; }
	inline void set_U3CIconU3Ek__BackingField_7(IconInfo_t3287921618 * value)
	{
		___U3CIconU3Ek__BackingField_7 = value;
		Il2CppCodeGenWriteBarrier((&___U3CIconU3Ek__BackingField_7), value);
	}
};

struct AdvCharacterSettingData_t1671945242_StaticFields
{
public:
	// Utage.AdvCharacterSettingData/ParseCustomFileTypeRootDir Utage.AdvCharacterSettingData::CallbackParseCustomFileTypeRootDir
	ParseCustomFileTypeRootDir_t3762798398 * ___CallbackParseCustomFileTypeRootDir_2;

public:
	inline static int32_t get_offset_of_CallbackParseCustomFileTypeRootDir_2() { return static_cast<int32_t>(offsetof(AdvCharacterSettingData_t1671945242_StaticFields, ___CallbackParseCustomFileTypeRootDir_2)); }
	inline ParseCustomFileTypeRootDir_t3762798398 * get_CallbackParseCustomFileTypeRootDir_2() const { return ___CallbackParseCustomFileTypeRootDir_2; }
	inline ParseCustomFileTypeRootDir_t3762798398 ** get_address_of_CallbackParseCustomFileTypeRootDir_2() { return &___CallbackParseCustomFileTypeRootDir_2; }
	inline void set_CallbackParseCustomFileTypeRootDir_2(ParseCustomFileTypeRootDir_t3762798398 * value)
	{
		___CallbackParseCustomFileTypeRootDir_2 = value;
		Il2CppCodeGenWriteBarrier((&___CallbackParseCustomFileTypeRootDir_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ADVCHARACTERSETTINGDATA_T1671945242_H
#ifndef ADVEYEBLINKDATA_T3287347958_H
#define ADVEYEBLINKDATA_T3287347958_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.AdvEyeBlinkData
struct  AdvEyeBlinkData_t3287347958  : public AdvSettingDictinoayItemBase_t203751633
{
public:
	// System.Single Utage.AdvEyeBlinkData::intervalMin
	float ___intervalMin_2;
	// System.Single Utage.AdvEyeBlinkData::intervalMax
	float ___intervalMax_3;
	// System.Single Utage.AdvEyeBlinkData::randomDoubleEyeBlink
	float ___randomDoubleEyeBlink_4;
	// System.String Utage.AdvEyeBlinkData::tag
	String_t* ___tag_5;
	// Utage.MiniAnimationData Utage.AdvEyeBlinkData::animationData
	MiniAnimationData_t521227391 * ___animationData_6;

public:
	inline static int32_t get_offset_of_intervalMin_2() { return static_cast<int32_t>(offsetof(AdvEyeBlinkData_t3287347958, ___intervalMin_2)); }
	inline float get_intervalMin_2() const { return ___intervalMin_2; }
	inline float* get_address_of_intervalMin_2() { return &___intervalMin_2; }
	inline void set_intervalMin_2(float value)
	{
		___intervalMin_2 = value;
	}

	inline static int32_t get_offset_of_intervalMax_3() { return static_cast<int32_t>(offsetof(AdvEyeBlinkData_t3287347958, ___intervalMax_3)); }
	inline float get_intervalMax_3() const { return ___intervalMax_3; }
	inline float* get_address_of_intervalMax_3() { return &___intervalMax_3; }
	inline void set_intervalMax_3(float value)
	{
		___intervalMax_3 = value;
	}

	inline static int32_t get_offset_of_randomDoubleEyeBlink_4() { return static_cast<int32_t>(offsetof(AdvEyeBlinkData_t3287347958, ___randomDoubleEyeBlink_4)); }
	inline float get_randomDoubleEyeBlink_4() const { return ___randomDoubleEyeBlink_4; }
	inline float* get_address_of_randomDoubleEyeBlink_4() { return &___randomDoubleEyeBlink_4; }
	inline void set_randomDoubleEyeBlink_4(float value)
	{
		___randomDoubleEyeBlink_4 = value;
	}

	inline static int32_t get_offset_of_tag_5() { return static_cast<int32_t>(offsetof(AdvEyeBlinkData_t3287347958, ___tag_5)); }
	inline String_t* get_tag_5() const { return ___tag_5; }
	inline String_t** get_address_of_tag_5() { return &___tag_5; }
	inline void set_tag_5(String_t* value)
	{
		___tag_5 = value;
		Il2CppCodeGenWriteBarrier((&___tag_5), value);
	}

	inline static int32_t get_offset_of_animationData_6() { return static_cast<int32_t>(offsetof(AdvEyeBlinkData_t3287347958, ___animationData_6)); }
	inline MiniAnimationData_t521227391 * get_animationData_6() const { return ___animationData_6; }
	inline MiniAnimationData_t521227391 ** get_address_of_animationData_6() { return &___animationData_6; }
	inline void set_animationData_6(MiniAnimationData_t521227391 * value)
	{
		___animationData_6 = value;
		Il2CppCodeGenWriteBarrier((&___animationData_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ADVEYEBLINKDATA_T3287347958_H
#ifndef CSVTYPE_T1603973182_H
#define CSVTYPE_T1603973182_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.CsvType
struct  CsvType_t1603973182 
{
public:
	// System.Int32 Utage.CsvType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(CsvType_t1603973182, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CSVTYPE_T1603973182_H
#ifndef TYPE_T927793387_H
#define TYPE_T927793387_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.AdvCharacterSettingData/IconInfo/Type
struct  Type_t927793387 
{
public:
	// System.Int32 Utage.AdvCharacterSettingData/IconInfo/Type::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(Type_t927793387, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TYPE_T927793387_H
#ifndef ALIGNMENT_T3342318601_H
#define ALIGNMENT_T3342318601_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.Alignment
struct  Alignment_t3342318601 
{
public:
	// System.Int32 Utage.Alignment::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(Alignment_t3342318601, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ALIGNMENT_T3342318601_H
#ifndef SKIPFLAGS_T182225432_H
#define SKIPFLAGS_T182225432_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.AdvAgingTest/SkipFlags
struct  SkipFlags_t182225432 
{
public:
	// System.Int32 Utage.AdvAgingTest/SkipFlags::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(SkipFlags_t182225432, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SKIPFLAGS_T182225432_H
#ifndef TYPE_T2792715206_H
#define TYPE_T2792715206_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.AdvAgingTest/Type
struct  Type_t2792715206 
{
public:
	// System.Int32 Utage.AdvAgingTest/Type::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(Type_t2792715206, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TYPE_T2792715206_H
#ifndef LIPSYNCHTYPE_T1469092900_H
#define LIPSYNCHTYPE_T1469092900_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.LipSynchType
struct  LipSynchType_t1469092900 
{
public:
	// System.Int32 Utage.LipSynchType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(LipSynchType_t1469092900, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LIPSYNCHTYPE_T1469092900_H
#ifndef ADVLIPSYNCHSETTING_T289134997_H
#define ADVLIPSYNCHSETTING_T289134997_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.AdvLipSynchSetting
struct  AdvLipSynchSetting_t289134997  : public AdvSettingDataDictinoayBase_1_t837457769
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ADVLIPSYNCHSETTING_T289134997_H
#ifndef TARGETTYPE_T2482913595_H
#define TARGETTYPE_T2482913595_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.AdvEffectManager/TargetType
struct  TargetType_t2482913595 
{
public:
	// System.Int32 Utage.AdvEffectManager/TargetType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(TargetType_t2482913595, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TARGETTYPE_T2482913595_H
#ifndef LIGHTINGMASK_T397726104_H
#define LIGHTINGMASK_T397726104_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.AdvCharacterGrayOutController/LightingMask
struct  LightingMask_t397726104 
{
public:
	// System.Int32 Utage.AdvCharacterGrayOutController/LightingMask::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(LightingMask_t397726104, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LIGHTINGMASK_T397726104_H
#ifndef BORDERTYPE_T365579711_H
#define BORDERTYPE_T365579711_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.AdvLayerSettingData/BorderType
struct  BorderType_t365579711 
{
public:
	// System.Int32 Utage.AdvLayerSettingData/BorderType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(BorderType_t365579711, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BORDERTYPE_T365579711_H
#ifndef ADVLAYERSETTING_T3281222136_H
#define ADVLAYERSETTING_T3281222136_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.AdvLayerSetting
struct  AdvLayerSetting_t3281222136  : public AdvSettingDataDictinoayBase_1_t1655781866
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ADVLAYERSETTING_T3281222136_H
#ifndef TYPE_T2823646136_H
#define TYPE_T2823646136_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.AdvTextSound/Type
struct  Type_t2823646136 
{
public:
	// System.Int32 Utage.AdvTextSound/Type::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(Type_t2823646136, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TYPE_T2823646136_H
#ifndef LAYERTYPE_T2078485382_H
#define LAYERTYPE_T2078485382_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.AdvLayerSettingData/LayerType
struct  LayerType_t2078485382 
{
public:
	// System.Int32 Utage.AdvLayerSettingData/LayerType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(LayerType_t2078485382, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LAYERTYPE_T2078485382_H
#ifndef ADVEYEBLINKSETTING_T1976982958_H
#define ADVEYEBLINKSETTING_T1976982958_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.AdvEyeBlinkSetting
struct  AdvEyeBlinkSetting_t1976982958  : public AdvSettingDataDictinoayBase_1_t3034357464
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ADVEYEBLINKSETTING_T1976982958_H
#ifndef TYPE_T3945296675_H
#define TYPE_T3945296675_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.AdvTextureSettingData/Type
struct  Type_t3945296675 
{
public:
	// System.Int32 Utage.AdvTextureSettingData/Type::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(Type_t3945296675, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TYPE_T3945296675_H
#ifndef U3CFADECOLORU3EC__ITERATOR0_T3211073950_H
#define U3CFADECOLORU3EC__ITERATOR0_T3211073950_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.AdvCharacterGrayOutController/<FadeColor>c__Iterator0
struct  U3CFadeColorU3Ec__Iterator0_t3211073950  : public RuntimeObject
{
public:
	// System.Single Utage.AdvCharacterGrayOutController/<FadeColor>c__Iterator0::<elapsed>__0
	float ___U3CelapsedU3E__0_0;
	// UnityEngine.Color Utage.AdvCharacterGrayOutController/<FadeColor>c__Iterator0::from
	Color_t2020392075  ___from_1;
	// UnityEngine.Color Utage.AdvCharacterGrayOutController/<FadeColor>c__Iterator0::to
	Color_t2020392075  ___to_2;
	// Utage.AdvEffectColor Utage.AdvCharacterGrayOutController/<FadeColor>c__Iterator0::effect
	AdvEffectColor_t2285992559 * ___effect_3;
	// Utage.AdvCharacterGrayOutController Utage.AdvCharacterGrayOutController/<FadeColor>c__Iterator0::$this
	AdvCharacterGrayOutController_t895965523 * ___U24this_4;
	// System.Object Utage.AdvCharacterGrayOutController/<FadeColor>c__Iterator0::$current
	RuntimeObject * ___U24current_5;
	// System.Boolean Utage.AdvCharacterGrayOutController/<FadeColor>c__Iterator0::$disposing
	bool ___U24disposing_6;
	// System.Int32 Utage.AdvCharacterGrayOutController/<FadeColor>c__Iterator0::$PC
	int32_t ___U24PC_7;

public:
	inline static int32_t get_offset_of_U3CelapsedU3E__0_0() { return static_cast<int32_t>(offsetof(U3CFadeColorU3Ec__Iterator0_t3211073950, ___U3CelapsedU3E__0_0)); }
	inline float get_U3CelapsedU3E__0_0() const { return ___U3CelapsedU3E__0_0; }
	inline float* get_address_of_U3CelapsedU3E__0_0() { return &___U3CelapsedU3E__0_0; }
	inline void set_U3CelapsedU3E__0_0(float value)
	{
		___U3CelapsedU3E__0_0 = value;
	}

	inline static int32_t get_offset_of_from_1() { return static_cast<int32_t>(offsetof(U3CFadeColorU3Ec__Iterator0_t3211073950, ___from_1)); }
	inline Color_t2020392075  get_from_1() const { return ___from_1; }
	inline Color_t2020392075 * get_address_of_from_1() { return &___from_1; }
	inline void set_from_1(Color_t2020392075  value)
	{
		___from_1 = value;
	}

	inline static int32_t get_offset_of_to_2() { return static_cast<int32_t>(offsetof(U3CFadeColorU3Ec__Iterator0_t3211073950, ___to_2)); }
	inline Color_t2020392075  get_to_2() const { return ___to_2; }
	inline Color_t2020392075 * get_address_of_to_2() { return &___to_2; }
	inline void set_to_2(Color_t2020392075  value)
	{
		___to_2 = value;
	}

	inline static int32_t get_offset_of_effect_3() { return static_cast<int32_t>(offsetof(U3CFadeColorU3Ec__Iterator0_t3211073950, ___effect_3)); }
	inline AdvEffectColor_t2285992559 * get_effect_3() const { return ___effect_3; }
	inline AdvEffectColor_t2285992559 ** get_address_of_effect_3() { return &___effect_3; }
	inline void set_effect_3(AdvEffectColor_t2285992559 * value)
	{
		___effect_3 = value;
		Il2CppCodeGenWriteBarrier((&___effect_3), value);
	}

	inline static int32_t get_offset_of_U24this_4() { return static_cast<int32_t>(offsetof(U3CFadeColorU3Ec__Iterator0_t3211073950, ___U24this_4)); }
	inline AdvCharacterGrayOutController_t895965523 * get_U24this_4() const { return ___U24this_4; }
	inline AdvCharacterGrayOutController_t895965523 ** get_address_of_U24this_4() { return &___U24this_4; }
	inline void set_U24this_4(AdvCharacterGrayOutController_t895965523 * value)
	{
		___U24this_4 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_4), value);
	}

	inline static int32_t get_offset_of_U24current_5() { return static_cast<int32_t>(offsetof(U3CFadeColorU3Ec__Iterator0_t3211073950, ___U24current_5)); }
	inline RuntimeObject * get_U24current_5() const { return ___U24current_5; }
	inline RuntimeObject ** get_address_of_U24current_5() { return &___U24current_5; }
	inline void set_U24current_5(RuntimeObject * value)
	{
		___U24current_5 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_5), value);
	}

	inline static int32_t get_offset_of_U24disposing_6() { return static_cast<int32_t>(offsetof(U3CFadeColorU3Ec__Iterator0_t3211073950, ___U24disposing_6)); }
	inline bool get_U24disposing_6() const { return ___U24disposing_6; }
	inline bool* get_address_of_U24disposing_6() { return &___U24disposing_6; }
	inline void set_U24disposing_6(bool value)
	{
		___U24disposing_6 = value;
	}

	inline static int32_t get_offset_of_U24PC_7() { return static_cast<int32_t>(offsetof(U3CFadeColorU3Ec__Iterator0_t3211073950, ___U24PC_7)); }
	inline int32_t get_U24PC_7() const { return ___U24PC_7; }
	inline int32_t* get_address_of_U24PC_7() { return &___U24PC_7; }
	inline void set_U24PC_7(int32_t value)
	{
		___U24PC_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CFADECOLORU3EC__ITERATOR0_T3211073950_H
#ifndef ADVCHARACTERSETTING_T2712258826_H
#define ADVCHARACTERSETTING_T2712258826_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.AdvCharacterSetting
struct  AdvCharacterSetting_t2712258826  : public AdvSettingDataDictinoayBase_1_t1418954748
{
public:
	// Utage.DictionaryString Utage.AdvCharacterSetting::defaultKey
	DictionaryString_t3376238631 * ___defaultKey_3;

public:
	inline static int32_t get_offset_of_defaultKey_3() { return static_cast<int32_t>(offsetof(AdvCharacterSetting_t2712258826, ___defaultKey_3)); }
	inline DictionaryString_t3376238631 * get_defaultKey_3() const { return ___defaultKey_3; }
	inline DictionaryString_t3376238631 ** get_address_of_defaultKey_3() { return &___defaultKey_3; }
	inline void set_defaultKey_3(DictionaryString_t3376238631 * value)
	{
		___defaultKey_3 = value;
		Il2CppCodeGenWriteBarrier((&___defaultKey_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ADVCHARACTERSETTING_T2712258826_H
#ifndef ADVTEXTURESETTING_T2060921594_H
#define ADVTEXTURESETTING_T2060921594_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.AdvTextureSetting
struct  AdvTextureSetting_t2060921594  : public AdvSettingDataDictinoayBase_1_t2723021772
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ADVTEXTURESETTING_T2060921594_H
#ifndef OBJECT_T1021602117_H
#define OBJECT_T1021602117_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Object
struct  Object_t1021602117  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Object::m_CachedPtr
	intptr_t ___m_CachedPtr_0;

public:
	inline static int32_t get_offset_of_m_CachedPtr_0() { return static_cast<int32_t>(offsetof(Object_t1021602117, ___m_CachedPtr_0)); }
	inline intptr_t get_m_CachedPtr_0() const { return ___m_CachedPtr_0; }
	inline intptr_t* get_address_of_m_CachedPtr_0() { return &___m_CachedPtr_0; }
	inline void set_m_CachedPtr_0(intptr_t value)
	{
		___m_CachedPtr_0 = value;
	}
};

struct Object_t1021602117_StaticFields
{
public:
	// System.Int32 UnityEngine.Object::OffsetOfInstanceIDInCPlusPlusObject
	int32_t ___OffsetOfInstanceIDInCPlusPlusObject_1;

public:
	inline static int32_t get_offset_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return static_cast<int32_t>(offsetof(Object_t1021602117_StaticFields, ___OffsetOfInstanceIDInCPlusPlusObject_1)); }
	inline int32_t get_OffsetOfInstanceIDInCPlusPlusObject_1() const { return ___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline int32_t* get_address_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return &___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline void set_OffsetOfInstanceIDInCPlusPlusObject_1(int32_t value)
	{
		___OffsetOfInstanceIDInCPlusPlusObject_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.Object
struct Object_t1021602117_marshaled_pinvoke
{
	intptr_t ___m_CachedPtr_0;
};
// Native definition for COM marshalling of UnityEngine.Object
struct Object_t1021602117_marshaled_com
{
	intptr_t ___m_CachedPtr_0;
};
#endif // OBJECT_T1021602117_H
#ifndef U3CWRITEU3EC__ANONSTOREY0_T894999320_H
#define U3CWRITEU3EC__ANONSTOREY0_T894999320_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.AdvParamManager/<Write>c__AnonStorey0
struct  U3CWriteU3Ec__AnonStorey0_t894999320  : public RuntimeObject
{
public:
	// System.Collections.Generic.KeyValuePair`2<System.String,Utage.AdvParamStructTbl> Utage.AdvParamManager/<Write>c__AnonStorey0::keyValue
	KeyValuePair_2_t666191687  ___keyValue_0;
	// Utage.AdvParamManager/<Write>c__AnonStorey1 Utage.AdvParamManager/<Write>c__AnonStorey0::<>f__ref$1
	U3CWriteU3Ec__AnonStorey1_t894999319 * ___U3CU3Ef__refU241_1;

public:
	inline static int32_t get_offset_of_keyValue_0() { return static_cast<int32_t>(offsetof(U3CWriteU3Ec__AnonStorey0_t894999320, ___keyValue_0)); }
	inline KeyValuePair_2_t666191687  get_keyValue_0() const { return ___keyValue_0; }
	inline KeyValuePair_2_t666191687 * get_address_of_keyValue_0() { return &___keyValue_0; }
	inline void set_keyValue_0(KeyValuePair_2_t666191687  value)
	{
		___keyValue_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3Ef__refU241_1() { return static_cast<int32_t>(offsetof(U3CWriteU3Ec__AnonStorey0_t894999320, ___U3CU3Ef__refU241_1)); }
	inline U3CWriteU3Ec__AnonStorey1_t894999319 * get_U3CU3Ef__refU241_1() const { return ___U3CU3Ef__refU241_1; }
	inline U3CWriteU3Ec__AnonStorey1_t894999319 ** get_address_of_U3CU3Ef__refU241_1() { return &___U3CU3Ef__refU241_1; }
	inline void set_U3CU3Ef__refU241_1(U3CWriteU3Ec__AnonStorey1_t894999319 * value)
	{
		___U3CU3Ef__refU241_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__refU241_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CWRITEU3EC__ANONSTOREY0_T894999320_H
#ifndef ADVSCENEGALLERYSETTING_T2241589337_H
#define ADVSCENEGALLERYSETTING_T2241589337_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.AdvSceneGallerySetting
struct  AdvSceneGallerySetting_t2241589337  : public AdvSettingDataDictinoayBase_1_t1027612555
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ADVSCENEGALLERYSETTING_T2241589337_H
#ifndef ADVCOLUMNNAME_T578583962_H
#define ADVCOLUMNNAME_T578583962_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.AdvColumnName
struct  AdvColumnName_t578583962 
{
public:
	// System.Int32 Utage.AdvColumnName::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(AdvColumnName_t578583962, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ADVCOLUMNNAME_T578583962_H
#ifndef SOUNDTYPE_T2173686033_H
#define SOUNDTYPE_T2173686033_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.SoundType
struct  SoundType_t2173686033 
{
public:
	// System.Int32 Utage.SoundType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(SoundType_t2173686033, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SOUNDTYPE_T2173686033_H
#ifndef FILETYPE_T1382753447_H
#define FILETYPE_T1382753447_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.AdvParamData/FileType
struct  FileType_t1382753447 
{
public:
	// System.Int32 Utage.AdvParamData/FileType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(FileType_t1382753447, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FILETYPE_T1382753447_H
#ifndef PARAMTYPE_T2968970818_H
#define PARAMTYPE_T2968970818_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.AdvParamData/ParamType
struct  ParamType_t2968970818 
{
public:
	// System.Int32 Utage.AdvParamData/ParamType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(ParamType_t2968970818, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PARAMTYPE_T2968970818_H
#ifndef ENUMERATOR_T1486349407_H
#define ENUMERATOR_T1486349407_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.Dictionary`2/Enumerator<System.String,Utage.AdvScenarioData>
struct  Enumerator_t1486349407 
{
public:
	// System.Collections.Generic.Dictionary`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator::dictionary
	Dictionary_2_t166324705 * ___dictionary_0;
	// System.Int32 System.Collections.Generic.Dictionary`2/Enumerator::next
	int32_t ___next_1;
	// System.Int32 System.Collections.Generic.Dictionary`2/Enumerator::stamp
	int32_t ___stamp_2;
	// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator::current
	KeyValuePair_2_t2218637223  ___current_3;

public:
	inline static int32_t get_offset_of_dictionary_0() { return static_cast<int32_t>(offsetof(Enumerator_t1486349407, ___dictionary_0)); }
	inline Dictionary_2_t166324705 * get_dictionary_0() const { return ___dictionary_0; }
	inline Dictionary_2_t166324705 ** get_address_of_dictionary_0() { return &___dictionary_0; }
	inline void set_dictionary_0(Dictionary_2_t166324705 * value)
	{
		___dictionary_0 = value;
		Il2CppCodeGenWriteBarrier((&___dictionary_0), value);
	}

	inline static int32_t get_offset_of_next_1() { return static_cast<int32_t>(offsetof(Enumerator_t1486349407, ___next_1)); }
	inline int32_t get_next_1() const { return ___next_1; }
	inline int32_t* get_address_of_next_1() { return &___next_1; }
	inline void set_next_1(int32_t value)
	{
		___next_1 = value;
	}

	inline static int32_t get_offset_of_stamp_2() { return static_cast<int32_t>(offsetof(Enumerator_t1486349407, ___stamp_2)); }
	inline int32_t get_stamp_2() const { return ___stamp_2; }
	inline int32_t* get_address_of_stamp_2() { return &___stamp_2; }
	inline void set_stamp_2(int32_t value)
	{
		___stamp_2 = value;
	}

	inline static int32_t get_offset_of_current_3() { return static_cast<int32_t>(offsetof(Enumerator_t1486349407, ___current_3)); }
	inline KeyValuePair_2_t2218637223  get_current_3() const { return ___current_3; }
	inline KeyValuePair_2_t2218637223 * get_address_of_current_3() { return &___current_3; }
	inline void set_current_3(KeyValuePair_2_t2218637223  value)
	{
		___current_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENUMERATOR_T1486349407_H
#ifndef ADVVIDEOSETTING_T3960366418_H
#define ADVVIDEOSETTING_T3960366418_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.AdvVideoSetting
struct  AdvVideoSetting_t3960366418  : public AdvSettingDataDictinoayBase_1_t2489327076
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ADVVIDEOSETTING_T3960366418_H
#ifndef PROPERTYTYPE_T3955667347_H
#define PROPERTYTYPE_T3955667347_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.AdvAnimationData/PropertyType
struct  PropertyType_t3955667347 
{
public:
	// System.Int32 Utage.AdvAnimationData/PropertyType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(PropertyType_t3955667347, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PROPERTYTYPE_T3955667347_H
#ifndef DELEGATE_T3022476291_H
#define DELEGATE_T3022476291_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Delegate
struct  Delegate_t3022476291  : public RuntimeObject
{
public:
	// System.IntPtr System.Delegate::method_ptr
	Il2CppMethodPointer ___method_ptr_0;
	// System.IntPtr System.Delegate::invoke_impl
	intptr_t ___invoke_impl_1;
	// System.Object System.Delegate::m_target
	RuntimeObject * ___m_target_2;
	// System.IntPtr System.Delegate::method
	intptr_t ___method_3;
	// System.IntPtr System.Delegate::delegate_trampoline
	intptr_t ___delegate_trampoline_4;
	// System.IntPtr System.Delegate::method_code
	intptr_t ___method_code_5;
	// System.Reflection.MethodInfo System.Delegate::method_info
	MethodInfo_t * ___method_info_6;
	// System.Reflection.MethodInfo System.Delegate::original_method_info
	MethodInfo_t * ___original_method_info_7;
	// System.DelegateData System.Delegate::data
	DelegateData_t1572802995 * ___data_8;

public:
	inline static int32_t get_offset_of_method_ptr_0() { return static_cast<int32_t>(offsetof(Delegate_t3022476291, ___method_ptr_0)); }
	inline Il2CppMethodPointer get_method_ptr_0() const { return ___method_ptr_0; }
	inline Il2CppMethodPointer* get_address_of_method_ptr_0() { return &___method_ptr_0; }
	inline void set_method_ptr_0(Il2CppMethodPointer value)
	{
		___method_ptr_0 = value;
	}

	inline static int32_t get_offset_of_invoke_impl_1() { return static_cast<int32_t>(offsetof(Delegate_t3022476291, ___invoke_impl_1)); }
	inline intptr_t get_invoke_impl_1() const { return ___invoke_impl_1; }
	inline intptr_t* get_address_of_invoke_impl_1() { return &___invoke_impl_1; }
	inline void set_invoke_impl_1(intptr_t value)
	{
		___invoke_impl_1 = value;
	}

	inline static int32_t get_offset_of_m_target_2() { return static_cast<int32_t>(offsetof(Delegate_t3022476291, ___m_target_2)); }
	inline RuntimeObject * get_m_target_2() const { return ___m_target_2; }
	inline RuntimeObject ** get_address_of_m_target_2() { return &___m_target_2; }
	inline void set_m_target_2(RuntimeObject * value)
	{
		___m_target_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_target_2), value);
	}

	inline static int32_t get_offset_of_method_3() { return static_cast<int32_t>(offsetof(Delegate_t3022476291, ___method_3)); }
	inline intptr_t get_method_3() const { return ___method_3; }
	inline intptr_t* get_address_of_method_3() { return &___method_3; }
	inline void set_method_3(intptr_t value)
	{
		___method_3 = value;
	}

	inline static int32_t get_offset_of_delegate_trampoline_4() { return static_cast<int32_t>(offsetof(Delegate_t3022476291, ___delegate_trampoline_4)); }
	inline intptr_t get_delegate_trampoline_4() const { return ___delegate_trampoline_4; }
	inline intptr_t* get_address_of_delegate_trampoline_4() { return &___delegate_trampoline_4; }
	inline void set_delegate_trampoline_4(intptr_t value)
	{
		___delegate_trampoline_4 = value;
	}

	inline static int32_t get_offset_of_method_code_5() { return static_cast<int32_t>(offsetof(Delegate_t3022476291, ___method_code_5)); }
	inline intptr_t get_method_code_5() const { return ___method_code_5; }
	inline intptr_t* get_address_of_method_code_5() { return &___method_code_5; }
	inline void set_method_code_5(intptr_t value)
	{
		___method_code_5 = value;
	}

	inline static int32_t get_offset_of_method_info_6() { return static_cast<int32_t>(offsetof(Delegate_t3022476291, ___method_info_6)); }
	inline MethodInfo_t * get_method_info_6() const { return ___method_info_6; }
	inline MethodInfo_t ** get_address_of_method_info_6() { return &___method_info_6; }
	inline void set_method_info_6(MethodInfo_t * value)
	{
		___method_info_6 = value;
		Il2CppCodeGenWriteBarrier((&___method_info_6), value);
	}

	inline static int32_t get_offset_of_original_method_info_7() { return static_cast<int32_t>(offsetof(Delegate_t3022476291, ___original_method_info_7)); }
	inline MethodInfo_t * get_original_method_info_7() const { return ___original_method_info_7; }
	inline MethodInfo_t ** get_address_of_original_method_info_7() { return &___original_method_info_7; }
	inline void set_original_method_info_7(MethodInfo_t * value)
	{
		___original_method_info_7 = value;
		Il2CppCodeGenWriteBarrier((&___original_method_info_7), value);
	}

	inline static int32_t get_offset_of_data_8() { return static_cast<int32_t>(offsetof(Delegate_t3022476291, ___data_8)); }
	inline DelegateData_t1572802995 * get_data_8() const { return ___data_8; }
	inline DelegateData_t1572802995 ** get_address_of_data_8() { return &___data_8; }
	inline void set_data_8(DelegateData_t1572802995 * value)
	{
		___data_8 = value;
		Il2CppCodeGenWriteBarrier((&___data_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DELEGATE_T3022476291_H
#ifndef U3CWRITEU3EC__ANONSTOREY0_T2299255854_H
#define U3CWRITEU3EC__ANONSTOREY0_T2299255854_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.AdvParamStructTbl/<Write>c__AnonStorey0
struct  U3CWriteU3Ec__AnonStorey0_t2299255854  : public RuntimeObject
{
public:
	// System.Collections.Generic.KeyValuePair`2<System.String,Utage.AdvParamStruct> Utage.AdvParamStructTbl/<Write>c__AnonStorey0::keyValue
	KeyValuePair_2_t1568511417  ___keyValue_0;
	// Utage.AdvParamStructTbl/<Write>c__AnonStorey1 Utage.AdvParamStructTbl/<Write>c__AnonStorey0::<>f__ref$1
	U3CWriteU3Ec__AnonStorey1_t2299255853 * ___U3CU3Ef__refU241_1;

public:
	inline static int32_t get_offset_of_keyValue_0() { return static_cast<int32_t>(offsetof(U3CWriteU3Ec__AnonStorey0_t2299255854, ___keyValue_0)); }
	inline KeyValuePair_2_t1568511417  get_keyValue_0() const { return ___keyValue_0; }
	inline KeyValuePair_2_t1568511417 * get_address_of_keyValue_0() { return &___keyValue_0; }
	inline void set_keyValue_0(KeyValuePair_2_t1568511417  value)
	{
		___keyValue_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3Ef__refU241_1() { return static_cast<int32_t>(offsetof(U3CWriteU3Ec__AnonStorey0_t2299255854, ___U3CU3Ef__refU241_1)); }
	inline U3CWriteU3Ec__AnonStorey1_t2299255853 * get_U3CU3Ef__refU241_1() const { return ___U3CU3Ef__refU241_1; }
	inline U3CWriteU3Ec__AnonStorey1_t2299255853 ** get_address_of_U3CU3Ef__refU241_1() { return &___U3CU3Ef__refU241_1; }
	inline void set_U3CU3Ef__refU241_1(U3CWriteU3Ec__AnonStorey1_t2299255853 * value)
	{
		___U3CU3Ef__refU241_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__refU241_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CWRITEU3EC__ANONSTOREY0_T2299255854_H
#ifndef WRAPMODE_T255797857_H
#define WRAPMODE_T255797857_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.WrapMode
struct  WrapMode_t255797857 
{
public:
	// System.Int32 UnityEngine.WrapMode::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(WrapMode_t255797857, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WRAPMODE_T255797857_H
#ifndef ADVSOUNDSETTING_T229690022_H
#define ADVSOUNDSETTING_T229690022_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.AdvSoundSetting
struct  AdvSoundSetting_t229690022  : public AdvSettingDataDictinoayBase_1_t2882400728
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ADVSOUNDSETTING_T229690022_H
#ifndef ADVPARTICLESETTING_T588078687_H
#define ADVPARTICLESETTING_T588078687_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.AdvParticleSetting
struct  AdvParticleSetting_t588078687  : public AdvSettingDataDictinoayBase_1_t1350530129
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ADVPARTICLESETTING_T588078687_H
#ifndef COMPONENT_T3819376471_H
#define COMPONENT_T3819376471_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Component
struct  Component_t3819376471  : public Object_t1021602117
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPONENT_T3819376471_H
#ifndef ENUMERATOR_T1852857469_H
#define ENUMERATOR_T1852857469_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.String,Utage.AdvScenarioData>
struct  Enumerator_t1852857469 
{
public:
	// System.Collections.Generic.Dictionary`2/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator::host_enumerator
	Enumerator_t1486349407  ___host_enumerator_0;

public:
	inline static int32_t get_offset_of_host_enumerator_0() { return static_cast<int32_t>(offsetof(Enumerator_t1852857469, ___host_enumerator_0)); }
	inline Enumerator_t1486349407  get_host_enumerator_0() const { return ___host_enumerator_0; }
	inline Enumerator_t1486349407 * get_address_of_host_enumerator_0() { return &___host_enumerator_0; }
	inline void set_host_enumerator_0(Enumerator_t1486349407  value)
	{
		___host_enumerator_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENUMERATOR_T1852857469_H
#ifndef STRINGGRID_T1872153679_H
#define STRINGGRID_T1872153679_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.StringGrid
struct  StringGrid_t1872153679  : public RuntimeObject
{
public:
	// System.Collections.Generic.List`1<Utage.StringGridRow> Utage.StringGrid::rows
	List_1_t3562358329 * ___rows_0;
	// System.String Utage.StringGrid::name
	String_t* ___name_1;
	// System.String Utage.StringGrid::sheetName
	String_t* ___sheetName_2;
	// Utage.CsvType Utage.StringGrid::type
	int32_t ___type_3;
	// System.Int32 Utage.StringGrid::textLength
	int32_t ___textLength_4;
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> Utage.StringGrid::columnIndexTbl
	Dictionary_2_t3986656710 * ___columnIndexTbl_5;
	// System.Int32 Utage.StringGrid::headerRow
	int32_t ___headerRow_6;

public:
	inline static int32_t get_offset_of_rows_0() { return static_cast<int32_t>(offsetof(StringGrid_t1872153679, ___rows_0)); }
	inline List_1_t3562358329 * get_rows_0() const { return ___rows_0; }
	inline List_1_t3562358329 ** get_address_of_rows_0() { return &___rows_0; }
	inline void set_rows_0(List_1_t3562358329 * value)
	{
		___rows_0 = value;
		Il2CppCodeGenWriteBarrier((&___rows_0), value);
	}

	inline static int32_t get_offset_of_name_1() { return static_cast<int32_t>(offsetof(StringGrid_t1872153679, ___name_1)); }
	inline String_t* get_name_1() const { return ___name_1; }
	inline String_t** get_address_of_name_1() { return &___name_1; }
	inline void set_name_1(String_t* value)
	{
		___name_1 = value;
		Il2CppCodeGenWriteBarrier((&___name_1), value);
	}

	inline static int32_t get_offset_of_sheetName_2() { return static_cast<int32_t>(offsetof(StringGrid_t1872153679, ___sheetName_2)); }
	inline String_t* get_sheetName_2() const { return ___sheetName_2; }
	inline String_t** get_address_of_sheetName_2() { return &___sheetName_2; }
	inline void set_sheetName_2(String_t* value)
	{
		___sheetName_2 = value;
		Il2CppCodeGenWriteBarrier((&___sheetName_2), value);
	}

	inline static int32_t get_offset_of_type_3() { return static_cast<int32_t>(offsetof(StringGrid_t1872153679, ___type_3)); }
	inline int32_t get_type_3() const { return ___type_3; }
	inline int32_t* get_address_of_type_3() { return &___type_3; }
	inline void set_type_3(int32_t value)
	{
		___type_3 = value;
	}

	inline static int32_t get_offset_of_textLength_4() { return static_cast<int32_t>(offsetof(StringGrid_t1872153679, ___textLength_4)); }
	inline int32_t get_textLength_4() const { return ___textLength_4; }
	inline int32_t* get_address_of_textLength_4() { return &___textLength_4; }
	inline void set_textLength_4(int32_t value)
	{
		___textLength_4 = value;
	}

	inline static int32_t get_offset_of_columnIndexTbl_5() { return static_cast<int32_t>(offsetof(StringGrid_t1872153679, ___columnIndexTbl_5)); }
	inline Dictionary_2_t3986656710 * get_columnIndexTbl_5() const { return ___columnIndexTbl_5; }
	inline Dictionary_2_t3986656710 ** get_address_of_columnIndexTbl_5() { return &___columnIndexTbl_5; }
	inline void set_columnIndexTbl_5(Dictionary_2_t3986656710 * value)
	{
		___columnIndexTbl_5 = value;
		Il2CppCodeGenWriteBarrier((&___columnIndexTbl_5), value);
	}

	inline static int32_t get_offset_of_headerRow_6() { return static_cast<int32_t>(offsetof(StringGrid_t1872153679, ___headerRow_6)); }
	inline int32_t get_headerRow_6() const { return ___headerRow_6; }
	inline int32_t* get_address_of_headerRow_6() { return &___headerRow_6; }
	inline void set_headerRow_6(int32_t value)
	{
		___headerRow_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STRINGGRID_T1872153679_H
#ifndef MULTICASTDELEGATE_T3201952435_H
#define MULTICASTDELEGATE_T3201952435_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.MulticastDelegate
struct  MulticastDelegate_t3201952435  : public Delegate_t3022476291
{
public:
	// System.MulticastDelegate System.MulticastDelegate::prev
	MulticastDelegate_t3201952435 * ___prev_9;
	// System.MulticastDelegate System.MulticastDelegate::kpm_next
	MulticastDelegate_t3201952435 * ___kpm_next_10;

public:
	inline static int32_t get_offset_of_prev_9() { return static_cast<int32_t>(offsetof(MulticastDelegate_t3201952435, ___prev_9)); }
	inline MulticastDelegate_t3201952435 * get_prev_9() const { return ___prev_9; }
	inline MulticastDelegate_t3201952435 ** get_address_of_prev_9() { return &___prev_9; }
	inline void set_prev_9(MulticastDelegate_t3201952435 * value)
	{
		___prev_9 = value;
		Il2CppCodeGenWriteBarrier((&___prev_9), value);
	}

	inline static int32_t get_offset_of_kpm_next_10() { return static_cast<int32_t>(offsetof(MulticastDelegate_t3201952435, ___kpm_next_10)); }
	inline MulticastDelegate_t3201952435 * get_kpm_next_10() const { return ___kpm_next_10; }
	inline MulticastDelegate_t3201952435 ** get_address_of_kpm_next_10() { return &___kpm_next_10; }
	inline void set_kpm_next_10(MulticastDelegate_t3201952435 * value)
	{
		___kpm_next_10 = value;
		Il2CppCodeGenWriteBarrier((&___kpm_next_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MULTICASTDELEGATE_T3201952435_H
#ifndef ICONINFO_T3287921618_H
#define ICONINFO_T3287921618_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.AdvCharacterSettingData/IconInfo
struct  IconInfo_t3287921618  : public RuntimeObject
{
public:
	// Utage.AdvCharacterSettingData/IconInfo/Type Utage.AdvCharacterSettingData/IconInfo::<IconType>k__BackingField
	int32_t ___U3CIconTypeU3Ek__BackingField_0;
	// System.String Utage.AdvCharacterSettingData/IconInfo::<FileName>k__BackingField
	String_t* ___U3CFileNameU3Ek__BackingField_1;
	// Utage.AssetFile Utage.AdvCharacterSettingData/IconInfo::<File>k__BackingField
	RuntimeObject* ___U3CFileU3Ek__BackingField_2;
	// UnityEngine.Rect Utage.AdvCharacterSettingData/IconInfo::<IconRect>k__BackingField
	Rect_t3681755626  ___U3CIconRectU3Ek__BackingField_3;
	// System.String Utage.AdvCharacterSettingData/IconInfo::<IconSubFileName>k__BackingField
	String_t* ___U3CIconSubFileNameU3Ek__BackingField_4;

public:
	inline static int32_t get_offset_of_U3CIconTypeU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(IconInfo_t3287921618, ___U3CIconTypeU3Ek__BackingField_0)); }
	inline int32_t get_U3CIconTypeU3Ek__BackingField_0() const { return ___U3CIconTypeU3Ek__BackingField_0; }
	inline int32_t* get_address_of_U3CIconTypeU3Ek__BackingField_0() { return &___U3CIconTypeU3Ek__BackingField_0; }
	inline void set_U3CIconTypeU3Ek__BackingField_0(int32_t value)
	{
		___U3CIconTypeU3Ek__BackingField_0 = value;
	}

	inline static int32_t get_offset_of_U3CFileNameU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(IconInfo_t3287921618, ___U3CFileNameU3Ek__BackingField_1)); }
	inline String_t* get_U3CFileNameU3Ek__BackingField_1() const { return ___U3CFileNameU3Ek__BackingField_1; }
	inline String_t** get_address_of_U3CFileNameU3Ek__BackingField_1() { return &___U3CFileNameU3Ek__BackingField_1; }
	inline void set_U3CFileNameU3Ek__BackingField_1(String_t* value)
	{
		___U3CFileNameU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CFileNameU3Ek__BackingField_1), value);
	}

	inline static int32_t get_offset_of_U3CFileU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(IconInfo_t3287921618, ___U3CFileU3Ek__BackingField_2)); }
	inline RuntimeObject* get_U3CFileU3Ek__BackingField_2() const { return ___U3CFileU3Ek__BackingField_2; }
	inline RuntimeObject** get_address_of_U3CFileU3Ek__BackingField_2() { return &___U3CFileU3Ek__BackingField_2; }
	inline void set_U3CFileU3Ek__BackingField_2(RuntimeObject* value)
	{
		___U3CFileU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CFileU3Ek__BackingField_2), value);
	}

	inline static int32_t get_offset_of_U3CIconRectU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(IconInfo_t3287921618, ___U3CIconRectU3Ek__BackingField_3)); }
	inline Rect_t3681755626  get_U3CIconRectU3Ek__BackingField_3() const { return ___U3CIconRectU3Ek__BackingField_3; }
	inline Rect_t3681755626 * get_address_of_U3CIconRectU3Ek__BackingField_3() { return &___U3CIconRectU3Ek__BackingField_3; }
	inline void set_U3CIconRectU3Ek__BackingField_3(Rect_t3681755626  value)
	{
		___U3CIconRectU3Ek__BackingField_3 = value;
	}

	inline static int32_t get_offset_of_U3CIconSubFileNameU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(IconInfo_t3287921618, ___U3CIconSubFileNameU3Ek__BackingField_4)); }
	inline String_t* get_U3CIconSubFileNameU3Ek__BackingField_4() const { return ___U3CIconSubFileNameU3Ek__BackingField_4; }
	inline String_t** get_address_of_U3CIconSubFileNameU3Ek__BackingField_4() { return &___U3CIconSubFileNameU3Ek__BackingField_4; }
	inline void set_U3CIconSubFileNameU3Ek__BackingField_4(String_t* value)
	{
		___U3CIconSubFileNameU3Ek__BackingField_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CIconSubFileNameU3Ek__BackingField_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ICONINFO_T3287921618_H
#ifndef ADVLAYERSETTINGDATA_T1908772360_H
#define ADVLAYERSETTINGDATA_T1908772360_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.AdvLayerSettingData
struct  AdvLayerSettingData_t1908772360  : public AdvSettingDictinoayItemBase_t203751633
{
public:
	// Utage.AdvLayerSettingData/LayerType Utage.AdvLayerSettingData::<Type>k__BackingField
	int32_t ___U3CTypeU3Ek__BackingField_2;
	// Utage.AdvLayerSettingData/RectSetting Utage.AdvLayerSettingData::<Horizontal>k__BackingField
	RectSetting_t1088783529 * ___U3CHorizontalU3Ek__BackingField_3;
	// Utage.AdvLayerSettingData/RectSetting Utage.AdvLayerSettingData::<Vertical>k__BackingField
	RectSetting_t1088783529 * ___U3CVerticalU3Ek__BackingField_4;
	// System.Single Utage.AdvLayerSettingData::<Z>k__BackingField
	float ___U3CZU3Ek__BackingField_5;
	// UnityEngine.Vector3 Utage.AdvLayerSettingData::<Scale>k__BackingField
	Vector3_t2243707580  ___U3CScaleU3Ek__BackingField_6;
	// UnityEngine.Vector2 Utage.AdvLayerSettingData::<Pivot>k__BackingField
	Vector2_t2243707579  ___U3CPivotU3Ek__BackingField_7;
	// System.Int32 Utage.AdvLayerSettingData::<Order>k__BackingField
	int32_t ___U3COrderU3Ek__BackingField_8;
	// System.String Utage.AdvLayerSettingData::<LayerMask>k__BackingField
	String_t* ___U3CLayerMaskU3Ek__BackingField_9;
	// Utage.Alignment Utage.AdvLayerSettingData::<Alignment>k__BackingField
	int32_t ___U3CAlignmentU3Ek__BackingField_10;
	// System.Boolean Utage.AdvLayerSettingData::<FlipX>k__BackingField
	bool ___U3CFlipXU3Ek__BackingField_11;
	// System.Boolean Utage.AdvLayerSettingData::<FlipY>k__BackingField
	bool ___U3CFlipYU3Ek__BackingField_12;
	// System.Boolean Utage.AdvLayerSettingData::isDefault
	bool ___isDefault_13;

public:
	inline static int32_t get_offset_of_U3CTypeU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(AdvLayerSettingData_t1908772360, ___U3CTypeU3Ek__BackingField_2)); }
	inline int32_t get_U3CTypeU3Ek__BackingField_2() const { return ___U3CTypeU3Ek__BackingField_2; }
	inline int32_t* get_address_of_U3CTypeU3Ek__BackingField_2() { return &___U3CTypeU3Ek__BackingField_2; }
	inline void set_U3CTypeU3Ek__BackingField_2(int32_t value)
	{
		___U3CTypeU3Ek__BackingField_2 = value;
	}

	inline static int32_t get_offset_of_U3CHorizontalU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(AdvLayerSettingData_t1908772360, ___U3CHorizontalU3Ek__BackingField_3)); }
	inline RectSetting_t1088783529 * get_U3CHorizontalU3Ek__BackingField_3() const { return ___U3CHorizontalU3Ek__BackingField_3; }
	inline RectSetting_t1088783529 ** get_address_of_U3CHorizontalU3Ek__BackingField_3() { return &___U3CHorizontalU3Ek__BackingField_3; }
	inline void set_U3CHorizontalU3Ek__BackingField_3(RectSetting_t1088783529 * value)
	{
		___U3CHorizontalU3Ek__BackingField_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CHorizontalU3Ek__BackingField_3), value);
	}

	inline static int32_t get_offset_of_U3CVerticalU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(AdvLayerSettingData_t1908772360, ___U3CVerticalU3Ek__BackingField_4)); }
	inline RectSetting_t1088783529 * get_U3CVerticalU3Ek__BackingField_4() const { return ___U3CVerticalU3Ek__BackingField_4; }
	inline RectSetting_t1088783529 ** get_address_of_U3CVerticalU3Ek__BackingField_4() { return &___U3CVerticalU3Ek__BackingField_4; }
	inline void set_U3CVerticalU3Ek__BackingField_4(RectSetting_t1088783529 * value)
	{
		___U3CVerticalU3Ek__BackingField_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CVerticalU3Ek__BackingField_4), value);
	}

	inline static int32_t get_offset_of_U3CZU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(AdvLayerSettingData_t1908772360, ___U3CZU3Ek__BackingField_5)); }
	inline float get_U3CZU3Ek__BackingField_5() const { return ___U3CZU3Ek__BackingField_5; }
	inline float* get_address_of_U3CZU3Ek__BackingField_5() { return &___U3CZU3Ek__BackingField_5; }
	inline void set_U3CZU3Ek__BackingField_5(float value)
	{
		___U3CZU3Ek__BackingField_5 = value;
	}

	inline static int32_t get_offset_of_U3CScaleU3Ek__BackingField_6() { return static_cast<int32_t>(offsetof(AdvLayerSettingData_t1908772360, ___U3CScaleU3Ek__BackingField_6)); }
	inline Vector3_t2243707580  get_U3CScaleU3Ek__BackingField_6() const { return ___U3CScaleU3Ek__BackingField_6; }
	inline Vector3_t2243707580 * get_address_of_U3CScaleU3Ek__BackingField_6() { return &___U3CScaleU3Ek__BackingField_6; }
	inline void set_U3CScaleU3Ek__BackingField_6(Vector3_t2243707580  value)
	{
		___U3CScaleU3Ek__BackingField_6 = value;
	}

	inline static int32_t get_offset_of_U3CPivotU3Ek__BackingField_7() { return static_cast<int32_t>(offsetof(AdvLayerSettingData_t1908772360, ___U3CPivotU3Ek__BackingField_7)); }
	inline Vector2_t2243707579  get_U3CPivotU3Ek__BackingField_7() const { return ___U3CPivotU3Ek__BackingField_7; }
	inline Vector2_t2243707579 * get_address_of_U3CPivotU3Ek__BackingField_7() { return &___U3CPivotU3Ek__BackingField_7; }
	inline void set_U3CPivotU3Ek__BackingField_7(Vector2_t2243707579  value)
	{
		___U3CPivotU3Ek__BackingField_7 = value;
	}

	inline static int32_t get_offset_of_U3COrderU3Ek__BackingField_8() { return static_cast<int32_t>(offsetof(AdvLayerSettingData_t1908772360, ___U3COrderU3Ek__BackingField_8)); }
	inline int32_t get_U3COrderU3Ek__BackingField_8() const { return ___U3COrderU3Ek__BackingField_8; }
	inline int32_t* get_address_of_U3COrderU3Ek__BackingField_8() { return &___U3COrderU3Ek__BackingField_8; }
	inline void set_U3COrderU3Ek__BackingField_8(int32_t value)
	{
		___U3COrderU3Ek__BackingField_8 = value;
	}

	inline static int32_t get_offset_of_U3CLayerMaskU3Ek__BackingField_9() { return static_cast<int32_t>(offsetof(AdvLayerSettingData_t1908772360, ___U3CLayerMaskU3Ek__BackingField_9)); }
	inline String_t* get_U3CLayerMaskU3Ek__BackingField_9() const { return ___U3CLayerMaskU3Ek__BackingField_9; }
	inline String_t** get_address_of_U3CLayerMaskU3Ek__BackingField_9() { return &___U3CLayerMaskU3Ek__BackingField_9; }
	inline void set_U3CLayerMaskU3Ek__BackingField_9(String_t* value)
	{
		___U3CLayerMaskU3Ek__BackingField_9 = value;
		Il2CppCodeGenWriteBarrier((&___U3CLayerMaskU3Ek__BackingField_9), value);
	}

	inline static int32_t get_offset_of_U3CAlignmentU3Ek__BackingField_10() { return static_cast<int32_t>(offsetof(AdvLayerSettingData_t1908772360, ___U3CAlignmentU3Ek__BackingField_10)); }
	inline int32_t get_U3CAlignmentU3Ek__BackingField_10() const { return ___U3CAlignmentU3Ek__BackingField_10; }
	inline int32_t* get_address_of_U3CAlignmentU3Ek__BackingField_10() { return &___U3CAlignmentU3Ek__BackingField_10; }
	inline void set_U3CAlignmentU3Ek__BackingField_10(int32_t value)
	{
		___U3CAlignmentU3Ek__BackingField_10 = value;
	}

	inline static int32_t get_offset_of_U3CFlipXU3Ek__BackingField_11() { return static_cast<int32_t>(offsetof(AdvLayerSettingData_t1908772360, ___U3CFlipXU3Ek__BackingField_11)); }
	inline bool get_U3CFlipXU3Ek__BackingField_11() const { return ___U3CFlipXU3Ek__BackingField_11; }
	inline bool* get_address_of_U3CFlipXU3Ek__BackingField_11() { return &___U3CFlipXU3Ek__BackingField_11; }
	inline void set_U3CFlipXU3Ek__BackingField_11(bool value)
	{
		___U3CFlipXU3Ek__BackingField_11 = value;
	}

	inline static int32_t get_offset_of_U3CFlipYU3Ek__BackingField_12() { return static_cast<int32_t>(offsetof(AdvLayerSettingData_t1908772360, ___U3CFlipYU3Ek__BackingField_12)); }
	inline bool get_U3CFlipYU3Ek__BackingField_12() const { return ___U3CFlipYU3Ek__BackingField_12; }
	inline bool* get_address_of_U3CFlipYU3Ek__BackingField_12() { return &___U3CFlipYU3Ek__BackingField_12; }
	inline void set_U3CFlipYU3Ek__BackingField_12(bool value)
	{
		___U3CFlipYU3Ek__BackingField_12 = value;
	}

	inline static int32_t get_offset_of_isDefault_13() { return static_cast<int32_t>(offsetof(AdvLayerSettingData_t1908772360, ___isDefault_13)); }
	inline bool get_isDefault_13() const { return ___isDefault_13; }
	inline bool* get_address_of_isDefault_13() { return &___isDefault_13; }
	inline void set_isDefault_13(bool value)
	{
		___isDefault_13 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ADVLAYERSETTINGDATA_T1908772360_H
#ifndef RECTSETTING_T1088783529_H
#define RECTSETTING_T1088783529_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.AdvLayerSettingData/RectSetting
struct  RectSetting_t1088783529  : public RuntimeObject
{
public:
	// Utage.AdvLayerSettingData/BorderType Utage.AdvLayerSettingData/RectSetting::type
	int32_t ___type_0;
	// System.Single Utage.AdvLayerSettingData/RectSetting::position
	float ___position_1;
	// System.Single Utage.AdvLayerSettingData/RectSetting::size
	float ___size_2;
	// System.Single Utage.AdvLayerSettingData/RectSetting::borderMin
	float ___borderMin_3;
	// System.Single Utage.AdvLayerSettingData/RectSetting::borderMax
	float ___borderMax_4;

public:
	inline static int32_t get_offset_of_type_0() { return static_cast<int32_t>(offsetof(RectSetting_t1088783529, ___type_0)); }
	inline int32_t get_type_0() const { return ___type_0; }
	inline int32_t* get_address_of_type_0() { return &___type_0; }
	inline void set_type_0(int32_t value)
	{
		___type_0 = value;
	}

	inline static int32_t get_offset_of_position_1() { return static_cast<int32_t>(offsetof(RectSetting_t1088783529, ___position_1)); }
	inline float get_position_1() const { return ___position_1; }
	inline float* get_address_of_position_1() { return &___position_1; }
	inline void set_position_1(float value)
	{
		___position_1 = value;
	}

	inline static int32_t get_offset_of_size_2() { return static_cast<int32_t>(offsetof(RectSetting_t1088783529, ___size_2)); }
	inline float get_size_2() const { return ___size_2; }
	inline float* get_address_of_size_2() { return &___size_2; }
	inline void set_size_2(float value)
	{
		___size_2 = value;
	}

	inline static int32_t get_offset_of_borderMin_3() { return static_cast<int32_t>(offsetof(RectSetting_t1088783529, ___borderMin_3)); }
	inline float get_borderMin_3() const { return ___borderMin_3; }
	inline float* get_address_of_borderMin_3() { return &___borderMin_3; }
	inline void set_borderMin_3(float value)
	{
		___borderMin_3 = value;
	}

	inline static int32_t get_offset_of_borderMax_4() { return static_cast<int32_t>(offsetof(RectSetting_t1088783529, ___borderMax_4)); }
	inline float get_borderMax_4() const { return ___borderMax_4; }
	inline float* get_address_of_borderMax_4() { return &___borderMax_4; }
	inline void set_borderMax_4(float value)
	{
		___borderMax_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RECTSETTING_T1088783529_H
#ifndef ADVSOUNDSETTINGDATA_T3135391222_H
#define ADVSOUNDSETTINGDATA_T3135391222_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.AdvSoundSettingData
struct  AdvSoundSettingData_t3135391222  : public AdvSettingDictinoayItemBase_t203751633
{
public:
	// Utage.SoundType Utage.AdvSoundSettingData::<Type>k__BackingField
	int32_t ___U3CTypeU3Ek__BackingField_2;
	// System.String Utage.AdvSoundSettingData::<Title>k__BackingField
	String_t* ___U3CTitleU3Ek__BackingField_3;
	// System.String Utage.AdvSoundSettingData::fileName
	String_t* ___fileName_4;
	// System.String Utage.AdvSoundSettingData::<FilePath>k__BackingField
	String_t* ___U3CFilePathU3Ek__BackingField_5;
	// System.Single Utage.AdvSoundSettingData::<IntroTime>k__BackingField
	float ___U3CIntroTimeU3Ek__BackingField_6;
	// System.Single Utage.AdvSoundSettingData::<Volume>k__BackingField
	float ___U3CVolumeU3Ek__BackingField_7;

public:
	inline static int32_t get_offset_of_U3CTypeU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(AdvSoundSettingData_t3135391222, ___U3CTypeU3Ek__BackingField_2)); }
	inline int32_t get_U3CTypeU3Ek__BackingField_2() const { return ___U3CTypeU3Ek__BackingField_2; }
	inline int32_t* get_address_of_U3CTypeU3Ek__BackingField_2() { return &___U3CTypeU3Ek__BackingField_2; }
	inline void set_U3CTypeU3Ek__BackingField_2(int32_t value)
	{
		___U3CTypeU3Ek__BackingField_2 = value;
	}

	inline static int32_t get_offset_of_U3CTitleU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(AdvSoundSettingData_t3135391222, ___U3CTitleU3Ek__BackingField_3)); }
	inline String_t* get_U3CTitleU3Ek__BackingField_3() const { return ___U3CTitleU3Ek__BackingField_3; }
	inline String_t** get_address_of_U3CTitleU3Ek__BackingField_3() { return &___U3CTitleU3Ek__BackingField_3; }
	inline void set_U3CTitleU3Ek__BackingField_3(String_t* value)
	{
		___U3CTitleU3Ek__BackingField_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CTitleU3Ek__BackingField_3), value);
	}

	inline static int32_t get_offset_of_fileName_4() { return static_cast<int32_t>(offsetof(AdvSoundSettingData_t3135391222, ___fileName_4)); }
	inline String_t* get_fileName_4() const { return ___fileName_4; }
	inline String_t** get_address_of_fileName_4() { return &___fileName_4; }
	inline void set_fileName_4(String_t* value)
	{
		___fileName_4 = value;
		Il2CppCodeGenWriteBarrier((&___fileName_4), value);
	}

	inline static int32_t get_offset_of_U3CFilePathU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(AdvSoundSettingData_t3135391222, ___U3CFilePathU3Ek__BackingField_5)); }
	inline String_t* get_U3CFilePathU3Ek__BackingField_5() const { return ___U3CFilePathU3Ek__BackingField_5; }
	inline String_t** get_address_of_U3CFilePathU3Ek__BackingField_5() { return &___U3CFilePathU3Ek__BackingField_5; }
	inline void set_U3CFilePathU3Ek__BackingField_5(String_t* value)
	{
		___U3CFilePathU3Ek__BackingField_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3CFilePathU3Ek__BackingField_5), value);
	}

	inline static int32_t get_offset_of_U3CIntroTimeU3Ek__BackingField_6() { return static_cast<int32_t>(offsetof(AdvSoundSettingData_t3135391222, ___U3CIntroTimeU3Ek__BackingField_6)); }
	inline float get_U3CIntroTimeU3Ek__BackingField_6() const { return ___U3CIntroTimeU3Ek__BackingField_6; }
	inline float* get_address_of_U3CIntroTimeU3Ek__BackingField_6() { return &___U3CIntroTimeU3Ek__BackingField_6; }
	inline void set_U3CIntroTimeU3Ek__BackingField_6(float value)
	{
		___U3CIntroTimeU3Ek__BackingField_6 = value;
	}

	inline static int32_t get_offset_of_U3CVolumeU3Ek__BackingField_7() { return static_cast<int32_t>(offsetof(AdvSoundSettingData_t3135391222, ___U3CVolumeU3Ek__BackingField_7)); }
	inline float get_U3CVolumeU3Ek__BackingField_7() const { return ___U3CVolumeU3Ek__BackingField_7; }
	inline float* get_address_of_U3CVolumeU3Ek__BackingField_7() { return &___U3CVolumeU3Ek__BackingField_7; }
	inline void set_U3CVolumeU3Ek__BackingField_7(float value)
	{
		___U3CVolumeU3Ek__BackingField_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ADVSOUNDSETTINGDATA_T3135391222_H
#ifndef ADVLIPSYNCHDATA_T1090448263_H
#define ADVLIPSYNCHDATA_T1090448263_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.AdvLipSynchData
struct  AdvLipSynchData_t1090448263  : public AdvSettingDictinoayItemBase_t203751633
{
public:
	// Utage.LipSynchType Utage.AdvLipSynchData::type
	int32_t ___type_2;
	// System.Single Utage.AdvLipSynchData::interval
	float ___interval_3;
	// System.Single Utage.AdvLipSynchData::scaleVoiceVolume
	float ___scaleVoiceVolume_4;
	// System.String Utage.AdvLipSynchData::tag
	String_t* ___tag_5;
	// Utage.MiniAnimationData Utage.AdvLipSynchData::animationData
	MiniAnimationData_t521227391 * ___animationData_6;

public:
	inline static int32_t get_offset_of_type_2() { return static_cast<int32_t>(offsetof(AdvLipSynchData_t1090448263, ___type_2)); }
	inline int32_t get_type_2() const { return ___type_2; }
	inline int32_t* get_address_of_type_2() { return &___type_2; }
	inline void set_type_2(int32_t value)
	{
		___type_2 = value;
	}

	inline static int32_t get_offset_of_interval_3() { return static_cast<int32_t>(offsetof(AdvLipSynchData_t1090448263, ___interval_3)); }
	inline float get_interval_3() const { return ___interval_3; }
	inline float* get_address_of_interval_3() { return &___interval_3; }
	inline void set_interval_3(float value)
	{
		___interval_3 = value;
	}

	inline static int32_t get_offset_of_scaleVoiceVolume_4() { return static_cast<int32_t>(offsetof(AdvLipSynchData_t1090448263, ___scaleVoiceVolume_4)); }
	inline float get_scaleVoiceVolume_4() const { return ___scaleVoiceVolume_4; }
	inline float* get_address_of_scaleVoiceVolume_4() { return &___scaleVoiceVolume_4; }
	inline void set_scaleVoiceVolume_4(float value)
	{
		___scaleVoiceVolume_4 = value;
	}

	inline static int32_t get_offset_of_tag_5() { return static_cast<int32_t>(offsetof(AdvLipSynchData_t1090448263, ___tag_5)); }
	inline String_t* get_tag_5() const { return ___tag_5; }
	inline String_t** get_address_of_tag_5() { return &___tag_5; }
	inline void set_tag_5(String_t* value)
	{
		___tag_5 = value;
		Il2CppCodeGenWriteBarrier((&___tag_5), value);
	}

	inline static int32_t get_offset_of_animationData_6() { return static_cast<int32_t>(offsetof(AdvLipSynchData_t1090448263, ___animationData_6)); }
	inline MiniAnimationData_t521227391 * get_animationData_6() const { return ___animationData_6; }
	inline MiniAnimationData_t521227391 ** get_address_of_animationData_6() { return &___animationData_6; }
	inline void set_animationData_6(MiniAnimationData_t521227391 * value)
	{
		___animationData_6 = value;
		Il2CppCodeGenWriteBarrier((&___animationData_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ADVLIPSYNCHDATA_T1090448263_H
#ifndef ADVPARAMDATA_T1457431784_H
#define ADVPARAMDATA_T1457431784_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.AdvParamData
struct  AdvParamData_t1457431784  : public RuntimeObject
{
public:
	// System.String Utage.AdvParamData::key
	String_t* ___key_0;
	// Utage.AdvParamData/ParamType Utage.AdvParamData::type
	int32_t ___type_1;
	// System.Object Utage.AdvParamData::parameter
	RuntimeObject * ___parameter_2;
	// System.String Utage.AdvParamData::parameterString
	String_t* ___parameterString_3;
	// Utage.AdvParamData/FileType Utage.AdvParamData::fileType
	int32_t ___fileType_4;

public:
	inline static int32_t get_offset_of_key_0() { return static_cast<int32_t>(offsetof(AdvParamData_t1457431784, ___key_0)); }
	inline String_t* get_key_0() const { return ___key_0; }
	inline String_t** get_address_of_key_0() { return &___key_0; }
	inline void set_key_0(String_t* value)
	{
		___key_0 = value;
		Il2CppCodeGenWriteBarrier((&___key_0), value);
	}

	inline static int32_t get_offset_of_type_1() { return static_cast<int32_t>(offsetof(AdvParamData_t1457431784, ___type_1)); }
	inline int32_t get_type_1() const { return ___type_1; }
	inline int32_t* get_address_of_type_1() { return &___type_1; }
	inline void set_type_1(int32_t value)
	{
		___type_1 = value;
	}

	inline static int32_t get_offset_of_parameter_2() { return static_cast<int32_t>(offsetof(AdvParamData_t1457431784, ___parameter_2)); }
	inline RuntimeObject * get_parameter_2() const { return ___parameter_2; }
	inline RuntimeObject ** get_address_of_parameter_2() { return &___parameter_2; }
	inline void set_parameter_2(RuntimeObject * value)
	{
		___parameter_2 = value;
		Il2CppCodeGenWriteBarrier((&___parameter_2), value);
	}

	inline static int32_t get_offset_of_parameterString_3() { return static_cast<int32_t>(offsetof(AdvParamData_t1457431784, ___parameterString_3)); }
	inline String_t* get_parameterString_3() const { return ___parameterString_3; }
	inline String_t** get_address_of_parameterString_3() { return &___parameterString_3; }
	inline void set_parameterString_3(String_t* value)
	{
		___parameterString_3 = value;
		Il2CppCodeGenWriteBarrier((&___parameterString_3), value);
	}

	inline static int32_t get_offset_of_fileType_4() { return static_cast<int32_t>(offsetof(AdvParamData_t1457431784, ___fileType_4)); }
	inline int32_t get_fileType_4() const { return ___fileType_4; }
	inline int32_t* get_address_of_fileType_4() { return &___fileType_4; }
	inline void set_fileType_4(int32_t value)
	{
		___fileType_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ADVPARAMDATA_T1457431784_H
#ifndef IOINERFACE_T3434613093_H
#define IOINERFACE_T3434613093_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.AdvParamManager/IoInerface
struct  IoInerface_t3434613093  : public RuntimeObject
{
public:
	// Utage.AdvParamData/FileType Utage.AdvParamManager/IoInerface::<FileType>k__BackingField
	int32_t ___U3CFileTypeU3Ek__BackingField_0;
	// Utage.AdvParamManager Utage.AdvParamManager/IoInerface::<Param>k__BackingField
	AdvParamManager_t1816006425 * ___U3CParamU3Ek__BackingField_1;

public:
	inline static int32_t get_offset_of_U3CFileTypeU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(IoInerface_t3434613093, ___U3CFileTypeU3Ek__BackingField_0)); }
	inline int32_t get_U3CFileTypeU3Ek__BackingField_0() const { return ___U3CFileTypeU3Ek__BackingField_0; }
	inline int32_t* get_address_of_U3CFileTypeU3Ek__BackingField_0() { return &___U3CFileTypeU3Ek__BackingField_0; }
	inline void set_U3CFileTypeU3Ek__BackingField_0(int32_t value)
	{
		___U3CFileTypeU3Ek__BackingField_0 = value;
	}

	inline static int32_t get_offset_of_U3CParamU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(IoInerface_t3434613093, ___U3CParamU3Ek__BackingField_1)); }
	inline AdvParamManager_t1816006425 * get_U3CParamU3Ek__BackingField_1() const { return ___U3CParamU3Ek__BackingField_1; }
	inline AdvParamManager_t1816006425 ** get_address_of_U3CParamU3Ek__BackingField_1() { return &___U3CParamU3Ek__BackingField_1; }
	inline void set_U3CParamU3Ek__BackingField_1(AdvParamManager_t1816006425 * value)
	{
		___U3CParamU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CParamU3Ek__BackingField_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IOINERFACE_T3434613093_H
#ifndef U3CREADU3EC__ANONSTOREY3_T2127335072_H
#define U3CREADU3EC__ANONSTOREY3_T2127335072_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.AdvParamManager/<Read>c__AnonStorey3
struct  U3CReadU3Ec__AnonStorey3_t2127335072  : public RuntimeObject
{
public:
	// Utage.AdvParamData/FileType Utage.AdvParamManager/<Read>c__AnonStorey3::fileType
	int32_t ___fileType_0;
	// Utage.AdvParamManager Utage.AdvParamManager/<Read>c__AnonStorey3::$this
	AdvParamManager_t1816006425 * ___U24this_1;

public:
	inline static int32_t get_offset_of_fileType_0() { return static_cast<int32_t>(offsetof(U3CReadU3Ec__AnonStorey3_t2127335072, ___fileType_0)); }
	inline int32_t get_fileType_0() const { return ___fileType_0; }
	inline int32_t* get_address_of_fileType_0() { return &___fileType_0; }
	inline void set_fileType_0(int32_t value)
	{
		___fileType_0 = value;
	}

	inline static int32_t get_offset_of_U24this_1() { return static_cast<int32_t>(offsetof(U3CReadU3Ec__AnonStorey3_t2127335072, ___U24this_1)); }
	inline AdvParamManager_t1816006425 * get_U24this_1() const { return ___U24this_1; }
	inline AdvParamManager_t1816006425 ** get_address_of_U24this_1() { return &___U24this_1; }
	inline void set_U24this_1(AdvParamManager_t1816006425 * value)
	{
		___U24this_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CREADU3EC__ANONSTOREY3_T2127335072_H
#ifndef U3CWRITEU3EC__ANONSTOREY1_T2299255853_H
#define U3CWRITEU3EC__ANONSTOREY1_T2299255853_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.AdvParamStructTbl/<Write>c__AnonStorey1
struct  U3CWriteU3Ec__AnonStorey1_t2299255853  : public RuntimeObject
{
public:
	// Utage.AdvParamData/FileType Utage.AdvParamStructTbl/<Write>c__AnonStorey1::fileType
	int32_t ___fileType_0;

public:
	inline static int32_t get_offset_of_fileType_0() { return static_cast<int32_t>(offsetof(U3CWriteU3Ec__AnonStorey1_t2299255853, ___fileType_0)); }
	inline int32_t get_fileType_0() const { return ___fileType_0; }
	inline int32_t* get_address_of_fileType_0() { return &___fileType_0; }
	inline void set_fileType_0(int32_t value)
	{
		___fileType_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CWRITEU3EC__ANONSTOREY1_T2299255853_H
#ifndef U3CREADU3EC__ANONSTOREY3_T881395170_H
#define U3CREADU3EC__ANONSTOREY3_T881395170_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.AdvParamStructTbl/<Read>c__AnonStorey3
struct  U3CReadU3Ec__AnonStorey3_t881395170  : public RuntimeObject
{
public:
	// Utage.AdvParamData/FileType Utage.AdvParamStructTbl/<Read>c__AnonStorey3::fileType
	int32_t ___fileType_0;
	// Utage.AdvParamStructTbl Utage.AdvParamStructTbl/<Read>c__AnonStorey3::$this
	AdvParamStructTbl_t994067203 * ___U24this_1;

public:
	inline static int32_t get_offset_of_fileType_0() { return static_cast<int32_t>(offsetof(U3CReadU3Ec__AnonStorey3_t881395170, ___fileType_0)); }
	inline int32_t get_fileType_0() const { return ___fileType_0; }
	inline int32_t* get_address_of_fileType_0() { return &___fileType_0; }
	inline void set_fileType_0(int32_t value)
	{
		___fileType_0 = value;
	}

	inline static int32_t get_offset_of_U24this_1() { return static_cast<int32_t>(offsetof(U3CReadU3Ec__AnonStorey3_t881395170, ___U24this_1)); }
	inline AdvParamStructTbl_t994067203 * get_U24this_1() const { return ___U24this_1; }
	inline AdvParamStructTbl_t994067203 ** get_address_of_U24this_1() { return &___U24this_1; }
	inline void set_U24this_1(AdvParamStructTbl_t994067203 * value)
	{
		___U24this_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CREADU3EC__ANONSTOREY3_T881395170_H
#ifndef ADVTEXTURESETTINGDATA_T2976012266_H
#define ADVTEXTURESETTINGDATA_T2976012266_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.AdvTextureSettingData
struct  AdvTextureSettingData_t2976012266  : public AdvSettingDictinoayItemBase_t203751633
{
public:
	// Utage.AdvTextureSettingData/Type Utage.AdvTextureSettingData::<TextureType>k__BackingField
	int32_t ___U3CTextureTypeU3Ek__BackingField_3;
	// Utage.AdvGraphicInfoList Utage.AdvTextureSettingData::<Graphic>k__BackingField
	AdvGraphicInfoList_t3537398639 * ___U3CGraphicU3Ek__BackingField_4;
	// System.String Utage.AdvTextureSettingData::thumbnailName
	String_t* ___thumbnailName_5;
	// System.String Utage.AdvTextureSettingData::<ThumbnailPath>k__BackingField
	String_t* ___U3CThumbnailPathU3Ek__BackingField_6;
	// System.Int32 Utage.AdvTextureSettingData::<ThumbnailVersion>k__BackingField
	int32_t ___U3CThumbnailVersionU3Ek__BackingField_7;
	// System.String Utage.AdvTextureSettingData::<CgCategory>k__BackingField
	String_t* ___U3CCgCategoryU3Ek__BackingField_8;

public:
	inline static int32_t get_offset_of_U3CTextureTypeU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(AdvTextureSettingData_t2976012266, ___U3CTextureTypeU3Ek__BackingField_3)); }
	inline int32_t get_U3CTextureTypeU3Ek__BackingField_3() const { return ___U3CTextureTypeU3Ek__BackingField_3; }
	inline int32_t* get_address_of_U3CTextureTypeU3Ek__BackingField_3() { return &___U3CTextureTypeU3Ek__BackingField_3; }
	inline void set_U3CTextureTypeU3Ek__BackingField_3(int32_t value)
	{
		___U3CTextureTypeU3Ek__BackingField_3 = value;
	}

	inline static int32_t get_offset_of_U3CGraphicU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(AdvTextureSettingData_t2976012266, ___U3CGraphicU3Ek__BackingField_4)); }
	inline AdvGraphicInfoList_t3537398639 * get_U3CGraphicU3Ek__BackingField_4() const { return ___U3CGraphicU3Ek__BackingField_4; }
	inline AdvGraphicInfoList_t3537398639 ** get_address_of_U3CGraphicU3Ek__BackingField_4() { return &___U3CGraphicU3Ek__BackingField_4; }
	inline void set_U3CGraphicU3Ek__BackingField_4(AdvGraphicInfoList_t3537398639 * value)
	{
		___U3CGraphicU3Ek__BackingField_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CGraphicU3Ek__BackingField_4), value);
	}

	inline static int32_t get_offset_of_thumbnailName_5() { return static_cast<int32_t>(offsetof(AdvTextureSettingData_t2976012266, ___thumbnailName_5)); }
	inline String_t* get_thumbnailName_5() const { return ___thumbnailName_5; }
	inline String_t** get_address_of_thumbnailName_5() { return &___thumbnailName_5; }
	inline void set_thumbnailName_5(String_t* value)
	{
		___thumbnailName_5 = value;
		Il2CppCodeGenWriteBarrier((&___thumbnailName_5), value);
	}

	inline static int32_t get_offset_of_U3CThumbnailPathU3Ek__BackingField_6() { return static_cast<int32_t>(offsetof(AdvTextureSettingData_t2976012266, ___U3CThumbnailPathU3Ek__BackingField_6)); }
	inline String_t* get_U3CThumbnailPathU3Ek__BackingField_6() const { return ___U3CThumbnailPathU3Ek__BackingField_6; }
	inline String_t** get_address_of_U3CThumbnailPathU3Ek__BackingField_6() { return &___U3CThumbnailPathU3Ek__BackingField_6; }
	inline void set_U3CThumbnailPathU3Ek__BackingField_6(String_t* value)
	{
		___U3CThumbnailPathU3Ek__BackingField_6 = value;
		Il2CppCodeGenWriteBarrier((&___U3CThumbnailPathU3Ek__BackingField_6), value);
	}

	inline static int32_t get_offset_of_U3CThumbnailVersionU3Ek__BackingField_7() { return static_cast<int32_t>(offsetof(AdvTextureSettingData_t2976012266, ___U3CThumbnailVersionU3Ek__BackingField_7)); }
	inline int32_t get_U3CThumbnailVersionU3Ek__BackingField_7() const { return ___U3CThumbnailVersionU3Ek__BackingField_7; }
	inline int32_t* get_address_of_U3CThumbnailVersionU3Ek__BackingField_7() { return &___U3CThumbnailVersionU3Ek__BackingField_7; }
	inline void set_U3CThumbnailVersionU3Ek__BackingField_7(int32_t value)
	{
		___U3CThumbnailVersionU3Ek__BackingField_7 = value;
	}

	inline static int32_t get_offset_of_U3CCgCategoryU3Ek__BackingField_8() { return static_cast<int32_t>(offsetof(AdvTextureSettingData_t2976012266, ___U3CCgCategoryU3Ek__BackingField_8)); }
	inline String_t* get_U3CCgCategoryU3Ek__BackingField_8() const { return ___U3CCgCategoryU3Ek__BackingField_8; }
	inline String_t** get_address_of_U3CCgCategoryU3Ek__BackingField_8() { return &___U3CCgCategoryU3Ek__BackingField_8; }
	inline void set_U3CCgCategoryU3Ek__BackingField_8(String_t* value)
	{
		___U3CCgCategoryU3Ek__BackingField_8 = value;
		Il2CppCodeGenWriteBarrier((&___U3CCgCategoryU3Ek__BackingField_8), value);
	}
};

struct AdvTextureSettingData_t2976012266_StaticFields
{
public:
	// Utage.AdvTextureSettingData/ParseCustomFileTypeRootDir Utage.AdvTextureSettingData::CallbackParseCustomFileTypeRootDir
	ParseCustomFileTypeRootDir_t2850020050 * ___CallbackParseCustomFileTypeRootDir_2;

public:
	inline static int32_t get_offset_of_CallbackParseCustomFileTypeRootDir_2() { return static_cast<int32_t>(offsetof(AdvTextureSettingData_t2976012266_StaticFields, ___CallbackParseCustomFileTypeRootDir_2)); }
	inline ParseCustomFileTypeRootDir_t2850020050 * get_CallbackParseCustomFileTypeRootDir_2() const { return ___CallbackParseCustomFileTypeRootDir_2; }
	inline ParseCustomFileTypeRootDir_t2850020050 ** get_address_of_CallbackParseCustomFileTypeRootDir_2() { return &___CallbackParseCustomFileTypeRootDir_2; }
	inline void set_CallbackParseCustomFileTypeRootDir_2(ParseCustomFileTypeRootDir_t2850020050 * value)
	{
		___CallbackParseCustomFileTypeRootDir_2 = value;
		Il2CppCodeGenWriteBarrier((&___CallbackParseCustomFileTypeRootDir_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ADVTEXTURESETTINGDATA_T2976012266_H
#ifndef SCRIPTABLEOBJECT_T1975622470_H
#define SCRIPTABLEOBJECT_T1975622470_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.ScriptableObject
struct  ScriptableObject_t1975622470  : public Object_t1021602117
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.ScriptableObject
struct ScriptableObject_t1975622470_marshaled_pinvoke : public Object_t1021602117_marshaled_pinvoke
{
};
// Native definition for COM marshalling of UnityEngine.ScriptableObject
struct ScriptableObject_t1975622470_marshaled_com : public Object_t1021602117_marshaled_com
{
};
#endif // SCRIPTABLEOBJECT_T1975622470_H
#ifndef U3CWRITEU3EC__ANONSTOREY1_T894999319_H
#define U3CWRITEU3EC__ANONSTOREY1_T894999319_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.AdvParamManager/<Write>c__AnonStorey1
struct  U3CWriteU3Ec__AnonStorey1_t894999319  : public RuntimeObject
{
public:
	// Utage.AdvParamData/FileType Utage.AdvParamManager/<Write>c__AnonStorey1::fileType
	int32_t ___fileType_0;

public:
	inline static int32_t get_offset_of_fileType_0() { return static_cast<int32_t>(offsetof(U3CWriteU3Ec__AnonStorey1_t894999319, ___fileType_0)); }
	inline int32_t get_fileType_0() const { return ___fileType_0; }
	inline int32_t* get_address_of_fileType_0() { return &___fileType_0; }
	inline void set_fileType_0(int32_t value)
	{
		___fileType_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CWRITEU3EC__ANONSTOREY1_T894999319_H
#ifndef U3CFINDDEFAULTLAYERU3EC__ANONSTOREY1_T1418813260_H
#define U3CFINDDEFAULTLAYERU3EC__ANONSTOREY1_T1418813260_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.AdvLayerSetting/<FindDefaultLayer>c__AnonStorey1
struct  U3CFindDefaultLayerU3Ec__AnonStorey1_t1418813260  : public RuntimeObject
{
public:
	// Utage.AdvLayerSettingData/LayerType Utage.AdvLayerSetting/<FindDefaultLayer>c__AnonStorey1::type
	int32_t ___type_0;

public:
	inline static int32_t get_offset_of_type_0() { return static_cast<int32_t>(offsetof(U3CFindDefaultLayerU3Ec__AnonStorey1_t1418813260, ___type_0)); }
	inline int32_t get_type_0() const { return ___type_0; }
	inline int32_t* get_address_of_type_0() { return &___type_0; }
	inline void set_type_0(int32_t value)
	{
		___type_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CFINDDEFAULTLAYERU3EC__ANONSTOREY1_T1418813260_H
#ifndef U3CINITDEFAULTU3EC__ANONSTOREY0_T4265531495_H
#define U3CINITDEFAULTU3EC__ANONSTOREY0_T4265531495_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.AdvLayerSetting/<InitDefault>c__AnonStorey0
struct  U3CInitDefaultU3Ec__AnonStorey0_t4265531495  : public RuntimeObject
{
public:
	// Utage.AdvLayerSettingData/LayerType Utage.AdvLayerSetting/<InitDefault>c__AnonStorey0::type
	int32_t ___type_0;

public:
	inline static int32_t get_offset_of_type_0() { return static_cast<int32_t>(offsetof(U3CInitDefaultU3Ec__AnonStorey0_t4265531495, ___type_0)); }
	inline int32_t get_type_0() const { return ___type_0; }
	inline int32_t* get_address_of_type_0() { return &___type_0; }
	inline void set_type_0(int32_t value)
	{
		___type_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CINITDEFAULTU3EC__ANONSTOREY0_T4265531495_H
#ifndef ADVIMPORTSCENARIOSHEET_T3549824885_H
#define ADVIMPORTSCENARIOSHEET_T3549824885_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.AdvImportScenarioSheet
struct  AdvImportScenarioSheet_t3549824885  : public StringGrid_t1872153679
{
public:
	// System.Collections.Generic.List`1<System.Int32> Utage.AdvImportScenarioSheet::entityIndexTbl
	List_1_t1440998580 * ___entityIndexTbl_7;
	// System.Collections.Generic.List`1<Utage.AdvEntityData> Utage.AdvImportScenarioSheet::entityDataList
	List_1_t834475628 * ___entityDataList_8;

public:
	inline static int32_t get_offset_of_entityIndexTbl_7() { return static_cast<int32_t>(offsetof(AdvImportScenarioSheet_t3549824885, ___entityIndexTbl_7)); }
	inline List_1_t1440998580 * get_entityIndexTbl_7() const { return ___entityIndexTbl_7; }
	inline List_1_t1440998580 ** get_address_of_entityIndexTbl_7() { return &___entityIndexTbl_7; }
	inline void set_entityIndexTbl_7(List_1_t1440998580 * value)
	{
		___entityIndexTbl_7 = value;
		Il2CppCodeGenWriteBarrier((&___entityIndexTbl_7), value);
	}

	inline static int32_t get_offset_of_entityDataList_8() { return static_cast<int32_t>(offsetof(AdvImportScenarioSheet_t3549824885, ___entityDataList_8)); }
	inline List_1_t834475628 * get_entityDataList_8() const { return ___entityDataList_8; }
	inline List_1_t834475628 ** get_address_of_entityDataList_8() { return &___entityDataList_8; }
	inline void set_entityDataList_8(List_1_t834475628 * value)
	{
		___entityDataList_8 = value;
		Il2CppCodeGenWriteBarrier((&___entityDataList_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ADVIMPORTSCENARIOSHEET_T3549824885_H
#ifndef ADVIMPORTSCENARIOS_T1226385437_H
#define ADVIMPORTSCENARIOS_T1226385437_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.AdvImportScenarios
struct  AdvImportScenarios_t1226385437  : public ScriptableObject_t1975622470
{
public:
	// System.Int32 Utage.AdvImportScenarios::importVersion
	int32_t ___importVersion_3;
	// System.Collections.Generic.List`1<Utage.AdvChapterData> Utage.AdvImportScenarios::chapters
	List_1_t54812456 * ___chapters_4;

public:
	inline static int32_t get_offset_of_importVersion_3() { return static_cast<int32_t>(offsetof(AdvImportScenarios_t1226385437, ___importVersion_3)); }
	inline int32_t get_importVersion_3() const { return ___importVersion_3; }
	inline int32_t* get_address_of_importVersion_3() { return &___importVersion_3; }
	inline void set_importVersion_3(int32_t value)
	{
		___importVersion_3 = value;
	}

	inline static int32_t get_offset_of_chapters_4() { return static_cast<int32_t>(offsetof(AdvImportScenarios_t1226385437, ___chapters_4)); }
	inline List_1_t54812456 * get_chapters_4() const { return ___chapters_4; }
	inline List_1_t54812456 ** get_address_of_chapters_4() { return &___chapters_4; }
	inline void set_chapters_4(List_1_t54812456 * value)
	{
		___chapters_4 = value;
		Il2CppCodeGenWriteBarrier((&___chapters_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ADVIMPORTSCENARIOS_T1226385437_H
#ifndef ADVIMPORTBOOK_T3958042025_H
#define ADVIMPORTBOOK_T3958042025_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.AdvImportBook
struct  AdvImportBook_t3958042025  : public ScriptableObject_t1975622470
{
public:
	// System.Int32 Utage.AdvImportBook::importVersion
	int32_t ___importVersion_3;
	// System.Collections.Generic.List`1<Utage.AdvImportScenarioSheet> Utage.AdvImportBook::importGridList
	List_1_t2918946017 * ___importGridList_4;

public:
	inline static int32_t get_offset_of_importVersion_3() { return static_cast<int32_t>(offsetof(AdvImportBook_t3958042025, ___importVersion_3)); }
	inline int32_t get_importVersion_3() const { return ___importVersion_3; }
	inline int32_t* get_address_of_importVersion_3() { return &___importVersion_3; }
	inline void set_importVersion_3(int32_t value)
	{
		___importVersion_3 = value;
	}

	inline static int32_t get_offset_of_importGridList_4() { return static_cast<int32_t>(offsetof(AdvImportBook_t3958042025, ___importGridList_4)); }
	inline List_1_t2918946017 * get_importGridList_4() const { return ___importGridList_4; }
	inline List_1_t2918946017 ** get_address_of_importGridList_4() { return &___importGridList_4; }
	inline void set_importGridList_4(List_1_t2918946017 * value)
	{
		___importGridList_4 = value;
		Il2CppCodeGenWriteBarrier((&___importGridList_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ADVIMPORTBOOK_T3958042025_H
#ifndef PARSECUSTOMFILETYPEROOTDIR_T3762798398_H
#define PARSECUSTOMFILETYPEROOTDIR_T3762798398_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.AdvCharacterSettingData/ParseCustomFileTypeRootDir
struct  ParseCustomFileTypeRootDir_t3762798398  : public MulticastDelegate_t3201952435
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PARSECUSTOMFILETYPEROOTDIR_T3762798398_H
#ifndef PARSECUSTOMFILETYPEROOTDIR_T2850020050_H
#define PARSECUSTOMFILETYPEROOTDIR_T2850020050_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.AdvTextureSettingData/ParseCustomFileTypeRootDir
struct  ParseCustomFileTypeRootDir_t2850020050  : public MulticastDelegate_t3201952435
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PARSECUSTOMFILETYPEROOTDIR_T2850020050_H
#ifndef BEHAVIOUR_T955675639_H
#define BEHAVIOUR_T955675639_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Behaviour
struct  Behaviour_t955675639  : public Component_t3819376471
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BEHAVIOUR_T955675639_H
#ifndef ADVCHAPTERDATA_T685691324_H
#define ADVCHAPTERDATA_T685691324_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.AdvChapterData
struct  AdvChapterData_t685691324  : public ScriptableObject_t1975622470
{
public:
	// System.String Utage.AdvChapterData::chapterName
	String_t* ___chapterName_2;
	// System.Collections.Generic.List`1<Utage.AdvImportBook> Utage.AdvChapterData::dataList
	List_1_t3327163157 * ___dataList_3;
	// System.Collections.Generic.List`1<Utage.StringGrid> Utage.AdvChapterData::settingList
	List_1_t1241274811 * ___settingList_4;
	// System.Boolean Utage.AdvChapterData::<IsInited>k__BackingField
	bool ___U3CIsInitedU3Ek__BackingField_5;

public:
	inline static int32_t get_offset_of_chapterName_2() { return static_cast<int32_t>(offsetof(AdvChapterData_t685691324, ___chapterName_2)); }
	inline String_t* get_chapterName_2() const { return ___chapterName_2; }
	inline String_t** get_address_of_chapterName_2() { return &___chapterName_2; }
	inline void set_chapterName_2(String_t* value)
	{
		___chapterName_2 = value;
		Il2CppCodeGenWriteBarrier((&___chapterName_2), value);
	}

	inline static int32_t get_offset_of_dataList_3() { return static_cast<int32_t>(offsetof(AdvChapterData_t685691324, ___dataList_3)); }
	inline List_1_t3327163157 * get_dataList_3() const { return ___dataList_3; }
	inline List_1_t3327163157 ** get_address_of_dataList_3() { return &___dataList_3; }
	inline void set_dataList_3(List_1_t3327163157 * value)
	{
		___dataList_3 = value;
		Il2CppCodeGenWriteBarrier((&___dataList_3), value);
	}

	inline static int32_t get_offset_of_settingList_4() { return static_cast<int32_t>(offsetof(AdvChapterData_t685691324, ___settingList_4)); }
	inline List_1_t1241274811 * get_settingList_4() const { return ___settingList_4; }
	inline List_1_t1241274811 ** get_address_of_settingList_4() { return &___settingList_4; }
	inline void set_settingList_4(List_1_t1241274811 * value)
	{
		___settingList_4 = value;
		Il2CppCodeGenWriteBarrier((&___settingList_4), value);
	}

	inline static int32_t get_offset_of_U3CIsInitedU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(AdvChapterData_t685691324, ___U3CIsInitedU3Ek__BackingField_5)); }
	inline bool get_U3CIsInitedU3Ek__BackingField_5() const { return ___U3CIsInitedU3Ek__BackingField_5; }
	inline bool* get_address_of_U3CIsInitedU3Ek__BackingField_5() { return &___U3CIsInitedU3Ek__BackingField_5; }
	inline void set_U3CIsInitedU3Ek__BackingField_5(bool value)
	{
		___U3CIsInitedU3Ek__BackingField_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ADVCHAPTERDATA_T685691324_H
#ifndef U3CCOBOOTINITSCENARIODDATAU3EC__ITERATOR0_T2934450674_H
#define U3CCOBOOTINITSCENARIODDATAU3EC__ITERATOR0_T2934450674_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.AdvDataManager/<CoBootInitScenariodData>c__Iterator0
struct  U3CCoBootInitScenariodDataU3Ec__Iterator0_t2934450674  : public RuntimeObject
{
public:
	// System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.String,Utage.AdvScenarioData> Utage.AdvDataManager/<CoBootInitScenariodData>c__Iterator0::$locvar0
	Enumerator_t1852857469  ___U24locvar0_0;
	// Utage.AdvScenarioData Utage.AdvDataManager/<CoBootInitScenariodData>c__Iterator0::<data>__1
	AdvScenarioData_t2546512739 * ___U3CdataU3E__1_1;
	// Utage.AdvDataManager Utage.AdvDataManager/<CoBootInitScenariodData>c__Iterator0::$this
	AdvDataManager_t2481232830 * ___U24this_2;
	// System.Object Utage.AdvDataManager/<CoBootInitScenariodData>c__Iterator0::$current
	RuntimeObject * ___U24current_3;
	// System.Boolean Utage.AdvDataManager/<CoBootInitScenariodData>c__Iterator0::$disposing
	bool ___U24disposing_4;
	// System.Int32 Utage.AdvDataManager/<CoBootInitScenariodData>c__Iterator0::$PC
	int32_t ___U24PC_5;

public:
	inline static int32_t get_offset_of_U24locvar0_0() { return static_cast<int32_t>(offsetof(U3CCoBootInitScenariodDataU3Ec__Iterator0_t2934450674, ___U24locvar0_0)); }
	inline Enumerator_t1852857469  get_U24locvar0_0() const { return ___U24locvar0_0; }
	inline Enumerator_t1852857469 * get_address_of_U24locvar0_0() { return &___U24locvar0_0; }
	inline void set_U24locvar0_0(Enumerator_t1852857469  value)
	{
		___U24locvar0_0 = value;
	}

	inline static int32_t get_offset_of_U3CdataU3E__1_1() { return static_cast<int32_t>(offsetof(U3CCoBootInitScenariodDataU3Ec__Iterator0_t2934450674, ___U3CdataU3E__1_1)); }
	inline AdvScenarioData_t2546512739 * get_U3CdataU3E__1_1() const { return ___U3CdataU3E__1_1; }
	inline AdvScenarioData_t2546512739 ** get_address_of_U3CdataU3E__1_1() { return &___U3CdataU3E__1_1; }
	inline void set_U3CdataU3E__1_1(AdvScenarioData_t2546512739 * value)
	{
		___U3CdataU3E__1_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CdataU3E__1_1), value);
	}

	inline static int32_t get_offset_of_U24this_2() { return static_cast<int32_t>(offsetof(U3CCoBootInitScenariodDataU3Ec__Iterator0_t2934450674, ___U24this_2)); }
	inline AdvDataManager_t2481232830 * get_U24this_2() const { return ___U24this_2; }
	inline AdvDataManager_t2481232830 ** get_address_of_U24this_2() { return &___U24this_2; }
	inline void set_U24this_2(AdvDataManager_t2481232830 * value)
	{
		___U24this_2 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_2), value);
	}

	inline static int32_t get_offset_of_U24current_3() { return static_cast<int32_t>(offsetof(U3CCoBootInitScenariodDataU3Ec__Iterator0_t2934450674, ___U24current_3)); }
	inline RuntimeObject * get_U24current_3() const { return ___U24current_3; }
	inline RuntimeObject ** get_address_of_U24current_3() { return &___U24current_3; }
	inline void set_U24current_3(RuntimeObject * value)
	{
		___U24current_3 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_3), value);
	}

	inline static int32_t get_offset_of_U24disposing_4() { return static_cast<int32_t>(offsetof(U3CCoBootInitScenariodDataU3Ec__Iterator0_t2934450674, ___U24disposing_4)); }
	inline bool get_U24disposing_4() const { return ___U24disposing_4; }
	inline bool* get_address_of_U24disposing_4() { return &___U24disposing_4; }
	inline void set_U24disposing_4(bool value)
	{
		___U24disposing_4 = value;
	}

	inline static int32_t get_offset_of_U24PC_5() { return static_cast<int32_t>(offsetof(U3CCoBootInitScenariodDataU3Ec__Iterator0_t2934450674, ___U24PC_5)); }
	inline int32_t get_U24PC_5() const { return ___U24PC_5; }
	inline int32_t* get_address_of_U24PC_5() { return &___U24PC_5; }
	inline void set_U24PC_5(int32_t value)
	{
		___U24PC_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CCOBOOTINITSCENARIODDATAU3EC__ITERATOR0_T2934450674_H
#ifndef MONOBEHAVIOUR_T1158329972_H
#define MONOBEHAVIOUR_T1158329972_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.MonoBehaviour
struct  MonoBehaviour_t1158329972  : public Behaviour_t955675639
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MONOBEHAVIOUR_T1158329972_H
#ifndef ADVDATAMANAGER_T2481232830_H
#define ADVDATAMANAGER_T2481232830_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.AdvDataManager
struct  AdvDataManager_t2481232830  : public MonoBehaviour_t1158329972
{
public:
	// System.Boolean Utage.AdvDataManager::isBackGroundDownload
	bool ___isBackGroundDownload_2;
	// Utage.AdvSettingDataManager Utage.AdvDataManager::settingDataManager
	AdvSettingDataManager_t931476416 * ___settingDataManager_3;
	// System.Collections.Generic.Dictionary`2<System.String,Utage.AdvScenarioData> Utage.AdvDataManager::scenarioDataTbl
	Dictionary_2_t166324705 * ___scenarioDataTbl_4;
	// Utage.AdvMacroManager Utage.AdvDataManager::macroManager
	AdvMacroManager_t3793610292 * ___macroManager_5;

public:
	inline static int32_t get_offset_of_isBackGroundDownload_2() { return static_cast<int32_t>(offsetof(AdvDataManager_t2481232830, ___isBackGroundDownload_2)); }
	inline bool get_isBackGroundDownload_2() const { return ___isBackGroundDownload_2; }
	inline bool* get_address_of_isBackGroundDownload_2() { return &___isBackGroundDownload_2; }
	inline void set_isBackGroundDownload_2(bool value)
	{
		___isBackGroundDownload_2 = value;
	}

	inline static int32_t get_offset_of_settingDataManager_3() { return static_cast<int32_t>(offsetof(AdvDataManager_t2481232830, ___settingDataManager_3)); }
	inline AdvSettingDataManager_t931476416 * get_settingDataManager_3() const { return ___settingDataManager_3; }
	inline AdvSettingDataManager_t931476416 ** get_address_of_settingDataManager_3() { return &___settingDataManager_3; }
	inline void set_settingDataManager_3(AdvSettingDataManager_t931476416 * value)
	{
		___settingDataManager_3 = value;
		Il2CppCodeGenWriteBarrier((&___settingDataManager_3), value);
	}

	inline static int32_t get_offset_of_scenarioDataTbl_4() { return static_cast<int32_t>(offsetof(AdvDataManager_t2481232830, ___scenarioDataTbl_4)); }
	inline Dictionary_2_t166324705 * get_scenarioDataTbl_4() const { return ___scenarioDataTbl_4; }
	inline Dictionary_2_t166324705 ** get_address_of_scenarioDataTbl_4() { return &___scenarioDataTbl_4; }
	inline void set_scenarioDataTbl_4(Dictionary_2_t166324705 * value)
	{
		___scenarioDataTbl_4 = value;
		Il2CppCodeGenWriteBarrier((&___scenarioDataTbl_4), value);
	}

	inline static int32_t get_offset_of_macroManager_5() { return static_cast<int32_t>(offsetof(AdvDataManager_t2481232830, ___macroManager_5)); }
	inline AdvMacroManager_t3793610292 * get_macroManager_5() const { return ___macroManager_5; }
	inline AdvMacroManager_t3793610292 ** get_address_of_macroManager_5() { return &___macroManager_5; }
	inline void set_macroManager_5(AdvMacroManager_t3793610292 * value)
	{
		___macroManager_5 = value;
		Il2CppCodeGenWriteBarrier((&___macroManager_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ADVDATAMANAGER_T2481232830_H
#ifndef ADVVIDEOLOADPATHCHANGER_T4171547847_H
#define ADVVIDEOLOADPATHCHANGER_T4171547847_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.AdvVideoLoadPathChanger
struct  AdvVideoLoadPathChanger_t4171547847  : public MonoBehaviour_t1158329972
{
public:
	// System.String Utage.AdvVideoLoadPathChanger::rootPath
	String_t* ___rootPath_2;

public:
	inline static int32_t get_offset_of_rootPath_2() { return static_cast<int32_t>(offsetof(AdvVideoLoadPathChanger_t4171547847, ___rootPath_2)); }
	inline String_t* get_rootPath_2() const { return ___rootPath_2; }
	inline String_t** get_address_of_rootPath_2() { return &___rootPath_2; }
	inline void set_rootPath_2(String_t* value)
	{
		___rootPath_2 = value;
		Il2CppCodeGenWriteBarrier((&___rootPath_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ADVVIDEOLOADPATHCHANGER_T4171547847_H
#ifndef ADVSELECTIONTIMELIMITTEXT_T1296437150_H
#define ADVSELECTIONTIMELIMITTEXT_T1296437150_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.AdvSelectionTimeLimitText
struct  AdvSelectionTimeLimitText_t1296437150  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.GameObject Utage.AdvSelectionTimeLimitText::targetRoot
	GameObject_t1756533147 * ___targetRoot_2;
	// UnityEngine.UI.Text Utage.AdvSelectionTimeLimitText::text
	Text_t356221433 * ___text_3;
	// Utage.AdvSelectionTimeLimit Utage.AdvSelectionTimeLimitText::timeLimit
	AdvSelectionTimeLimit_t2136628673 * ___timeLimit_4;
	// Utage.AdvEngine Utage.AdvSelectionTimeLimitText::engine
	AdvEngine_t1176753927 * ___engine_5;

public:
	inline static int32_t get_offset_of_targetRoot_2() { return static_cast<int32_t>(offsetof(AdvSelectionTimeLimitText_t1296437150, ___targetRoot_2)); }
	inline GameObject_t1756533147 * get_targetRoot_2() const { return ___targetRoot_2; }
	inline GameObject_t1756533147 ** get_address_of_targetRoot_2() { return &___targetRoot_2; }
	inline void set_targetRoot_2(GameObject_t1756533147 * value)
	{
		___targetRoot_2 = value;
		Il2CppCodeGenWriteBarrier((&___targetRoot_2), value);
	}

	inline static int32_t get_offset_of_text_3() { return static_cast<int32_t>(offsetof(AdvSelectionTimeLimitText_t1296437150, ___text_3)); }
	inline Text_t356221433 * get_text_3() const { return ___text_3; }
	inline Text_t356221433 ** get_address_of_text_3() { return &___text_3; }
	inline void set_text_3(Text_t356221433 * value)
	{
		___text_3 = value;
		Il2CppCodeGenWriteBarrier((&___text_3), value);
	}

	inline static int32_t get_offset_of_timeLimit_4() { return static_cast<int32_t>(offsetof(AdvSelectionTimeLimitText_t1296437150, ___timeLimit_4)); }
	inline AdvSelectionTimeLimit_t2136628673 * get_timeLimit_4() const { return ___timeLimit_4; }
	inline AdvSelectionTimeLimit_t2136628673 ** get_address_of_timeLimit_4() { return &___timeLimit_4; }
	inline void set_timeLimit_4(AdvSelectionTimeLimit_t2136628673 * value)
	{
		___timeLimit_4 = value;
		Il2CppCodeGenWriteBarrier((&___timeLimit_4), value);
	}

	inline static int32_t get_offset_of_engine_5() { return static_cast<int32_t>(offsetof(AdvSelectionTimeLimitText_t1296437150, ___engine_5)); }
	inline AdvEngine_t1176753927 * get_engine_5() const { return ___engine_5; }
	inline AdvEngine_t1176753927 ** get_address_of_engine_5() { return &___engine_5; }
	inline void set_engine_5(AdvEngine_t1176753927 * value)
	{
		___engine_5 = value;
		Il2CppCodeGenWriteBarrier((&___engine_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ADVSELECTIONTIMELIMITTEXT_T1296437150_H
#ifndef ADVSELECTIONTIMELIMIT_T2136628673_H
#define ADVSELECTIONTIMELIMIT_T2136628673_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.AdvSelectionTimeLimit
struct  AdvSelectionTimeLimit_t2136628673  : public MonoBehaviour_t1158329972
{
public:
	// System.Boolean Utage.AdvSelectionTimeLimit::disable
	bool ___disable_2;
	// Utage.AdvEngine Utage.AdvSelectionTimeLimit::engine
	AdvEngine_t1176753927 * ___engine_3;
	// Utage.AdvUguiSelection Utage.AdvSelectionTimeLimit::selection
	AdvUguiSelection_t2632218991 * ___selection_4;
	// System.Single Utage.AdvSelectionTimeLimit::limitTime
	float ___limitTime_5;
	// System.Int32 Utage.AdvSelectionTimeLimit::timeLimitIndex
	int32_t ___timeLimitIndex_6;
	// System.Single Utage.AdvSelectionTimeLimit::time
	float ___time_7;

public:
	inline static int32_t get_offset_of_disable_2() { return static_cast<int32_t>(offsetof(AdvSelectionTimeLimit_t2136628673, ___disable_2)); }
	inline bool get_disable_2() const { return ___disable_2; }
	inline bool* get_address_of_disable_2() { return &___disable_2; }
	inline void set_disable_2(bool value)
	{
		___disable_2 = value;
	}

	inline static int32_t get_offset_of_engine_3() { return static_cast<int32_t>(offsetof(AdvSelectionTimeLimit_t2136628673, ___engine_3)); }
	inline AdvEngine_t1176753927 * get_engine_3() const { return ___engine_3; }
	inline AdvEngine_t1176753927 ** get_address_of_engine_3() { return &___engine_3; }
	inline void set_engine_3(AdvEngine_t1176753927 * value)
	{
		___engine_3 = value;
		Il2CppCodeGenWriteBarrier((&___engine_3), value);
	}

	inline static int32_t get_offset_of_selection_4() { return static_cast<int32_t>(offsetof(AdvSelectionTimeLimit_t2136628673, ___selection_4)); }
	inline AdvUguiSelection_t2632218991 * get_selection_4() const { return ___selection_4; }
	inline AdvUguiSelection_t2632218991 ** get_address_of_selection_4() { return &___selection_4; }
	inline void set_selection_4(AdvUguiSelection_t2632218991 * value)
	{
		___selection_4 = value;
		Il2CppCodeGenWriteBarrier((&___selection_4), value);
	}

	inline static int32_t get_offset_of_limitTime_5() { return static_cast<int32_t>(offsetof(AdvSelectionTimeLimit_t2136628673, ___limitTime_5)); }
	inline float get_limitTime_5() const { return ___limitTime_5; }
	inline float* get_address_of_limitTime_5() { return &___limitTime_5; }
	inline void set_limitTime_5(float value)
	{
		___limitTime_5 = value;
	}

	inline static int32_t get_offset_of_timeLimitIndex_6() { return static_cast<int32_t>(offsetof(AdvSelectionTimeLimit_t2136628673, ___timeLimitIndex_6)); }
	inline int32_t get_timeLimitIndex_6() const { return ___timeLimitIndex_6; }
	inline int32_t* get_address_of_timeLimitIndex_6() { return &___timeLimitIndex_6; }
	inline void set_timeLimitIndex_6(int32_t value)
	{
		___timeLimitIndex_6 = value;
	}

	inline static int32_t get_offset_of_time_7() { return static_cast<int32_t>(offsetof(AdvSelectionTimeLimit_t2136628673, ___time_7)); }
	inline float get_time_7() const { return ___time_7; }
	inline float* get_address_of_time_7() { return &___time_7; }
	inline void set_time_7(float value)
	{
		___time_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ADVSELECTIONTIMELIMIT_T2136628673_H
#ifndef ADVLOADSCENE_T2523356965_H
#define ADVLOADSCENE_T2523356965_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.AdvLoadScene
struct  AdvLoadScene_t2523356965  : public MonoBehaviour_t1158329972
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ADVLOADSCENE_T2523356965_H
#ifndef ADVCHARACTERGRAYOUTCONTROLLER_T895965523_H
#define ADVCHARACTERGRAYOUTCONTROLLER_T895965523_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.AdvCharacterGrayOutController
struct  AdvCharacterGrayOutController_t895965523  : public MonoBehaviour_t1158329972
{
public:
	// Utage.AdvEngine Utage.AdvCharacterGrayOutController::engine
	AdvEngine_t1176753927 * ___engine_2;
	// Utage.AdvCharacterGrayOutController/LightingMask Utage.AdvCharacterGrayOutController::mask
	int32_t ___mask_3;
	// UnityEngine.Color Utage.AdvCharacterGrayOutController::mainColor
	Color_t2020392075  ___mainColor_4;
	// UnityEngine.Color Utage.AdvCharacterGrayOutController::subColor
	Color_t2020392075  ___subColor_5;
	// System.Single Utage.AdvCharacterGrayOutController::fadeTime
	float ___fadeTime_6;
	// System.Collections.Generic.List`1<System.String> Utage.AdvCharacterGrayOutController::noGrayoutCharacters
	List_1_t1398341365 * ___noGrayoutCharacters_7;
	// System.Boolean Utage.AdvCharacterGrayOutController::isChanged
	bool ___isChanged_8;
	// System.Collections.Generic.List`1<Utage.AdvGraphicLayer> Utage.AdvCharacterGrayOutController::pageBeginLayer
	List_1_t2999344954 * ___pageBeginLayer_9;

public:
	inline static int32_t get_offset_of_engine_2() { return static_cast<int32_t>(offsetof(AdvCharacterGrayOutController_t895965523, ___engine_2)); }
	inline AdvEngine_t1176753927 * get_engine_2() const { return ___engine_2; }
	inline AdvEngine_t1176753927 ** get_address_of_engine_2() { return &___engine_2; }
	inline void set_engine_2(AdvEngine_t1176753927 * value)
	{
		___engine_2 = value;
		Il2CppCodeGenWriteBarrier((&___engine_2), value);
	}

	inline static int32_t get_offset_of_mask_3() { return static_cast<int32_t>(offsetof(AdvCharacterGrayOutController_t895965523, ___mask_3)); }
	inline int32_t get_mask_3() const { return ___mask_3; }
	inline int32_t* get_address_of_mask_3() { return &___mask_3; }
	inline void set_mask_3(int32_t value)
	{
		___mask_3 = value;
	}

	inline static int32_t get_offset_of_mainColor_4() { return static_cast<int32_t>(offsetof(AdvCharacterGrayOutController_t895965523, ___mainColor_4)); }
	inline Color_t2020392075  get_mainColor_4() const { return ___mainColor_4; }
	inline Color_t2020392075 * get_address_of_mainColor_4() { return &___mainColor_4; }
	inline void set_mainColor_4(Color_t2020392075  value)
	{
		___mainColor_4 = value;
	}

	inline static int32_t get_offset_of_subColor_5() { return static_cast<int32_t>(offsetof(AdvCharacterGrayOutController_t895965523, ___subColor_5)); }
	inline Color_t2020392075  get_subColor_5() const { return ___subColor_5; }
	inline Color_t2020392075 * get_address_of_subColor_5() { return &___subColor_5; }
	inline void set_subColor_5(Color_t2020392075  value)
	{
		___subColor_5 = value;
	}

	inline static int32_t get_offset_of_fadeTime_6() { return static_cast<int32_t>(offsetof(AdvCharacterGrayOutController_t895965523, ___fadeTime_6)); }
	inline float get_fadeTime_6() const { return ___fadeTime_6; }
	inline float* get_address_of_fadeTime_6() { return &___fadeTime_6; }
	inline void set_fadeTime_6(float value)
	{
		___fadeTime_6 = value;
	}

	inline static int32_t get_offset_of_noGrayoutCharacters_7() { return static_cast<int32_t>(offsetof(AdvCharacterGrayOutController_t895965523, ___noGrayoutCharacters_7)); }
	inline List_1_t1398341365 * get_noGrayoutCharacters_7() const { return ___noGrayoutCharacters_7; }
	inline List_1_t1398341365 ** get_address_of_noGrayoutCharacters_7() { return &___noGrayoutCharacters_7; }
	inline void set_noGrayoutCharacters_7(List_1_t1398341365 * value)
	{
		___noGrayoutCharacters_7 = value;
		Il2CppCodeGenWriteBarrier((&___noGrayoutCharacters_7), value);
	}

	inline static int32_t get_offset_of_isChanged_8() { return static_cast<int32_t>(offsetof(AdvCharacterGrayOutController_t895965523, ___isChanged_8)); }
	inline bool get_isChanged_8() const { return ___isChanged_8; }
	inline bool* get_address_of_isChanged_8() { return &___isChanged_8; }
	inline void set_isChanged_8(bool value)
	{
		___isChanged_8 = value;
	}

	inline static int32_t get_offset_of_pageBeginLayer_9() { return static_cast<int32_t>(offsetof(AdvCharacterGrayOutController_t895965523, ___pageBeginLayer_9)); }
	inline List_1_t2999344954 * get_pageBeginLayer_9() const { return ___pageBeginLayer_9; }
	inline List_1_t2999344954 ** get_address_of_pageBeginLayer_9() { return &___pageBeginLayer_9; }
	inline void set_pageBeginLayer_9(List_1_t2999344954 * value)
	{
		___pageBeginLayer_9 = value;
		Il2CppCodeGenWriteBarrier((&___pageBeginLayer_9), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ADVCHARACTERGRAYOUTCONTROLLER_T895965523_H
#ifndef ADVBACKLOGFILTER_T120185244_H
#define ADVBACKLOGFILTER_T120185244_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.AdvBackLogFilter
struct  AdvBackLogFilter_t120185244  : public MonoBehaviour_t1158329972
{
public:
	// System.Boolean Utage.AdvBackLogFilter::disable
	bool ___disable_2;
	// System.Collections.Generic.List`1<System.String> Utage.AdvBackLogFilter::filterMessageWindowNames
	List_1_t1398341365 * ___filterMessageWindowNames_3;
	// Utage.AdvEngine Utage.AdvBackLogFilter::engine
	AdvEngine_t1176753927 * ___engine_4;

public:
	inline static int32_t get_offset_of_disable_2() { return static_cast<int32_t>(offsetof(AdvBackLogFilter_t120185244, ___disable_2)); }
	inline bool get_disable_2() const { return ___disable_2; }
	inline bool* get_address_of_disable_2() { return &___disable_2; }
	inline void set_disable_2(bool value)
	{
		___disable_2 = value;
	}

	inline static int32_t get_offset_of_filterMessageWindowNames_3() { return static_cast<int32_t>(offsetof(AdvBackLogFilter_t120185244, ___filterMessageWindowNames_3)); }
	inline List_1_t1398341365 * get_filterMessageWindowNames_3() const { return ___filterMessageWindowNames_3; }
	inline List_1_t1398341365 ** get_address_of_filterMessageWindowNames_3() { return &___filterMessageWindowNames_3; }
	inline void set_filterMessageWindowNames_3(List_1_t1398341365 * value)
	{
		___filterMessageWindowNames_3 = value;
		Il2CppCodeGenWriteBarrier((&___filterMessageWindowNames_3), value);
	}

	inline static int32_t get_offset_of_engine_4() { return static_cast<int32_t>(offsetof(AdvBackLogFilter_t120185244, ___engine_4)); }
	inline AdvEngine_t1176753927 * get_engine_4() const { return ___engine_4; }
	inline AdvEngine_t1176753927 ** get_address_of_engine_4() { return &___engine_4; }
	inline void set_engine_4(AdvEngine_t1176753927 * value)
	{
		___engine_4 = value;
		Il2CppCodeGenWriteBarrier((&___engine_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ADVBACKLOGFILTER_T120185244_H
#ifndef ADVAGINGTEST_T4147764237_H
#define ADVAGINGTEST_T4147764237_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.AdvAgingTest
struct  AdvAgingTest_t4147764237  : public MonoBehaviour_t1158329972
{
public:
	// Utage.AdvAgingTest/Type Utage.AdvAgingTest::type
	int32_t ___type_2;
	// System.Boolean Utage.AdvAgingTest::disable
	bool ___disable_3;
	// Utage.AdvAgingTest/SkipFlags Utage.AdvAgingTest::skipFilter
	int32_t ___skipFilter_4;
	// Utage.AdvEngine Utage.AdvAgingTest::engine
	AdvEngine_t1176753927 * ___engine_5;
	// System.Single Utage.AdvAgingTest::waitTime
	float ___waitTime_6;
	// System.Single Utage.AdvAgingTest::time
	float ___time_7;
	// System.Boolean Utage.AdvAgingTest::clearOnEnd
	bool ___clearOnEnd_8;
	// System.Collections.Generic.Dictionary`2<Utage.AdvScenarioPageData,System.Int32> Utage.AdvAgingTest::selectedDictionary
	Dictionary_2_t1287095029 * ___selectedDictionary_9;

public:
	inline static int32_t get_offset_of_type_2() { return static_cast<int32_t>(offsetof(AdvAgingTest_t4147764237, ___type_2)); }
	inline int32_t get_type_2() const { return ___type_2; }
	inline int32_t* get_address_of_type_2() { return &___type_2; }
	inline void set_type_2(int32_t value)
	{
		___type_2 = value;
	}

	inline static int32_t get_offset_of_disable_3() { return static_cast<int32_t>(offsetof(AdvAgingTest_t4147764237, ___disable_3)); }
	inline bool get_disable_3() const { return ___disable_3; }
	inline bool* get_address_of_disable_3() { return &___disable_3; }
	inline void set_disable_3(bool value)
	{
		___disable_3 = value;
	}

	inline static int32_t get_offset_of_skipFilter_4() { return static_cast<int32_t>(offsetof(AdvAgingTest_t4147764237, ___skipFilter_4)); }
	inline int32_t get_skipFilter_4() const { return ___skipFilter_4; }
	inline int32_t* get_address_of_skipFilter_4() { return &___skipFilter_4; }
	inline void set_skipFilter_4(int32_t value)
	{
		___skipFilter_4 = value;
	}

	inline static int32_t get_offset_of_engine_5() { return static_cast<int32_t>(offsetof(AdvAgingTest_t4147764237, ___engine_5)); }
	inline AdvEngine_t1176753927 * get_engine_5() const { return ___engine_5; }
	inline AdvEngine_t1176753927 ** get_address_of_engine_5() { return &___engine_5; }
	inline void set_engine_5(AdvEngine_t1176753927 * value)
	{
		___engine_5 = value;
		Il2CppCodeGenWriteBarrier((&___engine_5), value);
	}

	inline static int32_t get_offset_of_waitTime_6() { return static_cast<int32_t>(offsetof(AdvAgingTest_t4147764237, ___waitTime_6)); }
	inline float get_waitTime_6() const { return ___waitTime_6; }
	inline float* get_address_of_waitTime_6() { return &___waitTime_6; }
	inline void set_waitTime_6(float value)
	{
		___waitTime_6 = value;
	}

	inline static int32_t get_offset_of_time_7() { return static_cast<int32_t>(offsetof(AdvAgingTest_t4147764237, ___time_7)); }
	inline float get_time_7() const { return ___time_7; }
	inline float* get_address_of_time_7() { return &___time_7; }
	inline void set_time_7(float value)
	{
		___time_7 = value;
	}

	inline static int32_t get_offset_of_clearOnEnd_8() { return static_cast<int32_t>(offsetof(AdvAgingTest_t4147764237, ___clearOnEnd_8)); }
	inline bool get_clearOnEnd_8() const { return ___clearOnEnd_8; }
	inline bool* get_address_of_clearOnEnd_8() { return &___clearOnEnd_8; }
	inline void set_clearOnEnd_8(bool value)
	{
		___clearOnEnd_8 = value;
	}

	inline static int32_t get_offset_of_selectedDictionary_9() { return static_cast<int32_t>(offsetof(AdvAgingTest_t4147764237, ___selectedDictionary_9)); }
	inline Dictionary_2_t1287095029 * get_selectedDictionary_9() const { return ___selectedDictionary_9; }
	inline Dictionary_2_t1287095029 ** get_address_of_selectedDictionary_9() { return &___selectedDictionary_9; }
	inline void set_selectedDictionary_9(Dictionary_2_t1287095029 * value)
	{
		___selectedDictionary_9 = value;
		Il2CppCodeGenWriteBarrier((&___selectedDictionary_9), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ADVAGINGTEST_T4147764237_H
#ifndef ADVITWEENPLAYER_T1991557872_H
#define ADVITWEENPLAYER_T1991557872_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.AdvITweenPlayer
struct  AdvITweenPlayer_t1991557872  : public MonoBehaviour_t1158329972
{
public:
	// Utage.iTweenData Utage.AdvITweenPlayer::data
	iTweenData_t1904063128 * ___data_2;
	// System.Collections.Hashtable Utage.AdvITweenPlayer::hashTbl
	Hashtable_t909839986 * ___hashTbl_3;
	// System.Action`1<Utage.AdvITweenPlayer> Utage.AdvITweenPlayer::callbackComplete
	Action_1_t1793357254 * ___callbackComplete_4;
	// System.Boolean Utage.AdvITweenPlayer::isColorSprite
	bool ___isColorSprite_5;
	// System.Int32 Utage.AdvITweenPlayer::count
	int32_t ___count_6;
	// System.String Utage.AdvITweenPlayer::tweenName
	String_t* ___tweenName_7;
	// System.Boolean Utage.AdvITweenPlayer::isPlaying
	bool ___isPlaying_8;
	// Utage.AdvEffectColor Utage.AdvITweenPlayer::target
	AdvEffectColor_t2285992559 * ___target_9;
	// System.Collections.Generic.List`1<Utage.AdvITweenPlayer> Utage.AdvITweenPlayer::oldTweenPlayers
	List_1_t1360679004 * ___oldTweenPlayers_10;

public:
	inline static int32_t get_offset_of_data_2() { return static_cast<int32_t>(offsetof(AdvITweenPlayer_t1991557872, ___data_2)); }
	inline iTweenData_t1904063128 * get_data_2() const { return ___data_2; }
	inline iTweenData_t1904063128 ** get_address_of_data_2() { return &___data_2; }
	inline void set_data_2(iTweenData_t1904063128 * value)
	{
		___data_2 = value;
		Il2CppCodeGenWriteBarrier((&___data_2), value);
	}

	inline static int32_t get_offset_of_hashTbl_3() { return static_cast<int32_t>(offsetof(AdvITweenPlayer_t1991557872, ___hashTbl_3)); }
	inline Hashtable_t909839986 * get_hashTbl_3() const { return ___hashTbl_3; }
	inline Hashtable_t909839986 ** get_address_of_hashTbl_3() { return &___hashTbl_3; }
	inline void set_hashTbl_3(Hashtable_t909839986 * value)
	{
		___hashTbl_3 = value;
		Il2CppCodeGenWriteBarrier((&___hashTbl_3), value);
	}

	inline static int32_t get_offset_of_callbackComplete_4() { return static_cast<int32_t>(offsetof(AdvITweenPlayer_t1991557872, ___callbackComplete_4)); }
	inline Action_1_t1793357254 * get_callbackComplete_4() const { return ___callbackComplete_4; }
	inline Action_1_t1793357254 ** get_address_of_callbackComplete_4() { return &___callbackComplete_4; }
	inline void set_callbackComplete_4(Action_1_t1793357254 * value)
	{
		___callbackComplete_4 = value;
		Il2CppCodeGenWriteBarrier((&___callbackComplete_4), value);
	}

	inline static int32_t get_offset_of_isColorSprite_5() { return static_cast<int32_t>(offsetof(AdvITweenPlayer_t1991557872, ___isColorSprite_5)); }
	inline bool get_isColorSprite_5() const { return ___isColorSprite_5; }
	inline bool* get_address_of_isColorSprite_5() { return &___isColorSprite_5; }
	inline void set_isColorSprite_5(bool value)
	{
		___isColorSprite_5 = value;
	}

	inline static int32_t get_offset_of_count_6() { return static_cast<int32_t>(offsetof(AdvITweenPlayer_t1991557872, ___count_6)); }
	inline int32_t get_count_6() const { return ___count_6; }
	inline int32_t* get_address_of_count_6() { return &___count_6; }
	inline void set_count_6(int32_t value)
	{
		___count_6 = value;
	}

	inline static int32_t get_offset_of_tweenName_7() { return static_cast<int32_t>(offsetof(AdvITweenPlayer_t1991557872, ___tweenName_7)); }
	inline String_t* get_tweenName_7() const { return ___tweenName_7; }
	inline String_t** get_address_of_tweenName_7() { return &___tweenName_7; }
	inline void set_tweenName_7(String_t* value)
	{
		___tweenName_7 = value;
		Il2CppCodeGenWriteBarrier((&___tweenName_7), value);
	}

	inline static int32_t get_offset_of_isPlaying_8() { return static_cast<int32_t>(offsetof(AdvITweenPlayer_t1991557872, ___isPlaying_8)); }
	inline bool get_isPlaying_8() const { return ___isPlaying_8; }
	inline bool* get_address_of_isPlaying_8() { return &___isPlaying_8; }
	inline void set_isPlaying_8(bool value)
	{
		___isPlaying_8 = value;
	}

	inline static int32_t get_offset_of_target_9() { return static_cast<int32_t>(offsetof(AdvITweenPlayer_t1991557872, ___target_9)); }
	inline AdvEffectColor_t2285992559 * get_target_9() const { return ___target_9; }
	inline AdvEffectColor_t2285992559 ** get_address_of_target_9() { return &___target_9; }
	inline void set_target_9(AdvEffectColor_t2285992559 * value)
	{
		___target_9 = value;
		Il2CppCodeGenWriteBarrier((&___target_9), value);
	}

	inline static int32_t get_offset_of_oldTweenPlayers_10() { return static_cast<int32_t>(offsetof(AdvITweenPlayer_t1991557872, ___oldTweenPlayers_10)); }
	inline List_1_t1360679004 * get_oldTweenPlayers_10() const { return ___oldTweenPlayers_10; }
	inline List_1_t1360679004 ** get_address_of_oldTweenPlayers_10() { return &___oldTweenPlayers_10; }
	inline void set_oldTweenPlayers_10(List_1_t1360679004 * value)
	{
		___oldTweenPlayers_10 = value;
		Il2CppCodeGenWriteBarrier((&___oldTweenPlayers_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ADVITWEENPLAYER_T1991557872_H
#ifndef ADVEFFECTMANAGER_T3829746105_H
#define ADVEFFECTMANAGER_T3829746105_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.AdvEffectManager
struct  AdvEffectManager_t3829746105  : public MonoBehaviour_t1158329972
{
public:
	// Utage.AdvEngine Utage.AdvEffectManager::engine
	AdvEngine_t1176753927 * ___engine_2;
	// Utage.AdvUguiMessageWindowManager Utage.AdvEffectManager::messageWindow
	AdvUguiMessageWindowManager_t1210998287 * ___messageWindow_3;
	// System.Collections.Generic.List`1<UnityEngine.Texture2D> Utage.AdvEffectManager::ruleTextureList
	List_1_t2912116861 * ___ruleTextureList_4;

public:
	inline static int32_t get_offset_of_engine_2() { return static_cast<int32_t>(offsetof(AdvEffectManager_t3829746105, ___engine_2)); }
	inline AdvEngine_t1176753927 * get_engine_2() const { return ___engine_2; }
	inline AdvEngine_t1176753927 ** get_address_of_engine_2() { return &___engine_2; }
	inline void set_engine_2(AdvEngine_t1176753927 * value)
	{
		___engine_2 = value;
		Il2CppCodeGenWriteBarrier((&___engine_2), value);
	}

	inline static int32_t get_offset_of_messageWindow_3() { return static_cast<int32_t>(offsetof(AdvEffectManager_t3829746105, ___messageWindow_3)); }
	inline AdvUguiMessageWindowManager_t1210998287 * get_messageWindow_3() const { return ___messageWindow_3; }
	inline AdvUguiMessageWindowManager_t1210998287 ** get_address_of_messageWindow_3() { return &___messageWindow_3; }
	inline void set_messageWindow_3(AdvUguiMessageWindowManager_t1210998287 * value)
	{
		___messageWindow_3 = value;
		Il2CppCodeGenWriteBarrier((&___messageWindow_3), value);
	}

	inline static int32_t get_offset_of_ruleTextureList_4() { return static_cast<int32_t>(offsetof(AdvEffectManager_t3829746105, ___ruleTextureList_4)); }
	inline List_1_t2912116861 * get_ruleTextureList_4() const { return ___ruleTextureList_4; }
	inline List_1_t2912116861 ** get_address_of_ruleTextureList_4() { return &___ruleTextureList_4; }
	inline void set_ruleTextureList_4(List_1_t2912116861 * value)
	{
		___ruleTextureList_4 = value;
		Il2CppCodeGenWriteBarrier((&___ruleTextureList_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ADVEFFECTMANAGER_T3829746105_H
#ifndef ADVANIMATIONPLAYER_T1530269518_H
#define ADVANIMATIONPLAYER_T1530269518_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.AdvAnimationPlayer
struct  AdvAnimationPlayer_t1530269518  : public MonoBehaviour_t1158329972
{
public:
	// System.Boolean Utage.AdvAnimationPlayer::<AutoDestory>k__BackingField
	bool ___U3CAutoDestoryU3Ek__BackingField_3;
	// System.Boolean Utage.AdvAnimationPlayer::<EnableSave>k__BackingField
	bool ___U3CEnableSaveU3Ek__BackingField_4;
	// UnityEngine.AnimationClip Utage.AdvAnimationPlayer::<Clip>k__BackingField
	AnimationClip_t3510324950 * ___U3CClipU3Ek__BackingField_5;
	// System.Single Utage.AdvAnimationPlayer::<Speed>k__BackingField
	float ___U3CSpeedU3Ek__BackingField_6;
	// System.Action Utage.AdvAnimationPlayer::onComplete
	Action_t3226471752 * ___onComplete_7;
	// UnityEngine.Animation Utage.AdvAnimationPlayer::lecayAnimation
	Animation_t2068071072 * ___lecayAnimation_8;
	// UnityEngine.Animator Utage.AdvAnimationPlayer::animator
	Animator_t69676727 * ___animator_9;

public:
	inline static int32_t get_offset_of_U3CAutoDestoryU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(AdvAnimationPlayer_t1530269518, ___U3CAutoDestoryU3Ek__BackingField_3)); }
	inline bool get_U3CAutoDestoryU3Ek__BackingField_3() const { return ___U3CAutoDestoryU3Ek__BackingField_3; }
	inline bool* get_address_of_U3CAutoDestoryU3Ek__BackingField_3() { return &___U3CAutoDestoryU3Ek__BackingField_3; }
	inline void set_U3CAutoDestoryU3Ek__BackingField_3(bool value)
	{
		___U3CAutoDestoryU3Ek__BackingField_3 = value;
	}

	inline static int32_t get_offset_of_U3CEnableSaveU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(AdvAnimationPlayer_t1530269518, ___U3CEnableSaveU3Ek__BackingField_4)); }
	inline bool get_U3CEnableSaveU3Ek__BackingField_4() const { return ___U3CEnableSaveU3Ek__BackingField_4; }
	inline bool* get_address_of_U3CEnableSaveU3Ek__BackingField_4() { return &___U3CEnableSaveU3Ek__BackingField_4; }
	inline void set_U3CEnableSaveU3Ek__BackingField_4(bool value)
	{
		___U3CEnableSaveU3Ek__BackingField_4 = value;
	}

	inline static int32_t get_offset_of_U3CClipU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(AdvAnimationPlayer_t1530269518, ___U3CClipU3Ek__BackingField_5)); }
	inline AnimationClip_t3510324950 * get_U3CClipU3Ek__BackingField_5() const { return ___U3CClipU3Ek__BackingField_5; }
	inline AnimationClip_t3510324950 ** get_address_of_U3CClipU3Ek__BackingField_5() { return &___U3CClipU3Ek__BackingField_5; }
	inline void set_U3CClipU3Ek__BackingField_5(AnimationClip_t3510324950 * value)
	{
		___U3CClipU3Ek__BackingField_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3CClipU3Ek__BackingField_5), value);
	}

	inline static int32_t get_offset_of_U3CSpeedU3Ek__BackingField_6() { return static_cast<int32_t>(offsetof(AdvAnimationPlayer_t1530269518, ___U3CSpeedU3Ek__BackingField_6)); }
	inline float get_U3CSpeedU3Ek__BackingField_6() const { return ___U3CSpeedU3Ek__BackingField_6; }
	inline float* get_address_of_U3CSpeedU3Ek__BackingField_6() { return &___U3CSpeedU3Ek__BackingField_6; }
	inline void set_U3CSpeedU3Ek__BackingField_6(float value)
	{
		___U3CSpeedU3Ek__BackingField_6 = value;
	}

	inline static int32_t get_offset_of_onComplete_7() { return static_cast<int32_t>(offsetof(AdvAnimationPlayer_t1530269518, ___onComplete_7)); }
	inline Action_t3226471752 * get_onComplete_7() const { return ___onComplete_7; }
	inline Action_t3226471752 ** get_address_of_onComplete_7() { return &___onComplete_7; }
	inline void set_onComplete_7(Action_t3226471752 * value)
	{
		___onComplete_7 = value;
		Il2CppCodeGenWriteBarrier((&___onComplete_7), value);
	}

	inline static int32_t get_offset_of_lecayAnimation_8() { return static_cast<int32_t>(offsetof(AdvAnimationPlayer_t1530269518, ___lecayAnimation_8)); }
	inline Animation_t2068071072 * get_lecayAnimation_8() const { return ___lecayAnimation_8; }
	inline Animation_t2068071072 ** get_address_of_lecayAnimation_8() { return &___lecayAnimation_8; }
	inline void set_lecayAnimation_8(Animation_t2068071072 * value)
	{
		___lecayAnimation_8 = value;
		Il2CppCodeGenWriteBarrier((&___lecayAnimation_8), value);
	}

	inline static int32_t get_offset_of_animator_9() { return static_cast<int32_t>(offsetof(AdvAnimationPlayer_t1530269518, ___animator_9)); }
	inline Animator_t69676727 * get_animator_9() const { return ___animator_9; }
	inline Animator_t69676727 ** get_address_of_animator_9() { return &___animator_9; }
	inline void set_animator_9(Animator_t69676727 * value)
	{
		___animator_9 = value;
		Il2CppCodeGenWriteBarrier((&___animator_9), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ADVANIMATIONPLAYER_T1530269518_H
#ifndef ADVTEXTSOUND_T3558538083_H
#define ADVTEXTSOUND_T3558538083_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.AdvTextSound
struct  AdvTextSound_t3558538083  : public MonoBehaviour_t1158329972
{
public:
	// System.Boolean Utage.AdvTextSound::disable
	bool ___disable_2;
	// Utage.AdvEngine Utage.AdvTextSound::engine
	AdvEngine_t1176753927 * ___engine_3;
	// Utage.AdvTextSound/Type Utage.AdvTextSound::type
	int32_t ___type_4;
	// UnityEngine.AudioClip Utage.AdvTextSound::defaultSound
	AudioClip_t1932558630 * ___defaultSound_5;
	// System.Collections.Generic.List`1<Utage.AdvTextSound/SoundInfo> Utage.AdvTextSound::soundInfoList
	List_1_t2907673085 * ___soundInfoList_6;
	// System.Int32 Utage.AdvTextSound::intervalCount
	int32_t ___intervalCount_7;
	// System.Single Utage.AdvTextSound::intervalTime
	float ___intervalTime_8;
	// System.Single Utage.AdvTextSound::lastTime
	float ___lastTime_9;
	// System.Int32 Utage.AdvTextSound::lastCharacterCount
	int32_t ___lastCharacterCount_10;

public:
	inline static int32_t get_offset_of_disable_2() { return static_cast<int32_t>(offsetof(AdvTextSound_t3558538083, ___disable_2)); }
	inline bool get_disable_2() const { return ___disable_2; }
	inline bool* get_address_of_disable_2() { return &___disable_2; }
	inline void set_disable_2(bool value)
	{
		___disable_2 = value;
	}

	inline static int32_t get_offset_of_engine_3() { return static_cast<int32_t>(offsetof(AdvTextSound_t3558538083, ___engine_3)); }
	inline AdvEngine_t1176753927 * get_engine_3() const { return ___engine_3; }
	inline AdvEngine_t1176753927 ** get_address_of_engine_3() { return &___engine_3; }
	inline void set_engine_3(AdvEngine_t1176753927 * value)
	{
		___engine_3 = value;
		Il2CppCodeGenWriteBarrier((&___engine_3), value);
	}

	inline static int32_t get_offset_of_type_4() { return static_cast<int32_t>(offsetof(AdvTextSound_t3558538083, ___type_4)); }
	inline int32_t get_type_4() const { return ___type_4; }
	inline int32_t* get_address_of_type_4() { return &___type_4; }
	inline void set_type_4(int32_t value)
	{
		___type_4 = value;
	}

	inline static int32_t get_offset_of_defaultSound_5() { return static_cast<int32_t>(offsetof(AdvTextSound_t3558538083, ___defaultSound_5)); }
	inline AudioClip_t1932558630 * get_defaultSound_5() const { return ___defaultSound_5; }
	inline AudioClip_t1932558630 ** get_address_of_defaultSound_5() { return &___defaultSound_5; }
	inline void set_defaultSound_5(AudioClip_t1932558630 * value)
	{
		___defaultSound_5 = value;
		Il2CppCodeGenWriteBarrier((&___defaultSound_5), value);
	}

	inline static int32_t get_offset_of_soundInfoList_6() { return static_cast<int32_t>(offsetof(AdvTextSound_t3558538083, ___soundInfoList_6)); }
	inline List_1_t2907673085 * get_soundInfoList_6() const { return ___soundInfoList_6; }
	inline List_1_t2907673085 ** get_address_of_soundInfoList_6() { return &___soundInfoList_6; }
	inline void set_soundInfoList_6(List_1_t2907673085 * value)
	{
		___soundInfoList_6 = value;
		Il2CppCodeGenWriteBarrier((&___soundInfoList_6), value);
	}

	inline static int32_t get_offset_of_intervalCount_7() { return static_cast<int32_t>(offsetof(AdvTextSound_t3558538083, ___intervalCount_7)); }
	inline int32_t get_intervalCount_7() const { return ___intervalCount_7; }
	inline int32_t* get_address_of_intervalCount_7() { return &___intervalCount_7; }
	inline void set_intervalCount_7(int32_t value)
	{
		___intervalCount_7 = value;
	}

	inline static int32_t get_offset_of_intervalTime_8() { return static_cast<int32_t>(offsetof(AdvTextSound_t3558538083, ___intervalTime_8)); }
	inline float get_intervalTime_8() const { return ___intervalTime_8; }
	inline float* get_address_of_intervalTime_8() { return &___intervalTime_8; }
	inline void set_intervalTime_8(float value)
	{
		___intervalTime_8 = value;
	}

	inline static int32_t get_offset_of_lastTime_9() { return static_cast<int32_t>(offsetof(AdvTextSound_t3558538083, ___lastTime_9)); }
	inline float get_lastTime_9() const { return ___lastTime_9; }
	inline float* get_address_of_lastTime_9() { return &___lastTime_9; }
	inline void set_lastTime_9(float value)
	{
		___lastTime_9 = value;
	}

	inline static int32_t get_offset_of_lastCharacterCount_10() { return static_cast<int32_t>(offsetof(AdvTextSound_t3558538083, ___lastCharacterCount_10)); }
	inline int32_t get_lastCharacterCount_10() const { return ___lastCharacterCount_10; }
	inline int32_t* get_address_of_lastCharacterCount_10() { return &___lastCharacterCount_10; }
	inline void set_lastCharacterCount_10(int32_t value)
	{
		___lastCharacterCount_10 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ADVTEXTSOUND_T3558538083_H





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2300 = { sizeof (AdvCommandSendMessageByName_t3179247017), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2300[2] = 
{
	AdvCommandSendMessageByName_t3179247017::get_offset_of_U3CIsWaitU3Ek__BackingField_7(),
	AdvCommandSendMessageByName_t3179247017::get_offset_of_U3CEngineU3Ek__BackingField_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2301 = { sizeof (AdvChapterData_t685691324), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2301[4] = 
{
	AdvChapterData_t685691324::get_offset_of_chapterName_2(),
	AdvChapterData_t685691324::get_offset_of_dataList_3(),
	AdvChapterData_t685691324::get_offset_of_settingList_4(),
	AdvChapterData_t685691324::get_offset_of_U3CIsInitedU3Ek__BackingField_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2302 = { sizeof (AdvColumnName_t578583962)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2302[79] = 
{
	AdvColumnName_t578583962::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2303 = { sizeof (AdvColumnNameExtentison_t2488705969), -1, sizeof(AdvColumnNameExtentison_t2488705969_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2303[1] = 
{
	AdvColumnNameExtentison_t2488705969_StaticFields::get_offset_of_names_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2304 = { sizeof (AdvDataManager_t2481232830), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2304[4] = 
{
	AdvDataManager_t2481232830::get_offset_of_isBackGroundDownload_2(),
	AdvDataManager_t2481232830::get_offset_of_settingDataManager_3(),
	AdvDataManager_t2481232830::get_offset_of_scenarioDataTbl_4(),
	AdvDataManager_t2481232830::get_offset_of_macroManager_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2305 = { sizeof (U3CCoBootInitScenariodDataU3Ec__Iterator0_t2934450674), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2305[6] = 
{
	U3CCoBootInitScenariodDataU3Ec__Iterator0_t2934450674::get_offset_of_U24locvar0_0(),
	U3CCoBootInitScenariodDataU3Ec__Iterator0_t2934450674::get_offset_of_U3CdataU3E__1_1(),
	U3CCoBootInitScenariodDataU3Ec__Iterator0_t2934450674::get_offset_of_U24this_2(),
	U3CCoBootInitScenariodDataU3Ec__Iterator0_t2934450674::get_offset_of_U24current_3(),
	U3CCoBootInitScenariodDataU3Ec__Iterator0_t2934450674::get_offset_of_U24disposing_4(),
	U3CCoBootInitScenariodDataU3Ec__Iterator0_t2934450674::get_offset_of_U24PC_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2306 = { sizeof (AdvImportBook_t3958042025), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2306[3] = 
{
	0,
	AdvImportBook_t3958042025::get_offset_of_importVersion_3(),
	AdvImportBook_t3958042025::get_offset_of_importGridList_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2307 = { sizeof (AdvImportScenarios_t1226385437), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2307[3] = 
{
	0,
	AdvImportScenarios_t1226385437::get_offset_of_importVersion_3(),
	AdvImportScenarios_t1226385437::get_offset_of_chapters_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2308 = { sizeof (U3CTryAddChapterU3Ec__AnonStorey0_t2946455296), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2308[1] = 
{
	U3CTryAddChapterU3Ec__AnonStorey0_t2946455296::get_offset_of_chapterData_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2309 = { sizeof (AdvImportScenarioSheet_t3549824885), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2309[2] = 
{
	AdvImportScenarioSheet_t3549824885::get_offset_of_entityIndexTbl_7(),
	AdvImportScenarioSheet_t3549824885::get_offset_of_entityDataList_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2310 = { sizeof (StringGridRowMacroed_t3994913000), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2310[2] = 
{
	StringGridRowMacroed_t3994913000::get_offset_of_row_0(),
	StringGridRowMacroed_t3994913000::get_offset_of_entityData_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2311 = { sizeof (AdvParser_t1629086016), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2312 = { sizeof (AdvSettingDataManager_t931476416), -1, sizeof(AdvSettingDataManager_t931476416_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2312[16] = 
{
	AdvSettingDataManager_t931476416::get_offset_of_U3CImportedScenariosU3Ek__BackingField_0(),
	AdvSettingDataManager_t931476416::get_offset_of_bootSetting_1(),
	AdvSettingDataManager_t931476416::get_offset_of_characterSetting_2(),
	AdvSettingDataManager_t931476416::get_offset_of_textureSetting_3(),
	AdvSettingDataManager_t931476416::get_offset_of_soundSetting_4(),
	AdvSettingDataManager_t931476416::get_offset_of_layerSetting_5(),
	AdvSettingDataManager_t931476416::get_offset_of_defaultParam_6(),
	AdvSettingDataManager_t931476416::get_offset_of_sceneGallerySetting_7(),
	AdvSettingDataManager_t931476416::get_offset_of_localizeSetting_8(),
	AdvSettingDataManager_t931476416::get_offset_of_animationSetting_9(),
	AdvSettingDataManager_t931476416::get_offset_of_eyeBlinkSetting_10(),
	AdvSettingDataManager_t931476416::get_offset_of_lipSynchSetting_11(),
	AdvSettingDataManager_t931476416::get_offset_of_advParticleSetting_12(),
	AdvSettingDataManager_t931476416::get_offset_of_videoSetting_13(),
	AdvSettingDataManager_t931476416::get_offset_of_settingDataList_14(),
	AdvSettingDataManager_t931476416_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2313 = { sizeof (AdvSheetParser_t3206602165), -1, sizeof(AdvSheetParser_t3206602165_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2313[16] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	AdvSheetParser_t3206602165_StaticFields::get_offset_of_SheetNameRegex_12(),
	AdvSheetParser_t3206602165_StaticFields::get_offset_of_AnimationSheetNameRegix_13(),
	AdvSheetParser_t3206602165_StaticFields::get_offset_of_U3CU3Ef__switchU24map1_14(),
	AdvSheetParser_t3206602165_StaticFields::get_offset_of_U3CU3Ef__switchU24map2_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2314 = { sizeof (AdvAnimationData_t3431567515), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2314[1] = 
{
	AdvAnimationData_t3431567515::get_offset_of_U3CClipU3Ek__BackingField_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2315 = { sizeof (PropertyType_t3955667347)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2315[15] = 
{
	PropertyType_t3955667347::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2316 = { sizeof (AdvAnimationSetting_t2232720937), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2316[1] = 
{
	AdvAnimationSetting_t2232720937::get_offset_of_DataList_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2317 = { sizeof (U3CFindU3Ec__AnonStorey0_t2022056998), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2317[1] = 
{
	U3CFindU3Ec__AnonStorey0_t2022056998::get_offset_of_name_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2318 = { sizeof (AdvBootSetting_t3720085301), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2318[12] = 
{
	AdvBootSetting_t3720085301::get_offset_of_U3CResourceDirU3Ek__BackingField_0(),
	AdvBootSetting_t3720085301::get_offset_of_characterDirInfo_1(),
	AdvBootSetting_t3720085301::get_offset_of_bgDirInfo_2(),
	AdvBootSetting_t3720085301::get_offset_of_eventDirInfo_3(),
	AdvBootSetting_t3720085301::get_offset_of_spriteDirInfo_4(),
	AdvBootSetting_t3720085301::get_offset_of_thumbnailDirInfo_5(),
	AdvBootSetting_t3720085301::get_offset_of_bgmDirInfo_6(),
	AdvBootSetting_t3720085301::get_offset_of_seDirInfo_7(),
	AdvBootSetting_t3720085301::get_offset_of_ambienceDirInfo_8(),
	AdvBootSetting_t3720085301::get_offset_of_voiceDirInfo_9(),
	AdvBootSetting_t3720085301::get_offset_of_particleDirInfo_10(),
	AdvBootSetting_t3720085301::get_offset_of_videoDirInfo_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2319 = { sizeof (DefaultDirInfo_t3178678002), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2319[2] = 
{
	DefaultDirInfo_t3178678002::get_offset_of_defaultDir_0(),
	DefaultDirInfo_t3178678002::get_offset_of_defaultExt_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2320 = { sizeof (AdvCgGalleryData_t4276096361), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2320[3] = 
{
	AdvCgGalleryData_t4276096361::get_offset_of_list_0(),
	AdvCgGalleryData_t4276096361::get_offset_of_thumbnailPath_1(),
	AdvCgGalleryData_t4276096361::get_offset_of_saveData_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2321 = { sizeof (AdvCharacterInfo_t1582765630), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2321[5] = 
{
	AdvCharacterInfo_t1582765630::get_offset_of_U3CLabelU3Ek__BackingField_0(),
	AdvCharacterInfo_t1582765630::get_offset_of_U3CNameTextU3Ek__BackingField_1(),
	AdvCharacterInfo_t1582765630::get_offset_of_U3CPatternU3Ek__BackingField_2(),
	AdvCharacterInfo_t1582765630::get_offset_of_U3CIsHideU3Ek__BackingField_3(),
	AdvCharacterInfo_t1582765630::get_offset_of_U3CGraphicU3Ek__BackingField_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2322 = { sizeof (U3CCreateU3Ec__AnonStorey0_t3183710506), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2322[3] = 
{
	U3CCreateU3Ec__AnonStorey0_t3183710506::get_offset_of_isHide_0(),
	U3CCreateU3Ec__AnonStorey0_t3183710506::get_offset_of_characterLabel_1(),
	U3CCreateU3Ec__AnonStorey0_t3183710506::get_offset_of_erroMsg_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2323 = { sizeof (AdvCharacterSettingData_t1671945242), -1, sizeof(AdvCharacterSettingData_t1671945242_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2323[6] = 
{
	AdvCharacterSettingData_t1671945242_StaticFields::get_offset_of_CallbackParseCustomFileTypeRootDir_2(),
	AdvCharacterSettingData_t1671945242::get_offset_of_name_3(),
	AdvCharacterSettingData_t1671945242::get_offset_of_pattern_4(),
	AdvCharacterSettingData_t1671945242::get_offset_of_nameText_5(),
	AdvCharacterSettingData_t1671945242::get_offset_of_graphic_6(),
	AdvCharacterSettingData_t1671945242::get_offset_of_U3CIconU3Ek__BackingField_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2324 = { sizeof (ParseCustomFileTypeRootDir_t3762798398), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2325 = { sizeof (IconInfo_t3287921618), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2325[5] = 
{
	IconInfo_t3287921618::get_offset_of_U3CIconTypeU3Ek__BackingField_0(),
	IconInfo_t3287921618::get_offset_of_U3CFileNameU3Ek__BackingField_1(),
	IconInfo_t3287921618::get_offset_of_U3CFileU3Ek__BackingField_2(),
	IconInfo_t3287921618::get_offset_of_U3CIconRectU3Ek__BackingField_3(),
	IconInfo_t3287921618::get_offset_of_U3CIconSubFileNameU3Ek__BackingField_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2326 = { sizeof (Type_t927793387)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2326[5] = 
{
	Type_t927793387::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2327 = { sizeof (U3CBootInitU3Ec__AnonStorey0_t2996028236), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2327[2] = 
{
	U3CBootInitU3Ec__AnonStorey0_t2996028236::get_offset_of_dataManager_0(),
	U3CBootInitU3Ec__AnonStorey0_t2996028236::get_offset_of_U24this_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2328 = { sizeof (AdvCharacterSetting_t2712258826), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2328[1] = 
{
	AdvCharacterSetting_t2712258826::get_offset_of_defaultKey_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2329 = { sizeof (AdvEyeBlinkData_t3287347958), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2329[5] = 
{
	AdvEyeBlinkData_t3287347958::get_offset_of_intervalMin_2(),
	AdvEyeBlinkData_t3287347958::get_offset_of_intervalMax_3(),
	AdvEyeBlinkData_t3287347958::get_offset_of_randomDoubleEyeBlink_4(),
	AdvEyeBlinkData_t3287347958::get_offset_of_tag_5(),
	AdvEyeBlinkData_t3287347958::get_offset_of_animationData_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2330 = { sizeof (AdvEyeBlinkSetting_t1976982958), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2331 = { sizeof (AdvLayerSettingData_t1908772360), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2331[12] = 
{
	AdvLayerSettingData_t1908772360::get_offset_of_U3CTypeU3Ek__BackingField_2(),
	AdvLayerSettingData_t1908772360::get_offset_of_U3CHorizontalU3Ek__BackingField_3(),
	AdvLayerSettingData_t1908772360::get_offset_of_U3CVerticalU3Ek__BackingField_4(),
	AdvLayerSettingData_t1908772360::get_offset_of_U3CZU3Ek__BackingField_5(),
	AdvLayerSettingData_t1908772360::get_offset_of_U3CScaleU3Ek__BackingField_6(),
	AdvLayerSettingData_t1908772360::get_offset_of_U3CPivotU3Ek__BackingField_7(),
	AdvLayerSettingData_t1908772360::get_offset_of_U3COrderU3Ek__BackingField_8(),
	AdvLayerSettingData_t1908772360::get_offset_of_U3CLayerMaskU3Ek__BackingField_9(),
	AdvLayerSettingData_t1908772360::get_offset_of_U3CAlignmentU3Ek__BackingField_10(),
	AdvLayerSettingData_t1908772360::get_offset_of_U3CFlipXU3Ek__BackingField_11(),
	AdvLayerSettingData_t1908772360::get_offset_of_U3CFlipYU3Ek__BackingField_12(),
	AdvLayerSettingData_t1908772360::get_offset_of_isDefault_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2332 = { sizeof (LayerType_t2078485382)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2332[5] = 
{
	LayerType_t2078485382::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2333 = { sizeof (BorderType_t365579711)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2333[5] = 
{
	BorderType_t365579711::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2334 = { sizeof (RectSetting_t1088783529), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2334[5] = 
{
	RectSetting_t1088783529::get_offset_of_type_0(),
	RectSetting_t1088783529::get_offset_of_position_1(),
	RectSetting_t1088783529::get_offset_of_size_2(),
	RectSetting_t1088783529::get_offset_of_borderMin_3(),
	RectSetting_t1088783529::get_offset_of_borderMax_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2335 = { sizeof (AdvLayerSetting_t3281222136), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2336 = { sizeof (U3CInitDefaultU3Ec__AnonStorey0_t4265531495), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2336[1] = 
{
	U3CInitDefaultU3Ec__AnonStorey0_t4265531495::get_offset_of_type_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2337 = { sizeof (U3CFindDefaultLayerU3Ec__AnonStorey1_t1418813260), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2337[1] = 
{
	U3CFindDefaultLayerU3Ec__AnonStorey1_t1418813260::get_offset_of_type_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2338 = { sizeof (AdvLipSynchData_t1090448263), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2338[5] = 
{
	AdvLipSynchData_t1090448263::get_offset_of_type_2(),
	AdvLipSynchData_t1090448263::get_offset_of_interval_3(),
	AdvLipSynchData_t1090448263::get_offset_of_scaleVoiceVolume_4(),
	AdvLipSynchData_t1090448263::get_offset_of_tag_5(),
	AdvLipSynchData_t1090448263::get_offset_of_animationData_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2339 = { sizeof (AdvLipSynchSetting_t289134997), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2340 = { sizeof (AdvLocalizeSetting_t467665552), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2341 = { sizeof (AdvParticleSettingData_t1603520623), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2341[1] = 
{
	AdvParticleSettingData_t1603520623::get_offset_of_graphic_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2342 = { sizeof (U3CBootInitU3Ec__AnonStorey0_t2032018241), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2342[2] = 
{
	U3CBootInitU3Ec__AnonStorey0_t2032018241::get_offset_of_dataManager_0(),
	U3CBootInitU3Ec__AnonStorey0_t2032018241::get_offset_of_U24this_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2343 = { sizeof (AdvParticleSetting_t588078687), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2344 = { sizeof (AdvSceneGallerySettingData_t1280603049), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2344[5] = 
{
	AdvSceneGallerySettingData_t1280603049::get_offset_of_title_2(),
	AdvSceneGallerySettingData_t1280603049::get_offset_of_category_3(),
	AdvSceneGallerySettingData_t1280603049::get_offset_of_thumbnailName_4(),
	AdvSceneGallerySettingData_t1280603049::get_offset_of_thumbnailPath_5(),
	AdvSceneGallerySettingData_t1280603049::get_offset_of_thumbnailVersion_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2345 = { sizeof (AdvSceneGallerySetting_t2241589337), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2346 = { sizeof (AdvSettingBase_t3183557912), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2346[1] = 
{
	AdvSettingBase_t3183557912::get_offset_of_gridList_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2347 = { sizeof (AdvSettingDictinoayItemBase_t203751633), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2347[2] = 
{
	AdvSettingDictinoayItemBase_t203751633::get_offset_of_key_0(),
	AdvSettingDictinoayItemBase_t203751633::get_offset_of_U3CRowDataU3Ek__BackingField_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2348 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2348[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2349 = { sizeof (SoundType_t2173686033)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2349[6] = 
{
	SoundType_t2173686033::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2350 = { sizeof (AdvSoundSettingData_t3135391222), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2350[6] = 
{
	AdvSoundSettingData_t3135391222::get_offset_of_U3CTypeU3Ek__BackingField_2(),
	AdvSoundSettingData_t3135391222::get_offset_of_U3CTitleU3Ek__BackingField_3(),
	AdvSoundSettingData_t3135391222::get_offset_of_fileName_4(),
	AdvSoundSettingData_t3135391222::get_offset_of_U3CFilePathU3Ek__BackingField_5(),
	AdvSoundSettingData_t3135391222::get_offset_of_U3CIntroTimeU3Ek__BackingField_6(),
	AdvSoundSettingData_t3135391222::get_offset_of_U3CVolumeU3Ek__BackingField_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2351 = { sizeof (AdvSoundSetting_t229690022), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2352 = { sizeof (AdvTextureSettingData_t2976012266), -1, sizeof(AdvTextureSettingData_t2976012266_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2352[7] = 
{
	AdvTextureSettingData_t2976012266_StaticFields::get_offset_of_CallbackParseCustomFileTypeRootDir_2(),
	AdvTextureSettingData_t2976012266::get_offset_of_U3CTextureTypeU3Ek__BackingField_3(),
	AdvTextureSettingData_t2976012266::get_offset_of_U3CGraphicU3Ek__BackingField_4(),
	AdvTextureSettingData_t2976012266::get_offset_of_thumbnailName_5(),
	AdvTextureSettingData_t2976012266::get_offset_of_U3CThumbnailPathU3Ek__BackingField_6(),
	AdvTextureSettingData_t2976012266::get_offset_of_U3CThumbnailVersionU3Ek__BackingField_7(),
	AdvTextureSettingData_t2976012266::get_offset_of_U3CCgCategoryU3Ek__BackingField_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2353 = { sizeof (ParseCustomFileTypeRootDir_t2850020050), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2354 = { sizeof (Type_t3945296675)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2354[4] = 
{
	Type_t3945296675::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2355 = { sizeof (U3CBootInitU3Ec__AnonStorey0_t1781737112), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2355[2] = 
{
	U3CBootInitU3Ec__AnonStorey0_t1781737112::get_offset_of_dataManager_0(),
	U3CBootInitU3Ec__AnonStorey0_t1781737112::get_offset_of_U24this_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2356 = { sizeof (AdvTextureSetting_t2060921594), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2357 = { sizeof (AdvCommandSetting_t396459972), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2357[1] = 
{
	AdvCommandSetting_t396459972::get_offset_of_U3CCommandU3Ek__BackingField_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2358 = { sizeof (AdvVideoSettingData_t2742317570), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2358[1] = 
{
	AdvVideoSettingData_t2742317570::get_offset_of_graphic_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2359 = { sizeof (U3CBootInitU3Ec__AnonStorey0_t2854516436), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2359[2] = 
{
	U3CBootInitU3Ec__AnonStorey0_t2854516436::get_offset_of_dataManager_0(),
	U3CBootInitU3Ec__AnonStorey0_t2854516436::get_offset_of_U24this_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2360 = { sizeof (AdvVideoSetting_t3960366418), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2361 = { sizeof (AdvVoiceSetting_t1784182553), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2361[1] = 
{
	AdvVoiceSetting_t1784182553::get_offset_of_U3CRowDataU3Ek__BackingField_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2362 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2363 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2364 = { sizeof (AdvParamData_t1457431784), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2364[5] = 
{
	AdvParamData_t1457431784::get_offset_of_key_0(),
	AdvParamData_t1457431784::get_offset_of_type_1(),
	AdvParamData_t1457431784::get_offset_of_parameter_2(),
	AdvParamData_t1457431784::get_offset_of_parameterString_3(),
	AdvParamData_t1457431784::get_offset_of_fileType_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2365 = { sizeof (ParamType_t2968970818)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2365[5] = 
{
	ParamType_t2968970818::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2366 = { sizeof (FileType_t1382753447)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2366[4] = 
{
	FileType_t1382753447::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2367 = { sizeof (AdvParamManager_t1816006425), -1, sizeof(AdvParamManager_t1816006425_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2367[10] = 
{
	0,
	0,
	AdvParamManager_t1816006425_StaticFields::get_offset_of_KeyRegix_3(),
	AdvParamManager_t1816006425::get_offset_of_U3CIsInitU3Ek__BackingField_4(),
	AdvParamManager_t1816006425::get_offset_of_structTbl_5(),
	AdvParamManager_t1816006425::get_offset_of_U3CHasChangedSystemParamU3Ek__BackingField_6(),
	AdvParamManager_t1816006425::get_offset_of_U3CDefaultParameterU3Ek__BackingField_7(),
	0,
	AdvParamManager_t1816006425::get_offset_of_systemData_9(),
	AdvParamManager_t1816006425::get_offset_of_defaultData_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2368 = { sizeof (IoInerface_t3434613093), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2368[2] = 
{
	IoInerface_t3434613093::get_offset_of_U3CFileTypeU3Ek__BackingField_0(),
	IoInerface_t3434613093::get_offset_of_U3CParamU3Ek__BackingField_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2369 = { sizeof (U3CWriteU3Ec__AnonStorey1_t894999319), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2369[1] = 
{
	U3CWriteU3Ec__AnonStorey1_t894999319::get_offset_of_fileType_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2370 = { sizeof (U3CWriteU3Ec__AnonStorey0_t894999320), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2370[2] = 
{
	U3CWriteU3Ec__AnonStorey0_t894999320::get_offset_of_keyValue_0(),
	U3CWriteU3Ec__AnonStorey0_t894999320::get_offset_of_U3CU3Ef__refU241_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2371 = { sizeof (U3CReadU3Ec__AnonStorey3_t2127335072), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2371[2] = 
{
	U3CReadU3Ec__AnonStorey3_t2127335072::get_offset_of_fileType_0(),
	U3CReadU3Ec__AnonStorey3_t2127335072::get_offset_of_U24this_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2372 = { sizeof (U3CReadU3Ec__AnonStorey2_t561251131), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2372[2] = 
{
	U3CReadU3Ec__AnonStorey2_t561251131::get_offset_of_key_0(),
	U3CReadU3Ec__AnonStorey2_t561251131::get_offset_of_U3CU3Ef__refU243_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2373 = { sizeof (AdvParamStruct_t1896386933), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2373[2] = 
{
	AdvParamStruct_t1896386933::get_offset_of_tbl_0(),
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2374 = { sizeof (AdvParamStructTbl_t994067203), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2374[2] = 
{
	AdvParamStructTbl_t994067203::get_offset_of_tbl_0(),
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2375 = { sizeof (U3CWriteU3Ec__AnonStorey1_t2299255853), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2375[1] = 
{
	U3CWriteU3Ec__AnonStorey1_t2299255853::get_offset_of_fileType_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2376 = { sizeof (U3CWriteU3Ec__AnonStorey0_t2299255854), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2376[2] = 
{
	U3CWriteU3Ec__AnonStorey0_t2299255854::get_offset_of_keyValue_0(),
	U3CWriteU3Ec__AnonStorey0_t2299255854::get_offset_of_U3CU3Ef__refU241_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2377 = { sizeof (U3CReadU3Ec__AnonStorey3_t881395170), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2377[2] = 
{
	U3CReadU3Ec__AnonStorey3_t881395170::get_offset_of_fileType_0(),
	U3CReadU3Ec__AnonStorey3_t881395170::get_offset_of_U24this_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2378 = { sizeof (U3CReadU3Ec__AnonStorey2_t3610278525), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2378[2] = 
{
	U3CReadU3Ec__AnonStorey2_t3610278525::get_offset_of_key_0(),
	U3CReadU3Ec__AnonStorey2_t3610278525::get_offset_of_U3CU3Ef__refU243_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2379 = { sizeof (AdvAnimationPlayer_t1530269518), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2379[9] = 
{
	0,
	AdvAnimationPlayer_t1530269518::get_offset_of_U3CAutoDestoryU3Ek__BackingField_3(),
	AdvAnimationPlayer_t1530269518::get_offset_of_U3CEnableSaveU3Ek__BackingField_4(),
	AdvAnimationPlayer_t1530269518::get_offset_of_U3CClipU3Ek__BackingField_5(),
	AdvAnimationPlayer_t1530269518::get_offset_of_U3CSpeedU3Ek__BackingField_6(),
	AdvAnimationPlayer_t1530269518::get_offset_of_onComplete_7(),
	AdvAnimationPlayer_t1530269518::get_offset_of_lecayAnimation_8(),
	AdvAnimationPlayer_t1530269518::get_offset_of_animator_9(),
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2380 = { sizeof (AdvEffectManager_t3829746105), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2380[3] = 
{
	AdvEffectManager_t3829746105::get_offset_of_engine_2(),
	AdvEffectManager_t3829746105::get_offset_of_messageWindow_3(),
	AdvEffectManager_t3829746105::get_offset_of_ruleTextureList_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2381 = { sizeof (TargetType_t2482913595)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2381[5] = 
{
	TargetType_t2482913595::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2382 = { sizeof (U3CFindRuleTextureU3Ec__AnonStorey0_t4067206823), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2382[1] = 
{
	U3CFindRuleTextureU3Ec__AnonStorey0_t4067206823::get_offset_of_name_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2383 = { sizeof (AdvITweenPlayer_t1991557872), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2383[9] = 
{
	AdvITweenPlayer_t1991557872::get_offset_of_data_2(),
	AdvITweenPlayer_t1991557872::get_offset_of_hashTbl_3(),
	AdvITweenPlayer_t1991557872::get_offset_of_callbackComplete_4(),
	AdvITweenPlayer_t1991557872::get_offset_of_isColorSprite_5(),
	AdvITweenPlayer_t1991557872::get_offset_of_count_6(),
	AdvITweenPlayer_t1991557872::get_offset_of_tweenName_7(),
	AdvITweenPlayer_t1991557872::get_offset_of_isPlaying_8(),
	AdvITweenPlayer_t1991557872::get_offset_of_target_9(),
	AdvITweenPlayer_t1991557872::get_offset_of_oldTweenPlayers_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2384 = { sizeof (AdvAgingTest_t4147764237), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2384[8] = 
{
	AdvAgingTest_t4147764237::get_offset_of_type_2(),
	AdvAgingTest_t4147764237::get_offset_of_disable_3(),
	AdvAgingTest_t4147764237::get_offset_of_skipFilter_4(),
	AdvAgingTest_t4147764237::get_offset_of_engine_5(),
	AdvAgingTest_t4147764237::get_offset_of_waitTime_6(),
	AdvAgingTest_t4147764237::get_offset_of_time_7(),
	AdvAgingTest_t4147764237::get_offset_of_clearOnEnd_8(),
	AdvAgingTest_t4147764237::get_offset_of_selectedDictionary_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2385 = { sizeof (Type_t2792715206)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2385[3] = 
{
	Type_t2792715206::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2386 = { sizeof (SkipFlags_t182225432)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2386[3] = 
{
	SkipFlags_t182225432::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2387 = { sizeof (AdvBackLogFilter_t120185244), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2387[3] = 
{
	AdvBackLogFilter_t120185244::get_offset_of_disable_2(),
	AdvBackLogFilter_t120185244::get_offset_of_filterMessageWindowNames_3(),
	AdvBackLogFilter_t120185244::get_offset_of_engine_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2388 = { sizeof (AdvCharacterGrayOutController_t895965523), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2388[8] = 
{
	AdvCharacterGrayOutController_t895965523::get_offset_of_engine_2(),
	AdvCharacterGrayOutController_t895965523::get_offset_of_mask_3(),
	AdvCharacterGrayOutController_t895965523::get_offset_of_mainColor_4(),
	AdvCharacterGrayOutController_t895965523::get_offset_of_subColor_5(),
	AdvCharacterGrayOutController_t895965523::get_offset_of_fadeTime_6(),
	AdvCharacterGrayOutController_t895965523::get_offset_of_noGrayoutCharacters_7(),
	AdvCharacterGrayOutController_t895965523::get_offset_of_isChanged_8(),
	AdvCharacterGrayOutController_t895965523::get_offset_of_pageBeginLayer_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2389 = { sizeof (LightingMask_t397726104)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2389[4] = 
{
	LightingMask_t397726104::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2390 = { sizeof (U3CIsLightingCharacterU3Ec__AnonStorey1_t623611741), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2390[1] = 
{
	U3CIsLightingCharacterU3Ec__AnonStorey1_t623611741::get_offset_of_layer_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2391 = { sizeof (U3CFadeColorU3Ec__Iterator0_t3211073950), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2391[8] = 
{
	U3CFadeColorU3Ec__Iterator0_t3211073950::get_offset_of_U3CelapsedU3E__0_0(),
	U3CFadeColorU3Ec__Iterator0_t3211073950::get_offset_of_from_1(),
	U3CFadeColorU3Ec__Iterator0_t3211073950::get_offset_of_to_2(),
	U3CFadeColorU3Ec__Iterator0_t3211073950::get_offset_of_effect_3(),
	U3CFadeColorU3Ec__Iterator0_t3211073950::get_offset_of_U24this_4(),
	U3CFadeColorU3Ec__Iterator0_t3211073950::get_offset_of_U24current_5(),
	U3CFadeColorU3Ec__Iterator0_t3211073950::get_offset_of_U24disposing_6(),
	U3CFadeColorU3Ec__Iterator0_t3211073950::get_offset_of_U24PC_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2392 = { sizeof (AdvLoadScene_t2523356965), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2393 = { sizeof (AdvSelectionTimeLimit_t2136628673), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2393[6] = 
{
	AdvSelectionTimeLimit_t2136628673::get_offset_of_disable_2(),
	AdvSelectionTimeLimit_t2136628673::get_offset_of_engine_3(),
	AdvSelectionTimeLimit_t2136628673::get_offset_of_selection_4(),
	AdvSelectionTimeLimit_t2136628673::get_offset_of_limitTime_5(),
	AdvSelectionTimeLimit_t2136628673::get_offset_of_timeLimitIndex_6(),
	AdvSelectionTimeLimit_t2136628673::get_offset_of_time_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2394 = { sizeof (AdvSelectionTimeLimitText_t1296437150), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2394[4] = 
{
	AdvSelectionTimeLimitText_t1296437150::get_offset_of_targetRoot_2(),
	AdvSelectionTimeLimitText_t1296437150::get_offset_of_text_3(),
	AdvSelectionTimeLimitText_t1296437150::get_offset_of_timeLimit_4(),
	AdvSelectionTimeLimitText_t1296437150::get_offset_of_engine_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2395 = { sizeof (AdvTextSound_t3558538083), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2395[9] = 
{
	AdvTextSound_t3558538083::get_offset_of_disable_2(),
	AdvTextSound_t3558538083::get_offset_of_engine_3(),
	AdvTextSound_t3558538083::get_offset_of_type_4(),
	AdvTextSound_t3558538083::get_offset_of_defaultSound_5(),
	AdvTextSound_t3558538083::get_offset_of_soundInfoList_6(),
	AdvTextSound_t3558538083::get_offset_of_intervalCount_7(),
	AdvTextSound_t3558538083::get_offset_of_intervalTime_8(),
	AdvTextSound_t3558538083::get_offset_of_lastTime_9(),
	AdvTextSound_t3558538083::get_offset_of_lastCharacterCount_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2396 = { sizeof (Type_t2823646136)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2396[3] = 
{
	Type_t2823646136::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2397 = { sizeof (SoundInfo_t3538551953), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2397[2] = 
{
	SoundInfo_t3538551953::get_offset_of_key_0(),
	SoundInfo_t3538551953::get_offset_of_sound_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2398 = { sizeof (U3CGetSeU3Ec__AnonStorey0_t3599616041), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2398[1] = 
{
	U3CGetSeU3Ec__AnonStorey0_t3599616041::get_offset_of_page_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2399 = { sizeof (AdvVideoLoadPathChanger_t4171547847), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2399[1] = 
{
	AdvVideoLoadPathChanger_t4171547847::get_offset_of_rootPath_2(),
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
