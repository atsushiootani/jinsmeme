void RegisterAllStrippedInternalCalls()
{
	//Start Registrations for type : UnityEngine.Analytics.CustomEventData

		//System.Boolean UnityEngine.Analytics.CustomEventData::AddBool(System.String,System.Boolean)
		void Register_UnityEngine_Analytics_CustomEventData_AddBool();
		Register_UnityEngine_Analytics_CustomEventData_AddBool();

		//System.Boolean UnityEngine.Analytics.CustomEventData::AddByte(System.String,System.Byte)
		void Register_UnityEngine_Analytics_CustomEventData_AddByte();
		Register_UnityEngine_Analytics_CustomEventData_AddByte();

		//System.Boolean UnityEngine.Analytics.CustomEventData::AddChar(System.String,System.Char)
		void Register_UnityEngine_Analytics_CustomEventData_AddChar();
		Register_UnityEngine_Analytics_CustomEventData_AddChar();

		//System.Boolean UnityEngine.Analytics.CustomEventData::AddDouble(System.String,System.Double)
		void Register_UnityEngine_Analytics_CustomEventData_AddDouble();
		Register_UnityEngine_Analytics_CustomEventData_AddDouble();

		//System.Boolean UnityEngine.Analytics.CustomEventData::AddInt16(System.String,System.Int16)
		void Register_UnityEngine_Analytics_CustomEventData_AddInt16();
		Register_UnityEngine_Analytics_CustomEventData_AddInt16();

		//System.Boolean UnityEngine.Analytics.CustomEventData::AddInt32(System.String,System.Int32)
		void Register_UnityEngine_Analytics_CustomEventData_AddInt32();
		Register_UnityEngine_Analytics_CustomEventData_AddInt32();

		//System.Boolean UnityEngine.Analytics.CustomEventData::AddInt64(System.String,System.Int64)
		void Register_UnityEngine_Analytics_CustomEventData_AddInt64();
		Register_UnityEngine_Analytics_CustomEventData_AddInt64();

		//System.Boolean UnityEngine.Analytics.CustomEventData::AddSByte(System.String,System.SByte)
		void Register_UnityEngine_Analytics_CustomEventData_AddSByte();
		Register_UnityEngine_Analytics_CustomEventData_AddSByte();

		//System.Boolean UnityEngine.Analytics.CustomEventData::AddString(System.String,System.String)
		void Register_UnityEngine_Analytics_CustomEventData_AddString();
		Register_UnityEngine_Analytics_CustomEventData_AddString();

		//System.Boolean UnityEngine.Analytics.CustomEventData::AddUInt16(System.String,System.UInt16)
		void Register_UnityEngine_Analytics_CustomEventData_AddUInt16();
		Register_UnityEngine_Analytics_CustomEventData_AddUInt16();

		//System.Boolean UnityEngine.Analytics.CustomEventData::AddUInt32(System.String,System.UInt32)
		void Register_UnityEngine_Analytics_CustomEventData_AddUInt32();
		Register_UnityEngine_Analytics_CustomEventData_AddUInt32();

		//System.Boolean UnityEngine.Analytics.CustomEventData::AddUInt64(System.String,System.UInt64)
		void Register_UnityEngine_Analytics_CustomEventData_AddUInt64();
		Register_UnityEngine_Analytics_CustomEventData_AddUInt64();

		//System.Void UnityEngine.Analytics.CustomEventData::InternalCreate(System.String)
		void Register_UnityEngine_Analytics_CustomEventData_InternalCreate();
		Register_UnityEngine_Analytics_CustomEventData_InternalCreate();

		//System.Void UnityEngine.Analytics.CustomEventData::InternalDestroy()
		void Register_UnityEngine_Analytics_CustomEventData_InternalDestroy();
		Register_UnityEngine_Analytics_CustomEventData_InternalDestroy();

	//End Registrations for type : UnityEngine.Analytics.CustomEventData

	//Start Registrations for type : UnityEngine.Analytics.UnityAnalyticsHandler

		//System.Void UnityEngine.Analytics.UnityAnalyticsHandler::InternalCreate()
		void Register_UnityEngine_Analytics_UnityAnalyticsHandler_InternalCreate();
		Register_UnityEngine_Analytics_UnityAnalyticsHandler_InternalCreate();

		//System.Void UnityEngine.Analytics.UnityAnalyticsHandler::InternalDestroy()
		void Register_UnityEngine_Analytics_UnityAnalyticsHandler_InternalDestroy();
		Register_UnityEngine_Analytics_UnityAnalyticsHandler_InternalDestroy();

		//UnityEngine.Analytics.AnalyticsResult UnityEngine.Analytics.UnityAnalyticsHandler::SendCustomEvent(UnityEngine.Analytics.CustomEventData)
		void Register_UnityEngine_Analytics_UnityAnalyticsHandler_SendCustomEvent();
		Register_UnityEngine_Analytics_UnityAnalyticsHandler_SendCustomEvent();

		//UnityEngine.Analytics.AnalyticsResult UnityEngine.Analytics.UnityAnalyticsHandler::SendCustomEventName(System.String)
		void Register_UnityEngine_Analytics_UnityAnalyticsHandler_SendCustomEventName();
		Register_UnityEngine_Analytics_UnityAnalyticsHandler_SendCustomEventName();

	//End Registrations for type : UnityEngine.Analytics.UnityAnalyticsHandler

	//Start Registrations for type : UnityEngine.Animation

		//System.Boolean UnityEngine.Animation::Play(System.String,UnityEngine.PlayMode)
		void Register_UnityEngine_Animation_Play();
		Register_UnityEngine_Animation_Play();

		//System.Boolean UnityEngine.Animation::get_isPlaying()
		void Register_UnityEngine_Animation_get_isPlaying();
		Register_UnityEngine_Animation_get_isPlaying();

		//System.Int32 UnityEngine.Animation::GetStateCount()
		void Register_UnityEngine_Animation_GetStateCount();
		Register_UnityEngine_Animation_GetStateCount();

		//System.Void UnityEngine.Animation::AddClip(UnityEngine.AnimationClip,System.String,System.Int32,System.Int32,System.Boolean)
		void Register_UnityEngine_Animation_AddClip();
		Register_UnityEngine_Animation_AddClip();

		//System.Void UnityEngine.Animation::CrossFade(System.String,System.Single,UnityEngine.PlayMode)
		void Register_UnityEngine_Animation_CrossFade();
		Register_UnityEngine_Animation_CrossFade();

		//System.Void UnityEngine.Animation::INTERNAL_CALL_Stop(UnityEngine.Animation)
		void Register_UnityEngine_Animation_INTERNAL_CALL_Stop();
		Register_UnityEngine_Animation_INTERNAL_CALL_Stop();

		//UnityEngine.AnimationState UnityEngine.Animation::GetState(System.String)
		void Register_UnityEngine_Animation_GetState();
		Register_UnityEngine_Animation_GetState();

		//UnityEngine.AnimationState UnityEngine.Animation::GetStateAtIndex(System.Int32)
		void Register_UnityEngine_Animation_GetStateAtIndex();
		Register_UnityEngine_Animation_GetStateAtIndex();

	//End Registrations for type : UnityEngine.Animation

	//Start Registrations for type : UnityEngine.AnimationClip

		//System.Boolean UnityEngine.AnimationClip::get_legacy()
		void Register_UnityEngine_AnimationClip_get_legacy();
		Register_UnityEngine_AnimationClip_get_legacy();

		//System.Void UnityEngine.AnimationClip::AddEventInternal(System.Object)
		void Register_UnityEngine_AnimationClip_AddEventInternal();
		Register_UnityEngine_AnimationClip_AddEventInternal();

		//System.Void UnityEngine.AnimationClip::Internal_CreateAnimationClip(UnityEngine.AnimationClip)
		void Register_UnityEngine_AnimationClip_Internal_CreateAnimationClip();
		Register_UnityEngine_AnimationClip_Internal_CreateAnimationClip();

		//System.Void UnityEngine.AnimationClip::SetCurve(System.String,System.Type,System.String,UnityEngine.AnimationCurve)
		void Register_UnityEngine_AnimationClip_SetCurve();
		Register_UnityEngine_AnimationClip_SetCurve();

		//System.Void UnityEngine.AnimationClip::set_frameRate(System.Single)
		void Register_UnityEngine_AnimationClip_set_frameRate();
		Register_UnityEngine_AnimationClip_set_frameRate();

		//System.Void UnityEngine.AnimationClip::set_legacy(System.Boolean)
		void Register_UnityEngine_AnimationClip_set_legacy();
		Register_UnityEngine_AnimationClip_set_legacy();

		//System.Void UnityEngine.AnimationClip::set_wrapMode(UnityEngine.WrapMode)
		void Register_UnityEngine_AnimationClip_set_wrapMode();
		Register_UnityEngine_AnimationClip_set_wrapMode();

	//End Registrations for type : UnityEngine.AnimationClip

	//Start Registrations for type : UnityEngine.AnimationCurve

		//System.Int32 UnityEngine.AnimationCurve::INTERNAL_CALL_AddKey_Internal(UnityEngine.AnimationCurve,UnityEngine.Keyframe&)
		void Register_UnityEngine_AnimationCurve_INTERNAL_CALL_AddKey_Internal();
		Register_UnityEngine_AnimationCurve_INTERNAL_CALL_AddKey_Internal();

		//System.Int32 UnityEngine.AnimationCurve::get_length()
		void Register_UnityEngine_AnimationCurve_get_length();
		Register_UnityEngine_AnimationCurve_get_length();

		//System.Single UnityEngine.AnimationCurve::Evaluate(System.Single)
		void Register_UnityEngine_AnimationCurve_Evaluate();
		Register_UnityEngine_AnimationCurve_Evaluate();

		//System.Void UnityEngine.AnimationCurve::Cleanup()
		void Register_UnityEngine_AnimationCurve_Cleanup();
		Register_UnityEngine_AnimationCurve_Cleanup();

		//System.Void UnityEngine.AnimationCurve::Init(UnityEngine.Keyframe[])
		void Register_UnityEngine_AnimationCurve_Init();
		Register_UnityEngine_AnimationCurve_Init();

		//UnityEngine.Keyframe[] UnityEngine.AnimationCurve::GetKeys()
		void Register_UnityEngine_AnimationCurve_GetKeys();
		Register_UnityEngine_AnimationCurve_GetKeys();

	//End Registrations for type : UnityEngine.AnimationCurve

	//Start Registrations for type : UnityEngine.AnimationState

		//System.Single UnityEngine.AnimationState::get_time()
		void Register_UnityEngine_AnimationState_get_time();
		Register_UnityEngine_AnimationState_get_time();

		//System.Void UnityEngine.AnimationState::set_speed(System.Single)
		void Register_UnityEngine_AnimationState_set_speed();
		Register_UnityEngine_AnimationState_set_speed();

		//System.Void UnityEngine.AnimationState::set_time(System.Single)
		void Register_UnityEngine_AnimationState_set_time();
		Register_UnityEngine_AnimationState_set_time();

	//End Registrations for type : UnityEngine.AnimationState

	//Start Registrations for type : UnityEngine.Animator

		//System.Boolean UnityEngine.Animator::CheckIfInIKPassInternal()
		void Register_UnityEngine_Animator_CheckIfInIKPassInternal();
		Register_UnityEngine_Animator_CheckIfInIKPassInternal();

		//System.Boolean UnityEngine.Animator::GetBoolString(System.String)
		void Register_UnityEngine_Animator_GetBoolString();
		Register_UnityEngine_Animator_GetBoolString();

		//System.Boolean UnityEngine.Animator::IsInTransition(System.Int32)
		void Register_UnityEngine_Animator_IsInTransition();
		Register_UnityEngine_Animator_IsInTransition();

		//System.Boolean UnityEngine.Animator::get_hasBoundPlayables()
		void Register_UnityEngine_Animator_get_hasBoundPlayables();
		Register_UnityEngine_Animator_get_hasBoundPlayables();

		//System.Boolean UnityEngine.Animator::get_logWarnings()
		void Register_UnityEngine_Animator_get_logWarnings();
		Register_UnityEngine_Animator_get_logWarnings();

		//System.Int32 UnityEngine.Animator::StringToHash(System.String)
		void Register_UnityEngine_Animator_StringToHash();
		Register_UnityEngine_Animator_StringToHash();

		//System.Int32 UnityEngine.Animator::get_layerCount()
		void Register_UnityEngine_Animator_get_layerCount();
		Register_UnityEngine_Animator_get_layerCount();

		//System.Single UnityEngine.Animator::GetFloatString(System.String)
		void Register_UnityEngine_Animator_GetFloatString();
		Register_UnityEngine_Animator_GetFloatString();

		//System.Void UnityEngine.Animator::CrossFade(System.Int32,System.Single,System.Int32,System.Single)
		void Register_UnityEngine_Animator_CrossFade();
		Register_UnityEngine_Animator_CrossFade();

		//System.Void UnityEngine.Animator::INTERNAL_CALL_SetIKPositionInternal(UnityEngine.Animator,UnityEngine.AvatarIKGoal,UnityEngine.Vector3&)
		void Register_UnityEngine_Animator_INTERNAL_CALL_SetIKPositionInternal();
		Register_UnityEngine_Animator_INTERNAL_CALL_SetIKPositionInternal();

		//System.Void UnityEngine.Animator::INTERNAL_CALL_SetIKRotationInternal(UnityEngine.Animator,UnityEngine.AvatarIKGoal,UnityEngine.Quaternion&)
		void Register_UnityEngine_Animator_INTERNAL_CALL_SetIKRotationInternal();
		Register_UnityEngine_Animator_INTERNAL_CALL_SetIKRotationInternal();

		//System.Void UnityEngine.Animator::INTERNAL_CALL_SetLookAtPositionInternal(UnityEngine.Animator,UnityEngine.Vector3&)
		void Register_UnityEngine_Animator_INTERNAL_CALL_SetLookAtPositionInternal();
		Register_UnityEngine_Animator_INTERNAL_CALL_SetLookAtPositionInternal();

		//System.Void UnityEngine.Animator::Play(System.Int32,System.Int32,System.Single)
		void Register_UnityEngine_Animator_Play();
		Register_UnityEngine_Animator_Play();

		//System.Void UnityEngine.Animator::ResetTriggerString(System.String)
		void Register_UnityEngine_Animator_ResetTriggerString();
		Register_UnityEngine_Animator_ResetTriggerString();

		//System.Void UnityEngine.Animator::SetBoolString(System.String,System.Boolean)
		void Register_UnityEngine_Animator_SetBoolString();
		Register_UnityEngine_Animator_SetBoolString();

		//System.Void UnityEngine.Animator::SetFloatString(System.String,System.Single)
		void Register_UnityEngine_Animator_SetFloatString();
		Register_UnityEngine_Animator_SetFloatString();

		//System.Void UnityEngine.Animator::SetIKPositionWeightInternal(UnityEngine.AvatarIKGoal,System.Single)
		void Register_UnityEngine_Animator_SetIKPositionWeightInternal();
		Register_UnityEngine_Animator_SetIKPositionWeightInternal();

		//System.Void UnityEngine.Animator::SetIKRotationWeightInternal(UnityEngine.AvatarIKGoal,System.Single)
		void Register_UnityEngine_Animator_SetIKRotationWeightInternal();
		Register_UnityEngine_Animator_SetIKRotationWeightInternal();

		//System.Void UnityEngine.Animator::SetLayerWeight(System.Int32,System.Single)
		void Register_UnityEngine_Animator_SetLayerWeight();
		Register_UnityEngine_Animator_SetLayerWeight();

		//System.Void UnityEngine.Animator::SetLookAtWeightInternal(System.Single,System.Single,System.Single,System.Single,System.Single)
		void Register_UnityEngine_Animator_SetLookAtWeightInternal();
		Register_UnityEngine_Animator_SetLookAtWeightInternal();

		//System.Void UnityEngine.Animator::SetTriggerString(System.String)
		void Register_UnityEngine_Animator_SetTriggerString();
		Register_UnityEngine_Animator_SetTriggerString();

		//System.Void UnityEngine.Animator::set_speed(System.Single)
		void Register_UnityEngine_Animator_set_speed();
		Register_UnityEngine_Animator_set_speed();

		//UnityEngine.AnimatorStateInfo UnityEngine.Animator::GetCurrentAnimatorStateInfo(System.Int32)
		void Register_UnityEngine_Animator_GetCurrentAnimatorStateInfo();
		Register_UnityEngine_Animator_GetCurrentAnimatorStateInfo();

		//UnityEngine.AnimatorStateInfo UnityEngine.Animator::GetNextAnimatorStateInfo(System.Int32)
		void Register_UnityEngine_Animator_GetNextAnimatorStateInfo();
		Register_UnityEngine_Animator_GetNextAnimatorStateInfo();

	//End Registrations for type : UnityEngine.Animator

	//Start Registrations for type : UnityEngine.Application

		//System.Boolean UnityEngine.Application::get_isEditor()
		void Register_UnityEngine_Application_get_isEditor();
		Register_UnityEngine_Application_get_isEditor();

		//System.Boolean UnityEngine.Application::get_isPlaying()
		void Register_UnityEngine_Application_get_isPlaying();
		Register_UnityEngine_Application_get_isPlaying();

		//System.String UnityEngine.Application::get_persistentDataPath()
		void Register_UnityEngine_Application_get_persistentDataPath();
		Register_UnityEngine_Application_get_persistentDataPath();

		//System.String UnityEngine.Application::get_streamingAssetsPath()
		void Register_UnityEngine_Application_get_streamingAssetsPath();
		Register_UnityEngine_Application_get_streamingAssetsPath();

		//System.String UnityEngine.Application::get_temporaryCachePath()
		void Register_UnityEngine_Application_get_temporaryCachePath();
		Register_UnityEngine_Application_get_temporaryCachePath();

		//System.String UnityEngine.Application::get_unityVersion()
		void Register_UnityEngine_Application_get_unityVersion();
		Register_UnityEngine_Application_get_unityVersion();

		//System.String UnityEngine.Application::get_version()
		void Register_UnityEngine_Application_get_version();
		Register_UnityEngine_Application_get_version();

		//System.Void UnityEngine.Application::OpenURL(System.String)
		void Register_UnityEngine_Application_OpenURL();
		Register_UnityEngine_Application_OpenURL();

		//System.Void UnityEngine.Application::Quit()
		void Register_UnityEngine_Application_Quit();
		Register_UnityEngine_Application_Quit();

		//UnityEngine.NetworkReachability UnityEngine.Application::get_internetReachability()
		void Register_UnityEngine_Application_get_internetReachability();
		Register_UnityEngine_Application_get_internetReachability();

		//UnityEngine.RuntimePlatform UnityEngine.Application::get_platform()
		void Register_UnityEngine_Application_get_platform();
		Register_UnityEngine_Application_get_platform();

		//UnityEngine.SystemLanguage UnityEngine.Application::get_systemLanguage()
		void Register_UnityEngine_Application_get_systemLanguage();
		Register_UnityEngine_Application_get_systemLanguage();

	//End Registrations for type : UnityEngine.Application

	//Start Registrations for type : UnityEngine.AssetBundle

		//System.Void UnityEngine.AssetBundle::Unload(System.Boolean)
		void Register_UnityEngine_AssetBundle_Unload();
		Register_UnityEngine_AssetBundle_Unload();

		//UnityEngine.AssetBundle UnityEngine.AssetBundle::LoadFromMemory(System.Byte[],System.UInt32)
		void Register_UnityEngine_AssetBundle_LoadFromMemory();
		Register_UnityEngine_AssetBundle_LoadFromMemory();

		//UnityEngine.AssetBundleRequest UnityEngine.AssetBundle::LoadAssetAsync_Internal(System.String,System.Type)
		void Register_UnityEngine_AssetBundle_LoadAssetAsync_Internal();
		Register_UnityEngine_AssetBundle_LoadAssetAsync_Internal();

		//UnityEngine.AssetBundleRequest UnityEngine.AssetBundle::LoadAssetWithSubAssetsAsync_Internal(System.String,System.Type)
		void Register_UnityEngine_AssetBundle_LoadAssetWithSubAssetsAsync_Internal();
		Register_UnityEngine_AssetBundle_LoadAssetWithSubAssetsAsync_Internal();

		//UnityEngine.Object UnityEngine.AssetBundle::get_mainAsset()
		void Register_UnityEngine_AssetBundle_get_mainAsset();
		Register_UnityEngine_AssetBundle_get_mainAsset();

	//End Registrations for type : UnityEngine.AssetBundle

	//Start Registrations for type : UnityEngine.AssetBundleManifest

		//System.String[] UnityEngine.AssetBundleManifest::GetAllAssetBundles()
		void Register_UnityEngine_AssetBundleManifest_GetAllAssetBundles();
		Register_UnityEngine_AssetBundleManifest_GetAllAssetBundles();

		//System.Void UnityEngine.AssetBundleManifest::INTERNAL_CALL_GetAssetBundleHash(UnityEngine.AssetBundleManifest,System.String,UnityEngine.Hash128&)
		void Register_UnityEngine_AssetBundleManifest_INTERNAL_CALL_GetAssetBundleHash();
		Register_UnityEngine_AssetBundleManifest_INTERNAL_CALL_GetAssetBundleHash();

	//End Registrations for type : UnityEngine.AssetBundleManifest

	//Start Registrations for type : UnityEngine.AssetBundleRequest

		//UnityEngine.Object UnityEngine.AssetBundleRequest::get_asset()
		void Register_UnityEngine_AssetBundleRequest_get_asset();
		Register_UnityEngine_AssetBundleRequest_get_asset();

		//UnityEngine.Object[] UnityEngine.AssetBundleRequest::get_allAssets()
		void Register_UnityEngine_AssetBundleRequest_get_allAssets();
		Register_UnityEngine_AssetBundleRequest_get_allAssets();

	//End Registrations for type : UnityEngine.AssetBundleRequest

	//Start Registrations for type : UnityEngine.AsyncOperation

		//System.Boolean UnityEngine.AsyncOperation::get_isDone()
		void Register_UnityEngine_AsyncOperation_get_isDone();
		Register_UnityEngine_AsyncOperation_get_isDone();

		//System.Void UnityEngine.AsyncOperation::InternalDestroy()
		void Register_UnityEngine_AsyncOperation_InternalDestroy();
		Register_UnityEngine_AsyncOperation_InternalDestroy();

	//End Registrations for type : UnityEngine.AsyncOperation

	//Start Registrations for type : UnityEngine.AudioClip

		//System.Boolean UnityEngine.AudioClip::GetData(System.Single[],System.Int32)
		void Register_UnityEngine_AudioClip_GetData();
		Register_UnityEngine_AudioClip_GetData();

		//System.Boolean UnityEngine.AudioClip::SetData(System.Single[],System.Int32)
		void Register_UnityEngine_AudioClip_SetData();
		Register_UnityEngine_AudioClip_SetData();

		//System.Boolean UnityEngine.AudioClip::get_ambisonic()
		void Register_UnityEngine_AudioClip_get_ambisonic();
		Register_UnityEngine_AudioClip_get_ambisonic();

		//System.Int32 UnityEngine.AudioClip::get_channels()
		void Register_UnityEngine_AudioClip_get_channels();
		Register_UnityEngine_AudioClip_get_channels();

		//System.Int32 UnityEngine.AudioClip::get_frequency()
		void Register_UnityEngine_AudioClip_get_frequency();
		Register_UnityEngine_AudioClip_get_frequency();

		//System.Int32 UnityEngine.AudioClip::get_samples()
		void Register_UnityEngine_AudioClip_get_samples();
		Register_UnityEngine_AudioClip_get_samples();

		//System.Single UnityEngine.AudioClip::get_length()
		void Register_UnityEngine_AudioClip_get_length();
		Register_UnityEngine_AudioClip_get_length();

		//System.Void UnityEngine.AudioClip::Init_Internal(System.String,System.Int32,System.Int32,System.Int32,System.Boolean)
		void Register_UnityEngine_AudioClip_Init_Internal();
		Register_UnityEngine_AudioClip_Init_Internal();

		//UnityEngine.AudioClip UnityEngine.AudioClip::Construct_Internal()
		void Register_UnityEngine_AudioClip_Construct_Internal();
		Register_UnityEngine_AudioClip_Construct_Internal();

		//UnityEngine.AudioDataLoadState UnityEngine.AudioClip::get_loadState()
		void Register_UnityEngine_AudioClip_get_loadState();
		Register_UnityEngine_AudioClip_get_loadState();

	//End Registrations for type : UnityEngine.AudioClip

	//Start Registrations for type : UnityEngine.AudioExtensionManager

		//UnityEngine.Object UnityEngine.AudioExtensionManager::GetAudioListener()
		void Register_UnityEngine_AudioExtensionManager_GetAudioListener();
		Register_UnityEngine_AudioExtensionManager_GetAudioListener();

	//End Registrations for type : UnityEngine.AudioExtensionManager

	//Start Registrations for type : UnityEngine.AudioListener

		//System.Int32 UnityEngine.AudioListener::GetNumExtensionProperties()
		void Register_UnityEngine_AudioListener_GetNumExtensionProperties();
		Register_UnityEngine_AudioListener_GetNumExtensionProperties();

		//System.Single UnityEngine.AudioListener::ReadExtensionPropertyValue(System.Int32)
		void Register_UnityEngine_AudioListener_ReadExtensionPropertyValue();
		Register_UnityEngine_AudioListener_ReadExtensionPropertyValue();

		//System.Void UnityEngine.AudioListener::INTERNAL_CALL_ClearExtensionProperties(UnityEngine.AudioListener,UnityEngine.PropertyName&)
		void Register_UnityEngine_AudioListener_INTERNAL_CALL_ClearExtensionProperties();
		Register_UnityEngine_AudioListener_INTERNAL_CALL_ClearExtensionProperties();

		//System.Void UnityEngine.AudioListener::INTERNAL_CALL_ReadExtensionName(UnityEngine.AudioListener,System.Int32,UnityEngine.PropertyName&)
		void Register_UnityEngine_AudioListener_INTERNAL_CALL_ReadExtensionName();
		Register_UnityEngine_AudioListener_INTERNAL_CALL_ReadExtensionName();

		//System.Void UnityEngine.AudioListener::INTERNAL_CALL_ReadExtensionPropertyName(UnityEngine.AudioListener,System.Int32,UnityEngine.PropertyName&)
		void Register_UnityEngine_AudioListener_INTERNAL_CALL_ReadExtensionPropertyName();
		Register_UnityEngine_AudioListener_INTERNAL_CALL_ReadExtensionPropertyName();

	//End Registrations for type : UnityEngine.AudioListener

	//Start Registrations for type : UnityEngine.AudioSettings

		//System.Double UnityEngine.AudioSettings::get_dspTime()
		void Register_UnityEngine_AudioSettings_get_dspTime();
		Register_UnityEngine_AudioSettings_get_dspTime();

		//System.String UnityEngine.AudioSettings::GetAmbisonicDecoderPluginName()
		void Register_UnityEngine_AudioSettings_GetAmbisonicDecoderPluginName();
		Register_UnityEngine_AudioSettings_GetAmbisonicDecoderPluginName();

		//System.String UnityEngine.AudioSettings::GetSpatializerPluginName()
		void Register_UnityEngine_AudioSettings_GetSpatializerPluginName();
		Register_UnityEngine_AudioSettings_GetSpatializerPluginName();

	//End Registrations for type : UnityEngine.AudioSettings

	//Start Registrations for type : UnityEngine.AudioSource

		//System.Boolean UnityEngine.AudioSource::get_isPlaying()
		void Register_UnityEngine_AudioSource_get_isPlaying();
		Register_UnityEngine_AudioSource_get_isPlaying();

		//System.Boolean UnityEngine.AudioSource::get_spatializeInternal()
		void Register_UnityEngine_AudioSource_get_spatializeInternal();
		Register_UnityEngine_AudioSource_get_spatializeInternal();

		//System.Int32 UnityEngine.AudioSource::GetNumExtensionProperties()
		void Register_UnityEngine_AudioSource_GetNumExtensionProperties();
		Register_UnityEngine_AudioSource_GetNumExtensionProperties();

		//System.Single UnityEngine.AudioSource::ReadExtensionPropertyValue(System.Int32)
		void Register_UnityEngine_AudioSource_ReadExtensionPropertyValue();
		Register_UnityEngine_AudioSource_ReadExtensionPropertyValue();

		//System.Single UnityEngine.AudioSource::get_pitch()
		void Register_UnityEngine_AudioSource_get_pitch();
		Register_UnityEngine_AudioSource_get_pitch();

		//System.Single UnityEngine.AudioSource::get_time()
		void Register_UnityEngine_AudioSource_get_time();
		Register_UnityEngine_AudioSource_get_time();

		//System.Single UnityEngine.AudioSource::get_volume()
		void Register_UnityEngine_AudioSource_get_volume();
		Register_UnityEngine_AudioSource_get_volume();

		//System.Void UnityEngine.AudioSource::GetOutputDataHelper(System.Single[],System.Int32)
		void Register_UnityEngine_AudioSource_GetOutputDataHelper();
		Register_UnityEngine_AudioSource_GetOutputDataHelper();

		//System.Void UnityEngine.AudioSource::INTERNAL_CALL_ClearExtensionProperties(UnityEngine.AudioSource,UnityEngine.PropertyName&)
		void Register_UnityEngine_AudioSource_INTERNAL_CALL_ClearExtensionProperties();
		Register_UnityEngine_AudioSource_INTERNAL_CALL_ClearExtensionProperties();

		//System.Void UnityEngine.AudioSource::INTERNAL_CALL_ReadExtensionName(UnityEngine.AudioSource,System.Int32,UnityEngine.PropertyName&)
		void Register_UnityEngine_AudioSource_INTERNAL_CALL_ReadExtensionName();
		Register_UnityEngine_AudioSource_INTERNAL_CALL_ReadExtensionName();

		//System.Void UnityEngine.AudioSource::INTERNAL_CALL_ReadExtensionPropertyName(UnityEngine.AudioSource,System.Int32,UnityEngine.PropertyName&)
		void Register_UnityEngine_AudioSource_INTERNAL_CALL_ReadExtensionPropertyName();
		Register_UnityEngine_AudioSource_INTERNAL_CALL_ReadExtensionPropertyName();

		//System.Void UnityEngine.AudioSource::Play(System.UInt64)
		void Register_UnityEngine_AudioSource_Play();
		Register_UnityEngine_AudioSource_Play();

		//System.Void UnityEngine.AudioSource::PlayOneShotHelper(UnityEngine.AudioClip,System.Single)
		void Register_UnityEngine_AudioSource_PlayOneShotHelper();
		Register_UnityEngine_AudioSource_PlayOneShotHelper();

		//System.Void UnityEngine.AudioSource::PlayScheduled(System.Double)
		void Register_UnityEngine_AudioSource_PlayScheduled();
		Register_UnityEngine_AudioSource_PlayScheduled();

		//System.Void UnityEngine.AudioSource::Stop()
		void Register_UnityEngine_AudioSource_Stop();
		Register_UnityEngine_AudioSource_Stop();

		//System.Void UnityEngine.AudioSource::set_clip(UnityEngine.AudioClip)
		void Register_UnityEngine_AudioSource_set_clip();
		Register_UnityEngine_AudioSource_set_clip();

		//System.Void UnityEngine.AudioSource::set_loop(System.Boolean)
		void Register_UnityEngine_AudioSource_set_loop();
		Register_UnityEngine_AudioSource_set_loop();

		//System.Void UnityEngine.AudioSource::set_pitch(System.Single)
		void Register_UnityEngine_AudioSource_set_pitch();
		Register_UnityEngine_AudioSource_set_pitch();

		//System.Void UnityEngine.AudioSource::set_playOnAwake(System.Boolean)
		void Register_UnityEngine_AudioSource_set_playOnAwake();
		Register_UnityEngine_AudioSource_set_playOnAwake();

		//System.Void UnityEngine.AudioSource::set_spatialBlend(System.Single)
		void Register_UnityEngine_AudioSource_set_spatialBlend();
		Register_UnityEngine_AudioSource_set_spatialBlend();

		//System.Void UnityEngine.AudioSource::set_time(System.Single)
		void Register_UnityEngine_AudioSource_set_time();
		Register_UnityEngine_AudioSource_set_time();

		//System.Void UnityEngine.AudioSource::set_volume(System.Single)
		void Register_UnityEngine_AudioSource_set_volume();
		Register_UnityEngine_AudioSource_set_volume();

		//UnityEngine.AudioClip UnityEngine.AudioSource::get_clip()
		void Register_UnityEngine_AudioSource_get_clip();
		Register_UnityEngine_AudioSource_get_clip();

	//End Registrations for type : UnityEngine.AudioSource

	//Start Registrations for type : UnityEngine.Behaviour

		//System.Boolean UnityEngine.Behaviour::get_enabled()
		void Register_UnityEngine_Behaviour_get_enabled();
		Register_UnityEngine_Behaviour_get_enabled();

		//System.Boolean UnityEngine.Behaviour::get_isActiveAndEnabled()
		void Register_UnityEngine_Behaviour_get_isActiveAndEnabled();
		Register_UnityEngine_Behaviour_get_isActiveAndEnabled();

		//System.Void UnityEngine.Behaviour::set_enabled(System.Boolean)
		void Register_UnityEngine_Behaviour_set_enabled();
		Register_UnityEngine_Behaviour_set_enabled();

	//End Registrations for type : UnityEngine.Behaviour

	//Start Registrations for type : UnityEngine.Bounds

		//System.Boolean UnityEngine.Bounds::INTERNAL_CALL_Internal_Contains(UnityEngine.Bounds&,UnityEngine.Vector3&)
		void Register_UnityEngine_Bounds_INTERNAL_CALL_Internal_Contains();
		Register_UnityEngine_Bounds_INTERNAL_CALL_Internal_Contains();

	//End Registrations for type : UnityEngine.Bounds

	//Start Registrations for type : UnityEngine.Caching

		//System.Boolean UnityEngine.Caching::ClearCache()
		void Register_UnityEngine_Caching_ClearCache();
		Register_UnityEngine_Caching_ClearCache();

		//System.Boolean UnityEngine.Caching::INTERNAL_CALL_IsVersionCached(System.String,System.String,UnityEngine.Hash128&)
		void Register_UnityEngine_Caching_INTERNAL_CALL_IsVersionCached();
		Register_UnityEngine_Caching_INTERNAL_CALL_IsVersionCached();

		//System.Boolean UnityEngine.Caching::get_ready()
		void Register_UnityEngine_Caching_get_ready();
		Register_UnityEngine_Caching_get_ready();

	//End Registrations for type : UnityEngine.Caching

	//Start Registrations for type : UnityEngine.Camera

		//System.Boolean UnityEngine.Camera::get_orthographic()
		void Register_UnityEngine_Camera_get_orthographic();
		Register_UnityEngine_Camera_get_orthographic();

		//System.Int32 UnityEngine.Camera::GetAllCameras(UnityEngine.Camera[])
		void Register_UnityEngine_Camera_GetAllCameras();
		Register_UnityEngine_Camera_GetAllCameras();

		//System.Int32 UnityEngine.Camera::get_allCamerasCount()
		void Register_UnityEngine_Camera_get_allCamerasCount();
		Register_UnityEngine_Camera_get_allCamerasCount();

		//System.Int32 UnityEngine.Camera::get_cullingMask()
		void Register_UnityEngine_Camera_get_cullingMask();
		Register_UnityEngine_Camera_get_cullingMask();

		//System.Int32 UnityEngine.Camera::get_eventMask()
		void Register_UnityEngine_Camera_get_eventMask();
		Register_UnityEngine_Camera_get_eventMask();

		//System.Int32 UnityEngine.Camera::get_targetDisplay()
		void Register_UnityEngine_Camera_get_targetDisplay();
		Register_UnityEngine_Camera_get_targetDisplay();

		//System.Single UnityEngine.Camera::get_depth()
		void Register_UnityEngine_Camera_get_depth();
		Register_UnityEngine_Camera_get_depth();

		//System.Single UnityEngine.Camera::get_farClipPlane()
		void Register_UnityEngine_Camera_get_farClipPlane();
		Register_UnityEngine_Camera_get_farClipPlane();

		//System.Single UnityEngine.Camera::get_nearClipPlane()
		void Register_UnityEngine_Camera_get_nearClipPlane();
		Register_UnityEngine_Camera_get_nearClipPlane();

		//System.Void UnityEngine.Camera::INTERNAL_CALL_ScreenPointToRay(UnityEngine.Camera,UnityEngine.Vector3&,UnityEngine.Ray&)
		void Register_UnityEngine_Camera_INTERNAL_CALL_ScreenPointToRay();
		Register_UnityEngine_Camera_INTERNAL_CALL_ScreenPointToRay();

		//System.Void UnityEngine.Camera::INTERNAL_CALL_ScreenToViewportPoint(UnityEngine.Camera,UnityEngine.Vector3&,UnityEngine.Vector3&)
		void Register_UnityEngine_Camera_INTERNAL_CALL_ScreenToViewportPoint();
		Register_UnityEngine_Camera_INTERNAL_CALL_ScreenToViewportPoint();

		//System.Void UnityEngine.Camera::INTERNAL_CALL_ScreenToWorldPoint(UnityEngine.Camera,UnityEngine.Vector3&,UnityEngine.Vector3&)
		void Register_UnityEngine_Camera_INTERNAL_CALL_ScreenToWorldPoint();
		Register_UnityEngine_Camera_INTERNAL_CALL_ScreenToWorldPoint();

		//System.Void UnityEngine.Camera::INTERNAL_get_backgroundColor(UnityEngine.Color&)
		void Register_UnityEngine_Camera_INTERNAL_get_backgroundColor();
		Register_UnityEngine_Camera_INTERNAL_get_backgroundColor();

		//System.Void UnityEngine.Camera::INTERNAL_get_pixelRect(UnityEngine.Rect&)
		void Register_UnityEngine_Camera_INTERNAL_get_pixelRect();
		Register_UnityEngine_Camera_INTERNAL_get_pixelRect();

		//System.Void UnityEngine.Camera::INTERNAL_get_rect(UnityEngine.Rect&)
		void Register_UnityEngine_Camera_INTERNAL_get_rect();
		Register_UnityEngine_Camera_INTERNAL_get_rect();

		//System.Void UnityEngine.Camera::INTERNAL_set_backgroundColor(UnityEngine.Color&)
		void Register_UnityEngine_Camera_INTERNAL_set_backgroundColor();
		Register_UnityEngine_Camera_INTERNAL_set_backgroundColor();

		//System.Void UnityEngine.Camera::INTERNAL_set_rect(UnityEngine.Rect&)
		void Register_UnityEngine_Camera_INTERNAL_set_rect();
		Register_UnityEngine_Camera_INTERNAL_set_rect();

		//System.Void UnityEngine.Camera::set_clearFlags(UnityEngine.CameraClearFlags)
		void Register_UnityEngine_Camera_set_clearFlags();
		Register_UnityEngine_Camera_set_clearFlags();

		//System.Void UnityEngine.Camera::set_cullingMask(System.Int32)
		void Register_UnityEngine_Camera_set_cullingMask();
		Register_UnityEngine_Camera_set_cullingMask();

		//System.Void UnityEngine.Camera::set_depth(System.Single)
		void Register_UnityEngine_Camera_set_depth();
		Register_UnityEngine_Camera_set_depth();

		//System.Void UnityEngine.Camera::set_depthTextureMode(UnityEngine.DepthTextureMode)
		void Register_UnityEngine_Camera_set_depthTextureMode();
		Register_UnityEngine_Camera_set_depthTextureMode();

		//System.Void UnityEngine.Camera::set_nearClipPlane(System.Single)
		void Register_UnityEngine_Camera_set_nearClipPlane();
		Register_UnityEngine_Camera_set_nearClipPlane();

		//System.Void UnityEngine.Camera::set_orthographic(System.Boolean)
		void Register_UnityEngine_Camera_set_orthographic();
		Register_UnityEngine_Camera_set_orthographic();

		//System.Void UnityEngine.Camera::set_orthographicSize(System.Single)
		void Register_UnityEngine_Camera_set_orthographicSize();
		Register_UnityEngine_Camera_set_orthographicSize();

		//System.Void UnityEngine.Camera::set_targetTexture(UnityEngine.RenderTexture)
		void Register_UnityEngine_Camera_set_targetTexture();
		Register_UnityEngine_Camera_set_targetTexture();

		//UnityEngine.Camera UnityEngine.Camera::get_main()
		void Register_UnityEngine_Camera_get_main();
		Register_UnityEngine_Camera_get_main();

		//UnityEngine.CameraClearFlags UnityEngine.Camera::get_clearFlags()
		void Register_UnityEngine_Camera_get_clearFlags();
		Register_UnityEngine_Camera_get_clearFlags();

		//UnityEngine.DepthTextureMode UnityEngine.Camera::get_depthTextureMode()
		void Register_UnityEngine_Camera_get_depthTextureMode();
		Register_UnityEngine_Camera_get_depthTextureMode();

		//UnityEngine.GameObject UnityEngine.Camera::INTERNAL_CALL_RaycastTry(UnityEngine.Camera,UnityEngine.Ray&,System.Single,System.Int32)
		void Register_UnityEngine_Camera_INTERNAL_CALL_RaycastTry();
		Register_UnityEngine_Camera_INTERNAL_CALL_RaycastTry();

		//UnityEngine.GameObject UnityEngine.Camera::INTERNAL_CALL_RaycastTry2D(UnityEngine.Camera,UnityEngine.Ray&,System.Single,System.Int32)
		void Register_UnityEngine_Camera_INTERNAL_CALL_RaycastTry2D();
		Register_UnityEngine_Camera_INTERNAL_CALL_RaycastTry2D();

		//UnityEngine.RenderTexture UnityEngine.Camera::get_targetTexture()
		void Register_UnityEngine_Camera_get_targetTexture();
		Register_UnityEngine_Camera_get_targetTexture();

	//End Registrations for type : UnityEngine.Camera

	//Start Registrations for type : UnityEngine.Canvas

		//System.Boolean UnityEngine.Canvas::get_isRootCanvas()
		void Register_UnityEngine_Canvas_get_isRootCanvas();
		Register_UnityEngine_Canvas_get_isRootCanvas();

		//System.Boolean UnityEngine.Canvas::get_overrideSorting()
		void Register_UnityEngine_Canvas_get_overrideSorting();
		Register_UnityEngine_Canvas_get_overrideSorting();

		//System.Boolean UnityEngine.Canvas::get_pixelPerfect()
		void Register_UnityEngine_Canvas_get_pixelPerfect();
		Register_UnityEngine_Canvas_get_pixelPerfect();

		//System.Int32 UnityEngine.Canvas::get_renderOrder()
		void Register_UnityEngine_Canvas_get_renderOrder();
		Register_UnityEngine_Canvas_get_renderOrder();

		//System.Int32 UnityEngine.Canvas::get_sortingLayerID()
		void Register_UnityEngine_Canvas_get_sortingLayerID();
		Register_UnityEngine_Canvas_get_sortingLayerID();

		//System.Int32 UnityEngine.Canvas::get_sortingOrder()
		void Register_UnityEngine_Canvas_get_sortingOrder();
		Register_UnityEngine_Canvas_get_sortingOrder();

		//System.Int32 UnityEngine.Canvas::get_targetDisplay()
		void Register_UnityEngine_Canvas_get_targetDisplay();
		Register_UnityEngine_Canvas_get_targetDisplay();

		//System.Single UnityEngine.Canvas::get_referencePixelsPerUnit()
		void Register_UnityEngine_Canvas_get_referencePixelsPerUnit();
		Register_UnityEngine_Canvas_get_referencePixelsPerUnit();

		//System.Single UnityEngine.Canvas::get_scaleFactor()
		void Register_UnityEngine_Canvas_get_scaleFactor();
		Register_UnityEngine_Canvas_get_scaleFactor();

		//System.String UnityEngine.Canvas::get_sortingLayerName()
		void Register_UnityEngine_Canvas_get_sortingLayerName();
		Register_UnityEngine_Canvas_get_sortingLayerName();

		//System.Void UnityEngine.Canvas::set_additionalShaderChannels(UnityEngine.AdditionalCanvasShaderChannels)
		void Register_UnityEngine_Canvas_set_additionalShaderChannels();
		Register_UnityEngine_Canvas_set_additionalShaderChannels();

		//System.Void UnityEngine.Canvas::set_overrideSorting(System.Boolean)
		void Register_UnityEngine_Canvas_set_overrideSorting();
		Register_UnityEngine_Canvas_set_overrideSorting();

		//System.Void UnityEngine.Canvas::set_referencePixelsPerUnit(System.Single)
		void Register_UnityEngine_Canvas_set_referencePixelsPerUnit();
		Register_UnityEngine_Canvas_set_referencePixelsPerUnit();

		//System.Void UnityEngine.Canvas::set_renderMode(UnityEngine.RenderMode)
		void Register_UnityEngine_Canvas_set_renderMode();
		Register_UnityEngine_Canvas_set_renderMode();

		//System.Void UnityEngine.Canvas::set_scaleFactor(System.Single)
		void Register_UnityEngine_Canvas_set_scaleFactor();
		Register_UnityEngine_Canvas_set_scaleFactor();

		//System.Void UnityEngine.Canvas::set_sortingLayerID(System.Int32)
		void Register_UnityEngine_Canvas_set_sortingLayerID();
		Register_UnityEngine_Canvas_set_sortingLayerID();

		//System.Void UnityEngine.Canvas::set_sortingOrder(System.Int32)
		void Register_UnityEngine_Canvas_set_sortingOrder();
		Register_UnityEngine_Canvas_set_sortingOrder();

		//System.Void UnityEngine.Canvas::set_worldCamera(UnityEngine.Camera)
		void Register_UnityEngine_Canvas_set_worldCamera();
		Register_UnityEngine_Canvas_set_worldCamera();

		//UnityEngine.Camera UnityEngine.Canvas::get_worldCamera()
		void Register_UnityEngine_Canvas_get_worldCamera();
		Register_UnityEngine_Canvas_get_worldCamera();

		//UnityEngine.Canvas UnityEngine.Canvas::get_rootCanvas()
		void Register_UnityEngine_Canvas_get_rootCanvas();
		Register_UnityEngine_Canvas_get_rootCanvas();

		//UnityEngine.Material UnityEngine.Canvas::GetDefaultCanvasMaterial()
		void Register_UnityEngine_Canvas_GetDefaultCanvasMaterial();
		Register_UnityEngine_Canvas_GetDefaultCanvasMaterial();

		//UnityEngine.Material UnityEngine.Canvas::GetETC1SupportedCanvasMaterial()
		void Register_UnityEngine_Canvas_GetETC1SupportedCanvasMaterial();
		Register_UnityEngine_Canvas_GetETC1SupportedCanvasMaterial();

		//UnityEngine.RenderMode UnityEngine.Canvas::get_renderMode()
		void Register_UnityEngine_Canvas_get_renderMode();
		Register_UnityEngine_Canvas_get_renderMode();

	//End Registrations for type : UnityEngine.Canvas

	//Start Registrations for type : UnityEngine.CanvasGroup

		//System.Boolean UnityEngine.CanvasGroup::get_blocksRaycasts()
		void Register_UnityEngine_CanvasGroup_get_blocksRaycasts();
		Register_UnityEngine_CanvasGroup_get_blocksRaycasts();

		//System.Boolean UnityEngine.CanvasGroup::get_ignoreParentGroups()
		void Register_UnityEngine_CanvasGroup_get_ignoreParentGroups();
		Register_UnityEngine_CanvasGroup_get_ignoreParentGroups();

		//System.Boolean UnityEngine.CanvasGroup::get_interactable()
		void Register_UnityEngine_CanvasGroup_get_interactable();
		Register_UnityEngine_CanvasGroup_get_interactable();

		//System.Single UnityEngine.CanvasGroup::get_alpha()
		void Register_UnityEngine_CanvasGroup_get_alpha();
		Register_UnityEngine_CanvasGroup_get_alpha();

		//System.Void UnityEngine.CanvasGroup::set_alpha(System.Single)
		void Register_UnityEngine_CanvasGroup_set_alpha();
		Register_UnityEngine_CanvasGroup_set_alpha();

		//System.Void UnityEngine.CanvasGroup::set_blocksRaycasts(System.Boolean)
		void Register_UnityEngine_CanvasGroup_set_blocksRaycasts();
		Register_UnityEngine_CanvasGroup_set_blocksRaycasts();

		//System.Void UnityEngine.CanvasGroup::set_interactable(System.Boolean)
		void Register_UnityEngine_CanvasGroup_set_interactable();
		Register_UnityEngine_CanvasGroup_set_interactable();

	//End Registrations for type : UnityEngine.CanvasGroup

	//Start Registrations for type : UnityEngine.CanvasRenderer

		//System.Boolean UnityEngine.CanvasRenderer::get_cull()
		void Register_UnityEngine_CanvasRenderer_get_cull();
		Register_UnityEngine_CanvasRenderer_get_cull();

		//System.Boolean UnityEngine.CanvasRenderer::get_hasMoved()
		void Register_UnityEngine_CanvasRenderer_get_hasMoved();
		Register_UnityEngine_CanvasRenderer_get_hasMoved();

		//System.Int32 UnityEngine.CanvasRenderer::get_absoluteDepth()
		void Register_UnityEngine_CanvasRenderer_get_absoluteDepth();
		Register_UnityEngine_CanvasRenderer_get_absoluteDepth();

		//System.Int32 UnityEngine.CanvasRenderer::get_materialCount()
		void Register_UnityEngine_CanvasRenderer_get_materialCount();
		Register_UnityEngine_CanvasRenderer_get_materialCount();

		//System.Void UnityEngine.CanvasRenderer::Clear()
		void Register_UnityEngine_CanvasRenderer_Clear();
		Register_UnityEngine_CanvasRenderer_Clear();

		//System.Void UnityEngine.CanvasRenderer::CreateUIVertexStreamInternal(System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object)
		void Register_UnityEngine_CanvasRenderer_CreateUIVertexStreamInternal();
		Register_UnityEngine_CanvasRenderer_CreateUIVertexStreamInternal();

		//System.Void UnityEngine.CanvasRenderer::DisableRectClipping()
		void Register_UnityEngine_CanvasRenderer_DisableRectClipping();
		Register_UnityEngine_CanvasRenderer_DisableRectClipping();

		//System.Void UnityEngine.CanvasRenderer::INTERNAL_CALL_EnableRectClipping(UnityEngine.CanvasRenderer,UnityEngine.Rect&)
		void Register_UnityEngine_CanvasRenderer_INTERNAL_CALL_EnableRectClipping();
		Register_UnityEngine_CanvasRenderer_INTERNAL_CALL_EnableRectClipping();

		//System.Void UnityEngine.CanvasRenderer::INTERNAL_CALL_GetColor(UnityEngine.CanvasRenderer,UnityEngine.Color&)
		void Register_UnityEngine_CanvasRenderer_INTERNAL_CALL_GetColor();
		Register_UnityEngine_CanvasRenderer_INTERNAL_CALL_GetColor();

		//System.Void UnityEngine.CanvasRenderer::INTERNAL_CALL_SetColor(UnityEngine.CanvasRenderer,UnityEngine.Color&)
		void Register_UnityEngine_CanvasRenderer_INTERNAL_CALL_SetColor();
		Register_UnityEngine_CanvasRenderer_INTERNAL_CALL_SetColor();

		//System.Void UnityEngine.CanvasRenderer::SetAlphaTexture(UnityEngine.Texture)
		void Register_UnityEngine_CanvasRenderer_SetAlphaTexture();
		Register_UnityEngine_CanvasRenderer_SetAlphaTexture();

		//System.Void UnityEngine.CanvasRenderer::SetMaterial(UnityEngine.Material,System.Int32)
		void Register_UnityEngine_CanvasRenderer_SetMaterial();
		Register_UnityEngine_CanvasRenderer_SetMaterial();

		//System.Void UnityEngine.CanvasRenderer::SetMesh(UnityEngine.Mesh)
		void Register_UnityEngine_CanvasRenderer_SetMesh();
		Register_UnityEngine_CanvasRenderer_SetMesh();

		//System.Void UnityEngine.CanvasRenderer::SetPopMaterial(UnityEngine.Material,System.Int32)
		void Register_UnityEngine_CanvasRenderer_SetPopMaterial();
		Register_UnityEngine_CanvasRenderer_SetPopMaterial();

		//System.Void UnityEngine.CanvasRenderer::SetTexture(UnityEngine.Texture)
		void Register_UnityEngine_CanvasRenderer_SetTexture();
		Register_UnityEngine_CanvasRenderer_SetTexture();

		//System.Void UnityEngine.CanvasRenderer::SplitIndicesStreamsInternal(System.Object,System.Object)
		void Register_UnityEngine_CanvasRenderer_SplitIndicesStreamsInternal();
		Register_UnityEngine_CanvasRenderer_SplitIndicesStreamsInternal();

		//System.Void UnityEngine.CanvasRenderer::SplitUIVertexStreamsInternal(System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object)
		void Register_UnityEngine_CanvasRenderer_SplitUIVertexStreamsInternal();
		Register_UnityEngine_CanvasRenderer_SplitUIVertexStreamsInternal();

		//System.Void UnityEngine.CanvasRenderer::set_cull(System.Boolean)
		void Register_UnityEngine_CanvasRenderer_set_cull();
		Register_UnityEngine_CanvasRenderer_set_cull();

		//System.Void UnityEngine.CanvasRenderer::set_hasPopInstruction(System.Boolean)
		void Register_UnityEngine_CanvasRenderer_set_hasPopInstruction();
		Register_UnityEngine_CanvasRenderer_set_hasPopInstruction();

		//System.Void UnityEngine.CanvasRenderer::set_materialCount(System.Int32)
		void Register_UnityEngine_CanvasRenderer_set_materialCount();
		Register_UnityEngine_CanvasRenderer_set_materialCount();

		//System.Void UnityEngine.CanvasRenderer::set_popMaterialCount(System.Int32)
		void Register_UnityEngine_CanvasRenderer_set_popMaterialCount();
		Register_UnityEngine_CanvasRenderer_set_popMaterialCount();

	//End Registrations for type : UnityEngine.CanvasRenderer

	//Start Registrations for type : UnityEngine.CapsuleCollider

		//System.Single UnityEngine.CapsuleCollider::get_height()
		void Register_UnityEngine_CapsuleCollider_get_height();
		Register_UnityEngine_CapsuleCollider_get_height();

		//System.Void UnityEngine.CapsuleCollider::INTERNAL_get_center(UnityEngine.Vector3&)
		void Register_UnityEngine_CapsuleCollider_INTERNAL_get_center();
		Register_UnityEngine_CapsuleCollider_INTERNAL_get_center();

		//System.Void UnityEngine.CapsuleCollider::INTERNAL_set_center(UnityEngine.Vector3&)
		void Register_UnityEngine_CapsuleCollider_INTERNAL_set_center();
		Register_UnityEngine_CapsuleCollider_INTERNAL_set_center();

		//System.Void UnityEngine.CapsuleCollider::set_height(System.Single)
		void Register_UnityEngine_CapsuleCollider_set_height();
		Register_UnityEngine_CapsuleCollider_set_height();

	//End Registrations for type : UnityEngine.CapsuleCollider

	//Start Registrations for type : UnityEngine.Collider2D

		//System.Void UnityEngine.Collider2D::INTERNAL_get_offset(UnityEngine.Vector2&)
		void Register_UnityEngine_Collider2D_INTERNAL_get_offset();
		Register_UnityEngine_Collider2D_INTERNAL_get_offset();

		//System.Void UnityEngine.Collider2D::INTERNAL_set_offset(UnityEngine.Vector2&)
		void Register_UnityEngine_Collider2D_INTERNAL_set_offset();
		Register_UnityEngine_Collider2D_INTERNAL_set_offset();

	//End Registrations for type : UnityEngine.Collider2D

	//Start Registrations for type : UnityEngine.Component

		//System.Void UnityEngine.Component::GetComponentFastPath(System.Type,System.IntPtr)
		void Register_UnityEngine_Component_GetComponentFastPath();
		Register_UnityEngine_Component_GetComponentFastPath();

		//System.Void UnityEngine.Component::GetComponentsForListInternal(System.Type,System.Object)
		void Register_UnityEngine_Component_GetComponentsForListInternal();
		Register_UnityEngine_Component_GetComponentsForListInternal();

		//UnityEngine.GameObject UnityEngine.Component::get_gameObject()
		void Register_UnityEngine_Component_get_gameObject();
		Register_UnityEngine_Component_get_gameObject();

		//UnityEngine.Transform UnityEngine.Component::get_transform()
		void Register_UnityEngine_Component_get_transform();
		Register_UnityEngine_Component_get_transform();

	//End Registrations for type : UnityEngine.Component

	//Start Registrations for type : UnityEngine.Coroutine

		//System.Void UnityEngine.Coroutine::ReleaseCoroutine()
		void Register_UnityEngine_Coroutine_ReleaseCoroutine();
		Register_UnityEngine_Coroutine_ReleaseCoroutine();

	//End Registrations for type : UnityEngine.Coroutine

	//Start Registrations for type : UnityEngine.CullingGroup

		//System.Void UnityEngine.CullingGroup::Dispose()
		void Register_UnityEngine_CullingGroup_Dispose();
		Register_UnityEngine_CullingGroup_Dispose();

		//System.Void UnityEngine.CullingGroup::FinalizerFailure()
		void Register_UnityEngine_CullingGroup_FinalizerFailure();
		Register_UnityEngine_CullingGroup_FinalizerFailure();

	//End Registrations for type : UnityEngine.CullingGroup

	//Start Registrations for type : UnityEngine.Cursor

		//UnityEngine.CursorLockMode UnityEngine.Cursor::get_lockState()
		void Register_UnityEngine_Cursor_get_lockState();
		Register_UnityEngine_Cursor_get_lockState();

	//End Registrations for type : UnityEngine.Cursor

	//Start Registrations for type : UnityEngine.Debug

		//System.Boolean UnityEngine.Debug::get_isDebugBuild()
		void Register_UnityEngine_Debug_get_isDebugBuild();
		Register_UnityEngine_Debug_get_isDebugBuild();

	//End Registrations for type : UnityEngine.Debug

	//Start Registrations for type : UnityEngine.DebugLogHandler

		//System.Void UnityEngine.DebugLogHandler::Internal_Log(UnityEngine.LogType,System.String,UnityEngine.Object)
		void Register_UnityEngine_DebugLogHandler_Internal_Log();
		Register_UnityEngine_DebugLogHandler_Internal_Log();

		//System.Void UnityEngine.DebugLogHandler::Internal_LogException(System.Exception,UnityEngine.Object)
		void Register_UnityEngine_DebugLogHandler_Internal_LogException();
		Register_UnityEngine_DebugLogHandler_Internal_LogException();

	//End Registrations for type : UnityEngine.DebugLogHandler

	//Start Registrations for type : UnityEngine.Display

		//System.Int32 UnityEngine.Display::RelativeMouseAtImpl(System.Int32,System.Int32,System.Int32&,System.Int32&)
		void Register_UnityEngine_Display_RelativeMouseAtImpl();
		Register_UnityEngine_Display_RelativeMouseAtImpl();

		//System.Void UnityEngine.Display::GetRenderingExtImpl(System.IntPtr,System.Int32&,System.Int32&)
		void Register_UnityEngine_Display_GetRenderingExtImpl();
		Register_UnityEngine_Display_GetRenderingExtImpl();

		//System.Void UnityEngine.Display::GetSystemExtImpl(System.IntPtr,System.Int32&,System.Int32&)
		void Register_UnityEngine_Display_GetSystemExtImpl();
		Register_UnityEngine_Display_GetSystemExtImpl();

	//End Registrations for type : UnityEngine.Display

	//Start Registrations for type : UnityEngine.Event

		//System.Boolean UnityEngine.Event::PopEvent(UnityEngine.Event)
		void Register_UnityEngine_Event_PopEvent();
		Register_UnityEngine_Event_PopEvent();

		//System.Char UnityEngine.Event::get_character()
		void Register_UnityEngine_Event_get_character();
		Register_UnityEngine_Event_get_character();

		//System.String UnityEngine.Event::get_commandName()
		void Register_UnityEngine_Event_get_commandName();
		Register_UnityEngine_Event_get_commandName();

		//System.Void UnityEngine.Event::Cleanup()
		void Register_UnityEngine_Event_Cleanup();
		Register_UnityEngine_Event_Cleanup();

		//System.Void UnityEngine.Event::Init(System.Int32)
		void Register_UnityEngine_Event_Init();
		Register_UnityEngine_Event_Init();

		//System.Void UnityEngine.Event::Internal_GetMousePosition(UnityEngine.Vector2&)
		void Register_UnityEngine_Event_Internal_GetMousePosition();
		Register_UnityEngine_Event_Internal_GetMousePosition();

		//System.Void UnityEngine.Event::Internal_SetNativeEvent(System.IntPtr)
		void Register_UnityEngine_Event_Internal_SetNativeEvent();
		Register_UnityEngine_Event_Internal_SetNativeEvent();

		//System.Void UnityEngine.Event::Internal_Use()
		void Register_UnityEngine_Event_Internal_Use();
		Register_UnityEngine_Event_Internal_Use();

		//System.Void UnityEngine.Event::set_displayIndex(System.Int32)
		void Register_UnityEngine_Event_set_displayIndex();
		Register_UnityEngine_Event_set_displayIndex();

		//UnityEngine.EventModifiers UnityEngine.Event::get_modifiers()
		void Register_UnityEngine_Event_get_modifiers();
		Register_UnityEngine_Event_get_modifiers();

		//UnityEngine.EventType UnityEngine.Event::GetTypeForControl(System.Int32)
		void Register_UnityEngine_Event_GetTypeForControl();
		Register_UnityEngine_Event_GetTypeForControl();

		//UnityEngine.EventType UnityEngine.Event::get_rawType()
		void Register_UnityEngine_Event_get_rawType();
		Register_UnityEngine_Event_get_rawType();

		//UnityEngine.EventType UnityEngine.Event::get_type()
		void Register_UnityEngine_Event_get_type();
		Register_UnityEngine_Event_get_type();

		//UnityEngine.KeyCode UnityEngine.Event::get_keyCode()
		void Register_UnityEngine_Event_get_keyCode();
		Register_UnityEngine_Event_get_keyCode();

	//End Registrations for type : UnityEngine.Event

	//Start Registrations for type : UnityEngine.Font

		//System.Boolean UnityEngine.Font::GetCharacterInfo(System.Char,UnityEngine.CharacterInfo&,System.Int32,UnityEngine.FontStyle)
		void Register_UnityEngine_Font_GetCharacterInfo();
		Register_UnityEngine_Font_GetCharacterInfo();

		//System.Boolean UnityEngine.Font::HasCharacter(System.Char)
		void Register_UnityEngine_Font_HasCharacter();
		Register_UnityEngine_Font_HasCharacter();

		//System.Boolean UnityEngine.Font::get_dynamic()
		void Register_UnityEngine_Font_get_dynamic();
		Register_UnityEngine_Font_get_dynamic();

		//System.Int32 UnityEngine.Font::get_fontSize()
		void Register_UnityEngine_Font_get_fontSize();
		Register_UnityEngine_Font_get_fontSize();

		//System.Void UnityEngine.Font::RequestCharactersInTexture(System.String,System.Int32,UnityEngine.FontStyle)
		void Register_UnityEngine_Font_RequestCharactersInTexture();
		Register_UnityEngine_Font_RequestCharactersInTexture();

		//UnityEngine.Material UnityEngine.Font::get_material()
		void Register_UnityEngine_Font_get_material();
		Register_UnityEngine_Font_get_material();

	//End Registrations for type : UnityEngine.Font

	//Start Registrations for type : UnityEngine.GameObject

		//System.Array UnityEngine.GameObject::GetComponentsInternal(System.Type,System.Boolean,System.Boolean,System.Boolean,System.Boolean,System.Object)
		void Register_UnityEngine_GameObject_GetComponentsInternal();
		Register_UnityEngine_GameObject_GetComponentsInternal();

		//System.Boolean UnityEngine.GameObject::get_activeInHierarchy()
		void Register_UnityEngine_GameObject_get_activeInHierarchy();
		Register_UnityEngine_GameObject_get_activeInHierarchy();

		//System.Boolean UnityEngine.GameObject::get_activeSelf()
		void Register_UnityEngine_GameObject_get_activeSelf();
		Register_UnityEngine_GameObject_get_activeSelf();

		//System.Int32 UnityEngine.GameObject::get_layer()
		void Register_UnityEngine_GameObject_get_layer();
		Register_UnityEngine_GameObject_get_layer();

		//System.Void UnityEngine.GameObject::GetComponentFastPath(System.Type,System.IntPtr)
		void Register_UnityEngine_GameObject_GetComponentFastPath();
		Register_UnityEngine_GameObject_GetComponentFastPath();

		//System.Void UnityEngine.GameObject::Internal_CreateGameObject(UnityEngine.GameObject,System.String)
		void Register_UnityEngine_GameObject_Internal_CreateGameObject();
		Register_UnityEngine_GameObject_Internal_CreateGameObject();

		//System.Void UnityEngine.GameObject::SendMessage(System.String,System.Object,UnityEngine.SendMessageOptions)
		void Register_UnityEngine_GameObject_SendMessage();
		Register_UnityEngine_GameObject_SendMessage();

		//System.Void UnityEngine.GameObject::SetActive(System.Boolean)
		void Register_UnityEngine_GameObject_SetActive();
		Register_UnityEngine_GameObject_SetActive();

		//System.Void UnityEngine.GameObject::set_layer(System.Int32)
		void Register_UnityEngine_GameObject_set_layer();
		Register_UnityEngine_GameObject_set_layer();

		//UnityEngine.Component UnityEngine.GameObject::GetComponent(System.Type)
		void Register_UnityEngine_GameObject_GetComponent();
		Register_UnityEngine_GameObject_GetComponent();

		//UnityEngine.Component UnityEngine.GameObject::GetComponentInChildren(System.Type,System.Boolean)
		void Register_UnityEngine_GameObject_GetComponentInChildren();
		Register_UnityEngine_GameObject_GetComponentInChildren();

		//UnityEngine.Component UnityEngine.GameObject::GetComponentInParent(System.Type)
		void Register_UnityEngine_GameObject_GetComponentInParent();
		Register_UnityEngine_GameObject_GetComponentInParent();

		//UnityEngine.Component UnityEngine.GameObject::Internal_AddComponentWithType(System.Type)
		void Register_UnityEngine_GameObject_Internal_AddComponentWithType();
		Register_UnityEngine_GameObject_Internal_AddComponentWithType();

		//UnityEngine.GameObject UnityEngine.GameObject::Find(System.String)
		void Register_UnityEngine_GameObject_Find();
		Register_UnityEngine_GameObject_Find();

		//UnityEngine.GameObject UnityEngine.GameObject::FindGameObjectWithTag(System.String)
		void Register_UnityEngine_GameObject_FindGameObjectWithTag();
		Register_UnityEngine_GameObject_FindGameObjectWithTag();

		//UnityEngine.Transform UnityEngine.GameObject::get_transform()
		void Register_UnityEngine_GameObject_get_transform();
		Register_UnityEngine_GameObject_get_transform();

	//End Registrations for type : UnityEngine.GameObject

	//Start Registrations for type : UnityEngine.Gizmos

		//System.Void UnityEngine.Gizmos::INTERNAL_CALL_DrawLine(UnityEngine.Vector3&,UnityEngine.Vector3&)
		void Register_UnityEngine_Gizmos_INTERNAL_CALL_DrawLine();
		Register_UnityEngine_Gizmos_INTERNAL_CALL_DrawLine();

		//System.Void UnityEngine.Gizmos::INTERNAL_CALL_DrawWireCube(UnityEngine.Vector3&,UnityEngine.Vector3&)
		void Register_UnityEngine_Gizmos_INTERNAL_CALL_DrawWireCube();
		Register_UnityEngine_Gizmos_INTERNAL_CALL_DrawWireCube();

		//System.Void UnityEngine.Gizmos::INTERNAL_CALL_DrawWireSphere(UnityEngine.Vector3&,System.Single)
		void Register_UnityEngine_Gizmos_INTERNAL_CALL_DrawWireSphere();
		Register_UnityEngine_Gizmos_INTERNAL_CALL_DrawWireSphere();

		//System.Void UnityEngine.Gizmos::INTERNAL_set_color(UnityEngine.Color&)
		void Register_UnityEngine_Gizmos_INTERNAL_set_color();
		Register_UnityEngine_Gizmos_INTERNAL_set_color();

	//End Registrations for type : UnityEngine.Gizmos

	//Start Registrations for type : UnityEngine.Gradient

		//System.Void UnityEngine.Gradient::Cleanup()
		void Register_UnityEngine_Gradient_Cleanup();
		Register_UnityEngine_Gradient_Cleanup();

		//System.Void UnityEngine.Gradient::Init()
		void Register_UnityEngine_Gradient_Init();
		Register_UnityEngine_Gradient_Init();

	//End Registrations for type : UnityEngine.Gradient

	//Start Registrations for type : UnityEngine.Graphics

		//System.Int32 UnityEngine.Graphics::Internal_GetMaxDrawMeshInstanceCount()
		void Register_UnityEngine_Graphics_Internal_GetMaxDrawMeshInstanceCount();
		Register_UnityEngine_Graphics_Internal_GetMaxDrawMeshInstanceCount();

		//System.Void UnityEngine.Graphics::Blit(UnityEngine.Texture,UnityEngine.RenderTexture)
		void Register_UnityEngine_Graphics_Blit();
		Register_UnityEngine_Graphics_Blit();

		//System.Void UnityEngine.Graphics::ExecuteCommandBuffer(UnityEngine.Rendering.CommandBuffer)
		void Register_UnityEngine_Graphics_ExecuteCommandBuffer();
		Register_UnityEngine_Graphics_ExecuteCommandBuffer();

		//System.Void UnityEngine.Graphics::INTERNAL_CALL_Internal_BlitMaterial(UnityEngine.Texture,UnityEngine.RenderTexture,UnityEngine.Material,System.Int32,System.Boolean,UnityEngine.Vector2&,UnityEngine.Vector2&)
		void Register_UnityEngine_Graphics_INTERNAL_CALL_Internal_BlitMaterial();
		Register_UnityEngine_Graphics_INTERNAL_CALL_Internal_BlitMaterial();

		//System.Void UnityEngine.Graphics::Internal_DrawTexture(UnityEngine.Internal_DrawTextureArguments&)
		void Register_UnityEngine_Graphics_Internal_DrawTexture();
		Register_UnityEngine_Graphics_Internal_DrawTexture();

	//End Registrations for type : UnityEngine.Graphics

	//Start Registrations for type : UnityEngine.GUI

		//System.Boolean UnityEngine.GUI::INTERNAL_CALL_DoButton(UnityEngine.Rect&,UnityEngine.GUIContent,System.IntPtr)
		void Register_UnityEngine_GUI_INTERNAL_CALL_DoButton();
		Register_UnityEngine_GUI_INTERNAL_CALL_DoButton();

		//System.Boolean UnityEngine.GUI::INTERNAL_CALL_DoToggle(UnityEngine.Rect&,System.Int32,System.Boolean,UnityEngine.GUIContent,System.IntPtr)
		void Register_UnityEngine_GUI_INTERNAL_CALL_DoToggle();
		Register_UnityEngine_GUI_INTERNAL_CALL_DoToggle();

		//System.Void UnityEngine.GUI::INTERNAL_CALL_DoLabel(UnityEngine.Rect&,UnityEngine.GUIContent,System.IntPtr)
		void Register_UnityEngine_GUI_INTERNAL_CALL_DoLabel();
		Register_UnityEngine_GUI_INTERNAL_CALL_DoLabel();

		//System.Void UnityEngine.GUI::INTERNAL_get_color(UnityEngine.Color&)
		void Register_UnityEngine_GUI_INTERNAL_get_color();
		Register_UnityEngine_GUI_INTERNAL_get_color();

		//System.Void UnityEngine.GUI::INTERNAL_set_color(UnityEngine.Color&)
		void Register_UnityEngine_GUI_INTERNAL_set_color();
		Register_UnityEngine_GUI_INTERNAL_set_color();

		//System.Void UnityEngine.GUI::set_changed(System.Boolean)
		void Register_UnityEngine_GUI_set_changed();
		Register_UnityEngine_GUI_set_changed();

		//UnityEngine.Material UnityEngine.GUI::get_blendMaterial()
		void Register_UnityEngine_GUI_get_blendMaterial();
		Register_UnityEngine_GUI_get_blendMaterial();

		//UnityEngine.Material UnityEngine.GUI::get_blitMaterial()
		void Register_UnityEngine_GUI_get_blitMaterial();
		Register_UnityEngine_GUI_get_blitMaterial();

		//UnityEngine.Material UnityEngine.GUI::get_roundedRectMaterial()
		void Register_UnityEngine_GUI_get_roundedRectMaterial();
		Register_UnityEngine_GUI_get_roundedRectMaterial();

	//End Registrations for type : UnityEngine.GUI

	//Start Registrations for type : UnityEngine.GUIClip

		//System.Void UnityEngine.GUIClip::INTERNAL_CALL_Internal_Push(UnityEngine.Rect&,UnityEngine.Vector2&,UnityEngine.Vector2&,System.Boolean)
		void Register_UnityEngine_GUIClip_INTERNAL_CALL_Internal_Push();
		Register_UnityEngine_GUIClip_INTERNAL_CALL_Internal_Push();

		//System.Void UnityEngine.GUIClip::Internal_Pop()
		void Register_UnityEngine_GUIClip_Internal_Pop();
		Register_UnityEngine_GUIClip_Internal_Pop();

	//End Registrations for type : UnityEngine.GUIClip

	//Start Registrations for type : UnityEngine.GUILayer

		//UnityEngine.GUIElement UnityEngine.GUILayer::INTERNAL_CALL_HitTest(UnityEngine.GUILayer,UnityEngine.Vector3&)
		void Register_UnityEngine_GUILayer_INTERNAL_CALL_HitTest();
		Register_UnityEngine_GUILayer_INTERNAL_CALL_HitTest();

	//End Registrations for type : UnityEngine.GUILayer

	//Start Registrations for type : UnityEngine.GUILayoutUtility

		//System.Void UnityEngine.GUILayoutUtility::INTERNAL_CALL_Internal_GetWindowRect(System.Int32,UnityEngine.Rect&)
		void Register_UnityEngine_GUILayoutUtility_INTERNAL_CALL_Internal_GetWindowRect();
		Register_UnityEngine_GUILayoutUtility_INTERNAL_CALL_Internal_GetWindowRect();

		//System.Void UnityEngine.GUILayoutUtility::INTERNAL_CALL_Internal_MoveWindow(System.Int32,UnityEngine.Rect&)
		void Register_UnityEngine_GUILayoutUtility_INTERNAL_CALL_Internal_MoveWindow();
		Register_UnityEngine_GUILayoutUtility_INTERNAL_CALL_Internal_MoveWindow();

	//End Registrations for type : UnityEngine.GUILayoutUtility

	//Start Registrations for type : UnityEngine.GUIStyle

		//System.Boolean UnityEngine.GUIStyle::get_stretchHeight()
		void Register_UnityEngine_GUIStyle_get_stretchHeight();
		Register_UnityEngine_GUIStyle_get_stretchHeight();

		//System.Boolean UnityEngine.GUIStyle::get_stretchWidth()
		void Register_UnityEngine_GUIStyle_get_stretchWidth();
		Register_UnityEngine_GUIStyle_get_stretchWidth();

		//System.Boolean UnityEngine.GUIStyle::get_wordWrap()
		void Register_UnityEngine_GUIStyle_get_wordWrap();
		Register_UnityEngine_GUIStyle_get_wordWrap();

		//System.Single UnityEngine.GUIStyle::Internal_CalcHeight(System.IntPtr,UnityEngine.GUIContent,System.Single)
		void Register_UnityEngine_GUIStyle_Internal_CalcHeight();
		Register_UnityEngine_GUIStyle_Internal_CalcHeight();

		//System.Single UnityEngine.GUIStyle::get_fixedHeight()
		void Register_UnityEngine_GUIStyle_get_fixedHeight();
		Register_UnityEngine_GUIStyle_get_fixedHeight();

		//System.Single UnityEngine.GUIStyle::get_fixedWidth()
		void Register_UnityEngine_GUIStyle_get_fixedWidth();
		Register_UnityEngine_GUIStyle_get_fixedWidth();

		//System.String UnityEngine.GUIStyle::get_name()
		void Register_UnityEngine_GUIStyle_get_name();
		Register_UnityEngine_GUIStyle_get_name();

		//System.Void UnityEngine.GUIStyle::Cleanup()
		void Register_UnityEngine_GUIStyle_Cleanup();
		Register_UnityEngine_GUIStyle_Cleanup();

		//System.Void UnityEngine.GUIStyle::INTERNAL_CALL_GetRectOffsetPtr(UnityEngine.GUIStyle,System.Int32,System.IntPtr&)
		void Register_UnityEngine_GUIStyle_INTERNAL_CALL_GetRectOffsetPtr();
		Register_UnityEngine_GUIStyle_INTERNAL_CALL_GetRectOffsetPtr();

		//System.Void UnityEngine.GUIStyle::INTERNAL_CALL_Internal_CalcSizeWithConstraints(System.IntPtr,UnityEngine.GUIContent,UnityEngine.Vector2&,UnityEngine.Vector2&)
		void Register_UnityEngine_GUIStyle_INTERNAL_CALL_Internal_CalcSizeWithConstraints();
		Register_UnityEngine_GUIStyle_INTERNAL_CALL_Internal_CalcSizeWithConstraints();

		//System.Void UnityEngine.GUIStyle::INTERNAL_CALL_Internal_Draw2(System.IntPtr,UnityEngine.Rect&,UnityEngine.GUIContent,System.Int32,System.Boolean)
		void Register_UnityEngine_GUIStyle_INTERNAL_CALL_Internal_Draw2();
		Register_UnityEngine_GUIStyle_INTERNAL_CALL_Internal_Draw2();

		//System.Void UnityEngine.GUIStyle::Init()
		void Register_UnityEngine_GUIStyle_Init();
		Register_UnityEngine_GUIStyle_Init();

		//System.Void UnityEngine.GUIStyle::Internal_CalcMinMaxWidth(System.IntPtr,UnityEngine.GUIContent,System.Single&,System.Single&)
		void Register_UnityEngine_GUIStyle_Internal_CalcMinMaxWidth();
		Register_UnityEngine_GUIStyle_Internal_CalcMinMaxWidth();

		//System.Void UnityEngine.GUIStyle::SetDefaultFont(UnityEngine.Font)
		void Register_UnityEngine_GUIStyle_SetDefaultFont();
		Register_UnityEngine_GUIStyle_SetDefaultFont();

		//System.Void UnityEngine.GUIStyle::set_stretchWidth(System.Boolean)
		void Register_UnityEngine_GUIStyle_set_stretchWidth();
		Register_UnityEngine_GUIStyle_set_stretchWidth();

		//UnityEngine.ImagePosition UnityEngine.GUIStyle::get_imagePosition()
		void Register_UnityEngine_GUIStyle_get_imagePosition();
		Register_UnityEngine_GUIStyle_get_imagePosition();

	//End Registrations for type : UnityEngine.GUIStyle

	//Start Registrations for type : UnityEngine.GUIStyleState

		//System.Void UnityEngine.GUIStyleState::Cleanup()
		void Register_UnityEngine_GUIStyleState_Cleanup();
		Register_UnityEngine_GUIStyleState_Cleanup();

		//System.Void UnityEngine.GUIStyleState::Init()
		void Register_UnityEngine_GUIStyleState_Init();
		Register_UnityEngine_GUIStyleState_Init();

	//End Registrations for type : UnityEngine.GUIStyleState

	//Start Registrations for type : UnityEngine.GUIUtility

		//System.Int32 UnityEngine.GUIUtility::GetControlID(System.Int32,UnityEngine.FocusType)
		void Register_UnityEngine_GUIUtility_GetControlID();
		Register_UnityEngine_GUIUtility_GetControlID();

		//System.Int32 UnityEngine.GUIUtility::INTERNAL_CALL_Internal_GetNextControlID2(System.Int32,UnityEngine.FocusType,UnityEngine.Rect&)
		void Register_UnityEngine_GUIUtility_INTERNAL_CALL_Internal_GetNextControlID2();
		Register_UnityEngine_GUIUtility_INTERNAL_CALL_Internal_GetNextControlID2();

		//System.Int32 UnityEngine.GUIUtility::Internal_GetGUIDepth()
		void Register_UnityEngine_GUIUtility_Internal_GetGUIDepth();
		Register_UnityEngine_GUIUtility_Internal_GetGUIDepth();

		//System.Int32 UnityEngine.GUIUtility::Internal_GetHotControl()
		void Register_UnityEngine_GUIUtility_Internal_GetHotControl();
		Register_UnityEngine_GUIUtility_Internal_GetHotControl();

		//System.Single UnityEngine.GUIUtility::Internal_GetPixelsPerPoint()
		void Register_UnityEngine_GUIUtility_Internal_GetPixelsPerPoint();
		Register_UnityEngine_GUIUtility_Internal_GetPixelsPerPoint();

		//System.String UnityEngine.GUIUtility::get_systemCopyBuffer()
		void Register_UnityEngine_GUIUtility_get_systemCopyBuffer();
		Register_UnityEngine_GUIUtility_get_systemCopyBuffer();

		//System.Void UnityEngine.GUIUtility::Internal_ExitGUI()
		void Register_UnityEngine_GUIUtility_Internal_ExitGUI();
		Register_UnityEngine_GUIUtility_Internal_ExitGUI();

		//System.Void UnityEngine.GUIUtility::Internal_SetHotControl(System.Int32)
		void Register_UnityEngine_GUIUtility_Internal_SetHotControl();
		Register_UnityEngine_GUIUtility_Internal_SetHotControl();

		//System.Void UnityEngine.GUIUtility::set_mouseUsed(System.Boolean)
		void Register_UnityEngine_GUIUtility_set_mouseUsed();
		Register_UnityEngine_GUIUtility_set_mouseUsed();

		//System.Void UnityEngine.GUIUtility::set_systemCopyBuffer(System.String)
		void Register_UnityEngine_GUIUtility_set_systemCopyBuffer();
		Register_UnityEngine_GUIUtility_set_systemCopyBuffer();

		//UnityEngine.GUISkin UnityEngine.GUIUtility::Internal_GetDefaultSkin(System.Int32)
		void Register_UnityEngine_GUIUtility_Internal_GetDefaultSkin();
		Register_UnityEngine_GUIUtility_Internal_GetDefaultSkin();

	//End Registrations for type : UnityEngine.GUIUtility

	//Start Registrations for type : UnityEngine.Handheld

		//System.Boolean UnityEngine.Handheld::INTERNAL_CALL_PlayFullScreenMovie(System.String,UnityEngine.Color&,UnityEngine.FullScreenMovieControlMode,UnityEngine.FullScreenMovieScalingMode)
		void Register_UnityEngine_Handheld_INTERNAL_CALL_PlayFullScreenMovie();
		Register_UnityEngine_Handheld_INTERNAL_CALL_PlayFullScreenMovie();

		//System.Void UnityEngine.Handheld::SetActivityIndicatorStyleImpl(System.Int32)
		void Register_UnityEngine_Handheld_SetActivityIndicatorStyleImpl();
		Register_UnityEngine_Handheld_SetActivityIndicatorStyleImpl();

		//System.Void UnityEngine.Handheld::StartActivityIndicator()
		void Register_UnityEngine_Handheld_StartActivityIndicator();
		Register_UnityEngine_Handheld_StartActivityIndicator();

		//System.Void UnityEngine.Handheld::StopActivityIndicator()
		void Register_UnityEngine_Handheld_StopActivityIndicator();
		Register_UnityEngine_Handheld_StopActivityIndicator();

		//System.Void UnityEngine.Handheld::Vibrate()
		void Register_UnityEngine_Handheld_Vibrate();
		Register_UnityEngine_Handheld_Vibrate();

	//End Registrations for type : UnityEngine.Handheld

	//Start Registrations for type : UnityEngine.Hash128

		//System.String UnityEngine.Hash128::Internal_Hash128ToString(System.UInt32,System.UInt32,System.UInt32,System.UInt32)
		void Register_UnityEngine_Hash128_Internal_Hash128ToString();
		Register_UnityEngine_Hash128_Internal_Hash128ToString();

	//End Registrations for type : UnityEngine.Hash128

	//Start Registrations for type : UnityEngine.ImageConversion

		//System.Boolean UnityEngine.ImageConversion::LoadImage(UnityEngine.Texture2D,System.Byte[],System.Boolean)
		void Register_UnityEngine_ImageConversion_LoadImage();
		Register_UnityEngine_ImageConversion_LoadImage();

		//System.Byte[] UnityEngine.ImageConversion::EncodeToPNG(UnityEngine.Texture2D)
		void Register_UnityEngine_ImageConversion_EncodeToPNG();
		Register_UnityEngine_ImageConversion_EncodeToPNG();

	//End Registrations for type : UnityEngine.ImageConversion

	//Start Registrations for type : UnityEngine.Input

		//System.Boolean UnityEngine.Input::GetButton(System.String)
		void Register_UnityEngine_Input_GetButton();
		Register_UnityEngine_Input_GetButton();

		//System.Boolean UnityEngine.Input::GetButtonDown(System.String)
		void Register_UnityEngine_Input_GetButtonDown();
		Register_UnityEngine_Input_GetButtonDown();

		//System.Boolean UnityEngine.Input::GetKeyDownInt(System.Int32)
		void Register_UnityEngine_Input_GetKeyDownInt();
		Register_UnityEngine_Input_GetKeyDownInt();

		//System.Boolean UnityEngine.Input::GetKeyDownString(System.String)
		void Register_UnityEngine_Input_GetKeyDownString();
		Register_UnityEngine_Input_GetKeyDownString();

		//System.Boolean UnityEngine.Input::GetKeyInt(System.Int32)
		void Register_UnityEngine_Input_GetKeyInt();
		Register_UnityEngine_Input_GetKeyInt();

		//System.Boolean UnityEngine.Input::GetMouseButton(System.Int32)
		void Register_UnityEngine_Input_GetMouseButton();
		Register_UnityEngine_Input_GetMouseButton();

		//System.Boolean UnityEngine.Input::GetMouseButtonDown(System.Int32)
		void Register_UnityEngine_Input_GetMouseButtonDown();
		Register_UnityEngine_Input_GetMouseButtonDown();

		//System.Boolean UnityEngine.Input::GetMouseButtonUp(System.Int32)
		void Register_UnityEngine_Input_GetMouseButtonUp();
		Register_UnityEngine_Input_GetMouseButtonUp();

		//System.Boolean UnityEngine.Input::get_mousePresent()
		void Register_UnityEngine_Input_get_mousePresent();
		Register_UnityEngine_Input_get_mousePresent();

		//System.Boolean UnityEngine.Input::get_touchSupported()
		void Register_UnityEngine_Input_get_touchSupported();
		Register_UnityEngine_Input_get_touchSupported();

		//System.Int32 UnityEngine.Input::get_touchCount()
		void Register_UnityEngine_Input_get_touchCount();
		Register_UnityEngine_Input_get_touchCount();

		//System.Single UnityEngine.Input::GetAxis(System.String)
		void Register_UnityEngine_Input_GetAxis();
		Register_UnityEngine_Input_GetAxis();

		//System.Single UnityEngine.Input::GetAxisRaw(System.String)
		void Register_UnityEngine_Input_GetAxisRaw();
		Register_UnityEngine_Input_GetAxisRaw();

		//System.String UnityEngine.Input::get_compositionString()
		void Register_UnityEngine_Input_get_compositionString();
		Register_UnityEngine_Input_get_compositionString();

		//System.Void UnityEngine.Input::INTERNAL_CALL_GetTouch(System.Int32,UnityEngine.Touch&)
		void Register_UnityEngine_Input_INTERNAL_CALL_GetTouch();
		Register_UnityEngine_Input_INTERNAL_CALL_GetTouch();

		//System.Void UnityEngine.Input::INTERNAL_get_compositionCursorPos(UnityEngine.Vector2&)
		void Register_UnityEngine_Input_INTERNAL_get_compositionCursorPos();
		Register_UnityEngine_Input_INTERNAL_get_compositionCursorPos();

		//System.Void UnityEngine.Input::INTERNAL_get_mousePosition(UnityEngine.Vector3&)
		void Register_UnityEngine_Input_INTERNAL_get_mousePosition();
		Register_UnityEngine_Input_INTERNAL_get_mousePosition();

		//System.Void UnityEngine.Input::INTERNAL_get_mouseScrollDelta(UnityEngine.Vector2&)
		void Register_UnityEngine_Input_INTERNAL_get_mouseScrollDelta();
		Register_UnityEngine_Input_INTERNAL_get_mouseScrollDelta();

		//System.Void UnityEngine.Input::INTERNAL_set_compositionCursorPos(UnityEngine.Vector2&)
		void Register_UnityEngine_Input_INTERNAL_set_compositionCursorPos();
		Register_UnityEngine_Input_INTERNAL_set_compositionCursorPos();

		//System.Void UnityEngine.Input::set_imeCompositionMode(UnityEngine.IMECompositionMode)
		void Register_UnityEngine_Input_set_imeCompositionMode();
		Register_UnityEngine_Input_set_imeCompositionMode();

		//UnityEngine.IMECompositionMode UnityEngine.Input::get_imeCompositionMode()
		void Register_UnityEngine_Input_get_imeCompositionMode();
		Register_UnityEngine_Input_get_imeCompositionMode();

	//End Registrations for type : UnityEngine.Input

	//Start Registrations for type : UnityEngine.iOS.Device

		//System.Void UnityEngine.iOS.Device::SetNoBackupFlag(System.String)
		void Register_UnityEngine_iOS_Device_SetNoBackupFlag();
		Register_UnityEngine_iOS_Device_SetNoBackupFlag();

	//End Registrations for type : UnityEngine.iOS.Device

	//Start Registrations for type : UnityEngine.iOS.LocalNotification

		//System.Void UnityEngine.iOS.LocalNotification::Destroy()
		void Register_UnityEngine_iOS_LocalNotification_Destroy();
		Register_UnityEngine_iOS_LocalNotification_Destroy();

	//End Registrations for type : UnityEngine.iOS.LocalNotification

	//Start Registrations for type : UnityEngine.iOS.RemoteNotification

		//System.Void UnityEngine.iOS.RemoteNotification::Destroy()
		void Register_UnityEngine_iOS_RemoteNotification_Destroy();
		Register_UnityEngine_iOS_RemoteNotification_Destroy();

	//End Registrations for type : UnityEngine.iOS.RemoteNotification

	//Start Registrations for type : UnityEngine.JsonUtility

		//System.Object UnityEngine.JsonUtility::FromJson(System.String,System.Type)
		void Register_UnityEngine_JsonUtility_FromJson();
		Register_UnityEngine_JsonUtility_FromJson();

		//System.String UnityEngine.JsonUtility::ToJson(System.Object,System.Boolean)
		void Register_UnityEngine_JsonUtility_ToJson();
		Register_UnityEngine_JsonUtility_ToJson();

		//System.Void UnityEngine.JsonUtility::FromJsonOverwrite(System.String,System.Object)
		void Register_UnityEngine_JsonUtility_FromJsonOverwrite();
		Register_UnityEngine_JsonUtility_FromJsonOverwrite();

	//End Registrations for type : UnityEngine.JsonUtility

	//Start Registrations for type : UnityEngine.LayerMask

		//System.Int32 UnityEngine.LayerMask::NameToLayer(System.String)
		void Register_UnityEngine_LayerMask_NameToLayer();
		Register_UnityEngine_LayerMask_NameToLayer();

	//End Registrations for type : UnityEngine.LayerMask

	//Start Registrations for type : UnityEngine.Light

		//System.Void UnityEngine.Light::INTERNAL_get_color(UnityEngine.Color&)
		void Register_UnityEngine_Light_INTERNAL_get_color();
		Register_UnityEngine_Light_INTERNAL_get_color();

		//System.Void UnityEngine.Light::INTERNAL_set_color(UnityEngine.Color&)
		void Register_UnityEngine_Light_INTERNAL_set_color();
		Register_UnityEngine_Light_INTERNAL_set_color();

	//End Registrations for type : UnityEngine.Light

	//Start Registrations for type : UnityEngine.Material

		//System.Boolean UnityEngine.Material::HasProperty(System.Int32)
		void Register_UnityEngine_Material_HasProperty();
		Register_UnityEngine_Material_HasProperty();

		//System.Void UnityEngine.Material::DisableKeyword(System.String)
		void Register_UnityEngine_Material_DisableKeyword();
		Register_UnityEngine_Material_DisableKeyword();

		//System.Void UnityEngine.Material::EnableKeyword(System.String)
		void Register_UnityEngine_Material_EnableKeyword();
		Register_UnityEngine_Material_EnableKeyword();

		//System.Void UnityEngine.Material::INTERNAL_CALL_GetColorImpl(UnityEngine.Material,System.Int32,UnityEngine.Color&)
		void Register_UnityEngine_Material_INTERNAL_CALL_GetColorImpl();
		Register_UnityEngine_Material_INTERNAL_CALL_GetColorImpl();

		//System.Void UnityEngine.Material::INTERNAL_CALL_SetColorImpl(UnityEngine.Material,System.Int32,UnityEngine.Color&)
		void Register_UnityEngine_Material_INTERNAL_CALL_SetColorImpl();
		Register_UnityEngine_Material_INTERNAL_CALL_SetColorImpl();

		//System.Void UnityEngine.Material::INTERNAL_CALL_SetMatrixImpl(UnityEngine.Material,System.Int32,UnityEngine.Matrix4x4&)
		void Register_UnityEngine_Material_INTERNAL_CALL_SetMatrixImpl();
		Register_UnityEngine_Material_INTERNAL_CALL_SetMatrixImpl();

		//System.Void UnityEngine.Material::INTERNAL_CALL_SetVectorImpl(UnityEngine.Material,System.Int32,UnityEngine.Vector4&)
		void Register_UnityEngine_Material_INTERNAL_CALL_SetVectorImpl();
		Register_UnityEngine_Material_INTERNAL_CALL_SetVectorImpl();

		//System.Void UnityEngine.Material::Internal_CreateWithMaterial(UnityEngine.Material,UnityEngine.Material)
		void Register_UnityEngine_Material_Internal_CreateWithMaterial();
		Register_UnityEngine_Material_Internal_CreateWithMaterial();

		//System.Void UnityEngine.Material::Internal_CreateWithShader(UnityEngine.Material,UnityEngine.Shader)
		void Register_UnityEngine_Material_Internal_CreateWithShader();
		Register_UnityEngine_Material_Internal_CreateWithShader();

		//System.Void UnityEngine.Material::SetFloatImpl(System.Int32,System.Single)
		void Register_UnityEngine_Material_SetFloatImpl();
		Register_UnityEngine_Material_SetFloatImpl();

		//System.Void UnityEngine.Material::SetIntImpl(System.Int32,System.Int32)
		void Register_UnityEngine_Material_SetIntImpl();
		Register_UnityEngine_Material_SetIntImpl();

		//System.Void UnityEngine.Material::SetTextureImpl(System.Int32,UnityEngine.Texture)
		void Register_UnityEngine_Material_SetTextureImpl();
		Register_UnityEngine_Material_SetTextureImpl();

		//UnityEngine.Shader UnityEngine.Material::get_shader()
		void Register_UnityEngine_Material_get_shader();
		Register_UnityEngine_Material_get_shader();

		//UnityEngine.Texture UnityEngine.Material::GetTextureImpl(System.Int32)
		void Register_UnityEngine_Material_GetTextureImpl();
		Register_UnityEngine_Material_GetTextureImpl();

	//End Registrations for type : UnityEngine.Material

	//Start Registrations for type : UnityEngine.MaterialPropertyBlock

		//System.Void UnityEngine.MaterialPropertyBlock::DestroyBlock()
		void Register_UnityEngine_MaterialPropertyBlock_DestroyBlock();
		Register_UnityEngine_MaterialPropertyBlock_DestroyBlock();

		//System.Void UnityEngine.MaterialPropertyBlock::INTERNAL_CALL_SetVectorImpl(UnityEngine.MaterialPropertyBlock,System.Int32,UnityEngine.Vector4&)
		void Register_UnityEngine_MaterialPropertyBlock_INTERNAL_CALL_SetVectorImpl();
		Register_UnityEngine_MaterialPropertyBlock_INTERNAL_CALL_SetVectorImpl();

		//System.Void UnityEngine.MaterialPropertyBlock::InitBlock()
		void Register_UnityEngine_MaterialPropertyBlock_InitBlock();
		Register_UnityEngine_MaterialPropertyBlock_InitBlock();

		//System.Void UnityEngine.MaterialPropertyBlock::SetFloatImpl(System.Int32,System.Single)
		void Register_UnityEngine_MaterialPropertyBlock_SetFloatImpl();
		Register_UnityEngine_MaterialPropertyBlock_SetFloatImpl();

		//System.Void UnityEngine.MaterialPropertyBlock::SetTextureImpl(System.Int32,UnityEngine.Texture)
		void Register_UnityEngine_MaterialPropertyBlock_SetTextureImpl();
		Register_UnityEngine_MaterialPropertyBlock_SetTextureImpl();

	//End Registrations for type : UnityEngine.MaterialPropertyBlock

	//Start Registrations for type : UnityEngine.Mathf

		//System.Int32 UnityEngine.Mathf::ClosestPowerOfTwo(System.Int32)
		void Register_UnityEngine_Mathf_ClosestPowerOfTwo();
		Register_UnityEngine_Mathf_ClosestPowerOfTwo();

		//System.Single UnityEngine.Mathf::PerlinNoise(System.Single,System.Single)
		void Register_UnityEngine_Mathf_PerlinNoise();
		Register_UnityEngine_Mathf_PerlinNoise();

	//End Registrations for type : UnityEngine.Mathf

	//Start Registrations for type : UnityEngine.Matrix4x4

		//System.Void UnityEngine.Matrix4x4::INTERNAL_CALL_TRS(UnityEngine.Vector3&,UnityEngine.Quaternion&,UnityEngine.Vector3&,UnityEngine.Matrix4x4&)
		void Register_UnityEngine_Matrix4x4_INTERNAL_CALL_TRS();
		Register_UnityEngine_Matrix4x4_INTERNAL_CALL_TRS();

	//End Registrations for type : UnityEngine.Matrix4x4

	//Start Registrations for type : UnityEngine.Mesh

		//System.Array UnityEngine.Mesh::ExtractArrayFromList(System.Object)
		void Register_UnityEngine_Mesh_ExtractArrayFromList();
		Register_UnityEngine_Mesh_ExtractArrayFromList();

		//System.Array UnityEngine.Mesh::GetAllocArrayFromChannelImpl(UnityEngine.Mesh/InternalShaderChannel,UnityEngine.Mesh/InternalVertexChannelType,System.Int32)
		void Register_UnityEngine_Mesh_GetAllocArrayFromChannelImpl();
		Register_UnityEngine_Mesh_GetAllocArrayFromChannelImpl();

		//System.Boolean UnityEngine.Mesh::HasChannel(UnityEngine.Mesh/InternalShaderChannel)
		void Register_UnityEngine_Mesh_HasChannel();
		Register_UnityEngine_Mesh_HasChannel();

		//System.Boolean UnityEngine.Mesh::get_canAccess()
		void Register_UnityEngine_Mesh_get_canAccess();
		Register_UnityEngine_Mesh_get_canAccess();

		//System.Int32 UnityEngine.Mesh::get_subMeshCount()
		void Register_UnityEngine_Mesh_get_subMeshCount();
		Register_UnityEngine_Mesh_get_subMeshCount();

		//System.Int32 UnityEngine.Mesh::get_vertexCount()
		void Register_UnityEngine_Mesh_get_vertexCount();
		Register_UnityEngine_Mesh_get_vertexCount();

		//System.Int32[] UnityEngine.Mesh::GetIndicesImpl(System.Int32)
		void Register_UnityEngine_Mesh_GetIndicesImpl();
		Register_UnityEngine_Mesh_GetIndicesImpl();

		//System.Void UnityEngine.Mesh::ClearImpl(System.Boolean)
		void Register_UnityEngine_Mesh_ClearImpl();
		Register_UnityEngine_Mesh_ClearImpl();

		//System.Void UnityEngine.Mesh::INTERNAL_get_bounds(UnityEngine.Bounds&)
		void Register_UnityEngine_Mesh_INTERNAL_get_bounds();
		Register_UnityEngine_Mesh_INTERNAL_get_bounds();

		//System.Void UnityEngine.Mesh::Internal_Create(UnityEngine.Mesh)
		void Register_UnityEngine_Mesh_Internal_Create();
		Register_UnityEngine_Mesh_Internal_Create();

		//System.Void UnityEngine.Mesh::MarkDynamicImpl()
		void Register_UnityEngine_Mesh_MarkDynamicImpl();
		Register_UnityEngine_Mesh_MarkDynamicImpl();

		//System.Void UnityEngine.Mesh::PrintErrorCantAccessChannel(UnityEngine.Mesh/InternalShaderChannel)
		void Register_UnityEngine_Mesh_PrintErrorCantAccessChannel();
		Register_UnityEngine_Mesh_PrintErrorCantAccessChannel();

		//System.Void UnityEngine.Mesh::RecalculateBoundsImpl()
		void Register_UnityEngine_Mesh_RecalculateBoundsImpl();
		Register_UnityEngine_Mesh_RecalculateBoundsImpl();

		//System.Void UnityEngine.Mesh::SetArrayForChannelImpl(UnityEngine.Mesh/InternalShaderChannel,UnityEngine.Mesh/InternalVertexChannelType,System.Int32,System.Array,System.Int32)
		void Register_UnityEngine_Mesh_SetArrayForChannelImpl();
		Register_UnityEngine_Mesh_SetArrayForChannelImpl();

		//System.Void UnityEngine.Mesh::SetTrianglesImpl(System.Int32,System.Array,System.Int32,System.Boolean)
		void Register_UnityEngine_Mesh_SetTrianglesImpl();
		Register_UnityEngine_Mesh_SetTrianglesImpl();

	//End Registrations for type : UnityEngine.Mesh

	//Start Registrations for type : UnityEngine.MeshFilter

		//System.Void UnityEngine.MeshFilter::set_mesh(UnityEngine.Mesh)
		void Register_UnityEngine_MeshFilter_set_mesh();
		Register_UnityEngine_MeshFilter_set_mesh();

	//End Registrations for type : UnityEngine.MeshFilter

	//Start Registrations for type : UnityEngine.MonoBehaviour

		//System.Void UnityEngine.MonoBehaviour::Internal_CancelInvokeAll()
		void Register_UnityEngine_MonoBehaviour_Internal_CancelInvokeAll();
		Register_UnityEngine_MonoBehaviour_Internal_CancelInvokeAll();

		//System.Void UnityEngine.MonoBehaviour::InvokeRepeating(System.String,System.Single,System.Single)
		void Register_UnityEngine_MonoBehaviour_InvokeRepeating();
		Register_UnityEngine_MonoBehaviour_InvokeRepeating();

		//System.Void UnityEngine.MonoBehaviour::StopAllCoroutines()
		void Register_UnityEngine_MonoBehaviour_StopAllCoroutines();
		Register_UnityEngine_MonoBehaviour_StopAllCoroutines();

		//System.Void UnityEngine.MonoBehaviour::StopCoroutine(System.String)
		void Register_UnityEngine_MonoBehaviour_StopCoroutine();
		Register_UnityEngine_MonoBehaviour_StopCoroutine();

		//System.Void UnityEngine.MonoBehaviour::StopCoroutineViaEnumerator_Auto(System.Collections.IEnumerator)
		void Register_UnityEngine_MonoBehaviour_StopCoroutineViaEnumerator_Auto();
		Register_UnityEngine_MonoBehaviour_StopCoroutineViaEnumerator_Auto();

		//System.Void UnityEngine.MonoBehaviour::StopCoroutine_Auto(UnityEngine.Coroutine)
		void Register_UnityEngine_MonoBehaviour_StopCoroutine_Auto();
		Register_UnityEngine_MonoBehaviour_StopCoroutine_Auto();

		//UnityEngine.Coroutine UnityEngine.MonoBehaviour::StartCoroutine(System.String,System.Object)
		void Register_UnityEngine_MonoBehaviour_StartCoroutine();
		Register_UnityEngine_MonoBehaviour_StartCoroutine();

		//UnityEngine.Coroutine UnityEngine.MonoBehaviour::StartCoroutine_Auto_Internal(System.Collections.IEnumerator)
		void Register_UnityEngine_MonoBehaviour_StartCoroutine_Auto_Internal();
		Register_UnityEngine_MonoBehaviour_StartCoroutine_Auto_Internal();

	//End Registrations for type : UnityEngine.MonoBehaviour

	//Start Registrations for type : UnityEngine.Networking.DownloadHandler

		//System.Void UnityEngine.Networking.DownloadHandler::INTERNAL_CALL_InternalCreateAssetBundleCached(UnityEngine.Networking.DownloadHandler,System.String,System.String,UnityEngine.Hash128&,System.UInt32)
		void Register_UnityEngine_Networking_DownloadHandler_INTERNAL_CALL_InternalCreateAssetBundleCached();
		Register_UnityEngine_Networking_DownloadHandler_INTERNAL_CALL_InternalCreateAssetBundleCached();

		//System.Void UnityEngine.Networking.DownloadHandler::InternalCreateBuffer()
		void Register_UnityEngine_Networking_DownloadHandler_InternalCreateBuffer();
		Register_UnityEngine_Networking_DownloadHandler_InternalCreateBuffer();

		//System.Void UnityEngine.Networking.DownloadHandler::InternalDestroy()
		void Register_UnityEngine_Networking_DownloadHandler_InternalDestroy();
		Register_UnityEngine_Networking_DownloadHandler_InternalDestroy();

	//End Registrations for type : UnityEngine.Networking.DownloadHandler

	//Start Registrations for type : UnityEngine.Networking.DownloadHandlerAssetBundle

		//UnityEngine.AssetBundle UnityEngine.Networking.DownloadHandlerAssetBundle::get_assetBundle()
		void Register_UnityEngine_Networking_DownloadHandlerAssetBundle_get_assetBundle();
		Register_UnityEngine_Networking_DownloadHandlerAssetBundle_get_assetBundle();

	//End Registrations for type : UnityEngine.Networking.DownloadHandlerAssetBundle

	//Start Registrations for type : UnityEngine.Networking.DownloadHandlerBuffer

		//System.Byte[] UnityEngine.Networking.DownloadHandlerBuffer::InternalGetData()
		void Register_UnityEngine_Networking_DownloadHandlerBuffer_InternalGetData();
		Register_UnityEngine_Networking_DownloadHandlerBuffer_InternalGetData();

	//End Registrations for type : UnityEngine.Networking.DownloadHandlerBuffer

	//Start Registrations for type : UnityEngine.Networking.UnityWebRequest

		//System.Boolean UnityEngine.Networking.UnityWebRequest::get_isDone()
		void Register_UnityEngine_Networking_UnityWebRequest_get_isDone();
		Register_UnityEngine_Networking_UnityWebRequest_get_isDone();

		//System.Boolean UnityEngine.Networking.UnityWebRequest::get_isNetworkError()
		void Register_UnityEngine_Networking_UnityWebRequest_get_isNetworkError();
		Register_UnityEngine_Networking_UnityWebRequest_get_isNetworkError();

		//System.Int64 UnityEngine.Networking.UnityWebRequest::get_responseCode()
		void Register_UnityEngine_Networking_UnityWebRequest_get_responseCode();
		Register_UnityEngine_Networking_UnityWebRequest_get_responseCode();

		//System.Single UnityEngine.Networking.UnityWebRequest::get_downloadProgress()
		void Register_UnityEngine_Networking_UnityWebRequest_get_downloadProgress();
		Register_UnityEngine_Networking_UnityWebRequest_get_downloadProgress();

		//System.String UnityEngine.Networking.UnityWebRequest::InternalGetUrl()
		void Register_UnityEngine_Networking_UnityWebRequest_InternalGetUrl();
		Register_UnityEngine_Networking_UnityWebRequest_InternalGetUrl();

		//System.String UnityEngine.Networking.UnityWebRequest::get_error()
		void Register_UnityEngine_Networking_UnityWebRequest_get_error();
		Register_UnityEngine_Networking_UnityWebRequest_get_error();

		//System.Void UnityEngine.Networking.UnityWebRequest::InternalCreate()
		void Register_UnityEngine_Networking_UnityWebRequest_InternalCreate();
		Register_UnityEngine_Networking_UnityWebRequest_InternalCreate();

		//System.Void UnityEngine.Networking.UnityWebRequest::InternalDestroy()
		void Register_UnityEngine_Networking_UnityWebRequest_InternalDestroy();
		Register_UnityEngine_Networking_UnityWebRequest_InternalDestroy();

		//System.Void UnityEngine.Networking.UnityWebRequest::InternalSetCustomMethod(System.String)
		void Register_UnityEngine_Networking_UnityWebRequest_InternalSetCustomMethod();
		Register_UnityEngine_Networking_UnityWebRequest_InternalSetCustomMethod();

		//System.Void UnityEngine.Networking.UnityWebRequest::InternalSetMethod(UnityEngine.Networking.UnityWebRequest/UnityWebRequestMethod)
		void Register_UnityEngine_Networking_UnityWebRequest_InternalSetMethod();
		Register_UnityEngine_Networking_UnityWebRequest_InternalSetMethod();

		//System.Void UnityEngine.Networking.UnityWebRequest::InternalSetUrl(System.String)
		void Register_UnityEngine_Networking_UnityWebRequest_InternalSetUrl();
		Register_UnityEngine_Networking_UnityWebRequest_InternalSetUrl();

		//System.Void UnityEngine.Networking.UnityWebRequest::set_downloadHandler(UnityEngine.Networking.DownloadHandler)
		void Register_UnityEngine_Networking_UnityWebRequest_set_downloadHandler();
		Register_UnityEngine_Networking_UnityWebRequest_set_downloadHandler();

		//System.Void UnityEngine.Networking.UnityWebRequest::set_uploadHandler(UnityEngine.Networking.UploadHandler)
		void Register_UnityEngine_Networking_UnityWebRequest_set_uploadHandler();
		Register_UnityEngine_Networking_UnityWebRequest_set_uploadHandler();

		//UnityEngine.Networking.DownloadHandler UnityEngine.Networking.UnityWebRequest::GetDownloadHandler()
		void Register_UnityEngine_Networking_UnityWebRequest_GetDownloadHandler();
		Register_UnityEngine_Networking_UnityWebRequest_GetDownloadHandler();

		//UnityEngine.Networking.DownloadHandler UnityEngine.Networking.UnityWebRequest::get_downloadHandler()
		void Register_UnityEngine_Networking_UnityWebRequest_get_downloadHandler();
		Register_UnityEngine_Networking_UnityWebRequest_get_downloadHandler();

		//UnityEngine.Networking.UnityWebRequestAsyncOperation UnityEngine.Networking.UnityWebRequest::InternalBegin()
		void Register_UnityEngine_Networking_UnityWebRequest_InternalBegin();
		Register_UnityEngine_Networking_UnityWebRequest_InternalBegin();

		//UnityEngine.Networking.UploadHandler UnityEngine.Networking.UnityWebRequest::GetUploadHandler()
		void Register_UnityEngine_Networking_UnityWebRequest_GetUploadHandler();
		Register_UnityEngine_Networking_UnityWebRequest_GetUploadHandler();

	//End Registrations for type : UnityEngine.Networking.UnityWebRequest

	//Start Registrations for type : UnityEngine.Networking.UploadHandler

		//System.Void UnityEngine.Networking.UploadHandler::InternalDestroy()
		void Register_UnityEngine_Networking_UploadHandler_InternalDestroy();
		Register_UnityEngine_Networking_UploadHandler_InternalDestroy();

	//End Registrations for type : UnityEngine.Networking.UploadHandler

	//Start Registrations for type : UnityEngine.Object

		//System.Int32 UnityEngine.Object::GetOffsetOfInstanceIDInCPlusPlusObject()
		void Register_UnityEngine_Object_GetOffsetOfInstanceIDInCPlusPlusObject();
		Register_UnityEngine_Object_GetOffsetOfInstanceIDInCPlusPlusObject();

		//System.String UnityEngine.Object::ToString()
		void Register_UnityEngine_Object_ToString();
		Register_UnityEngine_Object_ToString();

		//System.String UnityEngine.Object::get_name()
		void Register_UnityEngine_Object_get_name();
		Register_UnityEngine_Object_get_name();

		//System.Void UnityEngine.Object::Destroy(UnityEngine.Object,System.Single)
		void Register_UnityEngine_Object_Destroy();
		Register_UnityEngine_Object_Destroy();

		//System.Void UnityEngine.Object::DestroyImmediate(UnityEngine.Object,System.Boolean)
		void Register_UnityEngine_Object_DestroyImmediate();
		Register_UnityEngine_Object_DestroyImmediate();

		//System.Void UnityEngine.Object::DontDestroyOnLoad(UnityEngine.Object)
		void Register_UnityEngine_Object_DontDestroyOnLoad();
		Register_UnityEngine_Object_DontDestroyOnLoad();

		//System.Void UnityEngine.Object::set_hideFlags(UnityEngine.HideFlags)
		void Register_UnityEngine_Object_set_hideFlags();
		Register_UnityEngine_Object_set_hideFlags();

		//System.Void UnityEngine.Object::set_name(System.String)
		void Register_UnityEngine_Object_set_name();
		Register_UnityEngine_Object_set_name();

		//UnityEngine.Object UnityEngine.Object::Internal_CloneSingle(UnityEngine.Object)
		void Register_UnityEngine_Object_Internal_CloneSingle();
		Register_UnityEngine_Object_Internal_CloneSingle();

		//UnityEngine.Object UnityEngine.Object::Internal_CloneSingleWithParent(UnityEngine.Object,UnityEngine.Transform,System.Boolean)
		void Register_UnityEngine_Object_Internal_CloneSingleWithParent();
		Register_UnityEngine_Object_Internal_CloneSingleWithParent();

		//UnityEngine.Object[] UnityEngine.Object::FindObjectsOfType(System.Type)
		void Register_UnityEngine_Object_FindObjectsOfType();
		Register_UnityEngine_Object_FindObjectsOfType();

	//End Registrations for type : UnityEngine.Object

	//Start Registrations for type : UnityEngine.ParticleSystem

		//System.Boolean UnityEngine.ParticleSystem::get_isPlaying()
		void Register_UnityEngine_ParticleSystem_get_isPlaying();
		Register_UnityEngine_ParticleSystem_get_isPlaying();

	//End Registrations for type : UnityEngine.ParticleSystem

	//Start Registrations for type : UnityEngine.ParticleSystem/ForceOverLifetimeModule

		//System.Boolean UnityEngine.ParticleSystem/ForceOverLifetimeModule::GetEnabled(UnityEngine.ParticleSystem)
		void Register_UnityEngine_ParticleSystem_ForceOverLifetimeModule_GetEnabled();
		Register_UnityEngine_ParticleSystem_ForceOverLifetimeModule_GetEnabled();

		//System.Void UnityEngine.ParticleSystem/ForceOverLifetimeModule::SetWorldSpace(UnityEngine.ParticleSystem,System.Boolean)
		void Register_UnityEngine_ParticleSystem_ForceOverLifetimeModule_SetWorldSpace();
		Register_UnityEngine_ParticleSystem_ForceOverLifetimeModule_SetWorldSpace();

	//End Registrations for type : UnityEngine.ParticleSystem/ForceOverLifetimeModule

	//Start Registrations for type : UnityEngine.ParticleSystem/MainModule

		//System.Void UnityEngine.ParticleSystem/MainModule::GetGravityModifier(UnityEngine.ParticleSystem,UnityEngine.ParticleSystem/MinMaxCurve&)
		void Register_UnityEngine_ParticleSystem_MainModule_GetGravityModifier();
		Register_UnityEngine_ParticleSystem_MainModule_GetGravityModifier();

		//System.Void UnityEngine.ParticleSystem/MainModule::GetStartColor(UnityEngine.ParticleSystem,UnityEngine.ParticleSystem/MinMaxGradient&)
		void Register_UnityEngine_ParticleSystem_MainModule_GetStartColor();
		Register_UnityEngine_ParticleSystem_MainModule_GetStartColor();

		//System.Void UnityEngine.ParticleSystem/MainModule::SetGravityModifier(UnityEngine.ParticleSystem,UnityEngine.ParticleSystem/MinMaxCurve&)
		void Register_UnityEngine_ParticleSystem_MainModule_SetGravityModifier();
		Register_UnityEngine_ParticleSystem_MainModule_SetGravityModifier();

		//System.Void UnityEngine.ParticleSystem/MainModule::SetScalingMode(UnityEngine.ParticleSystem,UnityEngine.ParticleSystemScalingMode)
		void Register_UnityEngine_ParticleSystem_MainModule_SetScalingMode();
		Register_UnityEngine_ParticleSystem_MainModule_SetScalingMode();

		//System.Void UnityEngine.ParticleSystem/MainModule::SetStartColor(UnityEngine.ParticleSystem,UnityEngine.ParticleSystem/MinMaxGradient&)
		void Register_UnityEngine_ParticleSystem_MainModule_SetStartColor();
		Register_UnityEngine_ParticleSystem_MainModule_SetStartColor();

	//End Registrations for type : UnityEngine.ParticleSystem/MainModule

	//Start Registrations for type : UnityEngine.ParticleSystem/VelocityOverLifetimeModule

		//System.Boolean UnityEngine.ParticleSystem/VelocityOverLifetimeModule::GetEnabled(UnityEngine.ParticleSystem)
		void Register_UnityEngine_ParticleSystem_VelocityOverLifetimeModule_GetEnabled();
		Register_UnityEngine_ParticleSystem_VelocityOverLifetimeModule_GetEnabled();

		//System.Void UnityEngine.ParticleSystem/VelocityOverLifetimeModule::SetWorldSpace(UnityEngine.ParticleSystem,System.Boolean)
		void Register_UnityEngine_ParticleSystem_VelocityOverLifetimeModule_SetWorldSpace();
		Register_UnityEngine_ParticleSystem_VelocityOverLifetimeModule_SetWorldSpace();

	//End Registrations for type : UnityEngine.ParticleSystem/VelocityOverLifetimeModule

	//Start Registrations for type : UnityEngine.ParticleSystemRenderer

		//System.Void UnityEngine.ParticleSystemRenderer::set_renderMode(UnityEngine.ParticleSystemRenderMode)
		void Register_UnityEngine_ParticleSystemRenderer_set_renderMode();
		Register_UnityEngine_ParticleSystemRenderer_set_renderMode();

		//UnityEngine.ParticleSystemRenderMode UnityEngine.ParticleSystemRenderer::get_renderMode()
		void Register_UnityEngine_ParticleSystemRenderer_get_renderMode();
		Register_UnityEngine_ParticleSystemRenderer_get_renderMode();

	//End Registrations for type : UnityEngine.ParticleSystemRenderer

	//Start Registrations for type : UnityEngine.Physics

		//System.Boolean UnityEngine.Physics::INTERNAL_CALL_Internal_Raycast(UnityEngine.Vector3&,UnityEngine.Vector3&,UnityEngine.RaycastHit&,System.Single,System.Int32,UnityEngine.QueryTriggerInteraction)
		void Register_UnityEngine_Physics_INTERNAL_CALL_Internal_Raycast();
		Register_UnityEngine_Physics_INTERNAL_CALL_Internal_Raycast();

		//System.Boolean UnityEngine.Physics::INTERNAL_CALL_Internal_RaycastTest(UnityEngine.Vector3&,UnityEngine.Vector3&,System.Single,System.Int32,UnityEngine.QueryTriggerInteraction)
		void Register_UnityEngine_Physics_INTERNAL_CALL_Internal_RaycastTest();
		Register_UnityEngine_Physics_INTERNAL_CALL_Internal_RaycastTest();

		//System.Void UnityEngine.Physics::INTERNAL_get_gravity(UnityEngine.Vector3&)
		void Register_UnityEngine_Physics_INTERNAL_get_gravity();
		Register_UnityEngine_Physics_INTERNAL_get_gravity();

		//UnityEngine.RaycastHit[] UnityEngine.Physics::INTERNAL_CALL_RaycastAll(UnityEngine.Vector3&,UnityEngine.Vector3&,System.Single,System.Int32,UnityEngine.QueryTriggerInteraction)
		void Register_UnityEngine_Physics_INTERNAL_CALL_RaycastAll();
		Register_UnityEngine_Physics_INTERNAL_CALL_RaycastAll();

	//End Registrations for type : UnityEngine.Physics

	//Start Registrations for type : UnityEngine.Physics2D

		//System.Boolean UnityEngine.Physics2D::get_queriesHitTriggers()
		void Register_UnityEngine_Physics2D_get_queriesHitTriggers();
		Register_UnityEngine_Physics2D_get_queriesHitTriggers();

		//System.Int32 UnityEngine.Physics2D::INTERNAL_CALL_Internal_RaycastNonAlloc(UnityEngine.Vector2&,UnityEngine.Vector2&,System.Single,UnityEngine.ContactFilter2D&,UnityEngine.RaycastHit2D[])
		void Register_UnityEngine_Physics2D_INTERNAL_CALL_Internal_RaycastNonAlloc();
		Register_UnityEngine_Physics2D_INTERNAL_CALL_Internal_RaycastNonAlloc();

		//System.Void UnityEngine.Physics2D::INTERNAL_CALL_Internal_Raycast(UnityEngine.Vector2&,UnityEngine.Vector2&,System.Single,UnityEngine.ContactFilter2D&,UnityEngine.RaycastHit2D&)
		void Register_UnityEngine_Physics2D_INTERNAL_CALL_Internal_Raycast();
		Register_UnityEngine_Physics2D_INTERNAL_CALL_Internal_Raycast();

		//UnityEngine.RaycastHit2D[] UnityEngine.Physics2D::INTERNAL_CALL_GetRayIntersectionAll(UnityEngine.Ray&,System.Single,System.Int32)
		void Register_UnityEngine_Physics2D_INTERNAL_CALL_GetRayIntersectionAll();
		Register_UnityEngine_Physics2D_INTERNAL_CALL_GetRayIntersectionAll();

	//End Registrations for type : UnityEngine.Physics2D

	//Start Registrations for type : UnityEngine.Playables.PlayableHandle

		//System.Boolean UnityEngine.Playables.PlayableHandle::INTERNAL_CALL_IsValidInternal(UnityEngine.Playables.PlayableHandle&)
		void Register_UnityEngine_Playables_PlayableHandle_INTERNAL_CALL_IsValidInternal();
		Register_UnityEngine_Playables_PlayableHandle_INTERNAL_CALL_IsValidInternal();

		//System.Type UnityEngine.Playables.PlayableHandle::INTERNAL_CALL_GetPlayableTypeOf(UnityEngine.Playables.PlayableHandle&)
		void Register_UnityEngine_Playables_PlayableHandle_INTERNAL_CALL_GetPlayableTypeOf();
		Register_UnityEngine_Playables_PlayableHandle_INTERNAL_CALL_GetPlayableTypeOf();

	//End Registrations for type : UnityEngine.Playables.PlayableHandle

	//Start Registrations for type : UnityEngine.PlayerPrefs

		//System.Void UnityEngine.PlayerPrefs::DeleteAll()
		void Register_UnityEngine_PlayerPrefs_DeleteAll();
		Register_UnityEngine_PlayerPrefs_DeleteAll();

	//End Registrations for type : UnityEngine.PlayerPrefs

	//Start Registrations for type : UnityEngine.Profiling.Profiler

		//System.Int64 UnityEngine.Profiling.Profiler::GetMonoHeapSizeLong()
		void Register_UnityEngine_Profiling_Profiler_GetMonoHeapSizeLong();
		Register_UnityEngine_Profiling_Profiler_GetMonoHeapSizeLong();

		//System.Int64 UnityEngine.Profiling.Profiler::GetMonoUsedSizeLong()
		void Register_UnityEngine_Profiling_Profiler_GetMonoUsedSizeLong();
		Register_UnityEngine_Profiling_Profiler_GetMonoUsedSizeLong();

		//System.Int64 UnityEngine.Profiling.Profiler::get_usedHeapSizeLong()
		void Register_UnityEngine_Profiling_Profiler_get_usedHeapSizeLong();
		Register_UnityEngine_Profiling_Profiler_get_usedHeapSizeLong();

	//End Registrations for type : UnityEngine.Profiling.Profiler

	//Start Registrations for type : UnityEngine.PropertyNameUtils

		//System.Void UnityEngine.PropertyNameUtils::PropertyNameFromString_Injected(System.String,UnityEngine.PropertyName&)
		void Register_UnityEngine_PropertyNameUtils_PropertyNameFromString_Injected();
		Register_UnityEngine_PropertyNameUtils_PropertyNameFromString_Injected();

	//End Registrations for type : UnityEngine.PropertyNameUtils

	//Start Registrations for type : UnityEngine.QualitySettings

		//UnityEngine.ColorSpace UnityEngine.QualitySettings::get_activeColorSpace()
		void Register_UnityEngine_QualitySettings_get_activeColorSpace();
		Register_UnityEngine_QualitySettings_get_activeColorSpace();

	//End Registrations for type : UnityEngine.QualitySettings

	//Start Registrations for type : UnityEngine.Quaternion

		//System.Void UnityEngine.Quaternion::INTERNAL_CALL_FromToRotation(UnityEngine.Vector3&,UnityEngine.Vector3&,UnityEngine.Quaternion&)
		void Register_UnityEngine_Quaternion_INTERNAL_CALL_FromToRotation();
		Register_UnityEngine_Quaternion_INTERNAL_CALL_FromToRotation();

		//System.Void UnityEngine.Quaternion::INTERNAL_CALL_Internal_FromEulerRad(UnityEngine.Vector3&,UnityEngine.Quaternion&)
		void Register_UnityEngine_Quaternion_INTERNAL_CALL_Internal_FromEulerRad();
		Register_UnityEngine_Quaternion_INTERNAL_CALL_Internal_FromEulerRad();

		//System.Void UnityEngine.Quaternion::INTERNAL_CALL_Internal_ToEulerRad(UnityEngine.Quaternion&,UnityEngine.Vector3&)
		void Register_UnityEngine_Quaternion_INTERNAL_CALL_Internal_ToEulerRad();
		Register_UnityEngine_Quaternion_INTERNAL_CALL_Internal_ToEulerRad();

		//System.Void UnityEngine.Quaternion::INTERNAL_CALL_Inverse(UnityEngine.Quaternion&,UnityEngine.Quaternion&)
		void Register_UnityEngine_Quaternion_INTERNAL_CALL_Inverse();
		Register_UnityEngine_Quaternion_INTERNAL_CALL_Inverse();

		//System.Void UnityEngine.Quaternion::INTERNAL_CALL_Lerp(UnityEngine.Quaternion&,UnityEngine.Quaternion&,System.Single,UnityEngine.Quaternion&)
		void Register_UnityEngine_Quaternion_INTERNAL_CALL_Lerp();
		Register_UnityEngine_Quaternion_INTERNAL_CALL_Lerp();

		//System.Void UnityEngine.Quaternion::INTERNAL_CALL_LookRotation(UnityEngine.Vector3&,UnityEngine.Vector3&,UnityEngine.Quaternion&)
		void Register_UnityEngine_Quaternion_INTERNAL_CALL_LookRotation();
		Register_UnityEngine_Quaternion_INTERNAL_CALL_LookRotation();

	//End Registrations for type : UnityEngine.Quaternion

	//Start Registrations for type : UnityEngine.Random

		//System.Int32 UnityEngine.Random::RandomRangeInt(System.Int32,System.Int32)
		void Register_UnityEngine_Random_RandomRangeInt();
		Register_UnityEngine_Random_RandomRangeInt();

		//System.Single UnityEngine.Random::Range(System.Single,System.Single)
		void Register_UnityEngine_Random_Range();
		Register_UnityEngine_Random_Range();

		//System.Single UnityEngine.Random::get_value()
		void Register_UnityEngine_Random_get_value();
		Register_UnityEngine_Random_get_value();

	//End Registrations for type : UnityEngine.Random

	//Start Registrations for type : UnityEngine.RectOffset

		//System.Int32 UnityEngine.RectOffset::get_bottom()
		void Register_UnityEngine_RectOffset_get_bottom();
		Register_UnityEngine_RectOffset_get_bottom();

		//System.Int32 UnityEngine.RectOffset::get_horizontal()
		void Register_UnityEngine_RectOffset_get_horizontal();
		Register_UnityEngine_RectOffset_get_horizontal();

		//System.Int32 UnityEngine.RectOffset::get_left()
		void Register_UnityEngine_RectOffset_get_left();
		Register_UnityEngine_RectOffset_get_left();

		//System.Int32 UnityEngine.RectOffset::get_right()
		void Register_UnityEngine_RectOffset_get_right();
		Register_UnityEngine_RectOffset_get_right();

		//System.Int32 UnityEngine.RectOffset::get_top()
		void Register_UnityEngine_RectOffset_get_top();
		Register_UnityEngine_RectOffset_get_top();

		//System.Int32 UnityEngine.RectOffset::get_vertical()
		void Register_UnityEngine_RectOffset_get_vertical();
		Register_UnityEngine_RectOffset_get_vertical();

		//System.Void UnityEngine.RectOffset::Cleanup()
		void Register_UnityEngine_RectOffset_Cleanup();
		Register_UnityEngine_RectOffset_Cleanup();

		//System.Void UnityEngine.RectOffset::Init()
		void Register_UnityEngine_RectOffset_Init();
		Register_UnityEngine_RectOffset_Init();

		//System.Void UnityEngine.RectOffset::set_bottom(System.Int32)
		void Register_UnityEngine_RectOffset_set_bottom();
		Register_UnityEngine_RectOffset_set_bottom();

		//System.Void UnityEngine.RectOffset::set_left(System.Int32)
		void Register_UnityEngine_RectOffset_set_left();
		Register_UnityEngine_RectOffset_set_left();

		//System.Void UnityEngine.RectOffset::set_right(System.Int32)
		void Register_UnityEngine_RectOffset_set_right();
		Register_UnityEngine_RectOffset_set_right();

		//System.Void UnityEngine.RectOffset::set_top(System.Int32)
		void Register_UnityEngine_RectOffset_set_top();
		Register_UnityEngine_RectOffset_set_top();

	//End Registrations for type : UnityEngine.RectOffset

	//Start Registrations for type : UnityEngine.RectTransform

		//System.Void UnityEngine.RectTransform::INTERNAL_get_anchorMax(UnityEngine.Vector2&)
		void Register_UnityEngine_RectTransform_INTERNAL_get_anchorMax();
		Register_UnityEngine_RectTransform_INTERNAL_get_anchorMax();

		//System.Void UnityEngine.RectTransform::INTERNAL_get_anchorMin(UnityEngine.Vector2&)
		void Register_UnityEngine_RectTransform_INTERNAL_get_anchorMin();
		Register_UnityEngine_RectTransform_INTERNAL_get_anchorMin();

		//System.Void UnityEngine.RectTransform::INTERNAL_get_anchoredPosition(UnityEngine.Vector2&)
		void Register_UnityEngine_RectTransform_INTERNAL_get_anchoredPosition();
		Register_UnityEngine_RectTransform_INTERNAL_get_anchoredPosition();

		//System.Void UnityEngine.RectTransform::INTERNAL_get_pivot(UnityEngine.Vector2&)
		void Register_UnityEngine_RectTransform_INTERNAL_get_pivot();
		Register_UnityEngine_RectTransform_INTERNAL_get_pivot();

		//System.Void UnityEngine.RectTransform::INTERNAL_get_rect(UnityEngine.Rect&)
		void Register_UnityEngine_RectTransform_INTERNAL_get_rect();
		Register_UnityEngine_RectTransform_INTERNAL_get_rect();

		//System.Void UnityEngine.RectTransform::INTERNAL_get_sizeDelta(UnityEngine.Vector2&)
		void Register_UnityEngine_RectTransform_INTERNAL_get_sizeDelta();
		Register_UnityEngine_RectTransform_INTERNAL_get_sizeDelta();

		//System.Void UnityEngine.RectTransform::INTERNAL_set_anchorMax(UnityEngine.Vector2&)
		void Register_UnityEngine_RectTransform_INTERNAL_set_anchorMax();
		Register_UnityEngine_RectTransform_INTERNAL_set_anchorMax();

		//System.Void UnityEngine.RectTransform::INTERNAL_set_anchorMin(UnityEngine.Vector2&)
		void Register_UnityEngine_RectTransform_INTERNAL_set_anchorMin();
		Register_UnityEngine_RectTransform_INTERNAL_set_anchorMin();

		//System.Void UnityEngine.RectTransform::INTERNAL_set_anchoredPosition(UnityEngine.Vector2&)
		void Register_UnityEngine_RectTransform_INTERNAL_set_anchoredPosition();
		Register_UnityEngine_RectTransform_INTERNAL_set_anchoredPosition();

		//System.Void UnityEngine.RectTransform::INTERNAL_set_pivot(UnityEngine.Vector2&)
		void Register_UnityEngine_RectTransform_INTERNAL_set_pivot();
		Register_UnityEngine_RectTransform_INTERNAL_set_pivot();

		//System.Void UnityEngine.RectTransform::INTERNAL_set_sizeDelta(UnityEngine.Vector2&)
		void Register_UnityEngine_RectTransform_INTERNAL_set_sizeDelta();
		Register_UnityEngine_RectTransform_INTERNAL_set_sizeDelta();

	//End Registrations for type : UnityEngine.RectTransform

	//Start Registrations for type : UnityEngine.RectTransformUtility

		//System.Boolean UnityEngine.RectTransformUtility::INTERNAL_CALL_RectangleContainsScreenPoint(UnityEngine.RectTransform,UnityEngine.Vector2&,UnityEngine.Camera)
		void Register_UnityEngine_RectTransformUtility_INTERNAL_CALL_RectangleContainsScreenPoint();
		Register_UnityEngine_RectTransformUtility_INTERNAL_CALL_RectangleContainsScreenPoint();

		//System.Void UnityEngine.RectTransformUtility::INTERNAL_CALL_PixelAdjustPoint(UnityEngine.Vector2&,UnityEngine.Transform,UnityEngine.Canvas,UnityEngine.Vector2&)
		void Register_UnityEngine_RectTransformUtility_INTERNAL_CALL_PixelAdjustPoint();
		Register_UnityEngine_RectTransformUtility_INTERNAL_CALL_PixelAdjustPoint();

		//System.Void UnityEngine.RectTransformUtility::INTERNAL_CALL_PixelAdjustRect(UnityEngine.RectTransform,UnityEngine.Canvas,UnityEngine.Rect&)
		void Register_UnityEngine_RectTransformUtility_INTERNAL_CALL_PixelAdjustRect();
		Register_UnityEngine_RectTransformUtility_INTERNAL_CALL_PixelAdjustRect();

	//End Registrations for type : UnityEngine.RectTransformUtility

	//Start Registrations for type : UnityEngine.Renderer

		//System.Boolean UnityEngine.Renderer::get_enabled()
		void Register_UnityEngine_Renderer_get_enabled();
		Register_UnityEngine_Renderer_get_enabled();

		//System.Int32 UnityEngine.Renderer::get_sortingLayerID()
		void Register_UnityEngine_Renderer_get_sortingLayerID();
		Register_UnityEngine_Renderer_get_sortingLayerID();

		//System.Int32 UnityEngine.Renderer::get_sortingOrder()
		void Register_UnityEngine_Renderer_get_sortingOrder();
		Register_UnityEngine_Renderer_get_sortingOrder();

		//System.String UnityEngine.Renderer::get_sortingLayerName()
		void Register_UnityEngine_Renderer_get_sortingLayerName();
		Register_UnityEngine_Renderer_get_sortingLayerName();

		//System.Void UnityEngine.Renderer::GetPropertyBlock(UnityEngine.MaterialPropertyBlock)
		void Register_UnityEngine_Renderer_GetPropertyBlock();
		Register_UnityEngine_Renderer_GetPropertyBlock();

		//System.Void UnityEngine.Renderer::INTERNAL_get_bounds(UnityEngine.Bounds&)
		void Register_UnityEngine_Renderer_INTERNAL_get_bounds();
		Register_UnityEngine_Renderer_INTERNAL_get_bounds();

		//System.Void UnityEngine.Renderer::SetPropertyBlock(UnityEngine.MaterialPropertyBlock)
		void Register_UnityEngine_Renderer_SetPropertyBlock();
		Register_UnityEngine_Renderer_SetPropertyBlock();

		//System.Void UnityEngine.Renderer::set_enabled(System.Boolean)
		void Register_UnityEngine_Renderer_set_enabled();
		Register_UnityEngine_Renderer_set_enabled();

		//System.Void UnityEngine.Renderer::set_lightProbeUsage(UnityEngine.Rendering.LightProbeUsage)
		void Register_UnityEngine_Renderer_set_lightProbeUsage();
		Register_UnityEngine_Renderer_set_lightProbeUsage();

		//System.Void UnityEngine.Renderer::set_material(UnityEngine.Material)
		void Register_UnityEngine_Renderer_set_material();
		Register_UnityEngine_Renderer_set_material();

		//System.Void UnityEngine.Renderer::set_receiveShadows(System.Boolean)
		void Register_UnityEngine_Renderer_set_receiveShadows();
		Register_UnityEngine_Renderer_set_receiveShadows();

		//System.Void UnityEngine.Renderer::set_shadowCastingMode(UnityEngine.Rendering.ShadowCastingMode)
		void Register_UnityEngine_Renderer_set_shadowCastingMode();
		Register_UnityEngine_Renderer_set_shadowCastingMode();

		//System.Void UnityEngine.Renderer::set_sortingLayerID(System.Int32)
		void Register_UnityEngine_Renderer_set_sortingLayerID();
		Register_UnityEngine_Renderer_set_sortingLayerID();

		//System.Void UnityEngine.Renderer::set_sortingLayerName(System.String)
		void Register_UnityEngine_Renderer_set_sortingLayerName();
		Register_UnityEngine_Renderer_set_sortingLayerName();

		//System.Void UnityEngine.Renderer::set_sortingOrder(System.Int32)
		void Register_UnityEngine_Renderer_set_sortingOrder();
		Register_UnityEngine_Renderer_set_sortingOrder();

		//UnityEngine.Material UnityEngine.Renderer::get_material()
		void Register_UnityEngine_Renderer_get_material();
		Register_UnityEngine_Renderer_get_material();

		//UnityEngine.Material UnityEngine.Renderer::get_sharedMaterial()
		void Register_UnityEngine_Renderer_get_sharedMaterial();
		Register_UnityEngine_Renderer_get_sharedMaterial();

		//UnityEngine.Material[] UnityEngine.Renderer::get_materials()
		void Register_UnityEngine_Renderer_get_materials();
		Register_UnityEngine_Renderer_get_materials();

	//End Registrations for type : UnityEngine.Renderer

	//Start Registrations for type : UnityEngine.Rendering.CommandBuffer

		//System.Void UnityEngine.Rendering.CommandBuffer::Clear()
		void Register_UnityEngine_Rendering_CommandBuffer_Clear();
		Register_UnityEngine_Rendering_CommandBuffer_Clear();

		//System.Void UnityEngine.Rendering.CommandBuffer::INTERNAL_CALL_ClearRenderTarget(UnityEngine.Rendering.CommandBuffer,System.Boolean,System.Boolean,UnityEngine.Color&,System.Single)
		void Register_UnityEngine_Rendering_CommandBuffer_INTERNAL_CALL_ClearRenderTarget();
		Register_UnityEngine_Rendering_CommandBuffer_INTERNAL_CALL_ClearRenderTarget();

		//System.Void UnityEngine.Rendering.CommandBuffer::INTERNAL_CALL_Internal_DrawMesh(UnityEngine.Rendering.CommandBuffer,UnityEngine.Mesh,UnityEngine.Matrix4x4&,UnityEngine.Material,System.Int32,System.Int32,UnityEngine.MaterialPropertyBlock)
		void Register_UnityEngine_Rendering_CommandBuffer_INTERNAL_CALL_Internal_DrawMesh();
		Register_UnityEngine_Rendering_CommandBuffer_INTERNAL_CALL_Internal_DrawMesh();

		//System.Void UnityEngine.Rendering.CommandBuffer::InitBuffer(UnityEngine.Rendering.CommandBuffer)
		void Register_UnityEngine_Rendering_CommandBuffer_InitBuffer();
		Register_UnityEngine_Rendering_CommandBuffer_InitBuffer();

		//System.Void UnityEngine.Rendering.CommandBuffer::ReleaseBuffer()
		void Register_UnityEngine_Rendering_CommandBuffer_ReleaseBuffer();
		Register_UnityEngine_Rendering_CommandBuffer_ReleaseBuffer();

		//System.Void UnityEngine.Rendering.CommandBuffer::SetRenderTarget_Single(UnityEngine.Rendering.RenderTargetIdentifier&,System.Int32,UnityEngine.CubemapFace,System.Int32)
		void Register_UnityEngine_Rendering_CommandBuffer_SetRenderTarget_Single();
		Register_UnityEngine_Rendering_CommandBuffer_SetRenderTarget_Single();

		//System.Void UnityEngine.Rendering.CommandBuffer::set_name(System.String)
		void Register_UnityEngine_Rendering_CommandBuffer_set_name();
		Register_UnityEngine_Rendering_CommandBuffer_set_name();

	//End Registrations for type : UnityEngine.Rendering.CommandBuffer

	//Start Registrations for type : UnityEngine.Rendering.SplashScreen

		//System.Boolean UnityEngine.Rendering.SplashScreen::get_isFinished()
		void Register_UnityEngine_Rendering_SplashScreen_get_isFinished();
		Register_UnityEngine_Rendering_SplashScreen_get_isFinished();

	//End Registrations for type : UnityEngine.Rendering.SplashScreen

	//Start Registrations for type : UnityEngine.RenderTexture

		//System.Boolean UnityEngine.RenderTexture::INTERNAL_CALL_Create(UnityEngine.RenderTexture)
		void Register_UnityEngine_RenderTexture_INTERNAL_CALL_Create();
		Register_UnityEngine_RenderTexture_INTERNAL_CALL_Create();

		//System.Boolean UnityEngine.RenderTexture::INTERNAL_CALL_IsCreated(UnityEngine.RenderTexture)
		void Register_UnityEngine_RenderTexture_INTERNAL_CALL_IsCreated();
		Register_UnityEngine_RenderTexture_INTERNAL_CALL_IsCreated();

		//System.Int32 UnityEngine.RenderTexture::Internal_GetHeight(UnityEngine.RenderTexture)
		void Register_UnityEngine_RenderTexture_Internal_GetHeight();
		Register_UnityEngine_RenderTexture_Internal_GetHeight();

		//System.Int32 UnityEngine.RenderTexture::Internal_GetWidth(UnityEngine.RenderTexture)
		void Register_UnityEngine_RenderTexture_Internal_GetWidth();
		Register_UnityEngine_RenderTexture_Internal_GetWidth();

		//System.Int32 UnityEngine.RenderTexture::get_depth()
		void Register_UnityEngine_RenderTexture_get_depth();
		Register_UnityEngine_RenderTexture_get_depth();

		//System.Void UnityEngine.RenderTexture::GetColorBuffer(UnityEngine.RenderBuffer&)
		void Register_UnityEngine_RenderTexture_GetColorBuffer();
		Register_UnityEngine_RenderTexture_GetColorBuffer();

		//System.Void UnityEngine.RenderTexture::INTERNAL_CALL_MarkRestoreExpected(UnityEngine.RenderTexture)
		void Register_UnityEngine_RenderTexture_INTERNAL_CALL_MarkRestoreExpected();
		Register_UnityEngine_RenderTexture_INTERNAL_CALL_MarkRestoreExpected();

		//System.Void UnityEngine.RenderTexture::INTERNAL_CALL_Release(UnityEngine.RenderTexture)
		void Register_UnityEngine_RenderTexture_INTERNAL_CALL_Release();
		Register_UnityEngine_RenderTexture_INTERNAL_CALL_Release();

		//System.Void UnityEngine.RenderTexture::Internal_CreateRenderTexture(UnityEngine.RenderTexture)
		void Register_UnityEngine_RenderTexture_Internal_CreateRenderTexture();
		Register_UnityEngine_RenderTexture_Internal_CreateRenderTexture();

		//System.Void UnityEngine.RenderTexture::Internal_SetHeight(UnityEngine.RenderTexture,System.Int32)
		void Register_UnityEngine_RenderTexture_Internal_SetHeight();
		Register_UnityEngine_RenderTexture_Internal_SetHeight();

		//System.Void UnityEngine.RenderTexture::Internal_SetSRGBReadWrite(UnityEngine.RenderTexture,System.Boolean)
		void Register_UnityEngine_RenderTexture_Internal_SetSRGBReadWrite();
		Register_UnityEngine_RenderTexture_Internal_SetSRGBReadWrite();

		//System.Void UnityEngine.RenderTexture::Internal_SetWidth(UnityEngine.RenderTexture,System.Int32)
		void Register_UnityEngine_RenderTexture_Internal_SetWidth();
		Register_UnityEngine_RenderTexture_Internal_SetWidth();

		//System.Void UnityEngine.RenderTexture::ReleaseTemporary(UnityEngine.RenderTexture)
		void Register_UnityEngine_RenderTexture_ReleaseTemporary();
		Register_UnityEngine_RenderTexture_ReleaseTemporary();

		//System.Void UnityEngine.RenderTexture::set_active(UnityEngine.RenderTexture)
		void Register_UnityEngine_RenderTexture_set_active();
		Register_UnityEngine_RenderTexture_set_active();

		//System.Void UnityEngine.RenderTexture::set_depth(System.Int32)
		void Register_UnityEngine_RenderTexture_set_depth();
		Register_UnityEngine_RenderTexture_set_depth();

		//System.Void UnityEngine.RenderTexture::set_format(UnityEngine.RenderTextureFormat)
		void Register_UnityEngine_RenderTexture_set_format();
		Register_UnityEngine_RenderTexture_set_format();

		//UnityEngine.RenderTexture UnityEngine.RenderTexture::INTERNAL_CALL_GetTemporary_Internal(UnityEngine.RenderTextureDescriptor&)
		void Register_UnityEngine_RenderTexture_INTERNAL_CALL_GetTemporary_Internal();
		Register_UnityEngine_RenderTexture_INTERNAL_CALL_GetTemporary_Internal();

		//UnityEngine.RenderTexture UnityEngine.RenderTexture::get_active()
		void Register_UnityEngine_RenderTexture_get_active();
		Register_UnityEngine_RenderTexture_get_active();

		//UnityEngine.RenderTextureFormat UnityEngine.RenderTexture::get_format()
		void Register_UnityEngine_RenderTexture_get_format();
		Register_UnityEngine_RenderTexture_get_format();

	//End Registrations for type : UnityEngine.RenderTexture

	//Start Registrations for type : UnityEngine.Resources

		//System.Void UnityEngine.Resources::UnloadAsset(UnityEngine.Object)
		void Register_UnityEngine_Resources_UnloadAsset();
		Register_UnityEngine_Resources_UnloadAsset();

		//UnityEngine.AsyncOperation UnityEngine.Resources::UnloadUnusedAssets()
		void Register_UnityEngine_Resources_UnloadUnusedAssets();
		Register_UnityEngine_Resources_UnloadUnusedAssets();

		//UnityEngine.Object UnityEngine.Resources::GetBuiltinResource(System.Type,System.String)
		void Register_UnityEngine_Resources_GetBuiltinResource();
		Register_UnityEngine_Resources_GetBuiltinResource();

		//UnityEngine.Object UnityEngine.Resources::Load(System.String,System.Type)
		void Register_UnityEngine_Resources_Load();
		Register_UnityEngine_Resources_Load();

		//UnityEngine.ResourceRequest UnityEngine.Resources::LoadAsyncInternal(System.String,System.Type)
		void Register_UnityEngine_Resources_LoadAsyncInternal();
		Register_UnityEngine_Resources_LoadAsyncInternal();

	//End Registrations for type : UnityEngine.Resources

	//Start Registrations for type : UnityEngine.Rigidbody

		//System.Void UnityEngine.Rigidbody::INTERNAL_CALL_AddForce(UnityEngine.Rigidbody,UnityEngine.Vector3&,UnityEngine.ForceMode)
		void Register_UnityEngine_Rigidbody_INTERNAL_CALL_AddForce();
		Register_UnityEngine_Rigidbody_INTERNAL_CALL_AddForce();

		//System.Void UnityEngine.Rigidbody::INTERNAL_CALL_MovePosition(UnityEngine.Rigidbody,UnityEngine.Vector3&)
		void Register_UnityEngine_Rigidbody_INTERNAL_CALL_MovePosition();
		Register_UnityEngine_Rigidbody_INTERNAL_CALL_MovePosition();

		//System.Void UnityEngine.Rigidbody::INTERNAL_CALL_MoveRotation(UnityEngine.Rigidbody,UnityEngine.Quaternion&)
		void Register_UnityEngine_Rigidbody_INTERNAL_CALL_MoveRotation();
		Register_UnityEngine_Rigidbody_INTERNAL_CALL_MoveRotation();

		//System.Void UnityEngine.Rigidbody::set_useGravity(System.Boolean)
		void Register_UnityEngine_Rigidbody_set_useGravity();
		Register_UnityEngine_Rigidbody_set_useGravity();

	//End Registrations for type : UnityEngine.Rigidbody

	//Start Registrations for type : UnityEngine.SceneManagement.Scene

		//System.String UnityEngine.SceneManagement.Scene::GetNameInternal(System.Int32)
		void Register_UnityEngine_SceneManagement_Scene_GetNameInternal();
		Register_UnityEngine_SceneManagement_Scene_GetNameInternal();

	//End Registrations for type : UnityEngine.SceneManagement.Scene

	//Start Registrations for type : UnityEngine.SceneManagement.SceneManager

		//System.Void UnityEngine.SceneManagement.SceneManager::INTERNAL_CALL_GetActiveScene(UnityEngine.SceneManagement.Scene&)
		void Register_UnityEngine_SceneManagement_SceneManager_INTERNAL_CALL_GetActiveScene();
		Register_UnityEngine_SceneManagement_SceneManager_INTERNAL_CALL_GetActiveScene();

		//UnityEngine.AsyncOperation UnityEngine.SceneManagement.SceneManager::LoadSceneAsyncNameIndexInternal(System.String,System.Int32,System.Boolean,System.Boolean)
		void Register_UnityEngine_SceneManagement_SceneManager_LoadSceneAsyncNameIndexInternal();
		Register_UnityEngine_SceneManagement_SceneManager_LoadSceneAsyncNameIndexInternal();

		//UnityEngine.AsyncOperation UnityEngine.SceneManagement.SceneManager::UnloadSceneNameIndexInternal(System.String,System.Int32,System.Boolean,System.Boolean&)
		void Register_UnityEngine_SceneManagement_SceneManager_UnloadSceneNameIndexInternal();
		Register_UnityEngine_SceneManagement_SceneManager_UnloadSceneNameIndexInternal();

	//End Registrations for type : UnityEngine.SceneManagement.SceneManager

	//Start Registrations for type : UnityEngine.Screen

		//System.Boolean UnityEngine.Screen::get_fullScreen()
		void Register_UnityEngine_Screen_get_fullScreen();
		Register_UnityEngine_Screen_get_fullScreen();

		//System.Int32 UnityEngine.Screen::get_height()
		void Register_UnityEngine_Screen_get_height();
		Register_UnityEngine_Screen_get_height();

		//System.Int32 UnityEngine.Screen::get_width()
		void Register_UnityEngine_Screen_get_width();
		Register_UnityEngine_Screen_get_width();

		//System.Single UnityEngine.Screen::get_dpi()
		void Register_UnityEngine_Screen_get_dpi();
		Register_UnityEngine_Screen_get_dpi();

		//System.Void UnityEngine.Screen::set_fullScreen(System.Boolean)
		void Register_UnityEngine_Screen_set_fullScreen();
		Register_UnityEngine_Screen_set_fullScreen();

	//End Registrations for type : UnityEngine.Screen

	//Start Registrations for type : UnityEngine.ScriptableObject

		//System.Void UnityEngine.ScriptableObject::Internal_CreateScriptableObject(UnityEngine.ScriptableObject)
		void Register_UnityEngine_ScriptableObject_Internal_CreateScriptableObject();
		Register_UnityEngine_ScriptableObject_Internal_CreateScriptableObject();

		//UnityEngine.ScriptableObject UnityEngine.ScriptableObject::CreateInstanceFromType(System.Type)
		void Register_UnityEngine_ScriptableObject_CreateInstanceFromType();
		Register_UnityEngine_ScriptableObject_CreateInstanceFromType();

	//End Registrations for type : UnityEngine.ScriptableObject

	//Start Registrations for type : UnityEngine.Shader

		//System.Boolean UnityEngine.Shader::get_isSupported()
		void Register_UnityEngine_Shader_get_isSupported();
		Register_UnityEngine_Shader_get_isSupported();

		//System.Int32 UnityEngine.Shader::PropertyToID(System.String)
		void Register_UnityEngine_Shader_PropertyToID();
		Register_UnityEngine_Shader_PropertyToID();

		//UnityEngine.Shader UnityEngine.Shader::Find(System.String)
		void Register_UnityEngine_Shader_Find();
		Register_UnityEngine_Shader_Find();

	//End Registrations for type : UnityEngine.Shader

	//Start Registrations for type : UnityEngine.SkinnedMeshRenderer

		//System.Single UnityEngine.SkinnedMeshRenderer::GetBlendShapeWeight(System.Int32)
		void Register_UnityEngine_SkinnedMeshRenderer_GetBlendShapeWeight();
		Register_UnityEngine_SkinnedMeshRenderer_GetBlendShapeWeight();

		//System.Void UnityEngine.SkinnedMeshRenderer::SetBlendShapeWeight(System.Int32,System.Single)
		void Register_UnityEngine_SkinnedMeshRenderer_SetBlendShapeWeight();
		Register_UnityEngine_SkinnedMeshRenderer_SetBlendShapeWeight();

	//End Registrations for type : UnityEngine.SkinnedMeshRenderer

	//Start Registrations for type : UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform

		//System.Boolean UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::Internal_Authenticated()
		void Register_UnityEngine_SocialPlatforms_GameCenter_GameCenterPlatform_Internal_Authenticated();
		Register_UnityEngine_SocialPlatforms_GameCenter_GameCenterPlatform_Internal_Authenticated();

		//System.Boolean UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::Internal_Underage()
		void Register_UnityEngine_SocialPlatforms_GameCenter_GameCenterPlatform_Internal_Underage();
		Register_UnityEngine_SocialPlatforms_GameCenter_GameCenterPlatform_Internal_Underage();

		//System.String UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::Internal_UserID()
		void Register_UnityEngine_SocialPlatforms_GameCenter_GameCenterPlatform_Internal_UserID();
		Register_UnityEngine_SocialPlatforms_GameCenter_GameCenterPlatform_Internal_UserID();

		//System.String UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::Internal_UserName()
		void Register_UnityEngine_SocialPlatforms_GameCenter_GameCenterPlatform_Internal_UserName();
		Register_UnityEngine_SocialPlatforms_GameCenter_GameCenterPlatform_Internal_UserName();

		//System.Void UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::Internal_Authenticate()
		void Register_UnityEngine_SocialPlatforms_GameCenter_GameCenterPlatform_Internal_Authenticate();
		Register_UnityEngine_SocialPlatforms_GameCenter_GameCenterPlatform_Internal_Authenticate();

		//System.Void UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::Internal_LoadAchievementDescriptions(System.Object)
		void Register_UnityEngine_SocialPlatforms_GameCenter_GameCenterPlatform_Internal_LoadAchievementDescriptions();
		Register_UnityEngine_SocialPlatforms_GameCenter_GameCenterPlatform_Internal_LoadAchievementDescriptions();

		//System.Void UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::Internal_LoadAchievements(System.Object)
		void Register_UnityEngine_SocialPlatforms_GameCenter_GameCenterPlatform_Internal_LoadAchievements();
		Register_UnityEngine_SocialPlatforms_GameCenter_GameCenterPlatform_Internal_LoadAchievements();

		//System.Void UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::Internal_LoadFriends(System.Object)
		void Register_UnityEngine_SocialPlatforms_GameCenter_GameCenterPlatform_Internal_LoadFriends();
		Register_UnityEngine_SocialPlatforms_GameCenter_GameCenterPlatform_Internal_LoadFriends();

		//System.Void UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::Internal_LoadScores(System.String,System.Object)
		void Register_UnityEngine_SocialPlatforms_GameCenter_GameCenterPlatform_Internal_LoadScores();
		Register_UnityEngine_SocialPlatforms_GameCenter_GameCenterPlatform_Internal_LoadScores();

		//System.Void UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::Internal_LoadUsers(System.String[],System.Object)
		void Register_UnityEngine_SocialPlatforms_GameCenter_GameCenterPlatform_Internal_LoadUsers();
		Register_UnityEngine_SocialPlatforms_GameCenter_GameCenterPlatform_Internal_LoadUsers();

		//System.Void UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::Internal_ReportProgress(System.String,System.Double,System.Object)
		void Register_UnityEngine_SocialPlatforms_GameCenter_GameCenterPlatform_Internal_ReportProgress();
		Register_UnityEngine_SocialPlatforms_GameCenter_GameCenterPlatform_Internal_ReportProgress();

		//System.Void UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::Internal_ReportScore(System.Int64,System.String,System.Object)
		void Register_UnityEngine_SocialPlatforms_GameCenter_GameCenterPlatform_Internal_ReportScore();
		Register_UnityEngine_SocialPlatforms_GameCenter_GameCenterPlatform_Internal_ReportScore();

		//System.Void UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::Internal_ResetAllAchievements()
		void Register_UnityEngine_SocialPlatforms_GameCenter_GameCenterPlatform_Internal_ResetAllAchievements();
		Register_UnityEngine_SocialPlatforms_GameCenter_GameCenterPlatform_Internal_ResetAllAchievements();

		//System.Void UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::Internal_ShowAchievementsUI()
		void Register_UnityEngine_SocialPlatforms_GameCenter_GameCenterPlatform_Internal_ShowAchievementsUI();
		Register_UnityEngine_SocialPlatforms_GameCenter_GameCenterPlatform_Internal_ShowAchievementsUI();

		//System.Void UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::Internal_ShowDefaultAchievementBanner(System.Boolean)
		void Register_UnityEngine_SocialPlatforms_GameCenter_GameCenterPlatform_Internal_ShowDefaultAchievementBanner();
		Register_UnityEngine_SocialPlatforms_GameCenter_GameCenterPlatform_Internal_ShowDefaultAchievementBanner();

		//System.Void UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::Internal_ShowLeaderboardUI()
		void Register_UnityEngine_SocialPlatforms_GameCenter_GameCenterPlatform_Internal_ShowLeaderboardUI();
		Register_UnityEngine_SocialPlatforms_GameCenter_GameCenterPlatform_Internal_ShowLeaderboardUI();

		//System.Void UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::Internal_ShowSpecificLeaderboardUI(System.String,System.Int32)
		void Register_UnityEngine_SocialPlatforms_GameCenter_GameCenterPlatform_Internal_ShowSpecificLeaderboardUI();
		Register_UnityEngine_SocialPlatforms_GameCenter_GameCenterPlatform_Internal_ShowSpecificLeaderboardUI();

		//UnityEngine.Texture2D UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::Internal_UserImage()
		void Register_UnityEngine_SocialPlatforms_GameCenter_GameCenterPlatform_Internal_UserImage();
		Register_UnityEngine_SocialPlatforms_GameCenter_GameCenterPlatform_Internal_UserImage();

	//End Registrations for type : UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform

	//Start Registrations for type : UnityEngine.SocialPlatforms.GameCenter.GcLeaderboard

		//System.Boolean UnityEngine.SocialPlatforms.GameCenter.GcLeaderboard::Loading()
		void Register_UnityEngine_SocialPlatforms_GameCenter_GcLeaderboard_Loading();
		Register_UnityEngine_SocialPlatforms_GameCenter_GcLeaderboard_Loading();

		//System.Void UnityEngine.SocialPlatforms.GameCenter.GcLeaderboard::Dispose()
		void Register_UnityEngine_SocialPlatforms_GameCenter_GcLeaderboard_Dispose();
		Register_UnityEngine_SocialPlatforms_GameCenter_GcLeaderboard_Dispose();

		//System.Void UnityEngine.SocialPlatforms.GameCenter.GcLeaderboard::Internal_LoadScores(System.String,System.Int32,System.Int32,System.String[],System.Int32,System.Int32,System.Object)
		void Register_UnityEngine_SocialPlatforms_GameCenter_GcLeaderboard_Internal_LoadScores();
		Register_UnityEngine_SocialPlatforms_GameCenter_GcLeaderboard_Internal_LoadScores();

	//End Registrations for type : UnityEngine.SocialPlatforms.GameCenter.GcLeaderboard

	//Start Registrations for type : UnityEngine.SortingLayer

		//System.Int32 UnityEngine.SortingLayer::GetLayerValueFromID(System.Int32)
		void Register_UnityEngine_SortingLayer_GetLayerValueFromID();
		Register_UnityEngine_SortingLayer_GetLayerValueFromID();

		//System.Int32 UnityEngine.SortingLayer::NameToID(System.String)
		void Register_UnityEngine_SortingLayer_NameToID();
		Register_UnityEngine_SortingLayer_NameToID();

		//System.String UnityEngine.SortingLayer::IDToName(System.Int32)
		void Register_UnityEngine_SortingLayer_IDToName();
		Register_UnityEngine_SortingLayer_IDToName();

	//End Registrations for type : UnityEngine.SortingLayer

	//Start Registrations for type : UnityEngine.Sprite

		//System.Boolean UnityEngine.Sprite::get_packed()
		void Register_UnityEngine_Sprite_get_packed();
		Register_UnityEngine_Sprite_get_packed();

		//System.Single UnityEngine.Sprite::get_pixelsPerUnit()
		void Register_UnityEngine_Sprite_get_pixelsPerUnit();
		Register_UnityEngine_Sprite_get_pixelsPerUnit();

		//System.Void UnityEngine.Sprite::INTERNAL_get_border(UnityEngine.Vector4&)
		void Register_UnityEngine_Sprite_INTERNAL_get_border();
		Register_UnityEngine_Sprite_INTERNAL_get_border();

		//System.Void UnityEngine.Sprite::INTERNAL_get_rect(UnityEngine.Rect&)
		void Register_UnityEngine_Sprite_INTERNAL_get_rect();
		Register_UnityEngine_Sprite_INTERNAL_get_rect();

		//System.Void UnityEngine.Sprite::INTERNAL_get_textureRect(UnityEngine.Rect&)
		void Register_UnityEngine_Sprite_INTERNAL_get_textureRect();
		Register_UnityEngine_Sprite_INTERNAL_get_textureRect();

		//UnityEngine.Sprite UnityEngine.Sprite::INTERNAL_CALL_Create(UnityEngine.Texture2D,UnityEngine.Rect&,UnityEngine.Vector2&,System.Single,System.UInt32,UnityEngine.SpriteMeshType,UnityEngine.Vector4&)
		void Register_UnityEngine_Sprite_INTERNAL_CALL_Create();
		Register_UnityEngine_Sprite_INTERNAL_CALL_Create();

		//UnityEngine.Texture2D UnityEngine.Sprite::get_associatedAlphaSplitTexture()
		void Register_UnityEngine_Sprite_get_associatedAlphaSplitTexture();
		Register_UnityEngine_Sprite_get_associatedAlphaSplitTexture();

		//UnityEngine.Texture2D UnityEngine.Sprite::get_texture()
		void Register_UnityEngine_Sprite_get_texture();
		Register_UnityEngine_Sprite_get_texture();

	//End Registrations for type : UnityEngine.Sprite

	//Start Registrations for type : UnityEngine.SpriteRenderer

		//System.Void UnityEngine.SpriteRenderer::INTERNAL_set_color(UnityEngine.Color&)
		void Register_UnityEngine_SpriteRenderer_INTERNAL_set_color();
		Register_UnityEngine_SpriteRenderer_INTERNAL_set_color();

	//End Registrations for type : UnityEngine.SpriteRenderer

	//Start Registrations for type : UnityEngine.Sprites.DataUtility

		//System.Void UnityEngine.Sprites.DataUtility::INTERNAL_CALL_GetInnerUV(UnityEngine.Sprite,UnityEngine.Vector4&)
		void Register_UnityEngine_Sprites_DataUtility_INTERNAL_CALL_GetInnerUV();
		Register_UnityEngine_Sprites_DataUtility_INTERNAL_CALL_GetInnerUV();

		//System.Void UnityEngine.Sprites.DataUtility::INTERNAL_CALL_GetOuterUV(UnityEngine.Sprite,UnityEngine.Vector4&)
		void Register_UnityEngine_Sprites_DataUtility_INTERNAL_CALL_GetOuterUV();
		Register_UnityEngine_Sprites_DataUtility_INTERNAL_CALL_GetOuterUV();

		//System.Void UnityEngine.Sprites.DataUtility::INTERNAL_CALL_GetPadding(UnityEngine.Sprite,UnityEngine.Vector4&)
		void Register_UnityEngine_Sprites_DataUtility_INTERNAL_CALL_GetPadding();
		Register_UnityEngine_Sprites_DataUtility_INTERNAL_CALL_GetPadding();

		//System.Void UnityEngine.Sprites.DataUtility::Internal_GetMinSize(UnityEngine.Sprite,UnityEngine.Vector2&)
		void Register_UnityEngine_Sprites_DataUtility_Internal_GetMinSize();
		Register_UnityEngine_Sprites_DataUtility_Internal_GetMinSize();

	//End Registrations for type : UnityEngine.Sprites.DataUtility

	//Start Registrations for type : UnityEngine.SystemInfo

		//System.Boolean UnityEngine.SystemInfo::SupportsRenderTextureFormat(UnityEngine.RenderTextureFormat)
		void Register_UnityEngine_SystemInfo_SupportsRenderTextureFormat();
		Register_UnityEngine_SystemInfo_SupportsRenderTextureFormat();

		//System.Boolean UnityEngine.SystemInfo::get_supportsComputeShaders()
		void Register_UnityEngine_SystemInfo_get_supportsComputeShaders();
		Register_UnityEngine_SystemInfo_get_supportsComputeShaders();

		//System.Boolean UnityEngine.SystemInfo::get_supportsImageEffects()
		void Register_UnityEngine_SystemInfo_get_supportsImageEffects();
		Register_UnityEngine_SystemInfo_get_supportsImageEffects();

		//System.Int32 UnityEngine.SystemInfo::get_graphicsMemorySize()
		void Register_UnityEngine_SystemInfo_get_graphicsMemorySize();
		Register_UnityEngine_SystemInfo_get_graphicsMemorySize();

		//System.Int32 UnityEngine.SystemInfo::get_graphicsShaderLevel()
		void Register_UnityEngine_SystemInfo_get_graphicsShaderLevel();
		Register_UnityEngine_SystemInfo_get_graphicsShaderLevel();

		//System.Int32 UnityEngine.SystemInfo::get_systemMemorySize()
		void Register_UnityEngine_SystemInfo_get_systemMemorySize();
		Register_UnityEngine_SystemInfo_get_systemMemorySize();

		//UnityEngine.OperatingSystemFamily UnityEngine.SystemInfo::get_operatingSystemFamily()
		void Register_UnityEngine_SystemInfo_get_operatingSystemFamily();
		Register_UnityEngine_SystemInfo_get_operatingSystemFamily();

	//End Registrations for type : UnityEngine.SystemInfo

	//Start Registrations for type : UnityEngine.TextAsset

		//System.Byte[] UnityEngine.TextAsset::get_bytes()
		void Register_UnityEngine_TextAsset_get_bytes();
		Register_UnityEngine_TextAsset_get_bytes();

		//System.String UnityEngine.TextAsset::get_text()
		void Register_UnityEngine_TextAsset_get_text();
		Register_UnityEngine_TextAsset_get_text();

	//End Registrations for type : UnityEngine.TextAsset

	//Start Registrations for type : UnityEngine.TextGenerator

		//System.Boolean UnityEngine.TextGenerator::INTERNAL_CALL_Populate_Internal_cpp(UnityEngine.TextGenerator,System.String,UnityEngine.Font,UnityEngine.Color&,System.Int32,System.Single,System.Single,UnityEngine.FontStyle,System.Boolean,System.Boolean,System.Int32,System.Int32,System.Int32,System.Int32,System.Boolean,UnityEngine.TextAnchor,System.Single,System.Single,System.Single,System.Single,System.Boolean,System.Boolean,System.UInt32&)
		void Register_UnityEngine_TextGenerator_INTERNAL_CALL_Populate_Internal_cpp();
		Register_UnityEngine_TextGenerator_INTERNAL_CALL_Populate_Internal_cpp();

		//System.Int32 UnityEngine.TextGenerator::get_characterCount()
		void Register_UnityEngine_TextGenerator_get_characterCount();
		Register_UnityEngine_TextGenerator_get_characterCount();

		//System.Int32 UnityEngine.TextGenerator::get_lineCount()
		void Register_UnityEngine_TextGenerator_get_lineCount();
		Register_UnityEngine_TextGenerator_get_lineCount();

		//System.Void UnityEngine.TextGenerator::Dispose_cpp()
		void Register_UnityEngine_TextGenerator_Dispose_cpp();
		Register_UnityEngine_TextGenerator_Dispose_cpp();

		//System.Void UnityEngine.TextGenerator::GetCharactersInternal(System.Object)
		void Register_UnityEngine_TextGenerator_GetCharactersInternal();
		Register_UnityEngine_TextGenerator_GetCharactersInternal();

		//System.Void UnityEngine.TextGenerator::GetLinesInternal(System.Object)
		void Register_UnityEngine_TextGenerator_GetLinesInternal();
		Register_UnityEngine_TextGenerator_GetLinesInternal();

		//System.Void UnityEngine.TextGenerator::GetVerticesInternal(System.Object)
		void Register_UnityEngine_TextGenerator_GetVerticesInternal();
		Register_UnityEngine_TextGenerator_GetVerticesInternal();

		//System.Void UnityEngine.TextGenerator::INTERNAL_get_rectExtents(UnityEngine.Rect&)
		void Register_UnityEngine_TextGenerator_INTERNAL_get_rectExtents();
		Register_UnityEngine_TextGenerator_INTERNAL_get_rectExtents();

		//System.Void UnityEngine.TextGenerator::Init()
		void Register_UnityEngine_TextGenerator_Init();
		Register_UnityEngine_TextGenerator_Init();

	//End Registrations for type : UnityEngine.TextGenerator

	//Start Registrations for type : UnityEngine.Texture

		//System.Int32 UnityEngine.Texture::Internal_GetHeight(UnityEngine.Texture)
		void Register_UnityEngine_Texture_Internal_GetHeight();
		Register_UnityEngine_Texture_Internal_GetHeight();

		//System.Int32 UnityEngine.Texture::Internal_GetWidth(UnityEngine.Texture)
		void Register_UnityEngine_Texture_Internal_GetWidth();
		Register_UnityEngine_Texture_Internal_GetWidth();

		//System.Void UnityEngine.Texture::INTERNAL_get_texelSize(UnityEngine.Vector2&)
		void Register_UnityEngine_Texture_INTERNAL_get_texelSize();
		Register_UnityEngine_Texture_INTERNAL_get_texelSize();

		//System.Void UnityEngine.Texture::set_filterMode(UnityEngine.FilterMode)
		void Register_UnityEngine_Texture_set_filterMode();
		Register_UnityEngine_Texture_set_filterMode();

		//System.Void UnityEngine.Texture::set_wrapMode(UnityEngine.TextureWrapMode)
		void Register_UnityEngine_Texture_set_wrapMode();
		Register_UnityEngine_Texture_set_wrapMode();

		//UnityEngine.TextureWrapMode UnityEngine.Texture::get_wrapMode()
		void Register_UnityEngine_Texture_get_wrapMode();
		Register_UnityEngine_Texture_get_wrapMode();

	//End Registrations for type : UnityEngine.Texture

	//Start Registrations for type : UnityEngine.Texture2D

		//System.Int32 UnityEngine.Texture2D::get_mipmapCount()
		void Register_UnityEngine_Texture2D_get_mipmapCount();
		Register_UnityEngine_Texture2D_get_mipmapCount();

		//System.Void UnityEngine.Texture2D::Apply(System.Boolean,System.Boolean)
		void Register_UnityEngine_Texture2D_Apply();
		Register_UnityEngine_Texture2D_Apply();

		//System.Void UnityEngine.Texture2D::INTERNAL_CALL_GetPixelBilinear(UnityEngine.Texture2D,System.Single,System.Single,UnityEngine.Color&)
		void Register_UnityEngine_Texture2D_INTERNAL_CALL_GetPixelBilinear();
		Register_UnityEngine_Texture2D_INTERNAL_CALL_GetPixelBilinear();

		//System.Void UnityEngine.Texture2D::INTERNAL_CALL_ReadPixels(UnityEngine.Texture2D,UnityEngine.Rect&,System.Int32,System.Int32,System.Boolean)
		void Register_UnityEngine_Texture2D_INTERNAL_CALL_ReadPixels();
		Register_UnityEngine_Texture2D_INTERNAL_CALL_ReadPixels();

		//System.Void UnityEngine.Texture2D::Internal_Create(UnityEngine.Texture2D,System.Int32,System.Int32,UnityEngine.TextureFormat,System.Boolean,System.Boolean,System.IntPtr)
		void Register_UnityEngine_Texture2D_Internal_Create();
		Register_UnityEngine_Texture2D_Internal_Create();

		//System.Void UnityEngine.Texture2D::SetPixels(System.Int32,System.Int32,System.Int32,System.Int32,UnityEngine.Color[],System.Int32)
		void Register_UnityEngine_Texture2D_SetPixels();
		Register_UnityEngine_Texture2D_SetPixels();

		//UnityEngine.Texture2D UnityEngine.Texture2D::get_whiteTexture()
		void Register_UnityEngine_Texture2D_get_whiteTexture();
		Register_UnityEngine_Texture2D_get_whiteTexture();

		//UnityEngine.TextureFormat UnityEngine.Texture2D::get_format()
		void Register_UnityEngine_Texture2D_get_format();
		Register_UnityEngine_Texture2D_get_format();

	//End Registrations for type : UnityEngine.Texture2D

	//Start Registrations for type : UnityEngine.Time

		//System.Int32 UnityEngine.Time::get_frameCount()
		void Register_UnityEngine_Time_get_frameCount();
		Register_UnityEngine_Time_get_frameCount();

		//System.Single UnityEngine.Time::get_deltaTime()
		void Register_UnityEngine_Time_get_deltaTime();
		Register_UnityEngine_Time_get_deltaTime();

		//System.Single UnityEngine.Time::get_fixedDeltaTime()
		void Register_UnityEngine_Time_get_fixedDeltaTime();
		Register_UnityEngine_Time_get_fixedDeltaTime();

		//System.Single UnityEngine.Time::get_realtimeSinceStartup()
		void Register_UnityEngine_Time_get_realtimeSinceStartup();
		Register_UnityEngine_Time_get_realtimeSinceStartup();

		//System.Single UnityEngine.Time::get_time()
		void Register_UnityEngine_Time_get_time();
		Register_UnityEngine_Time_get_time();

		//System.Single UnityEngine.Time::get_timeScale()
		void Register_UnityEngine_Time_get_timeScale();
		Register_UnityEngine_Time_get_timeScale();

		//System.Single UnityEngine.Time::get_unscaledDeltaTime()
		void Register_UnityEngine_Time_get_unscaledDeltaTime();
		Register_UnityEngine_Time_get_unscaledDeltaTime();

		//System.Single UnityEngine.Time::get_unscaledTime()
		void Register_UnityEngine_Time_get_unscaledTime();
		Register_UnityEngine_Time_get_unscaledTime();

	//End Registrations for type : UnityEngine.Time

	//Start Registrations for type : UnityEngine.TouchScreenKeyboard

		//System.Boolean UnityEngine.TouchScreenKeyboard::get_active()
		void Register_UnityEngine_TouchScreenKeyboard_get_active();
		Register_UnityEngine_TouchScreenKeyboard_get_active();

		//System.Boolean UnityEngine.TouchScreenKeyboard::get_canGetSelection()
		void Register_UnityEngine_TouchScreenKeyboard_get_canGetSelection();
		Register_UnityEngine_TouchScreenKeyboard_get_canGetSelection();

		//System.Boolean UnityEngine.TouchScreenKeyboard::get_done()
		void Register_UnityEngine_TouchScreenKeyboard_get_done();
		Register_UnityEngine_TouchScreenKeyboard_get_done();

		//System.Boolean UnityEngine.TouchScreenKeyboard::get_wasCanceled()
		void Register_UnityEngine_TouchScreenKeyboard_get_wasCanceled();
		Register_UnityEngine_TouchScreenKeyboard_get_wasCanceled();

		//System.String UnityEngine.TouchScreenKeyboard::get_text()
		void Register_UnityEngine_TouchScreenKeyboard_get_text();
		Register_UnityEngine_TouchScreenKeyboard_get_text();

		//System.Void UnityEngine.TouchScreenKeyboard::Destroy()
		void Register_UnityEngine_TouchScreenKeyboard_Destroy();
		Register_UnityEngine_TouchScreenKeyboard_Destroy();

		//System.Void UnityEngine.TouchScreenKeyboard::GetSelectionInternal(System.Int32&,System.Int32&)
		void Register_UnityEngine_TouchScreenKeyboard_GetSelectionInternal();
		Register_UnityEngine_TouchScreenKeyboard_GetSelectionInternal();

		//System.Void UnityEngine.TouchScreenKeyboard::TouchScreenKeyboard_InternalConstructorHelper(UnityEngine.TouchScreenKeyboard_InternalConstructorHelperArguments&,System.String,System.String)
		void Register_UnityEngine_TouchScreenKeyboard_TouchScreenKeyboard_InternalConstructorHelper();
		Register_UnityEngine_TouchScreenKeyboard_TouchScreenKeyboard_InternalConstructorHelper();

		//System.Void UnityEngine.TouchScreenKeyboard::set_active(System.Boolean)
		void Register_UnityEngine_TouchScreenKeyboard_set_active();
		Register_UnityEngine_TouchScreenKeyboard_set_active();

		//System.Void UnityEngine.TouchScreenKeyboard::set_hideInput(System.Boolean)
		void Register_UnityEngine_TouchScreenKeyboard_set_hideInput();
		Register_UnityEngine_TouchScreenKeyboard_set_hideInput();

		//System.Void UnityEngine.TouchScreenKeyboard::set_text(System.String)
		void Register_UnityEngine_TouchScreenKeyboard_set_text();
		Register_UnityEngine_TouchScreenKeyboard_set_text();

	//End Registrations for type : UnityEngine.TouchScreenKeyboard

	//Start Registrations for type : UnityEngine.Transform

		//System.Boolean UnityEngine.Transform::IsChildOf(UnityEngine.Transform)
		void Register_UnityEngine_Transform_IsChildOf();
		Register_UnityEngine_Transform_IsChildOf();

		//System.Boolean UnityEngine.Transform::get_hasChanged()
		void Register_UnityEngine_Transform_get_hasChanged();
		Register_UnityEngine_Transform_get_hasChanged();

		//System.Int32 UnityEngine.Transform::get_childCount()
		void Register_UnityEngine_Transform_get_childCount();
		Register_UnityEngine_Transform_get_childCount();

		//System.Void UnityEngine.Transform::DetachChildren()
		void Register_UnityEngine_Transform_DetachChildren();
		Register_UnityEngine_Transform_DetachChildren();

		//System.Void UnityEngine.Transform::INTERNAL_CALL_InverseTransformPoint(UnityEngine.Transform,UnityEngine.Vector3&,UnityEngine.Vector3&)
		void Register_UnityEngine_Transform_INTERNAL_CALL_InverseTransformPoint();
		Register_UnityEngine_Transform_INTERNAL_CALL_InverseTransformPoint();

		//System.Void UnityEngine.Transform::INTERNAL_CALL_InverseTransformVector(UnityEngine.Transform,UnityEngine.Vector3&,UnityEngine.Vector3&)
		void Register_UnityEngine_Transform_INTERNAL_CALL_InverseTransformVector();
		Register_UnityEngine_Transform_INTERNAL_CALL_InverseTransformVector();

		//System.Void UnityEngine.Transform::INTERNAL_CALL_LookAt(UnityEngine.Transform,UnityEngine.Vector3&,UnityEngine.Vector3&)
		void Register_UnityEngine_Transform_INTERNAL_CALL_LookAt();
		Register_UnityEngine_Transform_INTERNAL_CALL_LookAt();

		//System.Void UnityEngine.Transform::INTERNAL_CALL_TransformDirection(UnityEngine.Transform,UnityEngine.Vector3&,UnityEngine.Vector3&)
		void Register_UnityEngine_Transform_INTERNAL_CALL_TransformDirection();
		Register_UnityEngine_Transform_INTERNAL_CALL_TransformDirection();

		//System.Void UnityEngine.Transform::INTERNAL_CALL_TransformPoint(UnityEngine.Transform,UnityEngine.Vector3&,UnityEngine.Vector3&)
		void Register_UnityEngine_Transform_INTERNAL_CALL_TransformPoint();
		Register_UnityEngine_Transform_INTERNAL_CALL_TransformPoint();

		//System.Void UnityEngine.Transform::INTERNAL_CALL_TransformVector(UnityEngine.Transform,UnityEngine.Vector3&,UnityEngine.Vector3&)
		void Register_UnityEngine_Transform_INTERNAL_CALL_TransformVector();
		Register_UnityEngine_Transform_INTERNAL_CALL_TransformVector();

		//System.Void UnityEngine.Transform::INTERNAL_get_localPosition(UnityEngine.Vector3&)
		void Register_UnityEngine_Transform_INTERNAL_get_localPosition();
		Register_UnityEngine_Transform_INTERNAL_get_localPosition();

		//System.Void UnityEngine.Transform::INTERNAL_get_localRotation(UnityEngine.Quaternion&)
		void Register_UnityEngine_Transform_INTERNAL_get_localRotation();
		Register_UnityEngine_Transform_INTERNAL_get_localRotation();

		//System.Void UnityEngine.Transform::INTERNAL_get_localScale(UnityEngine.Vector3&)
		void Register_UnityEngine_Transform_INTERNAL_get_localScale();
		Register_UnityEngine_Transform_INTERNAL_get_localScale();

		//System.Void UnityEngine.Transform::INTERNAL_get_lossyScale(UnityEngine.Vector3&)
		void Register_UnityEngine_Transform_INTERNAL_get_lossyScale();
		Register_UnityEngine_Transform_INTERNAL_get_lossyScale();

		//System.Void UnityEngine.Transform::INTERNAL_get_position(UnityEngine.Vector3&)
		void Register_UnityEngine_Transform_INTERNAL_get_position();
		Register_UnityEngine_Transform_INTERNAL_get_position();

		//System.Void UnityEngine.Transform::INTERNAL_get_rotation(UnityEngine.Quaternion&)
		void Register_UnityEngine_Transform_INTERNAL_get_rotation();
		Register_UnityEngine_Transform_INTERNAL_get_rotation();

		//System.Void UnityEngine.Transform::INTERNAL_get_worldToLocalMatrix(UnityEngine.Matrix4x4&)
		void Register_UnityEngine_Transform_INTERNAL_get_worldToLocalMatrix();
		Register_UnityEngine_Transform_INTERNAL_get_worldToLocalMatrix();

		//System.Void UnityEngine.Transform::INTERNAL_set_localPosition(UnityEngine.Vector3&)
		void Register_UnityEngine_Transform_INTERNAL_set_localPosition();
		Register_UnityEngine_Transform_INTERNAL_set_localPosition();

		//System.Void UnityEngine.Transform::INTERNAL_set_localRotation(UnityEngine.Quaternion&)
		void Register_UnityEngine_Transform_INTERNAL_set_localRotation();
		Register_UnityEngine_Transform_INTERNAL_set_localRotation();

		//System.Void UnityEngine.Transform::INTERNAL_set_localScale(UnityEngine.Vector3&)
		void Register_UnityEngine_Transform_INTERNAL_set_localScale();
		Register_UnityEngine_Transform_INTERNAL_set_localScale();

		//System.Void UnityEngine.Transform::INTERNAL_set_position(UnityEngine.Vector3&)
		void Register_UnityEngine_Transform_INTERNAL_set_position();
		Register_UnityEngine_Transform_INTERNAL_set_position();

		//System.Void UnityEngine.Transform::INTERNAL_set_rotation(UnityEngine.Quaternion&)
		void Register_UnityEngine_Transform_INTERNAL_set_rotation();
		Register_UnityEngine_Transform_INTERNAL_set_rotation();

		//System.Void UnityEngine.Transform::SetAsFirstSibling()
		void Register_UnityEngine_Transform_SetAsFirstSibling();
		Register_UnityEngine_Transform_SetAsFirstSibling();

		//System.Void UnityEngine.Transform::SetParent(UnityEngine.Transform,System.Boolean)
		void Register_UnityEngine_Transform_SetParent();
		Register_UnityEngine_Transform_SetParent();

		//System.Void UnityEngine.Transform::SetSiblingIndex(System.Int32)
		void Register_UnityEngine_Transform_SetSiblingIndex();
		Register_UnityEngine_Transform_SetSiblingIndex();

		//System.Void UnityEngine.Transform::set_parentInternal(UnityEngine.Transform)
		void Register_UnityEngine_Transform_set_parentInternal();
		Register_UnityEngine_Transform_set_parentInternal();

		//UnityEngine.Transform UnityEngine.Transform::Find(System.String)
		void Register_UnityEngine_Transform_Find();
		Register_UnityEngine_Transform_Find();

		//UnityEngine.Transform UnityEngine.Transform::GetChild(System.Int32)
		void Register_UnityEngine_Transform_GetChild();
		Register_UnityEngine_Transform_GetChild();

		//UnityEngine.Transform UnityEngine.Transform::get_parentInternal()
		void Register_UnityEngine_Transform_get_parentInternal();
		Register_UnityEngine_Transform_get_parentInternal();

	//End Registrations for type : UnityEngine.Transform

	//Start Registrations for type : UnityEngine.U2D.SpriteAtlasManager

		//System.Void UnityEngine.U2D.SpriteAtlasManager::Register(UnityEngine.U2D.SpriteAtlas)
		void Register_UnityEngine_U2D_SpriteAtlasManager_Register();
		Register_UnityEngine_U2D_SpriteAtlasManager_Register();

	//End Registrations for type : UnityEngine.U2D.SpriteAtlasManager

	//Start Registrations for type : UnityEngine.UISystemProfilerApi

		//System.Void UnityEngine.UISystemProfilerApi::AddMarker(System.String,UnityEngine.Object)
		void Register_UnityEngine_UISystemProfilerApi_AddMarker();
		Register_UnityEngine_UISystemProfilerApi_AddMarker();

		//System.Void UnityEngine.UISystemProfilerApi::BeginSample(UnityEngine.UISystemProfilerApi/SampleType)
		void Register_UnityEngine_UISystemProfilerApi_BeginSample();
		Register_UnityEngine_UISystemProfilerApi_BeginSample();

		//System.Void UnityEngine.UISystemProfilerApi::EndSample(UnityEngine.UISystemProfilerApi/SampleType)
		void Register_UnityEngine_UISystemProfilerApi_EndSample();
		Register_UnityEngine_UISystemProfilerApi_EndSample();

	//End Registrations for type : UnityEngine.UISystemProfilerApi

	//Start Registrations for type : UnityEngine.UnhandledExceptionHandler

		//System.Void UnityEngine.UnhandledExceptionHandler::NativeUnhandledExceptionHandler(System.String,System.String,System.String)
		void Register_UnityEngine_UnhandledExceptionHandler_NativeUnhandledExceptionHandler();
		Register_UnityEngine_UnhandledExceptionHandler_NativeUnhandledExceptionHandler();

	//End Registrations for type : UnityEngine.UnhandledExceptionHandler

	//Start Registrations for type : UnityEngine.UnityLogWriter

		//System.Void UnityEngine.UnityLogWriter::WriteStringToUnityLog(System.String)
		void Register_UnityEngine_UnityLogWriter_WriteStringToUnityLog();
		Register_UnityEngine_UnityLogWriter_WriteStringToUnityLog();

	//End Registrations for type : UnityEngine.UnityLogWriter

	//Start Registrations for type : UnityEngine.Video.VideoClip

		//System.UInt32 UnityEngine.Video.VideoClip::get_height()
		void Register_UnityEngine_Video_VideoClip_get_height();
		Register_UnityEngine_Video_VideoClip_get_height();

		//System.UInt32 UnityEngine.Video.VideoClip::get_width()
		void Register_UnityEngine_Video_VideoClip_get_width();
		Register_UnityEngine_Video_VideoClip_get_width();

	//End Registrations for type : UnityEngine.Video.VideoClip

	//Start Registrations for type : UnityEngine.Video.VideoPlayer

		//System.Boolean UnityEngine.Video.VideoPlayer::get_isPlaying()
		void Register_UnityEngine_Video_VideoPlayer_get_isPlaying();
		Register_UnityEngine_Video_VideoPlayer_get_isPlaying();

		//System.Double UnityEngine.Video.VideoPlayer::get_time()
		void Register_UnityEngine_Video_VideoPlayer_get_time();
		Register_UnityEngine_Video_VideoPlayer_get_time();

		//System.Void UnityEngine.Video.VideoPlayer::INTERNAL_CALL_Play(UnityEngine.Video.VideoPlayer)
		void Register_UnityEngine_Video_VideoPlayer_INTERNAL_CALL_Play();
		Register_UnityEngine_Video_VideoPlayer_INTERNAL_CALL_Play();

		//System.Void UnityEngine.Video.VideoPlayer::INTERNAL_CALL_Stop(UnityEngine.Video.VideoPlayer)
		void Register_UnityEngine_Video_VideoPlayer_INTERNAL_CALL_Stop();
		Register_UnityEngine_Video_VideoPlayer_INTERNAL_CALL_Stop();

		//System.Void UnityEngine.Video.VideoPlayer::set_aspectRatio(UnityEngine.Video.VideoAspectRatio)
		void Register_UnityEngine_Video_VideoPlayer_set_aspectRatio();
		Register_UnityEngine_Video_VideoPlayer_set_aspectRatio();

		//System.Void UnityEngine.Video.VideoPlayer::set_clip(UnityEngine.Video.VideoClip)
		void Register_UnityEngine_Video_VideoPlayer_set_clip();
		Register_UnityEngine_Video_VideoPlayer_set_clip();

		//System.Void UnityEngine.Video.VideoPlayer::set_isLooping(System.Boolean)
		void Register_UnityEngine_Video_VideoPlayer_set_isLooping();
		Register_UnityEngine_Video_VideoPlayer_set_isLooping();

		//System.Void UnityEngine.Video.VideoPlayer::set_renderMode(UnityEngine.Video.VideoRenderMode)
		void Register_UnityEngine_Video_VideoPlayer_set_renderMode();
		Register_UnityEngine_Video_VideoPlayer_set_renderMode();

		//System.Void UnityEngine.Video.VideoPlayer::set_targetCamera(UnityEngine.Camera)
		void Register_UnityEngine_Video_VideoPlayer_set_targetCamera();
		Register_UnityEngine_Video_VideoPlayer_set_targetCamera();

		//System.Void UnityEngine.Video.VideoPlayer::set_targetTexture(UnityEngine.RenderTexture)
		void Register_UnityEngine_Video_VideoPlayer_set_targetTexture();
		Register_UnityEngine_Video_VideoPlayer_set_targetTexture();

	//End Registrations for type : UnityEngine.Video.VideoPlayer

}
