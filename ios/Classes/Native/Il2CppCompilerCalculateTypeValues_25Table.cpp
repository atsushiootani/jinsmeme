﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "object-internals.h"

// System.String
struct String_t;
// System.Collections.Generic.Dictionary`2<System.String,Utage.AdvMacroData>
struct Dictionary_2_t168349605;
// System.Text.RegularExpressions.Regex
struct Regex_t1803876613;
// System.Collections.Generic.List`1<System.String>
struct List_1_t1398341365;
// System.Collections.Generic.Dictionary`2<System.String,Utage.LanguageData/LanguageStrings>
struct Dictionary_2_t4287613913;
// System.Collections.Generic.Dictionary`2<System.String,System.Byte[]>
struct Dictionary_2_t1017145979;
// Utage.AdvGraphicInfo
struct AdvGraphicInfo_t3545565645;
// Utage.AdvUguiLoadGraphicFile
struct AdvUguiLoadGraphicFile_t1474676621;
// Utage.AssetFile
struct AssetFile_t3383013256;
// Utage.AdvUguiMessageWindowFaceIcon
struct AdvUguiMessageWindowFaceIcon_t623217832;
// System.Action`1<Utage.AdvUguiSelection>
struct Action_1_t2434018373;
// Utage.AdvUguiSelection
struct AdvUguiSelection_t2632218991;
// Utage.AdvSelection
struct AdvSelection_t2301351953;
// UnityEngine.GameObject
struct GameObject_t1756533147;
// UnityEngine.RectTransform
struct RectTransform_t3349966182;
// UnityEngine.Canvas
struct Canvas_t209405766;
// System.Byte[]
struct ByteU5BU5D_t3397334013;
// System.Collections.Generic.List`1<Utage.DictionaryKeyValueString>
struct List_1_t456006635;
// System.Collections.Generic.Dictionary`2<System.String,Utage.DictionaryKeyValueString>
struct Dictionary_2_t3001664765;
// System.Collections.Generic.List`1<Utage.DictionaryKeyValueDouble>
struct List_1_t504723423;
// System.Collections.Generic.Dictionary`2<System.String,Utage.DictionaryKeyValueDouble>
struct Dictionary_2_t3050381553;
// System.Collections.Generic.List`1<Utage.DictionaryKeyValueFloat>
struct List_1_t3217273144;
// System.Collections.Generic.Dictionary`2<System.String,Utage.DictionaryKeyValueFloat>
struct Dictionary_2_t1467963978;
// System.Collections.Generic.List`1<Utage.DictionaryKeyValueInt>
struct List_1_t2961215453;
// System.Collections.Generic.Dictionary`2<System.String,Utage.DictionaryKeyValueInt>
struct Dictionary_2_t1211906287;
// System.Collections.Generic.List`1<Utage.DictionaryKeyValueBool>
struct List_1_t2313607718;
// System.Collections.Generic.Dictionary`2<System.String,Utage.DictionaryKeyValueBool>
struct Dictionary_2_t564298552;
// UnityEngine.Events.InvokableCallList
struct InvokableCallList_t2295673753;
// UnityEngine.Events.PersistentCallGroup
struct PersistentCallGroup_t339478082;
// Utage.AdvSaveData
struct AdvSaveData_t4059487466;
// Utage.AdvScenarioPlayer
struct AdvScenarioPlayer_t1295831480;
// Utage.AdvBacklog
struct AdvBacklog_t3927301290;
// Utage.AdvUguiBacklog
struct AdvUguiBacklog_t1854927244;
// Utage.AdvDataManager
struct AdvDataManager_t2481232830;
// Utage.AdvScenarioPageData
struct AdvScenarioPageData_t3333166790;
// System.Collections.Generic.List`1<Utage.AdvCommandWaitBase>
struct List_1_t3542275616;
// Utage.AdvCommand
struct AdvCommand_t2859960984;
// Utage.AdvScenarioLabelData
struct AdvScenarioLabelData_t2266689885;
// Utage.AdvScenarioThread
struct AdvScenarioThread_t1270526825;
// UnityEngine.Coroutine
struct Coroutine_t2299508840;
// System.Collections.Generic.List`1<Utage.AdvCommand>
struct List_1_t2229082116;
// System.Collections.Generic.List`1<Utage.AdvCommandText>
struct List_1_t185954307;
// System.Collections.Generic.List`1<Utage.AdvScenarioLabelData>
struct List_1_t1635811017;
// System.Collections.Generic.HashSet`1<Utage.AssetFile>
struct HashSet_1_t1716474110;
// Utage.AdvImportScenarioSheet
struct AdvImportScenarioSheet_t3549824885;
// System.Collections.Generic.List`1<Utage.AdvScenarioJumpData>
struct List_1_t3515939073;
// System.Collections.Generic.Dictionary`2<System.String,Utage.AdvScenarioLabelData>
struct Dictionary_2_t4181469147;
// Utage.StringGridRow
struct StringGridRow_t4193237197;
// Utage.SoundManager
struct SoundManager_t1265124084;
// System.Collections.Generic.List`1<Utage.AdvScenarioPageData>
struct List_1_t2702287922;
// Utage.AdvCommandScenarioLabel
struct AdvCommandScenarioLabel_t2271667896;
// System.Action`1<Utage.AdvScenarioPageData>
struct Action_1_t3134966172;
// System.Char[]
struct CharU5BU5D_t1328083999;
// System.Object[]
struct ObjectU5BU5D_t3614634134;
// System.Void
struct Void_t1841601450;
// System.String[]
struct StringU5BU5D_t1642385972;
// System.Collections.Generic.List`1<System.Int32>
struct List_1_t1440998580;
// Utage.LanguageManager
struct LanguageManager_t2685744067;
// System.Collections.Generic.List`1<UnityEngine.TextAsset>
struct List_1_t3342280977;
// System.Action
struct Action_t3226471752;
// Utage.LanguageData
struct LanguageData_t3686821228;
// Utage.StringGridDictionary
struct StringGridDictionary_t1396150451;
// Utage.AdvEngine
struct AdvEngine_t1176753927;
// Utage.AdvGraphicObject
struct AdvGraphicObject_t3651915246;
// Utage.UguiNovelText
struct UguiNovelText_t4135744055;
// UnityEngine.UI.Text
struct Text_t356221433;
// UnityEngine.CanvasGroup
struct CanvasGroup_t3296560743;
// Utage.UguiListView
struct UguiListView_t169672791;
// Utage.AdvGraphicLoader
struct AdvGraphicLoader_t202702654;
// UnityEngine.UI.Graphic
struct Graphic_t2426225576;
// UnityEngine.Events.UnityEvent
struct UnityEvent_t408735097;
// Utage.LetterBoxCameraEvent
struct LetterBoxCameraEvent_t3581932304;
// UnityEngine.Camera
struct Camera_t189460977;
// Utage.LetterBoxCamera
struct LetterBoxCamera_t3507617684;
// System.Collections.Generic.List`1<Utage.CameraRoot>
struct List_1_t3217464533;
// Utage.AdvIfManager
struct AdvIfManager_t1468978853;
// Utage.AdvJumpManager
struct AdvJumpManager_t2493327610;
// Utage.AdvWaitManager
struct AdvWaitManager_t2981197357;
// System.Collections.Generic.List`1<Utage.AdvScenarioThread>
struct List_1_t639647957;
// System.Collections.Generic.List`1<UnityEngine.GameObject>
struct List_1_t1125654279;
// System.Collections.Generic.Dictionary`2<System.String,Utage.AdvGuiBase>
struct Dictionary_2_t3938720553;
// UnityEngine.UI.Button
struct Button_t2872111280;
// Utage.AdvScenarioPlayerEvent
struct AdvScenarioPlayerEvent_t1780583340;
// Utage.AdvCommandEvent
struct AdvCommandEvent_t2718201788;
// Utage.AdvGuiManager
struct AdvGuiManager_t2331361001;
// UnityEngine.EventSystems.PointerEventData
struct PointerEventData_t1599784723;
// Utage.CustomProjectSetting
struct CustomProjectSetting_t3840122254;
// UnityEngine.RenderTexture
struct RenderTexture_t2666733923;
// Utage.CaptureCameraEvent
struct CaptureCameraEvent_t3263087955;
// Utage.AdvUguiMessageWindowManager
struct AdvUguiMessageWindowManager_t1210998287;
// Utage.AdvUguiSelectionManager
struct AdvUguiSelectionManager_t456513248;
// Utage.AdvUguiBacklogManager
struct AdvUguiBacklogManager_t342992083;
// UnityEngine.AudioSource
struct AudioSource_t1135106623;




#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
#ifndef BINARYIOEXTENSIONS_T1647632937_H
#define BINARYIOEXTENSIONS_T1647632937_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UtageExtensions.BinaryIOExtensions
struct  BinaryIOExtensions_t1647632937  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BINARYIOEXTENSIONS_T1647632937_H
#ifndef BINARYUTIL_T1236589793_H
#define BINARYUTIL_T1236589793_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.BinaryUtil
struct  BinaryUtil_t1236589793  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BINARYUTIL_T1236589793_H
#ifndef U3CFINDCAMERAROOTU3EC__ANONSTOREY0_T424579444_H
#define U3CFINDCAMERAROOTU3EC__ANONSTOREY0_T424579444_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.CameraManager/<FindCameraRoot>c__AnonStorey0
struct  U3CFindCameraRootU3Ec__AnonStorey0_t424579444  : public RuntimeObject
{
public:
	// System.String Utage.CameraManager/<FindCameraRoot>c__AnonStorey0::name
	String_t* ___name_0;

public:
	inline static int32_t get_offset_of_name_0() { return static_cast<int32_t>(offsetof(U3CFindCameraRootU3Ec__AnonStorey0_t424579444, ___name_0)); }
	inline String_t* get_name_0() const { return ___name_0; }
	inline String_t** get_address_of_name_0() { return &___name_0; }
	inline void set_name_0(String_t* value)
	{
		___name_0 = value;
		Il2CppCodeGenWriteBarrier((&___name_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CFINDCAMERAROOTU3EC__ANONSTOREY0_T424579444_H
#ifndef ADVMACROMANAGER_T3793610292_H
#define ADVMACROMANAGER_T3793610292_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.AdvMacroManager
struct  AdvMacroManager_t3793610292  : public RuntimeObject
{
public:
	// System.Collections.Generic.Dictionary`2<System.String,Utage.AdvMacroData> Utage.AdvMacroManager::macroDataTbl
	Dictionary_2_t168349605 * ___macroDataTbl_0;

public:
	inline static int32_t get_offset_of_macroDataTbl_0() { return static_cast<int32_t>(offsetof(AdvMacroManager_t3793610292, ___macroDataTbl_0)); }
	inline Dictionary_2_t168349605 * get_macroDataTbl_0() const { return ___macroDataTbl_0; }
	inline Dictionary_2_t168349605 ** get_address_of_macroDataTbl_0() { return &___macroDataTbl_0; }
	inline void set_macroDataTbl_0(Dictionary_2_t168349605 * value)
	{
		___macroDataTbl_0 = value;
		Il2CppCodeGenWriteBarrier((&___macroDataTbl_0), value);
	}
};

struct AdvMacroManager_t3793610292_StaticFields
{
public:
	// System.Text.RegularExpressions.Regex Utage.AdvMacroManager::SheetNameRegex
	Regex_t1803876613 * ___SheetNameRegex_2;

public:
	inline static int32_t get_offset_of_SheetNameRegex_2() { return static_cast<int32_t>(offsetof(AdvMacroManager_t3793610292_StaticFields, ___SheetNameRegex_2)); }
	inline Regex_t1803876613 * get_SheetNameRegex_2() const { return ___SheetNameRegex_2; }
	inline Regex_t1803876613 ** get_address_of_SheetNameRegex_2() { return &___SheetNameRegex_2; }
	inline void set_SheetNameRegex_2(Regex_t1803876613 * value)
	{
		___SheetNameRegex_2 = value;
		Il2CppCodeGenWriteBarrier((&___SheetNameRegex_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ADVMACROMANAGER_T3793610292_H
#ifndef LANGUAGEERRORMSG_T750765065_H
#define LANGUAGEERRORMSG_T750765065_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.LanguageErrorMsg
struct  LanguageErrorMsg_t750765065  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LANGUAGEERRORMSG_T750765065_H
#ifndef LANGUAGEDATA_T3686821228_H
#define LANGUAGEDATA_T3686821228_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.LanguageData
struct  LanguageData_t3686821228  : public RuntimeObject
{
public:
	// System.Collections.Generic.List`1<System.String> Utage.LanguageData::languages
	List_1_t1398341365 * ___languages_0;
	// System.Collections.Generic.Dictionary`2<System.String,Utage.LanguageData/LanguageStrings> Utage.LanguageData::dataTbl
	Dictionary_2_t4287613913 * ___dataTbl_1;

public:
	inline static int32_t get_offset_of_languages_0() { return static_cast<int32_t>(offsetof(LanguageData_t3686821228, ___languages_0)); }
	inline List_1_t1398341365 * get_languages_0() const { return ___languages_0; }
	inline List_1_t1398341365 ** get_address_of_languages_0() { return &___languages_0; }
	inline void set_languages_0(List_1_t1398341365 * value)
	{
		___languages_0 = value;
		Il2CppCodeGenWriteBarrier((&___languages_0), value);
	}

	inline static int32_t get_offset_of_dataTbl_1() { return static_cast<int32_t>(offsetof(LanguageData_t3686821228, ___dataTbl_1)); }
	inline Dictionary_2_t4287613913 * get_dataTbl_1() const { return ___dataTbl_1; }
	inline Dictionary_2_t4287613913 ** get_address_of_dataTbl_1() { return &___dataTbl_1; }
	inline void set_dataTbl_1(Dictionary_2_t4287613913 * value)
	{
		___dataTbl_1 = value;
		Il2CppCodeGenWriteBarrier((&___dataTbl_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LANGUAGEDATA_T3686821228_H
#ifndef BINARYBUFFER_T1215122249_H
#define BINARYBUFFER_T1215122249_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.BinaryBuffer
struct  BinaryBuffer_t1215122249  : public RuntimeObject
{
public:
	// System.Collections.Generic.Dictionary`2<System.String,System.Byte[]> Utage.BinaryBuffer::buffers
	Dictionary_2_t1017145979 * ___buffers_0;

public:
	inline static int32_t get_offset_of_buffers_0() { return static_cast<int32_t>(offsetof(BinaryBuffer_t1215122249, ___buffers_0)); }
	inline Dictionary_2_t1017145979 * get_buffers_0() const { return ___buffers_0; }
	inline Dictionary_2_t1017145979 ** get_address_of_buffers_0() { return &___buffers_0; }
	inline void set_buffers_0(Dictionary_2_t1017145979 * value)
	{
		___buffers_0 = value;
		Il2CppCodeGenWriteBarrier((&___buffers_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BINARYBUFFER_T1215122249_H
#ifndef U3CLOADFILEU3EC__ANONSTOREY1_T2370668122_H
#define U3CLOADFILEU3EC__ANONSTOREY1_T2370668122_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.AdvUguiLoadGraphicFile/<LoadFile>c__AnonStorey1
struct  U3CLoadFileU3Ec__AnonStorey1_t2370668122  : public RuntimeObject
{
public:
	// Utage.AdvGraphicInfo Utage.AdvUguiLoadGraphicFile/<LoadFile>c__AnonStorey1::graphic
	AdvGraphicInfo_t3545565645 * ___graphic_0;
	// Utage.AdvUguiLoadGraphicFile Utage.AdvUguiLoadGraphicFile/<LoadFile>c__AnonStorey1::$this
	AdvUguiLoadGraphicFile_t1474676621 * ___U24this_1;

public:
	inline static int32_t get_offset_of_graphic_0() { return static_cast<int32_t>(offsetof(U3CLoadFileU3Ec__AnonStorey1_t2370668122, ___graphic_0)); }
	inline AdvGraphicInfo_t3545565645 * get_graphic_0() const { return ___graphic_0; }
	inline AdvGraphicInfo_t3545565645 ** get_address_of_graphic_0() { return &___graphic_0; }
	inline void set_graphic_0(AdvGraphicInfo_t3545565645 * value)
	{
		___graphic_0 = value;
		Il2CppCodeGenWriteBarrier((&___graphic_0), value);
	}

	inline static int32_t get_offset_of_U24this_1() { return static_cast<int32_t>(offsetof(U3CLoadFileU3Ec__AnonStorey1_t2370668122, ___U24this_1)); }
	inline AdvUguiLoadGraphicFile_t1474676621 * get_U24this_1() const { return ___U24this_1; }
	inline AdvUguiLoadGraphicFile_t1474676621 ** get_address_of_U24this_1() { return &___U24this_1; }
	inline void set_U24this_1(AdvUguiLoadGraphicFile_t1474676621 * value)
	{
		___U24this_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CLOADFILEU3EC__ANONSTOREY1_T2370668122_H
#ifndef U3CCOWAITTEXTUREFILELOADINGU3EC__ITERATOR0_T1598614109_H
#define U3CCOWAITTEXTUREFILELOADINGU3EC__ITERATOR0_T1598614109_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.AdvUguiLoadGraphicFile/<CoWaitTextureFileLoading>c__Iterator0
struct  U3CCoWaitTextureFileLoadingU3Ec__Iterator0_t1598614109  : public RuntimeObject
{
public:
	// Utage.AdvUguiLoadGraphicFile Utage.AdvUguiLoadGraphicFile/<CoWaitTextureFileLoading>c__Iterator0::$this
	AdvUguiLoadGraphicFile_t1474676621 * ___U24this_0;
	// System.Object Utage.AdvUguiLoadGraphicFile/<CoWaitTextureFileLoading>c__Iterator0::$current
	RuntimeObject * ___U24current_1;
	// System.Boolean Utage.AdvUguiLoadGraphicFile/<CoWaitTextureFileLoading>c__Iterator0::$disposing
	bool ___U24disposing_2;
	// System.Int32 Utage.AdvUguiLoadGraphicFile/<CoWaitTextureFileLoading>c__Iterator0::$PC
	int32_t ___U24PC_3;

public:
	inline static int32_t get_offset_of_U24this_0() { return static_cast<int32_t>(offsetof(U3CCoWaitTextureFileLoadingU3Ec__Iterator0_t1598614109, ___U24this_0)); }
	inline AdvUguiLoadGraphicFile_t1474676621 * get_U24this_0() const { return ___U24this_0; }
	inline AdvUguiLoadGraphicFile_t1474676621 ** get_address_of_U24this_0() { return &___U24this_0; }
	inline void set_U24this_0(AdvUguiLoadGraphicFile_t1474676621 * value)
	{
		___U24this_0 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_0), value);
	}

	inline static int32_t get_offset_of_U24current_1() { return static_cast<int32_t>(offsetof(U3CCoWaitTextureFileLoadingU3Ec__Iterator0_t1598614109, ___U24current_1)); }
	inline RuntimeObject * get_U24current_1() const { return ___U24current_1; }
	inline RuntimeObject ** get_address_of_U24current_1() { return &___U24current_1; }
	inline void set_U24current_1(RuntimeObject * value)
	{
		___U24current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_1), value);
	}

	inline static int32_t get_offset_of_U24disposing_2() { return static_cast<int32_t>(offsetof(U3CCoWaitTextureFileLoadingU3Ec__Iterator0_t1598614109, ___U24disposing_2)); }
	inline bool get_U24disposing_2() const { return ___U24disposing_2; }
	inline bool* get_address_of_U24disposing_2() { return &___U24disposing_2; }
	inline void set_U24disposing_2(bool value)
	{
		___U24disposing_2 = value;
	}

	inline static int32_t get_offset_of_U24PC_3() { return static_cast<int32_t>(offsetof(U3CCoWaitTextureFileLoadingU3Ec__Iterator0_t1598614109, ___U24PC_3)); }
	inline int32_t get_U24PC_3() const { return ___U24PC_3; }
	inline int32_t* get_address_of_U24PC_3() { return &___U24PC_3; }
	inline void set_U24PC_3(int32_t value)
	{
		___U24PC_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CCOWAITTEXTUREFILELOADINGU3EC__ITERATOR0_T1598614109_H
#ifndef U3CSETICONIMAGEU3EC__ANONSTOREY0_T2483997726_H
#define U3CSETICONIMAGEU3EC__ANONSTOREY0_T2483997726_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.AdvUguiMessageWindowFaceIcon/<SetIconImage>c__AnonStorey0
struct  U3CSetIconImageU3Ec__AnonStorey0_t2483997726  : public RuntimeObject
{
public:
	// Utage.AssetFile Utage.AdvUguiMessageWindowFaceIcon/<SetIconImage>c__AnonStorey0::file
	RuntimeObject* ___file_0;
	// Utage.AdvUguiMessageWindowFaceIcon Utage.AdvUguiMessageWindowFaceIcon/<SetIconImage>c__AnonStorey0::$this
	AdvUguiMessageWindowFaceIcon_t623217832 * ___U24this_1;

public:
	inline static int32_t get_offset_of_file_0() { return static_cast<int32_t>(offsetof(U3CSetIconImageU3Ec__AnonStorey0_t2483997726, ___file_0)); }
	inline RuntimeObject* get_file_0() const { return ___file_0; }
	inline RuntimeObject** get_address_of_file_0() { return &___file_0; }
	inline void set_file_0(RuntimeObject* value)
	{
		___file_0 = value;
		Il2CppCodeGenWriteBarrier((&___file_0), value);
	}

	inline static int32_t get_offset_of_U24this_1() { return static_cast<int32_t>(offsetof(U3CSetIconImageU3Ec__AnonStorey0_t2483997726, ___U24this_1)); }
	inline AdvUguiMessageWindowFaceIcon_t623217832 * get_U24this_1() const { return ___U24this_1; }
	inline AdvUguiMessageWindowFaceIcon_t623217832 ** get_address_of_U24this_1() { return &___U24this_1; }
	inline void set_U24this_1(AdvUguiMessageWindowFaceIcon_t623217832 * value)
	{
		___U24this_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CSETICONIMAGEU3EC__ANONSTOREY0_T2483997726_H
#ifndef U3CINITU3EC__ANONSTOREY0_T3081790771_H
#define U3CINITU3EC__ANONSTOREY0_T3081790771_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.AdvUguiSelection/<Init>c__AnonStorey0
struct  U3CInitU3Ec__AnonStorey0_t3081790771  : public RuntimeObject
{
public:
	// System.Action`1<Utage.AdvUguiSelection> Utage.AdvUguiSelection/<Init>c__AnonStorey0::ButtonClickedEvent
	Action_1_t2434018373 * ___ButtonClickedEvent_0;
	// Utage.AdvUguiSelection Utage.AdvUguiSelection/<Init>c__AnonStorey0::$this
	AdvUguiSelection_t2632218991 * ___U24this_1;

public:
	inline static int32_t get_offset_of_ButtonClickedEvent_0() { return static_cast<int32_t>(offsetof(U3CInitU3Ec__AnonStorey0_t3081790771, ___ButtonClickedEvent_0)); }
	inline Action_1_t2434018373 * get_ButtonClickedEvent_0() const { return ___ButtonClickedEvent_0; }
	inline Action_1_t2434018373 ** get_address_of_ButtonClickedEvent_0() { return &___ButtonClickedEvent_0; }
	inline void set_ButtonClickedEvent_0(Action_1_t2434018373 * value)
	{
		___ButtonClickedEvent_0 = value;
		Il2CppCodeGenWriteBarrier((&___ButtonClickedEvent_0), value);
	}

	inline static int32_t get_offset_of_U24this_1() { return static_cast<int32_t>(offsetof(U3CInitU3Ec__AnonStorey0_t3081790771, ___U24this_1)); }
	inline AdvUguiSelection_t2632218991 * get_U24this_1() const { return ___U24this_1; }
	inline AdvUguiSelection_t2632218991 ** get_address_of_U24this_1() { return &___U24this_1; }
	inline void set_U24this_1(AdvUguiSelection_t2632218991 * value)
	{
		___U24this_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CINITU3EC__ANONSTOREY0_T3081790771_H
#ifndef U3CGETPREFABU3EC__ANONSTOREY0_T1981674732_H
#define U3CGETPREFABU3EC__ANONSTOREY0_T1981674732_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.AdvUguiSelectionManager/<GetPrefab>c__AnonStorey0
struct  U3CGetPrefabU3Ec__AnonStorey0_t1981674732  : public RuntimeObject
{
public:
	// Utage.AdvSelection Utage.AdvUguiSelectionManager/<GetPrefab>c__AnonStorey0::selectionData
	AdvSelection_t2301351953 * ___selectionData_0;

public:
	inline static int32_t get_offset_of_selectionData_0() { return static_cast<int32_t>(offsetof(U3CGetPrefabU3Ec__AnonStorey0_t1981674732, ___selectionData_0)); }
	inline AdvSelection_t2301351953 * get_selectionData_0() const { return ___selectionData_0; }
	inline AdvSelection_t2301351953 ** get_address_of_selectionData_0() { return &___selectionData_0; }
	inline void set_selectionData_0(AdvSelection_t2301351953 * value)
	{
		___selectionData_0 = value;
		Il2CppCodeGenWriteBarrier((&___selectionData_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CGETPREFABU3EC__ANONSTOREY0_T1981674732_H
#ifndef ADVGUIBASE_T2023941291_H
#define ADVGUIBASE_T2023941291_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.AdvGuiBase
struct  AdvGuiBase_t2023941291  : public RuntimeObject
{
public:
	// UnityEngine.GameObject Utage.AdvGuiBase::<Target>k__BackingField
	GameObject_t1756533147 * ___U3CTargetU3Ek__BackingField_0;
	// UnityEngine.RectTransform Utage.AdvGuiBase::rectTransform
	RectTransform_t3349966182 * ___rectTransform_1;
	// UnityEngine.Canvas Utage.AdvGuiBase::canvas
	Canvas_t209405766 * ___canvas_2;
	// UnityEngine.RectTransform Utage.AdvGuiBase::canvasRectTransform
	RectTransform_t3349966182 * ___canvasRectTransform_3;
	// System.Boolean Utage.AdvGuiBase::<HasChanged>k__BackingField
	bool ___U3CHasChangedU3Ek__BackingField_4;
	// System.Byte[] Utage.AdvGuiBase::defaultData
	ByteU5BU5D_t3397334013* ___defaultData_5;

public:
	inline static int32_t get_offset_of_U3CTargetU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(AdvGuiBase_t2023941291, ___U3CTargetU3Ek__BackingField_0)); }
	inline GameObject_t1756533147 * get_U3CTargetU3Ek__BackingField_0() const { return ___U3CTargetU3Ek__BackingField_0; }
	inline GameObject_t1756533147 ** get_address_of_U3CTargetU3Ek__BackingField_0() { return &___U3CTargetU3Ek__BackingField_0; }
	inline void set_U3CTargetU3Ek__BackingField_0(GameObject_t1756533147 * value)
	{
		___U3CTargetU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CTargetU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_rectTransform_1() { return static_cast<int32_t>(offsetof(AdvGuiBase_t2023941291, ___rectTransform_1)); }
	inline RectTransform_t3349966182 * get_rectTransform_1() const { return ___rectTransform_1; }
	inline RectTransform_t3349966182 ** get_address_of_rectTransform_1() { return &___rectTransform_1; }
	inline void set_rectTransform_1(RectTransform_t3349966182 * value)
	{
		___rectTransform_1 = value;
		Il2CppCodeGenWriteBarrier((&___rectTransform_1), value);
	}

	inline static int32_t get_offset_of_canvas_2() { return static_cast<int32_t>(offsetof(AdvGuiBase_t2023941291, ___canvas_2)); }
	inline Canvas_t209405766 * get_canvas_2() const { return ___canvas_2; }
	inline Canvas_t209405766 ** get_address_of_canvas_2() { return &___canvas_2; }
	inline void set_canvas_2(Canvas_t209405766 * value)
	{
		___canvas_2 = value;
		Il2CppCodeGenWriteBarrier((&___canvas_2), value);
	}

	inline static int32_t get_offset_of_canvasRectTransform_3() { return static_cast<int32_t>(offsetof(AdvGuiBase_t2023941291, ___canvasRectTransform_3)); }
	inline RectTransform_t3349966182 * get_canvasRectTransform_3() const { return ___canvasRectTransform_3; }
	inline RectTransform_t3349966182 ** get_address_of_canvasRectTransform_3() { return &___canvasRectTransform_3; }
	inline void set_canvasRectTransform_3(RectTransform_t3349966182 * value)
	{
		___canvasRectTransform_3 = value;
		Il2CppCodeGenWriteBarrier((&___canvasRectTransform_3), value);
	}

	inline static int32_t get_offset_of_U3CHasChangedU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(AdvGuiBase_t2023941291, ___U3CHasChangedU3Ek__BackingField_4)); }
	inline bool get_U3CHasChangedU3Ek__BackingField_4() const { return ___U3CHasChangedU3Ek__BackingField_4; }
	inline bool* get_address_of_U3CHasChangedU3Ek__BackingField_4() { return &___U3CHasChangedU3Ek__BackingField_4; }
	inline void set_U3CHasChangedU3Ek__BackingField_4(bool value)
	{
		___U3CHasChangedU3Ek__BackingField_4 = value;
	}

	inline static int32_t get_offset_of_defaultData_5() { return static_cast<int32_t>(offsetof(AdvGuiBase_t2023941291, ___defaultData_5)); }
	inline ByteU5BU5D_t3397334013* get_defaultData_5() const { return ___defaultData_5; }
	inline ByteU5BU5D_t3397334013** get_address_of_defaultData_5() { return &___defaultData_5; }
	inline void set_defaultData_5(ByteU5BU5D_t3397334013* value)
	{
		___defaultData_5 = value;
		Il2CppCodeGenWriteBarrier((&___defaultData_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ADVGUIBASE_T2023941291_H
#ifndef LANGUAGESTRINGS_T2372834651_H
#define LANGUAGESTRINGS_T2372834651_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.LanguageData/LanguageStrings
struct  LanguageStrings_t2372834651  : public RuntimeObject
{
public:
	// System.Collections.Generic.List`1<System.String> Utage.LanguageData/LanguageStrings::<Strings>k__BackingField
	List_1_t1398341365 * ___U3CStringsU3Ek__BackingField_0;

public:
	inline static int32_t get_offset_of_U3CStringsU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(LanguageStrings_t2372834651, ___U3CStringsU3Ek__BackingField_0)); }
	inline List_1_t1398341365 * get_U3CStringsU3Ek__BackingField_0() const { return ___U3CStringsU3Ek__BackingField_0; }
	inline List_1_t1398341365 ** get_address_of_U3CStringsU3Ek__BackingField_0() { return &___U3CStringsU3Ek__BackingField_0; }
	inline void set_U3CStringsU3Ek__BackingField_0(List_1_t1398341365 * value)
	{
		___U3CStringsU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CStringsU3Ek__BackingField_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LANGUAGESTRINGS_T2372834651_H
#ifndef ATTRIBUTE_T542643598_H
#define ATTRIBUTE_T542643598_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Attribute
struct  Attribute_t542643598  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ATTRIBUTE_T542643598_H
#ifndef SERIALIZABLEDICTIONARY_1_T3054077954_H
#define SERIALIZABLEDICTIONARY_1_T3054077954_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.SerializableDictionary`1<Utage.DictionaryKeyValueString>
struct  SerializableDictionary_1_t3054077954  : public RuntimeObject
{
public:
	// System.Collections.Generic.List`1<T> Utage.SerializableDictionary`1::list
	List_1_t456006635 * ___list_0;
	// System.Collections.Generic.Dictionary`2<System.String,T> Utage.SerializableDictionary`1::dictionary
	Dictionary_2_t3001664765 * ___dictionary_1;

public:
	inline static int32_t get_offset_of_list_0() { return static_cast<int32_t>(offsetof(SerializableDictionary_1_t3054077954, ___list_0)); }
	inline List_1_t456006635 * get_list_0() const { return ___list_0; }
	inline List_1_t456006635 ** get_address_of_list_0() { return &___list_0; }
	inline void set_list_0(List_1_t456006635 * value)
	{
		___list_0 = value;
		Il2CppCodeGenWriteBarrier((&___list_0), value);
	}

	inline static int32_t get_offset_of_dictionary_1() { return static_cast<int32_t>(offsetof(SerializableDictionary_1_t3054077954, ___dictionary_1)); }
	inline Dictionary_2_t3001664765 * get_dictionary_1() const { return ___dictionary_1; }
	inline Dictionary_2_t3001664765 ** get_address_of_dictionary_1() { return &___dictionary_1; }
	inline void set_dictionary_1(Dictionary_2_t3001664765 * value)
	{
		___dictionary_1 = value;
		Il2CppCodeGenWriteBarrier((&___dictionary_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SERIALIZABLEDICTIONARY_1_T3054077954_H
#ifndef SERIALIZABLEDICTIONARY_1_T3102794742_H
#define SERIALIZABLEDICTIONARY_1_T3102794742_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.SerializableDictionary`1<Utage.DictionaryKeyValueDouble>
struct  SerializableDictionary_1_t3102794742  : public RuntimeObject
{
public:
	// System.Collections.Generic.List`1<T> Utage.SerializableDictionary`1::list
	List_1_t504723423 * ___list_0;
	// System.Collections.Generic.Dictionary`2<System.String,T> Utage.SerializableDictionary`1::dictionary
	Dictionary_2_t3050381553 * ___dictionary_1;

public:
	inline static int32_t get_offset_of_list_0() { return static_cast<int32_t>(offsetof(SerializableDictionary_1_t3102794742, ___list_0)); }
	inline List_1_t504723423 * get_list_0() const { return ___list_0; }
	inline List_1_t504723423 ** get_address_of_list_0() { return &___list_0; }
	inline void set_list_0(List_1_t504723423 * value)
	{
		___list_0 = value;
		Il2CppCodeGenWriteBarrier((&___list_0), value);
	}

	inline static int32_t get_offset_of_dictionary_1() { return static_cast<int32_t>(offsetof(SerializableDictionary_1_t3102794742, ___dictionary_1)); }
	inline Dictionary_2_t3050381553 * get_dictionary_1() const { return ___dictionary_1; }
	inline Dictionary_2_t3050381553 ** get_address_of_dictionary_1() { return &___dictionary_1; }
	inline void set_dictionary_1(Dictionary_2_t3050381553 * value)
	{
		___dictionary_1 = value;
		Il2CppCodeGenWriteBarrier((&___dictionary_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SERIALIZABLEDICTIONARY_1_T3102794742_H
#ifndef SERIALIZABLEDICTIONARY_1_T1520377167_H
#define SERIALIZABLEDICTIONARY_1_T1520377167_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.SerializableDictionary`1<Utage.DictionaryKeyValueFloat>
struct  SerializableDictionary_1_t1520377167  : public RuntimeObject
{
public:
	// System.Collections.Generic.List`1<T> Utage.SerializableDictionary`1::list
	List_1_t3217273144 * ___list_0;
	// System.Collections.Generic.Dictionary`2<System.String,T> Utage.SerializableDictionary`1::dictionary
	Dictionary_2_t1467963978 * ___dictionary_1;

public:
	inline static int32_t get_offset_of_list_0() { return static_cast<int32_t>(offsetof(SerializableDictionary_1_t1520377167, ___list_0)); }
	inline List_1_t3217273144 * get_list_0() const { return ___list_0; }
	inline List_1_t3217273144 ** get_address_of_list_0() { return &___list_0; }
	inline void set_list_0(List_1_t3217273144 * value)
	{
		___list_0 = value;
		Il2CppCodeGenWriteBarrier((&___list_0), value);
	}

	inline static int32_t get_offset_of_dictionary_1() { return static_cast<int32_t>(offsetof(SerializableDictionary_1_t1520377167, ___dictionary_1)); }
	inline Dictionary_2_t1467963978 * get_dictionary_1() const { return ___dictionary_1; }
	inline Dictionary_2_t1467963978 ** get_address_of_dictionary_1() { return &___dictionary_1; }
	inline void set_dictionary_1(Dictionary_2_t1467963978 * value)
	{
		___dictionary_1 = value;
		Il2CppCodeGenWriteBarrier((&___dictionary_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SERIALIZABLEDICTIONARY_1_T1520377167_H
#ifndef SERIALIZABLEDICTIONARY_1_T1264319476_H
#define SERIALIZABLEDICTIONARY_1_T1264319476_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.SerializableDictionary`1<Utage.DictionaryKeyValueInt>
struct  SerializableDictionary_1_t1264319476  : public RuntimeObject
{
public:
	// System.Collections.Generic.List`1<T> Utage.SerializableDictionary`1::list
	List_1_t2961215453 * ___list_0;
	// System.Collections.Generic.Dictionary`2<System.String,T> Utage.SerializableDictionary`1::dictionary
	Dictionary_2_t1211906287 * ___dictionary_1;

public:
	inline static int32_t get_offset_of_list_0() { return static_cast<int32_t>(offsetof(SerializableDictionary_1_t1264319476, ___list_0)); }
	inline List_1_t2961215453 * get_list_0() const { return ___list_0; }
	inline List_1_t2961215453 ** get_address_of_list_0() { return &___list_0; }
	inline void set_list_0(List_1_t2961215453 * value)
	{
		___list_0 = value;
		Il2CppCodeGenWriteBarrier((&___list_0), value);
	}

	inline static int32_t get_offset_of_dictionary_1() { return static_cast<int32_t>(offsetof(SerializableDictionary_1_t1264319476, ___dictionary_1)); }
	inline Dictionary_2_t1211906287 * get_dictionary_1() const { return ___dictionary_1; }
	inline Dictionary_2_t1211906287 ** get_address_of_dictionary_1() { return &___dictionary_1; }
	inline void set_dictionary_1(Dictionary_2_t1211906287 * value)
	{
		___dictionary_1 = value;
		Il2CppCodeGenWriteBarrier((&___dictionary_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SERIALIZABLEDICTIONARY_1_T1264319476_H
#ifndef SERIALIZABLEDICTIONARY_1_T616711741_H
#define SERIALIZABLEDICTIONARY_1_T616711741_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.SerializableDictionary`1<Utage.DictionaryKeyValueBool>
struct  SerializableDictionary_1_t616711741  : public RuntimeObject
{
public:
	// System.Collections.Generic.List`1<T> Utage.SerializableDictionary`1::list
	List_1_t2313607718 * ___list_0;
	// System.Collections.Generic.Dictionary`2<System.String,T> Utage.SerializableDictionary`1::dictionary
	Dictionary_2_t564298552 * ___dictionary_1;

public:
	inline static int32_t get_offset_of_list_0() { return static_cast<int32_t>(offsetof(SerializableDictionary_1_t616711741, ___list_0)); }
	inline List_1_t2313607718 * get_list_0() const { return ___list_0; }
	inline List_1_t2313607718 ** get_address_of_list_0() { return &___list_0; }
	inline void set_list_0(List_1_t2313607718 * value)
	{
		___list_0 = value;
		Il2CppCodeGenWriteBarrier((&___list_0), value);
	}

	inline static int32_t get_offset_of_dictionary_1() { return static_cast<int32_t>(offsetof(SerializableDictionary_1_t616711741, ___dictionary_1)); }
	inline Dictionary_2_t564298552 * get_dictionary_1() const { return ___dictionary_1; }
	inline Dictionary_2_t564298552 ** get_address_of_dictionary_1() { return &___dictionary_1; }
	inline void set_dictionary_1(Dictionary_2_t564298552 * value)
	{
		___dictionary_1 = value;
		Il2CppCodeGenWriteBarrier((&___dictionary_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SERIALIZABLEDICTIONARY_1_T616711741_H
#ifndef LANGUAGESYSTEMTEXT_T2944673402_H
#define LANGUAGESYSTEMTEXT_T2944673402_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.LanguageSystemText
struct  LanguageSystemText_t2944673402  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LANGUAGESYSTEMTEXT_T2944673402_H
#ifndef SERIALIZABLEDICTIONARYKEYVALUE_T1605592419_H
#define SERIALIZABLEDICTIONARYKEYVALUE_T1605592419_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.SerializableDictionaryKeyValue
struct  SerializableDictionaryKeyValue_t1605592419  : public RuntimeObject
{
public:
	// System.String Utage.SerializableDictionaryKeyValue::key
	String_t* ___key_0;

public:
	inline static int32_t get_offset_of_key_0() { return static_cast<int32_t>(offsetof(SerializableDictionaryKeyValue_t1605592419, ___key_0)); }
	inline String_t* get_key_0() const { return ___key_0; }
	inline String_t** get_address_of_key_0() { return &___key_0; }
	inline void set_key_0(String_t* value)
	{
		___key_0 = value;
		Il2CppCodeGenWriteBarrier((&___key_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SERIALIZABLEDICTIONARYKEYVALUE_T1605592419_H
#ifndef MINMAX_1_T926580158_H
#define MINMAX_1_T926580158_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.MinMax`1<System.Single>
struct  MinMax_1_t926580158  : public RuntimeObject
{
public:
	// T Utage.MinMax`1::min
	float ___min_0;
	// T Utage.MinMax`1::max
	float ___max_1;

public:
	inline static int32_t get_offset_of_min_0() { return static_cast<int32_t>(offsetof(MinMax_1_t926580158, ___min_0)); }
	inline float get_min_0() const { return ___min_0; }
	inline float* get_address_of_min_0() { return &___min_0; }
	inline void set_min_0(float value)
	{
		___min_0 = value;
	}

	inline static int32_t get_offset_of_max_1() { return static_cast<int32_t>(offsetof(MinMax_1_t926580158, ___max_1)); }
	inline float get_max_1() const { return ___max_1; }
	inline float* get_address_of_max_1() { return &___max_1; }
	inline void set_max_1(float value)
	{
		___max_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MINMAX_1_T926580158_H
#ifndef MINMAX_1_T921947674_H
#define MINMAX_1_T921947674_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.MinMax`1<System.Int32>
struct  MinMax_1_t921947674  : public RuntimeObject
{
public:
	// T Utage.MinMax`1::min
	int32_t ___min_0;
	// T Utage.MinMax`1::max
	int32_t ___max_1;

public:
	inline static int32_t get_offset_of_min_0() { return static_cast<int32_t>(offsetof(MinMax_1_t921947674, ___min_0)); }
	inline int32_t get_min_0() const { return ___min_0; }
	inline int32_t* get_address_of_min_0() { return &___min_0; }
	inline void set_min_0(int32_t value)
	{
		___min_0 = value;
	}

	inline static int32_t get_offset_of_max_1() { return static_cast<int32_t>(offsetof(MinMax_1_t921947674, ___max_1)); }
	inline int32_t get_max_1() const { return ___max_1; }
	inline int32_t* get_address_of_max_1() { return &___max_1; }
	inline void set_max_1(int32_t value)
	{
		___max_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MINMAX_1_T921947674_H
#ifndef UNITYEVENTBASE_T828812576_H
#define UNITYEVENTBASE_T828812576_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Events.UnityEventBase
struct  UnityEventBase_t828812576  : public RuntimeObject
{
public:
	// UnityEngine.Events.InvokableCallList UnityEngine.Events.UnityEventBase::m_Calls
	InvokableCallList_t2295673753 * ___m_Calls_0;
	// UnityEngine.Events.PersistentCallGroup UnityEngine.Events.UnityEventBase::m_PersistentCalls
	PersistentCallGroup_t339478082 * ___m_PersistentCalls_1;
	// System.String UnityEngine.Events.UnityEventBase::m_TypeName
	String_t* ___m_TypeName_2;
	// System.Boolean UnityEngine.Events.UnityEventBase::m_CallsDirty
	bool ___m_CallsDirty_3;

public:
	inline static int32_t get_offset_of_m_Calls_0() { return static_cast<int32_t>(offsetof(UnityEventBase_t828812576, ___m_Calls_0)); }
	inline InvokableCallList_t2295673753 * get_m_Calls_0() const { return ___m_Calls_0; }
	inline InvokableCallList_t2295673753 ** get_address_of_m_Calls_0() { return &___m_Calls_0; }
	inline void set_m_Calls_0(InvokableCallList_t2295673753 * value)
	{
		___m_Calls_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_Calls_0), value);
	}

	inline static int32_t get_offset_of_m_PersistentCalls_1() { return static_cast<int32_t>(offsetof(UnityEventBase_t828812576, ___m_PersistentCalls_1)); }
	inline PersistentCallGroup_t339478082 * get_m_PersistentCalls_1() const { return ___m_PersistentCalls_1; }
	inline PersistentCallGroup_t339478082 ** get_address_of_m_PersistentCalls_1() { return &___m_PersistentCalls_1; }
	inline void set_m_PersistentCalls_1(PersistentCallGroup_t339478082 * value)
	{
		___m_PersistentCalls_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_PersistentCalls_1), value);
	}

	inline static int32_t get_offset_of_m_TypeName_2() { return static_cast<int32_t>(offsetof(UnityEventBase_t828812576, ___m_TypeName_2)); }
	inline String_t* get_m_TypeName_2() const { return ___m_TypeName_2; }
	inline String_t** get_address_of_m_TypeName_2() { return &___m_TypeName_2; }
	inline void set_m_TypeName_2(String_t* value)
	{
		___m_TypeName_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_TypeName_2), value);
	}

	inline static int32_t get_offset_of_m_CallsDirty_3() { return static_cast<int32_t>(offsetof(UnityEventBase_t828812576, ___m_CallsDirty_3)); }
	inline bool get_m_CallsDirty_3() const { return ___m_CallsDirty_3; }
	inline bool* get_address_of_m_CallsDirty_3() { return &___m_CallsDirty_3; }
	inline void set_m_CallsDirty_3(bool value)
	{
		___m_CallsDirty_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYEVENTBASE_T828812576_H
#ifndef VALUETYPE_T3507792607_H
#define VALUETYPE_T3507792607_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ValueType
struct  ValueType_t3507792607  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t3507792607_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t3507792607_marshaled_com
{
};
#endif // VALUETYPE_T3507792607_H
#ifndef U3CCOSTARTSAVEDATAU3EC__ITERATOR0_T3464725285_H
#define U3CCOSTARTSAVEDATAU3EC__ITERATOR0_T3464725285_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.AdvScenarioPlayer/<CoStartSaveData>c__Iterator0
struct  U3CCoStartSaveDataU3Ec__Iterator0_t3464725285  : public RuntimeObject
{
public:
	// Utage.AdvSaveData Utage.AdvScenarioPlayer/<CoStartSaveData>c__Iterator0::saveData
	AdvSaveData_t4059487466 * ___saveData_0;
	// Utage.AdvScenarioPlayer Utage.AdvScenarioPlayer/<CoStartSaveData>c__Iterator0::$this
	AdvScenarioPlayer_t1295831480 * ___U24this_1;
	// System.Object Utage.AdvScenarioPlayer/<CoStartSaveData>c__Iterator0::$current
	RuntimeObject * ___U24current_2;
	// System.Boolean Utage.AdvScenarioPlayer/<CoStartSaveData>c__Iterator0::$disposing
	bool ___U24disposing_3;
	// System.Int32 Utage.AdvScenarioPlayer/<CoStartSaveData>c__Iterator0::$PC
	int32_t ___U24PC_4;

public:
	inline static int32_t get_offset_of_saveData_0() { return static_cast<int32_t>(offsetof(U3CCoStartSaveDataU3Ec__Iterator0_t3464725285, ___saveData_0)); }
	inline AdvSaveData_t4059487466 * get_saveData_0() const { return ___saveData_0; }
	inline AdvSaveData_t4059487466 ** get_address_of_saveData_0() { return &___saveData_0; }
	inline void set_saveData_0(AdvSaveData_t4059487466 * value)
	{
		___saveData_0 = value;
		Il2CppCodeGenWriteBarrier((&___saveData_0), value);
	}

	inline static int32_t get_offset_of_U24this_1() { return static_cast<int32_t>(offsetof(U3CCoStartSaveDataU3Ec__Iterator0_t3464725285, ___U24this_1)); }
	inline AdvScenarioPlayer_t1295831480 * get_U24this_1() const { return ___U24this_1; }
	inline AdvScenarioPlayer_t1295831480 ** get_address_of_U24this_1() { return &___U24this_1; }
	inline void set_U24this_1(AdvScenarioPlayer_t1295831480 * value)
	{
		___U24this_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_1), value);
	}

	inline static int32_t get_offset_of_U24current_2() { return static_cast<int32_t>(offsetof(U3CCoStartSaveDataU3Ec__Iterator0_t3464725285, ___U24current_2)); }
	inline RuntimeObject * get_U24current_2() const { return ___U24current_2; }
	inline RuntimeObject ** get_address_of_U24current_2() { return &___U24current_2; }
	inline void set_U24current_2(RuntimeObject * value)
	{
		___U24current_2 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_2), value);
	}

	inline static int32_t get_offset_of_U24disposing_3() { return static_cast<int32_t>(offsetof(U3CCoStartSaveDataU3Ec__Iterator0_t3464725285, ___U24disposing_3)); }
	inline bool get_U24disposing_3() const { return ___U24disposing_3; }
	inline bool* get_address_of_U24disposing_3() { return &___U24disposing_3; }
	inline void set_U24disposing_3(bool value)
	{
		___U24disposing_3 = value;
	}

	inline static int32_t get_offset_of_U24PC_4() { return static_cast<int32_t>(offsetof(U3CCoStartSaveDataU3Ec__Iterator0_t3464725285, ___U24PC_4)); }
	inline int32_t get_U24PC_4() const { return ___U24PC_4; }
	inline int32_t* get_address_of_U24PC_4() { return &___U24PC_4; }
	inline void set_U24PC_4(int32_t value)
	{
		___U24PC_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CCOSTARTSAVEDATAU3EC__ITERATOR0_T3464725285_H
#ifndef U3CINITU3EC__ANONSTOREY1_T1155527497_H
#define U3CINITU3EC__ANONSTOREY1_T1155527497_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.AdvUguiBacklog/<Init>c__AnonStorey1
struct  U3CInitU3Ec__AnonStorey1_t1155527497  : public RuntimeObject
{
public:
	// Utage.AdvBacklog Utage.AdvUguiBacklog/<Init>c__AnonStorey1::data
	AdvBacklog_t3927301290 * ___data_0;
	// Utage.AdvUguiBacklog Utage.AdvUguiBacklog/<Init>c__AnonStorey1::$this
	AdvUguiBacklog_t1854927244 * ___U24this_1;

public:
	inline static int32_t get_offset_of_data_0() { return static_cast<int32_t>(offsetof(U3CInitU3Ec__AnonStorey1_t1155527497, ___data_0)); }
	inline AdvBacklog_t3927301290 * get_data_0() const { return ___data_0; }
	inline AdvBacklog_t3927301290 ** get_address_of_data_0() { return &___data_0; }
	inline void set_data_0(AdvBacklog_t3927301290 * value)
	{
		___data_0 = value;
		Il2CppCodeGenWriteBarrier((&___data_0), value);
	}

	inline static int32_t get_offset_of_U24this_1() { return static_cast<int32_t>(offsetof(U3CInitU3Ec__AnonStorey1_t1155527497, ___U24this_1)); }
	inline AdvUguiBacklog_t1854927244 * get_U24this_1() const { return ___U24this_1; }
	inline AdvUguiBacklog_t1854927244 ** get_address_of_U24this_1() { return &___U24this_1; }
	inline void set_U24this_1(AdvUguiBacklog_t1854927244 * value)
	{
		___U24this_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CINITU3EC__ANONSTOREY1_T1155527497_H
#ifndef U3CGETJUMPSCENARIOLABELDATALISTU3EC__ANONSTOREY0_T3855642138_H
#define U3CGETJUMPSCENARIOLABELDATALISTU3EC__ANONSTOREY0_T3855642138_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.AdvScenarioPageData/<GetJumpScenarioLabelDataList>c__AnonStorey0
struct  U3CGetJumpScenarioLabelDataListU3Ec__AnonStorey0_t3855642138  : public RuntimeObject
{
public:
	// Utage.AdvDataManager Utage.AdvScenarioPageData/<GetJumpScenarioLabelDataList>c__AnonStorey0::dataManager
	AdvDataManager_t2481232830 * ___dataManager_0;
	// Utage.AdvScenarioPageData Utage.AdvScenarioPageData/<GetJumpScenarioLabelDataList>c__AnonStorey0::$this
	AdvScenarioPageData_t3333166790 * ___U24this_1;

public:
	inline static int32_t get_offset_of_dataManager_0() { return static_cast<int32_t>(offsetof(U3CGetJumpScenarioLabelDataListU3Ec__AnonStorey0_t3855642138, ___dataManager_0)); }
	inline AdvDataManager_t2481232830 * get_dataManager_0() const { return ___dataManager_0; }
	inline AdvDataManager_t2481232830 ** get_address_of_dataManager_0() { return &___dataManager_0; }
	inline void set_dataManager_0(AdvDataManager_t2481232830 * value)
	{
		___dataManager_0 = value;
		Il2CppCodeGenWriteBarrier((&___dataManager_0), value);
	}

	inline static int32_t get_offset_of_U24this_1() { return static_cast<int32_t>(offsetof(U3CGetJumpScenarioLabelDataListU3Ec__AnonStorey0_t3855642138, ___U24this_1)); }
	inline AdvScenarioPageData_t3333166790 * get_U24this_1() const { return ___U24this_1; }
	inline AdvScenarioPageData_t3333166790 ** get_address_of_U24this_1() { return &___U24this_1; }
	inline void set_U24this_1(AdvScenarioPageData_t3333166790 * value)
	{
		___U24this_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CGETJUMPSCENARIOLABELDATALISTU3EC__ANONSTOREY0_T3855642138_H
#ifndef U3CGETAUTOJUMPLABELSU3EC__ANONSTOREY1_T2589819677_H
#define U3CGETAUTOJUMPLABELSU3EC__ANONSTOREY1_T2589819677_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.AdvScenarioPageData/<GetAutoJumpLabels>c__AnonStorey1
struct  U3CGetAutoJumpLabelsU3Ec__AnonStorey1_t2589819677  : public RuntimeObject
{
public:
	// Utage.AdvDataManager Utage.AdvScenarioPageData/<GetAutoJumpLabels>c__AnonStorey1::dataManager
	AdvDataManager_t2481232830 * ___dataManager_0;
	// Utage.AdvScenarioPageData Utage.AdvScenarioPageData/<GetAutoJumpLabels>c__AnonStorey1::$this
	AdvScenarioPageData_t3333166790 * ___U24this_1;

public:
	inline static int32_t get_offset_of_dataManager_0() { return static_cast<int32_t>(offsetof(U3CGetAutoJumpLabelsU3Ec__AnonStorey1_t2589819677, ___dataManager_0)); }
	inline AdvDataManager_t2481232830 * get_dataManager_0() const { return ___dataManager_0; }
	inline AdvDataManager_t2481232830 ** get_address_of_dataManager_0() { return &___dataManager_0; }
	inline void set_dataManager_0(AdvDataManager_t2481232830 * value)
	{
		___dataManager_0 = value;
		Il2CppCodeGenWriteBarrier((&___dataManager_0), value);
	}

	inline static int32_t get_offset_of_U24this_1() { return static_cast<int32_t>(offsetof(U3CGetAutoJumpLabelsU3Ec__AnonStorey1_t2589819677, ___U24this_1)); }
	inline AdvScenarioPageData_t3333166790 * get_U24this_1() const { return ___U24this_1; }
	inline AdvScenarioPageData_t3333166790 ** get_address_of_U24this_1() { return &___U24this_1; }
	inline void set_U24this_1(AdvScenarioPageData_t3333166790 * value)
	{
		___U24this_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CGETAUTOJUMPLABELSU3EC__ANONSTOREY1_T2589819677_H
#ifndef ADVWAITMANAGER_T2981197357_H
#define ADVWAITMANAGER_T2981197357_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.AdvWaitManager
struct  AdvWaitManager_t2981197357  : public RuntimeObject
{
public:
	// System.Collections.Generic.List`1<Utage.AdvCommandWaitBase> Utage.AdvWaitManager::commandList
	List_1_t3542275616 * ___commandList_0;

public:
	inline static int32_t get_offset_of_commandList_0() { return static_cast<int32_t>(offsetof(AdvWaitManager_t2981197357, ___commandList_0)); }
	inline List_1_t3542275616 * get_commandList_0() const { return ___commandList_0; }
	inline List_1_t3542275616 ** get_address_of_commandList_0() { return &___commandList_0; }
	inline void set_commandList_0(List_1_t3542275616 * value)
	{
		___commandList_0 = value;
		Il2CppCodeGenWriteBarrier((&___commandList_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ADVWAITMANAGER_T2981197357_H
#ifndef U3CDOWNLOADU3EC__ANONSTOREY2_T2119668604_H
#define U3CDOWNLOADU3EC__ANONSTOREY2_T2119668604_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.AdvScenarioPageData/<Download>c__AnonStorey2
struct  U3CDownloadU3Ec__AnonStorey2_t2119668604  : public RuntimeObject
{
public:
	// Utage.AdvDataManager Utage.AdvScenarioPageData/<Download>c__AnonStorey2::dataManager
	AdvDataManager_t2481232830 * ___dataManager_0;

public:
	inline static int32_t get_offset_of_dataManager_0() { return static_cast<int32_t>(offsetof(U3CDownloadU3Ec__AnonStorey2_t2119668604, ___dataManager_0)); }
	inline AdvDataManager_t2481232830 * get_dataManager_0() const { return ___dataManager_0; }
	inline AdvDataManager_t2481232830 ** get_address_of_dataManager_0() { return &___dataManager_0; }
	inline void set_dataManager_0(AdvDataManager_t2481232830 * value)
	{
		___dataManager_0 = value;
		Il2CppCodeGenWriteBarrier((&___dataManager_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CDOWNLOADU3EC__ANONSTOREY2_T2119668604_H
#ifndef U3CCOSTARTPAGEU3EC__ITERATOR1_T1196900359_H
#define U3CCOSTARTPAGEU3EC__ITERATOR1_T1196900359_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.AdvScenarioThread/<CoStartPage>c__Iterator1
struct  U3CCoStartPageU3Ec__Iterator1_t1196900359  : public RuntimeObject
{
public:
	// System.Boolean Utage.AdvScenarioThread/<CoStartPage>c__Iterator1::skipPageHeaer
	bool ___skipPageHeaer_0;
	// Utage.AdvScenarioPageData Utage.AdvScenarioThread/<CoStartPage>c__Iterator1::pageData
	AdvScenarioPageData_t3333166790 * ___pageData_1;
	// System.Int32 Utage.AdvScenarioThread/<CoStartPage>c__Iterator1::<index>__0
	int32_t ___U3CindexU3E__0_2;
	// Utage.AdvCommand Utage.AdvScenarioThread/<CoStartPage>c__Iterator1::<command>__0
	AdvCommand_t2859960984 * ___U3CcommandU3E__0_3;
	// Utage.AdvCommand Utage.AdvScenarioThread/<CoStartPage>c__Iterator1::returnToCommand
	AdvCommand_t2859960984 * ___returnToCommand_4;
	// Utage.AdvScenarioLabelData Utage.AdvScenarioThread/<CoStartPage>c__Iterator1::labelData
	AdvScenarioLabelData_t2266689885 * ___labelData_5;
	// Utage.AdvScenarioThread Utage.AdvScenarioThread/<CoStartPage>c__Iterator1::$this
	AdvScenarioThread_t1270526825 * ___U24this_6;
	// System.Object Utage.AdvScenarioThread/<CoStartPage>c__Iterator1::$current
	RuntimeObject * ___U24current_7;
	// System.Boolean Utage.AdvScenarioThread/<CoStartPage>c__Iterator1::$disposing
	bool ___U24disposing_8;
	// System.Int32 Utage.AdvScenarioThread/<CoStartPage>c__Iterator1::$PC
	int32_t ___U24PC_9;

public:
	inline static int32_t get_offset_of_skipPageHeaer_0() { return static_cast<int32_t>(offsetof(U3CCoStartPageU3Ec__Iterator1_t1196900359, ___skipPageHeaer_0)); }
	inline bool get_skipPageHeaer_0() const { return ___skipPageHeaer_0; }
	inline bool* get_address_of_skipPageHeaer_0() { return &___skipPageHeaer_0; }
	inline void set_skipPageHeaer_0(bool value)
	{
		___skipPageHeaer_0 = value;
	}

	inline static int32_t get_offset_of_pageData_1() { return static_cast<int32_t>(offsetof(U3CCoStartPageU3Ec__Iterator1_t1196900359, ___pageData_1)); }
	inline AdvScenarioPageData_t3333166790 * get_pageData_1() const { return ___pageData_1; }
	inline AdvScenarioPageData_t3333166790 ** get_address_of_pageData_1() { return &___pageData_1; }
	inline void set_pageData_1(AdvScenarioPageData_t3333166790 * value)
	{
		___pageData_1 = value;
		Il2CppCodeGenWriteBarrier((&___pageData_1), value);
	}

	inline static int32_t get_offset_of_U3CindexU3E__0_2() { return static_cast<int32_t>(offsetof(U3CCoStartPageU3Ec__Iterator1_t1196900359, ___U3CindexU3E__0_2)); }
	inline int32_t get_U3CindexU3E__0_2() const { return ___U3CindexU3E__0_2; }
	inline int32_t* get_address_of_U3CindexU3E__0_2() { return &___U3CindexU3E__0_2; }
	inline void set_U3CindexU3E__0_2(int32_t value)
	{
		___U3CindexU3E__0_2 = value;
	}

	inline static int32_t get_offset_of_U3CcommandU3E__0_3() { return static_cast<int32_t>(offsetof(U3CCoStartPageU3Ec__Iterator1_t1196900359, ___U3CcommandU3E__0_3)); }
	inline AdvCommand_t2859960984 * get_U3CcommandU3E__0_3() const { return ___U3CcommandU3E__0_3; }
	inline AdvCommand_t2859960984 ** get_address_of_U3CcommandU3E__0_3() { return &___U3CcommandU3E__0_3; }
	inline void set_U3CcommandU3E__0_3(AdvCommand_t2859960984 * value)
	{
		___U3CcommandU3E__0_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CcommandU3E__0_3), value);
	}

	inline static int32_t get_offset_of_returnToCommand_4() { return static_cast<int32_t>(offsetof(U3CCoStartPageU3Ec__Iterator1_t1196900359, ___returnToCommand_4)); }
	inline AdvCommand_t2859960984 * get_returnToCommand_4() const { return ___returnToCommand_4; }
	inline AdvCommand_t2859960984 ** get_address_of_returnToCommand_4() { return &___returnToCommand_4; }
	inline void set_returnToCommand_4(AdvCommand_t2859960984 * value)
	{
		___returnToCommand_4 = value;
		Il2CppCodeGenWriteBarrier((&___returnToCommand_4), value);
	}

	inline static int32_t get_offset_of_labelData_5() { return static_cast<int32_t>(offsetof(U3CCoStartPageU3Ec__Iterator1_t1196900359, ___labelData_5)); }
	inline AdvScenarioLabelData_t2266689885 * get_labelData_5() const { return ___labelData_5; }
	inline AdvScenarioLabelData_t2266689885 ** get_address_of_labelData_5() { return &___labelData_5; }
	inline void set_labelData_5(AdvScenarioLabelData_t2266689885 * value)
	{
		___labelData_5 = value;
		Il2CppCodeGenWriteBarrier((&___labelData_5), value);
	}

	inline static int32_t get_offset_of_U24this_6() { return static_cast<int32_t>(offsetof(U3CCoStartPageU3Ec__Iterator1_t1196900359, ___U24this_6)); }
	inline AdvScenarioThread_t1270526825 * get_U24this_6() const { return ___U24this_6; }
	inline AdvScenarioThread_t1270526825 ** get_address_of_U24this_6() { return &___U24this_6; }
	inline void set_U24this_6(AdvScenarioThread_t1270526825 * value)
	{
		___U24this_6 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_6), value);
	}

	inline static int32_t get_offset_of_U24current_7() { return static_cast<int32_t>(offsetof(U3CCoStartPageU3Ec__Iterator1_t1196900359, ___U24current_7)); }
	inline RuntimeObject * get_U24current_7() const { return ___U24current_7; }
	inline RuntimeObject ** get_address_of_U24current_7() { return &___U24current_7; }
	inline void set_U24current_7(RuntimeObject * value)
	{
		___U24current_7 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_7), value);
	}

	inline static int32_t get_offset_of_U24disposing_8() { return static_cast<int32_t>(offsetof(U3CCoStartPageU3Ec__Iterator1_t1196900359, ___U24disposing_8)); }
	inline bool get_U24disposing_8() const { return ___U24disposing_8; }
	inline bool* get_address_of_U24disposing_8() { return &___U24disposing_8; }
	inline void set_U24disposing_8(bool value)
	{
		___U24disposing_8 = value;
	}

	inline static int32_t get_offset_of_U24PC_9() { return static_cast<int32_t>(offsetof(U3CCoStartPageU3Ec__Iterator1_t1196900359, ___U24PC_9)); }
	inline int32_t get_U24PC_9() const { return ___U24PC_9; }
	inline int32_t* get_address_of_U24PC_9() { return &___U24PC_9; }
	inline void set_U24PC_9(int32_t value)
	{
		___U24PC_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CCOSTARTPAGEU3EC__ITERATOR1_T1196900359_H
#ifndef U3CCOSTARTSCENARIOU3EC__ITERATOR0_T1715464187_H
#define U3CCOSTARTSCENARIOU3EC__ITERATOR0_T1715464187_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.AdvScenarioThread/<CoStartScenario>c__Iterator0
struct  U3CCoStartScenarioU3Ec__Iterator0_t1715464187  : public RuntimeObject
{
public:
	// System.String Utage.AdvScenarioThread/<CoStartScenario>c__Iterator0::label
	String_t* ___label_0;
	// System.Int32 Utage.AdvScenarioThread/<CoStartScenario>c__Iterator0::page
	int32_t ___page_1;
	// Utage.AdvScenarioLabelData Utage.AdvScenarioThread/<CoStartScenario>c__Iterator0::<currentLabelData>__0
	AdvScenarioLabelData_t2266689885 * ___U3CcurrentLabelDataU3E__0_2;
	// Utage.AdvScenarioPageData Utage.AdvScenarioThread/<CoStartScenario>c__Iterator0::<currentPageData>__1
	AdvScenarioPageData_t3333166790 * ___U3CcurrentPageDataU3E__1_3;
	// Utage.AdvCommand Utage.AdvScenarioThread/<CoStartScenario>c__Iterator0::returnToCommand
	AdvCommand_t2859960984 * ___returnToCommand_4;
	// System.Boolean Utage.AdvScenarioThread/<CoStartScenario>c__Iterator0::skipPageHeaer
	bool ___skipPageHeaer_5;
	// UnityEngine.Coroutine Utage.AdvScenarioThread/<CoStartScenario>c__Iterator0::<pageCoroutine>__2
	Coroutine_t2299508840 * ___U3CpageCoroutineU3E__2_6;
	// Utage.AdvScenarioThread Utage.AdvScenarioThread/<CoStartScenario>c__Iterator0::$this
	AdvScenarioThread_t1270526825 * ___U24this_7;
	// System.Object Utage.AdvScenarioThread/<CoStartScenario>c__Iterator0::$current
	RuntimeObject * ___U24current_8;
	// System.Boolean Utage.AdvScenarioThread/<CoStartScenario>c__Iterator0::$disposing
	bool ___U24disposing_9;
	// System.Int32 Utage.AdvScenarioThread/<CoStartScenario>c__Iterator0::$PC
	int32_t ___U24PC_10;

public:
	inline static int32_t get_offset_of_label_0() { return static_cast<int32_t>(offsetof(U3CCoStartScenarioU3Ec__Iterator0_t1715464187, ___label_0)); }
	inline String_t* get_label_0() const { return ___label_0; }
	inline String_t** get_address_of_label_0() { return &___label_0; }
	inline void set_label_0(String_t* value)
	{
		___label_0 = value;
		Il2CppCodeGenWriteBarrier((&___label_0), value);
	}

	inline static int32_t get_offset_of_page_1() { return static_cast<int32_t>(offsetof(U3CCoStartScenarioU3Ec__Iterator0_t1715464187, ___page_1)); }
	inline int32_t get_page_1() const { return ___page_1; }
	inline int32_t* get_address_of_page_1() { return &___page_1; }
	inline void set_page_1(int32_t value)
	{
		___page_1 = value;
	}

	inline static int32_t get_offset_of_U3CcurrentLabelDataU3E__0_2() { return static_cast<int32_t>(offsetof(U3CCoStartScenarioU3Ec__Iterator0_t1715464187, ___U3CcurrentLabelDataU3E__0_2)); }
	inline AdvScenarioLabelData_t2266689885 * get_U3CcurrentLabelDataU3E__0_2() const { return ___U3CcurrentLabelDataU3E__0_2; }
	inline AdvScenarioLabelData_t2266689885 ** get_address_of_U3CcurrentLabelDataU3E__0_2() { return &___U3CcurrentLabelDataU3E__0_2; }
	inline void set_U3CcurrentLabelDataU3E__0_2(AdvScenarioLabelData_t2266689885 * value)
	{
		___U3CcurrentLabelDataU3E__0_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CcurrentLabelDataU3E__0_2), value);
	}

	inline static int32_t get_offset_of_U3CcurrentPageDataU3E__1_3() { return static_cast<int32_t>(offsetof(U3CCoStartScenarioU3Ec__Iterator0_t1715464187, ___U3CcurrentPageDataU3E__1_3)); }
	inline AdvScenarioPageData_t3333166790 * get_U3CcurrentPageDataU3E__1_3() const { return ___U3CcurrentPageDataU3E__1_3; }
	inline AdvScenarioPageData_t3333166790 ** get_address_of_U3CcurrentPageDataU3E__1_3() { return &___U3CcurrentPageDataU3E__1_3; }
	inline void set_U3CcurrentPageDataU3E__1_3(AdvScenarioPageData_t3333166790 * value)
	{
		___U3CcurrentPageDataU3E__1_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CcurrentPageDataU3E__1_3), value);
	}

	inline static int32_t get_offset_of_returnToCommand_4() { return static_cast<int32_t>(offsetof(U3CCoStartScenarioU3Ec__Iterator0_t1715464187, ___returnToCommand_4)); }
	inline AdvCommand_t2859960984 * get_returnToCommand_4() const { return ___returnToCommand_4; }
	inline AdvCommand_t2859960984 ** get_address_of_returnToCommand_4() { return &___returnToCommand_4; }
	inline void set_returnToCommand_4(AdvCommand_t2859960984 * value)
	{
		___returnToCommand_4 = value;
		Il2CppCodeGenWriteBarrier((&___returnToCommand_4), value);
	}

	inline static int32_t get_offset_of_skipPageHeaer_5() { return static_cast<int32_t>(offsetof(U3CCoStartScenarioU3Ec__Iterator0_t1715464187, ___skipPageHeaer_5)); }
	inline bool get_skipPageHeaer_5() const { return ___skipPageHeaer_5; }
	inline bool* get_address_of_skipPageHeaer_5() { return &___skipPageHeaer_5; }
	inline void set_skipPageHeaer_5(bool value)
	{
		___skipPageHeaer_5 = value;
	}

	inline static int32_t get_offset_of_U3CpageCoroutineU3E__2_6() { return static_cast<int32_t>(offsetof(U3CCoStartScenarioU3Ec__Iterator0_t1715464187, ___U3CpageCoroutineU3E__2_6)); }
	inline Coroutine_t2299508840 * get_U3CpageCoroutineU3E__2_6() const { return ___U3CpageCoroutineU3E__2_6; }
	inline Coroutine_t2299508840 ** get_address_of_U3CpageCoroutineU3E__2_6() { return &___U3CpageCoroutineU3E__2_6; }
	inline void set_U3CpageCoroutineU3E__2_6(Coroutine_t2299508840 * value)
	{
		___U3CpageCoroutineU3E__2_6 = value;
		Il2CppCodeGenWriteBarrier((&___U3CpageCoroutineU3E__2_6), value);
	}

	inline static int32_t get_offset_of_U24this_7() { return static_cast<int32_t>(offsetof(U3CCoStartScenarioU3Ec__Iterator0_t1715464187, ___U24this_7)); }
	inline AdvScenarioThread_t1270526825 * get_U24this_7() const { return ___U24this_7; }
	inline AdvScenarioThread_t1270526825 ** get_address_of_U24this_7() { return &___U24this_7; }
	inline void set_U24this_7(AdvScenarioThread_t1270526825 * value)
	{
		___U24this_7 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_7), value);
	}

	inline static int32_t get_offset_of_U24current_8() { return static_cast<int32_t>(offsetof(U3CCoStartScenarioU3Ec__Iterator0_t1715464187, ___U24current_8)); }
	inline RuntimeObject * get_U24current_8() const { return ___U24current_8; }
	inline RuntimeObject ** get_address_of_U24current_8() { return &___U24current_8; }
	inline void set_U24current_8(RuntimeObject * value)
	{
		___U24current_8 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_8), value);
	}

	inline static int32_t get_offset_of_U24disposing_9() { return static_cast<int32_t>(offsetof(U3CCoStartScenarioU3Ec__Iterator0_t1715464187, ___U24disposing_9)); }
	inline bool get_U24disposing_9() const { return ___U24disposing_9; }
	inline bool* get_address_of_U24disposing_9() { return &___U24disposing_9; }
	inline void set_U24disposing_9(bool value)
	{
		___U24disposing_9 = value;
	}

	inline static int32_t get_offset_of_U24PC_10() { return static_cast<int32_t>(offsetof(U3CCoStartScenarioU3Ec__Iterator0_t1715464187, ___U24PC_10)); }
	inline int32_t get_U24PC_10() const { return ___U24PC_10; }
	inline int32_t* get_address_of_U24PC_10() { return &___U24PC_10; }
	inline void set_U24PC_10(int32_t value)
	{
		___U24PC_10 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CCOSTARTSCENARIOU3EC__ITERATOR0_T1715464187_H
#ifndef ADVSCENARIOPAGEDATA_T3333166790_H
#define ADVSCENARIOPAGEDATA_T3333166790_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.AdvScenarioPageData
struct  AdvScenarioPageData_t3333166790  : public RuntimeObject
{
public:
	// System.Collections.Generic.List`1<Utage.AdvCommand> Utage.AdvScenarioPageData::<CommandList>k__BackingField
	List_1_t2229082116 * ___U3CCommandListU3Ek__BackingField_0;
	// System.Collections.Generic.List`1<Utage.AdvCommandText> Utage.AdvScenarioPageData::<TextDataList>k__BackingField
	List_1_t185954307 * ___U3CTextDataListU3Ek__BackingField_1;
	// Utage.AdvScenarioLabelData Utage.AdvScenarioPageData::<ScenarioLabelData>k__BackingField
	AdvScenarioLabelData_t2266689885 * ___U3CScenarioLabelDataU3Ek__BackingField_2;
	// System.Collections.Generic.List`1<Utage.AdvScenarioLabelData> Utage.AdvScenarioPageData::<JumpLabelList>k__BackingField
	List_1_t1635811017 * ___U3CJumpLabelListU3Ek__BackingField_3;
	// System.Collections.Generic.List`1<Utage.AdvScenarioLabelData> Utage.AdvScenarioPageData::<AutoJumpLabelList>k__BackingField
	List_1_t1635811017 * ___U3CAutoJumpLabelListU3Ek__BackingField_4;
	// System.Int32 Utage.AdvScenarioPageData::<PageNo>k__BackingField
	int32_t ___U3CPageNoU3Ek__BackingField_5;
	// System.String Utage.AdvScenarioPageData::<MessageWindowName>k__BackingField
	String_t* ___U3CMessageWindowNameU3Ek__BackingField_6;
	// System.Int32 Utage.AdvScenarioPageData::<IndexTextTopCommand>k__BackingField
	int32_t ___U3CIndexTextTopCommandU3Ek__BackingField_7;
	// System.Boolean Utage.AdvScenarioPageData::<EnableSave>k__BackingField
	bool ___U3CEnableSaveU3Ek__BackingField_8;

public:
	inline static int32_t get_offset_of_U3CCommandListU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(AdvScenarioPageData_t3333166790, ___U3CCommandListU3Ek__BackingField_0)); }
	inline List_1_t2229082116 * get_U3CCommandListU3Ek__BackingField_0() const { return ___U3CCommandListU3Ek__BackingField_0; }
	inline List_1_t2229082116 ** get_address_of_U3CCommandListU3Ek__BackingField_0() { return &___U3CCommandListU3Ek__BackingField_0; }
	inline void set_U3CCommandListU3Ek__BackingField_0(List_1_t2229082116 * value)
	{
		___U3CCommandListU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CCommandListU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_U3CTextDataListU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(AdvScenarioPageData_t3333166790, ___U3CTextDataListU3Ek__BackingField_1)); }
	inline List_1_t185954307 * get_U3CTextDataListU3Ek__BackingField_1() const { return ___U3CTextDataListU3Ek__BackingField_1; }
	inline List_1_t185954307 ** get_address_of_U3CTextDataListU3Ek__BackingField_1() { return &___U3CTextDataListU3Ek__BackingField_1; }
	inline void set_U3CTextDataListU3Ek__BackingField_1(List_1_t185954307 * value)
	{
		___U3CTextDataListU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CTextDataListU3Ek__BackingField_1), value);
	}

	inline static int32_t get_offset_of_U3CScenarioLabelDataU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(AdvScenarioPageData_t3333166790, ___U3CScenarioLabelDataU3Ek__BackingField_2)); }
	inline AdvScenarioLabelData_t2266689885 * get_U3CScenarioLabelDataU3Ek__BackingField_2() const { return ___U3CScenarioLabelDataU3Ek__BackingField_2; }
	inline AdvScenarioLabelData_t2266689885 ** get_address_of_U3CScenarioLabelDataU3Ek__BackingField_2() { return &___U3CScenarioLabelDataU3Ek__BackingField_2; }
	inline void set_U3CScenarioLabelDataU3Ek__BackingField_2(AdvScenarioLabelData_t2266689885 * value)
	{
		___U3CScenarioLabelDataU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CScenarioLabelDataU3Ek__BackingField_2), value);
	}

	inline static int32_t get_offset_of_U3CJumpLabelListU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(AdvScenarioPageData_t3333166790, ___U3CJumpLabelListU3Ek__BackingField_3)); }
	inline List_1_t1635811017 * get_U3CJumpLabelListU3Ek__BackingField_3() const { return ___U3CJumpLabelListU3Ek__BackingField_3; }
	inline List_1_t1635811017 ** get_address_of_U3CJumpLabelListU3Ek__BackingField_3() { return &___U3CJumpLabelListU3Ek__BackingField_3; }
	inline void set_U3CJumpLabelListU3Ek__BackingField_3(List_1_t1635811017 * value)
	{
		___U3CJumpLabelListU3Ek__BackingField_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CJumpLabelListU3Ek__BackingField_3), value);
	}

	inline static int32_t get_offset_of_U3CAutoJumpLabelListU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(AdvScenarioPageData_t3333166790, ___U3CAutoJumpLabelListU3Ek__BackingField_4)); }
	inline List_1_t1635811017 * get_U3CAutoJumpLabelListU3Ek__BackingField_4() const { return ___U3CAutoJumpLabelListU3Ek__BackingField_4; }
	inline List_1_t1635811017 ** get_address_of_U3CAutoJumpLabelListU3Ek__BackingField_4() { return &___U3CAutoJumpLabelListU3Ek__BackingField_4; }
	inline void set_U3CAutoJumpLabelListU3Ek__BackingField_4(List_1_t1635811017 * value)
	{
		___U3CAutoJumpLabelListU3Ek__BackingField_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CAutoJumpLabelListU3Ek__BackingField_4), value);
	}

	inline static int32_t get_offset_of_U3CPageNoU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(AdvScenarioPageData_t3333166790, ___U3CPageNoU3Ek__BackingField_5)); }
	inline int32_t get_U3CPageNoU3Ek__BackingField_5() const { return ___U3CPageNoU3Ek__BackingField_5; }
	inline int32_t* get_address_of_U3CPageNoU3Ek__BackingField_5() { return &___U3CPageNoU3Ek__BackingField_5; }
	inline void set_U3CPageNoU3Ek__BackingField_5(int32_t value)
	{
		___U3CPageNoU3Ek__BackingField_5 = value;
	}

	inline static int32_t get_offset_of_U3CMessageWindowNameU3Ek__BackingField_6() { return static_cast<int32_t>(offsetof(AdvScenarioPageData_t3333166790, ___U3CMessageWindowNameU3Ek__BackingField_6)); }
	inline String_t* get_U3CMessageWindowNameU3Ek__BackingField_6() const { return ___U3CMessageWindowNameU3Ek__BackingField_6; }
	inline String_t** get_address_of_U3CMessageWindowNameU3Ek__BackingField_6() { return &___U3CMessageWindowNameU3Ek__BackingField_6; }
	inline void set_U3CMessageWindowNameU3Ek__BackingField_6(String_t* value)
	{
		___U3CMessageWindowNameU3Ek__BackingField_6 = value;
		Il2CppCodeGenWriteBarrier((&___U3CMessageWindowNameU3Ek__BackingField_6), value);
	}

	inline static int32_t get_offset_of_U3CIndexTextTopCommandU3Ek__BackingField_7() { return static_cast<int32_t>(offsetof(AdvScenarioPageData_t3333166790, ___U3CIndexTextTopCommandU3Ek__BackingField_7)); }
	inline int32_t get_U3CIndexTextTopCommandU3Ek__BackingField_7() const { return ___U3CIndexTextTopCommandU3Ek__BackingField_7; }
	inline int32_t* get_address_of_U3CIndexTextTopCommandU3Ek__BackingField_7() { return &___U3CIndexTextTopCommandU3Ek__BackingField_7; }
	inline void set_U3CIndexTextTopCommandU3Ek__BackingField_7(int32_t value)
	{
		___U3CIndexTextTopCommandU3Ek__BackingField_7 = value;
	}

	inline static int32_t get_offset_of_U3CEnableSaveU3Ek__BackingField_8() { return static_cast<int32_t>(offsetof(AdvScenarioPageData_t3333166790, ___U3CEnableSaveU3Ek__BackingField_8)); }
	inline bool get_U3CEnableSaveU3Ek__BackingField_8() const { return ___U3CEnableSaveU3Ek__BackingField_8; }
	inline bool* get_address_of_U3CEnableSaveU3Ek__BackingField_8() { return &___U3CEnableSaveU3Ek__BackingField_8; }
	inline void set_U3CEnableSaveU3Ek__BackingField_8(bool value)
	{
		___U3CEnableSaveU3Ek__BackingField_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ADVSCENARIOPAGEDATA_T3333166790_H
#ifndef U3CADDTOFILESETU3EC__ANONSTOREY3_T1526526459_H
#define U3CADDTOFILESETU3EC__ANONSTOREY3_T1526526459_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.AdvScenarioPageData/<AddToFileSet>c__AnonStorey3
struct  U3CAddToFileSetU3Ec__AnonStorey3_t1526526459  : public RuntimeObject
{
public:
	// System.Collections.Generic.HashSet`1<Utage.AssetFile> Utage.AdvScenarioPageData/<AddToFileSet>c__AnonStorey3::fileSet
	HashSet_1_t1716474110 * ___fileSet_0;

public:
	inline static int32_t get_offset_of_fileSet_0() { return static_cast<int32_t>(offsetof(U3CAddToFileSetU3Ec__AnonStorey3_t1526526459, ___fileSet_0)); }
	inline HashSet_1_t1716474110 * get_fileSet_0() const { return ___fileSet_0; }
	inline HashSet_1_t1716474110 ** get_address_of_fileSet_0() { return &___fileSet_0; }
	inline void set_fileSet_0(HashSet_1_t1716474110 * value)
	{
		___fileSet_0 = value;
		Il2CppCodeGenWriteBarrier((&___fileSet_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CADDTOFILESETU3EC__ANONSTOREY3_T1526526459_H
#ifndef ADVSCENARIODATA_T2546512739_H
#define ADVSCENARIODATA_T2546512739_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.AdvScenarioData
struct  AdvScenarioData_t2546512739  : public RuntimeObject
{
public:
	// System.String Utage.AdvScenarioData::name
	String_t* ___name_0;
	// Utage.AdvImportScenarioSheet Utage.AdvScenarioData::<DataGrid>k__BackingField
	AdvImportScenarioSheet_t3549824885 * ___U3CDataGridU3Ek__BackingField_1;
	// System.Boolean Utage.AdvScenarioData::isInit
	bool ___isInit_2;
	// System.Boolean Utage.AdvScenarioData::isAlreadyBackGroundLoad
	bool ___isAlreadyBackGroundLoad_3;
	// System.Collections.Generic.List`1<Utage.AdvScenarioJumpData> Utage.AdvScenarioData::jumpDataList
	List_1_t3515939073 * ___jumpDataList_4;
	// System.Collections.Generic.Dictionary`2<System.String,Utage.AdvScenarioLabelData> Utage.AdvScenarioData::scenarioLabels
	Dictionary_2_t4181469147 * ___scenarioLabels_5;

public:
	inline static int32_t get_offset_of_name_0() { return static_cast<int32_t>(offsetof(AdvScenarioData_t2546512739, ___name_0)); }
	inline String_t* get_name_0() const { return ___name_0; }
	inline String_t** get_address_of_name_0() { return &___name_0; }
	inline void set_name_0(String_t* value)
	{
		___name_0 = value;
		Il2CppCodeGenWriteBarrier((&___name_0), value);
	}

	inline static int32_t get_offset_of_U3CDataGridU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(AdvScenarioData_t2546512739, ___U3CDataGridU3Ek__BackingField_1)); }
	inline AdvImportScenarioSheet_t3549824885 * get_U3CDataGridU3Ek__BackingField_1() const { return ___U3CDataGridU3Ek__BackingField_1; }
	inline AdvImportScenarioSheet_t3549824885 ** get_address_of_U3CDataGridU3Ek__BackingField_1() { return &___U3CDataGridU3Ek__BackingField_1; }
	inline void set_U3CDataGridU3Ek__BackingField_1(AdvImportScenarioSheet_t3549824885 * value)
	{
		___U3CDataGridU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CDataGridU3Ek__BackingField_1), value);
	}

	inline static int32_t get_offset_of_isInit_2() { return static_cast<int32_t>(offsetof(AdvScenarioData_t2546512739, ___isInit_2)); }
	inline bool get_isInit_2() const { return ___isInit_2; }
	inline bool* get_address_of_isInit_2() { return &___isInit_2; }
	inline void set_isInit_2(bool value)
	{
		___isInit_2 = value;
	}

	inline static int32_t get_offset_of_isAlreadyBackGroundLoad_3() { return static_cast<int32_t>(offsetof(AdvScenarioData_t2546512739, ___isAlreadyBackGroundLoad_3)); }
	inline bool get_isAlreadyBackGroundLoad_3() const { return ___isAlreadyBackGroundLoad_3; }
	inline bool* get_address_of_isAlreadyBackGroundLoad_3() { return &___isAlreadyBackGroundLoad_3; }
	inline void set_isAlreadyBackGroundLoad_3(bool value)
	{
		___isAlreadyBackGroundLoad_3 = value;
	}

	inline static int32_t get_offset_of_jumpDataList_4() { return static_cast<int32_t>(offsetof(AdvScenarioData_t2546512739, ___jumpDataList_4)); }
	inline List_1_t3515939073 * get_jumpDataList_4() const { return ___jumpDataList_4; }
	inline List_1_t3515939073 ** get_address_of_jumpDataList_4() { return &___jumpDataList_4; }
	inline void set_jumpDataList_4(List_1_t3515939073 * value)
	{
		___jumpDataList_4 = value;
		Il2CppCodeGenWriteBarrier((&___jumpDataList_4), value);
	}

	inline static int32_t get_offset_of_scenarioLabels_5() { return static_cast<int32_t>(offsetof(AdvScenarioData_t2546512739, ___scenarioLabels_5)); }
	inline Dictionary_2_t4181469147 * get_scenarioLabels_5() const { return ___scenarioLabels_5; }
	inline Dictionary_2_t4181469147 ** get_address_of_scenarioLabels_5() { return &___scenarioLabels_5; }
	inline void set_scenarioLabels_5(Dictionary_2_t4181469147 * value)
	{
		___scenarioLabels_5 = value;
		Il2CppCodeGenWriteBarrier((&___scenarioLabels_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ADVSCENARIODATA_T2546512739_H
#ifndef ADVSCENARIOJUMPDATA_T4146817941_H
#define ADVSCENARIOJUMPDATA_T4146817941_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.AdvScenarioJumpData
struct  AdvScenarioJumpData_t4146817941  : public RuntimeObject
{
public:
	// System.String Utage.AdvScenarioJumpData::<ToLabel>k__BackingField
	String_t* ___U3CToLabelU3Ek__BackingField_0;
	// Utage.StringGridRow Utage.AdvScenarioJumpData::<FromRow>k__BackingField
	StringGridRow_t4193237197 * ___U3CFromRowU3Ek__BackingField_1;

public:
	inline static int32_t get_offset_of_U3CToLabelU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(AdvScenarioJumpData_t4146817941, ___U3CToLabelU3Ek__BackingField_0)); }
	inline String_t* get_U3CToLabelU3Ek__BackingField_0() const { return ___U3CToLabelU3Ek__BackingField_0; }
	inline String_t** get_address_of_U3CToLabelU3Ek__BackingField_0() { return &___U3CToLabelU3Ek__BackingField_0; }
	inline void set_U3CToLabelU3Ek__BackingField_0(String_t* value)
	{
		___U3CToLabelU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CToLabelU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_U3CFromRowU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(AdvScenarioJumpData_t4146817941, ___U3CFromRowU3Ek__BackingField_1)); }
	inline StringGridRow_t4193237197 * get_U3CFromRowU3Ek__BackingField_1() const { return ___U3CFromRowU3Ek__BackingField_1; }
	inline StringGridRow_t4193237197 ** get_address_of_U3CFromRowU3Ek__BackingField_1() { return &___U3CFromRowU3Ek__BackingField_1; }
	inline void set_U3CFromRowU3Ek__BackingField_1(StringGridRow_t4193237197 * value)
	{
		___U3CFromRowU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CFromRowU3Ek__BackingField_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ADVSCENARIOJUMPDATA_T4146817941_H
#ifndef U3CCOPLAYVOICEU3EC__ITERATOR0_T3696192360_H
#define U3CCOPLAYVOICEU3EC__ITERATOR0_T3696192360_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.AdvUguiBacklog/<CoPlayVoice>c__Iterator0
struct  U3CCoPlayVoiceU3Ec__Iterator0_t3696192360  : public RuntimeObject
{
public:
	// System.String Utage.AdvUguiBacklog/<CoPlayVoice>c__Iterator0::voiceFileName
	String_t* ___voiceFileName_0;
	// Utage.AssetFile Utage.AdvUguiBacklog/<CoPlayVoice>c__Iterator0::<file>__0
	RuntimeObject* ___U3CfileU3E__0_1;
	// Utage.SoundManager Utage.AdvUguiBacklog/<CoPlayVoice>c__Iterator0::<manager>__0
	SoundManager_t1265124084 * ___U3CmanagerU3E__0_2;
	// System.String Utage.AdvUguiBacklog/<CoPlayVoice>c__Iterator0::characterLabel
	String_t* ___characterLabel_3;
	// Utage.AdvUguiBacklog Utage.AdvUguiBacklog/<CoPlayVoice>c__Iterator0::$this
	AdvUguiBacklog_t1854927244 * ___U24this_4;
	// System.Object Utage.AdvUguiBacklog/<CoPlayVoice>c__Iterator0::$current
	RuntimeObject * ___U24current_5;
	// System.Boolean Utage.AdvUguiBacklog/<CoPlayVoice>c__Iterator0::$disposing
	bool ___U24disposing_6;
	// System.Int32 Utage.AdvUguiBacklog/<CoPlayVoice>c__Iterator0::$PC
	int32_t ___U24PC_7;

public:
	inline static int32_t get_offset_of_voiceFileName_0() { return static_cast<int32_t>(offsetof(U3CCoPlayVoiceU3Ec__Iterator0_t3696192360, ___voiceFileName_0)); }
	inline String_t* get_voiceFileName_0() const { return ___voiceFileName_0; }
	inline String_t** get_address_of_voiceFileName_0() { return &___voiceFileName_0; }
	inline void set_voiceFileName_0(String_t* value)
	{
		___voiceFileName_0 = value;
		Il2CppCodeGenWriteBarrier((&___voiceFileName_0), value);
	}

	inline static int32_t get_offset_of_U3CfileU3E__0_1() { return static_cast<int32_t>(offsetof(U3CCoPlayVoiceU3Ec__Iterator0_t3696192360, ___U3CfileU3E__0_1)); }
	inline RuntimeObject* get_U3CfileU3E__0_1() const { return ___U3CfileU3E__0_1; }
	inline RuntimeObject** get_address_of_U3CfileU3E__0_1() { return &___U3CfileU3E__0_1; }
	inline void set_U3CfileU3E__0_1(RuntimeObject* value)
	{
		___U3CfileU3E__0_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CfileU3E__0_1), value);
	}

	inline static int32_t get_offset_of_U3CmanagerU3E__0_2() { return static_cast<int32_t>(offsetof(U3CCoPlayVoiceU3Ec__Iterator0_t3696192360, ___U3CmanagerU3E__0_2)); }
	inline SoundManager_t1265124084 * get_U3CmanagerU3E__0_2() const { return ___U3CmanagerU3E__0_2; }
	inline SoundManager_t1265124084 ** get_address_of_U3CmanagerU3E__0_2() { return &___U3CmanagerU3E__0_2; }
	inline void set_U3CmanagerU3E__0_2(SoundManager_t1265124084 * value)
	{
		___U3CmanagerU3E__0_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CmanagerU3E__0_2), value);
	}

	inline static int32_t get_offset_of_characterLabel_3() { return static_cast<int32_t>(offsetof(U3CCoPlayVoiceU3Ec__Iterator0_t3696192360, ___characterLabel_3)); }
	inline String_t* get_characterLabel_3() const { return ___characterLabel_3; }
	inline String_t** get_address_of_characterLabel_3() { return &___characterLabel_3; }
	inline void set_characterLabel_3(String_t* value)
	{
		___characterLabel_3 = value;
		Il2CppCodeGenWriteBarrier((&___characterLabel_3), value);
	}

	inline static int32_t get_offset_of_U24this_4() { return static_cast<int32_t>(offsetof(U3CCoPlayVoiceU3Ec__Iterator0_t3696192360, ___U24this_4)); }
	inline AdvUguiBacklog_t1854927244 * get_U24this_4() const { return ___U24this_4; }
	inline AdvUguiBacklog_t1854927244 ** get_address_of_U24this_4() { return &___U24this_4; }
	inline void set_U24this_4(AdvUguiBacklog_t1854927244 * value)
	{
		___U24this_4 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_4), value);
	}

	inline static int32_t get_offset_of_U24current_5() { return static_cast<int32_t>(offsetof(U3CCoPlayVoiceU3Ec__Iterator0_t3696192360, ___U24current_5)); }
	inline RuntimeObject * get_U24current_5() const { return ___U24current_5; }
	inline RuntimeObject ** get_address_of_U24current_5() { return &___U24current_5; }
	inline void set_U24current_5(RuntimeObject * value)
	{
		___U24current_5 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_5), value);
	}

	inline static int32_t get_offset_of_U24disposing_6() { return static_cast<int32_t>(offsetof(U3CCoPlayVoiceU3Ec__Iterator0_t3696192360, ___U24disposing_6)); }
	inline bool get_U24disposing_6() const { return ___U24disposing_6; }
	inline bool* get_address_of_U24disposing_6() { return &___U24disposing_6; }
	inline void set_U24disposing_6(bool value)
	{
		___U24disposing_6 = value;
	}

	inline static int32_t get_offset_of_U24PC_7() { return static_cast<int32_t>(offsetof(U3CCoPlayVoiceU3Ec__Iterator0_t3696192360, ___U24PC_7)); }
	inline int32_t get_U24PC_7() const { return ___U24PC_7; }
	inline int32_t* get_address_of_U24PC_7() { return &___U24PC_7; }
	inline void set_U24PC_7(int32_t value)
	{
		___U24PC_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CCOPLAYVOICEU3EC__ITERATOR0_T3696192360_H
#ifndef ADVSCENARIOLABELDATA_T2266689885_H
#define ADVSCENARIOLABELDATA_T2266689885_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.AdvScenarioLabelData
struct  AdvScenarioLabelData_t2266689885  : public RuntimeObject
{
public:
	// System.Collections.Generic.List`1<Utage.AdvScenarioPageData> Utage.AdvScenarioLabelData::<PageDataList>k__BackingField
	List_1_t2702287922 * ___U3CPageDataListU3Ek__BackingField_0;
	// System.String Utage.AdvScenarioLabelData::<ScenarioLabel>k__BackingField
	String_t* ___U3CScenarioLabelU3Ek__BackingField_1;
	// Utage.AdvScenarioLabelData Utage.AdvScenarioLabelData::<Next>k__BackingField
	AdvScenarioLabelData_t2266689885 * ___U3CNextU3Ek__BackingField_2;
	// System.Collections.Generic.List`1<Utage.AdvCommand> Utage.AdvScenarioLabelData::<CommandList>k__BackingField
	List_1_t2229082116 * ___U3CCommandListU3Ek__BackingField_3;
	// Utage.AdvCommandScenarioLabel Utage.AdvScenarioLabelData::scenarioLabelCommand
	AdvCommandScenarioLabel_t2271667896 * ___scenarioLabelCommand_4;

public:
	inline static int32_t get_offset_of_U3CPageDataListU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(AdvScenarioLabelData_t2266689885, ___U3CPageDataListU3Ek__BackingField_0)); }
	inline List_1_t2702287922 * get_U3CPageDataListU3Ek__BackingField_0() const { return ___U3CPageDataListU3Ek__BackingField_0; }
	inline List_1_t2702287922 ** get_address_of_U3CPageDataListU3Ek__BackingField_0() { return &___U3CPageDataListU3Ek__BackingField_0; }
	inline void set_U3CPageDataListU3Ek__BackingField_0(List_1_t2702287922 * value)
	{
		___U3CPageDataListU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CPageDataListU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_U3CScenarioLabelU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(AdvScenarioLabelData_t2266689885, ___U3CScenarioLabelU3Ek__BackingField_1)); }
	inline String_t* get_U3CScenarioLabelU3Ek__BackingField_1() const { return ___U3CScenarioLabelU3Ek__BackingField_1; }
	inline String_t** get_address_of_U3CScenarioLabelU3Ek__BackingField_1() { return &___U3CScenarioLabelU3Ek__BackingField_1; }
	inline void set_U3CScenarioLabelU3Ek__BackingField_1(String_t* value)
	{
		___U3CScenarioLabelU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CScenarioLabelU3Ek__BackingField_1), value);
	}

	inline static int32_t get_offset_of_U3CNextU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(AdvScenarioLabelData_t2266689885, ___U3CNextU3Ek__BackingField_2)); }
	inline AdvScenarioLabelData_t2266689885 * get_U3CNextU3Ek__BackingField_2() const { return ___U3CNextU3Ek__BackingField_2; }
	inline AdvScenarioLabelData_t2266689885 ** get_address_of_U3CNextU3Ek__BackingField_2() { return &___U3CNextU3Ek__BackingField_2; }
	inline void set_U3CNextU3Ek__BackingField_2(AdvScenarioLabelData_t2266689885 * value)
	{
		___U3CNextU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CNextU3Ek__BackingField_2), value);
	}

	inline static int32_t get_offset_of_U3CCommandListU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(AdvScenarioLabelData_t2266689885, ___U3CCommandListU3Ek__BackingField_3)); }
	inline List_1_t2229082116 * get_U3CCommandListU3Ek__BackingField_3() const { return ___U3CCommandListU3Ek__BackingField_3; }
	inline List_1_t2229082116 ** get_address_of_U3CCommandListU3Ek__BackingField_3() { return &___U3CCommandListU3Ek__BackingField_3; }
	inline void set_U3CCommandListU3Ek__BackingField_3(List_1_t2229082116 * value)
	{
		___U3CCommandListU3Ek__BackingField_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CCommandListU3Ek__BackingField_3), value);
	}

	inline static int32_t get_offset_of_scenarioLabelCommand_4() { return static_cast<int32_t>(offsetof(AdvScenarioLabelData_t2266689885, ___scenarioLabelCommand_4)); }
	inline AdvCommandScenarioLabel_t2271667896 * get_scenarioLabelCommand_4() const { return ___scenarioLabelCommand_4; }
	inline AdvCommandScenarioLabel_t2271667896 ** get_address_of_scenarioLabelCommand_4() { return &___scenarioLabelCommand_4; }
	inline void set_scenarioLabelCommand_4(AdvCommandScenarioLabel_t2271667896 * value)
	{
		___scenarioLabelCommand_4 = value;
		Il2CppCodeGenWriteBarrier((&___scenarioLabelCommand_4), value);
	}
};

struct AdvScenarioLabelData_t2266689885_StaticFields
{
public:
	// System.Action`1<Utage.AdvScenarioPageData> Utage.AdvScenarioLabelData::<>f__am$cache0
	Action_1_t3134966172 * ___U3CU3Ef__amU24cache0_5;

public:
	inline static int32_t get_offset_of_U3CU3Ef__amU24cache0_5() { return static_cast<int32_t>(offsetof(AdvScenarioLabelData_t2266689885_StaticFields, ___U3CU3Ef__amU24cache0_5)); }
	inline Action_1_t3134966172 * get_U3CU3Ef__amU24cache0_5() const { return ___U3CU3Ef__amU24cache0_5; }
	inline Action_1_t3134966172 ** get_address_of_U3CU3Ef__amU24cache0_5() { return &___U3CU3Ef__amU24cache0_5; }
	inline void set_U3CU3Ef__amU24cache0_5(Action_1_t3134966172 * value)
	{
		___U3CU3Ef__amU24cache0_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache0_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ADVSCENARIOLABELDATA_T2266689885_H
#ifndef U3CDOWNLOADU3EC__ANONSTOREY0_T555719203_H
#define U3CDOWNLOADU3EC__ANONSTOREY0_T555719203_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.AdvScenarioLabelData/<Download>c__AnonStorey0
struct  U3CDownloadU3Ec__AnonStorey0_t555719203  : public RuntimeObject
{
public:
	// Utage.AdvDataManager Utage.AdvScenarioLabelData/<Download>c__AnonStorey0::dataManager
	AdvDataManager_t2481232830 * ___dataManager_0;

public:
	inline static int32_t get_offset_of_dataManager_0() { return static_cast<int32_t>(offsetof(U3CDownloadU3Ec__AnonStorey0_t555719203, ___dataManager_0)); }
	inline AdvDataManager_t2481232830 * get_dataManager_0() const { return ___dataManager_0; }
	inline AdvDataManager_t2481232830 ** get_address_of_dataManager_0() { return &___dataManager_0; }
	inline void set_dataManager_0(AdvDataManager_t2481232830 * value)
	{
		___dataManager_0 = value;
		Il2CppCodeGenWriteBarrier((&___dataManager_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CDOWNLOADU3EC__ANONSTOREY0_T555719203_H
#ifndef U3CPRELOADDEEPU3EC__ANONSTOREY2_T2020758728_H
#define U3CPRELOADDEEPU3EC__ANONSTOREY2_T2020758728_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.AdvScenarioLabelData/<PreloadDeep>c__AnonStorey2
struct  U3CPreloadDeepU3Ec__AnonStorey2_t2020758728  : public RuntimeObject
{
public:
	// Utage.AdvDataManager Utage.AdvScenarioLabelData/<PreloadDeep>c__AnonStorey2::dataManager
	AdvDataManager_t2481232830 * ___dataManager_0;
	// System.Collections.Generic.HashSet`1<Utage.AssetFile> Utage.AdvScenarioLabelData/<PreloadDeep>c__AnonStorey2::fileSet
	HashSet_1_t1716474110 * ___fileSet_1;
	// System.Int32 Utage.AdvScenarioLabelData/<PreloadDeep>c__AnonStorey2::maxFilePreload
	int32_t ___maxFilePreload_2;
	// System.Int32 Utage.AdvScenarioLabelData/<PreloadDeep>c__AnonStorey2::deepLevel
	int32_t ___deepLevel_3;

public:
	inline static int32_t get_offset_of_dataManager_0() { return static_cast<int32_t>(offsetof(U3CPreloadDeepU3Ec__AnonStorey2_t2020758728, ___dataManager_0)); }
	inline AdvDataManager_t2481232830 * get_dataManager_0() const { return ___dataManager_0; }
	inline AdvDataManager_t2481232830 ** get_address_of_dataManager_0() { return &___dataManager_0; }
	inline void set_dataManager_0(AdvDataManager_t2481232830 * value)
	{
		___dataManager_0 = value;
		Il2CppCodeGenWriteBarrier((&___dataManager_0), value);
	}

	inline static int32_t get_offset_of_fileSet_1() { return static_cast<int32_t>(offsetof(U3CPreloadDeepU3Ec__AnonStorey2_t2020758728, ___fileSet_1)); }
	inline HashSet_1_t1716474110 * get_fileSet_1() const { return ___fileSet_1; }
	inline HashSet_1_t1716474110 ** get_address_of_fileSet_1() { return &___fileSet_1; }
	inline void set_fileSet_1(HashSet_1_t1716474110 * value)
	{
		___fileSet_1 = value;
		Il2CppCodeGenWriteBarrier((&___fileSet_1), value);
	}

	inline static int32_t get_offset_of_maxFilePreload_2() { return static_cast<int32_t>(offsetof(U3CPreloadDeepU3Ec__AnonStorey2_t2020758728, ___maxFilePreload_2)); }
	inline int32_t get_maxFilePreload_2() const { return ___maxFilePreload_2; }
	inline int32_t* get_address_of_maxFilePreload_2() { return &___maxFilePreload_2; }
	inline void set_maxFilePreload_2(int32_t value)
	{
		___maxFilePreload_2 = value;
	}

	inline static int32_t get_offset_of_deepLevel_3() { return static_cast<int32_t>(offsetof(U3CPreloadDeepU3Ec__AnonStorey2_t2020758728, ___deepLevel_3)); }
	inline int32_t get_deepLevel_3() const { return ___deepLevel_3; }
	inline int32_t* get_address_of_deepLevel_3() { return &___deepLevel_3; }
	inline void set_deepLevel_3(int32_t value)
	{
		___deepLevel_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CPRELOADDEEPU3EC__ANONSTOREY2_T2020758728_H
#ifndef U3CPRELOADDEEPU3EC__ANONSTOREY1_T454674787_H
#define U3CPRELOADDEEPU3EC__ANONSTOREY1_T454674787_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.AdvScenarioLabelData/<PreloadDeep>c__AnonStorey1
struct  U3CPreloadDeepU3Ec__AnonStorey1_t454674787  : public RuntimeObject
{
public:
	// Utage.AdvDataManager Utage.AdvScenarioLabelData/<PreloadDeep>c__AnonStorey1::dataManager
	AdvDataManager_t2481232830 * ___dataManager_0;
	// System.Collections.Generic.HashSet`1<Utage.AssetFile> Utage.AdvScenarioLabelData/<PreloadDeep>c__AnonStorey1::fileSet
	HashSet_1_t1716474110 * ___fileSet_1;
	// System.Int32 Utage.AdvScenarioLabelData/<PreloadDeep>c__AnonStorey1::maxFilePreload
	int32_t ___maxFilePreload_2;
	// System.Int32 Utage.AdvScenarioLabelData/<PreloadDeep>c__AnonStorey1::deepLevel
	int32_t ___deepLevel_3;

public:
	inline static int32_t get_offset_of_dataManager_0() { return static_cast<int32_t>(offsetof(U3CPreloadDeepU3Ec__AnonStorey1_t454674787, ___dataManager_0)); }
	inline AdvDataManager_t2481232830 * get_dataManager_0() const { return ___dataManager_0; }
	inline AdvDataManager_t2481232830 ** get_address_of_dataManager_0() { return &___dataManager_0; }
	inline void set_dataManager_0(AdvDataManager_t2481232830 * value)
	{
		___dataManager_0 = value;
		Il2CppCodeGenWriteBarrier((&___dataManager_0), value);
	}

	inline static int32_t get_offset_of_fileSet_1() { return static_cast<int32_t>(offsetof(U3CPreloadDeepU3Ec__AnonStorey1_t454674787, ___fileSet_1)); }
	inline HashSet_1_t1716474110 * get_fileSet_1() const { return ___fileSet_1; }
	inline HashSet_1_t1716474110 ** get_address_of_fileSet_1() { return &___fileSet_1; }
	inline void set_fileSet_1(HashSet_1_t1716474110 * value)
	{
		___fileSet_1 = value;
		Il2CppCodeGenWriteBarrier((&___fileSet_1), value);
	}

	inline static int32_t get_offset_of_maxFilePreload_2() { return static_cast<int32_t>(offsetof(U3CPreloadDeepU3Ec__AnonStorey1_t454674787, ___maxFilePreload_2)); }
	inline int32_t get_maxFilePreload_2() const { return ___maxFilePreload_2; }
	inline int32_t* get_address_of_maxFilePreload_2() { return &___maxFilePreload_2; }
	inline void set_maxFilePreload_2(int32_t value)
	{
		___maxFilePreload_2 = value;
	}

	inline static int32_t get_offset_of_deepLevel_3() { return static_cast<int32_t>(offsetof(U3CPreloadDeepU3Ec__AnonStorey1_t454674787, ___deepLevel_3)); }
	inline int32_t get_deepLevel_3() const { return ___deepLevel_3; }
	inline int32_t* get_address_of_deepLevel_3() { return &___deepLevel_3; }
	inline void set_deepLevel_3(int32_t value)
	{
		___deepLevel_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CPRELOADDEEPU3EC__ANONSTOREY1_T454674787_H
#ifndef VECTOR3_T2243707580_H
#define VECTOR3_T2243707580_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector3
struct  Vector3_t2243707580 
{
public:
	// System.Single UnityEngine.Vector3::x
	float ___x_1;
	// System.Single UnityEngine.Vector3::y
	float ___y_2;
	// System.Single UnityEngine.Vector3::z
	float ___z_3;

public:
	inline static int32_t get_offset_of_x_1() { return static_cast<int32_t>(offsetof(Vector3_t2243707580, ___x_1)); }
	inline float get_x_1() const { return ___x_1; }
	inline float* get_address_of_x_1() { return &___x_1; }
	inline void set_x_1(float value)
	{
		___x_1 = value;
	}

	inline static int32_t get_offset_of_y_2() { return static_cast<int32_t>(offsetof(Vector3_t2243707580, ___y_2)); }
	inline float get_y_2() const { return ___y_2; }
	inline float* get_address_of_y_2() { return &___y_2; }
	inline void set_y_2(float value)
	{
		___y_2 = value;
	}

	inline static int32_t get_offset_of_z_3() { return static_cast<int32_t>(offsetof(Vector3_t2243707580, ___z_3)); }
	inline float get_z_3() const { return ___z_3; }
	inline float* get_address_of_z_3() { return &___z_3; }
	inline void set_z_3(float value)
	{
		___z_3 = value;
	}
};

struct Vector3_t2243707580_StaticFields
{
public:
	// UnityEngine.Vector3 UnityEngine.Vector3::zeroVector
	Vector3_t2243707580  ___zeroVector_4;
	// UnityEngine.Vector3 UnityEngine.Vector3::oneVector
	Vector3_t2243707580  ___oneVector_5;
	// UnityEngine.Vector3 UnityEngine.Vector3::upVector
	Vector3_t2243707580  ___upVector_6;
	// UnityEngine.Vector3 UnityEngine.Vector3::downVector
	Vector3_t2243707580  ___downVector_7;
	// UnityEngine.Vector3 UnityEngine.Vector3::leftVector
	Vector3_t2243707580  ___leftVector_8;
	// UnityEngine.Vector3 UnityEngine.Vector3::rightVector
	Vector3_t2243707580  ___rightVector_9;
	// UnityEngine.Vector3 UnityEngine.Vector3::forwardVector
	Vector3_t2243707580  ___forwardVector_10;
	// UnityEngine.Vector3 UnityEngine.Vector3::backVector
	Vector3_t2243707580  ___backVector_11;
	// UnityEngine.Vector3 UnityEngine.Vector3::positiveInfinityVector
	Vector3_t2243707580  ___positiveInfinityVector_12;
	// UnityEngine.Vector3 UnityEngine.Vector3::negativeInfinityVector
	Vector3_t2243707580  ___negativeInfinityVector_13;

public:
	inline static int32_t get_offset_of_zeroVector_4() { return static_cast<int32_t>(offsetof(Vector3_t2243707580_StaticFields, ___zeroVector_4)); }
	inline Vector3_t2243707580  get_zeroVector_4() const { return ___zeroVector_4; }
	inline Vector3_t2243707580 * get_address_of_zeroVector_4() { return &___zeroVector_4; }
	inline void set_zeroVector_4(Vector3_t2243707580  value)
	{
		___zeroVector_4 = value;
	}

	inline static int32_t get_offset_of_oneVector_5() { return static_cast<int32_t>(offsetof(Vector3_t2243707580_StaticFields, ___oneVector_5)); }
	inline Vector3_t2243707580  get_oneVector_5() const { return ___oneVector_5; }
	inline Vector3_t2243707580 * get_address_of_oneVector_5() { return &___oneVector_5; }
	inline void set_oneVector_5(Vector3_t2243707580  value)
	{
		___oneVector_5 = value;
	}

	inline static int32_t get_offset_of_upVector_6() { return static_cast<int32_t>(offsetof(Vector3_t2243707580_StaticFields, ___upVector_6)); }
	inline Vector3_t2243707580  get_upVector_6() const { return ___upVector_6; }
	inline Vector3_t2243707580 * get_address_of_upVector_6() { return &___upVector_6; }
	inline void set_upVector_6(Vector3_t2243707580  value)
	{
		___upVector_6 = value;
	}

	inline static int32_t get_offset_of_downVector_7() { return static_cast<int32_t>(offsetof(Vector3_t2243707580_StaticFields, ___downVector_7)); }
	inline Vector3_t2243707580  get_downVector_7() const { return ___downVector_7; }
	inline Vector3_t2243707580 * get_address_of_downVector_7() { return &___downVector_7; }
	inline void set_downVector_7(Vector3_t2243707580  value)
	{
		___downVector_7 = value;
	}

	inline static int32_t get_offset_of_leftVector_8() { return static_cast<int32_t>(offsetof(Vector3_t2243707580_StaticFields, ___leftVector_8)); }
	inline Vector3_t2243707580  get_leftVector_8() const { return ___leftVector_8; }
	inline Vector3_t2243707580 * get_address_of_leftVector_8() { return &___leftVector_8; }
	inline void set_leftVector_8(Vector3_t2243707580  value)
	{
		___leftVector_8 = value;
	}

	inline static int32_t get_offset_of_rightVector_9() { return static_cast<int32_t>(offsetof(Vector3_t2243707580_StaticFields, ___rightVector_9)); }
	inline Vector3_t2243707580  get_rightVector_9() const { return ___rightVector_9; }
	inline Vector3_t2243707580 * get_address_of_rightVector_9() { return &___rightVector_9; }
	inline void set_rightVector_9(Vector3_t2243707580  value)
	{
		___rightVector_9 = value;
	}

	inline static int32_t get_offset_of_forwardVector_10() { return static_cast<int32_t>(offsetof(Vector3_t2243707580_StaticFields, ___forwardVector_10)); }
	inline Vector3_t2243707580  get_forwardVector_10() const { return ___forwardVector_10; }
	inline Vector3_t2243707580 * get_address_of_forwardVector_10() { return &___forwardVector_10; }
	inline void set_forwardVector_10(Vector3_t2243707580  value)
	{
		___forwardVector_10 = value;
	}

	inline static int32_t get_offset_of_backVector_11() { return static_cast<int32_t>(offsetof(Vector3_t2243707580_StaticFields, ___backVector_11)); }
	inline Vector3_t2243707580  get_backVector_11() const { return ___backVector_11; }
	inline Vector3_t2243707580 * get_address_of_backVector_11() { return &___backVector_11; }
	inline void set_backVector_11(Vector3_t2243707580  value)
	{
		___backVector_11 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_12() { return static_cast<int32_t>(offsetof(Vector3_t2243707580_StaticFields, ___positiveInfinityVector_12)); }
	inline Vector3_t2243707580  get_positiveInfinityVector_12() const { return ___positiveInfinityVector_12; }
	inline Vector3_t2243707580 * get_address_of_positiveInfinityVector_12() { return &___positiveInfinityVector_12; }
	inline void set_positiveInfinityVector_12(Vector3_t2243707580  value)
	{
		___positiveInfinityVector_12 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_13() { return static_cast<int32_t>(offsetof(Vector3_t2243707580_StaticFields, ___negativeInfinityVector_13)); }
	inline Vector3_t2243707580  get_negativeInfinityVector_13() const { return ___negativeInfinityVector_13; }
	inline Vector3_t2243707580 * get_address_of_negativeInfinityVector_13() { return &___negativeInfinityVector_13; }
	inline void set_negativeInfinityVector_13(Vector3_t2243707580  value)
	{
		___negativeInfinityVector_13 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR3_T2243707580_H
#ifndef COLOR_T2020392075_H
#define COLOR_T2020392075_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Color
struct  Color_t2020392075 
{
public:
	// System.Single UnityEngine.Color::r
	float ___r_0;
	// System.Single UnityEngine.Color::g
	float ___g_1;
	// System.Single UnityEngine.Color::b
	float ___b_2;
	// System.Single UnityEngine.Color::a
	float ___a_3;

public:
	inline static int32_t get_offset_of_r_0() { return static_cast<int32_t>(offsetof(Color_t2020392075, ___r_0)); }
	inline float get_r_0() const { return ___r_0; }
	inline float* get_address_of_r_0() { return &___r_0; }
	inline void set_r_0(float value)
	{
		___r_0 = value;
	}

	inline static int32_t get_offset_of_g_1() { return static_cast<int32_t>(offsetof(Color_t2020392075, ___g_1)); }
	inline float get_g_1() const { return ___g_1; }
	inline float* get_address_of_g_1() { return &___g_1; }
	inline void set_g_1(float value)
	{
		___g_1 = value;
	}

	inline static int32_t get_offset_of_b_2() { return static_cast<int32_t>(offsetof(Color_t2020392075, ___b_2)); }
	inline float get_b_2() const { return ___b_2; }
	inline float* get_address_of_b_2() { return &___b_2; }
	inline void set_b_2(float value)
	{
		___b_2 = value;
	}

	inline static int32_t get_offset_of_a_3() { return static_cast<int32_t>(offsetof(Color_t2020392075, ___a_3)); }
	inline float get_a_3() const { return ___a_3; }
	inline float* get_address_of_a_3() { return &___a_3; }
	inline void set_a_3(float value)
	{
		___a_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLOR_T2020392075_H
#ifndef ENUM_T2459695545_H
#define ENUM_T2459695545_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Enum
struct  Enum_t2459695545  : public ValueType_t3507792607
{
public:

public:
};

struct Enum_t2459695545_StaticFields
{
public:
	// System.Char[] System.Enum::split_char
	CharU5BU5D_t1328083999* ___split_char_0;

public:
	inline static int32_t get_offset_of_split_char_0() { return static_cast<int32_t>(offsetof(Enum_t2459695545_StaticFields, ___split_char_0)); }
	inline CharU5BU5D_t1328083999* get_split_char_0() const { return ___split_char_0; }
	inline CharU5BU5D_t1328083999** get_address_of_split_char_0() { return &___split_char_0; }
	inline void set_split_char_0(CharU5BU5D_t1328083999* value)
	{
		___split_char_0 = value;
		Il2CppCodeGenWriteBarrier((&___split_char_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t2459695545_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t2459695545_marshaled_com
{
};
#endif // ENUM_T2459695545_H
#ifndef UNITYEVENT_1_T2898310999_H
#define UNITYEVENT_1_T2898310999_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Events.UnityEvent`1<Utage.AdvCommand>
struct  UnityEvent_1_t2898310999  : public UnityEventBase_t828812576
{
public:
	// System.Object[] UnityEngine.Events.UnityEvent`1::m_InvokeArray
	ObjectU5BU5D_t3614634134* ___m_InvokeArray_4;

public:
	inline static int32_t get_offset_of_m_InvokeArray_4() { return static_cast<int32_t>(offsetof(UnityEvent_1_t2898310999, ___m_InvokeArray_4)); }
	inline ObjectU5BU5D_t3614634134* get_m_InvokeArray_4() const { return ___m_InvokeArray_4; }
	inline ObjectU5BU5D_t3614634134** get_address_of_m_InvokeArray_4() { return &___m_InvokeArray_4; }
	inline void set_m_InvokeArray_4(ObjectU5BU5D_t3614634134* value)
	{
		___m_InvokeArray_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_InvokeArray_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYEVENT_1_T2898310999_H
#ifndef UNITYEVENT_1_T1334181495_H
#define UNITYEVENT_1_T1334181495_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Events.UnityEvent`1<Utage.AdvScenarioPlayer>
struct  UnityEvent_1_t1334181495  : public UnityEventBase_t828812576
{
public:
	// System.Object[] UnityEngine.Events.UnityEvent`1::m_InvokeArray
	ObjectU5BU5D_t3614634134* ___m_InvokeArray_4;

public:
	inline static int32_t get_offset_of_m_InvokeArray_4() { return static_cast<int32_t>(offsetof(UnityEvent_1_t1334181495, ___m_InvokeArray_4)); }
	inline ObjectU5BU5D_t3614634134* get_m_InvokeArray_4() const { return ___m_InvokeArray_4; }
	inline ObjectU5BU5D_t3614634134** get_address_of_m_InvokeArray_4() { return &___m_InvokeArray_4; }
	inline void set_m_InvokeArray_4(ObjectU5BU5D_t3614634134* value)
	{
		___m_InvokeArray_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_InvokeArray_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYEVENT_1_T1334181495_H
#ifndef SERIALIZABLEDICTIONARYBINARYIOKEYVALUE_T105246368_H
#define SERIALIZABLEDICTIONARYBINARYIOKEYVALUE_T105246368_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.SerializableDictionaryBinaryIOKeyValue
struct  SerializableDictionaryBinaryIOKeyValue_t105246368  : public SerializableDictionaryKeyValue_t1605592419
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SERIALIZABLEDICTIONARYBINARYIOKEYVALUE_T105246368_H
#ifndef MINMAXINT_T3259591635_H
#define MINMAXINT_T3259591635_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.MinMaxInt
struct  MinMaxInt_t3259591635  : public MinMax_1_t921947674
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MINMAXINT_T3259591635_H
#ifndef MINMAXFLOAT_T1425080750_H
#define MINMAXFLOAT_T1425080750_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.MinMaxFloat
struct  MinMaxFloat_t1425080750  : public MinMax_1_t926580158
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MINMAXFLOAT_T1425080750_H
#ifndef UNITYEVENT_1_T355565814_H
#define UNITYEVENT_1_T355565814_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Events.UnityEvent`1<Utage.CaptureCamera>
struct  UnityEvent_1_t355565814  : public UnityEventBase_t828812576
{
public:
	// System.Object[] UnityEngine.Events.UnityEvent`1::m_InvokeArray
	ObjectU5BU5D_t3614634134* ___m_InvokeArray_4;

public:
	inline static int32_t get_offset_of_m_InvokeArray_4() { return static_cast<int32_t>(offsetof(UnityEvent_1_t355565814, ___m_InvokeArray_4)); }
	inline ObjectU5BU5D_t3614634134* get_m_InvokeArray_4() const { return ___m_InvokeArray_4; }
	inline ObjectU5BU5D_t3614634134** get_address_of_m_InvokeArray_4() { return &___m_InvokeArray_4; }
	inline void set_m_InvokeArray_4(ObjectU5BU5D_t3614634134* value)
	{
		___m_InvokeArray_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_InvokeArray_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYEVENT_1_T355565814_H
#ifndef PROPERTYATTRIBUTE_T2606999759_H
#define PROPERTYATTRIBUTE_T2606999759_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.PropertyAttribute
struct  PropertyAttribute_t2606999759  : public Attribute_t542643598
{
public:
	// System.Int32 UnityEngine.PropertyAttribute::<order>k__BackingField
	int32_t ___U3CorderU3Ek__BackingField_0;

public:
	inline static int32_t get_offset_of_U3CorderU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(PropertyAttribute_t2606999759, ___U3CorderU3Ek__BackingField_0)); }
	inline int32_t get_U3CorderU3Ek__BackingField_0() const { return ___U3CorderU3Ek__BackingField_0; }
	inline int32_t* get_address_of_U3CorderU3Ek__BackingField_0() { return &___U3CorderU3Ek__BackingField_0; }
	inline void set_U3CorderU3Ek__BackingField_0(int32_t value)
	{
		___U3CorderU3Ek__BackingField_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PROPERTYATTRIBUTE_T2606999759_H
#ifndef SERIALIZABLEDICTIONARYBINARYIO_1_T2453522167_H
#define SERIALIZABLEDICTIONARYBINARYIO_1_T2453522167_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.SerializableDictionaryBinaryIO`1<Utage.DictionaryKeyValueString>
struct  SerializableDictionaryBinaryIO_1_t2453522167  : public SerializableDictionary_1_t3054077954
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SERIALIZABLEDICTIONARYBINARYIO_1_T2453522167_H
#ifndef INTPTR_T_H
#define INTPTR_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTPTR_T_H
#ifndef SERIALIZABLEDICTIONARYBINARYIO_1_T2502238955_H
#define SERIALIZABLEDICTIONARYBINARYIO_1_T2502238955_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.SerializableDictionaryBinaryIO`1<Utage.DictionaryKeyValueDouble>
struct  SerializableDictionaryBinaryIO_1_t2502238955  : public SerializableDictionary_1_t3102794742
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SERIALIZABLEDICTIONARYBINARYIO_1_T2502238955_H
#ifndef SERIALIZABLEDICTIONARYBINARYIO_1_T919821380_H
#define SERIALIZABLEDICTIONARYBINARYIO_1_T919821380_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.SerializableDictionaryBinaryIO`1<Utage.DictionaryKeyValueFloat>
struct  SerializableDictionaryBinaryIO_1_t919821380  : public SerializableDictionary_1_t1520377167
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SERIALIZABLEDICTIONARYBINARYIO_1_T919821380_H
#ifndef UNITYEVENT_1_T3545967699_H
#define UNITYEVENT_1_T3545967699_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Events.UnityEvent`1<Utage.LetterBoxCamera>
struct  UnityEvent_1_t3545967699  : public UnityEventBase_t828812576
{
public:
	// System.Object[] UnityEngine.Events.UnityEvent`1::m_InvokeArray
	ObjectU5BU5D_t3614634134* ___m_InvokeArray_4;

public:
	inline static int32_t get_offset_of_m_InvokeArray_4() { return static_cast<int32_t>(offsetof(UnityEvent_1_t3545967699, ___m_InvokeArray_4)); }
	inline ObjectU5BU5D_t3614634134* get_m_InvokeArray_4() const { return ___m_InvokeArray_4; }
	inline ObjectU5BU5D_t3614634134** get_address_of_m_InvokeArray_4() { return &___m_InvokeArray_4; }
	inline void set_m_InvokeArray_4(ObjectU5BU5D_t3614634134* value)
	{
		___m_InvokeArray_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_InvokeArray_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYEVENT_1_T3545967699_H
#ifndef VECTOR2_T2243707579_H
#define VECTOR2_T2243707579_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector2
struct  Vector2_t2243707579 
{
public:
	// System.Single UnityEngine.Vector2::x
	float ___x_0;
	// System.Single UnityEngine.Vector2::y
	float ___y_1;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Vector2_t2243707579, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Vector2_t2243707579, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}
};

struct Vector2_t2243707579_StaticFields
{
public:
	// UnityEngine.Vector2 UnityEngine.Vector2::zeroVector
	Vector2_t2243707579  ___zeroVector_2;
	// UnityEngine.Vector2 UnityEngine.Vector2::oneVector
	Vector2_t2243707579  ___oneVector_3;
	// UnityEngine.Vector2 UnityEngine.Vector2::upVector
	Vector2_t2243707579  ___upVector_4;
	// UnityEngine.Vector2 UnityEngine.Vector2::downVector
	Vector2_t2243707579  ___downVector_5;
	// UnityEngine.Vector2 UnityEngine.Vector2::leftVector
	Vector2_t2243707579  ___leftVector_6;
	// UnityEngine.Vector2 UnityEngine.Vector2::rightVector
	Vector2_t2243707579  ___rightVector_7;
	// UnityEngine.Vector2 UnityEngine.Vector2::positiveInfinityVector
	Vector2_t2243707579  ___positiveInfinityVector_8;
	// UnityEngine.Vector2 UnityEngine.Vector2::negativeInfinityVector
	Vector2_t2243707579  ___negativeInfinityVector_9;

public:
	inline static int32_t get_offset_of_zeroVector_2() { return static_cast<int32_t>(offsetof(Vector2_t2243707579_StaticFields, ___zeroVector_2)); }
	inline Vector2_t2243707579  get_zeroVector_2() const { return ___zeroVector_2; }
	inline Vector2_t2243707579 * get_address_of_zeroVector_2() { return &___zeroVector_2; }
	inline void set_zeroVector_2(Vector2_t2243707579  value)
	{
		___zeroVector_2 = value;
	}

	inline static int32_t get_offset_of_oneVector_3() { return static_cast<int32_t>(offsetof(Vector2_t2243707579_StaticFields, ___oneVector_3)); }
	inline Vector2_t2243707579  get_oneVector_3() const { return ___oneVector_3; }
	inline Vector2_t2243707579 * get_address_of_oneVector_3() { return &___oneVector_3; }
	inline void set_oneVector_3(Vector2_t2243707579  value)
	{
		___oneVector_3 = value;
	}

	inline static int32_t get_offset_of_upVector_4() { return static_cast<int32_t>(offsetof(Vector2_t2243707579_StaticFields, ___upVector_4)); }
	inline Vector2_t2243707579  get_upVector_4() const { return ___upVector_4; }
	inline Vector2_t2243707579 * get_address_of_upVector_4() { return &___upVector_4; }
	inline void set_upVector_4(Vector2_t2243707579  value)
	{
		___upVector_4 = value;
	}

	inline static int32_t get_offset_of_downVector_5() { return static_cast<int32_t>(offsetof(Vector2_t2243707579_StaticFields, ___downVector_5)); }
	inline Vector2_t2243707579  get_downVector_5() const { return ___downVector_5; }
	inline Vector2_t2243707579 * get_address_of_downVector_5() { return &___downVector_5; }
	inline void set_downVector_5(Vector2_t2243707579  value)
	{
		___downVector_5 = value;
	}

	inline static int32_t get_offset_of_leftVector_6() { return static_cast<int32_t>(offsetof(Vector2_t2243707579_StaticFields, ___leftVector_6)); }
	inline Vector2_t2243707579  get_leftVector_6() const { return ___leftVector_6; }
	inline Vector2_t2243707579 * get_address_of_leftVector_6() { return &___leftVector_6; }
	inline void set_leftVector_6(Vector2_t2243707579  value)
	{
		___leftVector_6 = value;
	}

	inline static int32_t get_offset_of_rightVector_7() { return static_cast<int32_t>(offsetof(Vector2_t2243707579_StaticFields, ___rightVector_7)); }
	inline Vector2_t2243707579  get_rightVector_7() const { return ___rightVector_7; }
	inline Vector2_t2243707579 * get_address_of_rightVector_7() { return &___rightVector_7; }
	inline void set_rightVector_7(Vector2_t2243707579  value)
	{
		___rightVector_7 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_8() { return static_cast<int32_t>(offsetof(Vector2_t2243707579_StaticFields, ___positiveInfinityVector_8)); }
	inline Vector2_t2243707579  get_positiveInfinityVector_8() const { return ___positiveInfinityVector_8; }
	inline Vector2_t2243707579 * get_address_of_positiveInfinityVector_8() { return &___positiveInfinityVector_8; }
	inline void set_positiveInfinityVector_8(Vector2_t2243707579  value)
	{
		___positiveInfinityVector_8 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_9() { return static_cast<int32_t>(offsetof(Vector2_t2243707579_StaticFields, ___negativeInfinityVector_9)); }
	inline Vector2_t2243707579  get_negativeInfinityVector_9() const { return ___negativeInfinityVector_9; }
	inline Vector2_t2243707579 * get_address_of_negativeInfinityVector_9() { return &___negativeInfinityVector_9; }
	inline void set_negativeInfinityVector_9(Vector2_t2243707579  value)
	{
		___negativeInfinityVector_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR2_T2243707579_H
#ifndef SERIALIZABLEDICTIONARYBINARYIO_1_T16155954_H
#define SERIALIZABLEDICTIONARYBINARYIO_1_T16155954_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.SerializableDictionaryBinaryIO`1<Utage.DictionaryKeyValueBool>
struct  SerializableDictionaryBinaryIO_1_t16155954  : public SerializableDictionary_1_t616711741
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SERIALIZABLEDICTIONARYBINARYIO_1_T16155954_H
#ifndef SERIALIZABLEDICTIONARYBINARYIO_1_T663763689_H
#define SERIALIZABLEDICTIONARYBINARYIO_1_T663763689_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.SerializableDictionaryBinaryIO`1<Utage.DictionaryKeyValueInt>
struct  SerializableDictionaryBinaryIO_1_t663763689  : public SerializableDictionary_1_t1264319476
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SERIALIZABLEDICTIONARYBINARYIO_1_T663763689_H
#ifndef BUTTONATTRIBUTE_T559151802_H
#define BUTTONATTRIBUTE_T559151802_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.ButtonAttribute
struct  ButtonAttribute_t559151802  : public PropertyAttribute_t2606999759
{
public:
	// System.String Utage.ButtonAttribute::<Function>k__BackingField
	String_t* ___U3CFunctionU3Ek__BackingField_1;
	// System.String Utage.ButtonAttribute::<Text>k__BackingField
	String_t* ___U3CTextU3Ek__BackingField_2;

public:
	inline static int32_t get_offset_of_U3CFunctionU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(ButtonAttribute_t559151802, ___U3CFunctionU3Ek__BackingField_1)); }
	inline String_t* get_U3CFunctionU3Ek__BackingField_1() const { return ___U3CFunctionU3Ek__BackingField_1; }
	inline String_t** get_address_of_U3CFunctionU3Ek__BackingField_1() { return &___U3CFunctionU3Ek__BackingField_1; }
	inline void set_U3CFunctionU3Ek__BackingField_1(String_t* value)
	{
		___U3CFunctionU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CFunctionU3Ek__BackingField_1), value);
	}

	inline static int32_t get_offset_of_U3CTextU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(ButtonAttribute_t559151802, ___U3CTextU3Ek__BackingField_2)); }
	inline String_t* get_U3CTextU3Ek__BackingField_2() const { return ___U3CTextU3Ek__BackingField_2; }
	inline String_t** get_address_of_U3CTextU3Ek__BackingField_2() { return &___U3CTextU3Ek__BackingField_2; }
	inline void set_U3CTextU3Ek__BackingField_2(String_t* value)
	{
		___U3CTextU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CTextU3Ek__BackingField_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BUTTONATTRIBUTE_T559151802_H
#ifndef DICTIONARYDOUBLE_T2509882451_H
#define DICTIONARYDOUBLE_T2509882451_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.DictionaryDouble
struct  DictionaryDouble_t2509882451  : public SerializableDictionaryBinaryIO_1_t2502238955
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DICTIONARYDOUBLE_T2509882451_H
#ifndef ADDBUTTONATTRIBUTE_T3818261841_H
#define ADDBUTTONATTRIBUTE_T3818261841_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.AddButtonAttribute
struct  AddButtonAttribute_t3818261841  : public PropertyAttribute_t2606999759
{
public:
	// System.String Utage.AddButtonAttribute::<Function>k__BackingField
	String_t* ___U3CFunctionU3Ek__BackingField_1;
	// System.String Utage.AddButtonAttribute::<Text>k__BackingField
	String_t* ___U3CTextU3Ek__BackingField_2;

public:
	inline static int32_t get_offset_of_U3CFunctionU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(AddButtonAttribute_t3818261841, ___U3CFunctionU3Ek__BackingField_1)); }
	inline String_t* get_U3CFunctionU3Ek__BackingField_1() const { return ___U3CFunctionU3Ek__BackingField_1; }
	inline String_t** get_address_of_U3CFunctionU3Ek__BackingField_1() { return &___U3CFunctionU3Ek__BackingField_1; }
	inline void set_U3CFunctionU3Ek__BackingField_1(String_t* value)
	{
		___U3CFunctionU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CFunctionU3Ek__BackingField_1), value);
	}

	inline static int32_t get_offset_of_U3CTextU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(AddButtonAttribute_t3818261841, ___U3CTextU3Ek__BackingField_2)); }
	inline String_t* get_U3CTextU3Ek__BackingField_2() const { return ___U3CTextU3Ek__BackingField_2; }
	inline String_t** get_address_of_U3CTextU3Ek__BackingField_2() { return &___U3CTextU3Ek__BackingField_2; }
	inline void set_U3CTextU3Ek__BackingField_2(String_t* value)
	{
		___U3CTextU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CTextU3Ek__BackingField_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ADDBUTTONATTRIBUTE_T3818261841_H
#ifndef TYPE_T3182439621_H
#define TYPE_T3182439621_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.HelpBoxAttribute/Type
struct  Type_t3182439621 
{
public:
	// System.Int32 Utage.HelpBoxAttribute/Type::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(Type_t3182439621, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TYPE_T3182439621_H
#ifndef DICTIONARYSTRING_T3376238631_H
#define DICTIONARYSTRING_T3376238631_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.DictionaryString
struct  DictionaryString_t3376238631  : public SerializableDictionaryBinaryIO_1_t2453522167
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DICTIONARYSTRING_T3376238631_H
#ifndef DICTIONARYKEYVALUESTRING_T1086885503_H
#define DICTIONARYKEYVALUESTRING_T1086885503_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.DictionaryKeyValueString
struct  DictionaryKeyValueString_t1086885503  : public SerializableDictionaryBinaryIOKeyValue_t105246368
{
public:
	// System.String Utage.DictionaryKeyValueString::value
	String_t* ___value_1;

public:
	inline static int32_t get_offset_of_value_1() { return static_cast<int32_t>(offsetof(DictionaryKeyValueString_t1086885503, ___value_1)); }
	inline String_t* get_value_1() const { return ___value_1; }
	inline String_t** get_address_of_value_1() { return &___value_1; }
	inline void set_value_1(String_t* value)
	{
		___value_1 = value;
		Il2CppCodeGenWriteBarrier((&___value_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DICTIONARYKEYVALUESTRING_T1086885503_H
#ifndef ENUMFLAGSATTRIBUTE_T3854793506_H
#define ENUMFLAGSATTRIBUTE_T3854793506_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.EnumFlagsAttribute
struct  EnumFlagsAttribute_t3854793506  : public PropertyAttribute_t2606999759
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENUMFLAGSATTRIBUTE_T3854793506_H
#ifndef OBJECT_T1021602117_H
#define OBJECT_T1021602117_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Object
struct  Object_t1021602117  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Object::m_CachedPtr
	intptr_t ___m_CachedPtr_0;

public:
	inline static int32_t get_offset_of_m_CachedPtr_0() { return static_cast<int32_t>(offsetof(Object_t1021602117, ___m_CachedPtr_0)); }
	inline intptr_t get_m_CachedPtr_0() const { return ___m_CachedPtr_0; }
	inline intptr_t* get_address_of_m_CachedPtr_0() { return &___m_CachedPtr_0; }
	inline void set_m_CachedPtr_0(intptr_t value)
	{
		___m_CachedPtr_0 = value;
	}
};

struct Object_t1021602117_StaticFields
{
public:
	// System.Int32 UnityEngine.Object::OffsetOfInstanceIDInCPlusPlusObject
	int32_t ___OffsetOfInstanceIDInCPlusPlusObject_1;

public:
	inline static int32_t get_offset_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return static_cast<int32_t>(offsetof(Object_t1021602117_StaticFields, ___OffsetOfInstanceIDInCPlusPlusObject_1)); }
	inline int32_t get_OffsetOfInstanceIDInCPlusPlusObject_1() const { return ___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline int32_t* get_address_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return &___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline void set_OffsetOfInstanceIDInCPlusPlusObject_1(int32_t value)
	{
		___OffsetOfInstanceIDInCPlusPlusObject_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.Object
struct Object_t1021602117_marshaled_pinvoke
{
	intptr_t ___m_CachedPtr_0;
};
// Native definition for COM marshalling of UnityEngine.Object
struct Object_t1021602117_marshaled_com
{
	intptr_t ___m_CachedPtr_0;
};
#endif // OBJECT_T1021602117_H
#ifndef BACKLOGTYPE_T3430840675_H
#define BACKLOGTYPE_T3430840675_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.AdvUguiBacklogManager/BacklogType
struct  BacklogType_t3430840675 
{
public:
	// System.Int32 Utage.AdvUguiBacklogManager/BacklogType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(BacklogType_t3430840675, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BACKLOGTYPE_T3430840675_H
#ifndef MINMAXATTRIBUTE_T10394644_H
#define MINMAXATTRIBUTE_T10394644_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.MinMaxAttribute
struct  MinMaxAttribute_t10394644  : public PropertyAttribute_t2606999759
{
public:
	// System.Single Utage.MinMaxAttribute::<Min>k__BackingField
	float ___U3CMinU3Ek__BackingField_1;
	// System.Single Utage.MinMaxAttribute::<Max>k__BackingField
	float ___U3CMaxU3Ek__BackingField_2;
	// System.String Utage.MinMaxAttribute::<MinPropertyName>k__BackingField
	String_t* ___U3CMinPropertyNameU3Ek__BackingField_3;
	// System.String Utage.MinMaxAttribute::<MaxPropertyName>k__BackingField
	String_t* ___U3CMaxPropertyNameU3Ek__BackingField_4;

public:
	inline static int32_t get_offset_of_U3CMinU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(MinMaxAttribute_t10394644, ___U3CMinU3Ek__BackingField_1)); }
	inline float get_U3CMinU3Ek__BackingField_1() const { return ___U3CMinU3Ek__BackingField_1; }
	inline float* get_address_of_U3CMinU3Ek__BackingField_1() { return &___U3CMinU3Ek__BackingField_1; }
	inline void set_U3CMinU3Ek__BackingField_1(float value)
	{
		___U3CMinU3Ek__BackingField_1 = value;
	}

	inline static int32_t get_offset_of_U3CMaxU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(MinMaxAttribute_t10394644, ___U3CMaxU3Ek__BackingField_2)); }
	inline float get_U3CMaxU3Ek__BackingField_2() const { return ___U3CMaxU3Ek__BackingField_2; }
	inline float* get_address_of_U3CMaxU3Ek__BackingField_2() { return &___U3CMaxU3Ek__BackingField_2; }
	inline void set_U3CMaxU3Ek__BackingField_2(float value)
	{
		___U3CMaxU3Ek__BackingField_2 = value;
	}

	inline static int32_t get_offset_of_U3CMinPropertyNameU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(MinMaxAttribute_t10394644, ___U3CMinPropertyNameU3Ek__BackingField_3)); }
	inline String_t* get_U3CMinPropertyNameU3Ek__BackingField_3() const { return ___U3CMinPropertyNameU3Ek__BackingField_3; }
	inline String_t** get_address_of_U3CMinPropertyNameU3Ek__BackingField_3() { return &___U3CMinPropertyNameU3Ek__BackingField_3; }
	inline void set_U3CMinPropertyNameU3Ek__BackingField_3(String_t* value)
	{
		___U3CMinPropertyNameU3Ek__BackingField_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CMinPropertyNameU3Ek__BackingField_3), value);
	}

	inline static int32_t get_offset_of_U3CMaxPropertyNameU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(MinMaxAttribute_t10394644, ___U3CMaxPropertyNameU3Ek__BackingField_4)); }
	inline String_t* get_U3CMaxPropertyNameU3Ek__BackingField_4() const { return ___U3CMaxPropertyNameU3Ek__BackingField_4; }
	inline String_t** get_address_of_U3CMaxPropertyNameU3Ek__BackingField_4() { return &___U3CMaxPropertyNameU3Ek__BackingField_4; }
	inline void set_U3CMaxPropertyNameU3Ek__BackingField_4(String_t* value)
	{
		___U3CMaxPropertyNameU3Ek__BackingField_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CMaxPropertyNameU3Ek__BackingField_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MINMAXATTRIBUTE_T10394644_H
#ifndef MINATTRIBUTE_T155206958_H
#define MINATTRIBUTE_T155206958_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.MinAttribute
struct  MinAttribute_t155206958  : public PropertyAttribute_t2606999759
{
public:
	// System.Single Utage.MinAttribute::<Min>k__BackingField
	float ___U3CMinU3Ek__BackingField_1;

public:
	inline static int32_t get_offset_of_U3CMinU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(MinAttribute_t155206958, ___U3CMinU3Ek__BackingField_1)); }
	inline float get_U3CMinU3Ek__BackingField_1() const { return ___U3CMinU3Ek__BackingField_1; }
	inline float* get_address_of_U3CMinU3Ek__BackingField_1() { return &___U3CMinU3Ek__BackingField_1; }
	inline void set_U3CMinU3Ek__BackingField_1(float value)
	{
		___U3CMinU3Ek__BackingField_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MINATTRIBUTE_T155206958_H
#ifndef LIMITENUMATTRIBUTE_T2236047900_H
#define LIMITENUMATTRIBUTE_T2236047900_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.LimitEnumAttribute
struct  LimitEnumAttribute_t2236047900  : public PropertyAttribute_t2606999759
{
public:
	// System.String[] Utage.LimitEnumAttribute::<Args>k__BackingField
	StringU5BU5D_t1642385972* ___U3CArgsU3Ek__BackingField_1;

public:
	inline static int32_t get_offset_of_U3CArgsU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(LimitEnumAttribute_t2236047900, ___U3CArgsU3Ek__BackingField_1)); }
	inline StringU5BU5D_t1642385972* get_U3CArgsU3Ek__BackingField_1() const { return ___U3CArgsU3Ek__BackingField_1; }
	inline StringU5BU5D_t1642385972** get_address_of_U3CArgsU3Ek__BackingField_1() { return &___U3CArgsU3Ek__BackingField_1; }
	inline void set_U3CArgsU3Ek__BackingField_1(StringU5BU5D_t1642385972* value)
	{
		___U3CArgsU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CArgsU3Ek__BackingField_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LIMITENUMATTRIBUTE_T2236047900_H
#ifndef INTPOPUPATTRIBUTE_T3633498147_H
#define INTPOPUPATTRIBUTE_T3633498147_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.IntPopupAttribute
struct  IntPopupAttribute_t3633498147  : public PropertyAttribute_t2606999759
{
public:
	// System.Collections.Generic.List`1<System.Int32> Utage.IntPopupAttribute::popupList
	List_1_t1440998580 * ___popupList_1;

public:
	inline static int32_t get_offset_of_popupList_1() { return static_cast<int32_t>(offsetof(IntPopupAttribute_t3633498147, ___popupList_1)); }
	inline List_1_t1440998580 * get_popupList_1() const { return ___popupList_1; }
	inline List_1_t1440998580 ** get_address_of_popupList_1() { return &___popupList_1; }
	inline void set_popupList_1(List_1_t1440998580 * value)
	{
		___popupList_1 = value;
		Il2CppCodeGenWriteBarrier((&___popupList_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTPOPUPATTRIBUTE_T3633498147_H
#ifndef HORIZONTALATTRIBUTE_T2286046922_H
#define HORIZONTALATTRIBUTE_T2286046922_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.HorizontalAttribute
struct  HorizontalAttribute_t2286046922  : public PropertyAttribute_t2606999759
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HORIZONTALATTRIBUTE_T2286046922_H
#ifndef HIDEATTRIBUTE_T1097616606_H
#define HIDEATTRIBUTE_T1097616606_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.HideAttribute
struct  HideAttribute_t1097616606  : public PropertyAttribute_t2606999759
{
public:
	// System.String Utage.HideAttribute::<Function>k__BackingField
	String_t* ___U3CFunctionU3Ek__BackingField_1;

public:
	inline static int32_t get_offset_of_U3CFunctionU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(HideAttribute_t1097616606, ___U3CFunctionU3Ek__BackingField_1)); }
	inline String_t* get_U3CFunctionU3Ek__BackingField_1() const { return ___U3CFunctionU3Ek__BackingField_1; }
	inline String_t** get_address_of_U3CFunctionU3Ek__BackingField_1() { return &___U3CFunctionU3Ek__BackingField_1; }
	inline void set_U3CFunctionU3Ek__BackingField_1(String_t* value)
	{
		___U3CFunctionU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CFunctionU3Ek__BackingField_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HIDEATTRIBUTE_T1097616606_H
#ifndef DICTIONARYKEYVALUEDOUBLE_T1135602291_H
#define DICTIONARYKEYVALUEDOUBLE_T1135602291_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.DictionaryKeyValueDouble
struct  DictionaryKeyValueDouble_t1135602291  : public SerializableDictionaryBinaryIOKeyValue_t105246368
{
public:
	// System.Double Utage.DictionaryKeyValueDouble::value
	double ___value_1;

public:
	inline static int32_t get_offset_of_value_1() { return static_cast<int32_t>(offsetof(DictionaryKeyValueDouble_t1135602291, ___value_1)); }
	inline double get_value_1() const { return ___value_1; }
	inline double* get_address_of_value_1() { return &___value_1; }
	inline void set_value_1(double value)
	{
		___value_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DICTIONARYKEYVALUEDOUBLE_T1135602291_H
#ifndef ICONGRAPHICTYPE_T2552029784_H
#define ICONGRAPHICTYPE_T2552029784_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.AdvUguiMessageWindowFaceIcon/IconGraphicType
struct  IconGraphicType_t2552029784 
{
public:
	// System.Int32 Utage.AdvUguiMessageWindowFaceIcon/IconGraphicType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(IconGraphicType_t2552029784, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ICONGRAPHICTYPE_T2552029784_H
#ifndef ADVCOMMANDEVENT_T2718201788_H
#define ADVCOMMANDEVENT_T2718201788_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.AdvCommandEvent
struct  AdvCommandEvent_t2718201788  : public UnityEvent_1_t2898310999
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ADVCOMMANDEVENT_T2718201788_H
#ifndef SELECTEDCOLORMODE_T2529639836_H
#define SELECTEDCOLORMODE_T2529639836_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.AdvUguiSelectionManager/SelectedColorMode
struct  SelectedColorMode_t2529639836 
{
public:
	// System.Int32 Utage.AdvUguiSelectionManager/SelectedColorMode::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(SelectedColorMode_t2529639836, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SELECTEDCOLORMODE_T2529639836_H
#ifndef DEBUGOUTPUT_T1254200721_H
#define DEBUGOUTPUT_T1254200721_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.AdvScenarioPlayer/DebugOutPut
struct  DebugOutPut_t1254200721 
{
public:
	// System.Int32 Utage.AdvScenarioPlayer/DebugOutPut::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(DebugOutPut_t1254200721, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DEBUGOUTPUT_T1254200721_H
#ifndef ERRORMSG_T1355364995_H
#define ERRORMSG_T1355364995_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.ErrorMsg
struct  ErrorMsg_t1355364995 
{
public:
	// System.Int32 Utage.ErrorMsg::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(ErrorMsg_t1355364995, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ERRORMSG_T1355364995_H
#ifndef UISTATUS_T3434108101_H
#define UISTATUS_T3434108101_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.AdvUiManager/UiStatus
struct  UiStatus_t3434108101 
{
public:
	// System.Int32 Utage.AdvUiManager/UiStatus::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(UiStatus_t3434108101, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UISTATUS_T3434108101_H
#ifndef ADVCOMMANDWAITTYPE_T3266086913_H
#define ADVCOMMANDWAITTYPE_T3266086913_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.AdvCommandWaitType
struct  AdvCommandWaitType_t3266086913 
{
public:
	// System.Int32 Utage.AdvCommandWaitType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(AdvCommandWaitType_t3266086913, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ADVCOMMANDWAITTYPE_T3266086913_H
#ifndef CAPTURECAMERAEVENT_T3263087955_H
#define CAPTURECAMERAEVENT_T3263087955_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.CaptureCameraEvent
struct  CaptureCameraEvent_t3263087955  : public UnityEvent_1_t355565814
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CAPTURECAMERAEVENT_T3263087955_H
#ifndef ANCHORTYPE_T847091700_H
#define ANCHORTYPE_T847091700_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.LetterBoxCamera/AnchorType
struct  AnchorType_t847091700 
{
public:
	// System.Int32 Utage.LetterBoxCamera/AnchorType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(AnchorType_t847091700, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ANCHORTYPE_T847091700_H
#ifndef LANGUAGENAME_T1361222579_H
#define LANGUAGENAME_T1361222579_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.LanguageName
struct  LanguageName_t1361222579 
{
public:
	// System.Int32 Utage.LanguageName::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(LanguageName_t1361222579, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LANGUAGENAME_T1361222579_H
#ifndef DICTIONARYFLOAT_T436926956_H
#define DICTIONARYFLOAT_T436926956_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.DictionaryFloat
struct  DictionaryFloat_t436926956  : public SerializableDictionaryBinaryIO_1_t919821380
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DICTIONARYFLOAT_T436926956_H
#ifndef DICTIONARYKEYVALUEFLOAT_T3848152012_H
#define DICTIONARYKEYVALUEFLOAT_T3848152012_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.DictionaryKeyValueFloat
struct  DictionaryKeyValueFloat_t3848152012  : public SerializableDictionaryBinaryIOKeyValue_t105246368
{
public:
	// System.Single Utage.DictionaryKeyValueFloat::value
	float ___value_1;

public:
	inline static int32_t get_offset_of_value_1() { return static_cast<int32_t>(offsetof(DictionaryKeyValueFloat_t3848152012, ___value_1)); }
	inline float get_value_1() const { return ___value_1; }
	inline float* get_address_of_value_1() { return &___value_1; }
	inline void set_value_1(float value)
	{
		___value_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DICTIONARYKEYVALUEFLOAT_T3848152012_H
#ifndef DICTIONARYINT_T3731914685_H
#define DICTIONARYINT_T3731914685_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.DictionaryInt
struct  DictionaryInt_t3731914685  : public SerializableDictionaryBinaryIO_1_t663763689
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DICTIONARYINT_T3731914685_H
#ifndef DICTIONARYKEYVALUEINT_T3592094321_H
#define DICTIONARYKEYVALUEINT_T3592094321_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.DictionaryKeyValueInt
struct  DictionaryKeyValueInt_t3592094321  : public SerializableDictionaryBinaryIOKeyValue_t105246368
{
public:
	// System.Int32 Utage.DictionaryKeyValueInt::value
	int32_t ___value_1;

public:
	inline static int32_t get_offset_of_value_1() { return static_cast<int32_t>(offsetof(DictionaryKeyValueInt_t3592094321, ___value_1)); }
	inline int32_t get_value_1() const { return ___value_1; }
	inline int32_t* get_address_of_value_1() { return &___value_1; }
	inline void set_value_1(int32_t value)
	{
		___value_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DICTIONARYKEYVALUEINT_T3592094321_H
#ifndef SIZESETTING_T3046087381_H
#define SIZESETTING_T3046087381_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.AdvUguiLoadGraphicFile/SizeSetting
struct  SizeSetting_t3046087381 
{
public:
	// System.Int32 Utage.AdvUguiLoadGraphicFile/SizeSetting::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(SizeSetting_t3046087381, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SIZESETTING_T3046087381_H
#ifndef READCOLORMODE_T2593131983_H
#define READCOLORMODE_T2593131983_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.AdvUguiMessageWindow/ReadColorMode
struct  ReadColorMode_t2593131983 
{
public:
	// System.Int32 Utage.AdvUguiMessageWindow/ReadColorMode::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(ReadColorMode_t2593131983, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // READCOLORMODE_T2593131983_H
#ifndef SYSTEMTEXT_T2499456704_H
#define SYSTEMTEXT_T2499456704_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.SystemText
struct  SystemText_t2499456704 
{
public:
	// System.Int32 Utage.SystemText::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(SystemText_t2499456704, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SYSTEMTEXT_T2499456704_H
#ifndef ADVSCENARIOPLAYEREVENT_T1780583340_H
#define ADVSCENARIOPLAYEREVENT_T1780583340_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.AdvScenarioPlayerEvent
struct  AdvScenarioPlayerEvent_t1780583340  : public UnityEvent_1_t1334181495
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ADVSCENARIOPLAYEREVENT_T1780583340_H
#ifndef DICTIONARYKEYVALUEBOOL_T2944486586_H
#define DICTIONARYKEYVALUEBOOL_T2944486586_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.DictionaryKeyValueBool
struct  DictionaryKeyValueBool_t2944486586  : public SerializableDictionaryBinaryIOKeyValue_t105246368
{
public:
	// System.Boolean Utage.DictionaryKeyValueBool::value
	bool ___value_1;

public:
	inline static int32_t get_offset_of_value_1() { return static_cast<int32_t>(offsetof(DictionaryKeyValueBool_t2944486586, ___value_1)); }
	inline bool get_value_1() const { return ___value_1; }
	inline bool* get_address_of_value_1() { return &___value_1; }
	inline void set_value_1(bool value)
	{
		___value_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DICTIONARYKEYVALUEBOOL_T2944486586_H
#ifndef DICTIONARYBOOL_T255000822_H
#define DICTIONARYBOOL_T255000822_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.DictionaryBool
struct  DictionaryBool_t255000822  : public SerializableDictionaryBinaryIO_1_t16155954
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DICTIONARYBOOL_T255000822_H
#ifndef LETTERBOXCAMERAEVENT_T3581932304_H
#define LETTERBOXCAMERAEVENT_T3581932304_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.LetterBoxCameraEvent
struct  LetterBoxCameraEvent_t3581932304  : public UnityEvent_1_t3545967699
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LETTERBOXCAMERAEVENT_T3581932304_H
#ifndef COMPONENT_T3819376471_H
#define COMPONENT_T3819376471_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Component
struct  Component_t3819376471  : public Object_t1021602117
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPONENT_T3819376471_H
#ifndef SCRIPTABLEOBJECT_T1975622470_H
#define SCRIPTABLEOBJECT_T1975622470_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.ScriptableObject
struct  ScriptableObject_t1975622470  : public Object_t1021602117
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.ScriptableObject
struct ScriptableObject_t1975622470_marshaled_pinvoke : public Object_t1021602117_marshaled_pinvoke
{
};
// Native definition for COM marshalling of UnityEngine.ScriptableObject
struct ScriptableObject_t1975622470_marshaled_com : public Object_t1021602117_marshaled_com
{
};
#endif // SCRIPTABLEOBJECT_T1975622470_H
#ifndef HELPBOXATTRIBUTE_T1722922102_H
#define HELPBOXATTRIBUTE_T1722922102_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.HelpBoxAttribute
struct  HelpBoxAttribute_t1722922102  : public PropertyAttribute_t2606999759
{
public:
	// System.String Utage.HelpBoxAttribute::<Message>k__BackingField
	String_t* ___U3CMessageU3Ek__BackingField_1;
	// Utage.HelpBoxAttribute/Type Utage.HelpBoxAttribute::<MessageType>k__BackingField
	int32_t ___U3CMessageTypeU3Ek__BackingField_2;

public:
	inline static int32_t get_offset_of_U3CMessageU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(HelpBoxAttribute_t1722922102, ___U3CMessageU3Ek__BackingField_1)); }
	inline String_t* get_U3CMessageU3Ek__BackingField_1() const { return ___U3CMessageU3Ek__BackingField_1; }
	inline String_t** get_address_of_U3CMessageU3Ek__BackingField_1() { return &___U3CMessageU3Ek__BackingField_1; }
	inline void set_U3CMessageU3Ek__BackingField_1(String_t* value)
	{
		___U3CMessageU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CMessageU3Ek__BackingField_1), value);
	}

	inline static int32_t get_offset_of_U3CMessageTypeU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(HelpBoxAttribute_t1722922102, ___U3CMessageTypeU3Ek__BackingField_2)); }
	inline int32_t get_U3CMessageTypeU3Ek__BackingField_2() const { return ___U3CMessageTypeU3Ek__BackingField_2; }
	inline int32_t* get_address_of_U3CMessageTypeU3Ek__BackingField_2() { return &___U3CMessageTypeU3Ek__BackingField_2; }
	inline void set_U3CMessageTypeU3Ek__BackingField_2(int32_t value)
	{
		___U3CMessageTypeU3Ek__BackingField_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HELPBOXATTRIBUTE_T1722922102_H
#ifndef CUSTOMPROJECTSETTING_T3840122254_H
#define CUSTOMPROJECTSETTING_T3840122254_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.CustomProjectSetting
struct  CustomProjectSetting_t3840122254  : public ScriptableObject_t1975622470
{
public:
	// Utage.LanguageManager Utage.CustomProjectSetting::language
	LanguageManager_t2685744067 * ___language_3;

public:
	inline static int32_t get_offset_of_language_3() { return static_cast<int32_t>(offsetof(CustomProjectSetting_t3840122254, ___language_3)); }
	inline LanguageManager_t2685744067 * get_language_3() const { return ___language_3; }
	inline LanguageManager_t2685744067 ** get_address_of_language_3() { return &___language_3; }
	inline void set_language_3(LanguageManager_t2685744067 * value)
	{
		___language_3 = value;
		Il2CppCodeGenWriteBarrier((&___language_3), value);
	}
};

struct CustomProjectSetting_t3840122254_StaticFields
{
public:
	// Utage.CustomProjectSetting Utage.CustomProjectSetting::instance
	CustomProjectSetting_t3840122254 * ___instance_2;

public:
	inline static int32_t get_offset_of_instance_2() { return static_cast<int32_t>(offsetof(CustomProjectSetting_t3840122254_StaticFields, ___instance_2)); }
	inline CustomProjectSetting_t3840122254 * get_instance_2() const { return ___instance_2; }
	inline CustomProjectSetting_t3840122254 ** get_address_of_instance_2() { return &___instance_2; }
	inline void set_instance_2(CustomProjectSetting_t3840122254 * value)
	{
		___instance_2 = value;
		Il2CppCodeGenWriteBarrier((&___instance_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CUSTOMPROJECTSETTING_T3840122254_H
#ifndef LANGUAGEMANAGERBASE_T1275854546_H
#define LANGUAGEMANAGERBASE_T1275854546_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.LanguageManagerBase
struct  LanguageManagerBase_t1275854546  : public ScriptableObject_t1975622470
{
public:
	// System.String Utage.LanguageManagerBase::language
	String_t* ___language_4;
	// System.String Utage.LanguageManagerBase::defaultLanguage
	String_t* ___defaultLanguage_5;
	// System.Collections.Generic.List`1<UnityEngine.TextAsset> Utage.LanguageManagerBase::languageData
	List_1_t3342280977 * ___languageData_6;
	// System.Boolean Utage.LanguageManagerBase::ignoreLocalizeUiText
	bool ___ignoreLocalizeUiText_7;
	// System.Boolean Utage.LanguageManagerBase::ignoreLocalizeVoice
	bool ___ignoreLocalizeVoice_8;
	// System.Collections.Generic.List`1<System.String> Utage.LanguageManagerBase::voiceLanguages
	List_1_t1398341365 * ___voiceLanguages_9;
	// System.Action Utage.LanguageManagerBase::<OnChangeLanugage>k__BackingField
	Action_t3226471752 * ___U3COnChangeLanugageU3Ek__BackingField_10;
	// System.String Utage.LanguageManagerBase::currentLanguage
	String_t* ___currentLanguage_11;
	// Utage.LanguageData Utage.LanguageManagerBase::<Data>k__BackingField
	LanguageData_t3686821228 * ___U3CDataU3Ek__BackingField_12;

public:
	inline static int32_t get_offset_of_language_4() { return static_cast<int32_t>(offsetof(LanguageManagerBase_t1275854546, ___language_4)); }
	inline String_t* get_language_4() const { return ___language_4; }
	inline String_t** get_address_of_language_4() { return &___language_4; }
	inline void set_language_4(String_t* value)
	{
		___language_4 = value;
		Il2CppCodeGenWriteBarrier((&___language_4), value);
	}

	inline static int32_t get_offset_of_defaultLanguage_5() { return static_cast<int32_t>(offsetof(LanguageManagerBase_t1275854546, ___defaultLanguage_5)); }
	inline String_t* get_defaultLanguage_5() const { return ___defaultLanguage_5; }
	inline String_t** get_address_of_defaultLanguage_5() { return &___defaultLanguage_5; }
	inline void set_defaultLanguage_5(String_t* value)
	{
		___defaultLanguage_5 = value;
		Il2CppCodeGenWriteBarrier((&___defaultLanguage_5), value);
	}

	inline static int32_t get_offset_of_languageData_6() { return static_cast<int32_t>(offsetof(LanguageManagerBase_t1275854546, ___languageData_6)); }
	inline List_1_t3342280977 * get_languageData_6() const { return ___languageData_6; }
	inline List_1_t3342280977 ** get_address_of_languageData_6() { return &___languageData_6; }
	inline void set_languageData_6(List_1_t3342280977 * value)
	{
		___languageData_6 = value;
		Il2CppCodeGenWriteBarrier((&___languageData_6), value);
	}

	inline static int32_t get_offset_of_ignoreLocalizeUiText_7() { return static_cast<int32_t>(offsetof(LanguageManagerBase_t1275854546, ___ignoreLocalizeUiText_7)); }
	inline bool get_ignoreLocalizeUiText_7() const { return ___ignoreLocalizeUiText_7; }
	inline bool* get_address_of_ignoreLocalizeUiText_7() { return &___ignoreLocalizeUiText_7; }
	inline void set_ignoreLocalizeUiText_7(bool value)
	{
		___ignoreLocalizeUiText_7 = value;
	}

	inline static int32_t get_offset_of_ignoreLocalizeVoice_8() { return static_cast<int32_t>(offsetof(LanguageManagerBase_t1275854546, ___ignoreLocalizeVoice_8)); }
	inline bool get_ignoreLocalizeVoice_8() const { return ___ignoreLocalizeVoice_8; }
	inline bool* get_address_of_ignoreLocalizeVoice_8() { return &___ignoreLocalizeVoice_8; }
	inline void set_ignoreLocalizeVoice_8(bool value)
	{
		___ignoreLocalizeVoice_8 = value;
	}

	inline static int32_t get_offset_of_voiceLanguages_9() { return static_cast<int32_t>(offsetof(LanguageManagerBase_t1275854546, ___voiceLanguages_9)); }
	inline List_1_t1398341365 * get_voiceLanguages_9() const { return ___voiceLanguages_9; }
	inline List_1_t1398341365 ** get_address_of_voiceLanguages_9() { return &___voiceLanguages_9; }
	inline void set_voiceLanguages_9(List_1_t1398341365 * value)
	{
		___voiceLanguages_9 = value;
		Il2CppCodeGenWriteBarrier((&___voiceLanguages_9), value);
	}

	inline static int32_t get_offset_of_U3COnChangeLanugageU3Ek__BackingField_10() { return static_cast<int32_t>(offsetof(LanguageManagerBase_t1275854546, ___U3COnChangeLanugageU3Ek__BackingField_10)); }
	inline Action_t3226471752 * get_U3COnChangeLanugageU3Ek__BackingField_10() const { return ___U3COnChangeLanugageU3Ek__BackingField_10; }
	inline Action_t3226471752 ** get_address_of_U3COnChangeLanugageU3Ek__BackingField_10() { return &___U3COnChangeLanugageU3Ek__BackingField_10; }
	inline void set_U3COnChangeLanugageU3Ek__BackingField_10(Action_t3226471752 * value)
	{
		___U3COnChangeLanugageU3Ek__BackingField_10 = value;
		Il2CppCodeGenWriteBarrier((&___U3COnChangeLanugageU3Ek__BackingField_10), value);
	}

	inline static int32_t get_offset_of_currentLanguage_11() { return static_cast<int32_t>(offsetof(LanguageManagerBase_t1275854546, ___currentLanguage_11)); }
	inline String_t* get_currentLanguage_11() const { return ___currentLanguage_11; }
	inline String_t** get_address_of_currentLanguage_11() { return &___currentLanguage_11; }
	inline void set_currentLanguage_11(String_t* value)
	{
		___currentLanguage_11 = value;
		Il2CppCodeGenWriteBarrier((&___currentLanguage_11), value);
	}

	inline static int32_t get_offset_of_U3CDataU3Ek__BackingField_12() { return static_cast<int32_t>(offsetof(LanguageManagerBase_t1275854546, ___U3CDataU3Ek__BackingField_12)); }
	inline LanguageData_t3686821228 * get_U3CDataU3Ek__BackingField_12() const { return ___U3CDataU3Ek__BackingField_12; }
	inline LanguageData_t3686821228 ** get_address_of_U3CDataU3Ek__BackingField_12() { return &___U3CDataU3Ek__BackingField_12; }
	inline void set_U3CDataU3Ek__BackingField_12(LanguageData_t3686821228 * value)
	{
		___U3CDataU3Ek__BackingField_12 = value;
		Il2CppCodeGenWriteBarrier((&___U3CDataU3Ek__BackingField_12), value);
	}
};

struct LanguageManagerBase_t1275854546_StaticFields
{
public:
	// Utage.LanguageManagerBase Utage.LanguageManagerBase::instance
	LanguageManagerBase_t1275854546 * ___instance_2;

public:
	inline static int32_t get_offset_of_instance_2() { return static_cast<int32_t>(offsetof(LanguageManagerBase_t1275854546_StaticFields, ___instance_2)); }
	inline LanguageManagerBase_t1275854546 * get_instance_2() const { return ___instance_2; }
	inline LanguageManagerBase_t1275854546 ** get_address_of_instance_2() { return &___instance_2; }
	inline void set_instance_2(LanguageManagerBase_t1275854546 * value)
	{
		___instance_2 = value;
		Il2CppCodeGenWriteBarrier((&___instance_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LANGUAGEMANAGERBASE_T1275854546_H
#ifndef BEHAVIOUR_T955675639_H
#define BEHAVIOUR_T955675639_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Behaviour
struct  Behaviour_t955675639  : public Component_t3819376471
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BEHAVIOUR_T955675639_H
#ifndef ADVSCENARIODATAEXPORTED_T393182426_H
#define ADVSCENARIODATAEXPORTED_T393182426_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.AdvScenarioDataExported
struct  AdvScenarioDataExported_t393182426  : public ScriptableObject_t1975622470
{
public:
	// Utage.StringGridDictionary Utage.AdvScenarioDataExported::dictionary
	StringGridDictionary_t1396150451 * ___dictionary_2;

public:
	inline static int32_t get_offset_of_dictionary_2() { return static_cast<int32_t>(offsetof(AdvScenarioDataExported_t393182426, ___dictionary_2)); }
	inline StringGridDictionary_t1396150451 * get_dictionary_2() const { return ___dictionary_2; }
	inline StringGridDictionary_t1396150451 ** get_address_of_dictionary_2() { return &___dictionary_2; }
	inline void set_dictionary_2(StringGridDictionary_t1396150451 * value)
	{
		___dictionary_2 = value;
		Il2CppCodeGenWriteBarrier((&___dictionary_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ADVSCENARIODATAEXPORTED_T393182426_H
#ifndef LANGUAGEMANAGER_T2685744067_H
#define LANGUAGEMANAGER_T2685744067_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.LanguageManager
struct  LanguageManager_t2685744067  : public LanguageManagerBase_t1275854546
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LANGUAGEMANAGER_T2685744067_H
#ifndef MONOBEHAVIOUR_T1158329972_H
#define MONOBEHAVIOUR_T1158329972_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.MonoBehaviour
struct  MonoBehaviour_t1158329972  : public Behaviour_t955675639
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MONOBEHAVIOUR_T1158329972_H
#ifndef ADVUGUIMESSAGEWINDOWMANAGER_T1210998287_H
#define ADVUGUIMESSAGEWINDOWMANAGER_T1210998287_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.AdvUguiMessageWindowManager
struct  AdvUguiMessageWindowManager_t1210998287  : public MonoBehaviour_t1158329972
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ADVUGUIMESSAGEWINDOWMANAGER_T1210998287_H
#ifndef ADVUGUIMESSAGEWINDOWFACEICON_T623217832_H
#define ADVUGUIMESSAGEWINDOWFACEICON_T623217832_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.AdvUguiMessageWindowFaceIcon
struct  AdvUguiMessageWindowFaceIcon_t623217832  : public MonoBehaviour_t1158329972
{
public:
	// Utage.AdvEngine Utage.AdvUguiMessageWindowFaceIcon::engine
	AdvEngine_t1176753927 * ___engine_2;
	// Utage.AdvGraphicObject Utage.AdvUguiMessageWindowFaceIcon::targetObject
	AdvGraphicObject_t3651915246 * ___targetObject_3;
	// UnityEngine.GameObject Utage.AdvUguiMessageWindowFaceIcon::iconRoot
	GameObject_t1756533147 * ___iconRoot_4;

public:
	inline static int32_t get_offset_of_engine_2() { return static_cast<int32_t>(offsetof(AdvUguiMessageWindowFaceIcon_t623217832, ___engine_2)); }
	inline AdvEngine_t1176753927 * get_engine_2() const { return ___engine_2; }
	inline AdvEngine_t1176753927 ** get_address_of_engine_2() { return &___engine_2; }
	inline void set_engine_2(AdvEngine_t1176753927 * value)
	{
		___engine_2 = value;
		Il2CppCodeGenWriteBarrier((&___engine_2), value);
	}

	inline static int32_t get_offset_of_targetObject_3() { return static_cast<int32_t>(offsetof(AdvUguiMessageWindowFaceIcon_t623217832, ___targetObject_3)); }
	inline AdvGraphicObject_t3651915246 * get_targetObject_3() const { return ___targetObject_3; }
	inline AdvGraphicObject_t3651915246 ** get_address_of_targetObject_3() { return &___targetObject_3; }
	inline void set_targetObject_3(AdvGraphicObject_t3651915246 * value)
	{
		___targetObject_3 = value;
		Il2CppCodeGenWriteBarrier((&___targetObject_3), value);
	}

	inline static int32_t get_offset_of_iconRoot_4() { return static_cast<int32_t>(offsetof(AdvUguiMessageWindowFaceIcon_t623217832, ___iconRoot_4)); }
	inline GameObject_t1756533147 * get_iconRoot_4() const { return ___iconRoot_4; }
	inline GameObject_t1756533147 ** get_address_of_iconRoot_4() { return &___iconRoot_4; }
	inline void set_iconRoot_4(GameObject_t1756533147 * value)
	{
		___iconRoot_4 = value;
		Il2CppCodeGenWriteBarrier((&___iconRoot_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ADVUGUIMESSAGEWINDOWFACEICON_T623217832_H
#ifndef ADVUGUIMESSAGEWINDOW_T2947985100_H
#define ADVUGUIMESSAGEWINDOW_T2947985100_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.AdvUguiMessageWindow
struct  AdvUguiMessageWindow_t2947985100  : public MonoBehaviour_t1158329972
{
public:
	// Utage.AdvEngine Utage.AdvUguiMessageWindow::engine
	AdvEngine_t1176753927 * ___engine_2;
	// Utage.AdvUguiMessageWindow/ReadColorMode Utage.AdvUguiMessageWindow::readColorMode
	int32_t ___readColorMode_3;
	// UnityEngine.Color Utage.AdvUguiMessageWindow::readColor
	Color_t2020392075  ___readColor_4;
	// UnityEngine.Color Utage.AdvUguiMessageWindow::defaultTextColor
	Color_t2020392075  ___defaultTextColor_5;
	// UnityEngine.Color Utage.AdvUguiMessageWindow::defaultNameTextColor
	Color_t2020392075  ___defaultNameTextColor_6;
	// Utage.UguiNovelText Utage.AdvUguiMessageWindow::text
	UguiNovelText_t4135744055 * ___text_7;
	// UnityEngine.UI.Text Utage.AdvUguiMessageWindow::nameText
	Text_t356221433 * ___nameText_8;
	// UnityEngine.GameObject Utage.AdvUguiMessageWindow::rootChildren
	GameObject_t1756533147 * ___rootChildren_9;
	// UnityEngine.CanvasGroup Utage.AdvUguiMessageWindow::translateMessageWindowRoot
	CanvasGroup_t3296560743 * ___translateMessageWindowRoot_10;
	// UnityEngine.GameObject Utage.AdvUguiMessageWindow::iconWaitInput
	GameObject_t1756533147 * ___iconWaitInput_11;
	// UnityEngine.GameObject Utage.AdvUguiMessageWindow::iconBrPage
	GameObject_t1756533147 * ___iconBrPage_12;
	// System.Boolean Utage.AdvUguiMessageWindow::isLinkPositionIconBrPage
	bool ___isLinkPositionIconBrPage_13;
	// System.Boolean Utage.AdvUguiMessageWindow::<IsCurrent>k__BackingField
	bool ___U3CIsCurrentU3Ek__BackingField_14;

public:
	inline static int32_t get_offset_of_engine_2() { return static_cast<int32_t>(offsetof(AdvUguiMessageWindow_t2947985100, ___engine_2)); }
	inline AdvEngine_t1176753927 * get_engine_2() const { return ___engine_2; }
	inline AdvEngine_t1176753927 ** get_address_of_engine_2() { return &___engine_2; }
	inline void set_engine_2(AdvEngine_t1176753927 * value)
	{
		___engine_2 = value;
		Il2CppCodeGenWriteBarrier((&___engine_2), value);
	}

	inline static int32_t get_offset_of_readColorMode_3() { return static_cast<int32_t>(offsetof(AdvUguiMessageWindow_t2947985100, ___readColorMode_3)); }
	inline int32_t get_readColorMode_3() const { return ___readColorMode_3; }
	inline int32_t* get_address_of_readColorMode_3() { return &___readColorMode_3; }
	inline void set_readColorMode_3(int32_t value)
	{
		___readColorMode_3 = value;
	}

	inline static int32_t get_offset_of_readColor_4() { return static_cast<int32_t>(offsetof(AdvUguiMessageWindow_t2947985100, ___readColor_4)); }
	inline Color_t2020392075  get_readColor_4() const { return ___readColor_4; }
	inline Color_t2020392075 * get_address_of_readColor_4() { return &___readColor_4; }
	inline void set_readColor_4(Color_t2020392075  value)
	{
		___readColor_4 = value;
	}

	inline static int32_t get_offset_of_defaultTextColor_5() { return static_cast<int32_t>(offsetof(AdvUguiMessageWindow_t2947985100, ___defaultTextColor_5)); }
	inline Color_t2020392075  get_defaultTextColor_5() const { return ___defaultTextColor_5; }
	inline Color_t2020392075 * get_address_of_defaultTextColor_5() { return &___defaultTextColor_5; }
	inline void set_defaultTextColor_5(Color_t2020392075  value)
	{
		___defaultTextColor_5 = value;
	}

	inline static int32_t get_offset_of_defaultNameTextColor_6() { return static_cast<int32_t>(offsetof(AdvUguiMessageWindow_t2947985100, ___defaultNameTextColor_6)); }
	inline Color_t2020392075  get_defaultNameTextColor_6() const { return ___defaultNameTextColor_6; }
	inline Color_t2020392075 * get_address_of_defaultNameTextColor_6() { return &___defaultNameTextColor_6; }
	inline void set_defaultNameTextColor_6(Color_t2020392075  value)
	{
		___defaultNameTextColor_6 = value;
	}

	inline static int32_t get_offset_of_text_7() { return static_cast<int32_t>(offsetof(AdvUguiMessageWindow_t2947985100, ___text_7)); }
	inline UguiNovelText_t4135744055 * get_text_7() const { return ___text_7; }
	inline UguiNovelText_t4135744055 ** get_address_of_text_7() { return &___text_7; }
	inline void set_text_7(UguiNovelText_t4135744055 * value)
	{
		___text_7 = value;
		Il2CppCodeGenWriteBarrier((&___text_7), value);
	}

	inline static int32_t get_offset_of_nameText_8() { return static_cast<int32_t>(offsetof(AdvUguiMessageWindow_t2947985100, ___nameText_8)); }
	inline Text_t356221433 * get_nameText_8() const { return ___nameText_8; }
	inline Text_t356221433 ** get_address_of_nameText_8() { return &___nameText_8; }
	inline void set_nameText_8(Text_t356221433 * value)
	{
		___nameText_8 = value;
		Il2CppCodeGenWriteBarrier((&___nameText_8), value);
	}

	inline static int32_t get_offset_of_rootChildren_9() { return static_cast<int32_t>(offsetof(AdvUguiMessageWindow_t2947985100, ___rootChildren_9)); }
	inline GameObject_t1756533147 * get_rootChildren_9() const { return ___rootChildren_9; }
	inline GameObject_t1756533147 ** get_address_of_rootChildren_9() { return &___rootChildren_9; }
	inline void set_rootChildren_9(GameObject_t1756533147 * value)
	{
		___rootChildren_9 = value;
		Il2CppCodeGenWriteBarrier((&___rootChildren_9), value);
	}

	inline static int32_t get_offset_of_translateMessageWindowRoot_10() { return static_cast<int32_t>(offsetof(AdvUguiMessageWindow_t2947985100, ___translateMessageWindowRoot_10)); }
	inline CanvasGroup_t3296560743 * get_translateMessageWindowRoot_10() const { return ___translateMessageWindowRoot_10; }
	inline CanvasGroup_t3296560743 ** get_address_of_translateMessageWindowRoot_10() { return &___translateMessageWindowRoot_10; }
	inline void set_translateMessageWindowRoot_10(CanvasGroup_t3296560743 * value)
	{
		___translateMessageWindowRoot_10 = value;
		Il2CppCodeGenWriteBarrier((&___translateMessageWindowRoot_10), value);
	}

	inline static int32_t get_offset_of_iconWaitInput_11() { return static_cast<int32_t>(offsetof(AdvUguiMessageWindow_t2947985100, ___iconWaitInput_11)); }
	inline GameObject_t1756533147 * get_iconWaitInput_11() const { return ___iconWaitInput_11; }
	inline GameObject_t1756533147 ** get_address_of_iconWaitInput_11() { return &___iconWaitInput_11; }
	inline void set_iconWaitInput_11(GameObject_t1756533147 * value)
	{
		___iconWaitInput_11 = value;
		Il2CppCodeGenWriteBarrier((&___iconWaitInput_11), value);
	}

	inline static int32_t get_offset_of_iconBrPage_12() { return static_cast<int32_t>(offsetof(AdvUguiMessageWindow_t2947985100, ___iconBrPage_12)); }
	inline GameObject_t1756533147 * get_iconBrPage_12() const { return ___iconBrPage_12; }
	inline GameObject_t1756533147 ** get_address_of_iconBrPage_12() { return &___iconBrPage_12; }
	inline void set_iconBrPage_12(GameObject_t1756533147 * value)
	{
		___iconBrPage_12 = value;
		Il2CppCodeGenWriteBarrier((&___iconBrPage_12), value);
	}

	inline static int32_t get_offset_of_isLinkPositionIconBrPage_13() { return static_cast<int32_t>(offsetof(AdvUguiMessageWindow_t2947985100, ___isLinkPositionIconBrPage_13)); }
	inline bool get_isLinkPositionIconBrPage_13() const { return ___isLinkPositionIconBrPage_13; }
	inline bool* get_address_of_isLinkPositionIconBrPage_13() { return &___isLinkPositionIconBrPage_13; }
	inline void set_isLinkPositionIconBrPage_13(bool value)
	{
		___isLinkPositionIconBrPage_13 = value;
	}

	inline static int32_t get_offset_of_U3CIsCurrentU3Ek__BackingField_14() { return static_cast<int32_t>(offsetof(AdvUguiMessageWindow_t2947985100, ___U3CIsCurrentU3Ek__BackingField_14)); }
	inline bool get_U3CIsCurrentU3Ek__BackingField_14() const { return ___U3CIsCurrentU3Ek__BackingField_14; }
	inline bool* get_address_of_U3CIsCurrentU3Ek__BackingField_14() { return &___U3CIsCurrentU3Ek__BackingField_14; }
	inline void set_U3CIsCurrentU3Ek__BackingField_14(bool value)
	{
		___U3CIsCurrentU3Ek__BackingField_14 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ADVUGUIMESSAGEWINDOW_T2947985100_H
#ifndef ADVUGUIBACKLOGMANAGER_T342992083_H
#define ADVUGUIBACKLOGMANAGER_T342992083_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.AdvUguiBacklogManager
struct  AdvUguiBacklogManager_t342992083  : public MonoBehaviour_t1158329972
{
public:
	// Utage.AdvUguiBacklogManager/BacklogType Utage.AdvUguiBacklogManager::type
	int32_t ___type_2;
	// Utage.AdvEngine Utage.AdvUguiBacklogManager::engine
	AdvEngine_t1176753927 * ___engine_3;
	// Utage.UguiListView Utage.AdvUguiBacklogManager::listView
	UguiListView_t169672791 * ___listView_4;
	// Utage.UguiNovelText Utage.AdvUguiBacklogManager::fullScreenLogText
	UguiNovelText_t4135744055 * ___fullScreenLogText_5;
	// System.Boolean Utage.AdvUguiBacklogManager::isCloseScrollWheelDown
	bool ___isCloseScrollWheelDown_6;

public:
	inline static int32_t get_offset_of_type_2() { return static_cast<int32_t>(offsetof(AdvUguiBacklogManager_t342992083, ___type_2)); }
	inline int32_t get_type_2() const { return ___type_2; }
	inline int32_t* get_address_of_type_2() { return &___type_2; }
	inline void set_type_2(int32_t value)
	{
		___type_2 = value;
	}

	inline static int32_t get_offset_of_engine_3() { return static_cast<int32_t>(offsetof(AdvUguiBacklogManager_t342992083, ___engine_3)); }
	inline AdvEngine_t1176753927 * get_engine_3() const { return ___engine_3; }
	inline AdvEngine_t1176753927 ** get_address_of_engine_3() { return &___engine_3; }
	inline void set_engine_3(AdvEngine_t1176753927 * value)
	{
		___engine_3 = value;
		Il2CppCodeGenWriteBarrier((&___engine_3), value);
	}

	inline static int32_t get_offset_of_listView_4() { return static_cast<int32_t>(offsetof(AdvUguiBacklogManager_t342992083, ___listView_4)); }
	inline UguiListView_t169672791 * get_listView_4() const { return ___listView_4; }
	inline UguiListView_t169672791 ** get_address_of_listView_4() { return &___listView_4; }
	inline void set_listView_4(UguiListView_t169672791 * value)
	{
		___listView_4 = value;
		Il2CppCodeGenWriteBarrier((&___listView_4), value);
	}

	inline static int32_t get_offset_of_fullScreenLogText_5() { return static_cast<int32_t>(offsetof(AdvUguiBacklogManager_t342992083, ___fullScreenLogText_5)); }
	inline UguiNovelText_t4135744055 * get_fullScreenLogText_5() const { return ___fullScreenLogText_5; }
	inline UguiNovelText_t4135744055 ** get_address_of_fullScreenLogText_5() { return &___fullScreenLogText_5; }
	inline void set_fullScreenLogText_5(UguiNovelText_t4135744055 * value)
	{
		___fullScreenLogText_5 = value;
		Il2CppCodeGenWriteBarrier((&___fullScreenLogText_5), value);
	}

	inline static int32_t get_offset_of_isCloseScrollWheelDown_6() { return static_cast<int32_t>(offsetof(AdvUguiBacklogManager_t342992083, ___isCloseScrollWheelDown_6)); }
	inline bool get_isCloseScrollWheelDown_6() const { return ___isCloseScrollWheelDown_6; }
	inline bool* get_address_of_isCloseScrollWheelDown_6() { return &___isCloseScrollWheelDown_6; }
	inline void set_isCloseScrollWheelDown_6(bool value)
	{
		___isCloseScrollWheelDown_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ADVUGUIBACKLOGMANAGER_T342992083_H
#ifndef ADVUGUILOADGRAPHICFILE_T1474676621_H
#define ADVUGUILOADGRAPHICFILE_T1474676621_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.AdvUguiLoadGraphicFile
struct  AdvUguiLoadGraphicFile_t1474676621  : public MonoBehaviour_t1158329972
{
public:
	// Utage.AdvGraphicLoader Utage.AdvUguiLoadGraphicFile::loader
	AdvGraphicLoader_t202702654 * ___loader_2;
	// UnityEngine.UI.Graphic Utage.AdvUguiLoadGraphicFile::<GraphicComponent>k__BackingField
	Graphic_t2426225576 * ___U3CGraphicComponentU3Ek__BackingField_3;
	// Utage.AssetFile Utage.AdvUguiLoadGraphicFile::<File>k__BackingField
	RuntimeObject* ___U3CFileU3Ek__BackingField_4;
	// Utage.AdvGraphicInfo Utage.AdvUguiLoadGraphicFile::<GraphicInfo>k__BackingField
	AdvGraphicInfo_t3545565645 * ___U3CGraphicInfoU3Ek__BackingField_5;
	// Utage.AdvUguiLoadGraphicFile/SizeSetting Utage.AdvUguiLoadGraphicFile::sizeSetting
	int32_t ___sizeSetting_6;
	// UnityEngine.Events.UnityEvent Utage.AdvUguiLoadGraphicFile::OnLoadEnd
	UnityEvent_t408735097 * ___OnLoadEnd_7;

public:
	inline static int32_t get_offset_of_loader_2() { return static_cast<int32_t>(offsetof(AdvUguiLoadGraphicFile_t1474676621, ___loader_2)); }
	inline AdvGraphicLoader_t202702654 * get_loader_2() const { return ___loader_2; }
	inline AdvGraphicLoader_t202702654 ** get_address_of_loader_2() { return &___loader_2; }
	inline void set_loader_2(AdvGraphicLoader_t202702654 * value)
	{
		___loader_2 = value;
		Il2CppCodeGenWriteBarrier((&___loader_2), value);
	}

	inline static int32_t get_offset_of_U3CGraphicComponentU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(AdvUguiLoadGraphicFile_t1474676621, ___U3CGraphicComponentU3Ek__BackingField_3)); }
	inline Graphic_t2426225576 * get_U3CGraphicComponentU3Ek__BackingField_3() const { return ___U3CGraphicComponentU3Ek__BackingField_3; }
	inline Graphic_t2426225576 ** get_address_of_U3CGraphicComponentU3Ek__BackingField_3() { return &___U3CGraphicComponentU3Ek__BackingField_3; }
	inline void set_U3CGraphicComponentU3Ek__BackingField_3(Graphic_t2426225576 * value)
	{
		___U3CGraphicComponentU3Ek__BackingField_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CGraphicComponentU3Ek__BackingField_3), value);
	}

	inline static int32_t get_offset_of_U3CFileU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(AdvUguiLoadGraphicFile_t1474676621, ___U3CFileU3Ek__BackingField_4)); }
	inline RuntimeObject* get_U3CFileU3Ek__BackingField_4() const { return ___U3CFileU3Ek__BackingField_4; }
	inline RuntimeObject** get_address_of_U3CFileU3Ek__BackingField_4() { return &___U3CFileU3Ek__BackingField_4; }
	inline void set_U3CFileU3Ek__BackingField_4(RuntimeObject* value)
	{
		___U3CFileU3Ek__BackingField_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CFileU3Ek__BackingField_4), value);
	}

	inline static int32_t get_offset_of_U3CGraphicInfoU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(AdvUguiLoadGraphicFile_t1474676621, ___U3CGraphicInfoU3Ek__BackingField_5)); }
	inline AdvGraphicInfo_t3545565645 * get_U3CGraphicInfoU3Ek__BackingField_5() const { return ___U3CGraphicInfoU3Ek__BackingField_5; }
	inline AdvGraphicInfo_t3545565645 ** get_address_of_U3CGraphicInfoU3Ek__BackingField_5() { return &___U3CGraphicInfoU3Ek__BackingField_5; }
	inline void set_U3CGraphicInfoU3Ek__BackingField_5(AdvGraphicInfo_t3545565645 * value)
	{
		___U3CGraphicInfoU3Ek__BackingField_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3CGraphicInfoU3Ek__BackingField_5), value);
	}

	inline static int32_t get_offset_of_sizeSetting_6() { return static_cast<int32_t>(offsetof(AdvUguiLoadGraphicFile_t1474676621, ___sizeSetting_6)); }
	inline int32_t get_sizeSetting_6() const { return ___sizeSetting_6; }
	inline int32_t* get_address_of_sizeSetting_6() { return &___sizeSetting_6; }
	inline void set_sizeSetting_6(int32_t value)
	{
		___sizeSetting_6 = value;
	}

	inline static int32_t get_offset_of_OnLoadEnd_7() { return static_cast<int32_t>(offsetof(AdvUguiLoadGraphicFile_t1474676621, ___OnLoadEnd_7)); }
	inline UnityEvent_t408735097 * get_OnLoadEnd_7() const { return ___OnLoadEnd_7; }
	inline UnityEvent_t408735097 ** get_address_of_OnLoadEnd_7() { return &___OnLoadEnd_7; }
	inline void set_OnLoadEnd_7(UnityEvent_t408735097 * value)
	{
		___OnLoadEnd_7 = value;
		Il2CppCodeGenWriteBarrier((&___OnLoadEnd_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ADVUGUILOADGRAPHICFILE_T1474676621_H
#ifndef ADVUGUISELECTION_T2632218991_H
#define ADVUGUISELECTION_T2632218991_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.AdvUguiSelection
struct  AdvUguiSelection_t2632218991  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.UI.Text Utage.AdvUguiSelection::text
	Text_t356221433 * ___text_2;
	// Utage.AdvSelection Utage.AdvUguiSelection::data
	AdvSelection_t2301351953 * ___data_3;

public:
	inline static int32_t get_offset_of_text_2() { return static_cast<int32_t>(offsetof(AdvUguiSelection_t2632218991, ___text_2)); }
	inline Text_t356221433 * get_text_2() const { return ___text_2; }
	inline Text_t356221433 ** get_address_of_text_2() { return &___text_2; }
	inline void set_text_2(Text_t356221433 * value)
	{
		___text_2 = value;
		Il2CppCodeGenWriteBarrier((&___text_2), value);
	}

	inline static int32_t get_offset_of_data_3() { return static_cast<int32_t>(offsetof(AdvUguiSelection_t2632218991, ___data_3)); }
	inline AdvSelection_t2301351953 * get_data_3() const { return ___data_3; }
	inline AdvSelection_t2301351953 ** get_address_of_data_3() { return &___data_3; }
	inline void set_data_3(AdvSelection_t2301351953 * value)
	{
		___data_3 = value;
		Il2CppCodeGenWriteBarrier((&___data_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ADVUGUISELECTION_T2632218991_H
#ifndef LETTERBOXCAMERA_T3507617684_H
#define LETTERBOXCAMERA_T3507617684_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.LetterBoxCamera
struct  LetterBoxCamera_t3507617684  : public MonoBehaviour_t1158329972
{
public:
	// System.Int32 Utage.LetterBoxCamera::pixelsToUnits
	int32_t ___pixelsToUnits_2;
	// System.Int32 Utage.LetterBoxCamera::width
	int32_t ___width_3;
	// System.Int32 Utage.LetterBoxCamera::height
	int32_t ___height_4;
	// System.Boolean Utage.LetterBoxCamera::isFlexible
	bool ___isFlexible_5;
	// System.Int32 Utage.LetterBoxCamera::maxWidth
	int32_t ___maxWidth_6;
	// System.Int32 Utage.LetterBoxCamera::maxHeight
	int32_t ___maxHeight_7;
	// Utage.LetterBoxCamera/AnchorType Utage.LetterBoxCamera::anchor
	int32_t ___anchor_8;
	// Utage.LetterBoxCameraEvent Utage.LetterBoxCamera::OnGameScreenSizeChange
	LetterBoxCameraEvent_t3581932304 * ___OnGameScreenSizeChange_9;
	// System.Single Utage.LetterBoxCamera::screenAspectRatio
	float ___screenAspectRatio_10;
	// UnityEngine.Vector2 Utage.LetterBoxCamera::padding
	Vector2_t2243707579  ___padding_11;
	// UnityEngine.Vector2 Utage.LetterBoxCamera::currentSize
	Vector2_t2243707579  ___currentSize_12;
	// System.Single Utage.LetterBoxCamera::zoom2D
	float ___zoom2D_13;
	// UnityEngine.Vector2 Utage.LetterBoxCamera::zoom2DCenter
	Vector2_t2243707579  ___zoom2DCenter_14;
	// UnityEngine.Camera Utage.LetterBoxCamera::cachedCamera
	Camera_t189460977 * ___cachedCamera_15;
	// System.Boolean Utage.LetterBoxCamera::hasChanged
	bool ___hasChanged_16;

public:
	inline static int32_t get_offset_of_pixelsToUnits_2() { return static_cast<int32_t>(offsetof(LetterBoxCamera_t3507617684, ___pixelsToUnits_2)); }
	inline int32_t get_pixelsToUnits_2() const { return ___pixelsToUnits_2; }
	inline int32_t* get_address_of_pixelsToUnits_2() { return &___pixelsToUnits_2; }
	inline void set_pixelsToUnits_2(int32_t value)
	{
		___pixelsToUnits_2 = value;
	}

	inline static int32_t get_offset_of_width_3() { return static_cast<int32_t>(offsetof(LetterBoxCamera_t3507617684, ___width_3)); }
	inline int32_t get_width_3() const { return ___width_3; }
	inline int32_t* get_address_of_width_3() { return &___width_3; }
	inline void set_width_3(int32_t value)
	{
		___width_3 = value;
	}

	inline static int32_t get_offset_of_height_4() { return static_cast<int32_t>(offsetof(LetterBoxCamera_t3507617684, ___height_4)); }
	inline int32_t get_height_4() const { return ___height_4; }
	inline int32_t* get_address_of_height_4() { return &___height_4; }
	inline void set_height_4(int32_t value)
	{
		___height_4 = value;
	}

	inline static int32_t get_offset_of_isFlexible_5() { return static_cast<int32_t>(offsetof(LetterBoxCamera_t3507617684, ___isFlexible_5)); }
	inline bool get_isFlexible_5() const { return ___isFlexible_5; }
	inline bool* get_address_of_isFlexible_5() { return &___isFlexible_5; }
	inline void set_isFlexible_5(bool value)
	{
		___isFlexible_5 = value;
	}

	inline static int32_t get_offset_of_maxWidth_6() { return static_cast<int32_t>(offsetof(LetterBoxCamera_t3507617684, ___maxWidth_6)); }
	inline int32_t get_maxWidth_6() const { return ___maxWidth_6; }
	inline int32_t* get_address_of_maxWidth_6() { return &___maxWidth_6; }
	inline void set_maxWidth_6(int32_t value)
	{
		___maxWidth_6 = value;
	}

	inline static int32_t get_offset_of_maxHeight_7() { return static_cast<int32_t>(offsetof(LetterBoxCamera_t3507617684, ___maxHeight_7)); }
	inline int32_t get_maxHeight_7() const { return ___maxHeight_7; }
	inline int32_t* get_address_of_maxHeight_7() { return &___maxHeight_7; }
	inline void set_maxHeight_7(int32_t value)
	{
		___maxHeight_7 = value;
	}

	inline static int32_t get_offset_of_anchor_8() { return static_cast<int32_t>(offsetof(LetterBoxCamera_t3507617684, ___anchor_8)); }
	inline int32_t get_anchor_8() const { return ___anchor_8; }
	inline int32_t* get_address_of_anchor_8() { return &___anchor_8; }
	inline void set_anchor_8(int32_t value)
	{
		___anchor_8 = value;
	}

	inline static int32_t get_offset_of_OnGameScreenSizeChange_9() { return static_cast<int32_t>(offsetof(LetterBoxCamera_t3507617684, ___OnGameScreenSizeChange_9)); }
	inline LetterBoxCameraEvent_t3581932304 * get_OnGameScreenSizeChange_9() const { return ___OnGameScreenSizeChange_9; }
	inline LetterBoxCameraEvent_t3581932304 ** get_address_of_OnGameScreenSizeChange_9() { return &___OnGameScreenSizeChange_9; }
	inline void set_OnGameScreenSizeChange_9(LetterBoxCameraEvent_t3581932304 * value)
	{
		___OnGameScreenSizeChange_9 = value;
		Il2CppCodeGenWriteBarrier((&___OnGameScreenSizeChange_9), value);
	}

	inline static int32_t get_offset_of_screenAspectRatio_10() { return static_cast<int32_t>(offsetof(LetterBoxCamera_t3507617684, ___screenAspectRatio_10)); }
	inline float get_screenAspectRatio_10() const { return ___screenAspectRatio_10; }
	inline float* get_address_of_screenAspectRatio_10() { return &___screenAspectRatio_10; }
	inline void set_screenAspectRatio_10(float value)
	{
		___screenAspectRatio_10 = value;
	}

	inline static int32_t get_offset_of_padding_11() { return static_cast<int32_t>(offsetof(LetterBoxCamera_t3507617684, ___padding_11)); }
	inline Vector2_t2243707579  get_padding_11() const { return ___padding_11; }
	inline Vector2_t2243707579 * get_address_of_padding_11() { return &___padding_11; }
	inline void set_padding_11(Vector2_t2243707579  value)
	{
		___padding_11 = value;
	}

	inline static int32_t get_offset_of_currentSize_12() { return static_cast<int32_t>(offsetof(LetterBoxCamera_t3507617684, ___currentSize_12)); }
	inline Vector2_t2243707579  get_currentSize_12() const { return ___currentSize_12; }
	inline Vector2_t2243707579 * get_address_of_currentSize_12() { return &___currentSize_12; }
	inline void set_currentSize_12(Vector2_t2243707579  value)
	{
		___currentSize_12 = value;
	}

	inline static int32_t get_offset_of_zoom2D_13() { return static_cast<int32_t>(offsetof(LetterBoxCamera_t3507617684, ___zoom2D_13)); }
	inline float get_zoom2D_13() const { return ___zoom2D_13; }
	inline float* get_address_of_zoom2D_13() { return &___zoom2D_13; }
	inline void set_zoom2D_13(float value)
	{
		___zoom2D_13 = value;
	}

	inline static int32_t get_offset_of_zoom2DCenter_14() { return static_cast<int32_t>(offsetof(LetterBoxCamera_t3507617684, ___zoom2DCenter_14)); }
	inline Vector2_t2243707579  get_zoom2DCenter_14() const { return ___zoom2DCenter_14; }
	inline Vector2_t2243707579 * get_address_of_zoom2DCenter_14() { return &___zoom2DCenter_14; }
	inline void set_zoom2DCenter_14(Vector2_t2243707579  value)
	{
		___zoom2DCenter_14 = value;
	}

	inline static int32_t get_offset_of_cachedCamera_15() { return static_cast<int32_t>(offsetof(LetterBoxCamera_t3507617684, ___cachedCamera_15)); }
	inline Camera_t189460977 * get_cachedCamera_15() const { return ___cachedCamera_15; }
	inline Camera_t189460977 ** get_address_of_cachedCamera_15() { return &___cachedCamera_15; }
	inline void set_cachedCamera_15(Camera_t189460977 * value)
	{
		___cachedCamera_15 = value;
		Il2CppCodeGenWriteBarrier((&___cachedCamera_15), value);
	}

	inline static int32_t get_offset_of_hasChanged_16() { return static_cast<int32_t>(offsetof(LetterBoxCamera_t3507617684, ___hasChanged_16)); }
	inline bool get_hasChanged_16() const { return ___hasChanged_16; }
	inline bool* get_address_of_hasChanged_16() { return &___hasChanged_16; }
	inline void set_hasChanged_16(bool value)
	{
		___hasChanged_16 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LETTERBOXCAMERA_T3507617684_H
#ifndef CAMERAROOT_T3848343401_H
#define CAMERAROOT_T3848343401_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.CameraRoot
struct  CameraRoot_t3848343401  : public MonoBehaviour_t1158329972
{
public:
	// Utage.LetterBoxCamera Utage.CameraRoot::letterBoxCamera
	LetterBoxCamera_t3507617684 * ___letterBoxCamera_2;
	// UnityEngine.Vector3 Utage.CameraRoot::startPosition
	Vector3_t2243707580  ___startPosition_3;
	// UnityEngine.Vector3 Utage.CameraRoot::startScale
	Vector3_t2243707580  ___startScale_4;
	// UnityEngine.Vector3 Utage.CameraRoot::startEulerAngles
	Vector3_t2243707580  ___startEulerAngles_5;

public:
	inline static int32_t get_offset_of_letterBoxCamera_2() { return static_cast<int32_t>(offsetof(CameraRoot_t3848343401, ___letterBoxCamera_2)); }
	inline LetterBoxCamera_t3507617684 * get_letterBoxCamera_2() const { return ___letterBoxCamera_2; }
	inline LetterBoxCamera_t3507617684 ** get_address_of_letterBoxCamera_2() { return &___letterBoxCamera_2; }
	inline void set_letterBoxCamera_2(LetterBoxCamera_t3507617684 * value)
	{
		___letterBoxCamera_2 = value;
		Il2CppCodeGenWriteBarrier((&___letterBoxCamera_2), value);
	}

	inline static int32_t get_offset_of_startPosition_3() { return static_cast<int32_t>(offsetof(CameraRoot_t3848343401, ___startPosition_3)); }
	inline Vector3_t2243707580  get_startPosition_3() const { return ___startPosition_3; }
	inline Vector3_t2243707580 * get_address_of_startPosition_3() { return &___startPosition_3; }
	inline void set_startPosition_3(Vector3_t2243707580  value)
	{
		___startPosition_3 = value;
	}

	inline static int32_t get_offset_of_startScale_4() { return static_cast<int32_t>(offsetof(CameraRoot_t3848343401, ___startScale_4)); }
	inline Vector3_t2243707580  get_startScale_4() const { return ___startScale_4; }
	inline Vector3_t2243707580 * get_address_of_startScale_4() { return &___startScale_4; }
	inline void set_startScale_4(Vector3_t2243707580  value)
	{
		___startScale_4 = value;
	}

	inline static int32_t get_offset_of_startEulerAngles_5() { return static_cast<int32_t>(offsetof(CameraRoot_t3848343401, ___startEulerAngles_5)); }
	inline Vector3_t2243707580  get_startEulerAngles_5() const { return ___startEulerAngles_5; }
	inline Vector3_t2243707580 * get_address_of_startEulerAngles_5() { return &___startEulerAngles_5; }
	inline void set_startEulerAngles_5(Vector3_t2243707580  value)
	{
		___startEulerAngles_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CAMERAROOT_T3848343401_H
#ifndef CAMERAMANAGER_T586526220_H
#define CAMERAMANAGER_T586526220_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.CameraManager
struct  CameraManager_t586526220  : public MonoBehaviour_t1158329972
{
public:
	// System.Collections.Generic.List`1<Utage.CameraRoot> Utage.CameraManager::cameraList
	List_1_t3217464533 * ___cameraList_2;

public:
	inline static int32_t get_offset_of_cameraList_2() { return static_cast<int32_t>(offsetof(CameraManager_t586526220, ___cameraList_2)); }
	inline List_1_t3217464533 * get_cameraList_2() const { return ___cameraList_2; }
	inline List_1_t3217464533 ** get_address_of_cameraList_2() { return &___cameraList_2; }
	inline void set_cameraList_2(List_1_t3217464533 * value)
	{
		___cameraList_2 = value;
		Il2CppCodeGenWriteBarrier((&___cameraList_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CAMERAMANAGER_T586526220_H
#ifndef ADVSCENARIOTHREAD_T1270526825_H
#define ADVSCENARIOTHREAD_T1270526825_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.AdvScenarioThread
struct  AdvScenarioThread_t1270526825  : public MonoBehaviour_t1158329972
{
public:
	// System.String Utage.AdvScenarioThread::threadName
	String_t* ___threadName_2;
	// System.Boolean Utage.AdvScenarioThread::<IsMainThread>k__BackingField
	bool ___U3CIsMainThreadU3Ek__BackingField_3;
	// System.Boolean Utage.AdvScenarioThread::<IsLoading>k__BackingField
	bool ___U3CIsLoadingU3Ek__BackingField_4;
	// System.Boolean Utage.AdvScenarioThread::<IsPlaying>k__BackingField
	bool ___U3CIsPlayingU3Ek__BackingField_5;
	// Utage.AdvIfManager Utage.AdvScenarioThread::ifManager
	AdvIfManager_t1468978853 * ___ifManager_6;
	// Utage.AdvJumpManager Utage.AdvScenarioThread::jumpManager
	AdvJumpManager_t2493327610 * ___jumpManager_7;
	// Utage.AdvWaitManager Utage.AdvScenarioThread::waitManager
	AdvWaitManager_t2981197357 * ___waitManager_8;
	// Utage.AdvScenarioThread Utage.AdvScenarioThread::<ParenetThread>k__BackingField
	AdvScenarioThread_t1270526825 * ___U3CParenetThreadU3Ek__BackingField_9;
	// System.Collections.Generic.List`1<Utage.AdvScenarioThread> Utage.AdvScenarioThread::subThreadList
	List_1_t639647957 * ___subThreadList_10;
	// System.Collections.Generic.HashSet`1<Utage.AssetFile> Utage.AdvScenarioThread::preloadFileSet
	HashSet_1_t1716474110 * ___preloadFileSet_11;
	// Utage.AdvCommand Utage.AdvScenarioThread::currentCommand
	AdvCommand_t2859960984 * ___currentCommand_12;
	// System.Boolean Utage.AdvScenarioThread::<SkipPageHeaerOnSave>k__BackingField
	bool ___U3CSkipPageHeaerOnSaveU3Ek__BackingField_13;
	// Utage.AdvScenarioPlayer Utage.AdvScenarioThread::<ScenarioPlayer>k__BackingField
	AdvScenarioPlayer_t1295831480 * ___U3CScenarioPlayerU3Ek__BackingField_14;

public:
	inline static int32_t get_offset_of_threadName_2() { return static_cast<int32_t>(offsetof(AdvScenarioThread_t1270526825, ___threadName_2)); }
	inline String_t* get_threadName_2() const { return ___threadName_2; }
	inline String_t** get_address_of_threadName_2() { return &___threadName_2; }
	inline void set_threadName_2(String_t* value)
	{
		___threadName_2 = value;
		Il2CppCodeGenWriteBarrier((&___threadName_2), value);
	}

	inline static int32_t get_offset_of_U3CIsMainThreadU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(AdvScenarioThread_t1270526825, ___U3CIsMainThreadU3Ek__BackingField_3)); }
	inline bool get_U3CIsMainThreadU3Ek__BackingField_3() const { return ___U3CIsMainThreadU3Ek__BackingField_3; }
	inline bool* get_address_of_U3CIsMainThreadU3Ek__BackingField_3() { return &___U3CIsMainThreadU3Ek__BackingField_3; }
	inline void set_U3CIsMainThreadU3Ek__BackingField_3(bool value)
	{
		___U3CIsMainThreadU3Ek__BackingField_3 = value;
	}

	inline static int32_t get_offset_of_U3CIsLoadingU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(AdvScenarioThread_t1270526825, ___U3CIsLoadingU3Ek__BackingField_4)); }
	inline bool get_U3CIsLoadingU3Ek__BackingField_4() const { return ___U3CIsLoadingU3Ek__BackingField_4; }
	inline bool* get_address_of_U3CIsLoadingU3Ek__BackingField_4() { return &___U3CIsLoadingU3Ek__BackingField_4; }
	inline void set_U3CIsLoadingU3Ek__BackingField_4(bool value)
	{
		___U3CIsLoadingU3Ek__BackingField_4 = value;
	}

	inline static int32_t get_offset_of_U3CIsPlayingU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(AdvScenarioThread_t1270526825, ___U3CIsPlayingU3Ek__BackingField_5)); }
	inline bool get_U3CIsPlayingU3Ek__BackingField_5() const { return ___U3CIsPlayingU3Ek__BackingField_5; }
	inline bool* get_address_of_U3CIsPlayingU3Ek__BackingField_5() { return &___U3CIsPlayingU3Ek__BackingField_5; }
	inline void set_U3CIsPlayingU3Ek__BackingField_5(bool value)
	{
		___U3CIsPlayingU3Ek__BackingField_5 = value;
	}

	inline static int32_t get_offset_of_ifManager_6() { return static_cast<int32_t>(offsetof(AdvScenarioThread_t1270526825, ___ifManager_6)); }
	inline AdvIfManager_t1468978853 * get_ifManager_6() const { return ___ifManager_6; }
	inline AdvIfManager_t1468978853 ** get_address_of_ifManager_6() { return &___ifManager_6; }
	inline void set_ifManager_6(AdvIfManager_t1468978853 * value)
	{
		___ifManager_6 = value;
		Il2CppCodeGenWriteBarrier((&___ifManager_6), value);
	}

	inline static int32_t get_offset_of_jumpManager_7() { return static_cast<int32_t>(offsetof(AdvScenarioThread_t1270526825, ___jumpManager_7)); }
	inline AdvJumpManager_t2493327610 * get_jumpManager_7() const { return ___jumpManager_7; }
	inline AdvJumpManager_t2493327610 ** get_address_of_jumpManager_7() { return &___jumpManager_7; }
	inline void set_jumpManager_7(AdvJumpManager_t2493327610 * value)
	{
		___jumpManager_7 = value;
		Il2CppCodeGenWriteBarrier((&___jumpManager_7), value);
	}

	inline static int32_t get_offset_of_waitManager_8() { return static_cast<int32_t>(offsetof(AdvScenarioThread_t1270526825, ___waitManager_8)); }
	inline AdvWaitManager_t2981197357 * get_waitManager_8() const { return ___waitManager_8; }
	inline AdvWaitManager_t2981197357 ** get_address_of_waitManager_8() { return &___waitManager_8; }
	inline void set_waitManager_8(AdvWaitManager_t2981197357 * value)
	{
		___waitManager_8 = value;
		Il2CppCodeGenWriteBarrier((&___waitManager_8), value);
	}

	inline static int32_t get_offset_of_U3CParenetThreadU3Ek__BackingField_9() { return static_cast<int32_t>(offsetof(AdvScenarioThread_t1270526825, ___U3CParenetThreadU3Ek__BackingField_9)); }
	inline AdvScenarioThread_t1270526825 * get_U3CParenetThreadU3Ek__BackingField_9() const { return ___U3CParenetThreadU3Ek__BackingField_9; }
	inline AdvScenarioThread_t1270526825 ** get_address_of_U3CParenetThreadU3Ek__BackingField_9() { return &___U3CParenetThreadU3Ek__BackingField_9; }
	inline void set_U3CParenetThreadU3Ek__BackingField_9(AdvScenarioThread_t1270526825 * value)
	{
		___U3CParenetThreadU3Ek__BackingField_9 = value;
		Il2CppCodeGenWriteBarrier((&___U3CParenetThreadU3Ek__BackingField_9), value);
	}

	inline static int32_t get_offset_of_subThreadList_10() { return static_cast<int32_t>(offsetof(AdvScenarioThread_t1270526825, ___subThreadList_10)); }
	inline List_1_t639647957 * get_subThreadList_10() const { return ___subThreadList_10; }
	inline List_1_t639647957 ** get_address_of_subThreadList_10() { return &___subThreadList_10; }
	inline void set_subThreadList_10(List_1_t639647957 * value)
	{
		___subThreadList_10 = value;
		Il2CppCodeGenWriteBarrier((&___subThreadList_10), value);
	}

	inline static int32_t get_offset_of_preloadFileSet_11() { return static_cast<int32_t>(offsetof(AdvScenarioThread_t1270526825, ___preloadFileSet_11)); }
	inline HashSet_1_t1716474110 * get_preloadFileSet_11() const { return ___preloadFileSet_11; }
	inline HashSet_1_t1716474110 ** get_address_of_preloadFileSet_11() { return &___preloadFileSet_11; }
	inline void set_preloadFileSet_11(HashSet_1_t1716474110 * value)
	{
		___preloadFileSet_11 = value;
		Il2CppCodeGenWriteBarrier((&___preloadFileSet_11), value);
	}

	inline static int32_t get_offset_of_currentCommand_12() { return static_cast<int32_t>(offsetof(AdvScenarioThread_t1270526825, ___currentCommand_12)); }
	inline AdvCommand_t2859960984 * get_currentCommand_12() const { return ___currentCommand_12; }
	inline AdvCommand_t2859960984 ** get_address_of_currentCommand_12() { return &___currentCommand_12; }
	inline void set_currentCommand_12(AdvCommand_t2859960984 * value)
	{
		___currentCommand_12 = value;
		Il2CppCodeGenWriteBarrier((&___currentCommand_12), value);
	}

	inline static int32_t get_offset_of_U3CSkipPageHeaerOnSaveU3Ek__BackingField_13() { return static_cast<int32_t>(offsetof(AdvScenarioThread_t1270526825, ___U3CSkipPageHeaerOnSaveU3Ek__BackingField_13)); }
	inline bool get_U3CSkipPageHeaerOnSaveU3Ek__BackingField_13() const { return ___U3CSkipPageHeaerOnSaveU3Ek__BackingField_13; }
	inline bool* get_address_of_U3CSkipPageHeaerOnSaveU3Ek__BackingField_13() { return &___U3CSkipPageHeaerOnSaveU3Ek__BackingField_13; }
	inline void set_U3CSkipPageHeaerOnSaveU3Ek__BackingField_13(bool value)
	{
		___U3CSkipPageHeaerOnSaveU3Ek__BackingField_13 = value;
	}

	inline static int32_t get_offset_of_U3CScenarioPlayerU3Ek__BackingField_14() { return static_cast<int32_t>(offsetof(AdvScenarioThread_t1270526825, ___U3CScenarioPlayerU3Ek__BackingField_14)); }
	inline AdvScenarioPlayer_t1295831480 * get_U3CScenarioPlayerU3Ek__BackingField_14() const { return ___U3CScenarioPlayerU3Ek__BackingField_14; }
	inline AdvScenarioPlayer_t1295831480 ** get_address_of_U3CScenarioPlayerU3Ek__BackingField_14() { return &___U3CScenarioPlayerU3Ek__BackingField_14; }
	inline void set_U3CScenarioPlayerU3Ek__BackingField_14(AdvScenarioPlayer_t1295831480 * value)
	{
		___U3CScenarioPlayerU3Ek__BackingField_14 = value;
		Il2CppCodeGenWriteBarrier((&___U3CScenarioPlayerU3Ek__BackingField_14), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ADVSCENARIOTHREAD_T1270526825_H
#ifndef ADVGUIMANAGER_T2331361001_H
#define ADVGUIMANAGER_T2331361001_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.AdvGuiManager
struct  AdvGuiManager_t2331361001  : public MonoBehaviour_t1158329972
{
public:
	// System.Collections.Generic.List`1<UnityEngine.GameObject> Utage.AdvGuiManager::guiObjects
	List_1_t1125654279 * ___guiObjects_2;
	// System.Collections.Generic.Dictionary`2<System.String,Utage.AdvGuiBase> Utage.AdvGuiManager::objects
	Dictionary_2_t3938720553 * ___objects_3;

public:
	inline static int32_t get_offset_of_guiObjects_2() { return static_cast<int32_t>(offsetof(AdvGuiManager_t2331361001, ___guiObjects_2)); }
	inline List_1_t1125654279 * get_guiObjects_2() const { return ___guiObjects_2; }
	inline List_1_t1125654279 ** get_address_of_guiObjects_2() { return &___guiObjects_2; }
	inline void set_guiObjects_2(List_1_t1125654279 * value)
	{
		___guiObjects_2 = value;
		Il2CppCodeGenWriteBarrier((&___guiObjects_2), value);
	}

	inline static int32_t get_offset_of_objects_3() { return static_cast<int32_t>(offsetof(AdvGuiManager_t2331361001, ___objects_3)); }
	inline Dictionary_2_t3938720553 * get_objects_3() const { return ___objects_3; }
	inline Dictionary_2_t3938720553 ** get_address_of_objects_3() { return &___objects_3; }
	inline void set_objects_3(Dictionary_2_t3938720553 * value)
	{
		___objects_3 = value;
		Il2CppCodeGenWriteBarrier((&___objects_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ADVGUIMANAGER_T2331361001_H
#ifndef ADVUGUIBACKLOG_T1854927244_H
#define ADVUGUIBACKLOG_T1854927244_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.AdvUguiBacklog
struct  AdvUguiBacklog_t1854927244  : public MonoBehaviour_t1158329972
{
public:
	// Utage.UguiNovelText Utage.AdvUguiBacklog::text
	UguiNovelText_t4135744055 * ___text_2;
	// UnityEngine.UI.Text Utage.AdvUguiBacklog::characterName
	Text_t356221433 * ___characterName_3;
	// UnityEngine.GameObject Utage.AdvUguiBacklog::soundIcon
	GameObject_t1756533147 * ___soundIcon_4;
	// UnityEngine.UI.Button Utage.AdvUguiBacklog::button
	Button_t2872111280 * ___button_5;
	// System.Boolean Utage.AdvUguiBacklog::isMultiTextInPage
	bool ___isMultiTextInPage_6;
	// Utage.AdvBacklog Utage.AdvUguiBacklog::data
	AdvBacklog_t3927301290 * ___data_7;

public:
	inline static int32_t get_offset_of_text_2() { return static_cast<int32_t>(offsetof(AdvUguiBacklog_t1854927244, ___text_2)); }
	inline UguiNovelText_t4135744055 * get_text_2() const { return ___text_2; }
	inline UguiNovelText_t4135744055 ** get_address_of_text_2() { return &___text_2; }
	inline void set_text_2(UguiNovelText_t4135744055 * value)
	{
		___text_2 = value;
		Il2CppCodeGenWriteBarrier((&___text_2), value);
	}

	inline static int32_t get_offset_of_characterName_3() { return static_cast<int32_t>(offsetof(AdvUguiBacklog_t1854927244, ___characterName_3)); }
	inline Text_t356221433 * get_characterName_3() const { return ___characterName_3; }
	inline Text_t356221433 ** get_address_of_characterName_3() { return &___characterName_3; }
	inline void set_characterName_3(Text_t356221433 * value)
	{
		___characterName_3 = value;
		Il2CppCodeGenWriteBarrier((&___characterName_3), value);
	}

	inline static int32_t get_offset_of_soundIcon_4() { return static_cast<int32_t>(offsetof(AdvUguiBacklog_t1854927244, ___soundIcon_4)); }
	inline GameObject_t1756533147 * get_soundIcon_4() const { return ___soundIcon_4; }
	inline GameObject_t1756533147 ** get_address_of_soundIcon_4() { return &___soundIcon_4; }
	inline void set_soundIcon_4(GameObject_t1756533147 * value)
	{
		___soundIcon_4 = value;
		Il2CppCodeGenWriteBarrier((&___soundIcon_4), value);
	}

	inline static int32_t get_offset_of_button_5() { return static_cast<int32_t>(offsetof(AdvUguiBacklog_t1854927244, ___button_5)); }
	inline Button_t2872111280 * get_button_5() const { return ___button_5; }
	inline Button_t2872111280 ** get_address_of_button_5() { return &___button_5; }
	inline void set_button_5(Button_t2872111280 * value)
	{
		___button_5 = value;
		Il2CppCodeGenWriteBarrier((&___button_5), value);
	}

	inline static int32_t get_offset_of_isMultiTextInPage_6() { return static_cast<int32_t>(offsetof(AdvUguiBacklog_t1854927244, ___isMultiTextInPage_6)); }
	inline bool get_isMultiTextInPage_6() const { return ___isMultiTextInPage_6; }
	inline bool* get_address_of_isMultiTextInPage_6() { return &___isMultiTextInPage_6; }
	inline void set_isMultiTextInPage_6(bool value)
	{
		___isMultiTextInPage_6 = value;
	}

	inline static int32_t get_offset_of_data_7() { return static_cast<int32_t>(offsetof(AdvUguiBacklog_t1854927244, ___data_7)); }
	inline AdvBacklog_t3927301290 * get_data_7() const { return ___data_7; }
	inline AdvBacklog_t3927301290 ** get_address_of_data_7() { return &___data_7; }
	inline void set_data_7(AdvBacklog_t3927301290 * value)
	{
		___data_7 = value;
		Il2CppCodeGenWriteBarrier((&___data_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ADVUGUIBACKLOG_T1854927244_H
#ifndef ADVUGUISELECTIONMANAGER_T456513248_H
#define ADVUGUISELECTIONMANAGER_T456513248_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.AdvUguiSelectionManager
struct  AdvUguiSelectionManager_t456513248  : public MonoBehaviour_t1158329972
{
public:
	// Utage.AdvEngine Utage.AdvUguiSelectionManager::engine
	AdvEngine_t1176753927 * ___engine_2;
	// Utage.AdvUguiSelectionManager/SelectedColorMode Utage.AdvUguiSelectionManager::selectedColorMode
	int32_t ___selectedColorMode_3;
	// UnityEngine.Color Utage.AdvUguiSelectionManager::selectedColor
	Color_t2020392075  ___selectedColor_4;
	// System.Collections.Generic.List`1<UnityEngine.GameObject> Utage.AdvUguiSelectionManager::prefabList
	List_1_t1125654279 * ___prefabList_5;
	// Utage.UguiListView Utage.AdvUguiSelectionManager::listView
	UguiListView_t169672791 * ___listView_6;
	// UnityEngine.CanvasGroup Utage.AdvUguiSelectionManager::canvasGroup
	CanvasGroup_t3296560743 * ___canvasGroup_7;
	// System.Collections.Generic.List`1<UnityEngine.GameObject> Utage.AdvUguiSelectionManager::items
	List_1_t1125654279 * ___items_8;

public:
	inline static int32_t get_offset_of_engine_2() { return static_cast<int32_t>(offsetof(AdvUguiSelectionManager_t456513248, ___engine_2)); }
	inline AdvEngine_t1176753927 * get_engine_2() const { return ___engine_2; }
	inline AdvEngine_t1176753927 ** get_address_of_engine_2() { return &___engine_2; }
	inline void set_engine_2(AdvEngine_t1176753927 * value)
	{
		___engine_2 = value;
		Il2CppCodeGenWriteBarrier((&___engine_2), value);
	}

	inline static int32_t get_offset_of_selectedColorMode_3() { return static_cast<int32_t>(offsetof(AdvUguiSelectionManager_t456513248, ___selectedColorMode_3)); }
	inline int32_t get_selectedColorMode_3() const { return ___selectedColorMode_3; }
	inline int32_t* get_address_of_selectedColorMode_3() { return &___selectedColorMode_3; }
	inline void set_selectedColorMode_3(int32_t value)
	{
		___selectedColorMode_3 = value;
	}

	inline static int32_t get_offset_of_selectedColor_4() { return static_cast<int32_t>(offsetof(AdvUguiSelectionManager_t456513248, ___selectedColor_4)); }
	inline Color_t2020392075  get_selectedColor_4() const { return ___selectedColor_4; }
	inline Color_t2020392075 * get_address_of_selectedColor_4() { return &___selectedColor_4; }
	inline void set_selectedColor_4(Color_t2020392075  value)
	{
		___selectedColor_4 = value;
	}

	inline static int32_t get_offset_of_prefabList_5() { return static_cast<int32_t>(offsetof(AdvUguiSelectionManager_t456513248, ___prefabList_5)); }
	inline List_1_t1125654279 * get_prefabList_5() const { return ___prefabList_5; }
	inline List_1_t1125654279 ** get_address_of_prefabList_5() { return &___prefabList_5; }
	inline void set_prefabList_5(List_1_t1125654279 * value)
	{
		___prefabList_5 = value;
		Il2CppCodeGenWriteBarrier((&___prefabList_5), value);
	}

	inline static int32_t get_offset_of_listView_6() { return static_cast<int32_t>(offsetof(AdvUguiSelectionManager_t456513248, ___listView_6)); }
	inline UguiListView_t169672791 * get_listView_6() const { return ___listView_6; }
	inline UguiListView_t169672791 ** get_address_of_listView_6() { return &___listView_6; }
	inline void set_listView_6(UguiListView_t169672791 * value)
	{
		___listView_6 = value;
		Il2CppCodeGenWriteBarrier((&___listView_6), value);
	}

	inline static int32_t get_offset_of_canvasGroup_7() { return static_cast<int32_t>(offsetof(AdvUguiSelectionManager_t456513248, ___canvasGroup_7)); }
	inline CanvasGroup_t3296560743 * get_canvasGroup_7() const { return ___canvasGroup_7; }
	inline CanvasGroup_t3296560743 ** get_address_of_canvasGroup_7() { return &___canvasGroup_7; }
	inline void set_canvasGroup_7(CanvasGroup_t3296560743 * value)
	{
		___canvasGroup_7 = value;
		Il2CppCodeGenWriteBarrier((&___canvasGroup_7), value);
	}

	inline static int32_t get_offset_of_items_8() { return static_cast<int32_t>(offsetof(AdvUguiSelectionManager_t456513248, ___items_8)); }
	inline List_1_t1125654279 * get_items_8() const { return ___items_8; }
	inline List_1_t1125654279 ** get_address_of_items_8() { return &___items_8; }
	inline void set_items_8(List_1_t1125654279 * value)
	{
		___items_8 = value;
		Il2CppCodeGenWriteBarrier((&___items_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ADVUGUISELECTIONMANAGER_T456513248_H
#ifndef ADVSCENARIOPLAYER_T1295831480_H
#define ADVSCENARIOPLAYER_T1295831480_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.AdvScenarioPlayer
struct  AdvScenarioPlayer_t1295831480  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.GameObject Utage.AdvScenarioPlayer::sendMessageTarget
	GameObject_t1756533147 * ___sendMessageTarget_2;
	// Utage.AdvScenarioPlayer/DebugOutPut Utage.AdvScenarioPlayer::debugOutPut
	int32_t ___debugOutPut_3;
	// System.Int32 Utage.AdvScenarioPlayer::maxFilePreload
	int32_t ___maxFilePreload_4;
	// System.Int32 Utage.AdvScenarioPlayer::preloadDeep
	int32_t ___preloadDeep_5;
	// Utage.AdvScenarioPlayerEvent Utage.AdvScenarioPlayer::onEndScenario
	AdvScenarioPlayerEvent_t1780583340 * ___onEndScenario_6;
	// Utage.AdvScenarioPlayerEvent Utage.AdvScenarioPlayer::onPauseScenario
	AdvScenarioPlayerEvent_t1780583340 * ___onPauseScenario_7;
	// Utage.AdvScenarioPlayerEvent Utage.AdvScenarioPlayer::onEndOrPauseScenario
	AdvScenarioPlayerEvent_t1780583340 * ___onEndOrPauseScenario_8;
	// Utage.AdvCommandEvent Utage.AdvScenarioPlayer::onBeginCommand
	AdvCommandEvent_t2718201788 * ___onBeginCommand_9;
	// Utage.AdvCommandEvent Utage.AdvScenarioPlayer::onUpdatePreWaitingCommand
	AdvCommandEvent_t2718201788 * ___onUpdatePreWaitingCommand_10;
	// Utage.AdvCommandEvent Utage.AdvScenarioPlayer::onUpdateWaitingCommand
	AdvCommandEvent_t2718201788 * ___onUpdateWaitingCommand_11;
	// Utage.AdvCommandEvent Utage.AdvScenarioPlayer::onEndCommand
	AdvCommandEvent_t2718201788 * ___onEndCommand_12;
	// Utage.AdvEngine Utage.AdvScenarioPlayer::engine
	AdvEngine_t1176753927 * ___engine_13;
	// Utage.AdvScenarioThread Utage.AdvScenarioPlayer::mainThread
	AdvScenarioThread_t1270526825 * ___mainThread_14;
	// System.Boolean Utage.AdvScenarioPlayer::<IsEndScenario>k__BackingField
	bool ___U3CIsEndScenarioU3Ek__BackingField_15;
	// System.Boolean Utage.AdvScenarioPlayer::<IsReservedEndScenario>k__BackingField
	bool ___U3CIsReservedEndScenarioU3Ek__BackingField_16;
	// System.Boolean Utage.AdvScenarioPlayer::<IsPausing>k__BackingField
	bool ___U3CIsPausingU3Ek__BackingField_17;
	// System.String Utage.AdvScenarioPlayer::<CurrentGallerySceneLabel>k__BackingField
	String_t* ___U3CCurrentGallerySceneLabelU3Ek__BackingField_18;

public:
	inline static int32_t get_offset_of_sendMessageTarget_2() { return static_cast<int32_t>(offsetof(AdvScenarioPlayer_t1295831480, ___sendMessageTarget_2)); }
	inline GameObject_t1756533147 * get_sendMessageTarget_2() const { return ___sendMessageTarget_2; }
	inline GameObject_t1756533147 ** get_address_of_sendMessageTarget_2() { return &___sendMessageTarget_2; }
	inline void set_sendMessageTarget_2(GameObject_t1756533147 * value)
	{
		___sendMessageTarget_2 = value;
		Il2CppCodeGenWriteBarrier((&___sendMessageTarget_2), value);
	}

	inline static int32_t get_offset_of_debugOutPut_3() { return static_cast<int32_t>(offsetof(AdvScenarioPlayer_t1295831480, ___debugOutPut_3)); }
	inline int32_t get_debugOutPut_3() const { return ___debugOutPut_3; }
	inline int32_t* get_address_of_debugOutPut_3() { return &___debugOutPut_3; }
	inline void set_debugOutPut_3(int32_t value)
	{
		___debugOutPut_3 = value;
	}

	inline static int32_t get_offset_of_maxFilePreload_4() { return static_cast<int32_t>(offsetof(AdvScenarioPlayer_t1295831480, ___maxFilePreload_4)); }
	inline int32_t get_maxFilePreload_4() const { return ___maxFilePreload_4; }
	inline int32_t* get_address_of_maxFilePreload_4() { return &___maxFilePreload_4; }
	inline void set_maxFilePreload_4(int32_t value)
	{
		___maxFilePreload_4 = value;
	}

	inline static int32_t get_offset_of_preloadDeep_5() { return static_cast<int32_t>(offsetof(AdvScenarioPlayer_t1295831480, ___preloadDeep_5)); }
	inline int32_t get_preloadDeep_5() const { return ___preloadDeep_5; }
	inline int32_t* get_address_of_preloadDeep_5() { return &___preloadDeep_5; }
	inline void set_preloadDeep_5(int32_t value)
	{
		___preloadDeep_5 = value;
	}

	inline static int32_t get_offset_of_onEndScenario_6() { return static_cast<int32_t>(offsetof(AdvScenarioPlayer_t1295831480, ___onEndScenario_6)); }
	inline AdvScenarioPlayerEvent_t1780583340 * get_onEndScenario_6() const { return ___onEndScenario_6; }
	inline AdvScenarioPlayerEvent_t1780583340 ** get_address_of_onEndScenario_6() { return &___onEndScenario_6; }
	inline void set_onEndScenario_6(AdvScenarioPlayerEvent_t1780583340 * value)
	{
		___onEndScenario_6 = value;
		Il2CppCodeGenWriteBarrier((&___onEndScenario_6), value);
	}

	inline static int32_t get_offset_of_onPauseScenario_7() { return static_cast<int32_t>(offsetof(AdvScenarioPlayer_t1295831480, ___onPauseScenario_7)); }
	inline AdvScenarioPlayerEvent_t1780583340 * get_onPauseScenario_7() const { return ___onPauseScenario_7; }
	inline AdvScenarioPlayerEvent_t1780583340 ** get_address_of_onPauseScenario_7() { return &___onPauseScenario_7; }
	inline void set_onPauseScenario_7(AdvScenarioPlayerEvent_t1780583340 * value)
	{
		___onPauseScenario_7 = value;
		Il2CppCodeGenWriteBarrier((&___onPauseScenario_7), value);
	}

	inline static int32_t get_offset_of_onEndOrPauseScenario_8() { return static_cast<int32_t>(offsetof(AdvScenarioPlayer_t1295831480, ___onEndOrPauseScenario_8)); }
	inline AdvScenarioPlayerEvent_t1780583340 * get_onEndOrPauseScenario_8() const { return ___onEndOrPauseScenario_8; }
	inline AdvScenarioPlayerEvent_t1780583340 ** get_address_of_onEndOrPauseScenario_8() { return &___onEndOrPauseScenario_8; }
	inline void set_onEndOrPauseScenario_8(AdvScenarioPlayerEvent_t1780583340 * value)
	{
		___onEndOrPauseScenario_8 = value;
		Il2CppCodeGenWriteBarrier((&___onEndOrPauseScenario_8), value);
	}

	inline static int32_t get_offset_of_onBeginCommand_9() { return static_cast<int32_t>(offsetof(AdvScenarioPlayer_t1295831480, ___onBeginCommand_9)); }
	inline AdvCommandEvent_t2718201788 * get_onBeginCommand_9() const { return ___onBeginCommand_9; }
	inline AdvCommandEvent_t2718201788 ** get_address_of_onBeginCommand_9() { return &___onBeginCommand_9; }
	inline void set_onBeginCommand_9(AdvCommandEvent_t2718201788 * value)
	{
		___onBeginCommand_9 = value;
		Il2CppCodeGenWriteBarrier((&___onBeginCommand_9), value);
	}

	inline static int32_t get_offset_of_onUpdatePreWaitingCommand_10() { return static_cast<int32_t>(offsetof(AdvScenarioPlayer_t1295831480, ___onUpdatePreWaitingCommand_10)); }
	inline AdvCommandEvent_t2718201788 * get_onUpdatePreWaitingCommand_10() const { return ___onUpdatePreWaitingCommand_10; }
	inline AdvCommandEvent_t2718201788 ** get_address_of_onUpdatePreWaitingCommand_10() { return &___onUpdatePreWaitingCommand_10; }
	inline void set_onUpdatePreWaitingCommand_10(AdvCommandEvent_t2718201788 * value)
	{
		___onUpdatePreWaitingCommand_10 = value;
		Il2CppCodeGenWriteBarrier((&___onUpdatePreWaitingCommand_10), value);
	}

	inline static int32_t get_offset_of_onUpdateWaitingCommand_11() { return static_cast<int32_t>(offsetof(AdvScenarioPlayer_t1295831480, ___onUpdateWaitingCommand_11)); }
	inline AdvCommandEvent_t2718201788 * get_onUpdateWaitingCommand_11() const { return ___onUpdateWaitingCommand_11; }
	inline AdvCommandEvent_t2718201788 ** get_address_of_onUpdateWaitingCommand_11() { return &___onUpdateWaitingCommand_11; }
	inline void set_onUpdateWaitingCommand_11(AdvCommandEvent_t2718201788 * value)
	{
		___onUpdateWaitingCommand_11 = value;
		Il2CppCodeGenWriteBarrier((&___onUpdateWaitingCommand_11), value);
	}

	inline static int32_t get_offset_of_onEndCommand_12() { return static_cast<int32_t>(offsetof(AdvScenarioPlayer_t1295831480, ___onEndCommand_12)); }
	inline AdvCommandEvent_t2718201788 * get_onEndCommand_12() const { return ___onEndCommand_12; }
	inline AdvCommandEvent_t2718201788 ** get_address_of_onEndCommand_12() { return &___onEndCommand_12; }
	inline void set_onEndCommand_12(AdvCommandEvent_t2718201788 * value)
	{
		___onEndCommand_12 = value;
		Il2CppCodeGenWriteBarrier((&___onEndCommand_12), value);
	}

	inline static int32_t get_offset_of_engine_13() { return static_cast<int32_t>(offsetof(AdvScenarioPlayer_t1295831480, ___engine_13)); }
	inline AdvEngine_t1176753927 * get_engine_13() const { return ___engine_13; }
	inline AdvEngine_t1176753927 ** get_address_of_engine_13() { return &___engine_13; }
	inline void set_engine_13(AdvEngine_t1176753927 * value)
	{
		___engine_13 = value;
		Il2CppCodeGenWriteBarrier((&___engine_13), value);
	}

	inline static int32_t get_offset_of_mainThread_14() { return static_cast<int32_t>(offsetof(AdvScenarioPlayer_t1295831480, ___mainThread_14)); }
	inline AdvScenarioThread_t1270526825 * get_mainThread_14() const { return ___mainThread_14; }
	inline AdvScenarioThread_t1270526825 ** get_address_of_mainThread_14() { return &___mainThread_14; }
	inline void set_mainThread_14(AdvScenarioThread_t1270526825 * value)
	{
		___mainThread_14 = value;
		Il2CppCodeGenWriteBarrier((&___mainThread_14), value);
	}

	inline static int32_t get_offset_of_U3CIsEndScenarioU3Ek__BackingField_15() { return static_cast<int32_t>(offsetof(AdvScenarioPlayer_t1295831480, ___U3CIsEndScenarioU3Ek__BackingField_15)); }
	inline bool get_U3CIsEndScenarioU3Ek__BackingField_15() const { return ___U3CIsEndScenarioU3Ek__BackingField_15; }
	inline bool* get_address_of_U3CIsEndScenarioU3Ek__BackingField_15() { return &___U3CIsEndScenarioU3Ek__BackingField_15; }
	inline void set_U3CIsEndScenarioU3Ek__BackingField_15(bool value)
	{
		___U3CIsEndScenarioU3Ek__BackingField_15 = value;
	}

	inline static int32_t get_offset_of_U3CIsReservedEndScenarioU3Ek__BackingField_16() { return static_cast<int32_t>(offsetof(AdvScenarioPlayer_t1295831480, ___U3CIsReservedEndScenarioU3Ek__BackingField_16)); }
	inline bool get_U3CIsReservedEndScenarioU3Ek__BackingField_16() const { return ___U3CIsReservedEndScenarioU3Ek__BackingField_16; }
	inline bool* get_address_of_U3CIsReservedEndScenarioU3Ek__BackingField_16() { return &___U3CIsReservedEndScenarioU3Ek__BackingField_16; }
	inline void set_U3CIsReservedEndScenarioU3Ek__BackingField_16(bool value)
	{
		___U3CIsReservedEndScenarioU3Ek__BackingField_16 = value;
	}

	inline static int32_t get_offset_of_U3CIsPausingU3Ek__BackingField_17() { return static_cast<int32_t>(offsetof(AdvScenarioPlayer_t1295831480, ___U3CIsPausingU3Ek__BackingField_17)); }
	inline bool get_U3CIsPausingU3Ek__BackingField_17() const { return ___U3CIsPausingU3Ek__BackingField_17; }
	inline bool* get_address_of_U3CIsPausingU3Ek__BackingField_17() { return &___U3CIsPausingU3Ek__BackingField_17; }
	inline void set_U3CIsPausingU3Ek__BackingField_17(bool value)
	{
		___U3CIsPausingU3Ek__BackingField_17 = value;
	}

	inline static int32_t get_offset_of_U3CCurrentGallerySceneLabelU3Ek__BackingField_18() { return static_cast<int32_t>(offsetof(AdvScenarioPlayer_t1295831480, ___U3CCurrentGallerySceneLabelU3Ek__BackingField_18)); }
	inline String_t* get_U3CCurrentGallerySceneLabelU3Ek__BackingField_18() const { return ___U3CCurrentGallerySceneLabelU3Ek__BackingField_18; }
	inline String_t** get_address_of_U3CCurrentGallerySceneLabelU3Ek__BackingField_18() { return &___U3CCurrentGallerySceneLabelU3Ek__BackingField_18; }
	inline void set_U3CCurrentGallerySceneLabelU3Ek__BackingField_18(String_t* value)
	{
		___U3CCurrentGallerySceneLabelU3Ek__BackingField_18 = value;
		Il2CppCodeGenWriteBarrier((&___U3CCurrentGallerySceneLabelU3Ek__BackingField_18), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ADVSCENARIOPLAYER_T1295831480_H
#ifndef ADVUIMANAGER_T4018716000_H
#define ADVUIMANAGER_T4018716000_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.AdvUiManager
struct  AdvUiManager_t4018716000  : public MonoBehaviour_t1158329972
{
public:
	// Utage.AdvEngine Utage.AdvUiManager::engine
	AdvEngine_t1176753927 * ___engine_2;
	// Utage.AdvGuiManager Utage.AdvUiManager::guiManager
	AdvGuiManager_t2331361001 * ___guiManager_3;
	// Utage.AdvUiManager/UiStatus Utage.AdvUiManager::status
	int32_t ___status_4;
	// UnityEngine.EventSystems.PointerEventData Utage.AdvUiManager::<CurrentPointerData>k__BackingField
	PointerEventData_t1599784723 * ___U3CCurrentPointerDataU3Ek__BackingField_5;
	// System.Boolean Utage.AdvUiManager::<IsInputTrig>k__BackingField
	bool ___U3CIsInputTrigU3Ek__BackingField_6;
	// System.Boolean Utage.AdvUiManager::<IsInputTrigCustom>k__BackingField
	bool ___U3CIsInputTrigCustomU3Ek__BackingField_7;
	// System.Boolean Utage.AdvUiManager::<IsShowingMessageWindow>k__BackingField
	bool ___U3CIsShowingMessageWindowU3Ek__BackingField_8;
	// System.Boolean Utage.AdvUiManager::<IsHideMenuButton>k__BackingField
	bool ___U3CIsHideMenuButtonU3Ek__BackingField_9;

public:
	inline static int32_t get_offset_of_engine_2() { return static_cast<int32_t>(offsetof(AdvUiManager_t4018716000, ___engine_2)); }
	inline AdvEngine_t1176753927 * get_engine_2() const { return ___engine_2; }
	inline AdvEngine_t1176753927 ** get_address_of_engine_2() { return &___engine_2; }
	inline void set_engine_2(AdvEngine_t1176753927 * value)
	{
		___engine_2 = value;
		Il2CppCodeGenWriteBarrier((&___engine_2), value);
	}

	inline static int32_t get_offset_of_guiManager_3() { return static_cast<int32_t>(offsetof(AdvUiManager_t4018716000, ___guiManager_3)); }
	inline AdvGuiManager_t2331361001 * get_guiManager_3() const { return ___guiManager_3; }
	inline AdvGuiManager_t2331361001 ** get_address_of_guiManager_3() { return &___guiManager_3; }
	inline void set_guiManager_3(AdvGuiManager_t2331361001 * value)
	{
		___guiManager_3 = value;
		Il2CppCodeGenWriteBarrier((&___guiManager_3), value);
	}

	inline static int32_t get_offset_of_status_4() { return static_cast<int32_t>(offsetof(AdvUiManager_t4018716000, ___status_4)); }
	inline int32_t get_status_4() const { return ___status_4; }
	inline int32_t* get_address_of_status_4() { return &___status_4; }
	inline void set_status_4(int32_t value)
	{
		___status_4 = value;
	}

	inline static int32_t get_offset_of_U3CCurrentPointerDataU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(AdvUiManager_t4018716000, ___U3CCurrentPointerDataU3Ek__BackingField_5)); }
	inline PointerEventData_t1599784723 * get_U3CCurrentPointerDataU3Ek__BackingField_5() const { return ___U3CCurrentPointerDataU3Ek__BackingField_5; }
	inline PointerEventData_t1599784723 ** get_address_of_U3CCurrentPointerDataU3Ek__BackingField_5() { return &___U3CCurrentPointerDataU3Ek__BackingField_5; }
	inline void set_U3CCurrentPointerDataU3Ek__BackingField_5(PointerEventData_t1599784723 * value)
	{
		___U3CCurrentPointerDataU3Ek__BackingField_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3CCurrentPointerDataU3Ek__BackingField_5), value);
	}

	inline static int32_t get_offset_of_U3CIsInputTrigU3Ek__BackingField_6() { return static_cast<int32_t>(offsetof(AdvUiManager_t4018716000, ___U3CIsInputTrigU3Ek__BackingField_6)); }
	inline bool get_U3CIsInputTrigU3Ek__BackingField_6() const { return ___U3CIsInputTrigU3Ek__BackingField_6; }
	inline bool* get_address_of_U3CIsInputTrigU3Ek__BackingField_6() { return &___U3CIsInputTrigU3Ek__BackingField_6; }
	inline void set_U3CIsInputTrigU3Ek__BackingField_6(bool value)
	{
		___U3CIsInputTrigU3Ek__BackingField_6 = value;
	}

	inline static int32_t get_offset_of_U3CIsInputTrigCustomU3Ek__BackingField_7() { return static_cast<int32_t>(offsetof(AdvUiManager_t4018716000, ___U3CIsInputTrigCustomU3Ek__BackingField_7)); }
	inline bool get_U3CIsInputTrigCustomU3Ek__BackingField_7() const { return ___U3CIsInputTrigCustomU3Ek__BackingField_7; }
	inline bool* get_address_of_U3CIsInputTrigCustomU3Ek__BackingField_7() { return &___U3CIsInputTrigCustomU3Ek__BackingField_7; }
	inline void set_U3CIsInputTrigCustomU3Ek__BackingField_7(bool value)
	{
		___U3CIsInputTrigCustomU3Ek__BackingField_7 = value;
	}

	inline static int32_t get_offset_of_U3CIsShowingMessageWindowU3Ek__BackingField_8() { return static_cast<int32_t>(offsetof(AdvUiManager_t4018716000, ___U3CIsShowingMessageWindowU3Ek__BackingField_8)); }
	inline bool get_U3CIsShowingMessageWindowU3Ek__BackingField_8() const { return ___U3CIsShowingMessageWindowU3Ek__BackingField_8; }
	inline bool* get_address_of_U3CIsShowingMessageWindowU3Ek__BackingField_8() { return &___U3CIsShowingMessageWindowU3Ek__BackingField_8; }
	inline void set_U3CIsShowingMessageWindowU3Ek__BackingField_8(bool value)
	{
		___U3CIsShowingMessageWindowU3Ek__BackingField_8 = value;
	}

	inline static int32_t get_offset_of_U3CIsHideMenuButtonU3Ek__BackingField_9() { return static_cast<int32_t>(offsetof(AdvUiManager_t4018716000, ___U3CIsHideMenuButtonU3Ek__BackingField_9)); }
	inline bool get_U3CIsHideMenuButtonU3Ek__BackingField_9() const { return ___U3CIsHideMenuButtonU3Ek__BackingField_9; }
	inline bool* get_address_of_U3CIsHideMenuButtonU3Ek__BackingField_9() { return &___U3CIsHideMenuButtonU3Ek__BackingField_9; }
	inline void set_U3CIsHideMenuButtonU3Ek__BackingField_9(bool value)
	{
		___U3CIsHideMenuButtonU3Ek__BackingField_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ADVUIMANAGER_T4018716000_H
#ifndef BOOTCUSTOMPROJECTSETTING_T856768400_H
#define BOOTCUSTOMPROJECTSETTING_T856768400_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BootCustomProjectSetting
struct  BootCustomProjectSetting_t856768400  : public MonoBehaviour_t1158329972
{
public:
	// Utage.CustomProjectSetting BootCustomProjectSetting::customProjectSetting
	CustomProjectSetting_t3840122254 * ___customProjectSetting_2;

public:
	inline static int32_t get_offset_of_customProjectSetting_2() { return static_cast<int32_t>(offsetof(BootCustomProjectSetting_t856768400, ___customProjectSetting_2)); }
	inline CustomProjectSetting_t3840122254 * get_customProjectSetting_2() const { return ___customProjectSetting_2; }
	inline CustomProjectSetting_t3840122254 ** get_address_of_customProjectSetting_2() { return &___customProjectSetting_2; }
	inline void set_customProjectSetting_2(CustomProjectSetting_t3840122254 * value)
	{
		___customProjectSetting_2 = value;
		Il2CppCodeGenWriteBarrier((&___customProjectSetting_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BOOTCUSTOMPROJECTSETTING_T856768400_H
#ifndef CAPTURECAMERA_T317215799_H
#define CAPTURECAMERA_T317215799_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.CaptureCamera
struct  CaptureCamera_t317215799  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.RenderTexture Utage.CaptureCamera::<CaptureImage>k__BackingField
	RenderTexture_t2666733923 * ___U3CCaptureImageU3Ek__BackingField_2;
	// Utage.CaptureCameraEvent Utage.CaptureCamera::OnCaptured
	CaptureCameraEvent_t3263087955 * ___OnCaptured_3;

public:
	inline static int32_t get_offset_of_U3CCaptureImageU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(CaptureCamera_t317215799, ___U3CCaptureImageU3Ek__BackingField_2)); }
	inline RenderTexture_t2666733923 * get_U3CCaptureImageU3Ek__BackingField_2() const { return ___U3CCaptureImageU3Ek__BackingField_2; }
	inline RenderTexture_t2666733923 ** get_address_of_U3CCaptureImageU3Ek__BackingField_2() { return &___U3CCaptureImageU3Ek__BackingField_2; }
	inline void set_U3CCaptureImageU3Ek__BackingField_2(RenderTexture_t2666733923 * value)
	{
		___U3CCaptureImageU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CCaptureImageU3Ek__BackingField_2), value);
	}

	inline static int32_t get_offset_of_OnCaptured_3() { return static_cast<int32_t>(offsetof(CaptureCamera_t317215799, ___OnCaptured_3)); }
	inline CaptureCameraEvent_t3263087955 * get_OnCaptured_3() const { return ___OnCaptured_3; }
	inline CaptureCameraEvent_t3263087955 ** get_address_of_OnCaptured_3() { return &___OnCaptured_3; }
	inline void set_OnCaptured_3(CaptureCameraEvent_t3263087955 * value)
	{
		___OnCaptured_3 = value;
		Il2CppCodeGenWriteBarrier((&___OnCaptured_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CAPTURECAMERA_T317215799_H
#ifndef ADVUGUIMANAGER_T362338064_H
#define ADVUGUIMANAGER_T362338064_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.AdvUguiManager
struct  AdvUguiManager_t362338064  : public AdvUiManager_t4018716000
{
public:
	// Utage.AdvUguiMessageWindowManager Utage.AdvUguiManager::messageWindow
	AdvUguiMessageWindowManager_t1210998287 * ___messageWindow_11;
	// Utage.AdvUguiSelectionManager Utage.AdvUguiManager::selection
	AdvUguiSelectionManager_t456513248 * ___selection_12;
	// Utage.AdvUguiBacklogManager Utage.AdvUguiManager::backLog
	AdvUguiBacklogManager_t342992083 * ___backLog_13;
	// System.Boolean Utage.AdvUguiManager::disableMouseWheelBackLog
	bool ___disableMouseWheelBackLog_14;
	// UnityEngine.AudioSource Utage.AdvUguiManager::tapSe
	AudioSource_t1135106623 * ___tapSe_15;

public:
	inline static int32_t get_offset_of_messageWindow_11() { return static_cast<int32_t>(offsetof(AdvUguiManager_t362338064, ___messageWindow_11)); }
	inline AdvUguiMessageWindowManager_t1210998287 * get_messageWindow_11() const { return ___messageWindow_11; }
	inline AdvUguiMessageWindowManager_t1210998287 ** get_address_of_messageWindow_11() { return &___messageWindow_11; }
	inline void set_messageWindow_11(AdvUguiMessageWindowManager_t1210998287 * value)
	{
		___messageWindow_11 = value;
		Il2CppCodeGenWriteBarrier((&___messageWindow_11), value);
	}

	inline static int32_t get_offset_of_selection_12() { return static_cast<int32_t>(offsetof(AdvUguiManager_t362338064, ___selection_12)); }
	inline AdvUguiSelectionManager_t456513248 * get_selection_12() const { return ___selection_12; }
	inline AdvUguiSelectionManager_t456513248 ** get_address_of_selection_12() { return &___selection_12; }
	inline void set_selection_12(AdvUguiSelectionManager_t456513248 * value)
	{
		___selection_12 = value;
		Il2CppCodeGenWriteBarrier((&___selection_12), value);
	}

	inline static int32_t get_offset_of_backLog_13() { return static_cast<int32_t>(offsetof(AdvUguiManager_t362338064, ___backLog_13)); }
	inline AdvUguiBacklogManager_t342992083 * get_backLog_13() const { return ___backLog_13; }
	inline AdvUguiBacklogManager_t342992083 ** get_address_of_backLog_13() { return &___backLog_13; }
	inline void set_backLog_13(AdvUguiBacklogManager_t342992083 * value)
	{
		___backLog_13 = value;
		Il2CppCodeGenWriteBarrier((&___backLog_13), value);
	}

	inline static int32_t get_offset_of_disableMouseWheelBackLog_14() { return static_cast<int32_t>(offsetof(AdvUguiManager_t362338064, ___disableMouseWheelBackLog_14)); }
	inline bool get_disableMouseWheelBackLog_14() const { return ___disableMouseWheelBackLog_14; }
	inline bool* get_address_of_disableMouseWheelBackLog_14() { return &___disableMouseWheelBackLog_14; }
	inline void set_disableMouseWheelBackLog_14(bool value)
	{
		___disableMouseWheelBackLog_14 = value;
	}

	inline static int32_t get_offset_of_tapSe_15() { return static_cast<int32_t>(offsetof(AdvUguiManager_t362338064, ___tapSe_15)); }
	inline AudioSource_t1135106623 * get_tapSe_15() const { return ___tapSe_15; }
	inline AudioSource_t1135106623 ** get_address_of_tapSe_15() { return &___tapSe_15; }
	inline void set_tapSe_15(AudioSource_t1135106623 * value)
	{
		___tapSe_15 = value;
		Il2CppCodeGenWriteBarrier((&___tapSe_15), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ADVUGUIMANAGER_T362338064_H





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2500 = { sizeof (AdvMacroManager_t3793610292), -1, sizeof(AdvMacroManager_t3793610292_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2500[3] = 
{
	AdvMacroManager_t3793610292::get_offset_of_macroDataTbl_0(),
	0,
	AdvMacroManager_t3793610292_StaticFields::get_offset_of_SheetNameRegex_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2501 = { sizeof (AdvScenarioData_t2546512739), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2501[6] = 
{
	AdvScenarioData_t2546512739::get_offset_of_name_0(),
	AdvScenarioData_t2546512739::get_offset_of_U3CDataGridU3Ek__BackingField_1(),
	AdvScenarioData_t2546512739::get_offset_of_isInit_2(),
	AdvScenarioData_t2546512739::get_offset_of_isAlreadyBackGroundLoad_3(),
	AdvScenarioData_t2546512739::get_offset_of_jumpDataList_4(),
	AdvScenarioData_t2546512739::get_offset_of_scenarioLabels_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2502 = { sizeof (AdvScenarioDataExported_t393182426), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2502[1] = 
{
	AdvScenarioDataExported_t393182426::get_offset_of_dictionary_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2503 = { sizeof (AdvScenarioJumpData_t4146817941), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2503[2] = 
{
	AdvScenarioJumpData_t4146817941::get_offset_of_U3CToLabelU3Ek__BackingField_0(),
	AdvScenarioJumpData_t4146817941::get_offset_of_U3CFromRowU3Ek__BackingField_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2504 = { sizeof (AdvScenarioLabelData_t2266689885), -1, sizeof(AdvScenarioLabelData_t2266689885_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2504[6] = 
{
	AdvScenarioLabelData_t2266689885::get_offset_of_U3CPageDataListU3Ek__BackingField_0(),
	AdvScenarioLabelData_t2266689885::get_offset_of_U3CScenarioLabelU3Ek__BackingField_1(),
	AdvScenarioLabelData_t2266689885::get_offset_of_U3CNextU3Ek__BackingField_2(),
	AdvScenarioLabelData_t2266689885::get_offset_of_U3CCommandListU3Ek__BackingField_3(),
	AdvScenarioLabelData_t2266689885::get_offset_of_scenarioLabelCommand_4(),
	AdvScenarioLabelData_t2266689885_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2505 = { sizeof (U3CDownloadU3Ec__AnonStorey0_t555719203), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2505[1] = 
{
	U3CDownloadU3Ec__AnonStorey0_t555719203::get_offset_of_dataManager_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2506 = { sizeof (U3CPreloadDeepU3Ec__AnonStorey1_t454674787), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2506[4] = 
{
	U3CPreloadDeepU3Ec__AnonStorey1_t454674787::get_offset_of_dataManager_0(),
	U3CPreloadDeepU3Ec__AnonStorey1_t454674787::get_offset_of_fileSet_1(),
	U3CPreloadDeepU3Ec__AnonStorey1_t454674787::get_offset_of_maxFilePreload_2(),
	U3CPreloadDeepU3Ec__AnonStorey1_t454674787::get_offset_of_deepLevel_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2507 = { sizeof (U3CPreloadDeepU3Ec__AnonStorey2_t2020758728), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2507[4] = 
{
	U3CPreloadDeepU3Ec__AnonStorey2_t2020758728::get_offset_of_dataManager_0(),
	U3CPreloadDeepU3Ec__AnonStorey2_t2020758728::get_offset_of_fileSet_1(),
	U3CPreloadDeepU3Ec__AnonStorey2_t2020758728::get_offset_of_maxFilePreload_2(),
	U3CPreloadDeepU3Ec__AnonStorey2_t2020758728::get_offset_of_deepLevel_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2508 = { sizeof (AdvScenarioPageData_t3333166790), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2508[9] = 
{
	AdvScenarioPageData_t3333166790::get_offset_of_U3CCommandListU3Ek__BackingField_0(),
	AdvScenarioPageData_t3333166790::get_offset_of_U3CTextDataListU3Ek__BackingField_1(),
	AdvScenarioPageData_t3333166790::get_offset_of_U3CScenarioLabelDataU3Ek__BackingField_2(),
	AdvScenarioPageData_t3333166790::get_offset_of_U3CJumpLabelListU3Ek__BackingField_3(),
	AdvScenarioPageData_t3333166790::get_offset_of_U3CAutoJumpLabelListU3Ek__BackingField_4(),
	AdvScenarioPageData_t3333166790::get_offset_of_U3CPageNoU3Ek__BackingField_5(),
	AdvScenarioPageData_t3333166790::get_offset_of_U3CMessageWindowNameU3Ek__BackingField_6(),
	AdvScenarioPageData_t3333166790::get_offset_of_U3CIndexTextTopCommandU3Ek__BackingField_7(),
	AdvScenarioPageData_t3333166790::get_offset_of_U3CEnableSaveU3Ek__BackingField_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2509 = { sizeof (U3CGetJumpScenarioLabelDataListU3Ec__AnonStorey0_t3855642138), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2509[2] = 
{
	U3CGetJumpScenarioLabelDataListU3Ec__AnonStorey0_t3855642138::get_offset_of_dataManager_0(),
	U3CGetJumpScenarioLabelDataListU3Ec__AnonStorey0_t3855642138::get_offset_of_U24this_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2510 = { sizeof (U3CGetAutoJumpLabelsU3Ec__AnonStorey1_t2589819677), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2510[2] = 
{
	U3CGetAutoJumpLabelsU3Ec__AnonStorey1_t2589819677::get_offset_of_dataManager_0(),
	U3CGetAutoJumpLabelsU3Ec__AnonStorey1_t2589819677::get_offset_of_U24this_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2511 = { sizeof (U3CDownloadU3Ec__AnonStorey2_t2119668604), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2511[1] = 
{
	U3CDownloadU3Ec__AnonStorey2_t2119668604::get_offset_of_dataManager_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2512 = { sizeof (U3CAddToFileSetU3Ec__AnonStorey3_t1526526459), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2512[1] = 
{
	U3CAddToFileSetU3Ec__AnonStorey3_t1526526459::get_offset_of_fileSet_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2513 = { sizeof (AdvScenarioPlayerEvent_t1780583340), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2514 = { sizeof (AdvCommandEvent_t2718201788), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2515 = { sizeof (AdvScenarioPlayer_t1295831480), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2515[18] = 
{
	AdvScenarioPlayer_t1295831480::get_offset_of_sendMessageTarget_2(),
	AdvScenarioPlayer_t1295831480::get_offset_of_debugOutPut_3(),
	AdvScenarioPlayer_t1295831480::get_offset_of_maxFilePreload_4(),
	AdvScenarioPlayer_t1295831480::get_offset_of_preloadDeep_5(),
	AdvScenarioPlayer_t1295831480::get_offset_of_onEndScenario_6(),
	AdvScenarioPlayer_t1295831480::get_offset_of_onPauseScenario_7(),
	AdvScenarioPlayer_t1295831480::get_offset_of_onEndOrPauseScenario_8(),
	AdvScenarioPlayer_t1295831480::get_offset_of_onBeginCommand_9(),
	AdvScenarioPlayer_t1295831480::get_offset_of_onUpdatePreWaitingCommand_10(),
	AdvScenarioPlayer_t1295831480::get_offset_of_onUpdateWaitingCommand_11(),
	AdvScenarioPlayer_t1295831480::get_offset_of_onEndCommand_12(),
	AdvScenarioPlayer_t1295831480::get_offset_of_engine_13(),
	AdvScenarioPlayer_t1295831480::get_offset_of_mainThread_14(),
	AdvScenarioPlayer_t1295831480::get_offset_of_U3CIsEndScenarioU3Ek__BackingField_15(),
	AdvScenarioPlayer_t1295831480::get_offset_of_U3CIsReservedEndScenarioU3Ek__BackingField_16(),
	AdvScenarioPlayer_t1295831480::get_offset_of_U3CIsPausingU3Ek__BackingField_17(),
	AdvScenarioPlayer_t1295831480::get_offset_of_U3CCurrentGallerySceneLabelU3Ek__BackingField_18(),
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2516 = { sizeof (DebugOutPut_t1254200721)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2516[4] = 
{
	DebugOutPut_t1254200721::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2517 = { sizeof (U3CCoStartSaveDataU3Ec__Iterator0_t3464725285), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2517[5] = 
{
	U3CCoStartSaveDataU3Ec__Iterator0_t3464725285::get_offset_of_saveData_0(),
	U3CCoStartSaveDataU3Ec__Iterator0_t3464725285::get_offset_of_U24this_1(),
	U3CCoStartSaveDataU3Ec__Iterator0_t3464725285::get_offset_of_U24current_2(),
	U3CCoStartSaveDataU3Ec__Iterator0_t3464725285::get_offset_of_U24disposing_3(),
	U3CCoStartSaveDataU3Ec__Iterator0_t3464725285::get_offset_of_U24PC_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2518 = { sizeof (AdvScenarioThread_t1270526825), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2518[13] = 
{
	AdvScenarioThread_t1270526825::get_offset_of_threadName_2(),
	AdvScenarioThread_t1270526825::get_offset_of_U3CIsMainThreadU3Ek__BackingField_3(),
	AdvScenarioThread_t1270526825::get_offset_of_U3CIsLoadingU3Ek__BackingField_4(),
	AdvScenarioThread_t1270526825::get_offset_of_U3CIsPlayingU3Ek__BackingField_5(),
	AdvScenarioThread_t1270526825::get_offset_of_ifManager_6(),
	AdvScenarioThread_t1270526825::get_offset_of_jumpManager_7(),
	AdvScenarioThread_t1270526825::get_offset_of_waitManager_8(),
	AdvScenarioThread_t1270526825::get_offset_of_U3CParenetThreadU3Ek__BackingField_9(),
	AdvScenarioThread_t1270526825::get_offset_of_subThreadList_10(),
	AdvScenarioThread_t1270526825::get_offset_of_preloadFileSet_11(),
	AdvScenarioThread_t1270526825::get_offset_of_currentCommand_12(),
	AdvScenarioThread_t1270526825::get_offset_of_U3CSkipPageHeaerOnSaveU3Ek__BackingField_13(),
	AdvScenarioThread_t1270526825::get_offset_of_U3CScenarioPlayerU3Ek__BackingField_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2519 = { sizeof (U3CCoStartScenarioU3Ec__Iterator0_t1715464187), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2519[11] = 
{
	U3CCoStartScenarioU3Ec__Iterator0_t1715464187::get_offset_of_label_0(),
	U3CCoStartScenarioU3Ec__Iterator0_t1715464187::get_offset_of_page_1(),
	U3CCoStartScenarioU3Ec__Iterator0_t1715464187::get_offset_of_U3CcurrentLabelDataU3E__0_2(),
	U3CCoStartScenarioU3Ec__Iterator0_t1715464187::get_offset_of_U3CcurrentPageDataU3E__1_3(),
	U3CCoStartScenarioU3Ec__Iterator0_t1715464187::get_offset_of_returnToCommand_4(),
	U3CCoStartScenarioU3Ec__Iterator0_t1715464187::get_offset_of_skipPageHeaer_5(),
	U3CCoStartScenarioU3Ec__Iterator0_t1715464187::get_offset_of_U3CpageCoroutineU3E__2_6(),
	U3CCoStartScenarioU3Ec__Iterator0_t1715464187::get_offset_of_U24this_7(),
	U3CCoStartScenarioU3Ec__Iterator0_t1715464187::get_offset_of_U24current_8(),
	U3CCoStartScenarioU3Ec__Iterator0_t1715464187::get_offset_of_U24disposing_9(),
	U3CCoStartScenarioU3Ec__Iterator0_t1715464187::get_offset_of_U24PC_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2520 = { sizeof (U3CCoStartPageU3Ec__Iterator1_t1196900359), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2520[10] = 
{
	U3CCoStartPageU3Ec__Iterator1_t1196900359::get_offset_of_skipPageHeaer_0(),
	U3CCoStartPageU3Ec__Iterator1_t1196900359::get_offset_of_pageData_1(),
	U3CCoStartPageU3Ec__Iterator1_t1196900359::get_offset_of_U3CindexU3E__0_2(),
	U3CCoStartPageU3Ec__Iterator1_t1196900359::get_offset_of_U3CcommandU3E__0_3(),
	U3CCoStartPageU3Ec__Iterator1_t1196900359::get_offset_of_returnToCommand_4(),
	U3CCoStartPageU3Ec__Iterator1_t1196900359::get_offset_of_labelData_5(),
	U3CCoStartPageU3Ec__Iterator1_t1196900359::get_offset_of_U24this_6(),
	U3CCoStartPageU3Ec__Iterator1_t1196900359::get_offset_of_U24current_7(),
	U3CCoStartPageU3Ec__Iterator1_t1196900359::get_offset_of_U24disposing_8(),
	U3CCoStartPageU3Ec__Iterator1_t1196900359::get_offset_of_U24PC_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2521 = { sizeof (AdvCommandWaitType_t3266086913)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2521[6] = 
{
	AdvCommandWaitType_t3266086913::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2522 = { sizeof (AdvWaitManager_t2981197357), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2522[1] = 
{
	AdvWaitManager_t2981197357::get_offset_of_commandList_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2523 = { sizeof (AdvUguiBacklog_t1854927244), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2523[6] = 
{
	AdvUguiBacklog_t1854927244::get_offset_of_text_2(),
	AdvUguiBacklog_t1854927244::get_offset_of_characterName_3(),
	AdvUguiBacklog_t1854927244::get_offset_of_soundIcon_4(),
	AdvUguiBacklog_t1854927244::get_offset_of_button_5(),
	AdvUguiBacklog_t1854927244::get_offset_of_isMultiTextInPage_6(),
	AdvUguiBacklog_t1854927244::get_offset_of_data_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2524 = { sizeof (U3CInitU3Ec__AnonStorey1_t1155527497), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2524[2] = 
{
	U3CInitU3Ec__AnonStorey1_t1155527497::get_offset_of_data_0(),
	U3CInitU3Ec__AnonStorey1_t1155527497::get_offset_of_U24this_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2525 = { sizeof (U3CCoPlayVoiceU3Ec__Iterator0_t3696192360), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2525[8] = 
{
	U3CCoPlayVoiceU3Ec__Iterator0_t3696192360::get_offset_of_voiceFileName_0(),
	U3CCoPlayVoiceU3Ec__Iterator0_t3696192360::get_offset_of_U3CfileU3E__0_1(),
	U3CCoPlayVoiceU3Ec__Iterator0_t3696192360::get_offset_of_U3CmanagerU3E__0_2(),
	U3CCoPlayVoiceU3Ec__Iterator0_t3696192360::get_offset_of_characterLabel_3(),
	U3CCoPlayVoiceU3Ec__Iterator0_t3696192360::get_offset_of_U24this_4(),
	U3CCoPlayVoiceU3Ec__Iterator0_t3696192360::get_offset_of_U24current_5(),
	U3CCoPlayVoiceU3Ec__Iterator0_t3696192360::get_offset_of_U24disposing_6(),
	U3CCoPlayVoiceU3Ec__Iterator0_t3696192360::get_offset_of_U24PC_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2526 = { sizeof (AdvUguiBacklogManager_t342992083), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2526[5] = 
{
	AdvUguiBacklogManager_t342992083::get_offset_of_type_2(),
	AdvUguiBacklogManager_t342992083::get_offset_of_engine_3(),
	AdvUguiBacklogManager_t342992083::get_offset_of_listView_4(),
	AdvUguiBacklogManager_t342992083::get_offset_of_fullScreenLogText_5(),
	AdvUguiBacklogManager_t342992083::get_offset_of_isCloseScrollWheelDown_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2527 = { sizeof (BacklogType_t3430840675)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2527[3] = 
{
	BacklogType_t3430840675::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2528 = { sizeof (AdvUguiLoadGraphicFile_t1474676621), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2528[6] = 
{
	AdvUguiLoadGraphicFile_t1474676621::get_offset_of_loader_2(),
	AdvUguiLoadGraphicFile_t1474676621::get_offset_of_U3CGraphicComponentU3Ek__BackingField_3(),
	AdvUguiLoadGraphicFile_t1474676621::get_offset_of_U3CFileU3Ek__BackingField_4(),
	AdvUguiLoadGraphicFile_t1474676621::get_offset_of_U3CGraphicInfoU3Ek__BackingField_5(),
	AdvUguiLoadGraphicFile_t1474676621::get_offset_of_sizeSetting_6(),
	AdvUguiLoadGraphicFile_t1474676621::get_offset_of_OnLoadEnd_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2529 = { sizeof (SizeSetting_t3046087381)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2529[4] = 
{
	SizeSetting_t3046087381::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2530 = { sizeof (U3CLoadFileU3Ec__AnonStorey1_t2370668122), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2530[2] = 
{
	U3CLoadFileU3Ec__AnonStorey1_t2370668122::get_offset_of_graphic_0(),
	U3CLoadFileU3Ec__AnonStorey1_t2370668122::get_offset_of_U24this_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2531 = { sizeof (U3CCoWaitTextureFileLoadingU3Ec__Iterator0_t1598614109), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2531[4] = 
{
	U3CCoWaitTextureFileLoadingU3Ec__Iterator0_t1598614109::get_offset_of_U24this_0(),
	U3CCoWaitTextureFileLoadingU3Ec__Iterator0_t1598614109::get_offset_of_U24current_1(),
	U3CCoWaitTextureFileLoadingU3Ec__Iterator0_t1598614109::get_offset_of_U24disposing_2(),
	U3CCoWaitTextureFileLoadingU3Ec__Iterator0_t1598614109::get_offset_of_U24PC_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2532 = { sizeof (AdvUguiManager_t362338064), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2532[5] = 
{
	AdvUguiManager_t362338064::get_offset_of_messageWindow_11(),
	AdvUguiManager_t362338064::get_offset_of_selection_12(),
	AdvUguiManager_t362338064::get_offset_of_backLog_13(),
	AdvUguiManager_t362338064::get_offset_of_disableMouseWheelBackLog_14(),
	AdvUguiManager_t362338064::get_offset_of_tapSe_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2533 = { sizeof (AdvUguiMessageWindow_t2947985100), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2533[13] = 
{
	AdvUguiMessageWindow_t2947985100::get_offset_of_engine_2(),
	AdvUguiMessageWindow_t2947985100::get_offset_of_readColorMode_3(),
	AdvUguiMessageWindow_t2947985100::get_offset_of_readColor_4(),
	AdvUguiMessageWindow_t2947985100::get_offset_of_defaultTextColor_5(),
	AdvUguiMessageWindow_t2947985100::get_offset_of_defaultNameTextColor_6(),
	AdvUguiMessageWindow_t2947985100::get_offset_of_text_7(),
	AdvUguiMessageWindow_t2947985100::get_offset_of_nameText_8(),
	AdvUguiMessageWindow_t2947985100::get_offset_of_rootChildren_9(),
	AdvUguiMessageWindow_t2947985100::get_offset_of_translateMessageWindowRoot_10(),
	AdvUguiMessageWindow_t2947985100::get_offset_of_iconWaitInput_11(),
	AdvUguiMessageWindow_t2947985100::get_offset_of_iconBrPage_12(),
	AdvUguiMessageWindow_t2947985100::get_offset_of_isLinkPositionIconBrPage_13(),
	AdvUguiMessageWindow_t2947985100::get_offset_of_U3CIsCurrentU3Ek__BackingField_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2534 = { sizeof (ReadColorMode_t2593131983)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2534[3] = 
{
	ReadColorMode_t2593131983::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2535 = { sizeof (AdvUguiMessageWindowFaceIcon_t623217832), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2535[3] = 
{
	AdvUguiMessageWindowFaceIcon_t623217832::get_offset_of_engine_2(),
	AdvUguiMessageWindowFaceIcon_t623217832::get_offset_of_targetObject_3(),
	AdvUguiMessageWindowFaceIcon_t623217832::get_offset_of_iconRoot_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2536 = { sizeof (IconGraphicType_t2552029784)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2536[5] = 
{
	IconGraphicType_t2552029784::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2537 = { sizeof (U3CSetIconImageU3Ec__AnonStorey0_t2483997726), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2537[2] = 
{
	U3CSetIconImageU3Ec__AnonStorey0_t2483997726::get_offset_of_file_0(),
	U3CSetIconImageU3Ec__AnonStorey0_t2483997726::get_offset_of_U24this_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2538 = { sizeof (AdvUguiMessageWindowManager_t1210998287), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2539 = { sizeof (AdvUguiSelection_t2632218991), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2539[2] = 
{
	AdvUguiSelection_t2632218991::get_offset_of_text_2(),
	AdvUguiSelection_t2632218991::get_offset_of_data_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2540 = { sizeof (U3CInitU3Ec__AnonStorey0_t3081790771), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2540[2] = 
{
	U3CInitU3Ec__AnonStorey0_t3081790771::get_offset_of_ButtonClickedEvent_0(),
	U3CInitU3Ec__AnonStorey0_t3081790771::get_offset_of_U24this_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2541 = { sizeof (AdvUguiSelectionManager_t456513248), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2541[7] = 
{
	AdvUguiSelectionManager_t456513248::get_offset_of_engine_2(),
	AdvUguiSelectionManager_t456513248::get_offset_of_selectedColorMode_3(),
	AdvUguiSelectionManager_t456513248::get_offset_of_selectedColor_4(),
	AdvUguiSelectionManager_t456513248::get_offset_of_prefabList_5(),
	AdvUguiSelectionManager_t456513248::get_offset_of_listView_6(),
	AdvUguiSelectionManager_t456513248::get_offset_of_canvasGroup_7(),
	AdvUguiSelectionManager_t456513248::get_offset_of_items_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2542 = { sizeof (SelectedColorMode_t2529639836)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2542[3] = 
{
	SelectedColorMode_t2529639836::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2543 = { sizeof (U3CGetPrefabU3Ec__AnonStorey0_t1981674732), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2543[1] = 
{
	U3CGetPrefabU3Ec__AnonStorey0_t1981674732::get_offset_of_selectionData_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2544 = { sizeof (AdvUiManager_t4018716000), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2544[9] = 
{
	AdvUiManager_t4018716000::get_offset_of_engine_2(),
	AdvUiManager_t4018716000::get_offset_of_guiManager_3(),
	AdvUiManager_t4018716000::get_offset_of_status_4(),
	AdvUiManager_t4018716000::get_offset_of_U3CCurrentPointerDataU3Ek__BackingField_5(),
	AdvUiManager_t4018716000::get_offset_of_U3CIsInputTrigU3Ek__BackingField_6(),
	AdvUiManager_t4018716000::get_offset_of_U3CIsInputTrigCustomU3Ek__BackingField_7(),
	AdvUiManager_t4018716000::get_offset_of_U3CIsShowingMessageWindowU3Ek__BackingField_8(),
	AdvUiManager_t4018716000::get_offset_of_U3CIsHideMenuButtonU3Ek__BackingField_9(),
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2545 = { sizeof (UiStatus_t3434108101)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2545[4] = 
{
	UiStatus_t3434108101::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2546 = { sizeof (AdvGuiBase_t2023941291), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2546[7] = 
{
	AdvGuiBase_t2023941291::get_offset_of_U3CTargetU3Ek__BackingField_0(),
	AdvGuiBase_t2023941291::get_offset_of_rectTransform_1(),
	AdvGuiBase_t2023941291::get_offset_of_canvas_2(),
	AdvGuiBase_t2023941291::get_offset_of_canvasRectTransform_3(),
	AdvGuiBase_t2023941291::get_offset_of_U3CHasChangedU3Ek__BackingField_4(),
	AdvGuiBase_t2023941291::get_offset_of_defaultData_5(),
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2547 = { sizeof (AdvGuiManager_t2331361001), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2547[3] = 
{
	AdvGuiManager_t2331361001::get_offset_of_guiObjects_2(),
	AdvGuiManager_t2331361001::get_offset_of_objects_3(),
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2548 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2549 = { sizeof (BinaryBuffer_t1215122249), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2549[1] = 
{
	BinaryBuffer_t1215122249::get_offset_of_buffers_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2550 = { sizeof (BinaryIOExtensions_t1647632937), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2551 = { sizeof (BinaryUtil_t1236589793), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2552 = { sizeof (CameraManager_t586526220), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2552[2] = 
{
	CameraManager_t586526220::get_offset_of_cameraList_2(),
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2553 = { sizeof (U3CFindCameraRootU3Ec__AnonStorey0_t424579444), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2553[1] = 
{
	U3CFindCameraRootU3Ec__AnonStorey0_t424579444::get_offset_of_name_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2554 = { sizeof (CameraRoot_t3848343401), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2554[5] = 
{
	CameraRoot_t3848343401::get_offset_of_letterBoxCamera_2(),
	CameraRoot_t3848343401::get_offset_of_startPosition_3(),
	CameraRoot_t3848343401::get_offset_of_startScale_4(),
	CameraRoot_t3848343401::get_offset_of_startEulerAngles_5(),
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2555 = { sizeof (CaptureCameraEvent_t3263087955), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2556 = { sizeof (CaptureCamera_t317215799), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2556[2] = 
{
	CaptureCamera_t317215799::get_offset_of_U3CCaptureImageU3Ek__BackingField_2(),
	CaptureCamera_t317215799::get_offset_of_OnCaptured_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2557 = { sizeof (LetterBoxCameraEvent_t3581932304), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2558 = { sizeof (LetterBoxCamera_t3507617684), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2558[16] = 
{
	LetterBoxCamera_t3507617684::get_offset_of_pixelsToUnits_2(),
	LetterBoxCamera_t3507617684::get_offset_of_width_3(),
	LetterBoxCamera_t3507617684::get_offset_of_height_4(),
	LetterBoxCamera_t3507617684::get_offset_of_isFlexible_5(),
	LetterBoxCamera_t3507617684::get_offset_of_maxWidth_6(),
	LetterBoxCamera_t3507617684::get_offset_of_maxHeight_7(),
	LetterBoxCamera_t3507617684::get_offset_of_anchor_8(),
	LetterBoxCamera_t3507617684::get_offset_of_OnGameScreenSizeChange_9(),
	LetterBoxCamera_t3507617684::get_offset_of_screenAspectRatio_10(),
	LetterBoxCamera_t3507617684::get_offset_of_padding_11(),
	LetterBoxCamera_t3507617684::get_offset_of_currentSize_12(),
	LetterBoxCamera_t3507617684::get_offset_of_zoom2D_13(),
	LetterBoxCamera_t3507617684::get_offset_of_zoom2DCenter_14(),
	LetterBoxCamera_t3507617684::get_offset_of_cachedCamera_15(),
	LetterBoxCamera_t3507617684::get_offset_of_hasChanged_16(),
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2559 = { sizeof (AnchorType_t847091700)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2559[10] = 
{
	AnchorType_t847091700::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2560 = { sizeof (BootCustomProjectSetting_t856768400), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2560[1] = 
{
	BootCustomProjectSetting_t856768400::get_offset_of_customProjectSetting_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2561 = { sizeof (CustomProjectSetting_t3840122254), -1, sizeof(CustomProjectSetting_t3840122254_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2561[2] = 
{
	CustomProjectSetting_t3840122254_StaticFields::get_offset_of_instance_2(),
	CustomProjectSetting_t3840122254::get_offset_of_language_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2562 = { sizeof (ErrorMsg_t1355364995)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2562[32] = 
{
	ErrorMsg_t1355364995::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2563 = { sizeof (LanguageErrorMsg_t750765065), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2563[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2564 = { sizeof (LanguageData_t3686821228), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2564[2] = 
{
	LanguageData_t3686821228::get_offset_of_languages_0(),
	LanguageData_t3686821228::get_offset_of_dataTbl_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2565 = { sizeof (LanguageStrings_t2372834651), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2565[1] = 
{
	LanguageStrings_t2372834651::get_offset_of_U3CStringsU3Ek__BackingField_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2566 = { sizeof (LanguageManager_t2685744067), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2567 = { sizeof (LanguageName_t1361222579)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2567[3] = 
{
	LanguageName_t1361222579::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2568 = { sizeof (LanguageManagerBase_t1275854546), -1, sizeof(LanguageManagerBase_t1275854546_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2568[11] = 
{
	LanguageManagerBase_t1275854546_StaticFields::get_offset_of_instance_2(),
	0,
	LanguageManagerBase_t1275854546::get_offset_of_language_4(),
	LanguageManagerBase_t1275854546::get_offset_of_defaultLanguage_5(),
	LanguageManagerBase_t1275854546::get_offset_of_languageData_6(),
	LanguageManagerBase_t1275854546::get_offset_of_ignoreLocalizeUiText_7(),
	LanguageManagerBase_t1275854546::get_offset_of_ignoreLocalizeVoice_8(),
	LanguageManagerBase_t1275854546::get_offset_of_voiceLanguages_9(),
	LanguageManagerBase_t1275854546::get_offset_of_U3COnChangeLanugageU3Ek__BackingField_10(),
	LanguageManagerBase_t1275854546::get_offset_of_currentLanguage_11(),
	LanguageManagerBase_t1275854546::get_offset_of_U3CDataU3Ek__BackingField_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2569 = { sizeof (SystemText_t2499456704)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2569[36] = 
{
	SystemText_t2499456704::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2570 = { sizeof (LanguageSystemText_t2944673402), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2570[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2571 = { sizeof (SerializableDictionaryKeyValue_t1605592419), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2571[1] = 
{
	SerializableDictionaryKeyValue_t1605592419::get_offset_of_key_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2572 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2572[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2573 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2573[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2574 = { sizeof (SerializableDictionaryBinaryIOKeyValue_t105246368), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2575 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2576 = { sizeof (DictionaryKeyValueBool_t2944486586), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2576[1] = 
{
	DictionaryKeyValueBool_t2944486586::get_offset_of_value_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2577 = { sizeof (DictionaryBool_t255000822), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2578 = { sizeof (DictionaryKeyValueInt_t3592094321), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2578[1] = 
{
	DictionaryKeyValueInt_t3592094321::get_offset_of_value_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2579 = { sizeof (DictionaryInt_t3731914685), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2580 = { sizeof (DictionaryKeyValueFloat_t3848152012), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2580[1] = 
{
	DictionaryKeyValueFloat_t3848152012::get_offset_of_value_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2581 = { sizeof (DictionaryFloat_t436926956), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2582 = { sizeof (DictionaryKeyValueDouble_t1135602291), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2582[1] = 
{
	DictionaryKeyValueDouble_t1135602291::get_offset_of_value_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2583 = { sizeof (DictionaryDouble_t2509882451), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2584 = { sizeof (DictionaryKeyValueString_t1086885503), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2584[1] = 
{
	DictionaryKeyValueString_t1086885503::get_offset_of_value_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2585 = { sizeof (DictionaryString_t3376238631), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2586 = { sizeof (HelpBoxAttribute_t1722922102), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2586[2] = 
{
	HelpBoxAttribute_t1722922102::get_offset_of_U3CMessageU3Ek__BackingField_1(),
	HelpBoxAttribute_t1722922102::get_offset_of_U3CMessageTypeU3Ek__BackingField_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2587 = { sizeof (Type_t3182439621)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2587[5] = 
{
	Type_t3182439621::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2588 = { sizeof (AddButtonAttribute_t3818261841), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2588[2] = 
{
	AddButtonAttribute_t3818261841::get_offset_of_U3CFunctionU3Ek__BackingField_1(),
	AddButtonAttribute_t3818261841::get_offset_of_U3CTextU3Ek__BackingField_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2589 = { sizeof (ButtonAttribute_t559151802), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2589[2] = 
{
	ButtonAttribute_t559151802::get_offset_of_U3CFunctionU3Ek__BackingField_1(),
	ButtonAttribute_t559151802::get_offset_of_U3CTextU3Ek__BackingField_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2590 = { sizeof (EnumFlagsAttribute_t3854793506), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2591 = { sizeof (HideAttribute_t1097616606), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2591[1] = 
{
	HideAttribute_t1097616606::get_offset_of_U3CFunctionU3Ek__BackingField_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2592 = { sizeof (HorizontalAttribute_t2286046922), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2593 = { sizeof (IntPopupAttribute_t3633498147), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2593[1] = 
{
	IntPopupAttribute_t3633498147::get_offset_of_popupList_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2594 = { sizeof (LimitEnumAttribute_t2236047900), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2594[1] = 
{
	LimitEnumAttribute_t2236047900::get_offset_of_U3CArgsU3Ek__BackingField_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2595 = { sizeof (MinAttribute_t155206958), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2595[1] = 
{
	MinAttribute_t155206958::get_offset_of_U3CMinU3Ek__BackingField_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2596 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2596[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2597 = { sizeof (MinMaxFloat_t1425080750), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2598 = { sizeof (MinMaxInt_t3259591635), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2599 = { sizeof (MinMaxAttribute_t10394644), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2599[4] = 
{
	MinMaxAttribute_t10394644::get_offset_of_U3CMinU3Ek__BackingField_1(),
	MinMaxAttribute_t10394644::get_offset_of_U3CMaxU3Ek__BackingField_2(),
	MinMaxAttribute_t10394644::get_offset_of_U3CMinPropertyNameU3Ek__BackingField_3(),
	MinMaxAttribute_t10394644::get_offset_of_U3CMaxPropertyNameU3Ek__BackingField_4(),
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
