﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "object-internals.h"

// System.String
struct String_t;
// Utage.SubRoutineInfo
struct SubRoutineInfo_t2697101720;
// System.Collections.Generic.Stack`1<Utage.SubRoutineInfo>
struct Stack_1_t3784829874;
// System.Collections.Generic.List`1<Utage.AdvJumpManager/RandomInfo>
struct List_1_t3145030736;
// Utage.UguiCrossFadeRawImage
struct UguiCrossFadeRawImage_t1509295035;
// Utage.AdvGraphicObjectRawImage
struct AdvGraphicObjectRawImage_t1741698045;
// System.IO.BinaryReader
struct BinaryReader_t2491843768;
// Utage.AdvCommand
struct AdvCommand_t2859960984;
// Utage.AdvIfData
struct AdvIfData_t1807102644;
// Utage.UguiCrossFadeDicing
struct UguiCrossFadeDicing_t2659796554;
// Utage.UguiTransition
struct UguiTransition_t3998485683;
// System.Action
struct Action_t3226471752;
// Utage.AdvGraphicObjectRenderTextureImage
struct AdvGraphicObjectRenderTextureImage_t750627570;
// Utage.StringGridRow
struct StringGridRow_t4193237197;
// System.Collections.Generic.List`1<Utage.StringGridRow>
struct List_1_t3562358329;
// UnityEngine.Video.VideoPlayer
struct VideoPlayer_t10059812;
// System.Collections.Generic.List`1<Utage.AdvBacklog/AdvBacklogDataInPage>
struct List_1_t4139040917;
// Utage.DictionaryInt
struct DictionaryInt_t3731914685;
// System.Collections.Generic.List`1<System.String>
struct List_1_t1398341365;
// Utage.TextData
struct TextData_t603454315;
// IAdvMessageWindow
struct IAdvMessageWindow_t4054595643;
// Utage.AdvVideoManager/VideoInfo
struct VideoInfo_t2935460087;
// Utage.AdvVideoManager
struct AdvVideoManager_t946313091;
// System.String[]
struct StringU5BU5D_t1642385972;
// Utage.AdvSelection
struct AdvSelection_t2301351953;
// System.Collections.Generic.List`1<Utage.AdvSelectedHistorySaveData/AdvSelectedHistoryData>
struct List_1_t1657183344;
// Utage.AdvSelectionManager
struct AdvSelectionManager_t3429904078;
// UnityEngine.Events.InvokableCallList
struct InvokableCallList_t2295673753;
// UnityEngine.Events.PersistentCallGroup
struct PersistentCallGroup_t339478082;
// Utage.AdvGraphicInfo
struct AdvGraphicInfo_t3545565645;
// Utage.AdvGraphicLayer
struct AdvGraphicLayer_t3630223822;
// System.Collections.Generic.List`1<Utage.AdvGraphicInfo>
struct List_1_t2914686777;
// Utage.AdvGraphicObject
struct AdvGraphicObject_t3651915246;
// Utage.AdvGraphicOperaitonArg
struct AdvGraphicOperaitonArg_t632373120;
// Utage.AdvGraphicLoader
struct AdvGraphicLoader_t202702654;
// System.Byte[]
struct ByteU5BU5D_t3397334013;
// System.Char[]
struct CharU5BU5D_t1328083999;
// System.Object[]
struct ObjectU5BU5D_t3614634134;
// System.Void
struct Void_t1841601450;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// System.DelegateData
struct DelegateData_t1572802995;
// Utage.ExpressionParser
struct ExpressionParser_t665800307;
// Utage.AdvGraphicInfo/CreateCustom
struct CreateCustom_t2292834413;
// System.Func`2<System.String,System.Boolean>
struct Func_2_t1989381442;
// Utage.IAdvSettingData
struct IAdvSettingData_t3351311174;
// Utage.AssetFile
struct AssetFile_t3383013256;
// Utage.AdvAnimationData
struct AdvAnimationData_t3431567515;
// Utage.AdvEyeBlinkData
struct AdvEyeBlinkData_t3287347958;
// Utage.AdvLipSynchData
struct AdvLipSynchData_t1090448263;
// Utage.AdvRenderTextureSetting
struct AdvRenderTextureSetting_t3039724782;
// System.Collections.Generic.Dictionary`2<System.String,System.Int32>
struct Dictionary_2_t3986656710;
// Utage.AssetFileManager
struct AssetFileManager_t1095395563;
// Utage.AssetFileInfo
struct AssetFileInfo_t1386031564;
// Utage.IAssetFileSettingData
struct IAssetFileSettingData_t3884760591;
// UnityEngine.TextAsset
struct TextAsset_t3973159845;
// UnityEngine.Texture2D
struct Texture2D_t3542995729;
// UnityEngine.AudioClip
struct AudioClip_t1932558630;
// UnityEngine.Object
struct Object_t1021602117;
// System.Collections.Generic.HashSet`1<System.Object>
struct HashSet_1_t1022910149;
// System.Predicate`1<System.Object>
struct Predicate_1_t1132419410;
// Utage.AdvGraphicManager
struct AdvGraphicManager_t3661251000;
// System.Collections.Generic.List`1<Utage.AdvGraphicLayer>
struct List_1_t2999344954;
// System.Int32[]
struct Int32U5BU5D_t3030399641;
// System.Collections.Generic.List`1<Utage.AdvConfigSaveData/TaggedMasterVolume>
struct List_1_t724273889;
// System.Type
struct Type_t;
// System.IAsyncResult
struct IAsyncResult_t1999651008;
// System.AsyncCallback
struct AsyncCallback_t163412349;
// Utage.BinaryBuffer
struct BinaryBuffer_t1215122249;
// UnityEngine.AssetBundle
struct AssetBundle_t2054978754;
// Utage.AdvGraphicBase
struct AdvGraphicBase_t2725442072;
// UnityEngine.Events.UnityAction`1<UnityEngine.EventSystems.BaseEventData>
struct UnityAction_1_t4047591376;
// Utage.AdvGraphicRenderTextureManager
struct AdvGraphicRenderTextureManager_t995712561;
// System.Collections.Generic.Dictionary`2<Utage.AdvLayerSettingData/LayerType,Utage.AdvGraphicGroup>
struct Dictionary_2_t2036845121;
// Utage.AdvEngine
struct AdvEngine_t1176753927;
// Utage.AdvRenderTextureSpace
struct AdvRenderTextureSpace_t3482187212;
// Utage.Timer
struct Timer_t2904185433;
// Utage.AdvEffectColor
struct AdvEffectColor_t2285992559;
// UnityEngine.RectTransform
struct RectTransform_t3349966182;
// UnityEngine.Events.UnityEvent
struct UnityEvent_t408735097;
// Utage.FileIOManager
struct FileIOManager_t855502573;
// Utage.AdvSaveManager/SaveSetting
struct SaveSetting_t424301263;
// System.Collections.Generic.List`1<UnityEngine.GameObject>
struct List_1_t1125654279;
// System.Collections.Generic.List`1<Utage.IBinaryIO>
struct List_1_t767462864;
// Utage.AdvSaveData
struct AdvSaveData_t4059487466;
// System.Collections.Generic.List`1<Utage.AdvSaveData>
struct List_1_t3428608598;
// Utage.EventEffectColor
struct EventEffectColor_t1679408286;
// Utage.AdvLayerSettingData
struct AdvLayerSettingData_t1908772360;
// System.Collections.Generic.Dictionary`2<System.String,Utage.AdvGraphicObject>
struct Dictionary_2_t1271727212;
// UnityEngine.Camera
struct Camera_t189460977;
// Utage.LetterBoxCamera
struct LetterBoxCamera_t3507617684;
// UnityEngine.Canvas
struct Canvas_t209405766;
// UnityEngine.Transform
struct Transform_t3275118058;
// System.Collections.Generic.List`1<Utage.AdvSelection>
struct List_1_t1670473085;
// Utage.SelectionEvent
struct SelectionEvent_t3989025344;
// Utage.MessageWindowEvent
struct MessageWindowEvent_t3103993945;
// System.Collections.Generic.Dictionary`2<System.String,Utage.AdvMessageWindow>
struct Dictionary_2_t1035234340;
// Utage.AdvMessageWindow
struct AdvMessageWindow_t3415422374;
// UnityEngine.RenderTexture
struct RenderTexture_t2666733923;
// UnityEngine.UI.CanvasScaler
struct CanvasScaler_t2574720772;
// UnityEngine.GameObject
struct GameObject_t1756533147;
// System.Collections.Generic.Dictionary`2<System.String,Utage.AdvVideoManager/VideoInfo>
struct Dictionary_2_t555272053;
// Utage.AdvReadHistorySaveData
struct AdvReadHistorySaveData_t361293356;
// Utage.AdvSelectedHistorySaveData
struct AdvSelectedHistorySaveData_t4269445723;
// Utage.AdvGallerySaveData
struct AdvGallerySaveData_t1012686890;
// System.Collections.Generic.List`1<Utage.AdvRenderTextureSpace>
struct List_1_t2851308344;
// Utage.AdvConfigSaveData
struct AdvConfigSaveData_t4190953152;
// Utage.AdvPageEvent
struct AdvPageEvent_t790047568;
// Utage.AdvScenarioPageData
struct AdvScenarioPageData_t3333166790;
// Utage.AdvCommandText
struct AdvCommandText_t816833175;
// System.Collections.Generic.List`1<Utage.AdvCommandText>
struct List_1_t185954307;
// Utage.AdvCharacterInfo
struct AdvCharacterInfo_t1582765630;
// Utage.AdvPageController
struct AdvPageController_t1125789268;
// Utage.BacklogEvent
struct BacklogEvent_t3497267401;
// System.Collections.Generic.List`1<Utage.AdvBacklog>
struct List_1_t3296422422;
// UnityEngine.Animator
struct Animator_t69676727;
// UnityEngine.UI.RawImage
struct RawImage_t2749640213;
// Utage.AssetFileReference
struct AssetFileReference_t4090268667;
// Utage.DicingImage
struct DicingImage_t2721298607;
// Utage.EyeBlinkDicing
struct EyeBlinkDicing_t891209267;
// Utage.LipSynchDicing
struct LipSynchDicing_t1869722688;
// Utage.AdvAnimationPlayer
struct AdvAnimationPlayer_t1530269518;
// Utage.AvatarImage
struct AvatarImage_t1946614104;
// Utage.EyeBlinkAvatar
struct EyeBlinkAvatar_t3311504802;
// Utage.LipSynchAvatar
struct LipSynchAvatar_t872933325;
// UnityEngine.CanvasGroup
struct CanvasGroup_t3296560743;
// UnityEngine.ParticleSystem[]
struct ParticleSystemU5BU5D_t1490986844;
// UnityEngine.SpriteRenderer
struct SpriteRenderer_t1209076198;
// UnityEngine.Video.VideoClip
struct VideoClip_t3831376279;




#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
#ifndef ADVJUMPMANAGER_T2493327610_H
#define ADVJUMPMANAGER_T2493327610_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.AdvJumpManager
struct  AdvJumpManager_t2493327610  : public RuntimeObject
{
public:
	// System.String Utage.AdvJumpManager::<Label>k__BackingField
	String_t* ___U3CLabelU3Ek__BackingField_0;
	// Utage.SubRoutineInfo Utage.AdvJumpManager::<SubRoutineReturnInfo>k__BackingField
	SubRoutineInfo_t2697101720 * ___U3CSubRoutineReturnInfoU3Ek__BackingField_1;
	// System.Collections.Generic.Stack`1<Utage.SubRoutineInfo> Utage.AdvJumpManager::subRoutineCallStack
	Stack_1_t3784829874 * ___subRoutineCallStack_2;
	// System.Collections.Generic.List`1<Utage.AdvJumpManager/RandomInfo> Utage.AdvJumpManager::randomInfoList
	List_1_t3145030736 * ___randomInfoList_3;

public:
	inline static int32_t get_offset_of_U3CLabelU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(AdvJumpManager_t2493327610, ___U3CLabelU3Ek__BackingField_0)); }
	inline String_t* get_U3CLabelU3Ek__BackingField_0() const { return ___U3CLabelU3Ek__BackingField_0; }
	inline String_t** get_address_of_U3CLabelU3Ek__BackingField_0() { return &___U3CLabelU3Ek__BackingField_0; }
	inline void set_U3CLabelU3Ek__BackingField_0(String_t* value)
	{
		___U3CLabelU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CLabelU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_U3CSubRoutineReturnInfoU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(AdvJumpManager_t2493327610, ___U3CSubRoutineReturnInfoU3Ek__BackingField_1)); }
	inline SubRoutineInfo_t2697101720 * get_U3CSubRoutineReturnInfoU3Ek__BackingField_1() const { return ___U3CSubRoutineReturnInfoU3Ek__BackingField_1; }
	inline SubRoutineInfo_t2697101720 ** get_address_of_U3CSubRoutineReturnInfoU3Ek__BackingField_1() { return &___U3CSubRoutineReturnInfoU3Ek__BackingField_1; }
	inline void set_U3CSubRoutineReturnInfoU3Ek__BackingField_1(SubRoutineInfo_t2697101720 * value)
	{
		___U3CSubRoutineReturnInfoU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CSubRoutineReturnInfoU3Ek__BackingField_1), value);
	}

	inline static int32_t get_offset_of_subRoutineCallStack_2() { return static_cast<int32_t>(offsetof(AdvJumpManager_t2493327610, ___subRoutineCallStack_2)); }
	inline Stack_1_t3784829874 * get_subRoutineCallStack_2() const { return ___subRoutineCallStack_2; }
	inline Stack_1_t3784829874 ** get_address_of_subRoutineCallStack_2() { return &___subRoutineCallStack_2; }
	inline void set_subRoutineCallStack_2(Stack_1_t3784829874 * value)
	{
		___subRoutineCallStack_2 = value;
		Il2CppCodeGenWriteBarrier((&___subRoutineCallStack_2), value);
	}

	inline static int32_t get_offset_of_randomInfoList_3() { return static_cast<int32_t>(offsetof(AdvJumpManager_t2493327610, ___randomInfoList_3)); }
	inline List_1_t3145030736 * get_randomInfoList_3() const { return ___randomInfoList_3; }
	inline List_1_t3145030736 ** get_address_of_randomInfoList_3() { return &___randomInfoList_3; }
	inline void set_randomInfoList_3(List_1_t3145030736 * value)
	{
		___randomInfoList_3 = value;
		Il2CppCodeGenWriteBarrier((&___randomInfoList_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ADVJUMPMANAGER_T2493327610_H
#ifndef U3CSTARTCROSSFADEIMAGEU3EC__ANONSTOREY0_T2540212446_H
#define U3CSTARTCROSSFADEIMAGEU3EC__ANONSTOREY0_T2540212446_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.AdvGraphicObjectRawImage/<StartCrossFadeImage>c__AnonStorey0
struct  U3CStartCrossFadeImageU3Ec__AnonStorey0_t2540212446  : public RuntimeObject
{
public:
	// Utage.UguiCrossFadeRawImage Utage.AdvGraphicObjectRawImage/<StartCrossFadeImage>c__AnonStorey0::crossFade
	UguiCrossFadeRawImage_t1509295035 * ___crossFade_0;
	// Utage.AdvGraphicObjectRawImage Utage.AdvGraphicObjectRawImage/<StartCrossFadeImage>c__AnonStorey0::$this
	AdvGraphicObjectRawImage_t1741698045 * ___U24this_1;

public:
	inline static int32_t get_offset_of_crossFade_0() { return static_cast<int32_t>(offsetof(U3CStartCrossFadeImageU3Ec__AnonStorey0_t2540212446, ___crossFade_0)); }
	inline UguiCrossFadeRawImage_t1509295035 * get_crossFade_0() const { return ___crossFade_0; }
	inline UguiCrossFadeRawImage_t1509295035 ** get_address_of_crossFade_0() { return &___crossFade_0; }
	inline void set_crossFade_0(UguiCrossFadeRawImage_t1509295035 * value)
	{
		___crossFade_0 = value;
		Il2CppCodeGenWriteBarrier((&___crossFade_0), value);
	}

	inline static int32_t get_offset_of_U24this_1() { return static_cast<int32_t>(offsetof(U3CStartCrossFadeImageU3Ec__AnonStorey0_t2540212446, ___U24this_1)); }
	inline AdvGraphicObjectRawImage_t1741698045 * get_U24this_1() const { return ___U24this_1; }
	inline AdvGraphicObjectRawImage_t1741698045 ** get_address_of_U24this_1() { return &___U24this_1; }
	inline void set_U24this_1(AdvGraphicObjectRawImage_t1741698045 * value)
	{
		___U24this_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CSTARTCROSSFADEIMAGEU3EC__ANONSTOREY0_T2540212446_H
#ifndef SUBROUTINEINFO_T2697101720_H
#define SUBROUTINEINFO_T2697101720_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.SubRoutineInfo
struct  SubRoutineInfo_t2697101720  : public RuntimeObject
{
public:
	// System.IO.BinaryReader Utage.SubRoutineInfo::reader
	BinaryReader_t2491843768 * ___reader_0;
	// System.String Utage.SubRoutineInfo::<ReturnLabel>k__BackingField
	String_t* ___U3CReturnLabelU3Ek__BackingField_1;
	// System.Int32 Utage.SubRoutineInfo::<ReturnPageNo>k__BackingField
	int32_t ___U3CReturnPageNoU3Ek__BackingField_2;
	// Utage.AdvCommand Utage.SubRoutineInfo::<ReturnCommand>k__BackingField
	AdvCommand_t2859960984 * ___U3CReturnCommandU3Ek__BackingField_3;
	// System.String Utage.SubRoutineInfo::<JumpLabel>k__BackingField
	String_t* ___U3CJumpLabelU3Ek__BackingField_4;
	// System.String Utage.SubRoutineInfo::<CalledLabel>k__BackingField
	String_t* ___U3CCalledLabelU3Ek__BackingField_5;
	// System.Int32 Utage.SubRoutineInfo::<CalledSubroutineCommandIndex>k__BackingField
	int32_t ___U3CCalledSubroutineCommandIndexU3Ek__BackingField_6;

public:
	inline static int32_t get_offset_of_reader_0() { return static_cast<int32_t>(offsetof(SubRoutineInfo_t2697101720, ___reader_0)); }
	inline BinaryReader_t2491843768 * get_reader_0() const { return ___reader_0; }
	inline BinaryReader_t2491843768 ** get_address_of_reader_0() { return &___reader_0; }
	inline void set_reader_0(BinaryReader_t2491843768 * value)
	{
		___reader_0 = value;
		Il2CppCodeGenWriteBarrier((&___reader_0), value);
	}

	inline static int32_t get_offset_of_U3CReturnLabelU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(SubRoutineInfo_t2697101720, ___U3CReturnLabelU3Ek__BackingField_1)); }
	inline String_t* get_U3CReturnLabelU3Ek__BackingField_1() const { return ___U3CReturnLabelU3Ek__BackingField_1; }
	inline String_t** get_address_of_U3CReturnLabelU3Ek__BackingField_1() { return &___U3CReturnLabelU3Ek__BackingField_1; }
	inline void set_U3CReturnLabelU3Ek__BackingField_1(String_t* value)
	{
		___U3CReturnLabelU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CReturnLabelU3Ek__BackingField_1), value);
	}

	inline static int32_t get_offset_of_U3CReturnPageNoU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(SubRoutineInfo_t2697101720, ___U3CReturnPageNoU3Ek__BackingField_2)); }
	inline int32_t get_U3CReturnPageNoU3Ek__BackingField_2() const { return ___U3CReturnPageNoU3Ek__BackingField_2; }
	inline int32_t* get_address_of_U3CReturnPageNoU3Ek__BackingField_2() { return &___U3CReturnPageNoU3Ek__BackingField_2; }
	inline void set_U3CReturnPageNoU3Ek__BackingField_2(int32_t value)
	{
		___U3CReturnPageNoU3Ek__BackingField_2 = value;
	}

	inline static int32_t get_offset_of_U3CReturnCommandU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(SubRoutineInfo_t2697101720, ___U3CReturnCommandU3Ek__BackingField_3)); }
	inline AdvCommand_t2859960984 * get_U3CReturnCommandU3Ek__BackingField_3() const { return ___U3CReturnCommandU3Ek__BackingField_3; }
	inline AdvCommand_t2859960984 ** get_address_of_U3CReturnCommandU3Ek__BackingField_3() { return &___U3CReturnCommandU3Ek__BackingField_3; }
	inline void set_U3CReturnCommandU3Ek__BackingField_3(AdvCommand_t2859960984 * value)
	{
		___U3CReturnCommandU3Ek__BackingField_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CReturnCommandU3Ek__BackingField_3), value);
	}

	inline static int32_t get_offset_of_U3CJumpLabelU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(SubRoutineInfo_t2697101720, ___U3CJumpLabelU3Ek__BackingField_4)); }
	inline String_t* get_U3CJumpLabelU3Ek__BackingField_4() const { return ___U3CJumpLabelU3Ek__BackingField_4; }
	inline String_t** get_address_of_U3CJumpLabelU3Ek__BackingField_4() { return &___U3CJumpLabelU3Ek__BackingField_4; }
	inline void set_U3CJumpLabelU3Ek__BackingField_4(String_t* value)
	{
		___U3CJumpLabelU3Ek__BackingField_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CJumpLabelU3Ek__BackingField_4), value);
	}

	inline static int32_t get_offset_of_U3CCalledLabelU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(SubRoutineInfo_t2697101720, ___U3CCalledLabelU3Ek__BackingField_5)); }
	inline String_t* get_U3CCalledLabelU3Ek__BackingField_5() const { return ___U3CCalledLabelU3Ek__BackingField_5; }
	inline String_t** get_address_of_U3CCalledLabelU3Ek__BackingField_5() { return &___U3CCalledLabelU3Ek__BackingField_5; }
	inline void set_U3CCalledLabelU3Ek__BackingField_5(String_t* value)
	{
		___U3CCalledLabelU3Ek__BackingField_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3CCalledLabelU3Ek__BackingField_5), value);
	}

	inline static int32_t get_offset_of_U3CCalledSubroutineCommandIndexU3Ek__BackingField_6() { return static_cast<int32_t>(offsetof(SubRoutineInfo_t2697101720, ___U3CCalledSubroutineCommandIndexU3Ek__BackingField_6)); }
	inline int32_t get_U3CCalledSubroutineCommandIndexU3Ek__BackingField_6() const { return ___U3CCalledSubroutineCommandIndexU3Ek__BackingField_6; }
	inline int32_t* get_address_of_U3CCalledSubroutineCommandIndexU3Ek__BackingField_6() { return &___U3CCalledSubroutineCommandIndexU3Ek__BackingField_6; }
	inline void set_U3CCalledSubroutineCommandIndexU3Ek__BackingField_6(int32_t value)
	{
		___U3CCalledSubroutineCommandIndexU3Ek__BackingField_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SUBROUTINEINFO_T2697101720_H
#ifndef ADVBACKLOGDATAINPAGE_T474952489_H
#define ADVBACKLOGDATAINPAGE_T474952489_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.AdvBacklog/AdvBacklogDataInPage
struct  AdvBacklogDataInPage_t474952489  : public RuntimeObject
{
public:
	// System.String Utage.AdvBacklog/AdvBacklogDataInPage::<LogText>k__BackingField
	String_t* ___U3CLogTextU3Ek__BackingField_0;
	// System.String Utage.AdvBacklog/AdvBacklogDataInPage::<CharacterLabel>k__BackingField
	String_t* ___U3CCharacterLabelU3Ek__BackingField_1;
	// System.String Utage.AdvBacklog/AdvBacklogDataInPage::<CharacterNameText>k__BackingField
	String_t* ___U3CCharacterNameTextU3Ek__BackingField_2;
	// System.String Utage.AdvBacklog/AdvBacklogDataInPage::<VoiceFileName>k__BackingField
	String_t* ___U3CVoiceFileNameU3Ek__BackingField_3;

public:
	inline static int32_t get_offset_of_U3CLogTextU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(AdvBacklogDataInPage_t474952489, ___U3CLogTextU3Ek__BackingField_0)); }
	inline String_t* get_U3CLogTextU3Ek__BackingField_0() const { return ___U3CLogTextU3Ek__BackingField_0; }
	inline String_t** get_address_of_U3CLogTextU3Ek__BackingField_0() { return &___U3CLogTextU3Ek__BackingField_0; }
	inline void set_U3CLogTextU3Ek__BackingField_0(String_t* value)
	{
		___U3CLogTextU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CLogTextU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_U3CCharacterLabelU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(AdvBacklogDataInPage_t474952489, ___U3CCharacterLabelU3Ek__BackingField_1)); }
	inline String_t* get_U3CCharacterLabelU3Ek__BackingField_1() const { return ___U3CCharacterLabelU3Ek__BackingField_1; }
	inline String_t** get_address_of_U3CCharacterLabelU3Ek__BackingField_1() { return &___U3CCharacterLabelU3Ek__BackingField_1; }
	inline void set_U3CCharacterLabelU3Ek__BackingField_1(String_t* value)
	{
		___U3CCharacterLabelU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CCharacterLabelU3Ek__BackingField_1), value);
	}

	inline static int32_t get_offset_of_U3CCharacterNameTextU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(AdvBacklogDataInPage_t474952489, ___U3CCharacterNameTextU3Ek__BackingField_2)); }
	inline String_t* get_U3CCharacterNameTextU3Ek__BackingField_2() const { return ___U3CCharacterNameTextU3Ek__BackingField_2; }
	inline String_t** get_address_of_U3CCharacterNameTextU3Ek__BackingField_2() { return &___U3CCharacterNameTextU3Ek__BackingField_2; }
	inline void set_U3CCharacterNameTextU3Ek__BackingField_2(String_t* value)
	{
		___U3CCharacterNameTextU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CCharacterNameTextU3Ek__BackingField_2), value);
	}

	inline static int32_t get_offset_of_U3CVoiceFileNameU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(AdvBacklogDataInPage_t474952489, ___U3CVoiceFileNameU3Ek__BackingField_3)); }
	inline String_t* get_U3CVoiceFileNameU3Ek__BackingField_3() const { return ___U3CVoiceFileNameU3Ek__BackingField_3; }
	inline String_t** get_address_of_U3CVoiceFileNameU3Ek__BackingField_3() { return &___U3CVoiceFileNameU3Ek__BackingField_3; }
	inline void set_U3CVoiceFileNameU3Ek__BackingField_3(String_t* value)
	{
		___U3CVoiceFileNameU3Ek__BackingField_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CVoiceFileNameU3Ek__BackingField_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ADVBACKLOGDATAINPAGE_T474952489_H
#ifndef ADVIFMANAGER_T1468978853_H
#define ADVIFMANAGER_T1468978853_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.AdvIfManager
struct  AdvIfManager_t1468978853  : public RuntimeObject
{
public:
	// System.Boolean Utage.AdvIfManager::isLoadInit
	bool ___isLoadInit_0;
	// Utage.AdvIfData Utage.AdvIfManager::current
	AdvIfData_t1807102644 * ___current_1;

public:
	inline static int32_t get_offset_of_isLoadInit_0() { return static_cast<int32_t>(offsetof(AdvIfManager_t1468978853, ___isLoadInit_0)); }
	inline bool get_isLoadInit_0() const { return ___isLoadInit_0; }
	inline bool* get_address_of_isLoadInit_0() { return &___isLoadInit_0; }
	inline void set_isLoadInit_0(bool value)
	{
		___isLoadInit_0 = value;
	}

	inline static int32_t get_offset_of_current_1() { return static_cast<int32_t>(offsetof(AdvIfManager_t1468978853, ___current_1)); }
	inline AdvIfData_t1807102644 * get_current_1() const { return ___current_1; }
	inline AdvIfData_t1807102644 ** get_address_of_current_1() { return &___current_1; }
	inline void set_current_1(AdvIfData_t1807102644 * value)
	{
		___current_1 = value;
		Il2CppCodeGenWriteBarrier((&___current_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ADVIFMANAGER_T1468978853_H
#ifndef ADVTRANSITIONARGS_T2412396169_H
#define ADVTRANSITIONARGS_T2412396169_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.AdvTransitionArgs
struct  AdvTransitionArgs_t2412396169  : public RuntimeObject
{
public:
	// System.String Utage.AdvTransitionArgs::<TextureName>k__BackingField
	String_t* ___U3CTextureNameU3Ek__BackingField_0;
	// System.Single Utage.AdvTransitionArgs::<Vague>k__BackingField
	float ___U3CVagueU3Ek__BackingField_1;
	// System.Single Utage.AdvTransitionArgs::<Time>k__BackingField
	float ___U3CTimeU3Ek__BackingField_2;

public:
	inline static int32_t get_offset_of_U3CTextureNameU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(AdvTransitionArgs_t2412396169, ___U3CTextureNameU3Ek__BackingField_0)); }
	inline String_t* get_U3CTextureNameU3Ek__BackingField_0() const { return ___U3CTextureNameU3Ek__BackingField_0; }
	inline String_t** get_address_of_U3CTextureNameU3Ek__BackingField_0() { return &___U3CTextureNameU3Ek__BackingField_0; }
	inline void set_U3CTextureNameU3Ek__BackingField_0(String_t* value)
	{
		___U3CTextureNameU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CTextureNameU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_U3CVagueU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(AdvTransitionArgs_t2412396169, ___U3CVagueU3Ek__BackingField_1)); }
	inline float get_U3CVagueU3Ek__BackingField_1() const { return ___U3CVagueU3Ek__BackingField_1; }
	inline float* get_address_of_U3CVagueU3Ek__BackingField_1() { return &___U3CVagueU3Ek__BackingField_1; }
	inline void set_U3CVagueU3Ek__BackingField_1(float value)
	{
		___U3CVagueU3Ek__BackingField_1 = value;
	}

	inline static int32_t get_offset_of_U3CTimeU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(AdvTransitionArgs_t2412396169, ___U3CTimeU3Ek__BackingField_2)); }
	inline float get_U3CTimeU3Ek__BackingField_2() const { return ___U3CTimeU3Ek__BackingField_2; }
	inline float* get_address_of_U3CTimeU3Ek__BackingField_2() { return &___U3CTimeU3Ek__BackingField_2; }
	inline void set_U3CTimeU3Ek__BackingField_2(float value)
	{
		___U3CTimeU3Ek__BackingField_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ADVTRANSITIONARGS_T2412396169_H
#ifndef ADVIFDATA_T1807102644_H
#define ADVIFDATA_T1807102644_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.AdvIfData
struct  AdvIfData_t1807102644  : public RuntimeObject
{
public:
	// Utage.AdvIfData Utage.AdvIfData::parent
	AdvIfData_t1807102644 * ___parent_0;
	// System.Boolean Utage.AdvIfData::isSkpping
	bool ___isSkpping_1;
	// System.Boolean Utage.AdvIfData::isIf
	bool ___isIf_2;

public:
	inline static int32_t get_offset_of_parent_0() { return static_cast<int32_t>(offsetof(AdvIfData_t1807102644, ___parent_0)); }
	inline AdvIfData_t1807102644 * get_parent_0() const { return ___parent_0; }
	inline AdvIfData_t1807102644 ** get_address_of_parent_0() { return &___parent_0; }
	inline void set_parent_0(AdvIfData_t1807102644 * value)
	{
		___parent_0 = value;
		Il2CppCodeGenWriteBarrier((&___parent_0), value);
	}

	inline static int32_t get_offset_of_isSkpping_1() { return static_cast<int32_t>(offsetof(AdvIfData_t1807102644, ___isSkpping_1)); }
	inline bool get_isSkpping_1() const { return ___isSkpping_1; }
	inline bool* get_address_of_isSkpping_1() { return &___isSkpping_1; }
	inline void set_isSkpping_1(bool value)
	{
		___isSkpping_1 = value;
	}

	inline static int32_t get_offset_of_isIf_2() { return static_cast<int32_t>(offsetof(AdvIfData_t1807102644, ___isIf_2)); }
	inline bool get_isIf_2() const { return ___isIf_2; }
	inline bool* get_address_of_isIf_2() { return &___isIf_2; }
	inline void set_isIf_2(bool value)
	{
		___isIf_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ADVIFDATA_T1807102644_H
#ifndef U3CTRYCREATECROSSFADEIMAGEU3EC__ANONSTOREY0_T2803660910_H
#define U3CTRYCREATECROSSFADEIMAGEU3EC__ANONSTOREY0_T2803660910_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.AdvGraphicObjectDicing/<TryCreateCrossFadeImage>c__AnonStorey0
struct  U3CTryCreateCrossFadeImageU3Ec__AnonStorey0_t2803660910  : public RuntimeObject
{
public:
	// Utage.UguiCrossFadeDicing Utage.AdvGraphicObjectDicing/<TryCreateCrossFadeImage>c__AnonStorey0::crossFade
	UguiCrossFadeDicing_t2659796554 * ___crossFade_0;

public:
	inline static int32_t get_offset_of_crossFade_0() { return static_cast<int32_t>(offsetof(U3CTryCreateCrossFadeImageU3Ec__AnonStorey0_t2803660910, ___crossFade_0)); }
	inline UguiCrossFadeDicing_t2659796554 * get_crossFade_0() const { return ___crossFade_0; }
	inline UguiCrossFadeDicing_t2659796554 ** get_address_of_crossFade_0() { return &___crossFade_0; }
	inline void set_crossFade_0(UguiCrossFadeDicing_t2659796554 * value)
	{
		___crossFade_0 = value;
		Il2CppCodeGenWriteBarrier((&___crossFade_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CTRYCREATECROSSFADEIMAGEU3EC__ANONSTOREY0_T2803660910_H
#ifndef U3CRULEFADEINU3EC__ANONSTOREY0_T2998002857_H
#define U3CRULEFADEINU3EC__ANONSTOREY0_T2998002857_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.AdvGraphicObjectRenderTextureImage/<RuleFadeIn>c__AnonStorey0
struct  U3CRuleFadeInU3Ec__AnonStorey0_t2998002857  : public RuntimeObject
{
public:
	// Utage.UguiTransition Utage.AdvGraphicObjectRenderTextureImage/<RuleFadeIn>c__AnonStorey0::transition
	UguiTransition_t3998485683 * ___transition_0;
	// System.Action Utage.AdvGraphicObjectRenderTextureImage/<RuleFadeIn>c__AnonStorey0::onComplete
	Action_t3226471752 * ___onComplete_1;

public:
	inline static int32_t get_offset_of_transition_0() { return static_cast<int32_t>(offsetof(U3CRuleFadeInU3Ec__AnonStorey0_t2998002857, ___transition_0)); }
	inline UguiTransition_t3998485683 * get_transition_0() const { return ___transition_0; }
	inline UguiTransition_t3998485683 ** get_address_of_transition_0() { return &___transition_0; }
	inline void set_transition_0(UguiTransition_t3998485683 * value)
	{
		___transition_0 = value;
		Il2CppCodeGenWriteBarrier((&___transition_0), value);
	}

	inline static int32_t get_offset_of_onComplete_1() { return static_cast<int32_t>(offsetof(U3CRuleFadeInU3Ec__AnonStorey0_t2998002857, ___onComplete_1)); }
	inline Action_t3226471752 * get_onComplete_1() const { return ___onComplete_1; }
	inline Action_t3226471752 ** get_address_of_onComplete_1() { return &___onComplete_1; }
	inline void set_onComplete_1(Action_t3226471752 * value)
	{
		___onComplete_1 = value;
		Il2CppCodeGenWriteBarrier((&___onComplete_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CRULEFADEINU3EC__ANONSTOREY0_T2998002857_H
#ifndef U3CRULEFADEOUTU3EC__ANONSTOREY1_T788669581_H
#define U3CRULEFADEOUTU3EC__ANONSTOREY1_T788669581_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.AdvGraphicObjectRenderTextureImage/<RuleFadeOut>c__AnonStorey1
struct  U3CRuleFadeOutU3Ec__AnonStorey1_t788669581  : public RuntimeObject
{
public:
	// Utage.UguiTransition Utage.AdvGraphicObjectRenderTextureImage/<RuleFadeOut>c__AnonStorey1::transition
	UguiTransition_t3998485683 * ___transition_0;
	// System.Action Utage.AdvGraphicObjectRenderTextureImage/<RuleFadeOut>c__AnonStorey1::onComplete
	Action_t3226471752 * ___onComplete_1;
	// Utage.AdvGraphicObjectRenderTextureImage Utage.AdvGraphicObjectRenderTextureImage/<RuleFadeOut>c__AnonStorey1::$this
	AdvGraphicObjectRenderTextureImage_t750627570 * ___U24this_2;

public:
	inline static int32_t get_offset_of_transition_0() { return static_cast<int32_t>(offsetof(U3CRuleFadeOutU3Ec__AnonStorey1_t788669581, ___transition_0)); }
	inline UguiTransition_t3998485683 * get_transition_0() const { return ___transition_0; }
	inline UguiTransition_t3998485683 ** get_address_of_transition_0() { return &___transition_0; }
	inline void set_transition_0(UguiTransition_t3998485683 * value)
	{
		___transition_0 = value;
		Il2CppCodeGenWriteBarrier((&___transition_0), value);
	}

	inline static int32_t get_offset_of_onComplete_1() { return static_cast<int32_t>(offsetof(U3CRuleFadeOutU3Ec__AnonStorey1_t788669581, ___onComplete_1)); }
	inline Action_t3226471752 * get_onComplete_1() const { return ___onComplete_1; }
	inline Action_t3226471752 ** get_address_of_onComplete_1() { return &___onComplete_1; }
	inline void set_onComplete_1(Action_t3226471752 * value)
	{
		___onComplete_1 = value;
		Il2CppCodeGenWriteBarrier((&___onComplete_1), value);
	}

	inline static int32_t get_offset_of_U24this_2() { return static_cast<int32_t>(offsetof(U3CRuleFadeOutU3Ec__AnonStorey1_t788669581, ___U24this_2)); }
	inline AdvGraphicObjectRenderTextureImage_t750627570 * get_U24this_2() const { return ___U24this_2; }
	inline AdvGraphicObjectRenderTextureImage_t750627570 ** get_address_of_U24this_2() { return &___U24this_2; }
	inline void set_U24this_2(AdvGraphicObjectRenderTextureImage_t750627570 * value)
	{
		___U24this_2 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CRULEFADEOUTU3EC__ANONSTOREY1_T788669581_H
#ifndef U3CTRYCREATECROSSFADEIMAGEU3EC__ANONSTOREY2_T559212944_H
#define U3CTRYCREATECROSSFADEIMAGEU3EC__ANONSTOREY2_T559212944_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.AdvGraphicObjectRenderTextureImage/<TryCreateCrossFadeImage>c__AnonStorey2
struct  U3CTryCreateCrossFadeImageU3Ec__AnonStorey2_t559212944  : public RuntimeObject
{
public:
	// Utage.UguiCrossFadeRawImage Utage.AdvGraphicObjectRenderTextureImage/<TryCreateCrossFadeImage>c__AnonStorey2::crossFade
	UguiCrossFadeRawImage_t1509295035 * ___crossFade_0;
	// Utage.AdvGraphicObjectRenderTextureImage Utage.AdvGraphicObjectRenderTextureImage/<TryCreateCrossFadeImage>c__AnonStorey2::$this
	AdvGraphicObjectRenderTextureImage_t750627570 * ___U24this_1;

public:
	inline static int32_t get_offset_of_crossFade_0() { return static_cast<int32_t>(offsetof(U3CTryCreateCrossFadeImageU3Ec__AnonStorey2_t559212944, ___crossFade_0)); }
	inline UguiCrossFadeRawImage_t1509295035 * get_crossFade_0() const { return ___crossFade_0; }
	inline UguiCrossFadeRawImage_t1509295035 ** get_address_of_crossFade_0() { return &___crossFade_0; }
	inline void set_crossFade_0(UguiCrossFadeRawImage_t1509295035 * value)
	{
		___crossFade_0 = value;
		Il2CppCodeGenWriteBarrier((&___crossFade_0), value);
	}

	inline static int32_t get_offset_of_U24this_1() { return static_cast<int32_t>(offsetof(U3CTryCreateCrossFadeImageU3Ec__AnonStorey2_t559212944, ___U24this_1)); }
	inline AdvGraphicObjectRenderTextureImage_t750627570 * get_U24this_1() const { return ___U24this_1; }
	inline AdvGraphicObjectRenderTextureImage_t750627570 ** get_address_of_U24this_1() { return &___U24this_1; }
	inline void set_U24this_1(AdvGraphicObjectRenderTextureImage_t750627570 * value)
	{
		___U24this_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CTRYCREATECROSSFADEIMAGEU3EC__ANONSTOREY2_T559212944_H
#ifndef ADVMACRODATA_T2548537639_H
#define ADVMACRODATA_T2548537639_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.AdvMacroData
struct  AdvMacroData_t2548537639  : public RuntimeObject
{
public:
	// System.String Utage.AdvMacroData::<Name>k__BackingField
	String_t* ___U3CNameU3Ek__BackingField_0;
	// Utage.StringGridRow Utage.AdvMacroData::<Header>k__BackingField
	StringGridRow_t4193237197 * ___U3CHeaderU3Ek__BackingField_1;
	// System.Collections.Generic.List`1<Utage.StringGridRow> Utage.AdvMacroData::<DataList>k__BackingField
	List_1_t3562358329 * ___U3CDataListU3Ek__BackingField_2;

public:
	inline static int32_t get_offset_of_U3CNameU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(AdvMacroData_t2548537639, ___U3CNameU3Ek__BackingField_0)); }
	inline String_t* get_U3CNameU3Ek__BackingField_0() const { return ___U3CNameU3Ek__BackingField_0; }
	inline String_t** get_address_of_U3CNameU3Ek__BackingField_0() { return &___U3CNameU3Ek__BackingField_0; }
	inline void set_U3CNameU3Ek__BackingField_0(String_t* value)
	{
		___U3CNameU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CNameU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_U3CHeaderU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(AdvMacroData_t2548537639, ___U3CHeaderU3Ek__BackingField_1)); }
	inline StringGridRow_t4193237197 * get_U3CHeaderU3Ek__BackingField_1() const { return ___U3CHeaderU3Ek__BackingField_1; }
	inline StringGridRow_t4193237197 ** get_address_of_U3CHeaderU3Ek__BackingField_1() { return &___U3CHeaderU3Ek__BackingField_1; }
	inline void set_U3CHeaderU3Ek__BackingField_1(StringGridRow_t4193237197 * value)
	{
		___U3CHeaderU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CHeaderU3Ek__BackingField_1), value);
	}

	inline static int32_t get_offset_of_U3CDataListU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(AdvMacroData_t2548537639, ___U3CDataListU3Ek__BackingField_2)); }
	inline List_1_t3562358329 * get_U3CDataListU3Ek__BackingField_2() const { return ___U3CDataListU3Ek__BackingField_2; }
	inline List_1_t3562358329 ** get_address_of_U3CDataListU3Ek__BackingField_2() { return &___U3CDataListU3Ek__BackingField_2; }
	inline void set_U3CDataListU3Ek__BackingField_2(List_1_t3562358329 * value)
	{
		___U3CDataListU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CDataListU3Ek__BackingField_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ADVMACRODATA_T2548537639_H
#ifndef U3CGETRANDOMJUMPCOMMANDU3EC__ANONSTOREY0_T4280582410_H
#define U3CGETRANDOMJUMPCOMMANDU3EC__ANONSTOREY0_T4280582410_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.AdvJumpManager/<GetRandomJumpCommand>c__AnonStorey0
struct  U3CGetRandomJumpCommandU3Ec__AnonStorey0_t4280582410  : public RuntimeObject
{
public:
	// System.Single Utage.AdvJumpManager/<GetRandomJumpCommand>c__AnonStorey0::sum
	float ___sum_0;

public:
	inline static int32_t get_offset_of_sum_0() { return static_cast<int32_t>(offsetof(U3CGetRandomJumpCommandU3Ec__AnonStorey0_t4280582410, ___sum_0)); }
	inline float get_sum_0() const { return ___sum_0; }
	inline float* get_address_of_sum_0() { return &___sum_0; }
	inline void set_sum_0(float value)
	{
		___sum_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CGETRANDOMJUMPCOMMANDU3EC__ANONSTOREY0_T4280582410_H
#ifndef RANDOMINFO_T3775909604_H
#define RANDOMINFO_T3775909604_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.AdvJumpManager/RandomInfo
struct  RandomInfo_t3775909604  : public RuntimeObject
{
public:
	// Utage.AdvCommand Utage.AdvJumpManager/RandomInfo::command
	AdvCommand_t2859960984 * ___command_0;
	// System.Single Utage.AdvJumpManager/RandomInfo::rate
	float ___rate_1;

public:
	inline static int32_t get_offset_of_command_0() { return static_cast<int32_t>(offsetof(RandomInfo_t3775909604, ___command_0)); }
	inline AdvCommand_t2859960984 * get_command_0() const { return ___command_0; }
	inline AdvCommand_t2859960984 ** get_address_of_command_0() { return &___command_0; }
	inline void set_command_0(AdvCommand_t2859960984 * value)
	{
		___command_0 = value;
		Il2CppCodeGenWriteBarrier((&___command_0), value);
	}

	inline static int32_t get_offset_of_rate_1() { return static_cast<int32_t>(offsetof(RandomInfo_t3775909604, ___rate_1)); }
	inline float get_rate_1() const { return ___rate_1; }
	inline float* get_address_of_rate_1() { return &___rate_1; }
	inline void set_rate_1(float value)
	{
		___rate_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RANDOMINFO_T3775909604_H
#ifndef VIDEOINFO_T2935460087_H
#define VIDEOINFO_T2935460087_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.AdvVideoManager/VideoInfo
struct  VideoInfo_t2935460087  : public RuntimeObject
{
public:
	// System.Boolean Utage.AdvVideoManager/VideoInfo::<Cancel>k__BackingField
	bool ___U3CCancelU3Ek__BackingField_0;
	// System.Boolean Utage.AdvVideoManager/VideoInfo::<Started>k__BackingField
	bool ___U3CStartedU3Ek__BackingField_1;
	// System.Boolean Utage.AdvVideoManager/VideoInfo::<Canceled>k__BackingField
	bool ___U3CCanceledU3Ek__BackingField_2;
	// UnityEngine.Video.VideoPlayer Utage.AdvVideoManager/VideoInfo::<Player>k__BackingField
	VideoPlayer_t10059812 * ___U3CPlayerU3Ek__BackingField_3;

public:
	inline static int32_t get_offset_of_U3CCancelU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(VideoInfo_t2935460087, ___U3CCancelU3Ek__BackingField_0)); }
	inline bool get_U3CCancelU3Ek__BackingField_0() const { return ___U3CCancelU3Ek__BackingField_0; }
	inline bool* get_address_of_U3CCancelU3Ek__BackingField_0() { return &___U3CCancelU3Ek__BackingField_0; }
	inline void set_U3CCancelU3Ek__BackingField_0(bool value)
	{
		___U3CCancelU3Ek__BackingField_0 = value;
	}

	inline static int32_t get_offset_of_U3CStartedU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(VideoInfo_t2935460087, ___U3CStartedU3Ek__BackingField_1)); }
	inline bool get_U3CStartedU3Ek__BackingField_1() const { return ___U3CStartedU3Ek__BackingField_1; }
	inline bool* get_address_of_U3CStartedU3Ek__BackingField_1() { return &___U3CStartedU3Ek__BackingField_1; }
	inline void set_U3CStartedU3Ek__BackingField_1(bool value)
	{
		___U3CStartedU3Ek__BackingField_1 = value;
	}

	inline static int32_t get_offset_of_U3CCanceledU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(VideoInfo_t2935460087, ___U3CCanceledU3Ek__BackingField_2)); }
	inline bool get_U3CCanceledU3Ek__BackingField_2() const { return ___U3CCanceledU3Ek__BackingField_2; }
	inline bool* get_address_of_U3CCanceledU3Ek__BackingField_2() { return &___U3CCanceledU3Ek__BackingField_2; }
	inline void set_U3CCanceledU3Ek__BackingField_2(bool value)
	{
		___U3CCanceledU3Ek__BackingField_2 = value;
	}

	inline static int32_t get_offset_of_U3CPlayerU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(VideoInfo_t2935460087, ___U3CPlayerU3Ek__BackingField_3)); }
	inline VideoPlayer_t10059812 * get_U3CPlayerU3Ek__BackingField_3() const { return ___U3CPlayerU3Ek__BackingField_3; }
	inline VideoPlayer_t10059812 ** get_address_of_U3CPlayerU3Ek__BackingField_3() { return &___U3CPlayerU3Ek__BackingField_3; }
	inline void set_U3CPlayerU3Ek__BackingField_3(VideoPlayer_t10059812 * value)
	{
		___U3CPlayerU3Ek__BackingField_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CPlayerU3Ek__BackingField_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VIDEOINFO_T2935460087_H
#ifndef ADVBACKLOG_T3927301290_H
#define ADVBACKLOG_T3927301290_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.AdvBacklog
struct  AdvBacklog_t3927301290  : public RuntimeObject
{
public:
	// System.Collections.Generic.List`1<Utage.AdvBacklog/AdvBacklogDataInPage> Utage.AdvBacklog::dataList
	List_1_t4139040917 * ___dataList_0;

public:
	inline static int32_t get_offset_of_dataList_0() { return static_cast<int32_t>(offsetof(AdvBacklog_t3927301290, ___dataList_0)); }
	inline List_1_t4139040917 * get_dataList_0() const { return ___dataList_0; }
	inline List_1_t4139040917 ** get_address_of_dataList_0() { return &___dataList_0; }
	inline void set_dataList_0(List_1_t4139040917 * value)
	{
		___dataList_0 = value;
		Il2CppCodeGenWriteBarrier((&___dataList_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ADVBACKLOG_T3927301290_H
#ifndef ADVREADHISTORYSAVEDATA_T361293356_H
#define ADVREADHISTORYSAVEDATA_T361293356_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.AdvReadHistorySaveData
struct  AdvReadHistorySaveData_t361293356  : public RuntimeObject
{
public:
	// Utage.DictionaryInt Utage.AdvReadHistorySaveData::data
	DictionaryInt_t3731914685 * ___data_0;

public:
	inline static int32_t get_offset_of_data_0() { return static_cast<int32_t>(offsetof(AdvReadHistorySaveData_t361293356, ___data_0)); }
	inline DictionaryInt_t3731914685 * get_data_0() const { return ___data_0; }
	inline DictionaryInt_t3731914685 ** get_address_of_data_0() { return &___data_0; }
	inline void set_data_0(DictionaryInt_t3731914685 * value)
	{
		___data_0 = value;
		Il2CppCodeGenWriteBarrier((&___data_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ADVREADHISTORYSAVEDATA_T361293356_H
#ifndef ADVGALLERYSAVEDATA_T1012686890_H
#define ADVGALLERYSAVEDATA_T1012686890_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.AdvGallerySaveData
struct  AdvGallerySaveData_t1012686890  : public RuntimeObject
{
public:
	// System.Collections.Generic.List`1<System.String> Utage.AdvGallerySaveData::eventSceneLabels
	List_1_t1398341365 * ___eventSceneLabels_0;
	// System.Collections.Generic.List`1<System.String> Utage.AdvGallerySaveData::eventCGLabels
	List_1_t1398341365 * ___eventCGLabels_1;

public:
	inline static int32_t get_offset_of_eventSceneLabels_0() { return static_cast<int32_t>(offsetof(AdvGallerySaveData_t1012686890, ___eventSceneLabels_0)); }
	inline List_1_t1398341365 * get_eventSceneLabels_0() const { return ___eventSceneLabels_0; }
	inline List_1_t1398341365 ** get_address_of_eventSceneLabels_0() { return &___eventSceneLabels_0; }
	inline void set_eventSceneLabels_0(List_1_t1398341365 * value)
	{
		___eventSceneLabels_0 = value;
		Il2CppCodeGenWriteBarrier((&___eventSceneLabels_0), value);
	}

	inline static int32_t get_offset_of_eventCGLabels_1() { return static_cast<int32_t>(offsetof(AdvGallerySaveData_t1012686890, ___eventCGLabels_1)); }
	inline List_1_t1398341365 * get_eventCGLabels_1() const { return ___eventCGLabels_1; }
	inline List_1_t1398341365 ** get_address_of_eventCGLabels_1() { return &___eventCGLabels_1; }
	inline void set_eventCGLabels_1(List_1_t1398341365 * value)
	{
		___eventCGLabels_1 = value;
		Il2CppCodeGenWriteBarrier((&___eventCGLabels_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ADVGALLERYSAVEDATA_T1012686890_H
#ifndef ADVMESSAGEWINDOW_T3415422374_H
#define ADVMESSAGEWINDOW_T3415422374_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.AdvMessageWindow
struct  AdvMessageWindow_t3415422374  : public RuntimeObject
{
public:
	// System.String Utage.AdvMessageWindow::<Name>k__BackingField
	String_t* ___U3CNameU3Ek__BackingField_0;
	// Utage.TextData Utage.AdvMessageWindow::<Text>k__BackingField
	TextData_t603454315 * ___U3CTextU3Ek__BackingField_1;
	// System.String Utage.AdvMessageWindow::<NameText>k__BackingField
	String_t* ___U3CNameTextU3Ek__BackingField_2;
	// System.String Utage.AdvMessageWindow::<CharacterLabel>k__BackingField
	String_t* ___U3CCharacterLabelU3Ek__BackingField_3;
	// System.Int32 Utage.AdvMessageWindow::<TextLength>k__BackingField
	int32_t ___U3CTextLengthU3Ek__BackingField_4;
	// IAdvMessageWindow Utage.AdvMessageWindow::<MessageWindow>k__BackingField
	RuntimeObject* ___U3CMessageWindowU3Ek__BackingField_5;

public:
	inline static int32_t get_offset_of_U3CNameU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(AdvMessageWindow_t3415422374, ___U3CNameU3Ek__BackingField_0)); }
	inline String_t* get_U3CNameU3Ek__BackingField_0() const { return ___U3CNameU3Ek__BackingField_0; }
	inline String_t** get_address_of_U3CNameU3Ek__BackingField_0() { return &___U3CNameU3Ek__BackingField_0; }
	inline void set_U3CNameU3Ek__BackingField_0(String_t* value)
	{
		___U3CNameU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CNameU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_U3CTextU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(AdvMessageWindow_t3415422374, ___U3CTextU3Ek__BackingField_1)); }
	inline TextData_t603454315 * get_U3CTextU3Ek__BackingField_1() const { return ___U3CTextU3Ek__BackingField_1; }
	inline TextData_t603454315 ** get_address_of_U3CTextU3Ek__BackingField_1() { return &___U3CTextU3Ek__BackingField_1; }
	inline void set_U3CTextU3Ek__BackingField_1(TextData_t603454315 * value)
	{
		___U3CTextU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CTextU3Ek__BackingField_1), value);
	}

	inline static int32_t get_offset_of_U3CNameTextU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(AdvMessageWindow_t3415422374, ___U3CNameTextU3Ek__BackingField_2)); }
	inline String_t* get_U3CNameTextU3Ek__BackingField_2() const { return ___U3CNameTextU3Ek__BackingField_2; }
	inline String_t** get_address_of_U3CNameTextU3Ek__BackingField_2() { return &___U3CNameTextU3Ek__BackingField_2; }
	inline void set_U3CNameTextU3Ek__BackingField_2(String_t* value)
	{
		___U3CNameTextU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CNameTextU3Ek__BackingField_2), value);
	}

	inline static int32_t get_offset_of_U3CCharacterLabelU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(AdvMessageWindow_t3415422374, ___U3CCharacterLabelU3Ek__BackingField_3)); }
	inline String_t* get_U3CCharacterLabelU3Ek__BackingField_3() const { return ___U3CCharacterLabelU3Ek__BackingField_3; }
	inline String_t** get_address_of_U3CCharacterLabelU3Ek__BackingField_3() { return &___U3CCharacterLabelU3Ek__BackingField_3; }
	inline void set_U3CCharacterLabelU3Ek__BackingField_3(String_t* value)
	{
		___U3CCharacterLabelU3Ek__BackingField_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CCharacterLabelU3Ek__BackingField_3), value);
	}

	inline static int32_t get_offset_of_U3CTextLengthU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(AdvMessageWindow_t3415422374, ___U3CTextLengthU3Ek__BackingField_4)); }
	inline int32_t get_U3CTextLengthU3Ek__BackingField_4() const { return ___U3CTextLengthU3Ek__BackingField_4; }
	inline int32_t* get_address_of_U3CTextLengthU3Ek__BackingField_4() { return &___U3CTextLengthU3Ek__BackingField_4; }
	inline void set_U3CTextLengthU3Ek__BackingField_4(int32_t value)
	{
		___U3CTextLengthU3Ek__BackingField_4 = value;
	}

	inline static int32_t get_offset_of_U3CMessageWindowU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(AdvMessageWindow_t3415422374, ___U3CMessageWindowU3Ek__BackingField_5)); }
	inline RuntimeObject* get_U3CMessageWindowU3Ek__BackingField_5() const { return ___U3CMessageWindowU3Ek__BackingField_5; }
	inline RuntimeObject** get_address_of_U3CMessageWindowU3Ek__BackingField_5() { return &___U3CMessageWindowU3Ek__BackingField_5; }
	inline void set_U3CMessageWindowU3Ek__BackingField_5(RuntimeObject* value)
	{
		___U3CMessageWindowU3Ek__BackingField_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3CMessageWindowU3Ek__BackingField_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ADVMESSAGEWINDOW_T3415422374_H
#ifndef U3CTRYGETTAGGEDMASTERVOLUMEU3EC__ANONSTOREY1_T3670310314_H
#define U3CTRYGETTAGGEDMASTERVOLUMEU3EC__ANONSTOREY1_T3670310314_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.AdvConfigSaveData/<TryGetTaggedMasterVolume>c__AnonStorey1
struct  U3CTryGetTaggedMasterVolumeU3Ec__AnonStorey1_t3670310314  : public RuntimeObject
{
public:
	// System.String Utage.AdvConfigSaveData/<TryGetTaggedMasterVolume>c__AnonStorey1::tag
	String_t* ___tag_0;

public:
	inline static int32_t get_offset_of_tag_0() { return static_cast<int32_t>(offsetof(U3CTryGetTaggedMasterVolumeU3Ec__AnonStorey1_t3670310314, ___tag_0)); }
	inline String_t* get_tag_0() const { return ___tag_0; }
	inline String_t** get_address_of_tag_0() { return &___tag_0; }
	inline void set_tag_0(String_t* value)
	{
		___tag_0 = value;
		Il2CppCodeGenWriteBarrier((&___tag_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CTRYGETTAGGEDMASTERVOLUMEU3EC__ANONSTOREY1_T3670310314_H
#ifndef U3CSETTAGGEDMASTERVOLUMEU3EC__ANONSTOREY0_T876186946_H
#define U3CSETTAGGEDMASTERVOLUMEU3EC__ANONSTOREY0_T876186946_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.AdvConfigSaveData/<SetTaggedMasterVolume>c__AnonStorey0
struct  U3CSetTaggedMasterVolumeU3Ec__AnonStorey0_t876186946  : public RuntimeObject
{
public:
	// System.String Utage.AdvConfigSaveData/<SetTaggedMasterVolume>c__AnonStorey0::tag
	String_t* ___tag_0;

public:
	inline static int32_t get_offset_of_tag_0() { return static_cast<int32_t>(offsetof(U3CSetTaggedMasterVolumeU3Ec__AnonStorey0_t876186946, ___tag_0)); }
	inline String_t* get_tag_0() const { return ___tag_0; }
	inline String_t** get_address_of_tag_0() { return &___tag_0; }
	inline void set_tag_0(String_t* value)
	{
		___tag_0 = value;
		Il2CppCodeGenWriteBarrier((&___tag_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CSETTAGGEDMASTERVOLUMEU3EC__ANONSTOREY0_T876186946_H
#ifndef TAGGEDMASTERVOLUME_T1355152757_H
#define TAGGEDMASTERVOLUME_T1355152757_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.AdvConfigSaveData/TaggedMasterVolume
struct  TaggedMasterVolume_t1355152757  : public RuntimeObject
{
public:
	// System.String Utage.AdvConfigSaveData/TaggedMasterVolume::tag
	String_t* ___tag_0;
	// System.Single Utage.AdvConfigSaveData/TaggedMasterVolume::volume
	float ___volume_1;

public:
	inline static int32_t get_offset_of_tag_0() { return static_cast<int32_t>(offsetof(TaggedMasterVolume_t1355152757, ___tag_0)); }
	inline String_t* get_tag_0() const { return ___tag_0; }
	inline String_t** get_address_of_tag_0() { return &___tag_0; }
	inline void set_tag_0(String_t* value)
	{
		___tag_0 = value;
		Il2CppCodeGenWriteBarrier((&___tag_0), value);
	}

	inline static int32_t get_offset_of_volume_1() { return static_cast<int32_t>(offsetof(TaggedMasterVolume_t1355152757, ___volume_1)); }
	inline float get_volume_1() const { return ___volume_1; }
	inline float* get_address_of_volume_1() { return &___volume_1; }
	inline void set_volume_1(float value)
	{
		___volume_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TAGGEDMASTERVOLUME_T1355152757_H
#ifndef ADVPAGECONTROLLER_T1125789268_H
#define ADVPAGECONTROLLER_T1125789268_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.AdvPageController
struct  AdvPageController_t1125789268  : public RuntimeObject
{
public:
	// System.Boolean Utage.AdvPageController::<IsKeepText>k__BackingField
	bool ___U3CIsKeepTextU3Ek__BackingField_0;
	// System.Boolean Utage.AdvPageController::<IsWaitInput>k__BackingField
	bool ___U3CIsWaitInputU3Ek__BackingField_1;
	// System.Boolean Utage.AdvPageController::<IsBr>k__BackingField
	bool ___U3CIsBrU3Ek__BackingField_2;

public:
	inline static int32_t get_offset_of_U3CIsKeepTextU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(AdvPageController_t1125789268, ___U3CIsKeepTextU3Ek__BackingField_0)); }
	inline bool get_U3CIsKeepTextU3Ek__BackingField_0() const { return ___U3CIsKeepTextU3Ek__BackingField_0; }
	inline bool* get_address_of_U3CIsKeepTextU3Ek__BackingField_0() { return &___U3CIsKeepTextU3Ek__BackingField_0; }
	inline void set_U3CIsKeepTextU3Ek__BackingField_0(bool value)
	{
		___U3CIsKeepTextU3Ek__BackingField_0 = value;
	}

	inline static int32_t get_offset_of_U3CIsWaitInputU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(AdvPageController_t1125789268, ___U3CIsWaitInputU3Ek__BackingField_1)); }
	inline bool get_U3CIsWaitInputU3Ek__BackingField_1() const { return ___U3CIsWaitInputU3Ek__BackingField_1; }
	inline bool* get_address_of_U3CIsWaitInputU3Ek__BackingField_1() { return &___U3CIsWaitInputU3Ek__BackingField_1; }
	inline void set_U3CIsWaitInputU3Ek__BackingField_1(bool value)
	{
		___U3CIsWaitInputU3Ek__BackingField_1 = value;
	}

	inline static int32_t get_offset_of_U3CIsBrU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(AdvPageController_t1125789268, ___U3CIsBrU3Ek__BackingField_2)); }
	inline bool get_U3CIsBrU3Ek__BackingField_2() const { return ___U3CIsBrU3Ek__BackingField_2; }
	inline bool* get_address_of_U3CIsBrU3Ek__BackingField_2() { return &___U3CIsBrU3Ek__BackingField_2; }
	inline void set_U3CIsBrU3Ek__BackingField_2(bool value)
	{
		___U3CIsBrU3Ek__BackingField_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ADVPAGECONTROLLER_T1125789268_H
#ifndef U3CPLAYU3EC__ANONSTOREY0_T1875904553_H
#define U3CPLAYU3EC__ANONSTOREY0_T1875904553_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.AdvVideoManager/<Play>c__AnonStorey0
struct  U3CPlayU3Ec__AnonStorey0_t1875904553  : public RuntimeObject
{
public:
	// Utage.AdvVideoManager/VideoInfo Utage.AdvVideoManager/<Play>c__AnonStorey0::info
	VideoInfo_t2935460087 * ___info_0;
	// Utage.AdvVideoManager Utage.AdvVideoManager/<Play>c__AnonStorey0::$this
	AdvVideoManager_t946313091 * ___U24this_1;

public:
	inline static int32_t get_offset_of_info_0() { return static_cast<int32_t>(offsetof(U3CPlayU3Ec__AnonStorey0_t1875904553, ___info_0)); }
	inline VideoInfo_t2935460087 * get_info_0() const { return ___info_0; }
	inline VideoInfo_t2935460087 ** get_address_of_info_0() { return &___info_0; }
	inline void set_info_0(VideoInfo_t2935460087 * value)
	{
		___info_0 = value;
		Il2CppCodeGenWriteBarrier((&___info_0), value);
	}

	inline static int32_t get_offset_of_U24this_1() { return static_cast<int32_t>(offsetof(U3CPlayU3Ec__AnonStorey0_t1875904553, ___U24this_1)); }
	inline AdvVideoManager_t946313091 * get_U24this_1() const { return ___U24this_1; }
	inline AdvVideoManager_t946313091 ** get_address_of_U24this_1() { return &___U24this_1; }
	inline void set_U24this_1(AdvVideoManager_t946313091 * value)
	{
		___U24this_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CPLAYU3EC__ANONSTOREY0_T1875904553_H
#ifndef ADVENTITYDATA_T1465354496_H
#define ADVENTITYDATA_T1465354496_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.AdvEntityData
struct  AdvEntityData_t1465354496  : public RuntimeObject
{
public:
	// System.String[] Utage.AdvEntityData::originalStrings
	StringU5BU5D_t1642385972* ___originalStrings_0;

public:
	inline static int32_t get_offset_of_originalStrings_0() { return static_cast<int32_t>(offsetof(AdvEntityData_t1465354496, ___originalStrings_0)); }
	inline StringU5BU5D_t1642385972* get_originalStrings_0() const { return ___originalStrings_0; }
	inline StringU5BU5D_t1642385972** get_address_of_originalStrings_0() { return &___originalStrings_0; }
	inline void set_originalStrings_0(StringU5BU5D_t1642385972* value)
	{
		___originalStrings_0 = value;
		Il2CppCodeGenWriteBarrier((&___originalStrings_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ADVENTITYDATA_T1465354496_H
#ifndef LANGUAGEADVERRORMSG_T3894398370_H
#define LANGUAGEADVERRORMSG_T3894398370_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.LanguageAdvErrorMsg
struct  LanguageAdvErrorMsg_t3894398370  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LANGUAGEADVERRORMSG_T3894398370_H
#ifndef U3CCHECKU3EC__ANONSTOREY0_T2585708515_H
#define U3CCHECKU3EC__ANONSTOREY0_T2585708515_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.AdvSelectedHistorySaveData/<Check>c__AnonStorey0
struct  U3CCheckU3Ec__AnonStorey0_t2585708515  : public RuntimeObject
{
public:
	// Utage.AdvSelection Utage.AdvSelectedHistorySaveData/<Check>c__AnonStorey0::selection
	AdvSelection_t2301351953 * ___selection_0;

public:
	inline static int32_t get_offset_of_selection_0() { return static_cast<int32_t>(offsetof(U3CCheckU3Ec__AnonStorey0_t2585708515, ___selection_0)); }
	inline AdvSelection_t2301351953 * get_selection_0() const { return ___selection_0; }
	inline AdvSelection_t2301351953 ** get_address_of_selection_0() { return &___selection_0; }
	inline void set_selection_0(AdvSelection_t2301351953 * value)
	{
		___selection_0 = value;
		Il2CppCodeGenWriteBarrier((&___selection_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CCHECKU3EC__ANONSTOREY0_T2585708515_H
#ifndef ADVSELECTEDHISTORYDATA_T2288062212_H
#define ADVSELECTEDHISTORYDATA_T2288062212_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.AdvSelectedHistorySaveData/AdvSelectedHistoryData
struct  AdvSelectedHistoryData_t2288062212  : public RuntimeObject
{
public:
	// System.String Utage.AdvSelectedHistorySaveData/AdvSelectedHistoryData::<Label>k__BackingField
	String_t* ___U3CLabelU3Ek__BackingField_0;
	// System.String Utage.AdvSelectedHistorySaveData/AdvSelectedHistoryData::<Text>k__BackingField
	String_t* ___U3CTextU3Ek__BackingField_1;
	// System.String Utage.AdvSelectedHistorySaveData/AdvSelectedHistoryData::<JumpLabel>k__BackingField
	String_t* ___U3CJumpLabelU3Ek__BackingField_2;

public:
	inline static int32_t get_offset_of_U3CLabelU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(AdvSelectedHistoryData_t2288062212, ___U3CLabelU3Ek__BackingField_0)); }
	inline String_t* get_U3CLabelU3Ek__BackingField_0() const { return ___U3CLabelU3Ek__BackingField_0; }
	inline String_t** get_address_of_U3CLabelU3Ek__BackingField_0() { return &___U3CLabelU3Ek__BackingField_0; }
	inline void set_U3CLabelU3Ek__BackingField_0(String_t* value)
	{
		___U3CLabelU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CLabelU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_U3CTextU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(AdvSelectedHistoryData_t2288062212, ___U3CTextU3Ek__BackingField_1)); }
	inline String_t* get_U3CTextU3Ek__BackingField_1() const { return ___U3CTextU3Ek__BackingField_1; }
	inline String_t** get_address_of_U3CTextU3Ek__BackingField_1() { return &___U3CTextU3Ek__BackingField_1; }
	inline void set_U3CTextU3Ek__BackingField_1(String_t* value)
	{
		___U3CTextU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CTextU3Ek__BackingField_1), value);
	}

	inline static int32_t get_offset_of_U3CJumpLabelU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(AdvSelectedHistoryData_t2288062212, ___U3CJumpLabelU3Ek__BackingField_2)); }
	inline String_t* get_U3CJumpLabelU3Ek__BackingField_2() const { return ___U3CJumpLabelU3Ek__BackingField_2; }
	inline String_t** get_address_of_U3CJumpLabelU3Ek__BackingField_2() { return &___U3CJumpLabelU3Ek__BackingField_2; }
	inline void set_U3CJumpLabelU3Ek__BackingField_2(String_t* value)
	{
		___U3CJumpLabelU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CJumpLabelU3Ek__BackingField_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ADVSELECTEDHISTORYDATA_T2288062212_H
#ifndef ADVSELECTEDHISTORYSAVEDATA_T4269445723_H
#define ADVSELECTEDHISTORYSAVEDATA_T4269445723_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.AdvSelectedHistorySaveData
struct  AdvSelectedHistorySaveData_t4269445723  : public RuntimeObject
{
public:
	// System.Collections.Generic.List`1<Utage.AdvSelectedHistorySaveData/AdvSelectedHistoryData> Utage.AdvSelectedHistorySaveData::dataList
	List_1_t1657183344 * ___dataList_0;

public:
	inline static int32_t get_offset_of_dataList_0() { return static_cast<int32_t>(offsetof(AdvSelectedHistorySaveData_t4269445723, ___dataList_0)); }
	inline List_1_t1657183344 * get_dataList_0() const { return ___dataList_0; }
	inline List_1_t1657183344 ** get_address_of_dataList_0() { return &___dataList_0; }
	inline void set_dataList_0(List_1_t1657183344 * value)
	{
		___dataList_0 = value;
		Il2CppCodeGenWriteBarrier((&___dataList_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ADVSELECTEDHISTORYSAVEDATA_T4269445723_H
#ifndef SAVESETTING_T424301263_H
#define SAVESETTING_T424301263_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.AdvSaveManager/SaveSetting
struct  SaveSetting_t424301263  : public RuntimeObject
{
public:
	// System.Int32 Utage.AdvSaveManager/SaveSetting::captureWidth
	int32_t ___captureWidth_0;
	// System.Int32 Utage.AdvSaveManager/SaveSetting::captureHeight
	int32_t ___captureHeight_1;
	// System.Int32 Utage.AdvSaveManager/SaveSetting::saveMax
	int32_t ___saveMax_2;

public:
	inline static int32_t get_offset_of_captureWidth_0() { return static_cast<int32_t>(offsetof(SaveSetting_t424301263, ___captureWidth_0)); }
	inline int32_t get_captureWidth_0() const { return ___captureWidth_0; }
	inline int32_t* get_address_of_captureWidth_0() { return &___captureWidth_0; }
	inline void set_captureWidth_0(int32_t value)
	{
		___captureWidth_0 = value;
	}

	inline static int32_t get_offset_of_captureHeight_1() { return static_cast<int32_t>(offsetof(SaveSetting_t424301263, ___captureHeight_1)); }
	inline int32_t get_captureHeight_1() const { return ___captureHeight_1; }
	inline int32_t* get_address_of_captureHeight_1() { return &___captureHeight_1; }
	inline void set_captureHeight_1(int32_t value)
	{
		___captureHeight_1 = value;
	}

	inline static int32_t get_offset_of_saveMax_2() { return static_cast<int32_t>(offsetof(SaveSetting_t424301263, ___saveMax_2)); }
	inline int32_t get_saveMax_2() const { return ___saveMax_2; }
	inline int32_t* get_address_of_saveMax_2() { return &___saveMax_2; }
	inline void set_saveMax_2(int32_t value)
	{
		___saveMax_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SAVESETTING_T424301263_H
#ifndef VALUETYPE_T3507792607_H
#define VALUETYPE_T3507792607_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ValueType
struct  ValueType_t3507792607  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t3507792607_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t3507792607_marshaled_com
{
};
#endif // VALUETYPE_T3507792607_H
#ifndef U3CRULEFADEOUTU3EC__ANONSTOREY1_T4086381191_H
#define U3CRULEFADEOUTU3EC__ANONSTOREY1_T4086381191_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.AdvGraphicBase/<RuleFadeOut>c__AnonStorey1
struct  U3CRuleFadeOutU3Ec__AnonStorey1_t4086381191  : public RuntimeObject
{
public:
	// Utage.UguiTransition Utage.AdvGraphicBase/<RuleFadeOut>c__AnonStorey1::transition
	UguiTransition_t3998485683 * ___transition_0;
	// System.Action Utage.AdvGraphicBase/<RuleFadeOut>c__AnonStorey1::onComplete
	Action_t3226471752 * ___onComplete_1;

public:
	inline static int32_t get_offset_of_transition_0() { return static_cast<int32_t>(offsetof(U3CRuleFadeOutU3Ec__AnonStorey1_t4086381191, ___transition_0)); }
	inline UguiTransition_t3998485683 * get_transition_0() const { return ___transition_0; }
	inline UguiTransition_t3998485683 ** get_address_of_transition_0() { return &___transition_0; }
	inline void set_transition_0(UguiTransition_t3998485683 * value)
	{
		___transition_0 = value;
		Il2CppCodeGenWriteBarrier((&___transition_0), value);
	}

	inline static int32_t get_offset_of_onComplete_1() { return static_cast<int32_t>(offsetof(U3CRuleFadeOutU3Ec__AnonStorey1_t4086381191, ___onComplete_1)); }
	inline Action_t3226471752 * get_onComplete_1() const { return ___onComplete_1; }
	inline Action_t3226471752 ** get_address_of_onComplete_1() { return &___onComplete_1; }
	inline void set_onComplete_1(Action_t3226471752 * value)
	{
		___onComplete_1 = value;
		Il2CppCodeGenWriteBarrier((&___onComplete_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CRULEFADEOUTU3EC__ANONSTOREY1_T4086381191_H
#ifndef U3CRULEFADEINU3EC__ANONSTOREY0_T716579423_H
#define U3CRULEFADEINU3EC__ANONSTOREY0_T716579423_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.AdvGraphicBase/<RuleFadeIn>c__AnonStorey0
struct  U3CRuleFadeInU3Ec__AnonStorey0_t716579423  : public RuntimeObject
{
public:
	// Utage.UguiTransition Utage.AdvGraphicBase/<RuleFadeIn>c__AnonStorey0::transition
	UguiTransition_t3998485683 * ___transition_0;
	// System.Action Utage.AdvGraphicBase/<RuleFadeIn>c__AnonStorey0::onComplete
	Action_t3226471752 * ___onComplete_1;

public:
	inline static int32_t get_offset_of_transition_0() { return static_cast<int32_t>(offsetof(U3CRuleFadeInU3Ec__AnonStorey0_t716579423, ___transition_0)); }
	inline UguiTransition_t3998485683 * get_transition_0() const { return ___transition_0; }
	inline UguiTransition_t3998485683 ** get_address_of_transition_0() { return &___transition_0; }
	inline void set_transition_0(UguiTransition_t3998485683 * value)
	{
		___transition_0 = value;
		Il2CppCodeGenWriteBarrier((&___transition_0), value);
	}

	inline static int32_t get_offset_of_onComplete_1() { return static_cast<int32_t>(offsetof(U3CRuleFadeInU3Ec__AnonStorey0_t716579423, ___onComplete_1)); }
	inline Action_t3226471752 * get_onComplete_1() const { return ___onComplete_1; }
	inline Action_t3226471752 ** get_address_of_onComplete_1() { return &___onComplete_1; }
	inline void set_onComplete_1(Action_t3226471752 * value)
	{
		___onComplete_1 = value;
		Il2CppCodeGenWriteBarrier((&___onComplete_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CRULEFADEINU3EC__ANONSTOREY0_T716579423_H
#ifndef U3CADDSPRITECLICKEVENTU3EC__ANONSTOREY0_T689962178_H
#define U3CADDSPRITECLICKEVENTU3EC__ANONSTOREY0_T689962178_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.AdvSelectionManager/<AddSpriteClickEvent>c__AnonStorey0
struct  U3CAddSpriteClickEventU3Ec__AnonStorey0_t689962178  : public RuntimeObject
{
public:
	// Utage.AdvSelection Utage.AdvSelectionManager/<AddSpriteClickEvent>c__AnonStorey0::select
	AdvSelection_t2301351953 * ___select_0;
	// Utage.AdvSelectionManager Utage.AdvSelectionManager/<AddSpriteClickEvent>c__AnonStorey0::$this
	AdvSelectionManager_t3429904078 * ___U24this_1;

public:
	inline static int32_t get_offset_of_select_0() { return static_cast<int32_t>(offsetof(U3CAddSpriteClickEventU3Ec__AnonStorey0_t689962178, ___select_0)); }
	inline AdvSelection_t2301351953 * get_select_0() const { return ___select_0; }
	inline AdvSelection_t2301351953 ** get_address_of_select_0() { return &___select_0; }
	inline void set_select_0(AdvSelection_t2301351953 * value)
	{
		___select_0 = value;
		Il2CppCodeGenWriteBarrier((&___select_0), value);
	}

	inline static int32_t get_offset_of_U24this_1() { return static_cast<int32_t>(offsetof(U3CAddSpriteClickEventU3Ec__AnonStorey0_t689962178, ___U24this_1)); }
	inline AdvSelectionManager_t3429904078 * get_U24this_1() const { return ___U24this_1; }
	inline AdvSelectionManager_t3429904078 ** get_address_of_U24this_1() { return &___U24this_1; }
	inline void set_U24this_1(AdvSelectionManager_t3429904078 * value)
	{
		___U24this_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CADDSPRITECLICKEVENTU3EC__ANONSTOREY0_T689962178_H
#ifndef UNITYEVENTBASE_T828812576_H
#define UNITYEVENTBASE_T828812576_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Events.UnityEventBase
struct  UnityEventBase_t828812576  : public RuntimeObject
{
public:
	// UnityEngine.Events.InvokableCallList UnityEngine.Events.UnityEventBase::m_Calls
	InvokableCallList_t2295673753 * ___m_Calls_0;
	// UnityEngine.Events.PersistentCallGroup UnityEngine.Events.UnityEventBase::m_PersistentCalls
	PersistentCallGroup_t339478082 * ___m_PersistentCalls_1;
	// System.String UnityEngine.Events.UnityEventBase::m_TypeName
	String_t* ___m_TypeName_2;
	// System.Boolean UnityEngine.Events.UnityEventBase::m_CallsDirty
	bool ___m_CallsDirty_3;

public:
	inline static int32_t get_offset_of_m_Calls_0() { return static_cast<int32_t>(offsetof(UnityEventBase_t828812576, ___m_Calls_0)); }
	inline InvokableCallList_t2295673753 * get_m_Calls_0() const { return ___m_Calls_0; }
	inline InvokableCallList_t2295673753 ** get_address_of_m_Calls_0() { return &___m_Calls_0; }
	inline void set_m_Calls_0(InvokableCallList_t2295673753 * value)
	{
		___m_Calls_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_Calls_0), value);
	}

	inline static int32_t get_offset_of_m_PersistentCalls_1() { return static_cast<int32_t>(offsetof(UnityEventBase_t828812576, ___m_PersistentCalls_1)); }
	inline PersistentCallGroup_t339478082 * get_m_PersistentCalls_1() const { return ___m_PersistentCalls_1; }
	inline PersistentCallGroup_t339478082 ** get_address_of_m_PersistentCalls_1() { return &___m_PersistentCalls_1; }
	inline void set_m_PersistentCalls_1(PersistentCallGroup_t339478082 * value)
	{
		___m_PersistentCalls_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_PersistentCalls_1), value);
	}

	inline static int32_t get_offset_of_m_TypeName_2() { return static_cast<int32_t>(offsetof(UnityEventBase_t828812576, ___m_TypeName_2)); }
	inline String_t* get_m_TypeName_2() const { return ___m_TypeName_2; }
	inline String_t** get_address_of_m_TypeName_2() { return &___m_TypeName_2; }
	inline void set_m_TypeName_2(String_t* value)
	{
		___m_TypeName_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_TypeName_2), value);
	}

	inline static int32_t get_offset_of_m_CallsDirty_3() { return static_cast<int32_t>(offsetof(UnityEventBase_t828812576, ___m_CallsDirty_3)); }
	inline bool get_m_CallsDirty_3() const { return ___m_CallsDirty_3; }
	inline bool* get_address_of_m_CallsDirty_3() { return &___m_CallsDirty_3; }
	inline void set_m_CallsDirty_3(bool value)
	{
		___m_CallsDirty_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYEVENTBASE_T828812576_H
#ifndef ADVGRAPHICOPERAITONARG_T632373120_H
#define ADVGRAPHICOPERAITONARG_T632373120_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.AdvGraphicOperaitonArg
struct  AdvGraphicOperaitonArg_t632373120  : public RuntimeObject
{
public:
	// System.Single Utage.AdvGraphicOperaitonArg::<FadeTime>k__BackingField
	float ___U3CFadeTimeU3Ek__BackingField_0;
	// Utage.AdvCommand Utage.AdvGraphicOperaitonArg::<Command>k__BackingField
	AdvCommand_t2859960984 * ___U3CCommandU3Ek__BackingField_1;
	// Utage.AdvGraphicInfo Utage.AdvGraphicOperaitonArg::<Graphic>k__BackingField
	AdvGraphicInfo_t3545565645 * ___U3CGraphicU3Ek__BackingField_2;

public:
	inline static int32_t get_offset_of_U3CFadeTimeU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(AdvGraphicOperaitonArg_t632373120, ___U3CFadeTimeU3Ek__BackingField_0)); }
	inline float get_U3CFadeTimeU3Ek__BackingField_0() const { return ___U3CFadeTimeU3Ek__BackingField_0; }
	inline float* get_address_of_U3CFadeTimeU3Ek__BackingField_0() { return &___U3CFadeTimeU3Ek__BackingField_0; }
	inline void set_U3CFadeTimeU3Ek__BackingField_0(float value)
	{
		___U3CFadeTimeU3Ek__BackingField_0 = value;
	}

	inline static int32_t get_offset_of_U3CCommandU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(AdvGraphicOperaitonArg_t632373120, ___U3CCommandU3Ek__BackingField_1)); }
	inline AdvCommand_t2859960984 * get_U3CCommandU3Ek__BackingField_1() const { return ___U3CCommandU3Ek__BackingField_1; }
	inline AdvCommand_t2859960984 ** get_address_of_U3CCommandU3Ek__BackingField_1() { return &___U3CCommandU3Ek__BackingField_1; }
	inline void set_U3CCommandU3Ek__BackingField_1(AdvCommand_t2859960984 * value)
	{
		___U3CCommandU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CCommandU3Ek__BackingField_1), value);
	}

	inline static int32_t get_offset_of_U3CGraphicU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(AdvGraphicOperaitonArg_t632373120, ___U3CGraphicU3Ek__BackingField_2)); }
	inline AdvGraphicInfo_t3545565645 * get_U3CGraphicU3Ek__BackingField_2() const { return ___U3CGraphicU3Ek__BackingField_2; }
	inline AdvGraphicInfo_t3545565645 ** get_address_of_U3CGraphicU3Ek__BackingField_2() { return &___U3CGraphicU3Ek__BackingField_2; }
	inline void set_U3CGraphicU3Ek__BackingField_2(AdvGraphicInfo_t3545565645 * value)
	{
		___U3CGraphicU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CGraphicU3Ek__BackingField_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ADVGRAPHICOPERAITONARG_T632373120_H
#ifndef U3CREADU3EC__ANONSTOREY2_T4147561692_H
#define U3CREADU3EC__ANONSTOREY2_T4147561692_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.AdvGraphicLayer/<Read>c__AnonStorey2
struct  U3CReadU3Ec__AnonStorey2_t4147561692  : public RuntimeObject
{
public:
	// Utage.AdvGraphicInfo Utage.AdvGraphicLayer/<Read>c__AnonStorey2::graphic
	AdvGraphicInfo_t3545565645 * ___graphic_0;
	// Utage.AdvGraphicLayer Utage.AdvGraphicLayer/<Read>c__AnonStorey2::$this
	AdvGraphicLayer_t3630223822 * ___U24this_1;

public:
	inline static int32_t get_offset_of_graphic_0() { return static_cast<int32_t>(offsetof(U3CReadU3Ec__AnonStorey2_t4147561692, ___graphic_0)); }
	inline AdvGraphicInfo_t3545565645 * get_graphic_0() const { return ___graphic_0; }
	inline AdvGraphicInfo_t3545565645 ** get_address_of_graphic_0() { return &___graphic_0; }
	inline void set_graphic_0(AdvGraphicInfo_t3545565645 * value)
	{
		___graphic_0 = value;
		Il2CppCodeGenWriteBarrier((&___graphic_0), value);
	}

	inline static int32_t get_offset_of_U24this_1() { return static_cast<int32_t>(offsetof(U3CReadU3Ec__AnonStorey2_t4147561692, ___U24this_1)); }
	inline AdvGraphicLayer_t3630223822 * get_U24this_1() const { return ___U24this_1; }
	inline AdvGraphicLayer_t3630223822 ** get_address_of_U24this_1() { return &___U24this_1; }
	inline void set_U24this_1(AdvGraphicLayer_t3630223822 * value)
	{
		___U24this_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CREADU3EC__ANONSTOREY2_T4147561692_H
#ifndef U3CDRAWCHARACTERU3EC__ANONSTOREY0_T1934820127_H
#define U3CDRAWCHARACTERU3EC__ANONSTOREY0_T1934820127_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.AdvGraphicGroup/<DrawCharacter>c__AnonStorey0
struct  U3CDrawCharacterU3Ec__AnonStorey0_t1934820127  : public RuntimeObject
{
public:
	// System.String Utage.AdvGraphicGroup/<DrawCharacter>c__AnonStorey0::name
	String_t* ___name_0;
	// System.String Utage.AdvGraphicGroup/<DrawCharacter>c__AnonStorey0::layerName
	String_t* ___layerName_1;

public:
	inline static int32_t get_offset_of_name_0() { return static_cast<int32_t>(offsetof(U3CDrawCharacterU3Ec__AnonStorey0_t1934820127, ___name_0)); }
	inline String_t* get_name_0() const { return ___name_0; }
	inline String_t** get_address_of_name_0() { return &___name_0; }
	inline void set_name_0(String_t* value)
	{
		___name_0 = value;
		Il2CppCodeGenWriteBarrier((&___name_0), value);
	}

	inline static int32_t get_offset_of_layerName_1() { return static_cast<int32_t>(offsetof(U3CDrawCharacterU3Ec__AnonStorey0_t1934820127, ___layerName_1)); }
	inline String_t* get_layerName_1() const { return ___layerName_1; }
	inline String_t** get_address_of_layerName_1() { return &___layerName_1; }
	inline void set_layerName_1(String_t* value)
	{
		___layerName_1 = value;
		Il2CppCodeGenWriteBarrier((&___layerName_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CDRAWCHARACTERU3EC__ANONSTOREY0_T1934820127_H
#ifndef U3CFINDLAYERU3EC__ANONSTOREY1_T802882485_H
#define U3CFINDLAYERU3EC__ANONSTOREY1_T802882485_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.AdvGraphicGroup/<FindLayer>c__AnonStorey1
struct  U3CFindLayerU3Ec__AnonStorey1_t802882485  : public RuntimeObject
{
public:
	// System.String Utage.AdvGraphicGroup/<FindLayer>c__AnonStorey1::name
	String_t* ___name_0;

public:
	inline static int32_t get_offset_of_name_0() { return static_cast<int32_t>(offsetof(U3CFindLayerU3Ec__AnonStorey1_t802882485, ___name_0)); }
	inline String_t* get_name_0() const { return ___name_0; }
	inline String_t** get_address_of_name_0() { return &___name_0; }
	inline void set_name_0(String_t* value)
	{
		___name_0 = value;
		Il2CppCodeGenWriteBarrier((&___name_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CFINDLAYERU3EC__ANONSTOREY1_T802882485_H
#ifndef U3CFINDLAYERORDEFAULTU3EC__ANONSTOREY2_T2419501820_H
#define U3CFINDLAYERORDEFAULTU3EC__ANONSTOREY2_T2419501820_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.AdvGraphicGroup/<FindLayerOrDefault>c__AnonStorey2
struct  U3CFindLayerOrDefaultU3Ec__AnonStorey2_t2419501820  : public RuntimeObject
{
public:
	// System.String Utage.AdvGraphicGroup/<FindLayerOrDefault>c__AnonStorey2::name
	String_t* ___name_0;

public:
	inline static int32_t get_offset_of_name_0() { return static_cast<int32_t>(offsetof(U3CFindLayerOrDefaultU3Ec__AnonStorey2_t2419501820, ___name_0)); }
	inline String_t* get_name_0() const { return ___name_0; }
	inline String_t** get_address_of_name_0() { return &___name_0; }
	inline void set_name_0(String_t* value)
	{
		___name_0 = value;
		Il2CppCodeGenWriteBarrier((&___name_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CFINDLAYERORDEFAULTU3EC__ANONSTOREY2_T2419501820_H
#ifndef ADVGRAPHICINFOLIST_T3537398639_H
#define ADVGRAPHICINFOLIST_T3537398639_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.AdvGraphicInfoList
struct  AdvGraphicInfoList_t3537398639  : public RuntimeObject
{
public:
	// System.String Utage.AdvGraphicInfoList::<Key>k__BackingField
	String_t* ___U3CKeyU3Ek__BackingField_0;
	// System.Collections.Generic.List`1<Utage.AdvGraphicInfo> Utage.AdvGraphicInfoList::infoList
	List_1_t2914686777 * ___infoList_1;

public:
	inline static int32_t get_offset_of_U3CKeyU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(AdvGraphicInfoList_t3537398639, ___U3CKeyU3Ek__BackingField_0)); }
	inline String_t* get_U3CKeyU3Ek__BackingField_0() const { return ___U3CKeyU3Ek__BackingField_0; }
	inline String_t** get_address_of_U3CKeyU3Ek__BackingField_0() { return &___U3CKeyU3Ek__BackingField_0; }
	inline void set_U3CKeyU3Ek__BackingField_0(String_t* value)
	{
		___U3CKeyU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CKeyU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_infoList_1() { return static_cast<int32_t>(offsetof(AdvGraphicInfoList_t3537398639, ___infoList_1)); }
	inline List_1_t2914686777 * get_infoList_1() const { return ___infoList_1; }
	inline List_1_t2914686777 ** get_address_of_infoList_1() { return &___infoList_1; }
	inline void set_infoList_1(List_1_t2914686777 * value)
	{
		___infoList_1 = value;
		Il2CppCodeGenWriteBarrier((&___infoList_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ADVGRAPHICINFOLIST_T3537398639_H
#ifndef U3CDRAWU3EC__ANONSTOREY1_T3123503767_H
#define U3CDRAWU3EC__ANONSTOREY1_T3123503767_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.AdvGraphicLayer/<Draw>c__AnonStorey1
struct  U3CDrawU3Ec__AnonStorey1_t3123503767  : public RuntimeObject
{
public:
	// Utage.AdvGraphicObject Utage.AdvGraphicLayer/<Draw>c__AnonStorey1::obj
	AdvGraphicObject_t3651915246 * ___obj_0;
	// Utage.AdvGraphicOperaitonArg Utage.AdvGraphicLayer/<Draw>c__AnonStorey1::arg
	AdvGraphicOperaitonArg_t632373120 * ___arg_1;
	// Utage.AdvGraphicLayer Utage.AdvGraphicLayer/<Draw>c__AnonStorey1::$this
	AdvGraphicLayer_t3630223822 * ___U24this_2;

public:
	inline static int32_t get_offset_of_obj_0() { return static_cast<int32_t>(offsetof(U3CDrawU3Ec__AnonStorey1_t3123503767, ___obj_0)); }
	inline AdvGraphicObject_t3651915246 * get_obj_0() const { return ___obj_0; }
	inline AdvGraphicObject_t3651915246 ** get_address_of_obj_0() { return &___obj_0; }
	inline void set_obj_0(AdvGraphicObject_t3651915246 * value)
	{
		___obj_0 = value;
		Il2CppCodeGenWriteBarrier((&___obj_0), value);
	}

	inline static int32_t get_offset_of_arg_1() { return static_cast<int32_t>(offsetof(U3CDrawU3Ec__AnonStorey1_t3123503767, ___arg_1)); }
	inline AdvGraphicOperaitonArg_t632373120 * get_arg_1() const { return ___arg_1; }
	inline AdvGraphicOperaitonArg_t632373120 ** get_address_of_arg_1() { return &___arg_1; }
	inline void set_arg_1(AdvGraphicOperaitonArg_t632373120 * value)
	{
		___arg_1 = value;
		Il2CppCodeGenWriteBarrier((&___arg_1), value);
	}

	inline static int32_t get_offset_of_U24this_2() { return static_cast<int32_t>(offsetof(U3CDrawU3Ec__AnonStorey1_t3123503767, ___U24this_2)); }
	inline AdvGraphicLayer_t3630223822 * get_U24this_2() const { return ___U24this_2; }
	inline AdvGraphicLayer_t3630223822 ** get_address_of_U24this_2() { return &___U24this_2; }
	inline void set_U24this_2(AdvGraphicLayer_t3630223822 * value)
	{
		___U24this_2 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CDRAWU3EC__ANONSTOREY1_T3123503767_H
#ifndef U3CCODELAYOUTU3EC__ITERATOR0_T2098714287_H
#define U3CCODELAYOUTU3EC__ITERATOR0_T2098714287_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.AdvGraphicLayer/<CoDelayOut>c__Iterator0
struct  U3CCoDelayOutU3Ec__Iterator0_t2098714287  : public RuntimeObject
{
public:
	// System.Single Utage.AdvGraphicLayer/<CoDelayOut>c__Iterator0::delay
	float ___delay_0;
	// Utage.AdvGraphicObject Utage.AdvGraphicLayer/<CoDelayOut>c__Iterator0::obj
	AdvGraphicObject_t3651915246 * ___obj_1;
	// System.Object Utage.AdvGraphicLayer/<CoDelayOut>c__Iterator0::$current
	RuntimeObject * ___U24current_2;
	// System.Boolean Utage.AdvGraphicLayer/<CoDelayOut>c__Iterator0::$disposing
	bool ___U24disposing_3;
	// System.Int32 Utage.AdvGraphicLayer/<CoDelayOut>c__Iterator0::$PC
	int32_t ___U24PC_4;

public:
	inline static int32_t get_offset_of_delay_0() { return static_cast<int32_t>(offsetof(U3CCoDelayOutU3Ec__Iterator0_t2098714287, ___delay_0)); }
	inline float get_delay_0() const { return ___delay_0; }
	inline float* get_address_of_delay_0() { return &___delay_0; }
	inline void set_delay_0(float value)
	{
		___delay_0 = value;
	}

	inline static int32_t get_offset_of_obj_1() { return static_cast<int32_t>(offsetof(U3CCoDelayOutU3Ec__Iterator0_t2098714287, ___obj_1)); }
	inline AdvGraphicObject_t3651915246 * get_obj_1() const { return ___obj_1; }
	inline AdvGraphicObject_t3651915246 ** get_address_of_obj_1() { return &___obj_1; }
	inline void set_obj_1(AdvGraphicObject_t3651915246 * value)
	{
		___obj_1 = value;
		Il2CppCodeGenWriteBarrier((&___obj_1), value);
	}

	inline static int32_t get_offset_of_U24current_2() { return static_cast<int32_t>(offsetof(U3CCoDelayOutU3Ec__Iterator0_t2098714287, ___U24current_2)); }
	inline RuntimeObject * get_U24current_2() const { return ___U24current_2; }
	inline RuntimeObject ** get_address_of_U24current_2() { return &___U24current_2; }
	inline void set_U24current_2(RuntimeObject * value)
	{
		___U24current_2 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_2), value);
	}

	inline static int32_t get_offset_of_U24disposing_3() { return static_cast<int32_t>(offsetof(U3CCoDelayOutU3Ec__Iterator0_t2098714287, ___U24disposing_3)); }
	inline bool get_U24disposing_3() const { return ___U24disposing_3; }
	inline bool* get_address_of_U24disposing_3() { return &___U24disposing_3; }
	inline void set_U24disposing_3(bool value)
	{
		___U24disposing_3 = value;
	}

	inline static int32_t get_offset_of_U24PC_4() { return static_cast<int32_t>(offsetof(U3CCoDelayOutU3Ec__Iterator0_t2098714287, ___U24PC_4)); }
	inline int32_t get_U24PC_4() const { return ___U24PC_4; }
	inline int32_t* get_address_of_U24PC_4() { return &___U24PC_4; }
	inline void set_U24PC_4(int32_t value)
	{
		___U24PC_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CCODELAYOUTU3EC__ITERATOR0_T2098714287_H
#ifndef U3CCOLOADWAITU3EC__ITERATOR0_T2103120723_H
#define U3CCOLOADWAITU3EC__ITERATOR0_T2103120723_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.AdvGraphicLoader/<CoLoadWait>c__Iterator0
struct  U3CCoLoadWaitU3Ec__Iterator0_t2103120723  : public RuntimeObject
{
public:
	// System.Action Utage.AdvGraphicLoader/<CoLoadWait>c__Iterator0::onComplete
	Action_t3226471752 * ___onComplete_0;
	// Utage.AdvGraphicLoader Utage.AdvGraphicLoader/<CoLoadWait>c__Iterator0::$this
	AdvGraphicLoader_t202702654 * ___U24this_1;
	// System.Object Utage.AdvGraphicLoader/<CoLoadWait>c__Iterator0::$current
	RuntimeObject * ___U24current_2;
	// System.Boolean Utage.AdvGraphicLoader/<CoLoadWait>c__Iterator0::$disposing
	bool ___U24disposing_3;
	// System.Int32 Utage.AdvGraphicLoader/<CoLoadWait>c__Iterator0::$PC
	int32_t ___U24PC_4;

public:
	inline static int32_t get_offset_of_onComplete_0() { return static_cast<int32_t>(offsetof(U3CCoLoadWaitU3Ec__Iterator0_t2103120723, ___onComplete_0)); }
	inline Action_t3226471752 * get_onComplete_0() const { return ___onComplete_0; }
	inline Action_t3226471752 ** get_address_of_onComplete_0() { return &___onComplete_0; }
	inline void set_onComplete_0(Action_t3226471752 * value)
	{
		___onComplete_0 = value;
		Il2CppCodeGenWriteBarrier((&___onComplete_0), value);
	}

	inline static int32_t get_offset_of_U24this_1() { return static_cast<int32_t>(offsetof(U3CCoLoadWaitU3Ec__Iterator0_t2103120723, ___U24this_1)); }
	inline AdvGraphicLoader_t202702654 * get_U24this_1() const { return ___U24this_1; }
	inline AdvGraphicLoader_t202702654 ** get_address_of_U24this_1() { return &___U24this_1; }
	inline void set_U24this_1(AdvGraphicLoader_t202702654 * value)
	{
		___U24this_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_1), value);
	}

	inline static int32_t get_offset_of_U24current_2() { return static_cast<int32_t>(offsetof(U3CCoLoadWaitU3Ec__Iterator0_t2103120723, ___U24current_2)); }
	inline RuntimeObject * get_U24current_2() const { return ___U24current_2; }
	inline RuntimeObject ** get_address_of_U24current_2() { return &___U24current_2; }
	inline void set_U24current_2(RuntimeObject * value)
	{
		___U24current_2 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_2), value);
	}

	inline static int32_t get_offset_of_U24disposing_3() { return static_cast<int32_t>(offsetof(U3CCoLoadWaitU3Ec__Iterator0_t2103120723, ___U24disposing_3)); }
	inline bool get_U24disposing_3() const { return ___U24disposing_3; }
	inline bool* get_address_of_U24disposing_3() { return &___U24disposing_3; }
	inline void set_U24disposing_3(bool value)
	{
		___U24disposing_3 = value;
	}

	inline static int32_t get_offset_of_U24PC_4() { return static_cast<int32_t>(offsetof(U3CCoLoadWaitU3Ec__Iterator0_t2103120723, ___U24PC_4)); }
	inline int32_t get_U24PC_4() const { return ___U24PC_4; }
	inline int32_t* get_address_of_U24PC_4() { return &___U24PC_4; }
	inline void set_U24PC_4(int32_t value)
	{
		___U24PC_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CCOLOADWAITU3EC__ITERATOR0_T2103120723_H
#ifndef U3CRULEFADEOUTU3EC__ANONSTOREY2_T205145622_H
#define U3CRULEFADEOUTU3EC__ANONSTOREY2_T205145622_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.AdvGraphicObject/<RuleFadeOut>c__AnonStorey2
struct  U3CRuleFadeOutU3Ec__AnonStorey2_t205145622  : public RuntimeObject
{
public:
	// System.Action Utage.AdvGraphicObject/<RuleFadeOut>c__AnonStorey2::onComplete
	Action_t3226471752 * ___onComplete_0;
	// Utage.AdvGraphicObject Utage.AdvGraphicObject/<RuleFadeOut>c__AnonStorey2::$this
	AdvGraphicObject_t3651915246 * ___U24this_1;

public:
	inline static int32_t get_offset_of_onComplete_0() { return static_cast<int32_t>(offsetof(U3CRuleFadeOutU3Ec__AnonStorey2_t205145622, ___onComplete_0)); }
	inline Action_t3226471752 * get_onComplete_0() const { return ___onComplete_0; }
	inline Action_t3226471752 ** get_address_of_onComplete_0() { return &___onComplete_0; }
	inline void set_onComplete_0(Action_t3226471752 * value)
	{
		___onComplete_0 = value;
		Il2CppCodeGenWriteBarrier((&___onComplete_0), value);
	}

	inline static int32_t get_offset_of_U24this_1() { return static_cast<int32_t>(offsetof(U3CRuleFadeOutU3Ec__AnonStorey2_t205145622, ___U24this_1)); }
	inline AdvGraphicObject_t3651915246 * get_U24this_1() const { return ___U24this_1; }
	inline AdvGraphicObject_t3651915246 ** get_address_of_U24this_1() { return &___U24this_1; }
	inline void set_U24this_1(AdvGraphicObject_t3651915246 * value)
	{
		___U24this_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CRULEFADEOUTU3EC__ANONSTOREY2_T205145622_H
#ifndef U3CREADU3EC__ANONSTOREY3_T2030984333_H
#define U3CREADU3EC__ANONSTOREY3_T2030984333_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.AdvGraphicObject/<Read>c__AnonStorey3
struct  U3CReadU3Ec__AnonStorey3_t2030984333  : public RuntimeObject
{
public:
	// Utage.AdvGraphicInfo Utage.AdvGraphicObject/<Read>c__AnonStorey3::graphic
	AdvGraphicInfo_t3545565645 * ___graphic_0;
	// System.Byte[] Utage.AdvGraphicObject/<Read>c__AnonStorey3::buffer
	ByteU5BU5D_t3397334013* ___buffer_1;
	// Utage.AdvGraphicObject Utage.AdvGraphicObject/<Read>c__AnonStorey3::$this
	AdvGraphicObject_t3651915246 * ___U24this_2;

public:
	inline static int32_t get_offset_of_graphic_0() { return static_cast<int32_t>(offsetof(U3CReadU3Ec__AnonStorey3_t2030984333, ___graphic_0)); }
	inline AdvGraphicInfo_t3545565645 * get_graphic_0() const { return ___graphic_0; }
	inline AdvGraphicInfo_t3545565645 ** get_address_of_graphic_0() { return &___graphic_0; }
	inline void set_graphic_0(AdvGraphicInfo_t3545565645 * value)
	{
		___graphic_0 = value;
		Il2CppCodeGenWriteBarrier((&___graphic_0), value);
	}

	inline static int32_t get_offset_of_buffer_1() { return static_cast<int32_t>(offsetof(U3CReadU3Ec__AnonStorey3_t2030984333, ___buffer_1)); }
	inline ByteU5BU5D_t3397334013* get_buffer_1() const { return ___buffer_1; }
	inline ByteU5BU5D_t3397334013** get_address_of_buffer_1() { return &___buffer_1; }
	inline void set_buffer_1(ByteU5BU5D_t3397334013* value)
	{
		___buffer_1 = value;
		Il2CppCodeGenWriteBarrier((&___buffer_1), value);
	}

	inline static int32_t get_offset_of_U24this_2() { return static_cast<int32_t>(offsetof(U3CReadU3Ec__AnonStorey3_t2030984333, ___U24this_2)); }
	inline AdvGraphicObject_t3651915246 * get_U24this_2() const { return ___U24this_2; }
	inline AdvGraphicObject_t3651915246 ** get_address_of_U24this_2() { return &___U24this_2; }
	inline void set_U24this_2(AdvGraphicObject_t3651915246 * value)
	{
		___U24this_2 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CREADU3EC__ANONSTOREY3_T2030984333_H
#ifndef U3CFADEOUTU3EC__ANONSTOREY1_T4206137005_H
#define U3CFADEOUTU3EC__ANONSTOREY1_T4206137005_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.AdvGraphicObject/<FadeOut>c__AnonStorey1
struct  U3CFadeOutU3Ec__AnonStorey1_t4206137005  : public RuntimeObject
{
public:
	// System.Single Utage.AdvGraphicObject/<FadeOut>c__AnonStorey1::begin
	float ___begin_0;
	// System.Single Utage.AdvGraphicObject/<FadeOut>c__AnonStorey1::end
	float ___end_1;
	// System.Action Utage.AdvGraphicObject/<FadeOut>c__AnonStorey1::onComplete
	Action_t3226471752 * ___onComplete_2;
	// Utage.AdvGraphicObject Utage.AdvGraphicObject/<FadeOut>c__AnonStorey1::$this
	AdvGraphicObject_t3651915246 * ___U24this_3;

public:
	inline static int32_t get_offset_of_begin_0() { return static_cast<int32_t>(offsetof(U3CFadeOutU3Ec__AnonStorey1_t4206137005, ___begin_0)); }
	inline float get_begin_0() const { return ___begin_0; }
	inline float* get_address_of_begin_0() { return &___begin_0; }
	inline void set_begin_0(float value)
	{
		___begin_0 = value;
	}

	inline static int32_t get_offset_of_end_1() { return static_cast<int32_t>(offsetof(U3CFadeOutU3Ec__AnonStorey1_t4206137005, ___end_1)); }
	inline float get_end_1() const { return ___end_1; }
	inline float* get_address_of_end_1() { return &___end_1; }
	inline void set_end_1(float value)
	{
		___end_1 = value;
	}

	inline static int32_t get_offset_of_onComplete_2() { return static_cast<int32_t>(offsetof(U3CFadeOutU3Ec__AnonStorey1_t4206137005, ___onComplete_2)); }
	inline Action_t3226471752 * get_onComplete_2() const { return ___onComplete_2; }
	inline Action_t3226471752 ** get_address_of_onComplete_2() { return &___onComplete_2; }
	inline void set_onComplete_2(Action_t3226471752 * value)
	{
		___onComplete_2 = value;
		Il2CppCodeGenWriteBarrier((&___onComplete_2), value);
	}

	inline static int32_t get_offset_of_U24this_3() { return static_cast<int32_t>(offsetof(U3CFadeOutU3Ec__AnonStorey1_t4206137005, ___U24this_3)); }
	inline AdvGraphicObject_t3651915246 * get_U24this_3() const { return ___U24this_3; }
	inline AdvGraphicObject_t3651915246 ** get_address_of_U24this_3() { return &___U24this_3; }
	inline void set_U24this_3(AdvGraphicObject_t3651915246 * value)
	{
		___U24this_3 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CFADEOUTU3EC__ANONSTOREY1_T4206137005_H
#ifndef U3CFADEINU3EC__ANONSTOREY0_T2111304937_H
#define U3CFADEINU3EC__ANONSTOREY0_T2111304937_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.AdvGraphicObject/<FadeIn>c__AnonStorey0
struct  U3CFadeInU3Ec__AnonStorey0_t2111304937  : public RuntimeObject
{
public:
	// System.Single Utage.AdvGraphicObject/<FadeIn>c__AnonStorey0::begin
	float ___begin_0;
	// System.Single Utage.AdvGraphicObject/<FadeIn>c__AnonStorey0::end
	float ___end_1;
	// System.Action Utage.AdvGraphicObject/<FadeIn>c__AnonStorey0::onComplete
	Action_t3226471752 * ___onComplete_2;
	// Utage.AdvGraphicObject Utage.AdvGraphicObject/<FadeIn>c__AnonStorey0::$this
	AdvGraphicObject_t3651915246 * ___U24this_3;

public:
	inline static int32_t get_offset_of_begin_0() { return static_cast<int32_t>(offsetof(U3CFadeInU3Ec__AnonStorey0_t2111304937, ___begin_0)); }
	inline float get_begin_0() const { return ___begin_0; }
	inline float* get_address_of_begin_0() { return &___begin_0; }
	inline void set_begin_0(float value)
	{
		___begin_0 = value;
	}

	inline static int32_t get_offset_of_end_1() { return static_cast<int32_t>(offsetof(U3CFadeInU3Ec__AnonStorey0_t2111304937, ___end_1)); }
	inline float get_end_1() const { return ___end_1; }
	inline float* get_address_of_end_1() { return &___end_1; }
	inline void set_end_1(float value)
	{
		___end_1 = value;
	}

	inline static int32_t get_offset_of_onComplete_2() { return static_cast<int32_t>(offsetof(U3CFadeInU3Ec__AnonStorey0_t2111304937, ___onComplete_2)); }
	inline Action_t3226471752 * get_onComplete_2() const { return ___onComplete_2; }
	inline Action_t3226471752 ** get_address_of_onComplete_2() { return &___onComplete_2; }
	inline void set_onComplete_2(Action_t3226471752 * value)
	{
		___onComplete_2 = value;
		Il2CppCodeGenWriteBarrier((&___onComplete_2), value);
	}

	inline static int32_t get_offset_of_U24this_3() { return static_cast<int32_t>(offsetof(U3CFadeInU3Ec__AnonStorey0_t2111304937, ___U24this_3)); }
	inline AdvGraphicObject_t3651915246 * get_U24this_3() const { return ___U24this_3; }
	inline AdvGraphicObject_t3651915246 ** get_address_of_U24this_3() { return &___U24this_3; }
	inline void set_U24this_3(AdvGraphicObject_t3651915246 * value)
	{
		___U24this_3 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CFADEINU3EC__ANONSTOREY0_T2111304937_H
#ifndef ENUM_T2459695545_H
#define ENUM_T2459695545_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Enum
struct  Enum_t2459695545  : public ValueType_t3507792607
{
public:

public:
};

struct Enum_t2459695545_StaticFields
{
public:
	// System.Char[] System.Enum::split_char
	CharU5BU5D_t1328083999* ___split_char_0;

public:
	inline static int32_t get_offset_of_split_char_0() { return static_cast<int32_t>(offsetof(Enum_t2459695545_StaticFields, ___split_char_0)); }
	inline CharU5BU5D_t1328083999* get_split_char_0() const { return ___split_char_0; }
	inline CharU5BU5D_t1328083999** get_address_of_split_char_0() { return &___split_char_0; }
	inline void set_split_char_0(CharU5BU5D_t1328083999* value)
	{
		___split_char_0 = value;
		Il2CppCodeGenWriteBarrier((&___split_char_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t2459695545_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t2459695545_marshaled_com
{
};
#endif // ENUM_T2459695545_H
#ifndef UNITYEVENT_1_T2753368147_H
#define UNITYEVENT_1_T2753368147_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Events.UnityEvent`1<Utage.AdvPage>
struct  UnityEvent_1_t2753368147  : public UnityEventBase_t828812576
{
public:
	// System.Object[] UnityEngine.Events.UnityEvent`1::m_InvokeArray
	ObjectU5BU5D_t3614634134* ___m_InvokeArray_4;

public:
	inline static int32_t get_offset_of_m_InvokeArray_4() { return static_cast<int32_t>(offsetof(UnityEvent_1_t2753368147, ___m_InvokeArray_4)); }
	inline ObjectU5BU5D_t3614634134* get_m_InvokeArray_4() const { return ___m_InvokeArray_4; }
	inline ObjectU5BU5D_t3614634134** get_address_of_m_InvokeArray_4() { return &___m_InvokeArray_4; }
	inline void set_m_InvokeArray_4(ObjectU5BU5D_t3614634134* value)
	{
		___m_InvokeArray_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_InvokeArray_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYEVENT_1_T2753368147_H
#ifndef COLOR_T2020392075_H
#define COLOR_T2020392075_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Color
struct  Color_t2020392075 
{
public:
	// System.Single UnityEngine.Color::r
	float ___r_0;
	// System.Single UnityEngine.Color::g
	float ___g_1;
	// System.Single UnityEngine.Color::b
	float ___b_2;
	// System.Single UnityEngine.Color::a
	float ___a_3;

public:
	inline static int32_t get_offset_of_r_0() { return static_cast<int32_t>(offsetof(Color_t2020392075, ___r_0)); }
	inline float get_r_0() const { return ___r_0; }
	inline float* get_address_of_r_0() { return &___r_0; }
	inline void set_r_0(float value)
	{
		___r_0 = value;
	}

	inline static int32_t get_offset_of_g_1() { return static_cast<int32_t>(offsetof(Color_t2020392075, ___g_1)); }
	inline float get_g_1() const { return ___g_1; }
	inline float* get_address_of_g_1() { return &___g_1; }
	inline void set_g_1(float value)
	{
		___g_1 = value;
	}

	inline static int32_t get_offset_of_b_2() { return static_cast<int32_t>(offsetof(Color_t2020392075, ___b_2)); }
	inline float get_b_2() const { return ___b_2; }
	inline float* get_address_of_b_2() { return &___b_2; }
	inline void set_b_2(float value)
	{
		___b_2 = value;
	}

	inline static int32_t get_offset_of_a_3() { return static_cast<int32_t>(offsetof(Color_t2020392075, ___a_3)); }
	inline float get_a_3() const { return ___a_3; }
	inline float* get_address_of_a_3() { return &___a_3; }
	inline void set_a_3(float value)
	{
		___a_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLOR_T2020392075_H
#ifndef VECTOR3_T2243707580_H
#define VECTOR3_T2243707580_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector3
struct  Vector3_t2243707580 
{
public:
	// System.Single UnityEngine.Vector3::x
	float ___x_1;
	// System.Single UnityEngine.Vector3::y
	float ___y_2;
	// System.Single UnityEngine.Vector3::z
	float ___z_3;

public:
	inline static int32_t get_offset_of_x_1() { return static_cast<int32_t>(offsetof(Vector3_t2243707580, ___x_1)); }
	inline float get_x_1() const { return ___x_1; }
	inline float* get_address_of_x_1() { return &___x_1; }
	inline void set_x_1(float value)
	{
		___x_1 = value;
	}

	inline static int32_t get_offset_of_y_2() { return static_cast<int32_t>(offsetof(Vector3_t2243707580, ___y_2)); }
	inline float get_y_2() const { return ___y_2; }
	inline float* get_address_of_y_2() { return &___y_2; }
	inline void set_y_2(float value)
	{
		___y_2 = value;
	}

	inline static int32_t get_offset_of_z_3() { return static_cast<int32_t>(offsetof(Vector3_t2243707580, ___z_3)); }
	inline float get_z_3() const { return ___z_3; }
	inline float* get_address_of_z_3() { return &___z_3; }
	inline void set_z_3(float value)
	{
		___z_3 = value;
	}
};

struct Vector3_t2243707580_StaticFields
{
public:
	// UnityEngine.Vector3 UnityEngine.Vector3::zeroVector
	Vector3_t2243707580  ___zeroVector_4;
	// UnityEngine.Vector3 UnityEngine.Vector3::oneVector
	Vector3_t2243707580  ___oneVector_5;
	// UnityEngine.Vector3 UnityEngine.Vector3::upVector
	Vector3_t2243707580  ___upVector_6;
	// UnityEngine.Vector3 UnityEngine.Vector3::downVector
	Vector3_t2243707580  ___downVector_7;
	// UnityEngine.Vector3 UnityEngine.Vector3::leftVector
	Vector3_t2243707580  ___leftVector_8;
	// UnityEngine.Vector3 UnityEngine.Vector3::rightVector
	Vector3_t2243707580  ___rightVector_9;
	// UnityEngine.Vector3 UnityEngine.Vector3::forwardVector
	Vector3_t2243707580  ___forwardVector_10;
	// UnityEngine.Vector3 UnityEngine.Vector3::backVector
	Vector3_t2243707580  ___backVector_11;
	// UnityEngine.Vector3 UnityEngine.Vector3::positiveInfinityVector
	Vector3_t2243707580  ___positiveInfinityVector_12;
	// UnityEngine.Vector3 UnityEngine.Vector3::negativeInfinityVector
	Vector3_t2243707580  ___negativeInfinityVector_13;

public:
	inline static int32_t get_offset_of_zeroVector_4() { return static_cast<int32_t>(offsetof(Vector3_t2243707580_StaticFields, ___zeroVector_4)); }
	inline Vector3_t2243707580  get_zeroVector_4() const { return ___zeroVector_4; }
	inline Vector3_t2243707580 * get_address_of_zeroVector_4() { return &___zeroVector_4; }
	inline void set_zeroVector_4(Vector3_t2243707580  value)
	{
		___zeroVector_4 = value;
	}

	inline static int32_t get_offset_of_oneVector_5() { return static_cast<int32_t>(offsetof(Vector3_t2243707580_StaticFields, ___oneVector_5)); }
	inline Vector3_t2243707580  get_oneVector_5() const { return ___oneVector_5; }
	inline Vector3_t2243707580 * get_address_of_oneVector_5() { return &___oneVector_5; }
	inline void set_oneVector_5(Vector3_t2243707580  value)
	{
		___oneVector_5 = value;
	}

	inline static int32_t get_offset_of_upVector_6() { return static_cast<int32_t>(offsetof(Vector3_t2243707580_StaticFields, ___upVector_6)); }
	inline Vector3_t2243707580  get_upVector_6() const { return ___upVector_6; }
	inline Vector3_t2243707580 * get_address_of_upVector_6() { return &___upVector_6; }
	inline void set_upVector_6(Vector3_t2243707580  value)
	{
		___upVector_6 = value;
	}

	inline static int32_t get_offset_of_downVector_7() { return static_cast<int32_t>(offsetof(Vector3_t2243707580_StaticFields, ___downVector_7)); }
	inline Vector3_t2243707580  get_downVector_7() const { return ___downVector_7; }
	inline Vector3_t2243707580 * get_address_of_downVector_7() { return &___downVector_7; }
	inline void set_downVector_7(Vector3_t2243707580  value)
	{
		___downVector_7 = value;
	}

	inline static int32_t get_offset_of_leftVector_8() { return static_cast<int32_t>(offsetof(Vector3_t2243707580_StaticFields, ___leftVector_8)); }
	inline Vector3_t2243707580  get_leftVector_8() const { return ___leftVector_8; }
	inline Vector3_t2243707580 * get_address_of_leftVector_8() { return &___leftVector_8; }
	inline void set_leftVector_8(Vector3_t2243707580  value)
	{
		___leftVector_8 = value;
	}

	inline static int32_t get_offset_of_rightVector_9() { return static_cast<int32_t>(offsetof(Vector3_t2243707580_StaticFields, ___rightVector_9)); }
	inline Vector3_t2243707580  get_rightVector_9() const { return ___rightVector_9; }
	inline Vector3_t2243707580 * get_address_of_rightVector_9() { return &___rightVector_9; }
	inline void set_rightVector_9(Vector3_t2243707580  value)
	{
		___rightVector_9 = value;
	}

	inline static int32_t get_offset_of_forwardVector_10() { return static_cast<int32_t>(offsetof(Vector3_t2243707580_StaticFields, ___forwardVector_10)); }
	inline Vector3_t2243707580  get_forwardVector_10() const { return ___forwardVector_10; }
	inline Vector3_t2243707580 * get_address_of_forwardVector_10() { return &___forwardVector_10; }
	inline void set_forwardVector_10(Vector3_t2243707580  value)
	{
		___forwardVector_10 = value;
	}

	inline static int32_t get_offset_of_backVector_11() { return static_cast<int32_t>(offsetof(Vector3_t2243707580_StaticFields, ___backVector_11)); }
	inline Vector3_t2243707580  get_backVector_11() const { return ___backVector_11; }
	inline Vector3_t2243707580 * get_address_of_backVector_11() { return &___backVector_11; }
	inline void set_backVector_11(Vector3_t2243707580  value)
	{
		___backVector_11 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_12() { return static_cast<int32_t>(offsetof(Vector3_t2243707580_StaticFields, ___positiveInfinityVector_12)); }
	inline Vector3_t2243707580  get_positiveInfinityVector_12() const { return ___positiveInfinityVector_12; }
	inline Vector3_t2243707580 * get_address_of_positiveInfinityVector_12() { return &___positiveInfinityVector_12; }
	inline void set_positiveInfinityVector_12(Vector3_t2243707580  value)
	{
		___positiveInfinityVector_12 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_13() { return static_cast<int32_t>(offsetof(Vector3_t2243707580_StaticFields, ___negativeInfinityVector_13)); }
	inline Vector3_t2243707580  get_negativeInfinityVector_13() const { return ___negativeInfinityVector_13; }
	inline Vector3_t2243707580 * get_address_of_negativeInfinityVector_13() { return &___negativeInfinityVector_13; }
	inline void set_negativeInfinityVector_13(Vector3_t2243707580  value)
	{
		___negativeInfinityVector_13 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR3_T2243707580_H
#ifndef VOID_T1841601450_H
#define VOID_T1841601450_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Void
struct  Void_t1841601450 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VOID_T1841601450_H
#ifndef INTPTR_T_H
#define INTPTR_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTPTR_T_H
#ifndef VECTOR2_T2243707579_H
#define VECTOR2_T2243707579_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector2
struct  Vector2_t2243707579 
{
public:
	// System.Single UnityEngine.Vector2::x
	float ___x_0;
	// System.Single UnityEngine.Vector2::y
	float ___y_1;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Vector2_t2243707579, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Vector2_t2243707579, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}
};

struct Vector2_t2243707579_StaticFields
{
public:
	// UnityEngine.Vector2 UnityEngine.Vector2::zeroVector
	Vector2_t2243707579  ___zeroVector_2;
	// UnityEngine.Vector2 UnityEngine.Vector2::oneVector
	Vector2_t2243707579  ___oneVector_3;
	// UnityEngine.Vector2 UnityEngine.Vector2::upVector
	Vector2_t2243707579  ___upVector_4;
	// UnityEngine.Vector2 UnityEngine.Vector2::downVector
	Vector2_t2243707579  ___downVector_5;
	// UnityEngine.Vector2 UnityEngine.Vector2::leftVector
	Vector2_t2243707579  ___leftVector_6;
	// UnityEngine.Vector2 UnityEngine.Vector2::rightVector
	Vector2_t2243707579  ___rightVector_7;
	// UnityEngine.Vector2 UnityEngine.Vector2::positiveInfinityVector
	Vector2_t2243707579  ___positiveInfinityVector_8;
	// UnityEngine.Vector2 UnityEngine.Vector2::negativeInfinityVector
	Vector2_t2243707579  ___negativeInfinityVector_9;

public:
	inline static int32_t get_offset_of_zeroVector_2() { return static_cast<int32_t>(offsetof(Vector2_t2243707579_StaticFields, ___zeroVector_2)); }
	inline Vector2_t2243707579  get_zeroVector_2() const { return ___zeroVector_2; }
	inline Vector2_t2243707579 * get_address_of_zeroVector_2() { return &___zeroVector_2; }
	inline void set_zeroVector_2(Vector2_t2243707579  value)
	{
		___zeroVector_2 = value;
	}

	inline static int32_t get_offset_of_oneVector_3() { return static_cast<int32_t>(offsetof(Vector2_t2243707579_StaticFields, ___oneVector_3)); }
	inline Vector2_t2243707579  get_oneVector_3() const { return ___oneVector_3; }
	inline Vector2_t2243707579 * get_address_of_oneVector_3() { return &___oneVector_3; }
	inline void set_oneVector_3(Vector2_t2243707579  value)
	{
		___oneVector_3 = value;
	}

	inline static int32_t get_offset_of_upVector_4() { return static_cast<int32_t>(offsetof(Vector2_t2243707579_StaticFields, ___upVector_4)); }
	inline Vector2_t2243707579  get_upVector_4() const { return ___upVector_4; }
	inline Vector2_t2243707579 * get_address_of_upVector_4() { return &___upVector_4; }
	inline void set_upVector_4(Vector2_t2243707579  value)
	{
		___upVector_4 = value;
	}

	inline static int32_t get_offset_of_downVector_5() { return static_cast<int32_t>(offsetof(Vector2_t2243707579_StaticFields, ___downVector_5)); }
	inline Vector2_t2243707579  get_downVector_5() const { return ___downVector_5; }
	inline Vector2_t2243707579 * get_address_of_downVector_5() { return &___downVector_5; }
	inline void set_downVector_5(Vector2_t2243707579  value)
	{
		___downVector_5 = value;
	}

	inline static int32_t get_offset_of_leftVector_6() { return static_cast<int32_t>(offsetof(Vector2_t2243707579_StaticFields, ___leftVector_6)); }
	inline Vector2_t2243707579  get_leftVector_6() const { return ___leftVector_6; }
	inline Vector2_t2243707579 * get_address_of_leftVector_6() { return &___leftVector_6; }
	inline void set_leftVector_6(Vector2_t2243707579  value)
	{
		___leftVector_6 = value;
	}

	inline static int32_t get_offset_of_rightVector_7() { return static_cast<int32_t>(offsetof(Vector2_t2243707579_StaticFields, ___rightVector_7)); }
	inline Vector2_t2243707579  get_rightVector_7() const { return ___rightVector_7; }
	inline Vector2_t2243707579 * get_address_of_rightVector_7() { return &___rightVector_7; }
	inline void set_rightVector_7(Vector2_t2243707579  value)
	{
		___rightVector_7 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_8() { return static_cast<int32_t>(offsetof(Vector2_t2243707579_StaticFields, ___positiveInfinityVector_8)); }
	inline Vector2_t2243707579  get_positiveInfinityVector_8() const { return ___positiveInfinityVector_8; }
	inline Vector2_t2243707579 * get_address_of_positiveInfinityVector_8() { return &___positiveInfinityVector_8; }
	inline void set_positiveInfinityVector_8(Vector2_t2243707579  value)
	{
		___positiveInfinityVector_8 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_9() { return static_cast<int32_t>(offsetof(Vector2_t2243707579_StaticFields, ___negativeInfinityVector_9)); }
	inline Vector2_t2243707579  get_negativeInfinityVector_9() const { return ___negativeInfinityVector_9; }
	inline Vector2_t2243707579 * get_address_of_negativeInfinityVector_9() { return &___negativeInfinityVector_9; }
	inline void set_negativeInfinityVector_9(Vector2_t2243707579  value)
	{
		___negativeInfinityVector_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR2_T2243707579_H
#ifndef UNITYEVENT_1_T1209346020_H
#define UNITYEVENT_1_T1209346020_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Events.UnityEvent`1<Utage.AdvMessageWindowManager>
struct  UnityEvent_1_t1209346020  : public UnityEventBase_t828812576
{
public:
	// System.Object[] UnityEngine.Events.UnityEvent`1::m_InvokeArray
	ObjectU5BU5D_t3614634134* ___m_InvokeArray_4;

public:
	inline static int32_t get_offset_of_m_InvokeArray_4() { return static_cast<int32_t>(offsetof(UnityEvent_1_t1209346020, ___m_InvokeArray_4)); }
	inline ObjectU5BU5D_t3614634134* get_m_InvokeArray_4() const { return ___m_InvokeArray_4; }
	inline ObjectU5BU5D_t3614634134** get_address_of_m_InvokeArray_4() { return &___m_InvokeArray_4; }
	inline void set_m_InvokeArray_4(ObjectU5BU5D_t3614634134* value)
	{
		___m_InvokeArray_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_InvokeArray_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYEVENT_1_T1209346020_H
#ifndef NULLABLE_1_T339576247_H
#define NULLABLE_1_T339576247_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Nullable`1<System.Single>
struct  Nullable_1_t339576247 
{
public:
	// T System.Nullable`1::value
	float ___value_0;
	// System.Boolean System.Nullable`1::has_value
	bool ___has_value_1;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(Nullable_1_t339576247, ___value_0)); }
	inline float get_value_0() const { return ___value_0; }
	inline float* get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(float value)
	{
		___value_0 = value;
	}

	inline static int32_t get_offset_of_has_value_1() { return static_cast<int32_t>(offsetof(Nullable_1_t339576247, ___has_value_1)); }
	inline bool get_has_value_1() const { return ___has_value_1; }
	inline bool* get_address_of_has_value_1() { return &___has_value_1; }
	inline void set_has_value_1(bool value)
	{
		___has_value_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NULLABLE_1_T339576247_H
#ifndef UNITYEVENT_1_T3468254093_H
#define UNITYEVENT_1_T3468254093_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Events.UnityEvent`1<Utage.AdvSelectionManager>
struct  UnityEvent_1_t3468254093  : public UnityEventBase_t828812576
{
public:
	// System.Object[] UnityEngine.Events.UnityEvent`1::m_InvokeArray
	ObjectU5BU5D_t3614634134* ___m_InvokeArray_4;

public:
	inline static int32_t get_offset_of_m_InvokeArray_4() { return static_cast<int32_t>(offsetof(UnityEvent_1_t3468254093, ___m_InvokeArray_4)); }
	inline ObjectU5BU5D_t3614634134* get_m_InvokeArray_4() const { return ___m_InvokeArray_4; }
	inline ObjectU5BU5D_t3614634134** get_address_of_m_InvokeArray_4() { return &___m_InvokeArray_4; }
	inline void set_m_InvokeArray_4(ObjectU5BU5D_t3614634134* value)
	{
		___m_InvokeArray_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_InvokeArray_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYEVENT_1_T3468254093_H
#ifndef UNITYEVENT_1_T2324342574_H
#define UNITYEVENT_1_T2324342574_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Events.UnityEvent`1<Utage.AdvEffectColor>
struct  UnityEvent_1_t2324342574  : public UnityEventBase_t828812576
{
public:
	// System.Object[] UnityEngine.Events.UnityEvent`1::m_InvokeArray
	ObjectU5BU5D_t3614634134* ___m_InvokeArray_4;

public:
	inline static int32_t get_offset_of_m_InvokeArray_4() { return static_cast<int32_t>(offsetof(UnityEvent_1_t2324342574, ___m_InvokeArray_4)); }
	inline ObjectU5BU5D_t3614634134* get_m_InvokeArray_4() const { return ___m_InvokeArray_4; }
	inline ObjectU5BU5D_t3614634134** get_address_of_m_InvokeArray_4() { return &___m_InvokeArray_4; }
	inline void set_m_InvokeArray_4(ObjectU5BU5D_t3614634134* value)
	{
		___m_InvokeArray_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_InvokeArray_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYEVENT_1_T2324342574_H
#ifndef UNITYEVENT_1_T3535287012_H
#define UNITYEVENT_1_T3535287012_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Events.UnityEvent`1<Utage.AdvBacklogManager>
struct  UnityEvent_1_t3535287012  : public UnityEventBase_t828812576
{
public:
	// System.Object[] UnityEngine.Events.UnityEvent`1::m_InvokeArray
	ObjectU5BU5D_t3614634134* ___m_InvokeArray_4;

public:
	inline static int32_t get_offset_of_m_InvokeArray_4() { return static_cast<int32_t>(offsetof(UnityEvent_1_t3535287012, ___m_InvokeArray_4)); }
	inline ObjectU5BU5D_t3614634134* get_m_InvokeArray_4() const { return ___m_InvokeArray_4; }
	inline ObjectU5BU5D_t3614634134** get_address_of_m_InvokeArray_4() { return &___m_InvokeArray_4; }
	inline void set_m_InvokeArray_4(ObjectU5BU5D_t3614634134* value)
	{
		___m_InvokeArray_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_InvokeArray_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYEVENT_1_T3535287012_H
#ifndef TIMESPAN_T3430258949_H
#define TIMESPAN_T3430258949_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.TimeSpan
struct  TimeSpan_t3430258949 
{
public:
	// System.Int64 System.TimeSpan::_ticks
	int64_t ____ticks_8;

public:
	inline static int32_t get_offset_of__ticks_8() { return static_cast<int32_t>(offsetof(TimeSpan_t3430258949, ____ticks_8)); }
	inline int64_t get__ticks_8() const { return ____ticks_8; }
	inline int64_t* get_address_of__ticks_8() { return &____ticks_8; }
	inline void set__ticks_8(int64_t value)
	{
		____ticks_8 = value;
	}
};

struct TimeSpan_t3430258949_StaticFields
{
public:
	// System.TimeSpan System.TimeSpan::MaxValue
	TimeSpan_t3430258949  ___MaxValue_5;
	// System.TimeSpan System.TimeSpan::MinValue
	TimeSpan_t3430258949  ___MinValue_6;
	// System.TimeSpan System.TimeSpan::Zero
	TimeSpan_t3430258949  ___Zero_7;

public:
	inline static int32_t get_offset_of_MaxValue_5() { return static_cast<int32_t>(offsetof(TimeSpan_t3430258949_StaticFields, ___MaxValue_5)); }
	inline TimeSpan_t3430258949  get_MaxValue_5() const { return ___MaxValue_5; }
	inline TimeSpan_t3430258949 * get_address_of_MaxValue_5() { return &___MaxValue_5; }
	inline void set_MaxValue_5(TimeSpan_t3430258949  value)
	{
		___MaxValue_5 = value;
	}

	inline static int32_t get_offset_of_MinValue_6() { return static_cast<int32_t>(offsetof(TimeSpan_t3430258949_StaticFields, ___MinValue_6)); }
	inline TimeSpan_t3430258949  get_MinValue_6() const { return ___MinValue_6; }
	inline TimeSpan_t3430258949 * get_address_of_MinValue_6() { return &___MinValue_6; }
	inline void set_MinValue_6(TimeSpan_t3430258949  value)
	{
		___MinValue_6 = value;
	}

	inline static int32_t get_offset_of_Zero_7() { return static_cast<int32_t>(offsetof(TimeSpan_t3430258949_StaticFields, ___Zero_7)); }
	inline TimeSpan_t3430258949  get_Zero_7() const { return ___Zero_7; }
	inline TimeSpan_t3430258949 * get_address_of_Zero_7() { return &___Zero_7; }
	inline void set_Zero_7(TimeSpan_t3430258949  value)
	{
		___Zero_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TIMESPAN_T3430258949_H
#ifndef VOICESTOPTYPE_T2348785448_H
#define VOICESTOPTYPE_T2348785448_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.VoiceStopType
struct  VoiceStopType_t2348785448 
{
public:
	// System.Int32 Utage.VoiceStopType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(VoiceStopType_t2348785448, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VOICESTOPTYPE_T2348785448_H
#ifndef LAYERTYPE_T2078485382_H
#define LAYERTYPE_T2078485382_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.AdvLayerSettingData/LayerType
struct  LayerType_t2078485382 
{
public:
	// System.Int32 Utage.AdvLayerSettingData/LayerType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(LayerType_t2078485382, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LAYERTYPE_T2078485382_H
#ifndef SAVETYPE_T1450193093_H
#define SAVETYPE_T1450193093_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.AdvSaveManager/SaveType
struct  SaveType_t1450193093 
{
public:
	// System.Int32 Utage.AdvSaveManager/SaveType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(SaveType_t1450193093, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SAVETYPE_T1450193093_H
#ifndef DELEGATE_T3022476291_H
#define DELEGATE_T3022476291_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Delegate
struct  Delegate_t3022476291  : public RuntimeObject
{
public:
	// System.IntPtr System.Delegate::method_ptr
	Il2CppMethodPointer ___method_ptr_0;
	// System.IntPtr System.Delegate::invoke_impl
	intptr_t ___invoke_impl_1;
	// System.Object System.Delegate::m_target
	RuntimeObject * ___m_target_2;
	// System.IntPtr System.Delegate::method
	intptr_t ___method_3;
	// System.IntPtr System.Delegate::delegate_trampoline
	intptr_t ___delegate_trampoline_4;
	// System.IntPtr System.Delegate::method_code
	intptr_t ___method_code_5;
	// System.Reflection.MethodInfo System.Delegate::method_info
	MethodInfo_t * ___method_info_6;
	// System.Reflection.MethodInfo System.Delegate::original_method_info
	MethodInfo_t * ___original_method_info_7;
	// System.DelegateData System.Delegate::data
	DelegateData_t1572802995 * ___data_8;

public:
	inline static int32_t get_offset_of_method_ptr_0() { return static_cast<int32_t>(offsetof(Delegate_t3022476291, ___method_ptr_0)); }
	inline Il2CppMethodPointer get_method_ptr_0() const { return ___method_ptr_0; }
	inline Il2CppMethodPointer* get_address_of_method_ptr_0() { return &___method_ptr_0; }
	inline void set_method_ptr_0(Il2CppMethodPointer value)
	{
		___method_ptr_0 = value;
	}

	inline static int32_t get_offset_of_invoke_impl_1() { return static_cast<int32_t>(offsetof(Delegate_t3022476291, ___invoke_impl_1)); }
	inline intptr_t get_invoke_impl_1() const { return ___invoke_impl_1; }
	inline intptr_t* get_address_of_invoke_impl_1() { return &___invoke_impl_1; }
	inline void set_invoke_impl_1(intptr_t value)
	{
		___invoke_impl_1 = value;
	}

	inline static int32_t get_offset_of_m_target_2() { return static_cast<int32_t>(offsetof(Delegate_t3022476291, ___m_target_2)); }
	inline RuntimeObject * get_m_target_2() const { return ___m_target_2; }
	inline RuntimeObject ** get_address_of_m_target_2() { return &___m_target_2; }
	inline void set_m_target_2(RuntimeObject * value)
	{
		___m_target_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_target_2), value);
	}

	inline static int32_t get_offset_of_method_3() { return static_cast<int32_t>(offsetof(Delegate_t3022476291, ___method_3)); }
	inline intptr_t get_method_3() const { return ___method_3; }
	inline intptr_t* get_address_of_method_3() { return &___method_3; }
	inline void set_method_3(intptr_t value)
	{
		___method_3 = value;
	}

	inline static int32_t get_offset_of_delegate_trampoline_4() { return static_cast<int32_t>(offsetof(Delegate_t3022476291, ___delegate_trampoline_4)); }
	inline intptr_t get_delegate_trampoline_4() const { return ___delegate_trampoline_4; }
	inline intptr_t* get_address_of_delegate_trampoline_4() { return &___delegate_trampoline_4; }
	inline void set_delegate_trampoline_4(intptr_t value)
	{
		___delegate_trampoline_4 = value;
	}

	inline static int32_t get_offset_of_method_code_5() { return static_cast<int32_t>(offsetof(Delegate_t3022476291, ___method_code_5)); }
	inline intptr_t get_method_code_5() const { return ___method_code_5; }
	inline intptr_t* get_address_of_method_code_5() { return &___method_code_5; }
	inline void set_method_code_5(intptr_t value)
	{
		___method_code_5 = value;
	}

	inline static int32_t get_offset_of_method_info_6() { return static_cast<int32_t>(offsetof(Delegate_t3022476291, ___method_info_6)); }
	inline MethodInfo_t * get_method_info_6() const { return ___method_info_6; }
	inline MethodInfo_t ** get_address_of_method_info_6() { return &___method_info_6; }
	inline void set_method_info_6(MethodInfo_t * value)
	{
		___method_info_6 = value;
		Il2CppCodeGenWriteBarrier((&___method_info_6), value);
	}

	inline static int32_t get_offset_of_original_method_info_7() { return static_cast<int32_t>(offsetof(Delegate_t3022476291, ___original_method_info_7)); }
	inline MethodInfo_t * get_original_method_info_7() const { return ___original_method_info_7; }
	inline MethodInfo_t ** get_address_of_original_method_info_7() { return &___original_method_info_7; }
	inline void set_original_method_info_7(MethodInfo_t * value)
	{
		___original_method_info_7 = value;
		Il2CppCodeGenWriteBarrier((&___original_method_info_7), value);
	}

	inline static int32_t get_offset_of_data_8() { return static_cast<int32_t>(offsetof(Delegate_t3022476291, ___data_8)); }
	inline DelegateData_t1572802995 * get_data_8() const { return ___data_8; }
	inline DelegateData_t1572802995 ** get_address_of_data_8() { return &___data_8; }
	inline void set_data_8(DelegateData_t1572802995 * value)
	{
		___data_8 = value;
		Il2CppCodeGenWriteBarrier((&___data_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DELEGATE_T3022476291_H
#ifndef ASSETFILELOADPRIORITY_T1290657756_H
#define ASSETFILELOADPRIORITY_T1290657756_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.AssetFileLoadPriority
struct  AssetFileLoadPriority_t1290657756 
{
public:
	// System.Int32 Utage.AssetFileLoadPriority::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(AssetFileLoadPriority_t1290657756, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ASSETFILELOADPRIORITY_T1290657756_H
#ifndef DATETIMEKIND_T2186819611_H
#define DATETIMEKIND_T2186819611_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.DateTimeKind
struct  DateTimeKind_t2186819611 
{
public:
	// System.Int32 System.DateTimeKind::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(DateTimeKind_t2186819611, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DATETIMEKIND_T2186819611_H
#ifndef SAVEDATATYPE_T3353763446_H
#define SAVEDATATYPE_T3353763446_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.AdvSaveData/SaveDataType
struct  SaveDataType_t3353763446 
{
public:
	// System.Int32 Utage.AdvSaveData/SaveDataType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(SaveDataType_t3353763446, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SAVEDATATYPE_T3353763446_H
#ifndef ASSETFILETYPE_T2804304536_H
#define ASSETFILETYPE_T2804304536_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.AssetFileType
struct  AssetFileType_t2804304536 
{
public:
	// System.Int32 Utage.AssetFileType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(AssetFileType_t2804304536, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ASSETFILETYPE_T2804304536_H
#ifndef OBJECT_T1021602117_H
#define OBJECT_T1021602117_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Object
struct  Object_t1021602117  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Object::m_CachedPtr
	intptr_t ___m_CachedPtr_0;

public:
	inline static int32_t get_offset_of_m_CachedPtr_0() { return static_cast<int32_t>(offsetof(Object_t1021602117, ___m_CachedPtr_0)); }
	inline intptr_t get_m_CachedPtr_0() const { return ___m_CachedPtr_0; }
	inline intptr_t* get_address_of_m_CachedPtr_0() { return &___m_CachedPtr_0; }
	inline void set_m_CachedPtr_0(intptr_t value)
	{
		___m_CachedPtr_0 = value;
	}
};

struct Object_t1021602117_StaticFields
{
public:
	// System.Int32 UnityEngine.Object::OffsetOfInstanceIDInCPlusPlusObject
	int32_t ___OffsetOfInstanceIDInCPlusPlusObject_1;

public:
	inline static int32_t get_offset_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return static_cast<int32_t>(offsetof(Object_t1021602117_StaticFields, ___OffsetOfInstanceIDInCPlusPlusObject_1)); }
	inline int32_t get_OffsetOfInstanceIDInCPlusPlusObject_1() const { return ___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline int32_t* get_address_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return &___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline void set_OffsetOfInstanceIDInCPlusPlusObject_1(int32_t value)
	{
		___OffsetOfInstanceIDInCPlusPlusObject_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.Object
struct Object_t1021602117_marshaled_pinvoke
{
	intptr_t ___m_CachedPtr_0;
};
// Native definition for COM marshalling of UnityEngine.Object
struct Object_t1021602117_marshaled_com
{
	intptr_t ___m_CachedPtr_0;
};
#endif // OBJECT_T1021602117_H
#ifndef ADVPAGECONTROLLERTYPE_T2095572932_H
#define ADVPAGECONTROLLERTYPE_T2095572932_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.AdvPageControllerType
struct  AdvPageControllerType_t2095572932 
{
public:
	// System.Int32 Utage.AdvPageControllerType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(AdvPageControllerType_t2095572932, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ADVPAGECONTROLLERTYPE_T2095572932_H
#ifndef PAGESTATUS_T47322912_H
#define PAGESTATUS_T47322912_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.AdvPage/PageStatus
struct  PageStatus_t47322912 
{
public:
	// System.Int32 Utage.AdvPage/PageStatus::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(PageStatus_t47322912, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PAGESTATUS_T47322912_H
#ifndef SAVETYPE_T1557152155_H
#define SAVETYPE_T1557152155_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.AdvGraphicObjectPrefabBase/SaveType
struct  SaveType_t1557152155 
{
public:
	// System.Int32 Utage.AdvGraphicObjectPrefabBase/SaveType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(SaveType_t1557152155, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SAVETYPE_T1557152155_H
#ifndef ADVPAGEEVENT_T790047568_H
#define ADVPAGEEVENT_T790047568_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.AdvPageEvent
struct  AdvPageEvent_t790047568  : public UnityEvent_1_t2753368147
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ADVPAGEEVENT_T790047568_H
#ifndef ADVRENDERTEXTUREMODE_T3600907407_H
#define ADVRENDERTEXTUREMODE_T3600907407_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.AdvRenderTextureMode
struct  AdvRenderTextureMode_t3600907407 
{
public:
	// System.Int32 Utage.AdvRenderTextureMode::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(AdvRenderTextureMode_t3600907407, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ADVRENDERTEXTUREMODE_T3600907407_H
#ifndef ADVERRORMSG_T1899702012_H
#define ADVERRORMSG_T1899702012_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.AdvErrorMsg
struct  AdvErrorMsg_t1899702012 
{
public:
	// System.Int32 Utage.AdvErrorMsg::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(AdvErrorMsg_t1899702012, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ADVERRORMSG_T1899702012_H
#ifndef BACKLOGEVENT_T3497267401_H
#define BACKLOGEVENT_T3497267401_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.BacklogEvent
struct  BacklogEvent_t3497267401  : public UnityEvent_1_t3535287012
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BACKLOGEVENT_T3497267401_H
#ifndef SELECTIONEVENT_T3989025344_H
#define SELECTIONEVENT_T3989025344_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.SelectionEvent
struct  SelectionEvent_t3989025344  : public UnityEvent_1_t3468254093
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SELECTIONEVENT_T3989025344_H
#ifndef ADVSELECTION_T2301351953_H
#define ADVSELECTION_T2301351953_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.AdvSelection
struct  AdvSelection_t2301351953  : public RuntimeObject
{
public:
	// System.String Utage.AdvSelection::<Label>k__BackingField
	String_t* ___U3CLabelU3Ek__BackingField_0;
	// System.String Utage.AdvSelection::text
	String_t* ___text_1;
	// System.String Utage.AdvSelection::jumpLabel
	String_t* ___jumpLabel_2;
	// Utage.ExpressionParser Utage.AdvSelection::exp
	ExpressionParser_t665800307 * ___exp_3;
	// System.String Utage.AdvSelection::<PrefabName>k__BackingField
	String_t* ___U3CPrefabNameU3Ek__BackingField_4;
	// System.Nullable`1<System.Single> Utage.AdvSelection::<X>k__BackingField
	Nullable_1_t339576247  ___U3CXU3Ek__BackingField_5;
	// System.Nullable`1<System.Single> Utage.AdvSelection::<Y>k__BackingField
	Nullable_1_t339576247  ___U3CYU3Ek__BackingField_6;
	// System.String Utage.AdvSelection::spriteName
	String_t* ___spriteName_7;
	// System.Boolean Utage.AdvSelection::isPolygon
	bool ___isPolygon_8;
	// Utage.StringGridRow Utage.AdvSelection::row
	StringGridRow_t4193237197 * ___row_9;

public:
	inline static int32_t get_offset_of_U3CLabelU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(AdvSelection_t2301351953, ___U3CLabelU3Ek__BackingField_0)); }
	inline String_t* get_U3CLabelU3Ek__BackingField_0() const { return ___U3CLabelU3Ek__BackingField_0; }
	inline String_t** get_address_of_U3CLabelU3Ek__BackingField_0() { return &___U3CLabelU3Ek__BackingField_0; }
	inline void set_U3CLabelU3Ek__BackingField_0(String_t* value)
	{
		___U3CLabelU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CLabelU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_text_1() { return static_cast<int32_t>(offsetof(AdvSelection_t2301351953, ___text_1)); }
	inline String_t* get_text_1() const { return ___text_1; }
	inline String_t** get_address_of_text_1() { return &___text_1; }
	inline void set_text_1(String_t* value)
	{
		___text_1 = value;
		Il2CppCodeGenWriteBarrier((&___text_1), value);
	}

	inline static int32_t get_offset_of_jumpLabel_2() { return static_cast<int32_t>(offsetof(AdvSelection_t2301351953, ___jumpLabel_2)); }
	inline String_t* get_jumpLabel_2() const { return ___jumpLabel_2; }
	inline String_t** get_address_of_jumpLabel_2() { return &___jumpLabel_2; }
	inline void set_jumpLabel_2(String_t* value)
	{
		___jumpLabel_2 = value;
		Il2CppCodeGenWriteBarrier((&___jumpLabel_2), value);
	}

	inline static int32_t get_offset_of_exp_3() { return static_cast<int32_t>(offsetof(AdvSelection_t2301351953, ___exp_3)); }
	inline ExpressionParser_t665800307 * get_exp_3() const { return ___exp_3; }
	inline ExpressionParser_t665800307 ** get_address_of_exp_3() { return &___exp_3; }
	inline void set_exp_3(ExpressionParser_t665800307 * value)
	{
		___exp_3 = value;
		Il2CppCodeGenWriteBarrier((&___exp_3), value);
	}

	inline static int32_t get_offset_of_U3CPrefabNameU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(AdvSelection_t2301351953, ___U3CPrefabNameU3Ek__BackingField_4)); }
	inline String_t* get_U3CPrefabNameU3Ek__BackingField_4() const { return ___U3CPrefabNameU3Ek__BackingField_4; }
	inline String_t** get_address_of_U3CPrefabNameU3Ek__BackingField_4() { return &___U3CPrefabNameU3Ek__BackingField_4; }
	inline void set_U3CPrefabNameU3Ek__BackingField_4(String_t* value)
	{
		___U3CPrefabNameU3Ek__BackingField_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CPrefabNameU3Ek__BackingField_4), value);
	}

	inline static int32_t get_offset_of_U3CXU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(AdvSelection_t2301351953, ___U3CXU3Ek__BackingField_5)); }
	inline Nullable_1_t339576247  get_U3CXU3Ek__BackingField_5() const { return ___U3CXU3Ek__BackingField_5; }
	inline Nullable_1_t339576247 * get_address_of_U3CXU3Ek__BackingField_5() { return &___U3CXU3Ek__BackingField_5; }
	inline void set_U3CXU3Ek__BackingField_5(Nullable_1_t339576247  value)
	{
		___U3CXU3Ek__BackingField_5 = value;
	}

	inline static int32_t get_offset_of_U3CYU3Ek__BackingField_6() { return static_cast<int32_t>(offsetof(AdvSelection_t2301351953, ___U3CYU3Ek__BackingField_6)); }
	inline Nullable_1_t339576247  get_U3CYU3Ek__BackingField_6() const { return ___U3CYU3Ek__BackingField_6; }
	inline Nullable_1_t339576247 * get_address_of_U3CYU3Ek__BackingField_6() { return &___U3CYU3Ek__BackingField_6; }
	inline void set_U3CYU3Ek__BackingField_6(Nullable_1_t339576247  value)
	{
		___U3CYU3Ek__BackingField_6 = value;
	}

	inline static int32_t get_offset_of_spriteName_7() { return static_cast<int32_t>(offsetof(AdvSelection_t2301351953, ___spriteName_7)); }
	inline String_t* get_spriteName_7() const { return ___spriteName_7; }
	inline String_t** get_address_of_spriteName_7() { return &___spriteName_7; }
	inline void set_spriteName_7(String_t* value)
	{
		___spriteName_7 = value;
		Il2CppCodeGenWriteBarrier((&___spriteName_7), value);
	}

	inline static int32_t get_offset_of_isPolygon_8() { return static_cast<int32_t>(offsetof(AdvSelection_t2301351953, ___isPolygon_8)); }
	inline bool get_isPolygon_8() const { return ___isPolygon_8; }
	inline bool* get_address_of_isPolygon_8() { return &___isPolygon_8; }
	inline void set_isPolygon_8(bool value)
	{
		___isPolygon_8 = value;
	}

	inline static int32_t get_offset_of_row_9() { return static_cast<int32_t>(offsetof(AdvSelection_t2301351953, ___row_9)); }
	inline StringGridRow_t4193237197 * get_row_9() const { return ___row_9; }
	inline StringGridRow_t4193237197 ** get_address_of_row_9() { return &___row_9; }
	inline void set_row_9(StringGridRow_t4193237197 * value)
	{
		___row_9 = value;
		Il2CppCodeGenWriteBarrier((&___row_9), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ADVSELECTION_T2301351953_H
#ifndef ADVGRAPHICINFO_T3545565645_H
#define ADVGRAPHICINFO_T3545565645_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.AdvGraphicInfo
struct  AdvGraphicInfo_t3545565645  : public RuntimeObject
{
public:
	// System.String Utage.AdvGraphicInfo::<DataType>k__BackingField
	String_t* ___U3CDataTypeU3Ek__BackingField_16;
	// System.Int32 Utage.AdvGraphicInfo::<Index>k__BackingField
	int32_t ___U3CIndexU3Ek__BackingField_17;
	// System.String Utage.AdvGraphicInfo::<Key>k__BackingField
	String_t* ___U3CKeyU3Ek__BackingField_18;
	// System.String Utage.AdvGraphicInfo::<FileType>k__BackingField
	String_t* ___U3CFileTypeU3Ek__BackingField_19;
	// Utage.StringGridRow Utage.AdvGraphicInfo::<RowData>k__BackingField
	StringGridRow_t4193237197 * ___U3CRowDataU3Ek__BackingField_20;
	// Utage.IAdvSettingData Utage.AdvGraphicInfo::<SettingData>k__BackingField
	RuntimeObject* ___U3CSettingDataU3Ek__BackingField_21;
	// System.String Utage.AdvGraphicInfo::<FileName>k__BackingField
	String_t* ___U3CFileNameU3Ek__BackingField_22;
	// Utage.AssetFile Utage.AdvGraphicInfo::file
	RuntimeObject* ___file_23;
	// UnityEngine.Vector2 Utage.AdvGraphicInfo::<Pivot>k__BackingField
	Vector2_t2243707579  ___U3CPivotU3Ek__BackingField_24;
	// UnityEngine.Vector3 Utage.AdvGraphicInfo::<Scale>k__BackingField
	Vector3_t2243707580  ___U3CScaleU3Ek__BackingField_25;
	// UnityEngine.Vector3 Utage.AdvGraphicInfo::<Position>k__BackingField
	Vector3_t2243707580  ___U3CPositionU3Ek__BackingField_26;
	// System.String Utage.AdvGraphicInfo::<SubFileName>k__BackingField
	String_t* ___U3CSubFileNameU3Ek__BackingField_27;
	// Utage.AdvAnimationData Utage.AdvGraphicInfo::<AnimationData>k__BackingField
	AdvAnimationData_t3431567515 * ___U3CAnimationDataU3Ek__BackingField_28;
	// Utage.AdvEyeBlinkData Utage.AdvGraphicInfo::<EyeBlinkData>k__BackingField
	AdvEyeBlinkData_t3287347958 * ___U3CEyeBlinkDataU3Ek__BackingField_29;
	// Utage.AdvLipSynchData Utage.AdvGraphicInfo::<LipSynchData>k__BackingField
	AdvLipSynchData_t1090448263 * ___U3CLipSynchDataU3Ek__BackingField_30;
	// System.String Utage.AdvGraphicInfo::<ConditionalExpression>k__BackingField
	String_t* ___U3CConditionalExpressionU3Ek__BackingField_31;
	// Utage.AdvRenderTextureSetting Utage.AdvGraphicInfo::renderTextureSetting
	AdvRenderTextureSetting_t3039724782 * ___renderTextureSetting_32;

public:
	inline static int32_t get_offset_of_U3CDataTypeU3Ek__BackingField_16() { return static_cast<int32_t>(offsetof(AdvGraphicInfo_t3545565645, ___U3CDataTypeU3Ek__BackingField_16)); }
	inline String_t* get_U3CDataTypeU3Ek__BackingField_16() const { return ___U3CDataTypeU3Ek__BackingField_16; }
	inline String_t** get_address_of_U3CDataTypeU3Ek__BackingField_16() { return &___U3CDataTypeU3Ek__BackingField_16; }
	inline void set_U3CDataTypeU3Ek__BackingField_16(String_t* value)
	{
		___U3CDataTypeU3Ek__BackingField_16 = value;
		Il2CppCodeGenWriteBarrier((&___U3CDataTypeU3Ek__BackingField_16), value);
	}

	inline static int32_t get_offset_of_U3CIndexU3Ek__BackingField_17() { return static_cast<int32_t>(offsetof(AdvGraphicInfo_t3545565645, ___U3CIndexU3Ek__BackingField_17)); }
	inline int32_t get_U3CIndexU3Ek__BackingField_17() const { return ___U3CIndexU3Ek__BackingField_17; }
	inline int32_t* get_address_of_U3CIndexU3Ek__BackingField_17() { return &___U3CIndexU3Ek__BackingField_17; }
	inline void set_U3CIndexU3Ek__BackingField_17(int32_t value)
	{
		___U3CIndexU3Ek__BackingField_17 = value;
	}

	inline static int32_t get_offset_of_U3CKeyU3Ek__BackingField_18() { return static_cast<int32_t>(offsetof(AdvGraphicInfo_t3545565645, ___U3CKeyU3Ek__BackingField_18)); }
	inline String_t* get_U3CKeyU3Ek__BackingField_18() const { return ___U3CKeyU3Ek__BackingField_18; }
	inline String_t** get_address_of_U3CKeyU3Ek__BackingField_18() { return &___U3CKeyU3Ek__BackingField_18; }
	inline void set_U3CKeyU3Ek__BackingField_18(String_t* value)
	{
		___U3CKeyU3Ek__BackingField_18 = value;
		Il2CppCodeGenWriteBarrier((&___U3CKeyU3Ek__BackingField_18), value);
	}

	inline static int32_t get_offset_of_U3CFileTypeU3Ek__BackingField_19() { return static_cast<int32_t>(offsetof(AdvGraphicInfo_t3545565645, ___U3CFileTypeU3Ek__BackingField_19)); }
	inline String_t* get_U3CFileTypeU3Ek__BackingField_19() const { return ___U3CFileTypeU3Ek__BackingField_19; }
	inline String_t** get_address_of_U3CFileTypeU3Ek__BackingField_19() { return &___U3CFileTypeU3Ek__BackingField_19; }
	inline void set_U3CFileTypeU3Ek__BackingField_19(String_t* value)
	{
		___U3CFileTypeU3Ek__BackingField_19 = value;
		Il2CppCodeGenWriteBarrier((&___U3CFileTypeU3Ek__BackingField_19), value);
	}

	inline static int32_t get_offset_of_U3CRowDataU3Ek__BackingField_20() { return static_cast<int32_t>(offsetof(AdvGraphicInfo_t3545565645, ___U3CRowDataU3Ek__BackingField_20)); }
	inline StringGridRow_t4193237197 * get_U3CRowDataU3Ek__BackingField_20() const { return ___U3CRowDataU3Ek__BackingField_20; }
	inline StringGridRow_t4193237197 ** get_address_of_U3CRowDataU3Ek__BackingField_20() { return &___U3CRowDataU3Ek__BackingField_20; }
	inline void set_U3CRowDataU3Ek__BackingField_20(StringGridRow_t4193237197 * value)
	{
		___U3CRowDataU3Ek__BackingField_20 = value;
		Il2CppCodeGenWriteBarrier((&___U3CRowDataU3Ek__BackingField_20), value);
	}

	inline static int32_t get_offset_of_U3CSettingDataU3Ek__BackingField_21() { return static_cast<int32_t>(offsetof(AdvGraphicInfo_t3545565645, ___U3CSettingDataU3Ek__BackingField_21)); }
	inline RuntimeObject* get_U3CSettingDataU3Ek__BackingField_21() const { return ___U3CSettingDataU3Ek__BackingField_21; }
	inline RuntimeObject** get_address_of_U3CSettingDataU3Ek__BackingField_21() { return &___U3CSettingDataU3Ek__BackingField_21; }
	inline void set_U3CSettingDataU3Ek__BackingField_21(RuntimeObject* value)
	{
		___U3CSettingDataU3Ek__BackingField_21 = value;
		Il2CppCodeGenWriteBarrier((&___U3CSettingDataU3Ek__BackingField_21), value);
	}

	inline static int32_t get_offset_of_U3CFileNameU3Ek__BackingField_22() { return static_cast<int32_t>(offsetof(AdvGraphicInfo_t3545565645, ___U3CFileNameU3Ek__BackingField_22)); }
	inline String_t* get_U3CFileNameU3Ek__BackingField_22() const { return ___U3CFileNameU3Ek__BackingField_22; }
	inline String_t** get_address_of_U3CFileNameU3Ek__BackingField_22() { return &___U3CFileNameU3Ek__BackingField_22; }
	inline void set_U3CFileNameU3Ek__BackingField_22(String_t* value)
	{
		___U3CFileNameU3Ek__BackingField_22 = value;
		Il2CppCodeGenWriteBarrier((&___U3CFileNameU3Ek__BackingField_22), value);
	}

	inline static int32_t get_offset_of_file_23() { return static_cast<int32_t>(offsetof(AdvGraphicInfo_t3545565645, ___file_23)); }
	inline RuntimeObject* get_file_23() const { return ___file_23; }
	inline RuntimeObject** get_address_of_file_23() { return &___file_23; }
	inline void set_file_23(RuntimeObject* value)
	{
		___file_23 = value;
		Il2CppCodeGenWriteBarrier((&___file_23), value);
	}

	inline static int32_t get_offset_of_U3CPivotU3Ek__BackingField_24() { return static_cast<int32_t>(offsetof(AdvGraphicInfo_t3545565645, ___U3CPivotU3Ek__BackingField_24)); }
	inline Vector2_t2243707579  get_U3CPivotU3Ek__BackingField_24() const { return ___U3CPivotU3Ek__BackingField_24; }
	inline Vector2_t2243707579 * get_address_of_U3CPivotU3Ek__BackingField_24() { return &___U3CPivotU3Ek__BackingField_24; }
	inline void set_U3CPivotU3Ek__BackingField_24(Vector2_t2243707579  value)
	{
		___U3CPivotU3Ek__BackingField_24 = value;
	}

	inline static int32_t get_offset_of_U3CScaleU3Ek__BackingField_25() { return static_cast<int32_t>(offsetof(AdvGraphicInfo_t3545565645, ___U3CScaleU3Ek__BackingField_25)); }
	inline Vector3_t2243707580  get_U3CScaleU3Ek__BackingField_25() const { return ___U3CScaleU3Ek__BackingField_25; }
	inline Vector3_t2243707580 * get_address_of_U3CScaleU3Ek__BackingField_25() { return &___U3CScaleU3Ek__BackingField_25; }
	inline void set_U3CScaleU3Ek__BackingField_25(Vector3_t2243707580  value)
	{
		___U3CScaleU3Ek__BackingField_25 = value;
	}

	inline static int32_t get_offset_of_U3CPositionU3Ek__BackingField_26() { return static_cast<int32_t>(offsetof(AdvGraphicInfo_t3545565645, ___U3CPositionU3Ek__BackingField_26)); }
	inline Vector3_t2243707580  get_U3CPositionU3Ek__BackingField_26() const { return ___U3CPositionU3Ek__BackingField_26; }
	inline Vector3_t2243707580 * get_address_of_U3CPositionU3Ek__BackingField_26() { return &___U3CPositionU3Ek__BackingField_26; }
	inline void set_U3CPositionU3Ek__BackingField_26(Vector3_t2243707580  value)
	{
		___U3CPositionU3Ek__BackingField_26 = value;
	}

	inline static int32_t get_offset_of_U3CSubFileNameU3Ek__BackingField_27() { return static_cast<int32_t>(offsetof(AdvGraphicInfo_t3545565645, ___U3CSubFileNameU3Ek__BackingField_27)); }
	inline String_t* get_U3CSubFileNameU3Ek__BackingField_27() const { return ___U3CSubFileNameU3Ek__BackingField_27; }
	inline String_t** get_address_of_U3CSubFileNameU3Ek__BackingField_27() { return &___U3CSubFileNameU3Ek__BackingField_27; }
	inline void set_U3CSubFileNameU3Ek__BackingField_27(String_t* value)
	{
		___U3CSubFileNameU3Ek__BackingField_27 = value;
		Il2CppCodeGenWriteBarrier((&___U3CSubFileNameU3Ek__BackingField_27), value);
	}

	inline static int32_t get_offset_of_U3CAnimationDataU3Ek__BackingField_28() { return static_cast<int32_t>(offsetof(AdvGraphicInfo_t3545565645, ___U3CAnimationDataU3Ek__BackingField_28)); }
	inline AdvAnimationData_t3431567515 * get_U3CAnimationDataU3Ek__BackingField_28() const { return ___U3CAnimationDataU3Ek__BackingField_28; }
	inline AdvAnimationData_t3431567515 ** get_address_of_U3CAnimationDataU3Ek__BackingField_28() { return &___U3CAnimationDataU3Ek__BackingField_28; }
	inline void set_U3CAnimationDataU3Ek__BackingField_28(AdvAnimationData_t3431567515 * value)
	{
		___U3CAnimationDataU3Ek__BackingField_28 = value;
		Il2CppCodeGenWriteBarrier((&___U3CAnimationDataU3Ek__BackingField_28), value);
	}

	inline static int32_t get_offset_of_U3CEyeBlinkDataU3Ek__BackingField_29() { return static_cast<int32_t>(offsetof(AdvGraphicInfo_t3545565645, ___U3CEyeBlinkDataU3Ek__BackingField_29)); }
	inline AdvEyeBlinkData_t3287347958 * get_U3CEyeBlinkDataU3Ek__BackingField_29() const { return ___U3CEyeBlinkDataU3Ek__BackingField_29; }
	inline AdvEyeBlinkData_t3287347958 ** get_address_of_U3CEyeBlinkDataU3Ek__BackingField_29() { return &___U3CEyeBlinkDataU3Ek__BackingField_29; }
	inline void set_U3CEyeBlinkDataU3Ek__BackingField_29(AdvEyeBlinkData_t3287347958 * value)
	{
		___U3CEyeBlinkDataU3Ek__BackingField_29 = value;
		Il2CppCodeGenWriteBarrier((&___U3CEyeBlinkDataU3Ek__BackingField_29), value);
	}

	inline static int32_t get_offset_of_U3CLipSynchDataU3Ek__BackingField_30() { return static_cast<int32_t>(offsetof(AdvGraphicInfo_t3545565645, ___U3CLipSynchDataU3Ek__BackingField_30)); }
	inline AdvLipSynchData_t1090448263 * get_U3CLipSynchDataU3Ek__BackingField_30() const { return ___U3CLipSynchDataU3Ek__BackingField_30; }
	inline AdvLipSynchData_t1090448263 ** get_address_of_U3CLipSynchDataU3Ek__BackingField_30() { return &___U3CLipSynchDataU3Ek__BackingField_30; }
	inline void set_U3CLipSynchDataU3Ek__BackingField_30(AdvLipSynchData_t1090448263 * value)
	{
		___U3CLipSynchDataU3Ek__BackingField_30 = value;
		Il2CppCodeGenWriteBarrier((&___U3CLipSynchDataU3Ek__BackingField_30), value);
	}

	inline static int32_t get_offset_of_U3CConditionalExpressionU3Ek__BackingField_31() { return static_cast<int32_t>(offsetof(AdvGraphicInfo_t3545565645, ___U3CConditionalExpressionU3Ek__BackingField_31)); }
	inline String_t* get_U3CConditionalExpressionU3Ek__BackingField_31() const { return ___U3CConditionalExpressionU3Ek__BackingField_31; }
	inline String_t** get_address_of_U3CConditionalExpressionU3Ek__BackingField_31() { return &___U3CConditionalExpressionU3Ek__BackingField_31; }
	inline void set_U3CConditionalExpressionU3Ek__BackingField_31(String_t* value)
	{
		___U3CConditionalExpressionU3Ek__BackingField_31 = value;
		Il2CppCodeGenWriteBarrier((&___U3CConditionalExpressionU3Ek__BackingField_31), value);
	}

	inline static int32_t get_offset_of_renderTextureSetting_32() { return static_cast<int32_t>(offsetof(AdvGraphicInfo_t3545565645, ___renderTextureSetting_32)); }
	inline AdvRenderTextureSetting_t3039724782 * get_renderTextureSetting_32() const { return ___renderTextureSetting_32; }
	inline AdvRenderTextureSetting_t3039724782 ** get_address_of_renderTextureSetting_32() { return &___renderTextureSetting_32; }
	inline void set_renderTextureSetting_32(AdvRenderTextureSetting_t3039724782 * value)
	{
		___renderTextureSetting_32 = value;
		Il2CppCodeGenWriteBarrier((&___renderTextureSetting_32), value);
	}
};

struct AdvGraphicInfo_t3545565645_StaticFields
{
public:
	// Utage.AdvGraphicInfo/CreateCustom Utage.AdvGraphicInfo::CallbackCreateCustom
	CreateCustom_t2292834413 * ___CallbackCreateCustom_5;
	// System.Func`2<System.String,System.Boolean> Utage.AdvGraphicInfo::CallbackExpression
	Func_2_t1989381442 * ___CallbackExpression_6;
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> Utage.AdvGraphicInfo::<>f__switch$map3
	Dictionary_2_t3986656710 * ___U3CU3Ef__switchU24map3_34;

public:
	inline static int32_t get_offset_of_CallbackCreateCustom_5() { return static_cast<int32_t>(offsetof(AdvGraphicInfo_t3545565645_StaticFields, ___CallbackCreateCustom_5)); }
	inline CreateCustom_t2292834413 * get_CallbackCreateCustom_5() const { return ___CallbackCreateCustom_5; }
	inline CreateCustom_t2292834413 ** get_address_of_CallbackCreateCustom_5() { return &___CallbackCreateCustom_5; }
	inline void set_CallbackCreateCustom_5(CreateCustom_t2292834413 * value)
	{
		___CallbackCreateCustom_5 = value;
		Il2CppCodeGenWriteBarrier((&___CallbackCreateCustom_5), value);
	}

	inline static int32_t get_offset_of_CallbackExpression_6() { return static_cast<int32_t>(offsetof(AdvGraphicInfo_t3545565645_StaticFields, ___CallbackExpression_6)); }
	inline Func_2_t1989381442 * get_CallbackExpression_6() const { return ___CallbackExpression_6; }
	inline Func_2_t1989381442 ** get_address_of_CallbackExpression_6() { return &___CallbackExpression_6; }
	inline void set_CallbackExpression_6(Func_2_t1989381442 * value)
	{
		___CallbackExpression_6 = value;
		Il2CppCodeGenWriteBarrier((&___CallbackExpression_6), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__switchU24map3_34() { return static_cast<int32_t>(offsetof(AdvGraphicInfo_t3545565645_StaticFields, ___U3CU3Ef__switchU24map3_34)); }
	inline Dictionary_2_t3986656710 * get_U3CU3Ef__switchU24map3_34() const { return ___U3CU3Ef__switchU24map3_34; }
	inline Dictionary_2_t3986656710 ** get_address_of_U3CU3Ef__switchU24map3_34() { return &___U3CU3Ef__switchU24map3_34; }
	inline void set_U3CU3Ef__switchU24map3_34(Dictionary_2_t3986656710 * value)
	{
		___U3CU3Ef__switchU24map3_34 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__switchU24map3_34), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ADVGRAPHICINFO_T3545565645_H
#ifndef MESSAGEWINDOWEVENT_T3103993945_H
#define MESSAGEWINDOWEVENT_T3103993945_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.MessageWindowEvent
struct  MessageWindowEvent_t3103993945  : public UnityEvent_1_t1209346020
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MESSAGEWINDOWEVENT_T3103993945_H
#ifndef EVENTEFFECTCOLOR_T1679408286_H
#define EVENTEFFECTCOLOR_T1679408286_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.EventEffectColor
struct  EventEffectColor_t1679408286  : public UnityEvent_1_t2324342574
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EVENTEFFECTCOLOR_T1679408286_H
#ifndef ADVRENDERTEXTURESETTING_T3039724782_H
#define ADVRENDERTEXTURESETTING_T3039724782_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.AdvRenderTextureSetting
struct  AdvRenderTextureSetting_t3039724782  : public RuntimeObject
{
public:
	// Utage.AdvRenderTextureMode Utage.AdvRenderTextureSetting::<RenderTextureType>k__BackingField
	int32_t ___U3CRenderTextureTypeU3Ek__BackingField_0;
	// UnityEngine.Vector2 Utage.AdvRenderTextureSetting::<RenderTextureSize>k__BackingField
	Vector2_t2243707579  ___U3CRenderTextureSizeU3Ek__BackingField_1;
	// UnityEngine.Vector3 Utage.AdvRenderTextureSetting::<RenderTextureOffset>k__BackingField
	Vector3_t2243707580  ___U3CRenderTextureOffsetU3Ek__BackingField_2;

public:
	inline static int32_t get_offset_of_U3CRenderTextureTypeU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(AdvRenderTextureSetting_t3039724782, ___U3CRenderTextureTypeU3Ek__BackingField_0)); }
	inline int32_t get_U3CRenderTextureTypeU3Ek__BackingField_0() const { return ___U3CRenderTextureTypeU3Ek__BackingField_0; }
	inline int32_t* get_address_of_U3CRenderTextureTypeU3Ek__BackingField_0() { return &___U3CRenderTextureTypeU3Ek__BackingField_0; }
	inline void set_U3CRenderTextureTypeU3Ek__BackingField_0(int32_t value)
	{
		___U3CRenderTextureTypeU3Ek__BackingField_0 = value;
	}

	inline static int32_t get_offset_of_U3CRenderTextureSizeU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(AdvRenderTextureSetting_t3039724782, ___U3CRenderTextureSizeU3Ek__BackingField_1)); }
	inline Vector2_t2243707579  get_U3CRenderTextureSizeU3Ek__BackingField_1() const { return ___U3CRenderTextureSizeU3Ek__BackingField_1; }
	inline Vector2_t2243707579 * get_address_of_U3CRenderTextureSizeU3Ek__BackingField_1() { return &___U3CRenderTextureSizeU3Ek__BackingField_1; }
	inline void set_U3CRenderTextureSizeU3Ek__BackingField_1(Vector2_t2243707579  value)
	{
		___U3CRenderTextureSizeU3Ek__BackingField_1 = value;
	}

	inline static int32_t get_offset_of_U3CRenderTextureOffsetU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(AdvRenderTextureSetting_t3039724782, ___U3CRenderTextureOffsetU3Ek__BackingField_2)); }
	inline Vector3_t2243707580  get_U3CRenderTextureOffsetU3Ek__BackingField_2() const { return ___U3CRenderTextureOffsetU3Ek__BackingField_2; }
	inline Vector3_t2243707580 * get_address_of_U3CRenderTextureOffsetU3Ek__BackingField_2() { return &___U3CRenderTextureOffsetU3Ek__BackingField_2; }
	inline void set_U3CRenderTextureOffsetU3Ek__BackingField_2(Vector3_t2243707580  value)
	{
		___U3CRenderTextureOffsetU3Ek__BackingField_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ADVRENDERTEXTURESETTING_T3039724782_H
#ifndef COMPONENT_T3819376471_H
#define COMPONENT_T3819376471_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Component
struct  Component_t3819376471  : public Object_t1021602117
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPONENT_T3819376471_H
#ifndef ASSETFILEBASE_T4101360697_H
#define ASSETFILEBASE_T4101360697_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.AssetFileBase
struct  AssetFileBase_t4101360697  : public RuntimeObject
{
public:
	// Utage.AssetFileManager Utage.AssetFileBase::<FileManager>k__BackingField
	AssetFileManager_t1095395563 * ___U3CFileManagerU3Ek__BackingField_0;
	// Utage.AssetFileInfo Utage.AssetFileBase::<FileInfo>k__BackingField
	AssetFileInfo_t1386031564 * ___U3CFileInfoU3Ek__BackingField_1;
	// Utage.IAssetFileSettingData Utage.AssetFileBase::<SettingData>k__BackingField
	RuntimeObject* ___U3CSettingDataU3Ek__BackingField_2;
	// Utage.AssetFileType Utage.AssetFileBase::<FileType>k__BackingField
	int32_t ___U3CFileTypeU3Ek__BackingField_3;
	// System.Boolean Utage.AssetFileBase::<IsLoadEnd>k__BackingField
	bool ___U3CIsLoadEndU3Ek__BackingField_4;
	// System.Boolean Utage.AssetFileBase::<IsLoadError>k__BackingField
	bool ___U3CIsLoadErrorU3Ek__BackingField_5;
	// System.String Utage.AssetFileBase::<LoadErrorMsg>k__BackingField
	String_t* ___U3CLoadErrorMsgU3Ek__BackingField_6;
	// UnityEngine.TextAsset Utage.AssetFileBase::<Text>k__BackingField
	TextAsset_t3973159845 * ___U3CTextU3Ek__BackingField_7;
	// UnityEngine.Texture2D Utage.AssetFileBase::<Texture>k__BackingField
	Texture2D_t3542995729 * ___U3CTextureU3Ek__BackingField_8;
	// UnityEngine.AudioClip Utage.AssetFileBase::<Sound>k__BackingField
	AudioClip_t1932558630 * ___U3CSoundU3Ek__BackingField_9;
	// UnityEngine.Object Utage.AssetFileBase::<UnityObject>k__BackingField
	Object_t1021602117 * ___U3CUnityObjectU3Ek__BackingField_10;
	// Utage.AssetFileLoadPriority Utage.AssetFileBase::<Priority>k__BackingField
	int32_t ___U3CPriorityU3Ek__BackingField_11;
	// System.Boolean Utage.AssetFileBase::<IgnoreUnload>k__BackingField
	bool ___U3CIgnoreUnloadU3Ek__BackingField_12;
	// System.Collections.Generic.HashSet`1<System.Object> Utage.AssetFileBase::referenceSet
	HashSet_1_t1022910149 * ___referenceSet_13;

public:
	inline static int32_t get_offset_of_U3CFileManagerU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(AssetFileBase_t4101360697, ___U3CFileManagerU3Ek__BackingField_0)); }
	inline AssetFileManager_t1095395563 * get_U3CFileManagerU3Ek__BackingField_0() const { return ___U3CFileManagerU3Ek__BackingField_0; }
	inline AssetFileManager_t1095395563 ** get_address_of_U3CFileManagerU3Ek__BackingField_0() { return &___U3CFileManagerU3Ek__BackingField_0; }
	inline void set_U3CFileManagerU3Ek__BackingField_0(AssetFileManager_t1095395563 * value)
	{
		___U3CFileManagerU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CFileManagerU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_U3CFileInfoU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(AssetFileBase_t4101360697, ___U3CFileInfoU3Ek__BackingField_1)); }
	inline AssetFileInfo_t1386031564 * get_U3CFileInfoU3Ek__BackingField_1() const { return ___U3CFileInfoU3Ek__BackingField_1; }
	inline AssetFileInfo_t1386031564 ** get_address_of_U3CFileInfoU3Ek__BackingField_1() { return &___U3CFileInfoU3Ek__BackingField_1; }
	inline void set_U3CFileInfoU3Ek__BackingField_1(AssetFileInfo_t1386031564 * value)
	{
		___U3CFileInfoU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CFileInfoU3Ek__BackingField_1), value);
	}

	inline static int32_t get_offset_of_U3CSettingDataU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(AssetFileBase_t4101360697, ___U3CSettingDataU3Ek__BackingField_2)); }
	inline RuntimeObject* get_U3CSettingDataU3Ek__BackingField_2() const { return ___U3CSettingDataU3Ek__BackingField_2; }
	inline RuntimeObject** get_address_of_U3CSettingDataU3Ek__BackingField_2() { return &___U3CSettingDataU3Ek__BackingField_2; }
	inline void set_U3CSettingDataU3Ek__BackingField_2(RuntimeObject* value)
	{
		___U3CSettingDataU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CSettingDataU3Ek__BackingField_2), value);
	}

	inline static int32_t get_offset_of_U3CFileTypeU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(AssetFileBase_t4101360697, ___U3CFileTypeU3Ek__BackingField_3)); }
	inline int32_t get_U3CFileTypeU3Ek__BackingField_3() const { return ___U3CFileTypeU3Ek__BackingField_3; }
	inline int32_t* get_address_of_U3CFileTypeU3Ek__BackingField_3() { return &___U3CFileTypeU3Ek__BackingField_3; }
	inline void set_U3CFileTypeU3Ek__BackingField_3(int32_t value)
	{
		___U3CFileTypeU3Ek__BackingField_3 = value;
	}

	inline static int32_t get_offset_of_U3CIsLoadEndU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(AssetFileBase_t4101360697, ___U3CIsLoadEndU3Ek__BackingField_4)); }
	inline bool get_U3CIsLoadEndU3Ek__BackingField_4() const { return ___U3CIsLoadEndU3Ek__BackingField_4; }
	inline bool* get_address_of_U3CIsLoadEndU3Ek__BackingField_4() { return &___U3CIsLoadEndU3Ek__BackingField_4; }
	inline void set_U3CIsLoadEndU3Ek__BackingField_4(bool value)
	{
		___U3CIsLoadEndU3Ek__BackingField_4 = value;
	}

	inline static int32_t get_offset_of_U3CIsLoadErrorU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(AssetFileBase_t4101360697, ___U3CIsLoadErrorU3Ek__BackingField_5)); }
	inline bool get_U3CIsLoadErrorU3Ek__BackingField_5() const { return ___U3CIsLoadErrorU3Ek__BackingField_5; }
	inline bool* get_address_of_U3CIsLoadErrorU3Ek__BackingField_5() { return &___U3CIsLoadErrorU3Ek__BackingField_5; }
	inline void set_U3CIsLoadErrorU3Ek__BackingField_5(bool value)
	{
		___U3CIsLoadErrorU3Ek__BackingField_5 = value;
	}

	inline static int32_t get_offset_of_U3CLoadErrorMsgU3Ek__BackingField_6() { return static_cast<int32_t>(offsetof(AssetFileBase_t4101360697, ___U3CLoadErrorMsgU3Ek__BackingField_6)); }
	inline String_t* get_U3CLoadErrorMsgU3Ek__BackingField_6() const { return ___U3CLoadErrorMsgU3Ek__BackingField_6; }
	inline String_t** get_address_of_U3CLoadErrorMsgU3Ek__BackingField_6() { return &___U3CLoadErrorMsgU3Ek__BackingField_6; }
	inline void set_U3CLoadErrorMsgU3Ek__BackingField_6(String_t* value)
	{
		___U3CLoadErrorMsgU3Ek__BackingField_6 = value;
		Il2CppCodeGenWriteBarrier((&___U3CLoadErrorMsgU3Ek__BackingField_6), value);
	}

	inline static int32_t get_offset_of_U3CTextU3Ek__BackingField_7() { return static_cast<int32_t>(offsetof(AssetFileBase_t4101360697, ___U3CTextU3Ek__BackingField_7)); }
	inline TextAsset_t3973159845 * get_U3CTextU3Ek__BackingField_7() const { return ___U3CTextU3Ek__BackingField_7; }
	inline TextAsset_t3973159845 ** get_address_of_U3CTextU3Ek__BackingField_7() { return &___U3CTextU3Ek__BackingField_7; }
	inline void set_U3CTextU3Ek__BackingField_7(TextAsset_t3973159845 * value)
	{
		___U3CTextU3Ek__BackingField_7 = value;
		Il2CppCodeGenWriteBarrier((&___U3CTextU3Ek__BackingField_7), value);
	}

	inline static int32_t get_offset_of_U3CTextureU3Ek__BackingField_8() { return static_cast<int32_t>(offsetof(AssetFileBase_t4101360697, ___U3CTextureU3Ek__BackingField_8)); }
	inline Texture2D_t3542995729 * get_U3CTextureU3Ek__BackingField_8() const { return ___U3CTextureU3Ek__BackingField_8; }
	inline Texture2D_t3542995729 ** get_address_of_U3CTextureU3Ek__BackingField_8() { return &___U3CTextureU3Ek__BackingField_8; }
	inline void set_U3CTextureU3Ek__BackingField_8(Texture2D_t3542995729 * value)
	{
		___U3CTextureU3Ek__BackingField_8 = value;
		Il2CppCodeGenWriteBarrier((&___U3CTextureU3Ek__BackingField_8), value);
	}

	inline static int32_t get_offset_of_U3CSoundU3Ek__BackingField_9() { return static_cast<int32_t>(offsetof(AssetFileBase_t4101360697, ___U3CSoundU3Ek__BackingField_9)); }
	inline AudioClip_t1932558630 * get_U3CSoundU3Ek__BackingField_9() const { return ___U3CSoundU3Ek__BackingField_9; }
	inline AudioClip_t1932558630 ** get_address_of_U3CSoundU3Ek__BackingField_9() { return &___U3CSoundU3Ek__BackingField_9; }
	inline void set_U3CSoundU3Ek__BackingField_9(AudioClip_t1932558630 * value)
	{
		___U3CSoundU3Ek__BackingField_9 = value;
		Il2CppCodeGenWriteBarrier((&___U3CSoundU3Ek__BackingField_9), value);
	}

	inline static int32_t get_offset_of_U3CUnityObjectU3Ek__BackingField_10() { return static_cast<int32_t>(offsetof(AssetFileBase_t4101360697, ___U3CUnityObjectU3Ek__BackingField_10)); }
	inline Object_t1021602117 * get_U3CUnityObjectU3Ek__BackingField_10() const { return ___U3CUnityObjectU3Ek__BackingField_10; }
	inline Object_t1021602117 ** get_address_of_U3CUnityObjectU3Ek__BackingField_10() { return &___U3CUnityObjectU3Ek__BackingField_10; }
	inline void set_U3CUnityObjectU3Ek__BackingField_10(Object_t1021602117 * value)
	{
		___U3CUnityObjectU3Ek__BackingField_10 = value;
		Il2CppCodeGenWriteBarrier((&___U3CUnityObjectU3Ek__BackingField_10), value);
	}

	inline static int32_t get_offset_of_U3CPriorityU3Ek__BackingField_11() { return static_cast<int32_t>(offsetof(AssetFileBase_t4101360697, ___U3CPriorityU3Ek__BackingField_11)); }
	inline int32_t get_U3CPriorityU3Ek__BackingField_11() const { return ___U3CPriorityU3Ek__BackingField_11; }
	inline int32_t* get_address_of_U3CPriorityU3Ek__BackingField_11() { return &___U3CPriorityU3Ek__BackingField_11; }
	inline void set_U3CPriorityU3Ek__BackingField_11(int32_t value)
	{
		___U3CPriorityU3Ek__BackingField_11 = value;
	}

	inline static int32_t get_offset_of_U3CIgnoreUnloadU3Ek__BackingField_12() { return static_cast<int32_t>(offsetof(AssetFileBase_t4101360697, ___U3CIgnoreUnloadU3Ek__BackingField_12)); }
	inline bool get_U3CIgnoreUnloadU3Ek__BackingField_12() const { return ___U3CIgnoreUnloadU3Ek__BackingField_12; }
	inline bool* get_address_of_U3CIgnoreUnloadU3Ek__BackingField_12() { return &___U3CIgnoreUnloadU3Ek__BackingField_12; }
	inline void set_U3CIgnoreUnloadU3Ek__BackingField_12(bool value)
	{
		___U3CIgnoreUnloadU3Ek__BackingField_12 = value;
	}

	inline static int32_t get_offset_of_referenceSet_13() { return static_cast<int32_t>(offsetof(AssetFileBase_t4101360697, ___referenceSet_13)); }
	inline HashSet_1_t1022910149 * get_referenceSet_13() const { return ___referenceSet_13; }
	inline HashSet_1_t1022910149 ** get_address_of_referenceSet_13() { return &___referenceSet_13; }
	inline void set_referenceSet_13(HashSet_1_t1022910149 * value)
	{
		___referenceSet_13 = value;
		Il2CppCodeGenWriteBarrier((&___referenceSet_13), value);
	}
};

struct AssetFileBase_t4101360697_StaticFields
{
public:
	// System.Predicate`1<System.Object> Utage.AssetFileBase::<>f__am$cache0
	Predicate_1_t1132419410 * ___U3CU3Ef__amU24cache0_14;

public:
	inline static int32_t get_offset_of_U3CU3Ef__amU24cache0_14() { return static_cast<int32_t>(offsetof(AssetFileBase_t4101360697_StaticFields, ___U3CU3Ef__amU24cache0_14)); }
	inline Predicate_1_t1132419410 * get_U3CU3Ef__amU24cache0_14() const { return ___U3CU3Ef__amU24cache0_14; }
	inline Predicate_1_t1132419410 ** get_address_of_U3CU3Ef__amU24cache0_14() { return &___U3CU3Ef__amU24cache0_14; }
	inline void set_U3CU3Ef__amU24cache0_14(Predicate_1_t1132419410 * value)
	{
		___U3CU3Ef__amU24cache0_14 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache0_14), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ASSETFILEBASE_T4101360697_H
#ifndef MULTICASTDELEGATE_T3201952435_H
#define MULTICASTDELEGATE_T3201952435_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.MulticastDelegate
struct  MulticastDelegate_t3201952435  : public Delegate_t3022476291
{
public:
	// System.MulticastDelegate System.MulticastDelegate::prev
	MulticastDelegate_t3201952435 * ___prev_9;
	// System.MulticastDelegate System.MulticastDelegate::kpm_next
	MulticastDelegate_t3201952435 * ___kpm_next_10;

public:
	inline static int32_t get_offset_of_prev_9() { return static_cast<int32_t>(offsetof(MulticastDelegate_t3201952435, ___prev_9)); }
	inline MulticastDelegate_t3201952435 * get_prev_9() const { return ___prev_9; }
	inline MulticastDelegate_t3201952435 ** get_address_of_prev_9() { return &___prev_9; }
	inline void set_prev_9(MulticastDelegate_t3201952435 * value)
	{
		___prev_9 = value;
		Il2CppCodeGenWriteBarrier((&___prev_9), value);
	}

	inline static int32_t get_offset_of_kpm_next_10() { return static_cast<int32_t>(offsetof(MulticastDelegate_t3201952435, ___kpm_next_10)); }
	inline MulticastDelegate_t3201952435 * get_kpm_next_10() const { return ___kpm_next_10; }
	inline MulticastDelegate_t3201952435 ** get_address_of_kpm_next_10() { return &___kpm_next_10; }
	inline void set_kpm_next_10(MulticastDelegate_t3201952435 * value)
	{
		___kpm_next_10 = value;
		Il2CppCodeGenWriteBarrier((&___kpm_next_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MULTICASTDELEGATE_T3201952435_H
#ifndef ADVGRAPHICGROUP_T2205048404_H
#define ADVGRAPHICGROUP_T2205048404_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.AdvGraphicGroup
struct  AdvGraphicGroup_t2205048404  : public RuntimeObject
{
public:
	// Utage.AdvLayerSettingData/LayerType Utage.AdvGraphicGroup::type
	int32_t ___type_0;
	// Utage.AdvGraphicLayer Utage.AdvGraphicGroup::<DefaultLayer>k__BackingField
	AdvGraphicLayer_t3630223822 * ___U3CDefaultLayerU3Ek__BackingField_1;
	// Utage.AdvGraphicManager Utage.AdvGraphicGroup::manager
	AdvGraphicManager_t3661251000 * ___manager_2;
	// System.Collections.Generic.List`1<Utage.AdvGraphicLayer> Utage.AdvGraphicGroup::layers
	List_1_t2999344954 * ___layers_3;

public:
	inline static int32_t get_offset_of_type_0() { return static_cast<int32_t>(offsetof(AdvGraphicGroup_t2205048404, ___type_0)); }
	inline int32_t get_type_0() const { return ___type_0; }
	inline int32_t* get_address_of_type_0() { return &___type_0; }
	inline void set_type_0(int32_t value)
	{
		___type_0 = value;
	}

	inline static int32_t get_offset_of_U3CDefaultLayerU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(AdvGraphicGroup_t2205048404, ___U3CDefaultLayerU3Ek__BackingField_1)); }
	inline AdvGraphicLayer_t3630223822 * get_U3CDefaultLayerU3Ek__BackingField_1() const { return ___U3CDefaultLayerU3Ek__BackingField_1; }
	inline AdvGraphicLayer_t3630223822 ** get_address_of_U3CDefaultLayerU3Ek__BackingField_1() { return &___U3CDefaultLayerU3Ek__BackingField_1; }
	inline void set_U3CDefaultLayerU3Ek__BackingField_1(AdvGraphicLayer_t3630223822 * value)
	{
		___U3CDefaultLayerU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CDefaultLayerU3Ek__BackingField_1), value);
	}

	inline static int32_t get_offset_of_manager_2() { return static_cast<int32_t>(offsetof(AdvGraphicGroup_t2205048404, ___manager_2)); }
	inline AdvGraphicManager_t3661251000 * get_manager_2() const { return ___manager_2; }
	inline AdvGraphicManager_t3661251000 ** get_address_of_manager_2() { return &___manager_2; }
	inline void set_manager_2(AdvGraphicManager_t3661251000 * value)
	{
		___manager_2 = value;
		Il2CppCodeGenWriteBarrier((&___manager_2), value);
	}

	inline static int32_t get_offset_of_layers_3() { return static_cast<int32_t>(offsetof(AdvGraphicGroup_t2205048404, ___layers_3)); }
	inline List_1_t2999344954 * get_layers_3() const { return ___layers_3; }
	inline List_1_t2999344954 ** get_address_of_layers_3() { return &___layers_3; }
	inline void set_layers_3(List_1_t2999344954 * value)
	{
		___layers_3 = value;
		Il2CppCodeGenWriteBarrier((&___layers_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ADVGRAPHICGROUP_T2205048404_H
#ifndef DATETIME_T693205669_H
#define DATETIME_T693205669_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.DateTime
struct  DateTime_t693205669 
{
public:
	// System.TimeSpan System.DateTime::ticks
	TimeSpan_t3430258949  ___ticks_10;
	// System.DateTimeKind System.DateTime::kind
	int32_t ___kind_11;

public:
	inline static int32_t get_offset_of_ticks_10() { return static_cast<int32_t>(offsetof(DateTime_t693205669, ___ticks_10)); }
	inline TimeSpan_t3430258949  get_ticks_10() const { return ___ticks_10; }
	inline TimeSpan_t3430258949 * get_address_of_ticks_10() { return &___ticks_10; }
	inline void set_ticks_10(TimeSpan_t3430258949  value)
	{
		___ticks_10 = value;
	}

	inline static int32_t get_offset_of_kind_11() { return static_cast<int32_t>(offsetof(DateTime_t693205669, ___kind_11)); }
	inline int32_t get_kind_11() const { return ___kind_11; }
	inline int32_t* get_address_of_kind_11() { return &___kind_11; }
	inline void set_kind_11(int32_t value)
	{
		___kind_11 = value;
	}
};

struct DateTime_t693205669_StaticFields
{
public:
	// System.DateTime System.DateTime::MaxValue
	DateTime_t693205669  ___MaxValue_12;
	// System.DateTime System.DateTime::MinValue
	DateTime_t693205669  ___MinValue_13;
	// System.String[] System.DateTime::ParseTimeFormats
	StringU5BU5D_t1642385972* ___ParseTimeFormats_14;
	// System.String[] System.DateTime::ParseYearDayMonthFormats
	StringU5BU5D_t1642385972* ___ParseYearDayMonthFormats_15;
	// System.String[] System.DateTime::ParseYearMonthDayFormats
	StringU5BU5D_t1642385972* ___ParseYearMonthDayFormats_16;
	// System.String[] System.DateTime::ParseDayMonthYearFormats
	StringU5BU5D_t1642385972* ___ParseDayMonthYearFormats_17;
	// System.String[] System.DateTime::ParseMonthDayYearFormats
	StringU5BU5D_t1642385972* ___ParseMonthDayYearFormats_18;
	// System.String[] System.DateTime::MonthDayShortFormats
	StringU5BU5D_t1642385972* ___MonthDayShortFormats_19;
	// System.String[] System.DateTime::DayMonthShortFormats
	StringU5BU5D_t1642385972* ___DayMonthShortFormats_20;
	// System.Int32[] System.DateTime::daysmonth
	Int32U5BU5D_t3030399641* ___daysmonth_21;
	// System.Int32[] System.DateTime::daysmonthleap
	Int32U5BU5D_t3030399641* ___daysmonthleap_22;
	// System.Object System.DateTime::to_local_time_span_object
	RuntimeObject * ___to_local_time_span_object_23;
	// System.Int64 System.DateTime::last_now
	int64_t ___last_now_24;

public:
	inline static int32_t get_offset_of_MaxValue_12() { return static_cast<int32_t>(offsetof(DateTime_t693205669_StaticFields, ___MaxValue_12)); }
	inline DateTime_t693205669  get_MaxValue_12() const { return ___MaxValue_12; }
	inline DateTime_t693205669 * get_address_of_MaxValue_12() { return &___MaxValue_12; }
	inline void set_MaxValue_12(DateTime_t693205669  value)
	{
		___MaxValue_12 = value;
	}

	inline static int32_t get_offset_of_MinValue_13() { return static_cast<int32_t>(offsetof(DateTime_t693205669_StaticFields, ___MinValue_13)); }
	inline DateTime_t693205669  get_MinValue_13() const { return ___MinValue_13; }
	inline DateTime_t693205669 * get_address_of_MinValue_13() { return &___MinValue_13; }
	inline void set_MinValue_13(DateTime_t693205669  value)
	{
		___MinValue_13 = value;
	}

	inline static int32_t get_offset_of_ParseTimeFormats_14() { return static_cast<int32_t>(offsetof(DateTime_t693205669_StaticFields, ___ParseTimeFormats_14)); }
	inline StringU5BU5D_t1642385972* get_ParseTimeFormats_14() const { return ___ParseTimeFormats_14; }
	inline StringU5BU5D_t1642385972** get_address_of_ParseTimeFormats_14() { return &___ParseTimeFormats_14; }
	inline void set_ParseTimeFormats_14(StringU5BU5D_t1642385972* value)
	{
		___ParseTimeFormats_14 = value;
		Il2CppCodeGenWriteBarrier((&___ParseTimeFormats_14), value);
	}

	inline static int32_t get_offset_of_ParseYearDayMonthFormats_15() { return static_cast<int32_t>(offsetof(DateTime_t693205669_StaticFields, ___ParseYearDayMonthFormats_15)); }
	inline StringU5BU5D_t1642385972* get_ParseYearDayMonthFormats_15() const { return ___ParseYearDayMonthFormats_15; }
	inline StringU5BU5D_t1642385972** get_address_of_ParseYearDayMonthFormats_15() { return &___ParseYearDayMonthFormats_15; }
	inline void set_ParseYearDayMonthFormats_15(StringU5BU5D_t1642385972* value)
	{
		___ParseYearDayMonthFormats_15 = value;
		Il2CppCodeGenWriteBarrier((&___ParseYearDayMonthFormats_15), value);
	}

	inline static int32_t get_offset_of_ParseYearMonthDayFormats_16() { return static_cast<int32_t>(offsetof(DateTime_t693205669_StaticFields, ___ParseYearMonthDayFormats_16)); }
	inline StringU5BU5D_t1642385972* get_ParseYearMonthDayFormats_16() const { return ___ParseYearMonthDayFormats_16; }
	inline StringU5BU5D_t1642385972** get_address_of_ParseYearMonthDayFormats_16() { return &___ParseYearMonthDayFormats_16; }
	inline void set_ParseYearMonthDayFormats_16(StringU5BU5D_t1642385972* value)
	{
		___ParseYearMonthDayFormats_16 = value;
		Il2CppCodeGenWriteBarrier((&___ParseYearMonthDayFormats_16), value);
	}

	inline static int32_t get_offset_of_ParseDayMonthYearFormats_17() { return static_cast<int32_t>(offsetof(DateTime_t693205669_StaticFields, ___ParseDayMonthYearFormats_17)); }
	inline StringU5BU5D_t1642385972* get_ParseDayMonthYearFormats_17() const { return ___ParseDayMonthYearFormats_17; }
	inline StringU5BU5D_t1642385972** get_address_of_ParseDayMonthYearFormats_17() { return &___ParseDayMonthYearFormats_17; }
	inline void set_ParseDayMonthYearFormats_17(StringU5BU5D_t1642385972* value)
	{
		___ParseDayMonthYearFormats_17 = value;
		Il2CppCodeGenWriteBarrier((&___ParseDayMonthYearFormats_17), value);
	}

	inline static int32_t get_offset_of_ParseMonthDayYearFormats_18() { return static_cast<int32_t>(offsetof(DateTime_t693205669_StaticFields, ___ParseMonthDayYearFormats_18)); }
	inline StringU5BU5D_t1642385972* get_ParseMonthDayYearFormats_18() const { return ___ParseMonthDayYearFormats_18; }
	inline StringU5BU5D_t1642385972** get_address_of_ParseMonthDayYearFormats_18() { return &___ParseMonthDayYearFormats_18; }
	inline void set_ParseMonthDayYearFormats_18(StringU5BU5D_t1642385972* value)
	{
		___ParseMonthDayYearFormats_18 = value;
		Il2CppCodeGenWriteBarrier((&___ParseMonthDayYearFormats_18), value);
	}

	inline static int32_t get_offset_of_MonthDayShortFormats_19() { return static_cast<int32_t>(offsetof(DateTime_t693205669_StaticFields, ___MonthDayShortFormats_19)); }
	inline StringU5BU5D_t1642385972* get_MonthDayShortFormats_19() const { return ___MonthDayShortFormats_19; }
	inline StringU5BU5D_t1642385972** get_address_of_MonthDayShortFormats_19() { return &___MonthDayShortFormats_19; }
	inline void set_MonthDayShortFormats_19(StringU5BU5D_t1642385972* value)
	{
		___MonthDayShortFormats_19 = value;
		Il2CppCodeGenWriteBarrier((&___MonthDayShortFormats_19), value);
	}

	inline static int32_t get_offset_of_DayMonthShortFormats_20() { return static_cast<int32_t>(offsetof(DateTime_t693205669_StaticFields, ___DayMonthShortFormats_20)); }
	inline StringU5BU5D_t1642385972* get_DayMonthShortFormats_20() const { return ___DayMonthShortFormats_20; }
	inline StringU5BU5D_t1642385972** get_address_of_DayMonthShortFormats_20() { return &___DayMonthShortFormats_20; }
	inline void set_DayMonthShortFormats_20(StringU5BU5D_t1642385972* value)
	{
		___DayMonthShortFormats_20 = value;
		Il2CppCodeGenWriteBarrier((&___DayMonthShortFormats_20), value);
	}

	inline static int32_t get_offset_of_daysmonth_21() { return static_cast<int32_t>(offsetof(DateTime_t693205669_StaticFields, ___daysmonth_21)); }
	inline Int32U5BU5D_t3030399641* get_daysmonth_21() const { return ___daysmonth_21; }
	inline Int32U5BU5D_t3030399641** get_address_of_daysmonth_21() { return &___daysmonth_21; }
	inline void set_daysmonth_21(Int32U5BU5D_t3030399641* value)
	{
		___daysmonth_21 = value;
		Il2CppCodeGenWriteBarrier((&___daysmonth_21), value);
	}

	inline static int32_t get_offset_of_daysmonthleap_22() { return static_cast<int32_t>(offsetof(DateTime_t693205669_StaticFields, ___daysmonthleap_22)); }
	inline Int32U5BU5D_t3030399641* get_daysmonthleap_22() const { return ___daysmonthleap_22; }
	inline Int32U5BU5D_t3030399641** get_address_of_daysmonthleap_22() { return &___daysmonthleap_22; }
	inline void set_daysmonthleap_22(Int32U5BU5D_t3030399641* value)
	{
		___daysmonthleap_22 = value;
		Il2CppCodeGenWriteBarrier((&___daysmonthleap_22), value);
	}

	inline static int32_t get_offset_of_to_local_time_span_object_23() { return static_cast<int32_t>(offsetof(DateTime_t693205669_StaticFields, ___to_local_time_span_object_23)); }
	inline RuntimeObject * get_to_local_time_span_object_23() const { return ___to_local_time_span_object_23; }
	inline RuntimeObject ** get_address_of_to_local_time_span_object_23() { return &___to_local_time_span_object_23; }
	inline void set_to_local_time_span_object_23(RuntimeObject * value)
	{
		___to_local_time_span_object_23 = value;
		Il2CppCodeGenWriteBarrier((&___to_local_time_span_object_23), value);
	}

	inline static int32_t get_offset_of_last_now_24() { return static_cast<int32_t>(offsetof(DateTime_t693205669_StaticFields, ___last_now_24)); }
	inline int64_t get_last_now_24() const { return ___last_now_24; }
	inline int64_t* get_address_of_last_now_24() { return &___last_now_24; }
	inline void set_last_now_24(int64_t value)
	{
		___last_now_24 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DATETIME_T693205669_H
#ifndef ADVCONFIGSAVEDATA_T4190953152_H
#define ADVCONFIGSAVEDATA_T4190953152_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.AdvConfigSaveData
struct  AdvConfigSaveData_t4190953152  : public RuntimeObject
{
public:
	// System.Boolean Utage.AdvConfigSaveData::isFullScreen
	bool ___isFullScreen_0;
	// System.Boolean Utage.AdvConfigSaveData::isMouseWheelSendMessage
	bool ___isMouseWheelSendMessage_1;
	// System.Boolean Utage.AdvConfigSaveData::isEffect
	bool ___isEffect_2;
	// System.Boolean Utage.AdvConfigSaveData::isSkipUnread
	bool ___isSkipUnread_3;
	// System.Boolean Utage.AdvConfigSaveData::isStopSkipInSelection
	bool ___isStopSkipInSelection_4;
	// System.Single Utage.AdvConfigSaveData::messageSpeed
	float ___messageSpeed_5;
	// System.Single Utage.AdvConfigSaveData::autoBrPageSpeed
	float ___autoBrPageSpeed_6;
	// System.Single Utage.AdvConfigSaveData::messageWindowTransparency
	float ___messageWindowTransparency_7;
	// System.Single Utage.AdvConfigSaveData::soundMasterVolume
	float ___soundMasterVolume_8;
	// System.Single Utage.AdvConfigSaveData::bgmVolume
	float ___bgmVolume_9;
	// System.Single Utage.AdvConfigSaveData::seVolume
	float ___seVolume_10;
	// System.Single Utage.AdvConfigSaveData::ambienceVolume
	float ___ambienceVolume_11;
	// System.Single Utage.AdvConfigSaveData::voiceVolume
	float ___voiceVolume_12;
	// Utage.VoiceStopType Utage.AdvConfigSaveData::voiceStopType
	int32_t ___voiceStopType_13;
	// System.Boolean Utage.AdvConfigSaveData::isAutoBrPage
	bool ___isAutoBrPage_14;
	// System.Single Utage.AdvConfigSaveData::messageSpeedRead
	float ___messageSpeedRead_15;
	// System.Boolean Utage.AdvConfigSaveData::hideMessageWindowOnPlayingVoice
	bool ___hideMessageWindowOnPlayingVoice_16;
	// System.Collections.Generic.List`1<Utage.AdvConfigSaveData/TaggedMasterVolume> Utage.AdvConfigSaveData::taggedMasterVolumeList
	List_1_t724273889 * ___taggedMasterVolumeList_17;

public:
	inline static int32_t get_offset_of_isFullScreen_0() { return static_cast<int32_t>(offsetof(AdvConfigSaveData_t4190953152, ___isFullScreen_0)); }
	inline bool get_isFullScreen_0() const { return ___isFullScreen_0; }
	inline bool* get_address_of_isFullScreen_0() { return &___isFullScreen_0; }
	inline void set_isFullScreen_0(bool value)
	{
		___isFullScreen_0 = value;
	}

	inline static int32_t get_offset_of_isMouseWheelSendMessage_1() { return static_cast<int32_t>(offsetof(AdvConfigSaveData_t4190953152, ___isMouseWheelSendMessage_1)); }
	inline bool get_isMouseWheelSendMessage_1() const { return ___isMouseWheelSendMessage_1; }
	inline bool* get_address_of_isMouseWheelSendMessage_1() { return &___isMouseWheelSendMessage_1; }
	inline void set_isMouseWheelSendMessage_1(bool value)
	{
		___isMouseWheelSendMessage_1 = value;
	}

	inline static int32_t get_offset_of_isEffect_2() { return static_cast<int32_t>(offsetof(AdvConfigSaveData_t4190953152, ___isEffect_2)); }
	inline bool get_isEffect_2() const { return ___isEffect_2; }
	inline bool* get_address_of_isEffect_2() { return &___isEffect_2; }
	inline void set_isEffect_2(bool value)
	{
		___isEffect_2 = value;
	}

	inline static int32_t get_offset_of_isSkipUnread_3() { return static_cast<int32_t>(offsetof(AdvConfigSaveData_t4190953152, ___isSkipUnread_3)); }
	inline bool get_isSkipUnread_3() const { return ___isSkipUnread_3; }
	inline bool* get_address_of_isSkipUnread_3() { return &___isSkipUnread_3; }
	inline void set_isSkipUnread_3(bool value)
	{
		___isSkipUnread_3 = value;
	}

	inline static int32_t get_offset_of_isStopSkipInSelection_4() { return static_cast<int32_t>(offsetof(AdvConfigSaveData_t4190953152, ___isStopSkipInSelection_4)); }
	inline bool get_isStopSkipInSelection_4() const { return ___isStopSkipInSelection_4; }
	inline bool* get_address_of_isStopSkipInSelection_4() { return &___isStopSkipInSelection_4; }
	inline void set_isStopSkipInSelection_4(bool value)
	{
		___isStopSkipInSelection_4 = value;
	}

	inline static int32_t get_offset_of_messageSpeed_5() { return static_cast<int32_t>(offsetof(AdvConfigSaveData_t4190953152, ___messageSpeed_5)); }
	inline float get_messageSpeed_5() const { return ___messageSpeed_5; }
	inline float* get_address_of_messageSpeed_5() { return &___messageSpeed_5; }
	inline void set_messageSpeed_5(float value)
	{
		___messageSpeed_5 = value;
	}

	inline static int32_t get_offset_of_autoBrPageSpeed_6() { return static_cast<int32_t>(offsetof(AdvConfigSaveData_t4190953152, ___autoBrPageSpeed_6)); }
	inline float get_autoBrPageSpeed_6() const { return ___autoBrPageSpeed_6; }
	inline float* get_address_of_autoBrPageSpeed_6() { return &___autoBrPageSpeed_6; }
	inline void set_autoBrPageSpeed_6(float value)
	{
		___autoBrPageSpeed_6 = value;
	}

	inline static int32_t get_offset_of_messageWindowTransparency_7() { return static_cast<int32_t>(offsetof(AdvConfigSaveData_t4190953152, ___messageWindowTransparency_7)); }
	inline float get_messageWindowTransparency_7() const { return ___messageWindowTransparency_7; }
	inline float* get_address_of_messageWindowTransparency_7() { return &___messageWindowTransparency_7; }
	inline void set_messageWindowTransparency_7(float value)
	{
		___messageWindowTransparency_7 = value;
	}

	inline static int32_t get_offset_of_soundMasterVolume_8() { return static_cast<int32_t>(offsetof(AdvConfigSaveData_t4190953152, ___soundMasterVolume_8)); }
	inline float get_soundMasterVolume_8() const { return ___soundMasterVolume_8; }
	inline float* get_address_of_soundMasterVolume_8() { return &___soundMasterVolume_8; }
	inline void set_soundMasterVolume_8(float value)
	{
		___soundMasterVolume_8 = value;
	}

	inline static int32_t get_offset_of_bgmVolume_9() { return static_cast<int32_t>(offsetof(AdvConfigSaveData_t4190953152, ___bgmVolume_9)); }
	inline float get_bgmVolume_9() const { return ___bgmVolume_9; }
	inline float* get_address_of_bgmVolume_9() { return &___bgmVolume_9; }
	inline void set_bgmVolume_9(float value)
	{
		___bgmVolume_9 = value;
	}

	inline static int32_t get_offset_of_seVolume_10() { return static_cast<int32_t>(offsetof(AdvConfigSaveData_t4190953152, ___seVolume_10)); }
	inline float get_seVolume_10() const { return ___seVolume_10; }
	inline float* get_address_of_seVolume_10() { return &___seVolume_10; }
	inline void set_seVolume_10(float value)
	{
		___seVolume_10 = value;
	}

	inline static int32_t get_offset_of_ambienceVolume_11() { return static_cast<int32_t>(offsetof(AdvConfigSaveData_t4190953152, ___ambienceVolume_11)); }
	inline float get_ambienceVolume_11() const { return ___ambienceVolume_11; }
	inline float* get_address_of_ambienceVolume_11() { return &___ambienceVolume_11; }
	inline void set_ambienceVolume_11(float value)
	{
		___ambienceVolume_11 = value;
	}

	inline static int32_t get_offset_of_voiceVolume_12() { return static_cast<int32_t>(offsetof(AdvConfigSaveData_t4190953152, ___voiceVolume_12)); }
	inline float get_voiceVolume_12() const { return ___voiceVolume_12; }
	inline float* get_address_of_voiceVolume_12() { return &___voiceVolume_12; }
	inline void set_voiceVolume_12(float value)
	{
		___voiceVolume_12 = value;
	}

	inline static int32_t get_offset_of_voiceStopType_13() { return static_cast<int32_t>(offsetof(AdvConfigSaveData_t4190953152, ___voiceStopType_13)); }
	inline int32_t get_voiceStopType_13() const { return ___voiceStopType_13; }
	inline int32_t* get_address_of_voiceStopType_13() { return &___voiceStopType_13; }
	inline void set_voiceStopType_13(int32_t value)
	{
		___voiceStopType_13 = value;
	}

	inline static int32_t get_offset_of_isAutoBrPage_14() { return static_cast<int32_t>(offsetof(AdvConfigSaveData_t4190953152, ___isAutoBrPage_14)); }
	inline bool get_isAutoBrPage_14() const { return ___isAutoBrPage_14; }
	inline bool* get_address_of_isAutoBrPage_14() { return &___isAutoBrPage_14; }
	inline void set_isAutoBrPage_14(bool value)
	{
		___isAutoBrPage_14 = value;
	}

	inline static int32_t get_offset_of_messageSpeedRead_15() { return static_cast<int32_t>(offsetof(AdvConfigSaveData_t4190953152, ___messageSpeedRead_15)); }
	inline float get_messageSpeedRead_15() const { return ___messageSpeedRead_15; }
	inline float* get_address_of_messageSpeedRead_15() { return &___messageSpeedRead_15; }
	inline void set_messageSpeedRead_15(float value)
	{
		___messageSpeedRead_15 = value;
	}

	inline static int32_t get_offset_of_hideMessageWindowOnPlayingVoice_16() { return static_cast<int32_t>(offsetof(AdvConfigSaveData_t4190953152, ___hideMessageWindowOnPlayingVoice_16)); }
	inline bool get_hideMessageWindowOnPlayingVoice_16() const { return ___hideMessageWindowOnPlayingVoice_16; }
	inline bool* get_address_of_hideMessageWindowOnPlayingVoice_16() { return &___hideMessageWindowOnPlayingVoice_16; }
	inline void set_hideMessageWindowOnPlayingVoice_16(bool value)
	{
		___hideMessageWindowOnPlayingVoice_16 = value;
	}

	inline static int32_t get_offset_of_taggedMasterVolumeList_17() { return static_cast<int32_t>(offsetof(AdvConfigSaveData_t4190953152, ___taggedMasterVolumeList_17)); }
	inline List_1_t724273889 * get_taggedMasterVolumeList_17() const { return ___taggedMasterVolumeList_17; }
	inline List_1_t724273889 ** get_address_of_taggedMasterVolumeList_17() { return &___taggedMasterVolumeList_17; }
	inline void set_taggedMasterVolumeList_17(List_1_t724273889 * value)
	{
		___taggedMasterVolumeList_17 = value;
		Il2CppCodeGenWriteBarrier((&___taggedMasterVolumeList_17), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ADVCONFIGSAVEDATA_T4190953152_H
#ifndef BEHAVIOUR_T955675639_H
#define BEHAVIOUR_T955675639_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Behaviour
struct  Behaviour_t955675639  : public Component_t3819376471
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BEHAVIOUR_T955675639_H
#ifndef CREATECUSTOM_T2292834413_H
#define CREATECUSTOM_T2292834413_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.AdvGraphicInfo/CreateCustom
struct  CreateCustom_t2292834413  : public MulticastDelegate_t3201952435
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CREATECUSTOM_T2292834413_H
#ifndef ADVSAVEDATA_T4059487466_H
#define ADVSAVEDATA_T4059487466_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.AdvSaveData
struct  AdvSaveData_t4059487466  : public RuntimeObject
{
public:
	// System.String Utage.AdvSaveData::<Path>k__BackingField
	String_t* ___U3CPathU3Ek__BackingField_0;
	// Utage.AdvSaveData/SaveDataType Utage.AdvSaveData::<Type>k__BackingField
	int32_t ___U3CTypeU3Ek__BackingField_1;
	// System.String Utage.AdvSaveData::<Title>k__BackingField
	String_t* ___U3CTitleU3Ek__BackingField_2;
	// UnityEngine.Texture2D Utage.AdvSaveData::texture
	Texture2D_t3542995729 * ___texture_3;
	// System.DateTime Utage.AdvSaveData::<Date>k__BackingField
	DateTime_t693205669  ___U3CDateU3Ek__BackingField_4;
	// System.Int32 Utage.AdvSaveData::<FileVersion>k__BackingField
	int32_t ___U3CFileVersionU3Ek__BackingField_5;
	// Utage.BinaryBuffer Utage.AdvSaveData::Buffer
	BinaryBuffer_t1215122249 * ___Buffer_8;

public:
	inline static int32_t get_offset_of_U3CPathU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(AdvSaveData_t4059487466, ___U3CPathU3Ek__BackingField_0)); }
	inline String_t* get_U3CPathU3Ek__BackingField_0() const { return ___U3CPathU3Ek__BackingField_0; }
	inline String_t** get_address_of_U3CPathU3Ek__BackingField_0() { return &___U3CPathU3Ek__BackingField_0; }
	inline void set_U3CPathU3Ek__BackingField_0(String_t* value)
	{
		___U3CPathU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CPathU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_U3CTypeU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(AdvSaveData_t4059487466, ___U3CTypeU3Ek__BackingField_1)); }
	inline int32_t get_U3CTypeU3Ek__BackingField_1() const { return ___U3CTypeU3Ek__BackingField_1; }
	inline int32_t* get_address_of_U3CTypeU3Ek__BackingField_1() { return &___U3CTypeU3Ek__BackingField_1; }
	inline void set_U3CTypeU3Ek__BackingField_1(int32_t value)
	{
		___U3CTypeU3Ek__BackingField_1 = value;
	}

	inline static int32_t get_offset_of_U3CTitleU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(AdvSaveData_t4059487466, ___U3CTitleU3Ek__BackingField_2)); }
	inline String_t* get_U3CTitleU3Ek__BackingField_2() const { return ___U3CTitleU3Ek__BackingField_2; }
	inline String_t** get_address_of_U3CTitleU3Ek__BackingField_2() { return &___U3CTitleU3Ek__BackingField_2; }
	inline void set_U3CTitleU3Ek__BackingField_2(String_t* value)
	{
		___U3CTitleU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CTitleU3Ek__BackingField_2), value);
	}

	inline static int32_t get_offset_of_texture_3() { return static_cast<int32_t>(offsetof(AdvSaveData_t4059487466, ___texture_3)); }
	inline Texture2D_t3542995729 * get_texture_3() const { return ___texture_3; }
	inline Texture2D_t3542995729 ** get_address_of_texture_3() { return &___texture_3; }
	inline void set_texture_3(Texture2D_t3542995729 * value)
	{
		___texture_3 = value;
		Il2CppCodeGenWriteBarrier((&___texture_3), value);
	}

	inline static int32_t get_offset_of_U3CDateU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(AdvSaveData_t4059487466, ___U3CDateU3Ek__BackingField_4)); }
	inline DateTime_t693205669  get_U3CDateU3Ek__BackingField_4() const { return ___U3CDateU3Ek__BackingField_4; }
	inline DateTime_t693205669 * get_address_of_U3CDateU3Ek__BackingField_4() { return &___U3CDateU3Ek__BackingField_4; }
	inline void set_U3CDateU3Ek__BackingField_4(DateTime_t693205669  value)
	{
		___U3CDateU3Ek__BackingField_4 = value;
	}

	inline static int32_t get_offset_of_U3CFileVersionU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(AdvSaveData_t4059487466, ___U3CFileVersionU3Ek__BackingField_5)); }
	inline int32_t get_U3CFileVersionU3Ek__BackingField_5() const { return ___U3CFileVersionU3Ek__BackingField_5; }
	inline int32_t* get_address_of_U3CFileVersionU3Ek__BackingField_5() { return &___U3CFileVersionU3Ek__BackingField_5; }
	inline void set_U3CFileVersionU3Ek__BackingField_5(int32_t value)
	{
		___U3CFileVersionU3Ek__BackingField_5 = value;
	}

	inline static int32_t get_offset_of_Buffer_8() { return static_cast<int32_t>(offsetof(AdvSaveData_t4059487466, ___Buffer_8)); }
	inline BinaryBuffer_t1215122249 * get_Buffer_8() const { return ___Buffer_8; }
	inline BinaryBuffer_t1215122249 ** get_address_of_Buffer_8() { return &___Buffer_8; }
	inline void set_Buffer_8(BinaryBuffer_t1215122249 * value)
	{
		___Buffer_8 = value;
		Il2CppCodeGenWriteBarrier((&___Buffer_8), value);
	}
};

struct AdvSaveData_t4059487466_StaticFields
{
public:
	// System.Int32 Utage.AdvSaveData::MagicID
	int32_t ___MagicID_6;

public:
	inline static int32_t get_offset_of_MagicID_6() { return static_cast<int32_t>(offsetof(AdvSaveData_t4059487466_StaticFields, ___MagicID_6)); }
	inline int32_t get_MagicID_6() const { return ___MagicID_6; }
	inline int32_t* get_address_of_MagicID_6() { return &___MagicID_6; }
	inline void set_MagicID_6(int32_t value)
	{
		___MagicID_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ADVSAVEDATA_T4059487466_H
#ifndef ASSETFILEUTAGE_T3568203916_H
#define ASSETFILEUTAGE_T3568203916_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.AssetFileUtage
struct  AssetFileUtage_t3568203916  : public AssetFileBase_t4101360697
{
public:
	// System.String Utage.AssetFileUtage::<LoadPath>k__BackingField
	String_t* ___U3CLoadPathU3Ek__BackingField_15;
	// UnityEngine.AssetBundle Utage.AssetFileUtage::<AssetBundle>k__BackingField
	AssetBundle_t2054978754 * ___U3CAssetBundleU3Ek__BackingField_16;

public:
	inline static int32_t get_offset_of_U3CLoadPathU3Ek__BackingField_15() { return static_cast<int32_t>(offsetof(AssetFileUtage_t3568203916, ___U3CLoadPathU3Ek__BackingField_15)); }
	inline String_t* get_U3CLoadPathU3Ek__BackingField_15() const { return ___U3CLoadPathU3Ek__BackingField_15; }
	inline String_t** get_address_of_U3CLoadPathU3Ek__BackingField_15() { return &___U3CLoadPathU3Ek__BackingField_15; }
	inline void set_U3CLoadPathU3Ek__BackingField_15(String_t* value)
	{
		___U3CLoadPathU3Ek__BackingField_15 = value;
		Il2CppCodeGenWriteBarrier((&___U3CLoadPathU3Ek__BackingField_15), value);
	}

	inline static int32_t get_offset_of_U3CAssetBundleU3Ek__BackingField_16() { return static_cast<int32_t>(offsetof(AssetFileUtage_t3568203916, ___U3CAssetBundleU3Ek__BackingField_16)); }
	inline AssetBundle_t2054978754 * get_U3CAssetBundleU3Ek__BackingField_16() const { return ___U3CAssetBundleU3Ek__BackingField_16; }
	inline AssetBundle_t2054978754 ** get_address_of_U3CAssetBundleU3Ek__BackingField_16() { return &___U3CAssetBundleU3Ek__BackingField_16; }
	inline void set_U3CAssetBundleU3Ek__BackingField_16(AssetBundle_t2054978754 * value)
	{
		___U3CAssetBundleU3Ek__BackingField_16 = value;
		Il2CppCodeGenWriteBarrier((&___U3CAssetBundleU3Ek__BackingField_16), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ASSETFILEUTAGE_T3568203916_H
#ifndef ADVLOCALVIDEOFILE_T3013566383_H
#define ADVLOCALVIDEOFILE_T3013566383_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.AdvLocalVideoFile
struct  AdvLocalVideoFile_t3013566383  : public AssetFileUtage_t3568203916
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ADVLOCALVIDEOFILE_T3013566383_H
#ifndef MONOBEHAVIOUR_T1158329972_H
#define MONOBEHAVIOUR_T1158329972_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.MonoBehaviour
struct  MonoBehaviour_t1158329972  : public Behaviour_t955675639
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MONOBEHAVIOUR_T1158329972_H
#ifndef ADVCLICKEVENT_T2495855495_H
#define ADVCLICKEVENT_T2495855495_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.AdvClickEvent
struct  AdvClickEvent_t2495855495  : public MonoBehaviour_t1158329972
{
public:
	// Utage.AdvGraphicBase Utage.AdvClickEvent::advGraphic
	AdvGraphicBase_t2725442072 * ___advGraphic_2;
	// Utage.StringGridRow Utage.AdvClickEvent::<Row>k__BackingField
	StringGridRow_t4193237197 * ___U3CRowU3Ek__BackingField_3;
	// UnityEngine.Events.UnityAction`1<UnityEngine.EventSystems.BaseEventData> Utage.AdvClickEvent::<action>k__BackingField
	UnityAction_1_t4047591376 * ___U3CactionU3Ek__BackingField_4;

public:
	inline static int32_t get_offset_of_advGraphic_2() { return static_cast<int32_t>(offsetof(AdvClickEvent_t2495855495, ___advGraphic_2)); }
	inline AdvGraphicBase_t2725442072 * get_advGraphic_2() const { return ___advGraphic_2; }
	inline AdvGraphicBase_t2725442072 ** get_address_of_advGraphic_2() { return &___advGraphic_2; }
	inline void set_advGraphic_2(AdvGraphicBase_t2725442072 * value)
	{
		___advGraphic_2 = value;
		Il2CppCodeGenWriteBarrier((&___advGraphic_2), value);
	}

	inline static int32_t get_offset_of_U3CRowU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(AdvClickEvent_t2495855495, ___U3CRowU3Ek__BackingField_3)); }
	inline StringGridRow_t4193237197 * get_U3CRowU3Ek__BackingField_3() const { return ___U3CRowU3Ek__BackingField_3; }
	inline StringGridRow_t4193237197 ** get_address_of_U3CRowU3Ek__BackingField_3() { return &___U3CRowU3Ek__BackingField_3; }
	inline void set_U3CRowU3Ek__BackingField_3(StringGridRow_t4193237197 * value)
	{
		___U3CRowU3Ek__BackingField_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CRowU3Ek__BackingField_3), value);
	}

	inline static int32_t get_offset_of_U3CactionU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(AdvClickEvent_t2495855495, ___U3CactionU3Ek__BackingField_4)); }
	inline UnityAction_1_t4047591376 * get_U3CactionU3Ek__BackingField_4() const { return ___U3CactionU3Ek__BackingField_4; }
	inline UnityAction_1_t4047591376 ** get_address_of_U3CactionU3Ek__BackingField_4() { return &___U3CactionU3Ek__BackingField_4; }
	inline void set_U3CactionU3Ek__BackingField_4(UnityAction_1_t4047591376 * value)
	{
		___U3CactionU3Ek__BackingField_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CactionU3Ek__BackingField_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ADVCLICKEVENT_T2495855495_H
#ifndef ADVGRAPHICMANAGER_T3661251000_H
#define ADVGRAPHICMANAGER_T3661251000_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.AdvGraphicManager
struct  AdvGraphicManager_t3661251000  : public MonoBehaviour_t1158329972
{
public:
	// System.Single Utage.AdvGraphicManager::pixelsToUnits
	float ___pixelsToUnits_2;
	// System.Single Utage.AdvGraphicManager::sortOderToZUnits
	float ___sortOderToZUnits_3;
	// System.String Utage.AdvGraphicManager::bgSpriteName
	String_t* ___bgSpriteName_4;
	// System.Boolean Utage.AdvGraphicManager::debugAutoResetCanvasPosition
	bool ___debugAutoResetCanvasPosition_5;
	// Utage.AdvGraphicRenderTextureManager Utage.AdvGraphicManager::renderTextureManager
	AdvGraphicRenderTextureManager_t995712561 * ___renderTextureManager_6;
	// Utage.AdvVideoManager Utage.AdvGraphicManager::videoManager
	AdvVideoManager_t946313091 * ___videoManager_7;
	// System.Boolean Utage.AdvGraphicManager::isEventMode
	bool ___isEventMode_8;
	// System.Collections.Generic.Dictionary`2<Utage.AdvLayerSettingData/LayerType,Utage.AdvGraphicGroup> Utage.AdvGraphicManager::Groups
	Dictionary_2_t2036845121 * ___Groups_9;
	// Utage.AdvEngine Utage.AdvGraphicManager::engine
	AdvEngine_t1176753927 * ___engine_10;

public:
	inline static int32_t get_offset_of_pixelsToUnits_2() { return static_cast<int32_t>(offsetof(AdvGraphicManager_t3661251000, ___pixelsToUnits_2)); }
	inline float get_pixelsToUnits_2() const { return ___pixelsToUnits_2; }
	inline float* get_address_of_pixelsToUnits_2() { return &___pixelsToUnits_2; }
	inline void set_pixelsToUnits_2(float value)
	{
		___pixelsToUnits_2 = value;
	}

	inline static int32_t get_offset_of_sortOderToZUnits_3() { return static_cast<int32_t>(offsetof(AdvGraphicManager_t3661251000, ___sortOderToZUnits_3)); }
	inline float get_sortOderToZUnits_3() const { return ___sortOderToZUnits_3; }
	inline float* get_address_of_sortOderToZUnits_3() { return &___sortOderToZUnits_3; }
	inline void set_sortOderToZUnits_3(float value)
	{
		___sortOderToZUnits_3 = value;
	}

	inline static int32_t get_offset_of_bgSpriteName_4() { return static_cast<int32_t>(offsetof(AdvGraphicManager_t3661251000, ___bgSpriteName_4)); }
	inline String_t* get_bgSpriteName_4() const { return ___bgSpriteName_4; }
	inline String_t** get_address_of_bgSpriteName_4() { return &___bgSpriteName_4; }
	inline void set_bgSpriteName_4(String_t* value)
	{
		___bgSpriteName_4 = value;
		Il2CppCodeGenWriteBarrier((&___bgSpriteName_4), value);
	}

	inline static int32_t get_offset_of_debugAutoResetCanvasPosition_5() { return static_cast<int32_t>(offsetof(AdvGraphicManager_t3661251000, ___debugAutoResetCanvasPosition_5)); }
	inline bool get_debugAutoResetCanvasPosition_5() const { return ___debugAutoResetCanvasPosition_5; }
	inline bool* get_address_of_debugAutoResetCanvasPosition_5() { return &___debugAutoResetCanvasPosition_5; }
	inline void set_debugAutoResetCanvasPosition_5(bool value)
	{
		___debugAutoResetCanvasPosition_5 = value;
	}

	inline static int32_t get_offset_of_renderTextureManager_6() { return static_cast<int32_t>(offsetof(AdvGraphicManager_t3661251000, ___renderTextureManager_6)); }
	inline AdvGraphicRenderTextureManager_t995712561 * get_renderTextureManager_6() const { return ___renderTextureManager_6; }
	inline AdvGraphicRenderTextureManager_t995712561 ** get_address_of_renderTextureManager_6() { return &___renderTextureManager_6; }
	inline void set_renderTextureManager_6(AdvGraphicRenderTextureManager_t995712561 * value)
	{
		___renderTextureManager_6 = value;
		Il2CppCodeGenWriteBarrier((&___renderTextureManager_6), value);
	}

	inline static int32_t get_offset_of_videoManager_7() { return static_cast<int32_t>(offsetof(AdvGraphicManager_t3661251000, ___videoManager_7)); }
	inline AdvVideoManager_t946313091 * get_videoManager_7() const { return ___videoManager_7; }
	inline AdvVideoManager_t946313091 ** get_address_of_videoManager_7() { return &___videoManager_7; }
	inline void set_videoManager_7(AdvVideoManager_t946313091 * value)
	{
		___videoManager_7 = value;
		Il2CppCodeGenWriteBarrier((&___videoManager_7), value);
	}

	inline static int32_t get_offset_of_isEventMode_8() { return static_cast<int32_t>(offsetof(AdvGraphicManager_t3661251000, ___isEventMode_8)); }
	inline bool get_isEventMode_8() const { return ___isEventMode_8; }
	inline bool* get_address_of_isEventMode_8() { return &___isEventMode_8; }
	inline void set_isEventMode_8(bool value)
	{
		___isEventMode_8 = value;
	}

	inline static int32_t get_offset_of_Groups_9() { return static_cast<int32_t>(offsetof(AdvGraphicManager_t3661251000, ___Groups_9)); }
	inline Dictionary_2_t2036845121 * get_Groups_9() const { return ___Groups_9; }
	inline Dictionary_2_t2036845121 ** get_address_of_Groups_9() { return &___Groups_9; }
	inline void set_Groups_9(Dictionary_2_t2036845121 * value)
	{
		___Groups_9 = value;
		Il2CppCodeGenWriteBarrier((&___Groups_9), value);
	}

	inline static int32_t get_offset_of_engine_10() { return static_cast<int32_t>(offsetof(AdvGraphicManager_t3661251000, ___engine_10)); }
	inline AdvEngine_t1176753927 * get_engine_10() const { return ___engine_10; }
	inline AdvEngine_t1176753927 ** get_address_of_engine_10() { return &___engine_10; }
	inline void set_engine_10(AdvEngine_t1176753927 * value)
	{
		___engine_10 = value;
		Il2CppCodeGenWriteBarrier((&___engine_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ADVGRAPHICMANAGER_T3661251000_H
#ifndef ADVGRAPHICOBJECT_T3651915246_H
#define ADVGRAPHICOBJECT_T3651915246_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.AdvGraphicObject
struct  AdvGraphicObject_t3651915246  : public MonoBehaviour_t1158329972
{
public:
	// Utage.AdvGraphicLoader Utage.AdvGraphicObject::loader
	AdvGraphicLoader_t202702654 * ___loader_2;
	// Utage.AdvGraphicLayer Utage.AdvGraphicObject::layer
	AdvGraphicLayer_t3630223822 * ___layer_3;
	// Utage.AdvGraphicInfo Utage.AdvGraphicObject::<LastResource>k__BackingField
	AdvGraphicInfo_t3545565645 * ___U3CLastResourceU3Ek__BackingField_4;
	// Utage.AdvGraphicBase Utage.AdvGraphicObject::<TargetObject>k__BackingField
	AdvGraphicBase_t2725442072 * ___U3CTargetObjectU3Ek__BackingField_5;
	// Utage.AdvGraphicBase Utage.AdvGraphicObject::<RenderObject>k__BackingField
	AdvGraphicBase_t2725442072 * ___U3CRenderObjectU3Ek__BackingField_6;
	// Utage.AdvRenderTextureSpace Utage.AdvGraphicObject::<RenderTextureSpace>k__BackingField
	AdvRenderTextureSpace_t3482187212 * ___U3CRenderTextureSpaceU3Ek__BackingField_7;
	// Utage.Timer Utage.AdvGraphicObject::<FadeTimer>k__BackingField
	Timer_t2904185433 * ___U3CFadeTimerU3Ek__BackingField_8;
	// Utage.AdvEffectColor Utage.AdvGraphicObject::effectColor
	AdvEffectColor_t2285992559 * ___effectColor_9;
	// UnityEngine.RectTransform Utage.AdvGraphicObject::<rectTransform>k__BackingField
	RectTransform_t3349966182 * ___U3CrectTransformU3Ek__BackingField_10;

public:
	inline static int32_t get_offset_of_loader_2() { return static_cast<int32_t>(offsetof(AdvGraphicObject_t3651915246, ___loader_2)); }
	inline AdvGraphicLoader_t202702654 * get_loader_2() const { return ___loader_2; }
	inline AdvGraphicLoader_t202702654 ** get_address_of_loader_2() { return &___loader_2; }
	inline void set_loader_2(AdvGraphicLoader_t202702654 * value)
	{
		___loader_2 = value;
		Il2CppCodeGenWriteBarrier((&___loader_2), value);
	}

	inline static int32_t get_offset_of_layer_3() { return static_cast<int32_t>(offsetof(AdvGraphicObject_t3651915246, ___layer_3)); }
	inline AdvGraphicLayer_t3630223822 * get_layer_3() const { return ___layer_3; }
	inline AdvGraphicLayer_t3630223822 ** get_address_of_layer_3() { return &___layer_3; }
	inline void set_layer_3(AdvGraphicLayer_t3630223822 * value)
	{
		___layer_3 = value;
		Il2CppCodeGenWriteBarrier((&___layer_3), value);
	}

	inline static int32_t get_offset_of_U3CLastResourceU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(AdvGraphicObject_t3651915246, ___U3CLastResourceU3Ek__BackingField_4)); }
	inline AdvGraphicInfo_t3545565645 * get_U3CLastResourceU3Ek__BackingField_4() const { return ___U3CLastResourceU3Ek__BackingField_4; }
	inline AdvGraphicInfo_t3545565645 ** get_address_of_U3CLastResourceU3Ek__BackingField_4() { return &___U3CLastResourceU3Ek__BackingField_4; }
	inline void set_U3CLastResourceU3Ek__BackingField_4(AdvGraphicInfo_t3545565645 * value)
	{
		___U3CLastResourceU3Ek__BackingField_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CLastResourceU3Ek__BackingField_4), value);
	}

	inline static int32_t get_offset_of_U3CTargetObjectU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(AdvGraphicObject_t3651915246, ___U3CTargetObjectU3Ek__BackingField_5)); }
	inline AdvGraphicBase_t2725442072 * get_U3CTargetObjectU3Ek__BackingField_5() const { return ___U3CTargetObjectU3Ek__BackingField_5; }
	inline AdvGraphicBase_t2725442072 ** get_address_of_U3CTargetObjectU3Ek__BackingField_5() { return &___U3CTargetObjectU3Ek__BackingField_5; }
	inline void set_U3CTargetObjectU3Ek__BackingField_5(AdvGraphicBase_t2725442072 * value)
	{
		___U3CTargetObjectU3Ek__BackingField_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3CTargetObjectU3Ek__BackingField_5), value);
	}

	inline static int32_t get_offset_of_U3CRenderObjectU3Ek__BackingField_6() { return static_cast<int32_t>(offsetof(AdvGraphicObject_t3651915246, ___U3CRenderObjectU3Ek__BackingField_6)); }
	inline AdvGraphicBase_t2725442072 * get_U3CRenderObjectU3Ek__BackingField_6() const { return ___U3CRenderObjectU3Ek__BackingField_6; }
	inline AdvGraphicBase_t2725442072 ** get_address_of_U3CRenderObjectU3Ek__BackingField_6() { return &___U3CRenderObjectU3Ek__BackingField_6; }
	inline void set_U3CRenderObjectU3Ek__BackingField_6(AdvGraphicBase_t2725442072 * value)
	{
		___U3CRenderObjectU3Ek__BackingField_6 = value;
		Il2CppCodeGenWriteBarrier((&___U3CRenderObjectU3Ek__BackingField_6), value);
	}

	inline static int32_t get_offset_of_U3CRenderTextureSpaceU3Ek__BackingField_7() { return static_cast<int32_t>(offsetof(AdvGraphicObject_t3651915246, ___U3CRenderTextureSpaceU3Ek__BackingField_7)); }
	inline AdvRenderTextureSpace_t3482187212 * get_U3CRenderTextureSpaceU3Ek__BackingField_7() const { return ___U3CRenderTextureSpaceU3Ek__BackingField_7; }
	inline AdvRenderTextureSpace_t3482187212 ** get_address_of_U3CRenderTextureSpaceU3Ek__BackingField_7() { return &___U3CRenderTextureSpaceU3Ek__BackingField_7; }
	inline void set_U3CRenderTextureSpaceU3Ek__BackingField_7(AdvRenderTextureSpace_t3482187212 * value)
	{
		___U3CRenderTextureSpaceU3Ek__BackingField_7 = value;
		Il2CppCodeGenWriteBarrier((&___U3CRenderTextureSpaceU3Ek__BackingField_7), value);
	}

	inline static int32_t get_offset_of_U3CFadeTimerU3Ek__BackingField_8() { return static_cast<int32_t>(offsetof(AdvGraphicObject_t3651915246, ___U3CFadeTimerU3Ek__BackingField_8)); }
	inline Timer_t2904185433 * get_U3CFadeTimerU3Ek__BackingField_8() const { return ___U3CFadeTimerU3Ek__BackingField_8; }
	inline Timer_t2904185433 ** get_address_of_U3CFadeTimerU3Ek__BackingField_8() { return &___U3CFadeTimerU3Ek__BackingField_8; }
	inline void set_U3CFadeTimerU3Ek__BackingField_8(Timer_t2904185433 * value)
	{
		___U3CFadeTimerU3Ek__BackingField_8 = value;
		Il2CppCodeGenWriteBarrier((&___U3CFadeTimerU3Ek__BackingField_8), value);
	}

	inline static int32_t get_offset_of_effectColor_9() { return static_cast<int32_t>(offsetof(AdvGraphicObject_t3651915246, ___effectColor_9)); }
	inline AdvEffectColor_t2285992559 * get_effectColor_9() const { return ___effectColor_9; }
	inline AdvEffectColor_t2285992559 ** get_address_of_effectColor_9() { return &___effectColor_9; }
	inline void set_effectColor_9(AdvEffectColor_t2285992559 * value)
	{
		___effectColor_9 = value;
		Il2CppCodeGenWriteBarrier((&___effectColor_9), value);
	}

	inline static int32_t get_offset_of_U3CrectTransformU3Ek__BackingField_10() { return static_cast<int32_t>(offsetof(AdvGraphicObject_t3651915246, ___U3CrectTransformU3Ek__BackingField_10)); }
	inline RectTransform_t3349966182 * get_U3CrectTransformU3Ek__BackingField_10() const { return ___U3CrectTransformU3Ek__BackingField_10; }
	inline RectTransform_t3349966182 ** get_address_of_U3CrectTransformU3Ek__BackingField_10() { return &___U3CrectTransformU3Ek__BackingField_10; }
	inline void set_U3CrectTransformU3Ek__BackingField_10(RectTransform_t3349966182 * value)
	{
		___U3CrectTransformU3Ek__BackingField_10 = value;
		Il2CppCodeGenWriteBarrier((&___U3CrectTransformU3Ek__BackingField_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ADVGRAPHICOBJECT_T3651915246_H
#ifndef ADVGRAPHICLOADER_T202702654_H
#define ADVGRAPHICLOADER_T202702654_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.AdvGraphicLoader
struct  AdvGraphicLoader_t202702654  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.Events.UnityEvent Utage.AdvGraphicLoader::OnComplete
	UnityEvent_t408735097 * ___OnComplete_2;
	// Utage.AdvGraphicInfo Utage.AdvGraphicLoader::graphic
	AdvGraphicInfo_t3545565645 * ___graphic_3;

public:
	inline static int32_t get_offset_of_OnComplete_2() { return static_cast<int32_t>(offsetof(AdvGraphicLoader_t202702654, ___OnComplete_2)); }
	inline UnityEvent_t408735097 * get_OnComplete_2() const { return ___OnComplete_2; }
	inline UnityEvent_t408735097 ** get_address_of_OnComplete_2() { return &___OnComplete_2; }
	inline void set_OnComplete_2(UnityEvent_t408735097 * value)
	{
		___OnComplete_2 = value;
		Il2CppCodeGenWriteBarrier((&___OnComplete_2), value);
	}

	inline static int32_t get_offset_of_graphic_3() { return static_cast<int32_t>(offsetof(AdvGraphicLoader_t202702654, ___graphic_3)); }
	inline AdvGraphicInfo_t3545565645 * get_graphic_3() const { return ___graphic_3; }
	inline AdvGraphicInfo_t3545565645 ** get_address_of_graphic_3() { return &___graphic_3; }
	inline void set_graphic_3(AdvGraphicInfo_t3545565645 * value)
	{
		___graphic_3 = value;
		Il2CppCodeGenWriteBarrier((&___graphic_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ADVGRAPHICLOADER_T202702654_H
#ifndef ADVGRAPHICBASE_T2725442072_H
#define ADVGRAPHICBASE_T2725442072_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.AdvGraphicBase
struct  AdvGraphicBase_t2725442072  : public MonoBehaviour_t1158329972
{
public:
	// Utage.AdvGraphicObject Utage.AdvGraphicBase::<ParentObject>k__BackingField
	AdvGraphicObject_t3651915246 * ___U3CParentObjectU3Ek__BackingField_2;

public:
	inline static int32_t get_offset_of_U3CParentObjectU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(AdvGraphicBase_t2725442072, ___U3CParentObjectU3Ek__BackingField_2)); }
	inline AdvGraphicObject_t3651915246 * get_U3CParentObjectU3Ek__BackingField_2() const { return ___U3CParentObjectU3Ek__BackingField_2; }
	inline AdvGraphicObject_t3651915246 ** get_address_of_U3CParentObjectU3Ek__BackingField_2() { return &___U3CParentObjectU3Ek__BackingField_2; }
	inline void set_U3CParentObjectU3Ek__BackingField_2(AdvGraphicObject_t3651915246 * value)
	{
		___U3CParentObjectU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CParentObjectU3Ek__BackingField_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ADVGRAPHICBASE_T2725442072_H
#ifndef ADVSAVEMANAGER_T2382020483_H
#define ADVSAVEMANAGER_T2382020483_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.AdvSaveManager
struct  AdvSaveManager_t2382020483  : public MonoBehaviour_t1158329972
{
public:
	// Utage.FileIOManager Utage.AdvSaveManager::fileIOManager
	FileIOManager_t855502573 * ___fileIOManager_2;
	// Utage.AdvSaveManager/SaveType Utage.AdvSaveManager::type
	int32_t ___type_3;
	// System.Boolean Utage.AdvSaveManager::isAutoSave
	bool ___isAutoSave_4;
	// System.String Utage.AdvSaveManager::directoryName
	String_t* ___directoryName_5;
	// System.String Utage.AdvSaveManager::fileName
	String_t* ___fileName_6;
	// Utage.AdvSaveManager/SaveSetting Utage.AdvSaveManager::defaultSetting
	SaveSetting_t424301263 * ___defaultSetting_7;
	// Utage.AdvSaveManager/SaveSetting Utage.AdvSaveManager::webPlayerSetting
	SaveSetting_t424301263 * ___webPlayerSetting_8;
	// System.Collections.Generic.List`1<UnityEngine.GameObject> Utage.AdvSaveManager::CustomSaveDataObjects
	List_1_t1125654279 * ___CustomSaveDataObjects_9;
	// System.Collections.Generic.List`1<Utage.IBinaryIO> Utage.AdvSaveManager::saveIoList
	List_1_t767462864 * ___saveIoList_10;
	// Utage.AdvSaveData Utage.AdvSaveManager::autoSaveData
	AdvSaveData_t4059487466 * ___autoSaveData_11;
	// Utage.AdvSaveData Utage.AdvSaveManager::currentAutoSaveData
	AdvSaveData_t4059487466 * ___currentAutoSaveData_12;
	// Utage.AdvSaveData Utage.AdvSaveManager::quickSaveData
	AdvSaveData_t4059487466 * ___quickSaveData_13;
	// System.Collections.Generic.List`1<Utage.AdvSaveData> Utage.AdvSaveManager::saveDataList
	List_1_t3428608598 * ___saveDataList_14;
	// UnityEngine.Texture2D Utage.AdvSaveManager::captureTexture
	Texture2D_t3542995729 * ___captureTexture_15;

public:
	inline static int32_t get_offset_of_fileIOManager_2() { return static_cast<int32_t>(offsetof(AdvSaveManager_t2382020483, ___fileIOManager_2)); }
	inline FileIOManager_t855502573 * get_fileIOManager_2() const { return ___fileIOManager_2; }
	inline FileIOManager_t855502573 ** get_address_of_fileIOManager_2() { return &___fileIOManager_2; }
	inline void set_fileIOManager_2(FileIOManager_t855502573 * value)
	{
		___fileIOManager_2 = value;
		Il2CppCodeGenWriteBarrier((&___fileIOManager_2), value);
	}

	inline static int32_t get_offset_of_type_3() { return static_cast<int32_t>(offsetof(AdvSaveManager_t2382020483, ___type_3)); }
	inline int32_t get_type_3() const { return ___type_3; }
	inline int32_t* get_address_of_type_3() { return &___type_3; }
	inline void set_type_3(int32_t value)
	{
		___type_3 = value;
	}

	inline static int32_t get_offset_of_isAutoSave_4() { return static_cast<int32_t>(offsetof(AdvSaveManager_t2382020483, ___isAutoSave_4)); }
	inline bool get_isAutoSave_4() const { return ___isAutoSave_4; }
	inline bool* get_address_of_isAutoSave_4() { return &___isAutoSave_4; }
	inline void set_isAutoSave_4(bool value)
	{
		___isAutoSave_4 = value;
	}

	inline static int32_t get_offset_of_directoryName_5() { return static_cast<int32_t>(offsetof(AdvSaveManager_t2382020483, ___directoryName_5)); }
	inline String_t* get_directoryName_5() const { return ___directoryName_5; }
	inline String_t** get_address_of_directoryName_5() { return &___directoryName_5; }
	inline void set_directoryName_5(String_t* value)
	{
		___directoryName_5 = value;
		Il2CppCodeGenWriteBarrier((&___directoryName_5), value);
	}

	inline static int32_t get_offset_of_fileName_6() { return static_cast<int32_t>(offsetof(AdvSaveManager_t2382020483, ___fileName_6)); }
	inline String_t* get_fileName_6() const { return ___fileName_6; }
	inline String_t** get_address_of_fileName_6() { return &___fileName_6; }
	inline void set_fileName_6(String_t* value)
	{
		___fileName_6 = value;
		Il2CppCodeGenWriteBarrier((&___fileName_6), value);
	}

	inline static int32_t get_offset_of_defaultSetting_7() { return static_cast<int32_t>(offsetof(AdvSaveManager_t2382020483, ___defaultSetting_7)); }
	inline SaveSetting_t424301263 * get_defaultSetting_7() const { return ___defaultSetting_7; }
	inline SaveSetting_t424301263 ** get_address_of_defaultSetting_7() { return &___defaultSetting_7; }
	inline void set_defaultSetting_7(SaveSetting_t424301263 * value)
	{
		___defaultSetting_7 = value;
		Il2CppCodeGenWriteBarrier((&___defaultSetting_7), value);
	}

	inline static int32_t get_offset_of_webPlayerSetting_8() { return static_cast<int32_t>(offsetof(AdvSaveManager_t2382020483, ___webPlayerSetting_8)); }
	inline SaveSetting_t424301263 * get_webPlayerSetting_8() const { return ___webPlayerSetting_8; }
	inline SaveSetting_t424301263 ** get_address_of_webPlayerSetting_8() { return &___webPlayerSetting_8; }
	inline void set_webPlayerSetting_8(SaveSetting_t424301263 * value)
	{
		___webPlayerSetting_8 = value;
		Il2CppCodeGenWriteBarrier((&___webPlayerSetting_8), value);
	}

	inline static int32_t get_offset_of_CustomSaveDataObjects_9() { return static_cast<int32_t>(offsetof(AdvSaveManager_t2382020483, ___CustomSaveDataObjects_9)); }
	inline List_1_t1125654279 * get_CustomSaveDataObjects_9() const { return ___CustomSaveDataObjects_9; }
	inline List_1_t1125654279 ** get_address_of_CustomSaveDataObjects_9() { return &___CustomSaveDataObjects_9; }
	inline void set_CustomSaveDataObjects_9(List_1_t1125654279 * value)
	{
		___CustomSaveDataObjects_9 = value;
		Il2CppCodeGenWriteBarrier((&___CustomSaveDataObjects_9), value);
	}

	inline static int32_t get_offset_of_saveIoList_10() { return static_cast<int32_t>(offsetof(AdvSaveManager_t2382020483, ___saveIoList_10)); }
	inline List_1_t767462864 * get_saveIoList_10() const { return ___saveIoList_10; }
	inline List_1_t767462864 ** get_address_of_saveIoList_10() { return &___saveIoList_10; }
	inline void set_saveIoList_10(List_1_t767462864 * value)
	{
		___saveIoList_10 = value;
		Il2CppCodeGenWriteBarrier((&___saveIoList_10), value);
	}

	inline static int32_t get_offset_of_autoSaveData_11() { return static_cast<int32_t>(offsetof(AdvSaveManager_t2382020483, ___autoSaveData_11)); }
	inline AdvSaveData_t4059487466 * get_autoSaveData_11() const { return ___autoSaveData_11; }
	inline AdvSaveData_t4059487466 ** get_address_of_autoSaveData_11() { return &___autoSaveData_11; }
	inline void set_autoSaveData_11(AdvSaveData_t4059487466 * value)
	{
		___autoSaveData_11 = value;
		Il2CppCodeGenWriteBarrier((&___autoSaveData_11), value);
	}

	inline static int32_t get_offset_of_currentAutoSaveData_12() { return static_cast<int32_t>(offsetof(AdvSaveManager_t2382020483, ___currentAutoSaveData_12)); }
	inline AdvSaveData_t4059487466 * get_currentAutoSaveData_12() const { return ___currentAutoSaveData_12; }
	inline AdvSaveData_t4059487466 ** get_address_of_currentAutoSaveData_12() { return &___currentAutoSaveData_12; }
	inline void set_currentAutoSaveData_12(AdvSaveData_t4059487466 * value)
	{
		___currentAutoSaveData_12 = value;
		Il2CppCodeGenWriteBarrier((&___currentAutoSaveData_12), value);
	}

	inline static int32_t get_offset_of_quickSaveData_13() { return static_cast<int32_t>(offsetof(AdvSaveManager_t2382020483, ___quickSaveData_13)); }
	inline AdvSaveData_t4059487466 * get_quickSaveData_13() const { return ___quickSaveData_13; }
	inline AdvSaveData_t4059487466 ** get_address_of_quickSaveData_13() { return &___quickSaveData_13; }
	inline void set_quickSaveData_13(AdvSaveData_t4059487466 * value)
	{
		___quickSaveData_13 = value;
		Il2CppCodeGenWriteBarrier((&___quickSaveData_13), value);
	}

	inline static int32_t get_offset_of_saveDataList_14() { return static_cast<int32_t>(offsetof(AdvSaveManager_t2382020483, ___saveDataList_14)); }
	inline List_1_t3428608598 * get_saveDataList_14() const { return ___saveDataList_14; }
	inline List_1_t3428608598 ** get_address_of_saveDataList_14() { return &___saveDataList_14; }
	inline void set_saveDataList_14(List_1_t3428608598 * value)
	{
		___saveDataList_14 = value;
		Il2CppCodeGenWriteBarrier((&___saveDataList_14), value);
	}

	inline static int32_t get_offset_of_captureTexture_15() { return static_cast<int32_t>(offsetof(AdvSaveManager_t2382020483, ___captureTexture_15)); }
	inline Texture2D_t3542995729 * get_captureTexture_15() const { return ___captureTexture_15; }
	inline Texture2D_t3542995729 ** get_address_of_captureTexture_15() { return &___captureTexture_15; }
	inline void set_captureTexture_15(Texture2D_t3542995729 * value)
	{
		___captureTexture_15 = value;
		Il2CppCodeGenWriteBarrier((&___captureTexture_15), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ADVSAVEMANAGER_T2382020483_H
#ifndef ADVEFFECTCOLOR_T2285992559_H
#define ADVEFFECTCOLOR_T2285992559_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.AdvEffectColor
struct  AdvEffectColor_t2285992559  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.Color Utage.AdvEffectColor::animationColor
	Color_t2020392075  ___animationColor_2;
	// UnityEngine.Color Utage.AdvEffectColor::tweenColor
	Color_t2020392075  ___tweenColor_3;
	// UnityEngine.Color Utage.AdvEffectColor::scriptColor
	Color_t2020392075  ___scriptColor_4;
	// UnityEngine.Color Utage.AdvEffectColor::customColor
	Color_t2020392075  ___customColor_5;
	// System.Single Utage.AdvEffectColor::fadeAlpha
	float ___fadeAlpha_6;
	// Utage.EventEffectColor Utage.AdvEffectColor::OnValueChanged
	EventEffectColor_t1679408286 * ___OnValueChanged_7;
	// UnityEngine.Color Utage.AdvEffectColor::lastColor
	Color_t2020392075  ___lastColor_8;

public:
	inline static int32_t get_offset_of_animationColor_2() { return static_cast<int32_t>(offsetof(AdvEffectColor_t2285992559, ___animationColor_2)); }
	inline Color_t2020392075  get_animationColor_2() const { return ___animationColor_2; }
	inline Color_t2020392075 * get_address_of_animationColor_2() { return &___animationColor_2; }
	inline void set_animationColor_2(Color_t2020392075  value)
	{
		___animationColor_2 = value;
	}

	inline static int32_t get_offset_of_tweenColor_3() { return static_cast<int32_t>(offsetof(AdvEffectColor_t2285992559, ___tweenColor_3)); }
	inline Color_t2020392075  get_tweenColor_3() const { return ___tweenColor_3; }
	inline Color_t2020392075 * get_address_of_tweenColor_3() { return &___tweenColor_3; }
	inline void set_tweenColor_3(Color_t2020392075  value)
	{
		___tweenColor_3 = value;
	}

	inline static int32_t get_offset_of_scriptColor_4() { return static_cast<int32_t>(offsetof(AdvEffectColor_t2285992559, ___scriptColor_4)); }
	inline Color_t2020392075  get_scriptColor_4() const { return ___scriptColor_4; }
	inline Color_t2020392075 * get_address_of_scriptColor_4() { return &___scriptColor_4; }
	inline void set_scriptColor_4(Color_t2020392075  value)
	{
		___scriptColor_4 = value;
	}

	inline static int32_t get_offset_of_customColor_5() { return static_cast<int32_t>(offsetof(AdvEffectColor_t2285992559, ___customColor_5)); }
	inline Color_t2020392075  get_customColor_5() const { return ___customColor_5; }
	inline Color_t2020392075 * get_address_of_customColor_5() { return &___customColor_5; }
	inline void set_customColor_5(Color_t2020392075  value)
	{
		___customColor_5 = value;
	}

	inline static int32_t get_offset_of_fadeAlpha_6() { return static_cast<int32_t>(offsetof(AdvEffectColor_t2285992559, ___fadeAlpha_6)); }
	inline float get_fadeAlpha_6() const { return ___fadeAlpha_6; }
	inline float* get_address_of_fadeAlpha_6() { return &___fadeAlpha_6; }
	inline void set_fadeAlpha_6(float value)
	{
		___fadeAlpha_6 = value;
	}

	inline static int32_t get_offset_of_OnValueChanged_7() { return static_cast<int32_t>(offsetof(AdvEffectColor_t2285992559, ___OnValueChanged_7)); }
	inline EventEffectColor_t1679408286 * get_OnValueChanged_7() const { return ___OnValueChanged_7; }
	inline EventEffectColor_t1679408286 ** get_address_of_OnValueChanged_7() { return &___OnValueChanged_7; }
	inline void set_OnValueChanged_7(EventEffectColor_t1679408286 * value)
	{
		___OnValueChanged_7 = value;
		Il2CppCodeGenWriteBarrier((&___OnValueChanged_7), value);
	}

	inline static int32_t get_offset_of_lastColor_8() { return static_cast<int32_t>(offsetof(AdvEffectColor_t2285992559, ___lastColor_8)); }
	inline Color_t2020392075  get_lastColor_8() const { return ___lastColor_8; }
	inline Color_t2020392075 * get_address_of_lastColor_8() { return &___lastColor_8; }
	inline void set_lastColor_8(Color_t2020392075  value)
	{
		___lastColor_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ADVEFFECTCOLOR_T2285992559_H
#ifndef ADVGRAPHICLAYER_T3630223822_H
#define ADVGRAPHICLAYER_T3630223822_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.AdvGraphicLayer
struct  AdvGraphicLayer_t3630223822  : public MonoBehaviour_t1158329972
{
public:
	// Utage.AdvGraphicManager Utage.AdvGraphicLayer::<Manager>k__BackingField
	AdvGraphicManager_t3661251000 * ___U3CManagerU3Ek__BackingField_2;
	// Utage.AdvLayerSettingData Utage.AdvGraphicLayer::<SettingData>k__BackingField
	AdvLayerSettingData_t1908772360 * ___U3CSettingDataU3Ek__BackingField_3;
	// Utage.AdvGraphicObject Utage.AdvGraphicLayer::<DefaultObject>k__BackingField
	AdvGraphicObject_t3651915246 * ___U3CDefaultObjectU3Ek__BackingField_4;
	// System.Collections.Generic.Dictionary`2<System.String,Utage.AdvGraphicObject> Utage.AdvGraphicLayer::currentGraphics
	Dictionary_2_t1271727212 * ___currentGraphics_5;
	// UnityEngine.Camera Utage.AdvGraphicLayer::<Camera>k__BackingField
	Camera_t189460977 * ___U3CCameraU3Ek__BackingField_6;
	// Utage.LetterBoxCamera Utage.AdvGraphicLayer::<LetterBoxCamera>k__BackingField
	LetterBoxCamera_t3507617684 * ___U3CLetterBoxCameraU3Ek__BackingField_7;
	// UnityEngine.Canvas Utage.AdvGraphicLayer::<Canvas>k__BackingField
	Canvas_t209405766 * ___U3CCanvasU3Ek__BackingField_8;
	// UnityEngine.Transform Utage.AdvGraphicLayer::rootObjects
	Transform_t3275118058 * ___rootObjects_9;

public:
	inline static int32_t get_offset_of_U3CManagerU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(AdvGraphicLayer_t3630223822, ___U3CManagerU3Ek__BackingField_2)); }
	inline AdvGraphicManager_t3661251000 * get_U3CManagerU3Ek__BackingField_2() const { return ___U3CManagerU3Ek__BackingField_2; }
	inline AdvGraphicManager_t3661251000 ** get_address_of_U3CManagerU3Ek__BackingField_2() { return &___U3CManagerU3Ek__BackingField_2; }
	inline void set_U3CManagerU3Ek__BackingField_2(AdvGraphicManager_t3661251000 * value)
	{
		___U3CManagerU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CManagerU3Ek__BackingField_2), value);
	}

	inline static int32_t get_offset_of_U3CSettingDataU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(AdvGraphicLayer_t3630223822, ___U3CSettingDataU3Ek__BackingField_3)); }
	inline AdvLayerSettingData_t1908772360 * get_U3CSettingDataU3Ek__BackingField_3() const { return ___U3CSettingDataU3Ek__BackingField_3; }
	inline AdvLayerSettingData_t1908772360 ** get_address_of_U3CSettingDataU3Ek__BackingField_3() { return &___U3CSettingDataU3Ek__BackingField_3; }
	inline void set_U3CSettingDataU3Ek__BackingField_3(AdvLayerSettingData_t1908772360 * value)
	{
		___U3CSettingDataU3Ek__BackingField_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CSettingDataU3Ek__BackingField_3), value);
	}

	inline static int32_t get_offset_of_U3CDefaultObjectU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(AdvGraphicLayer_t3630223822, ___U3CDefaultObjectU3Ek__BackingField_4)); }
	inline AdvGraphicObject_t3651915246 * get_U3CDefaultObjectU3Ek__BackingField_4() const { return ___U3CDefaultObjectU3Ek__BackingField_4; }
	inline AdvGraphicObject_t3651915246 ** get_address_of_U3CDefaultObjectU3Ek__BackingField_4() { return &___U3CDefaultObjectU3Ek__BackingField_4; }
	inline void set_U3CDefaultObjectU3Ek__BackingField_4(AdvGraphicObject_t3651915246 * value)
	{
		___U3CDefaultObjectU3Ek__BackingField_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CDefaultObjectU3Ek__BackingField_4), value);
	}

	inline static int32_t get_offset_of_currentGraphics_5() { return static_cast<int32_t>(offsetof(AdvGraphicLayer_t3630223822, ___currentGraphics_5)); }
	inline Dictionary_2_t1271727212 * get_currentGraphics_5() const { return ___currentGraphics_5; }
	inline Dictionary_2_t1271727212 ** get_address_of_currentGraphics_5() { return &___currentGraphics_5; }
	inline void set_currentGraphics_5(Dictionary_2_t1271727212 * value)
	{
		___currentGraphics_5 = value;
		Il2CppCodeGenWriteBarrier((&___currentGraphics_5), value);
	}

	inline static int32_t get_offset_of_U3CCameraU3Ek__BackingField_6() { return static_cast<int32_t>(offsetof(AdvGraphicLayer_t3630223822, ___U3CCameraU3Ek__BackingField_6)); }
	inline Camera_t189460977 * get_U3CCameraU3Ek__BackingField_6() const { return ___U3CCameraU3Ek__BackingField_6; }
	inline Camera_t189460977 ** get_address_of_U3CCameraU3Ek__BackingField_6() { return &___U3CCameraU3Ek__BackingField_6; }
	inline void set_U3CCameraU3Ek__BackingField_6(Camera_t189460977 * value)
	{
		___U3CCameraU3Ek__BackingField_6 = value;
		Il2CppCodeGenWriteBarrier((&___U3CCameraU3Ek__BackingField_6), value);
	}

	inline static int32_t get_offset_of_U3CLetterBoxCameraU3Ek__BackingField_7() { return static_cast<int32_t>(offsetof(AdvGraphicLayer_t3630223822, ___U3CLetterBoxCameraU3Ek__BackingField_7)); }
	inline LetterBoxCamera_t3507617684 * get_U3CLetterBoxCameraU3Ek__BackingField_7() const { return ___U3CLetterBoxCameraU3Ek__BackingField_7; }
	inline LetterBoxCamera_t3507617684 ** get_address_of_U3CLetterBoxCameraU3Ek__BackingField_7() { return &___U3CLetterBoxCameraU3Ek__BackingField_7; }
	inline void set_U3CLetterBoxCameraU3Ek__BackingField_7(LetterBoxCamera_t3507617684 * value)
	{
		___U3CLetterBoxCameraU3Ek__BackingField_7 = value;
		Il2CppCodeGenWriteBarrier((&___U3CLetterBoxCameraU3Ek__BackingField_7), value);
	}

	inline static int32_t get_offset_of_U3CCanvasU3Ek__BackingField_8() { return static_cast<int32_t>(offsetof(AdvGraphicLayer_t3630223822, ___U3CCanvasU3Ek__BackingField_8)); }
	inline Canvas_t209405766 * get_U3CCanvasU3Ek__BackingField_8() const { return ___U3CCanvasU3Ek__BackingField_8; }
	inline Canvas_t209405766 ** get_address_of_U3CCanvasU3Ek__BackingField_8() { return &___U3CCanvasU3Ek__BackingField_8; }
	inline void set_U3CCanvasU3Ek__BackingField_8(Canvas_t209405766 * value)
	{
		___U3CCanvasU3Ek__BackingField_8 = value;
		Il2CppCodeGenWriteBarrier((&___U3CCanvasU3Ek__BackingField_8), value);
	}

	inline static int32_t get_offset_of_rootObjects_9() { return static_cast<int32_t>(offsetof(AdvGraphicLayer_t3630223822, ___rootObjects_9)); }
	inline Transform_t3275118058 * get_rootObjects_9() const { return ___rootObjects_9; }
	inline Transform_t3275118058 ** get_address_of_rootObjects_9() { return &___rootObjects_9; }
	inline void set_rootObjects_9(Transform_t3275118058 * value)
	{
		___rootObjects_9 = value;
		Il2CppCodeGenWriteBarrier((&___rootObjects_9), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ADVGRAPHICLAYER_T3630223822_H
#ifndef ADVSELECTIONMANAGER_T3429904078_H
#define ADVSELECTIONMANAGER_T3429904078_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.AdvSelectionManager
struct  AdvSelectionManager_t3429904078  : public MonoBehaviour_t1158329972
{
public:
	// Utage.AdvEngine Utage.AdvSelectionManager::engine
	AdvEngine_t1176753927 * ___engine_2;
	// System.Collections.Generic.List`1<Utage.AdvSelection> Utage.AdvSelectionManager::selections
	List_1_t1670473085 * ___selections_3;
	// System.Collections.Generic.List`1<Utage.AdvSelection> Utage.AdvSelectionManager::spriteSelections
	List_1_t1670473085 * ___spriteSelections_4;
	// System.Boolean Utage.AdvSelectionManager::<IsShowing>k__BackingField
	bool ___U3CIsShowingU3Ek__BackingField_5;
	// Utage.SelectionEvent Utage.AdvSelectionManager::onClear
	SelectionEvent_t3989025344 * ___onClear_6;
	// Utage.SelectionEvent Utage.AdvSelectionManager::onBeginShow
	SelectionEvent_t3989025344 * ___onBeginShow_7;
	// Utage.SelectionEvent Utage.AdvSelectionManager::onBeginWaitInput
	SelectionEvent_t3989025344 * ___onBeginWaitInput_8;
	// Utage.SelectionEvent Utage.AdvSelectionManager::onUpdateWaitInput
	SelectionEvent_t3989025344 * ___onUpdateWaitInput_9;
	// Utage.SelectionEvent Utage.AdvSelectionManager::onSelected
	SelectionEvent_t3989025344 * ___onSelected_10;
	// Utage.AdvSelection Utage.AdvSelectionManager::selected
	AdvSelection_t2301351953 * ___selected_11;
	// System.Boolean Utage.AdvSelectionManager::<IsWaitInput>k__BackingField
	bool ___U3CIsWaitInputU3Ek__BackingField_12;

public:
	inline static int32_t get_offset_of_engine_2() { return static_cast<int32_t>(offsetof(AdvSelectionManager_t3429904078, ___engine_2)); }
	inline AdvEngine_t1176753927 * get_engine_2() const { return ___engine_2; }
	inline AdvEngine_t1176753927 ** get_address_of_engine_2() { return &___engine_2; }
	inline void set_engine_2(AdvEngine_t1176753927 * value)
	{
		___engine_2 = value;
		Il2CppCodeGenWriteBarrier((&___engine_2), value);
	}

	inline static int32_t get_offset_of_selections_3() { return static_cast<int32_t>(offsetof(AdvSelectionManager_t3429904078, ___selections_3)); }
	inline List_1_t1670473085 * get_selections_3() const { return ___selections_3; }
	inline List_1_t1670473085 ** get_address_of_selections_3() { return &___selections_3; }
	inline void set_selections_3(List_1_t1670473085 * value)
	{
		___selections_3 = value;
		Il2CppCodeGenWriteBarrier((&___selections_3), value);
	}

	inline static int32_t get_offset_of_spriteSelections_4() { return static_cast<int32_t>(offsetof(AdvSelectionManager_t3429904078, ___spriteSelections_4)); }
	inline List_1_t1670473085 * get_spriteSelections_4() const { return ___spriteSelections_4; }
	inline List_1_t1670473085 ** get_address_of_spriteSelections_4() { return &___spriteSelections_4; }
	inline void set_spriteSelections_4(List_1_t1670473085 * value)
	{
		___spriteSelections_4 = value;
		Il2CppCodeGenWriteBarrier((&___spriteSelections_4), value);
	}

	inline static int32_t get_offset_of_U3CIsShowingU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(AdvSelectionManager_t3429904078, ___U3CIsShowingU3Ek__BackingField_5)); }
	inline bool get_U3CIsShowingU3Ek__BackingField_5() const { return ___U3CIsShowingU3Ek__BackingField_5; }
	inline bool* get_address_of_U3CIsShowingU3Ek__BackingField_5() { return &___U3CIsShowingU3Ek__BackingField_5; }
	inline void set_U3CIsShowingU3Ek__BackingField_5(bool value)
	{
		___U3CIsShowingU3Ek__BackingField_5 = value;
	}

	inline static int32_t get_offset_of_onClear_6() { return static_cast<int32_t>(offsetof(AdvSelectionManager_t3429904078, ___onClear_6)); }
	inline SelectionEvent_t3989025344 * get_onClear_6() const { return ___onClear_6; }
	inline SelectionEvent_t3989025344 ** get_address_of_onClear_6() { return &___onClear_6; }
	inline void set_onClear_6(SelectionEvent_t3989025344 * value)
	{
		___onClear_6 = value;
		Il2CppCodeGenWriteBarrier((&___onClear_6), value);
	}

	inline static int32_t get_offset_of_onBeginShow_7() { return static_cast<int32_t>(offsetof(AdvSelectionManager_t3429904078, ___onBeginShow_7)); }
	inline SelectionEvent_t3989025344 * get_onBeginShow_7() const { return ___onBeginShow_7; }
	inline SelectionEvent_t3989025344 ** get_address_of_onBeginShow_7() { return &___onBeginShow_7; }
	inline void set_onBeginShow_7(SelectionEvent_t3989025344 * value)
	{
		___onBeginShow_7 = value;
		Il2CppCodeGenWriteBarrier((&___onBeginShow_7), value);
	}

	inline static int32_t get_offset_of_onBeginWaitInput_8() { return static_cast<int32_t>(offsetof(AdvSelectionManager_t3429904078, ___onBeginWaitInput_8)); }
	inline SelectionEvent_t3989025344 * get_onBeginWaitInput_8() const { return ___onBeginWaitInput_8; }
	inline SelectionEvent_t3989025344 ** get_address_of_onBeginWaitInput_8() { return &___onBeginWaitInput_8; }
	inline void set_onBeginWaitInput_8(SelectionEvent_t3989025344 * value)
	{
		___onBeginWaitInput_8 = value;
		Il2CppCodeGenWriteBarrier((&___onBeginWaitInput_8), value);
	}

	inline static int32_t get_offset_of_onUpdateWaitInput_9() { return static_cast<int32_t>(offsetof(AdvSelectionManager_t3429904078, ___onUpdateWaitInput_9)); }
	inline SelectionEvent_t3989025344 * get_onUpdateWaitInput_9() const { return ___onUpdateWaitInput_9; }
	inline SelectionEvent_t3989025344 ** get_address_of_onUpdateWaitInput_9() { return &___onUpdateWaitInput_9; }
	inline void set_onUpdateWaitInput_9(SelectionEvent_t3989025344 * value)
	{
		___onUpdateWaitInput_9 = value;
		Il2CppCodeGenWriteBarrier((&___onUpdateWaitInput_9), value);
	}

	inline static int32_t get_offset_of_onSelected_10() { return static_cast<int32_t>(offsetof(AdvSelectionManager_t3429904078, ___onSelected_10)); }
	inline SelectionEvent_t3989025344 * get_onSelected_10() const { return ___onSelected_10; }
	inline SelectionEvent_t3989025344 ** get_address_of_onSelected_10() { return &___onSelected_10; }
	inline void set_onSelected_10(SelectionEvent_t3989025344 * value)
	{
		___onSelected_10 = value;
		Il2CppCodeGenWriteBarrier((&___onSelected_10), value);
	}

	inline static int32_t get_offset_of_selected_11() { return static_cast<int32_t>(offsetof(AdvSelectionManager_t3429904078, ___selected_11)); }
	inline AdvSelection_t2301351953 * get_selected_11() const { return ___selected_11; }
	inline AdvSelection_t2301351953 ** get_address_of_selected_11() { return &___selected_11; }
	inline void set_selected_11(AdvSelection_t2301351953 * value)
	{
		___selected_11 = value;
		Il2CppCodeGenWriteBarrier((&___selected_11), value);
	}

	inline static int32_t get_offset_of_U3CIsWaitInputU3Ek__BackingField_12() { return static_cast<int32_t>(offsetof(AdvSelectionManager_t3429904078, ___U3CIsWaitInputU3Ek__BackingField_12)); }
	inline bool get_U3CIsWaitInputU3Ek__BackingField_12() const { return ___U3CIsWaitInputU3Ek__BackingField_12; }
	inline bool* get_address_of_U3CIsWaitInputU3Ek__BackingField_12() { return &___U3CIsWaitInputU3Ek__BackingField_12; }
	inline void set_U3CIsWaitInputU3Ek__BackingField_12(bool value)
	{
		___U3CIsWaitInputU3Ek__BackingField_12 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ADVSELECTIONMANAGER_T3429904078_H
#ifndef ADVMESSAGEWINDOWMANAGER_T1170996005_H
#define ADVMESSAGEWINDOWMANAGER_T1170996005_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.AdvMessageWindowManager
struct  AdvMessageWindowManager_t1170996005  : public MonoBehaviour_t1158329972
{
public:
	// Utage.MessageWindowEvent Utage.AdvMessageWindowManager::onReset
	MessageWindowEvent_t3103993945 * ___onReset_2;
	// Utage.MessageWindowEvent Utage.AdvMessageWindowManager::onChangeActiveWindows
	MessageWindowEvent_t3103993945 * ___onChangeActiveWindows_3;
	// Utage.MessageWindowEvent Utage.AdvMessageWindowManager::onChangeCurrentWindow
	MessageWindowEvent_t3103993945 * ___onChangeCurrentWindow_4;
	// Utage.MessageWindowEvent Utage.AdvMessageWindowManager::onTextChange
	MessageWindowEvent_t3103993945 * ___onTextChange_5;
	// Utage.AdvEngine Utage.AdvMessageWindowManager::engine
	AdvEngine_t1176753927 * ___engine_6;
	// System.Boolean Utage.AdvMessageWindowManager::isInit
	bool ___isInit_7;
	// System.Collections.Generic.Dictionary`2<System.String,Utage.AdvMessageWindow> Utage.AdvMessageWindowManager::allWindows
	Dictionary_2_t1035234340 * ___allWindows_8;
	// System.Collections.Generic.List`1<System.String> Utage.AdvMessageWindowManager::defaultActiveWindowNameList
	List_1_t1398341365 * ___defaultActiveWindowNameList_9;
	// System.Collections.Generic.Dictionary`2<System.String,Utage.AdvMessageWindow> Utage.AdvMessageWindowManager::activeWindows
	Dictionary_2_t1035234340 * ___activeWindows_10;
	// Utage.AdvMessageWindow Utage.AdvMessageWindowManager::<CurrentWindow>k__BackingField
	AdvMessageWindow_t3415422374 * ___U3CCurrentWindowU3Ek__BackingField_11;
	// Utage.AdvMessageWindow Utage.AdvMessageWindowManager::<LastWindow>k__BackingField
	AdvMessageWindow_t3415422374 * ___U3CLastWindowU3Ek__BackingField_12;

public:
	inline static int32_t get_offset_of_onReset_2() { return static_cast<int32_t>(offsetof(AdvMessageWindowManager_t1170996005, ___onReset_2)); }
	inline MessageWindowEvent_t3103993945 * get_onReset_2() const { return ___onReset_2; }
	inline MessageWindowEvent_t3103993945 ** get_address_of_onReset_2() { return &___onReset_2; }
	inline void set_onReset_2(MessageWindowEvent_t3103993945 * value)
	{
		___onReset_2 = value;
		Il2CppCodeGenWriteBarrier((&___onReset_2), value);
	}

	inline static int32_t get_offset_of_onChangeActiveWindows_3() { return static_cast<int32_t>(offsetof(AdvMessageWindowManager_t1170996005, ___onChangeActiveWindows_3)); }
	inline MessageWindowEvent_t3103993945 * get_onChangeActiveWindows_3() const { return ___onChangeActiveWindows_3; }
	inline MessageWindowEvent_t3103993945 ** get_address_of_onChangeActiveWindows_3() { return &___onChangeActiveWindows_3; }
	inline void set_onChangeActiveWindows_3(MessageWindowEvent_t3103993945 * value)
	{
		___onChangeActiveWindows_3 = value;
		Il2CppCodeGenWriteBarrier((&___onChangeActiveWindows_3), value);
	}

	inline static int32_t get_offset_of_onChangeCurrentWindow_4() { return static_cast<int32_t>(offsetof(AdvMessageWindowManager_t1170996005, ___onChangeCurrentWindow_4)); }
	inline MessageWindowEvent_t3103993945 * get_onChangeCurrentWindow_4() const { return ___onChangeCurrentWindow_4; }
	inline MessageWindowEvent_t3103993945 ** get_address_of_onChangeCurrentWindow_4() { return &___onChangeCurrentWindow_4; }
	inline void set_onChangeCurrentWindow_4(MessageWindowEvent_t3103993945 * value)
	{
		___onChangeCurrentWindow_4 = value;
		Il2CppCodeGenWriteBarrier((&___onChangeCurrentWindow_4), value);
	}

	inline static int32_t get_offset_of_onTextChange_5() { return static_cast<int32_t>(offsetof(AdvMessageWindowManager_t1170996005, ___onTextChange_5)); }
	inline MessageWindowEvent_t3103993945 * get_onTextChange_5() const { return ___onTextChange_5; }
	inline MessageWindowEvent_t3103993945 ** get_address_of_onTextChange_5() { return &___onTextChange_5; }
	inline void set_onTextChange_5(MessageWindowEvent_t3103993945 * value)
	{
		___onTextChange_5 = value;
		Il2CppCodeGenWriteBarrier((&___onTextChange_5), value);
	}

	inline static int32_t get_offset_of_engine_6() { return static_cast<int32_t>(offsetof(AdvMessageWindowManager_t1170996005, ___engine_6)); }
	inline AdvEngine_t1176753927 * get_engine_6() const { return ___engine_6; }
	inline AdvEngine_t1176753927 ** get_address_of_engine_6() { return &___engine_6; }
	inline void set_engine_6(AdvEngine_t1176753927 * value)
	{
		___engine_6 = value;
		Il2CppCodeGenWriteBarrier((&___engine_6), value);
	}

	inline static int32_t get_offset_of_isInit_7() { return static_cast<int32_t>(offsetof(AdvMessageWindowManager_t1170996005, ___isInit_7)); }
	inline bool get_isInit_7() const { return ___isInit_7; }
	inline bool* get_address_of_isInit_7() { return &___isInit_7; }
	inline void set_isInit_7(bool value)
	{
		___isInit_7 = value;
	}

	inline static int32_t get_offset_of_allWindows_8() { return static_cast<int32_t>(offsetof(AdvMessageWindowManager_t1170996005, ___allWindows_8)); }
	inline Dictionary_2_t1035234340 * get_allWindows_8() const { return ___allWindows_8; }
	inline Dictionary_2_t1035234340 ** get_address_of_allWindows_8() { return &___allWindows_8; }
	inline void set_allWindows_8(Dictionary_2_t1035234340 * value)
	{
		___allWindows_8 = value;
		Il2CppCodeGenWriteBarrier((&___allWindows_8), value);
	}

	inline static int32_t get_offset_of_defaultActiveWindowNameList_9() { return static_cast<int32_t>(offsetof(AdvMessageWindowManager_t1170996005, ___defaultActiveWindowNameList_9)); }
	inline List_1_t1398341365 * get_defaultActiveWindowNameList_9() const { return ___defaultActiveWindowNameList_9; }
	inline List_1_t1398341365 ** get_address_of_defaultActiveWindowNameList_9() { return &___defaultActiveWindowNameList_9; }
	inline void set_defaultActiveWindowNameList_9(List_1_t1398341365 * value)
	{
		___defaultActiveWindowNameList_9 = value;
		Il2CppCodeGenWriteBarrier((&___defaultActiveWindowNameList_9), value);
	}

	inline static int32_t get_offset_of_activeWindows_10() { return static_cast<int32_t>(offsetof(AdvMessageWindowManager_t1170996005, ___activeWindows_10)); }
	inline Dictionary_2_t1035234340 * get_activeWindows_10() const { return ___activeWindows_10; }
	inline Dictionary_2_t1035234340 ** get_address_of_activeWindows_10() { return &___activeWindows_10; }
	inline void set_activeWindows_10(Dictionary_2_t1035234340 * value)
	{
		___activeWindows_10 = value;
		Il2CppCodeGenWriteBarrier((&___activeWindows_10), value);
	}

	inline static int32_t get_offset_of_U3CCurrentWindowU3Ek__BackingField_11() { return static_cast<int32_t>(offsetof(AdvMessageWindowManager_t1170996005, ___U3CCurrentWindowU3Ek__BackingField_11)); }
	inline AdvMessageWindow_t3415422374 * get_U3CCurrentWindowU3Ek__BackingField_11() const { return ___U3CCurrentWindowU3Ek__BackingField_11; }
	inline AdvMessageWindow_t3415422374 ** get_address_of_U3CCurrentWindowU3Ek__BackingField_11() { return &___U3CCurrentWindowU3Ek__BackingField_11; }
	inline void set_U3CCurrentWindowU3Ek__BackingField_11(AdvMessageWindow_t3415422374 * value)
	{
		___U3CCurrentWindowU3Ek__BackingField_11 = value;
		Il2CppCodeGenWriteBarrier((&___U3CCurrentWindowU3Ek__BackingField_11), value);
	}

	inline static int32_t get_offset_of_U3CLastWindowU3Ek__BackingField_12() { return static_cast<int32_t>(offsetof(AdvMessageWindowManager_t1170996005, ___U3CLastWindowU3Ek__BackingField_12)); }
	inline AdvMessageWindow_t3415422374 * get_U3CLastWindowU3Ek__BackingField_12() const { return ___U3CLastWindowU3Ek__BackingField_12; }
	inline AdvMessageWindow_t3415422374 ** get_address_of_U3CLastWindowU3Ek__BackingField_12() { return &___U3CLastWindowU3Ek__BackingField_12; }
	inline void set_U3CLastWindowU3Ek__BackingField_12(AdvMessageWindow_t3415422374 * value)
	{
		___U3CLastWindowU3Ek__BackingField_12 = value;
		Il2CppCodeGenWriteBarrier((&___U3CLastWindowU3Ek__BackingField_12), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ADVMESSAGEWINDOWMANAGER_T1170996005_H
#ifndef ADVRENDERTEXTURESPACE_T3482187212_H
#define ADVRENDERTEXTURESPACE_T3482187212_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.AdvRenderTextureSpace
struct  AdvRenderTextureSpace_t3482187212  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.RenderTexture Utage.AdvRenderTextureSpace::<RenderTexture>k__BackingField
	RenderTexture_t2666733923 * ___U3CRenderTextureU3Ek__BackingField_2;
	// UnityEngine.Camera Utage.AdvRenderTextureSpace::<RenderCamera>k__BackingField
	Camera_t189460977 * ___U3CRenderCameraU3Ek__BackingField_3;
	// UnityEngine.Canvas Utage.AdvRenderTextureSpace::<Canvas>k__BackingField
	Canvas_t209405766 * ___U3CCanvasU3Ek__BackingField_4;
	// UnityEngine.UI.CanvasScaler Utage.AdvRenderTextureSpace::<CanvasScaler>k__BackingField
	CanvasScaler_t2574720772 * ___U3CCanvasScalerU3Ek__BackingField_5;
	// UnityEngine.GameObject Utage.AdvRenderTextureSpace::<RenderRoot>k__BackingField
	GameObject_t1756533147 * ___U3CRenderRootU3Ek__BackingField_6;
	// Utage.AdvRenderTextureSetting Utage.AdvRenderTextureSpace::<Setting>k__BackingField
	AdvRenderTextureSetting_t3039724782 * ___U3CSettingU3Ek__BackingField_7;

public:
	inline static int32_t get_offset_of_U3CRenderTextureU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(AdvRenderTextureSpace_t3482187212, ___U3CRenderTextureU3Ek__BackingField_2)); }
	inline RenderTexture_t2666733923 * get_U3CRenderTextureU3Ek__BackingField_2() const { return ___U3CRenderTextureU3Ek__BackingField_2; }
	inline RenderTexture_t2666733923 ** get_address_of_U3CRenderTextureU3Ek__BackingField_2() { return &___U3CRenderTextureU3Ek__BackingField_2; }
	inline void set_U3CRenderTextureU3Ek__BackingField_2(RenderTexture_t2666733923 * value)
	{
		___U3CRenderTextureU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CRenderTextureU3Ek__BackingField_2), value);
	}

	inline static int32_t get_offset_of_U3CRenderCameraU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(AdvRenderTextureSpace_t3482187212, ___U3CRenderCameraU3Ek__BackingField_3)); }
	inline Camera_t189460977 * get_U3CRenderCameraU3Ek__BackingField_3() const { return ___U3CRenderCameraU3Ek__BackingField_3; }
	inline Camera_t189460977 ** get_address_of_U3CRenderCameraU3Ek__BackingField_3() { return &___U3CRenderCameraU3Ek__BackingField_3; }
	inline void set_U3CRenderCameraU3Ek__BackingField_3(Camera_t189460977 * value)
	{
		___U3CRenderCameraU3Ek__BackingField_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CRenderCameraU3Ek__BackingField_3), value);
	}

	inline static int32_t get_offset_of_U3CCanvasU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(AdvRenderTextureSpace_t3482187212, ___U3CCanvasU3Ek__BackingField_4)); }
	inline Canvas_t209405766 * get_U3CCanvasU3Ek__BackingField_4() const { return ___U3CCanvasU3Ek__BackingField_4; }
	inline Canvas_t209405766 ** get_address_of_U3CCanvasU3Ek__BackingField_4() { return &___U3CCanvasU3Ek__BackingField_4; }
	inline void set_U3CCanvasU3Ek__BackingField_4(Canvas_t209405766 * value)
	{
		___U3CCanvasU3Ek__BackingField_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CCanvasU3Ek__BackingField_4), value);
	}

	inline static int32_t get_offset_of_U3CCanvasScalerU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(AdvRenderTextureSpace_t3482187212, ___U3CCanvasScalerU3Ek__BackingField_5)); }
	inline CanvasScaler_t2574720772 * get_U3CCanvasScalerU3Ek__BackingField_5() const { return ___U3CCanvasScalerU3Ek__BackingField_5; }
	inline CanvasScaler_t2574720772 ** get_address_of_U3CCanvasScalerU3Ek__BackingField_5() { return &___U3CCanvasScalerU3Ek__BackingField_5; }
	inline void set_U3CCanvasScalerU3Ek__BackingField_5(CanvasScaler_t2574720772 * value)
	{
		___U3CCanvasScalerU3Ek__BackingField_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3CCanvasScalerU3Ek__BackingField_5), value);
	}

	inline static int32_t get_offset_of_U3CRenderRootU3Ek__BackingField_6() { return static_cast<int32_t>(offsetof(AdvRenderTextureSpace_t3482187212, ___U3CRenderRootU3Ek__BackingField_6)); }
	inline GameObject_t1756533147 * get_U3CRenderRootU3Ek__BackingField_6() const { return ___U3CRenderRootU3Ek__BackingField_6; }
	inline GameObject_t1756533147 ** get_address_of_U3CRenderRootU3Ek__BackingField_6() { return &___U3CRenderRootU3Ek__BackingField_6; }
	inline void set_U3CRenderRootU3Ek__BackingField_6(GameObject_t1756533147 * value)
	{
		___U3CRenderRootU3Ek__BackingField_6 = value;
		Il2CppCodeGenWriteBarrier((&___U3CRenderRootU3Ek__BackingField_6), value);
	}

	inline static int32_t get_offset_of_U3CSettingU3Ek__BackingField_7() { return static_cast<int32_t>(offsetof(AdvRenderTextureSpace_t3482187212, ___U3CSettingU3Ek__BackingField_7)); }
	inline AdvRenderTextureSetting_t3039724782 * get_U3CSettingU3Ek__BackingField_7() const { return ___U3CSettingU3Ek__BackingField_7; }
	inline AdvRenderTextureSetting_t3039724782 ** get_address_of_U3CSettingU3Ek__BackingField_7() { return &___U3CSettingU3Ek__BackingField_7; }
	inline void set_U3CSettingU3Ek__BackingField_7(AdvRenderTextureSetting_t3039724782 * value)
	{
		___U3CSettingU3Ek__BackingField_7 = value;
		Il2CppCodeGenWriteBarrier((&___U3CSettingU3Ek__BackingField_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ADVRENDERTEXTURESPACE_T3482187212_H
#ifndef ADVVIDEOMANAGER_T946313091_H
#define ADVVIDEOMANAGER_T946313091_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.AdvVideoManager
struct  AdvVideoManager_t946313091  : public MonoBehaviour_t1158329972
{
public:
	// Utage.AdvEngine Utage.AdvVideoManager::engine
	AdvEngine_t1176753927 * ___engine_2;
	// System.Collections.Generic.Dictionary`2<System.String,Utage.AdvVideoManager/VideoInfo> Utage.AdvVideoManager::videos
	Dictionary_2_t555272053 * ___videos_3;

public:
	inline static int32_t get_offset_of_engine_2() { return static_cast<int32_t>(offsetof(AdvVideoManager_t946313091, ___engine_2)); }
	inline AdvEngine_t1176753927 * get_engine_2() const { return ___engine_2; }
	inline AdvEngine_t1176753927 ** get_address_of_engine_2() { return &___engine_2; }
	inline void set_engine_2(AdvEngine_t1176753927 * value)
	{
		___engine_2 = value;
		Il2CppCodeGenWriteBarrier((&___engine_2), value);
	}

	inline static int32_t get_offset_of_videos_3() { return static_cast<int32_t>(offsetof(AdvVideoManager_t946313091, ___videos_3)); }
	inline Dictionary_2_t555272053 * get_videos_3() const { return ___videos_3; }
	inline Dictionary_2_t555272053 ** get_address_of_videos_3() { return &___videos_3; }
	inline void set_videos_3(Dictionary_2_t555272053 * value)
	{
		___videos_3 = value;
		Il2CppCodeGenWriteBarrier((&___videos_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ADVVIDEOMANAGER_T946313091_H
#ifndef ADVSYSTEMSAVEDATA_T276732409_H
#define ADVSYSTEMSAVEDATA_T276732409_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.AdvSystemSaveData
struct  AdvSystemSaveData_t276732409  : public MonoBehaviour_t1158329972
{
public:
	// System.Boolean Utage.AdvSystemSaveData::dontUseSystemSaveData
	bool ___dontUseSystemSaveData_2;
	// System.Boolean Utage.AdvSystemSaveData::isAutoSaveOnQuit
	bool ___isAutoSaveOnQuit_3;
	// Utage.FileIOManager Utage.AdvSystemSaveData::fileIOManager
	FileIOManager_t855502573 * ___fileIOManager_4;
	// System.String Utage.AdvSystemSaveData::directoryName
	String_t* ___directoryName_5;
	// System.String Utage.AdvSystemSaveData::fileName
	String_t* ___fileName_6;
	// System.String Utage.AdvSystemSaveData::<Path>k__BackingField
	String_t* ___U3CPathU3Ek__BackingField_7;
	// Utage.AdvReadHistorySaveData Utage.AdvSystemSaveData::readData
	AdvReadHistorySaveData_t361293356 * ___readData_8;
	// Utage.AdvSelectedHistorySaveData Utage.AdvSystemSaveData::selectionData
	AdvSelectedHistorySaveData_t4269445723 * ___selectionData_9;
	// Utage.AdvGallerySaveData Utage.AdvSystemSaveData::galleryData
	AdvGallerySaveData_t1012686890 * ___galleryData_10;
	// Utage.AdvEngine Utage.AdvSystemSaveData::engine
	AdvEngine_t1176753927 * ___engine_11;
	// System.Boolean Utage.AdvSystemSaveData::isInit
	bool ___isInit_12;

public:
	inline static int32_t get_offset_of_dontUseSystemSaveData_2() { return static_cast<int32_t>(offsetof(AdvSystemSaveData_t276732409, ___dontUseSystemSaveData_2)); }
	inline bool get_dontUseSystemSaveData_2() const { return ___dontUseSystemSaveData_2; }
	inline bool* get_address_of_dontUseSystemSaveData_2() { return &___dontUseSystemSaveData_2; }
	inline void set_dontUseSystemSaveData_2(bool value)
	{
		___dontUseSystemSaveData_2 = value;
	}

	inline static int32_t get_offset_of_isAutoSaveOnQuit_3() { return static_cast<int32_t>(offsetof(AdvSystemSaveData_t276732409, ___isAutoSaveOnQuit_3)); }
	inline bool get_isAutoSaveOnQuit_3() const { return ___isAutoSaveOnQuit_3; }
	inline bool* get_address_of_isAutoSaveOnQuit_3() { return &___isAutoSaveOnQuit_3; }
	inline void set_isAutoSaveOnQuit_3(bool value)
	{
		___isAutoSaveOnQuit_3 = value;
	}

	inline static int32_t get_offset_of_fileIOManager_4() { return static_cast<int32_t>(offsetof(AdvSystemSaveData_t276732409, ___fileIOManager_4)); }
	inline FileIOManager_t855502573 * get_fileIOManager_4() const { return ___fileIOManager_4; }
	inline FileIOManager_t855502573 ** get_address_of_fileIOManager_4() { return &___fileIOManager_4; }
	inline void set_fileIOManager_4(FileIOManager_t855502573 * value)
	{
		___fileIOManager_4 = value;
		Il2CppCodeGenWriteBarrier((&___fileIOManager_4), value);
	}

	inline static int32_t get_offset_of_directoryName_5() { return static_cast<int32_t>(offsetof(AdvSystemSaveData_t276732409, ___directoryName_5)); }
	inline String_t* get_directoryName_5() const { return ___directoryName_5; }
	inline String_t** get_address_of_directoryName_5() { return &___directoryName_5; }
	inline void set_directoryName_5(String_t* value)
	{
		___directoryName_5 = value;
		Il2CppCodeGenWriteBarrier((&___directoryName_5), value);
	}

	inline static int32_t get_offset_of_fileName_6() { return static_cast<int32_t>(offsetof(AdvSystemSaveData_t276732409, ___fileName_6)); }
	inline String_t* get_fileName_6() const { return ___fileName_6; }
	inline String_t** get_address_of_fileName_6() { return &___fileName_6; }
	inline void set_fileName_6(String_t* value)
	{
		___fileName_6 = value;
		Il2CppCodeGenWriteBarrier((&___fileName_6), value);
	}

	inline static int32_t get_offset_of_U3CPathU3Ek__BackingField_7() { return static_cast<int32_t>(offsetof(AdvSystemSaveData_t276732409, ___U3CPathU3Ek__BackingField_7)); }
	inline String_t* get_U3CPathU3Ek__BackingField_7() const { return ___U3CPathU3Ek__BackingField_7; }
	inline String_t** get_address_of_U3CPathU3Ek__BackingField_7() { return &___U3CPathU3Ek__BackingField_7; }
	inline void set_U3CPathU3Ek__BackingField_7(String_t* value)
	{
		___U3CPathU3Ek__BackingField_7 = value;
		Il2CppCodeGenWriteBarrier((&___U3CPathU3Ek__BackingField_7), value);
	}

	inline static int32_t get_offset_of_readData_8() { return static_cast<int32_t>(offsetof(AdvSystemSaveData_t276732409, ___readData_8)); }
	inline AdvReadHistorySaveData_t361293356 * get_readData_8() const { return ___readData_8; }
	inline AdvReadHistorySaveData_t361293356 ** get_address_of_readData_8() { return &___readData_8; }
	inline void set_readData_8(AdvReadHistorySaveData_t361293356 * value)
	{
		___readData_8 = value;
		Il2CppCodeGenWriteBarrier((&___readData_8), value);
	}

	inline static int32_t get_offset_of_selectionData_9() { return static_cast<int32_t>(offsetof(AdvSystemSaveData_t276732409, ___selectionData_9)); }
	inline AdvSelectedHistorySaveData_t4269445723 * get_selectionData_9() const { return ___selectionData_9; }
	inline AdvSelectedHistorySaveData_t4269445723 ** get_address_of_selectionData_9() { return &___selectionData_9; }
	inline void set_selectionData_9(AdvSelectedHistorySaveData_t4269445723 * value)
	{
		___selectionData_9 = value;
		Il2CppCodeGenWriteBarrier((&___selectionData_9), value);
	}

	inline static int32_t get_offset_of_galleryData_10() { return static_cast<int32_t>(offsetof(AdvSystemSaveData_t276732409, ___galleryData_10)); }
	inline AdvGallerySaveData_t1012686890 * get_galleryData_10() const { return ___galleryData_10; }
	inline AdvGallerySaveData_t1012686890 ** get_address_of_galleryData_10() { return &___galleryData_10; }
	inline void set_galleryData_10(AdvGallerySaveData_t1012686890 * value)
	{
		___galleryData_10 = value;
		Il2CppCodeGenWriteBarrier((&___galleryData_10), value);
	}

	inline static int32_t get_offset_of_engine_11() { return static_cast<int32_t>(offsetof(AdvSystemSaveData_t276732409, ___engine_11)); }
	inline AdvEngine_t1176753927 * get_engine_11() const { return ___engine_11; }
	inline AdvEngine_t1176753927 ** get_address_of_engine_11() { return &___engine_11; }
	inline void set_engine_11(AdvEngine_t1176753927 * value)
	{
		___engine_11 = value;
		Il2CppCodeGenWriteBarrier((&___engine_11), value);
	}

	inline static int32_t get_offset_of_isInit_12() { return static_cast<int32_t>(offsetof(AdvSystemSaveData_t276732409, ___isInit_12)); }
	inline bool get_isInit_12() const { return ___isInit_12; }
	inline bool* get_address_of_isInit_12() { return &___isInit_12; }
	inline void set_isInit_12(bool value)
	{
		___isInit_12 = value;
	}
};

struct AdvSystemSaveData_t276732409_StaticFields
{
public:
	// System.Int32 Utage.AdvSystemSaveData::MagicID
	int32_t ___MagicID_13;

public:
	inline static int32_t get_offset_of_MagicID_13() { return static_cast<int32_t>(offsetof(AdvSystemSaveData_t276732409_StaticFields, ___MagicID_13)); }
	inline int32_t get_MagicID_13() const { return ___MagicID_13; }
	inline int32_t* get_address_of_MagicID_13() { return &___MagicID_13; }
	inline void set_MagicID_13(int32_t value)
	{
		___MagicID_13 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ADVSYSTEMSAVEDATA_T276732409_H
#ifndef ADVGRAPHICRENDERTEXTUREMANAGER_T995712561_H
#define ADVGRAPHICRENDERTEXTUREMANAGER_T995712561_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.AdvGraphicRenderTextureManager
struct  AdvGraphicRenderTextureManager_t995712561  : public MonoBehaviour_t1158329972
{
public:
	// System.Single Utage.AdvGraphicRenderTextureManager::offset
	float ___offset_2;
	// System.Collections.Generic.List`1<Utage.AdvRenderTextureSpace> Utage.AdvGraphicRenderTextureManager::spaceList
	List_1_t2851308344 * ___spaceList_3;

public:
	inline static int32_t get_offset_of_offset_2() { return static_cast<int32_t>(offsetof(AdvGraphicRenderTextureManager_t995712561, ___offset_2)); }
	inline float get_offset_2() const { return ___offset_2; }
	inline float* get_address_of_offset_2() { return &___offset_2; }
	inline void set_offset_2(float value)
	{
		___offset_2 = value;
	}

	inline static int32_t get_offset_of_spaceList_3() { return static_cast<int32_t>(offsetof(AdvGraphicRenderTextureManager_t995712561, ___spaceList_3)); }
	inline List_1_t2851308344 * get_spaceList_3() const { return ___spaceList_3; }
	inline List_1_t2851308344 ** get_address_of_spaceList_3() { return &___spaceList_3; }
	inline void set_spaceList_3(List_1_t2851308344 * value)
	{
		___spaceList_3 = value;
		Il2CppCodeGenWriteBarrier((&___spaceList_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ADVGRAPHICRENDERTEXTUREMANAGER_T995712561_H
#ifndef ADVCONFIG_T2941837115_H
#define ADVCONFIG_T2941837115_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.AdvConfig
struct  AdvConfig_t2941837115  : public MonoBehaviour_t1158329972
{
public:
	// System.Boolean Utage.AdvConfig::dontUseSystemSaveData
	bool ___dontUseSystemSaveData_2;
	// System.Boolean Utage.AdvConfig::dontSaveFullScreen
	bool ___dontSaveFullScreen_3;
	// System.Single Utage.AdvConfig::sendCharWaitSecMax
	float ___sendCharWaitSecMax_4;
	// System.Single Utage.AdvConfig::autoPageWaitSecMax
	float ___autoPageWaitSecMax_5;
	// System.Single Utage.AdvConfig::autoPageWaitSecMin
	float ___autoPageWaitSecMin_6;
	// System.Single Utage.AdvConfig::bgmVolumeFilterOfPlayingVoice
	float ___bgmVolumeFilterOfPlayingVoice_7;
	// System.Boolean Utage.AdvConfig::forceSkipInputCtl
	bool ___forceSkipInputCtl_8;
	// System.Boolean Utage.AdvConfig::useMessageSpeedRead
	bool ___useMessageSpeedRead_9;
	// System.Single Utage.AdvConfig::skipSpeed
	float ___skipSpeed_10;
	// System.Boolean Utage.AdvConfig::skipVoiceAndSe
	bool ___skipVoiceAndSe_11;
	// Utage.AdvConfigSaveData Utage.AdvConfig::defaultData
	AdvConfigSaveData_t4190953152 * ___defaultData_12;
	// Utage.AdvConfigSaveData Utage.AdvConfig::current
	AdvConfigSaveData_t4190953152 * ___current_13;
	// System.Boolean Utage.AdvConfig::isSkip
	bool ___isSkip_14;

public:
	inline static int32_t get_offset_of_dontUseSystemSaveData_2() { return static_cast<int32_t>(offsetof(AdvConfig_t2941837115, ___dontUseSystemSaveData_2)); }
	inline bool get_dontUseSystemSaveData_2() const { return ___dontUseSystemSaveData_2; }
	inline bool* get_address_of_dontUseSystemSaveData_2() { return &___dontUseSystemSaveData_2; }
	inline void set_dontUseSystemSaveData_2(bool value)
	{
		___dontUseSystemSaveData_2 = value;
	}

	inline static int32_t get_offset_of_dontSaveFullScreen_3() { return static_cast<int32_t>(offsetof(AdvConfig_t2941837115, ___dontSaveFullScreen_3)); }
	inline bool get_dontSaveFullScreen_3() const { return ___dontSaveFullScreen_3; }
	inline bool* get_address_of_dontSaveFullScreen_3() { return &___dontSaveFullScreen_3; }
	inline void set_dontSaveFullScreen_3(bool value)
	{
		___dontSaveFullScreen_3 = value;
	}

	inline static int32_t get_offset_of_sendCharWaitSecMax_4() { return static_cast<int32_t>(offsetof(AdvConfig_t2941837115, ___sendCharWaitSecMax_4)); }
	inline float get_sendCharWaitSecMax_4() const { return ___sendCharWaitSecMax_4; }
	inline float* get_address_of_sendCharWaitSecMax_4() { return &___sendCharWaitSecMax_4; }
	inline void set_sendCharWaitSecMax_4(float value)
	{
		___sendCharWaitSecMax_4 = value;
	}

	inline static int32_t get_offset_of_autoPageWaitSecMax_5() { return static_cast<int32_t>(offsetof(AdvConfig_t2941837115, ___autoPageWaitSecMax_5)); }
	inline float get_autoPageWaitSecMax_5() const { return ___autoPageWaitSecMax_5; }
	inline float* get_address_of_autoPageWaitSecMax_5() { return &___autoPageWaitSecMax_5; }
	inline void set_autoPageWaitSecMax_5(float value)
	{
		___autoPageWaitSecMax_5 = value;
	}

	inline static int32_t get_offset_of_autoPageWaitSecMin_6() { return static_cast<int32_t>(offsetof(AdvConfig_t2941837115, ___autoPageWaitSecMin_6)); }
	inline float get_autoPageWaitSecMin_6() const { return ___autoPageWaitSecMin_6; }
	inline float* get_address_of_autoPageWaitSecMin_6() { return &___autoPageWaitSecMin_6; }
	inline void set_autoPageWaitSecMin_6(float value)
	{
		___autoPageWaitSecMin_6 = value;
	}

	inline static int32_t get_offset_of_bgmVolumeFilterOfPlayingVoice_7() { return static_cast<int32_t>(offsetof(AdvConfig_t2941837115, ___bgmVolumeFilterOfPlayingVoice_7)); }
	inline float get_bgmVolumeFilterOfPlayingVoice_7() const { return ___bgmVolumeFilterOfPlayingVoice_7; }
	inline float* get_address_of_bgmVolumeFilterOfPlayingVoice_7() { return &___bgmVolumeFilterOfPlayingVoice_7; }
	inline void set_bgmVolumeFilterOfPlayingVoice_7(float value)
	{
		___bgmVolumeFilterOfPlayingVoice_7 = value;
	}

	inline static int32_t get_offset_of_forceSkipInputCtl_8() { return static_cast<int32_t>(offsetof(AdvConfig_t2941837115, ___forceSkipInputCtl_8)); }
	inline bool get_forceSkipInputCtl_8() const { return ___forceSkipInputCtl_8; }
	inline bool* get_address_of_forceSkipInputCtl_8() { return &___forceSkipInputCtl_8; }
	inline void set_forceSkipInputCtl_8(bool value)
	{
		___forceSkipInputCtl_8 = value;
	}

	inline static int32_t get_offset_of_useMessageSpeedRead_9() { return static_cast<int32_t>(offsetof(AdvConfig_t2941837115, ___useMessageSpeedRead_9)); }
	inline bool get_useMessageSpeedRead_9() const { return ___useMessageSpeedRead_9; }
	inline bool* get_address_of_useMessageSpeedRead_9() { return &___useMessageSpeedRead_9; }
	inline void set_useMessageSpeedRead_9(bool value)
	{
		___useMessageSpeedRead_9 = value;
	}

	inline static int32_t get_offset_of_skipSpeed_10() { return static_cast<int32_t>(offsetof(AdvConfig_t2941837115, ___skipSpeed_10)); }
	inline float get_skipSpeed_10() const { return ___skipSpeed_10; }
	inline float* get_address_of_skipSpeed_10() { return &___skipSpeed_10; }
	inline void set_skipSpeed_10(float value)
	{
		___skipSpeed_10 = value;
	}

	inline static int32_t get_offset_of_skipVoiceAndSe_11() { return static_cast<int32_t>(offsetof(AdvConfig_t2941837115, ___skipVoiceAndSe_11)); }
	inline bool get_skipVoiceAndSe_11() const { return ___skipVoiceAndSe_11; }
	inline bool* get_address_of_skipVoiceAndSe_11() { return &___skipVoiceAndSe_11; }
	inline void set_skipVoiceAndSe_11(bool value)
	{
		___skipVoiceAndSe_11 = value;
	}

	inline static int32_t get_offset_of_defaultData_12() { return static_cast<int32_t>(offsetof(AdvConfig_t2941837115, ___defaultData_12)); }
	inline AdvConfigSaveData_t4190953152 * get_defaultData_12() const { return ___defaultData_12; }
	inline AdvConfigSaveData_t4190953152 ** get_address_of_defaultData_12() { return &___defaultData_12; }
	inline void set_defaultData_12(AdvConfigSaveData_t4190953152 * value)
	{
		___defaultData_12 = value;
		Il2CppCodeGenWriteBarrier((&___defaultData_12), value);
	}

	inline static int32_t get_offset_of_current_13() { return static_cast<int32_t>(offsetof(AdvConfig_t2941837115, ___current_13)); }
	inline AdvConfigSaveData_t4190953152 * get_current_13() const { return ___current_13; }
	inline AdvConfigSaveData_t4190953152 ** get_address_of_current_13() { return &___current_13; }
	inline void set_current_13(AdvConfigSaveData_t4190953152 * value)
	{
		___current_13 = value;
		Il2CppCodeGenWriteBarrier((&___current_13), value);
	}

	inline static int32_t get_offset_of_isSkip_14() { return static_cast<int32_t>(offsetof(AdvConfig_t2941837115, ___isSkip_14)); }
	inline bool get_isSkip_14() const { return ___isSkip_14; }
	inline bool* get_address_of_isSkip_14() { return &___isSkip_14; }
	inline void set_isSkip_14(bool value)
	{
		___isSkip_14 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ADVCONFIG_T2941837115_H
#ifndef ADVPAGE_T2715018132_H
#define ADVPAGE_T2715018132_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.AdvPage
struct  AdvPage_t2715018132  : public MonoBehaviour_t1158329972
{
public:
	// Utage.AdvPageEvent Utage.AdvPage::onBeginPage
	AdvPageEvent_t790047568 * ___onBeginPage_2;
	// Utage.AdvPageEvent Utage.AdvPage::onBeginText
	AdvPageEvent_t790047568 * ___onBeginText_3;
	// Utage.AdvPageEvent Utage.AdvPage::onChangeText
	AdvPageEvent_t790047568 * ___onChangeText_4;
	// Utage.AdvPageEvent Utage.AdvPage::onUpdateSendChar
	AdvPageEvent_t790047568 * ___onUpdateSendChar_5;
	// Utage.AdvPageEvent Utage.AdvPage::onEndText
	AdvPageEvent_t790047568 * ___onEndText_6;
	// Utage.AdvPageEvent Utage.AdvPage::onEndPage
	AdvPageEvent_t790047568 * ___onEndPage_7;
	// Utage.AdvPageEvent Utage.AdvPage::onChangeStatus
	AdvPageEvent_t790047568 * ___onChangeStatus_8;
	// Utage.AdvPageEvent Utage.AdvPage::onTrigWaitInputInPage
	AdvPageEvent_t790047568 * ___onTrigWaitInputInPage_9;
	// Utage.AdvPageEvent Utage.AdvPage::onTrigWaitInputBrPage
	AdvPageEvent_t790047568 * ___onTrigWaitInputBrPage_10;
	// Utage.AdvPageEvent Utage.AdvPage::onTrigInput
	AdvPageEvent_t790047568 * ___onTrigInput_11;
	// Utage.AdvScenarioPageData Utage.AdvPage::<CurrentData>k__BackingField
	AdvScenarioPageData_t3333166790 * ___U3CCurrentDataU3Ek__BackingField_12;
	// Utage.TextData Utage.AdvPage::<TextData>k__BackingField
	TextData_t603454315 * ___U3CTextDataU3Ek__BackingField_13;
	// Utage.AdvCommandText Utage.AdvPage::<CurrentTextDataInPage>k__BackingField
	AdvCommandText_t816833175 * ___U3CCurrentTextDataInPageU3Ek__BackingField_14;
	// System.Collections.Generic.List`1<Utage.AdvCommandText> Utage.AdvPage::textDataList
	List_1_t185954307 * ___textDataList_15;
	// Utage.AdvCharacterInfo Utage.AdvPage::<CharacterInfo>k__BackingField
	AdvCharacterInfo_t1582765630 * ___U3CCharacterInfoU3Ek__BackingField_16;
	// System.String Utage.AdvPage::<SaveDataTitle>k__BackingField
	String_t* ___U3CSaveDataTitleU3Ek__BackingField_17;
	// System.Int32 Utage.AdvPage::<CurrentTextLength>k__BackingField
	int32_t ___U3CCurrentTextLengthU3Ek__BackingField_18;
	// System.Int32 Utage.AdvPage::<CurrentTextLengthMax>k__BackingField
	int32_t ___U3CCurrentTextLengthMaxU3Ek__BackingField_19;
	// Utage.AdvPage/PageStatus Utage.AdvPage::status
	int32_t ___status_20;
	// System.Boolean Utage.AdvPage::<IsWaitingInputCommand>k__BackingField
	bool ___U3CIsWaitingInputCommandU3Ek__BackingField_21;
	// Utage.AdvPageController Utage.AdvPage::contoller
	AdvPageController_t1125789268 * ___contoller_22;
	// Utage.AdvEngine Utage.AdvPage::engine
	AdvEngine_t1176753927 * ___engine_23;
	// System.Boolean Utage.AdvPage::isInputSendMessage
	bool ___isInputSendMessage_24;
	// System.Boolean Utage.AdvPage::<LastInputSendMessage>k__BackingField
	bool ___U3CLastInputSendMessageU3Ek__BackingField_25;
	// System.Single Utage.AdvPage::deltaTimeSendMessage
	float ___deltaTimeSendMessage_26;
	// System.Single Utage.AdvPage::waitingTimeInput
	float ___waitingTimeInput_27;

public:
	inline static int32_t get_offset_of_onBeginPage_2() { return static_cast<int32_t>(offsetof(AdvPage_t2715018132, ___onBeginPage_2)); }
	inline AdvPageEvent_t790047568 * get_onBeginPage_2() const { return ___onBeginPage_2; }
	inline AdvPageEvent_t790047568 ** get_address_of_onBeginPage_2() { return &___onBeginPage_2; }
	inline void set_onBeginPage_2(AdvPageEvent_t790047568 * value)
	{
		___onBeginPage_2 = value;
		Il2CppCodeGenWriteBarrier((&___onBeginPage_2), value);
	}

	inline static int32_t get_offset_of_onBeginText_3() { return static_cast<int32_t>(offsetof(AdvPage_t2715018132, ___onBeginText_3)); }
	inline AdvPageEvent_t790047568 * get_onBeginText_3() const { return ___onBeginText_3; }
	inline AdvPageEvent_t790047568 ** get_address_of_onBeginText_3() { return &___onBeginText_3; }
	inline void set_onBeginText_3(AdvPageEvent_t790047568 * value)
	{
		___onBeginText_3 = value;
		Il2CppCodeGenWriteBarrier((&___onBeginText_3), value);
	}

	inline static int32_t get_offset_of_onChangeText_4() { return static_cast<int32_t>(offsetof(AdvPage_t2715018132, ___onChangeText_4)); }
	inline AdvPageEvent_t790047568 * get_onChangeText_4() const { return ___onChangeText_4; }
	inline AdvPageEvent_t790047568 ** get_address_of_onChangeText_4() { return &___onChangeText_4; }
	inline void set_onChangeText_4(AdvPageEvent_t790047568 * value)
	{
		___onChangeText_4 = value;
		Il2CppCodeGenWriteBarrier((&___onChangeText_4), value);
	}

	inline static int32_t get_offset_of_onUpdateSendChar_5() { return static_cast<int32_t>(offsetof(AdvPage_t2715018132, ___onUpdateSendChar_5)); }
	inline AdvPageEvent_t790047568 * get_onUpdateSendChar_5() const { return ___onUpdateSendChar_5; }
	inline AdvPageEvent_t790047568 ** get_address_of_onUpdateSendChar_5() { return &___onUpdateSendChar_5; }
	inline void set_onUpdateSendChar_5(AdvPageEvent_t790047568 * value)
	{
		___onUpdateSendChar_5 = value;
		Il2CppCodeGenWriteBarrier((&___onUpdateSendChar_5), value);
	}

	inline static int32_t get_offset_of_onEndText_6() { return static_cast<int32_t>(offsetof(AdvPage_t2715018132, ___onEndText_6)); }
	inline AdvPageEvent_t790047568 * get_onEndText_6() const { return ___onEndText_6; }
	inline AdvPageEvent_t790047568 ** get_address_of_onEndText_6() { return &___onEndText_6; }
	inline void set_onEndText_6(AdvPageEvent_t790047568 * value)
	{
		___onEndText_6 = value;
		Il2CppCodeGenWriteBarrier((&___onEndText_6), value);
	}

	inline static int32_t get_offset_of_onEndPage_7() { return static_cast<int32_t>(offsetof(AdvPage_t2715018132, ___onEndPage_7)); }
	inline AdvPageEvent_t790047568 * get_onEndPage_7() const { return ___onEndPage_7; }
	inline AdvPageEvent_t790047568 ** get_address_of_onEndPage_7() { return &___onEndPage_7; }
	inline void set_onEndPage_7(AdvPageEvent_t790047568 * value)
	{
		___onEndPage_7 = value;
		Il2CppCodeGenWriteBarrier((&___onEndPage_7), value);
	}

	inline static int32_t get_offset_of_onChangeStatus_8() { return static_cast<int32_t>(offsetof(AdvPage_t2715018132, ___onChangeStatus_8)); }
	inline AdvPageEvent_t790047568 * get_onChangeStatus_8() const { return ___onChangeStatus_8; }
	inline AdvPageEvent_t790047568 ** get_address_of_onChangeStatus_8() { return &___onChangeStatus_8; }
	inline void set_onChangeStatus_8(AdvPageEvent_t790047568 * value)
	{
		___onChangeStatus_8 = value;
		Il2CppCodeGenWriteBarrier((&___onChangeStatus_8), value);
	}

	inline static int32_t get_offset_of_onTrigWaitInputInPage_9() { return static_cast<int32_t>(offsetof(AdvPage_t2715018132, ___onTrigWaitInputInPage_9)); }
	inline AdvPageEvent_t790047568 * get_onTrigWaitInputInPage_9() const { return ___onTrigWaitInputInPage_9; }
	inline AdvPageEvent_t790047568 ** get_address_of_onTrigWaitInputInPage_9() { return &___onTrigWaitInputInPage_9; }
	inline void set_onTrigWaitInputInPage_9(AdvPageEvent_t790047568 * value)
	{
		___onTrigWaitInputInPage_9 = value;
		Il2CppCodeGenWriteBarrier((&___onTrigWaitInputInPage_9), value);
	}

	inline static int32_t get_offset_of_onTrigWaitInputBrPage_10() { return static_cast<int32_t>(offsetof(AdvPage_t2715018132, ___onTrigWaitInputBrPage_10)); }
	inline AdvPageEvent_t790047568 * get_onTrigWaitInputBrPage_10() const { return ___onTrigWaitInputBrPage_10; }
	inline AdvPageEvent_t790047568 ** get_address_of_onTrigWaitInputBrPage_10() { return &___onTrigWaitInputBrPage_10; }
	inline void set_onTrigWaitInputBrPage_10(AdvPageEvent_t790047568 * value)
	{
		___onTrigWaitInputBrPage_10 = value;
		Il2CppCodeGenWriteBarrier((&___onTrigWaitInputBrPage_10), value);
	}

	inline static int32_t get_offset_of_onTrigInput_11() { return static_cast<int32_t>(offsetof(AdvPage_t2715018132, ___onTrigInput_11)); }
	inline AdvPageEvent_t790047568 * get_onTrigInput_11() const { return ___onTrigInput_11; }
	inline AdvPageEvent_t790047568 ** get_address_of_onTrigInput_11() { return &___onTrigInput_11; }
	inline void set_onTrigInput_11(AdvPageEvent_t790047568 * value)
	{
		___onTrigInput_11 = value;
		Il2CppCodeGenWriteBarrier((&___onTrigInput_11), value);
	}

	inline static int32_t get_offset_of_U3CCurrentDataU3Ek__BackingField_12() { return static_cast<int32_t>(offsetof(AdvPage_t2715018132, ___U3CCurrentDataU3Ek__BackingField_12)); }
	inline AdvScenarioPageData_t3333166790 * get_U3CCurrentDataU3Ek__BackingField_12() const { return ___U3CCurrentDataU3Ek__BackingField_12; }
	inline AdvScenarioPageData_t3333166790 ** get_address_of_U3CCurrentDataU3Ek__BackingField_12() { return &___U3CCurrentDataU3Ek__BackingField_12; }
	inline void set_U3CCurrentDataU3Ek__BackingField_12(AdvScenarioPageData_t3333166790 * value)
	{
		___U3CCurrentDataU3Ek__BackingField_12 = value;
		Il2CppCodeGenWriteBarrier((&___U3CCurrentDataU3Ek__BackingField_12), value);
	}

	inline static int32_t get_offset_of_U3CTextDataU3Ek__BackingField_13() { return static_cast<int32_t>(offsetof(AdvPage_t2715018132, ___U3CTextDataU3Ek__BackingField_13)); }
	inline TextData_t603454315 * get_U3CTextDataU3Ek__BackingField_13() const { return ___U3CTextDataU3Ek__BackingField_13; }
	inline TextData_t603454315 ** get_address_of_U3CTextDataU3Ek__BackingField_13() { return &___U3CTextDataU3Ek__BackingField_13; }
	inline void set_U3CTextDataU3Ek__BackingField_13(TextData_t603454315 * value)
	{
		___U3CTextDataU3Ek__BackingField_13 = value;
		Il2CppCodeGenWriteBarrier((&___U3CTextDataU3Ek__BackingField_13), value);
	}

	inline static int32_t get_offset_of_U3CCurrentTextDataInPageU3Ek__BackingField_14() { return static_cast<int32_t>(offsetof(AdvPage_t2715018132, ___U3CCurrentTextDataInPageU3Ek__BackingField_14)); }
	inline AdvCommandText_t816833175 * get_U3CCurrentTextDataInPageU3Ek__BackingField_14() const { return ___U3CCurrentTextDataInPageU3Ek__BackingField_14; }
	inline AdvCommandText_t816833175 ** get_address_of_U3CCurrentTextDataInPageU3Ek__BackingField_14() { return &___U3CCurrentTextDataInPageU3Ek__BackingField_14; }
	inline void set_U3CCurrentTextDataInPageU3Ek__BackingField_14(AdvCommandText_t816833175 * value)
	{
		___U3CCurrentTextDataInPageU3Ek__BackingField_14 = value;
		Il2CppCodeGenWriteBarrier((&___U3CCurrentTextDataInPageU3Ek__BackingField_14), value);
	}

	inline static int32_t get_offset_of_textDataList_15() { return static_cast<int32_t>(offsetof(AdvPage_t2715018132, ___textDataList_15)); }
	inline List_1_t185954307 * get_textDataList_15() const { return ___textDataList_15; }
	inline List_1_t185954307 ** get_address_of_textDataList_15() { return &___textDataList_15; }
	inline void set_textDataList_15(List_1_t185954307 * value)
	{
		___textDataList_15 = value;
		Il2CppCodeGenWriteBarrier((&___textDataList_15), value);
	}

	inline static int32_t get_offset_of_U3CCharacterInfoU3Ek__BackingField_16() { return static_cast<int32_t>(offsetof(AdvPage_t2715018132, ___U3CCharacterInfoU3Ek__BackingField_16)); }
	inline AdvCharacterInfo_t1582765630 * get_U3CCharacterInfoU3Ek__BackingField_16() const { return ___U3CCharacterInfoU3Ek__BackingField_16; }
	inline AdvCharacterInfo_t1582765630 ** get_address_of_U3CCharacterInfoU3Ek__BackingField_16() { return &___U3CCharacterInfoU3Ek__BackingField_16; }
	inline void set_U3CCharacterInfoU3Ek__BackingField_16(AdvCharacterInfo_t1582765630 * value)
	{
		___U3CCharacterInfoU3Ek__BackingField_16 = value;
		Il2CppCodeGenWriteBarrier((&___U3CCharacterInfoU3Ek__BackingField_16), value);
	}

	inline static int32_t get_offset_of_U3CSaveDataTitleU3Ek__BackingField_17() { return static_cast<int32_t>(offsetof(AdvPage_t2715018132, ___U3CSaveDataTitleU3Ek__BackingField_17)); }
	inline String_t* get_U3CSaveDataTitleU3Ek__BackingField_17() const { return ___U3CSaveDataTitleU3Ek__BackingField_17; }
	inline String_t** get_address_of_U3CSaveDataTitleU3Ek__BackingField_17() { return &___U3CSaveDataTitleU3Ek__BackingField_17; }
	inline void set_U3CSaveDataTitleU3Ek__BackingField_17(String_t* value)
	{
		___U3CSaveDataTitleU3Ek__BackingField_17 = value;
		Il2CppCodeGenWriteBarrier((&___U3CSaveDataTitleU3Ek__BackingField_17), value);
	}

	inline static int32_t get_offset_of_U3CCurrentTextLengthU3Ek__BackingField_18() { return static_cast<int32_t>(offsetof(AdvPage_t2715018132, ___U3CCurrentTextLengthU3Ek__BackingField_18)); }
	inline int32_t get_U3CCurrentTextLengthU3Ek__BackingField_18() const { return ___U3CCurrentTextLengthU3Ek__BackingField_18; }
	inline int32_t* get_address_of_U3CCurrentTextLengthU3Ek__BackingField_18() { return &___U3CCurrentTextLengthU3Ek__BackingField_18; }
	inline void set_U3CCurrentTextLengthU3Ek__BackingField_18(int32_t value)
	{
		___U3CCurrentTextLengthU3Ek__BackingField_18 = value;
	}

	inline static int32_t get_offset_of_U3CCurrentTextLengthMaxU3Ek__BackingField_19() { return static_cast<int32_t>(offsetof(AdvPage_t2715018132, ___U3CCurrentTextLengthMaxU3Ek__BackingField_19)); }
	inline int32_t get_U3CCurrentTextLengthMaxU3Ek__BackingField_19() const { return ___U3CCurrentTextLengthMaxU3Ek__BackingField_19; }
	inline int32_t* get_address_of_U3CCurrentTextLengthMaxU3Ek__BackingField_19() { return &___U3CCurrentTextLengthMaxU3Ek__BackingField_19; }
	inline void set_U3CCurrentTextLengthMaxU3Ek__BackingField_19(int32_t value)
	{
		___U3CCurrentTextLengthMaxU3Ek__BackingField_19 = value;
	}

	inline static int32_t get_offset_of_status_20() { return static_cast<int32_t>(offsetof(AdvPage_t2715018132, ___status_20)); }
	inline int32_t get_status_20() const { return ___status_20; }
	inline int32_t* get_address_of_status_20() { return &___status_20; }
	inline void set_status_20(int32_t value)
	{
		___status_20 = value;
	}

	inline static int32_t get_offset_of_U3CIsWaitingInputCommandU3Ek__BackingField_21() { return static_cast<int32_t>(offsetof(AdvPage_t2715018132, ___U3CIsWaitingInputCommandU3Ek__BackingField_21)); }
	inline bool get_U3CIsWaitingInputCommandU3Ek__BackingField_21() const { return ___U3CIsWaitingInputCommandU3Ek__BackingField_21; }
	inline bool* get_address_of_U3CIsWaitingInputCommandU3Ek__BackingField_21() { return &___U3CIsWaitingInputCommandU3Ek__BackingField_21; }
	inline void set_U3CIsWaitingInputCommandU3Ek__BackingField_21(bool value)
	{
		___U3CIsWaitingInputCommandU3Ek__BackingField_21 = value;
	}

	inline static int32_t get_offset_of_contoller_22() { return static_cast<int32_t>(offsetof(AdvPage_t2715018132, ___contoller_22)); }
	inline AdvPageController_t1125789268 * get_contoller_22() const { return ___contoller_22; }
	inline AdvPageController_t1125789268 ** get_address_of_contoller_22() { return &___contoller_22; }
	inline void set_contoller_22(AdvPageController_t1125789268 * value)
	{
		___contoller_22 = value;
		Il2CppCodeGenWriteBarrier((&___contoller_22), value);
	}

	inline static int32_t get_offset_of_engine_23() { return static_cast<int32_t>(offsetof(AdvPage_t2715018132, ___engine_23)); }
	inline AdvEngine_t1176753927 * get_engine_23() const { return ___engine_23; }
	inline AdvEngine_t1176753927 ** get_address_of_engine_23() { return &___engine_23; }
	inline void set_engine_23(AdvEngine_t1176753927 * value)
	{
		___engine_23 = value;
		Il2CppCodeGenWriteBarrier((&___engine_23), value);
	}

	inline static int32_t get_offset_of_isInputSendMessage_24() { return static_cast<int32_t>(offsetof(AdvPage_t2715018132, ___isInputSendMessage_24)); }
	inline bool get_isInputSendMessage_24() const { return ___isInputSendMessage_24; }
	inline bool* get_address_of_isInputSendMessage_24() { return &___isInputSendMessage_24; }
	inline void set_isInputSendMessage_24(bool value)
	{
		___isInputSendMessage_24 = value;
	}

	inline static int32_t get_offset_of_U3CLastInputSendMessageU3Ek__BackingField_25() { return static_cast<int32_t>(offsetof(AdvPage_t2715018132, ___U3CLastInputSendMessageU3Ek__BackingField_25)); }
	inline bool get_U3CLastInputSendMessageU3Ek__BackingField_25() const { return ___U3CLastInputSendMessageU3Ek__BackingField_25; }
	inline bool* get_address_of_U3CLastInputSendMessageU3Ek__BackingField_25() { return &___U3CLastInputSendMessageU3Ek__BackingField_25; }
	inline void set_U3CLastInputSendMessageU3Ek__BackingField_25(bool value)
	{
		___U3CLastInputSendMessageU3Ek__BackingField_25 = value;
	}

	inline static int32_t get_offset_of_deltaTimeSendMessage_26() { return static_cast<int32_t>(offsetof(AdvPage_t2715018132, ___deltaTimeSendMessage_26)); }
	inline float get_deltaTimeSendMessage_26() const { return ___deltaTimeSendMessage_26; }
	inline float* get_address_of_deltaTimeSendMessage_26() { return &___deltaTimeSendMessage_26; }
	inline void set_deltaTimeSendMessage_26(float value)
	{
		___deltaTimeSendMessage_26 = value;
	}

	inline static int32_t get_offset_of_waitingTimeInput_27() { return static_cast<int32_t>(offsetof(AdvPage_t2715018132, ___waitingTimeInput_27)); }
	inline float get_waitingTimeInput_27() const { return ___waitingTimeInput_27; }
	inline float* get_address_of_waitingTimeInput_27() { return &___waitingTimeInput_27; }
	inline void set_waitingTimeInput_27(float value)
	{
		___waitingTimeInput_27 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ADVPAGE_T2715018132_H
#ifndef ADVBACKLOGMANAGER_T3496936997_H
#define ADVBACKLOGMANAGER_T3496936997_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.AdvBacklogManager
struct  AdvBacklogManager_t3496936997  : public MonoBehaviour_t1158329972
{
public:
	// System.Int32 Utage.AdvBacklogManager::maxLog
	int32_t ___maxLog_2;
	// System.Boolean Utage.AdvBacklogManager::ignoreLog
	bool ___ignoreLog_3;
	// Utage.BacklogEvent Utage.AdvBacklogManager::onAddPage
	BacklogEvent_t3497267401 * ___onAddPage_4;
	// Utage.BacklogEvent Utage.AdvBacklogManager::onAddData
	BacklogEvent_t3497267401 * ___onAddData_5;
	// System.Collections.Generic.List`1<Utage.AdvBacklog> Utage.AdvBacklogManager::backlogs
	List_1_t3296422422 * ___backlogs_6;

public:
	inline static int32_t get_offset_of_maxLog_2() { return static_cast<int32_t>(offsetof(AdvBacklogManager_t3496936997, ___maxLog_2)); }
	inline int32_t get_maxLog_2() const { return ___maxLog_2; }
	inline int32_t* get_address_of_maxLog_2() { return &___maxLog_2; }
	inline void set_maxLog_2(int32_t value)
	{
		___maxLog_2 = value;
	}

	inline static int32_t get_offset_of_ignoreLog_3() { return static_cast<int32_t>(offsetof(AdvBacklogManager_t3496936997, ___ignoreLog_3)); }
	inline bool get_ignoreLog_3() const { return ___ignoreLog_3; }
	inline bool* get_address_of_ignoreLog_3() { return &___ignoreLog_3; }
	inline void set_ignoreLog_3(bool value)
	{
		___ignoreLog_3 = value;
	}

	inline static int32_t get_offset_of_onAddPage_4() { return static_cast<int32_t>(offsetof(AdvBacklogManager_t3496936997, ___onAddPage_4)); }
	inline BacklogEvent_t3497267401 * get_onAddPage_4() const { return ___onAddPage_4; }
	inline BacklogEvent_t3497267401 ** get_address_of_onAddPage_4() { return &___onAddPage_4; }
	inline void set_onAddPage_4(BacklogEvent_t3497267401 * value)
	{
		___onAddPage_4 = value;
		Il2CppCodeGenWriteBarrier((&___onAddPage_4), value);
	}

	inline static int32_t get_offset_of_onAddData_5() { return static_cast<int32_t>(offsetof(AdvBacklogManager_t3496936997, ___onAddData_5)); }
	inline BacklogEvent_t3497267401 * get_onAddData_5() const { return ___onAddData_5; }
	inline BacklogEvent_t3497267401 ** get_address_of_onAddData_5() { return &___onAddData_5; }
	inline void set_onAddData_5(BacklogEvent_t3497267401 * value)
	{
		___onAddData_5 = value;
		Il2CppCodeGenWriteBarrier((&___onAddData_5), value);
	}

	inline static int32_t get_offset_of_backlogs_6() { return static_cast<int32_t>(offsetof(AdvBacklogManager_t3496936997, ___backlogs_6)); }
	inline List_1_t3296422422 * get_backlogs_6() const { return ___backlogs_6; }
	inline List_1_t3296422422 ** get_address_of_backlogs_6() { return &___backlogs_6; }
	inline void set_backlogs_6(List_1_t3296422422 * value)
	{
		___backlogs_6 = value;
		Il2CppCodeGenWriteBarrier((&___backlogs_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ADVBACKLOGMANAGER_T3496936997_H
#ifndef ADVGRAPHICOBJECTPREFABBASE_T1129217777_H
#define ADVGRAPHICOBJECTPREFABBASE_T1129217777_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.AdvGraphicObjectPrefabBase
struct  AdvGraphicObjectPrefabBase_t1129217777  : public AdvGraphicBase_t2725442072
{
public:
	// UnityEngine.GameObject Utage.AdvGraphicObjectPrefabBase::currentObject
	GameObject_t1756533147 * ___currentObject_3;
	// UnityEngine.Animator Utage.AdvGraphicObjectPrefabBase::animator
	Animator_t69676727 * ___animator_4;
	// System.String Utage.AdvGraphicObjectPrefabBase::<AnimationStateName>k__BackingField
	String_t* ___U3CAnimationStateNameU3Ek__BackingField_5;

public:
	inline static int32_t get_offset_of_currentObject_3() { return static_cast<int32_t>(offsetof(AdvGraphicObjectPrefabBase_t1129217777, ___currentObject_3)); }
	inline GameObject_t1756533147 * get_currentObject_3() const { return ___currentObject_3; }
	inline GameObject_t1756533147 ** get_address_of_currentObject_3() { return &___currentObject_3; }
	inline void set_currentObject_3(GameObject_t1756533147 * value)
	{
		___currentObject_3 = value;
		Il2CppCodeGenWriteBarrier((&___currentObject_3), value);
	}

	inline static int32_t get_offset_of_animator_4() { return static_cast<int32_t>(offsetof(AdvGraphicObjectPrefabBase_t1129217777, ___animator_4)); }
	inline Animator_t69676727 * get_animator_4() const { return ___animator_4; }
	inline Animator_t69676727 ** get_address_of_animator_4() { return &___animator_4; }
	inline void set_animator_4(Animator_t69676727 * value)
	{
		___animator_4 = value;
		Il2CppCodeGenWriteBarrier((&___animator_4), value);
	}

	inline static int32_t get_offset_of_U3CAnimationStateNameU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(AdvGraphicObjectPrefabBase_t1129217777, ___U3CAnimationStateNameU3Ek__BackingField_5)); }
	inline String_t* get_U3CAnimationStateNameU3Ek__BackingField_5() const { return ___U3CAnimationStateNameU3Ek__BackingField_5; }
	inline String_t** get_address_of_U3CAnimationStateNameU3Ek__BackingField_5() { return &___U3CAnimationStateNameU3Ek__BackingField_5; }
	inline void set_U3CAnimationStateNameU3Ek__BackingField_5(String_t* value)
	{
		___U3CAnimationStateNameU3Ek__BackingField_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3CAnimationStateNameU3Ek__BackingField_5), value);
	}
};

struct AdvGraphicObjectPrefabBase_t1129217777_StaticFields
{
public:
	// System.Action Utage.AdvGraphicObjectPrefabBase::<>f__am$cache0
	Action_t3226471752 * ___U3CU3Ef__amU24cache0_7;

public:
	inline static int32_t get_offset_of_U3CU3Ef__amU24cache0_7() { return static_cast<int32_t>(offsetof(AdvGraphicObjectPrefabBase_t1129217777_StaticFields, ___U3CU3Ef__amU24cache0_7)); }
	inline Action_t3226471752 * get_U3CU3Ef__amU24cache0_7() const { return ___U3CU3Ef__amU24cache0_7; }
	inline Action_t3226471752 ** get_address_of_U3CU3Ef__amU24cache0_7() { return &___U3CU3Ef__amU24cache0_7; }
	inline void set_U3CU3Ef__amU24cache0_7(Action_t3226471752 * value)
	{
		___U3CU3Ef__amU24cache0_7 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache0_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ADVGRAPHICOBJECTPREFABBASE_T1129217777_H
#ifndef ADVGRAPHICOBJECTUGUIBASE_T2728041661_H
#define ADVGRAPHICOBJECTUGUIBASE_T2728041661_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.AdvGraphicObjectUguiBase
struct  AdvGraphicObjectUguiBase_t2728041661  : public AdvGraphicBase_t2725442072
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ADVGRAPHICOBJECTUGUIBASE_T2728041661_H
#ifndef ADVGRAPHICOBJECTRAWIMAGE_T1741698045_H
#define ADVGRAPHICOBJECTRAWIMAGE_T1741698045_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.AdvGraphicObjectRawImage
struct  AdvGraphicObjectRawImage_t1741698045  : public AdvGraphicObjectUguiBase_t2728041661
{
public:
	// UnityEngine.UI.RawImage Utage.AdvGraphicObjectRawImage::<RawImage>k__BackingField
	RawImage_t2749640213 * ___U3CRawImageU3Ek__BackingField_3;
	// Utage.AssetFileReference Utage.AdvGraphicObjectRawImage::crossFadeReference
	AssetFileReference_t4090268667 * ___crossFadeReference_4;

public:
	inline static int32_t get_offset_of_U3CRawImageU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(AdvGraphicObjectRawImage_t1741698045, ___U3CRawImageU3Ek__BackingField_3)); }
	inline RawImage_t2749640213 * get_U3CRawImageU3Ek__BackingField_3() const { return ___U3CRawImageU3Ek__BackingField_3; }
	inline RawImage_t2749640213 ** get_address_of_U3CRawImageU3Ek__BackingField_3() { return &___U3CRawImageU3Ek__BackingField_3; }
	inline void set_U3CRawImageU3Ek__BackingField_3(RawImage_t2749640213 * value)
	{
		___U3CRawImageU3Ek__BackingField_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CRawImageU3Ek__BackingField_3), value);
	}

	inline static int32_t get_offset_of_crossFadeReference_4() { return static_cast<int32_t>(offsetof(AdvGraphicObjectRawImage_t1741698045, ___crossFadeReference_4)); }
	inline AssetFileReference_t4090268667 * get_crossFadeReference_4() const { return ___crossFadeReference_4; }
	inline AssetFileReference_t4090268667 ** get_address_of_crossFadeReference_4() { return &___crossFadeReference_4; }
	inline void set_crossFadeReference_4(AssetFileReference_t4090268667 * value)
	{
		___crossFadeReference_4 = value;
		Il2CppCodeGenWriteBarrier((&___crossFadeReference_4), value);
	}
};

struct AdvGraphicObjectRawImage_t1741698045_StaticFields
{
public:
	// System.Action Utage.AdvGraphicObjectRawImage::<>f__am$cache0
	Action_t3226471752 * ___U3CU3Ef__amU24cache0_5;

public:
	inline static int32_t get_offset_of_U3CU3Ef__amU24cache0_5() { return static_cast<int32_t>(offsetof(AdvGraphicObjectRawImage_t1741698045_StaticFields, ___U3CU3Ef__amU24cache0_5)); }
	inline Action_t3226471752 * get_U3CU3Ef__amU24cache0_5() const { return ___U3CU3Ef__amU24cache0_5; }
	inline Action_t3226471752 ** get_address_of_U3CU3Ef__amU24cache0_5() { return &___U3CU3Ef__amU24cache0_5; }
	inline void set_U3CU3Ef__amU24cache0_5(Action_t3226471752 * value)
	{
		___U3CU3Ef__amU24cache0_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache0_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ADVGRAPHICOBJECTRAWIMAGE_T1741698045_H
#ifndef ADVGRAPHICOBJECTDICING_T4264806966_H
#define ADVGRAPHICOBJECTDICING_T4264806966_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.AdvGraphicObjectDicing
struct  AdvGraphicObjectDicing_t4264806966  : public AdvGraphicObjectUguiBase_t2728041661
{
public:
	// Utage.DicingImage Utage.AdvGraphicObjectDicing::<Dicing>k__BackingField
	DicingImage_t2721298607 * ___U3CDicingU3Ek__BackingField_3;
	// Utage.EyeBlinkDicing Utage.AdvGraphicObjectDicing::<EyeBlink>k__BackingField
	EyeBlinkDicing_t891209267 * ___U3CEyeBlinkU3Ek__BackingField_4;
	// Utage.LipSynchDicing Utage.AdvGraphicObjectDicing::<LipSynch>k__BackingField
	LipSynchDicing_t1869722688 * ___U3CLipSynchU3Ek__BackingField_5;
	// Utage.AdvAnimationPlayer Utage.AdvGraphicObjectDicing::<Animation>k__BackingField
	AdvAnimationPlayer_t1530269518 * ___U3CAnimationU3Ek__BackingField_6;
	// Utage.AssetFileReference Utage.AdvGraphicObjectDicing::crossFadeReference
	AssetFileReference_t4090268667 * ___crossFadeReference_7;

public:
	inline static int32_t get_offset_of_U3CDicingU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(AdvGraphicObjectDicing_t4264806966, ___U3CDicingU3Ek__BackingField_3)); }
	inline DicingImage_t2721298607 * get_U3CDicingU3Ek__BackingField_3() const { return ___U3CDicingU3Ek__BackingField_3; }
	inline DicingImage_t2721298607 ** get_address_of_U3CDicingU3Ek__BackingField_3() { return &___U3CDicingU3Ek__BackingField_3; }
	inline void set_U3CDicingU3Ek__BackingField_3(DicingImage_t2721298607 * value)
	{
		___U3CDicingU3Ek__BackingField_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CDicingU3Ek__BackingField_3), value);
	}

	inline static int32_t get_offset_of_U3CEyeBlinkU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(AdvGraphicObjectDicing_t4264806966, ___U3CEyeBlinkU3Ek__BackingField_4)); }
	inline EyeBlinkDicing_t891209267 * get_U3CEyeBlinkU3Ek__BackingField_4() const { return ___U3CEyeBlinkU3Ek__BackingField_4; }
	inline EyeBlinkDicing_t891209267 ** get_address_of_U3CEyeBlinkU3Ek__BackingField_4() { return &___U3CEyeBlinkU3Ek__BackingField_4; }
	inline void set_U3CEyeBlinkU3Ek__BackingField_4(EyeBlinkDicing_t891209267 * value)
	{
		___U3CEyeBlinkU3Ek__BackingField_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CEyeBlinkU3Ek__BackingField_4), value);
	}

	inline static int32_t get_offset_of_U3CLipSynchU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(AdvGraphicObjectDicing_t4264806966, ___U3CLipSynchU3Ek__BackingField_5)); }
	inline LipSynchDicing_t1869722688 * get_U3CLipSynchU3Ek__BackingField_5() const { return ___U3CLipSynchU3Ek__BackingField_5; }
	inline LipSynchDicing_t1869722688 ** get_address_of_U3CLipSynchU3Ek__BackingField_5() { return &___U3CLipSynchU3Ek__BackingField_5; }
	inline void set_U3CLipSynchU3Ek__BackingField_5(LipSynchDicing_t1869722688 * value)
	{
		___U3CLipSynchU3Ek__BackingField_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3CLipSynchU3Ek__BackingField_5), value);
	}

	inline static int32_t get_offset_of_U3CAnimationU3Ek__BackingField_6() { return static_cast<int32_t>(offsetof(AdvGraphicObjectDicing_t4264806966, ___U3CAnimationU3Ek__BackingField_6)); }
	inline AdvAnimationPlayer_t1530269518 * get_U3CAnimationU3Ek__BackingField_6() const { return ___U3CAnimationU3Ek__BackingField_6; }
	inline AdvAnimationPlayer_t1530269518 ** get_address_of_U3CAnimationU3Ek__BackingField_6() { return &___U3CAnimationU3Ek__BackingField_6; }
	inline void set_U3CAnimationU3Ek__BackingField_6(AdvAnimationPlayer_t1530269518 * value)
	{
		___U3CAnimationU3Ek__BackingField_6 = value;
		Il2CppCodeGenWriteBarrier((&___U3CAnimationU3Ek__BackingField_6), value);
	}

	inline static int32_t get_offset_of_crossFadeReference_7() { return static_cast<int32_t>(offsetof(AdvGraphicObjectDicing_t4264806966, ___crossFadeReference_7)); }
	inline AssetFileReference_t4090268667 * get_crossFadeReference_7() const { return ___crossFadeReference_7; }
	inline AssetFileReference_t4090268667 ** get_address_of_crossFadeReference_7() { return &___crossFadeReference_7; }
	inline void set_crossFadeReference_7(AssetFileReference_t4090268667 * value)
	{
		___crossFadeReference_7 = value;
		Il2CppCodeGenWriteBarrier((&___crossFadeReference_7), value);
	}
};

struct AdvGraphicObjectDicing_t4264806966_StaticFields
{
public:
	// System.Action Utage.AdvGraphicObjectDicing::<>f__am$cache0
	Action_t3226471752 * ___U3CU3Ef__amU24cache0_8;

public:
	inline static int32_t get_offset_of_U3CU3Ef__amU24cache0_8() { return static_cast<int32_t>(offsetof(AdvGraphicObjectDicing_t4264806966_StaticFields, ___U3CU3Ef__amU24cache0_8)); }
	inline Action_t3226471752 * get_U3CU3Ef__amU24cache0_8() const { return ___U3CU3Ef__amU24cache0_8; }
	inline Action_t3226471752 ** get_address_of_U3CU3Ef__amU24cache0_8() { return &___U3CU3Ef__amU24cache0_8; }
	inline void set_U3CU3Ef__amU24cache0_8(Action_t3226471752 * value)
	{
		___U3CU3Ef__amU24cache0_8 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache0_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ADVGRAPHICOBJECTDICING_T4264806966_H
#ifndef ADVGRAPHICOBJECTAVATAR_T3592307263_H
#define ADVGRAPHICOBJECTAVATAR_T3592307263_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.AdvGraphicObjectAvatar
struct  AdvGraphicObjectAvatar_t3592307263  : public AdvGraphicObjectUguiBase_t2728041661
{
public:
	// Utage.Timer Utage.AdvGraphicObjectAvatar::<FadeTimer>k__BackingField
	Timer_t2904185433 * ___U3CFadeTimerU3Ek__BackingField_3;
	// Utage.AvatarImage Utage.AdvGraphicObjectAvatar::<Avatar>k__BackingField
	AvatarImage_t1946614104 * ___U3CAvatarU3Ek__BackingField_4;
	// Utage.EyeBlinkAvatar Utage.AdvGraphicObjectAvatar::<EyeBlink>k__BackingField
	EyeBlinkAvatar_t3311504802 * ___U3CEyeBlinkU3Ek__BackingField_5;
	// Utage.LipSynchAvatar Utage.AdvGraphicObjectAvatar::<LipSynch>k__BackingField
	LipSynchAvatar_t872933325 * ___U3CLipSynchU3Ek__BackingField_6;
	// Utage.AdvAnimationPlayer Utage.AdvGraphicObjectAvatar::<Animation>k__BackingField
	AdvAnimationPlayer_t1530269518 * ___U3CAnimationU3Ek__BackingField_7;
	// UnityEngine.CanvasGroup Utage.AdvGraphicObjectAvatar::<Group>k__BackingField
	CanvasGroup_t3296560743 * ___U3CGroupU3Ek__BackingField_8;

public:
	inline static int32_t get_offset_of_U3CFadeTimerU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(AdvGraphicObjectAvatar_t3592307263, ___U3CFadeTimerU3Ek__BackingField_3)); }
	inline Timer_t2904185433 * get_U3CFadeTimerU3Ek__BackingField_3() const { return ___U3CFadeTimerU3Ek__BackingField_3; }
	inline Timer_t2904185433 ** get_address_of_U3CFadeTimerU3Ek__BackingField_3() { return &___U3CFadeTimerU3Ek__BackingField_3; }
	inline void set_U3CFadeTimerU3Ek__BackingField_3(Timer_t2904185433 * value)
	{
		___U3CFadeTimerU3Ek__BackingField_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CFadeTimerU3Ek__BackingField_3), value);
	}

	inline static int32_t get_offset_of_U3CAvatarU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(AdvGraphicObjectAvatar_t3592307263, ___U3CAvatarU3Ek__BackingField_4)); }
	inline AvatarImage_t1946614104 * get_U3CAvatarU3Ek__BackingField_4() const { return ___U3CAvatarU3Ek__BackingField_4; }
	inline AvatarImage_t1946614104 ** get_address_of_U3CAvatarU3Ek__BackingField_4() { return &___U3CAvatarU3Ek__BackingField_4; }
	inline void set_U3CAvatarU3Ek__BackingField_4(AvatarImage_t1946614104 * value)
	{
		___U3CAvatarU3Ek__BackingField_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CAvatarU3Ek__BackingField_4), value);
	}

	inline static int32_t get_offset_of_U3CEyeBlinkU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(AdvGraphicObjectAvatar_t3592307263, ___U3CEyeBlinkU3Ek__BackingField_5)); }
	inline EyeBlinkAvatar_t3311504802 * get_U3CEyeBlinkU3Ek__BackingField_5() const { return ___U3CEyeBlinkU3Ek__BackingField_5; }
	inline EyeBlinkAvatar_t3311504802 ** get_address_of_U3CEyeBlinkU3Ek__BackingField_5() { return &___U3CEyeBlinkU3Ek__BackingField_5; }
	inline void set_U3CEyeBlinkU3Ek__BackingField_5(EyeBlinkAvatar_t3311504802 * value)
	{
		___U3CEyeBlinkU3Ek__BackingField_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3CEyeBlinkU3Ek__BackingField_5), value);
	}

	inline static int32_t get_offset_of_U3CLipSynchU3Ek__BackingField_6() { return static_cast<int32_t>(offsetof(AdvGraphicObjectAvatar_t3592307263, ___U3CLipSynchU3Ek__BackingField_6)); }
	inline LipSynchAvatar_t872933325 * get_U3CLipSynchU3Ek__BackingField_6() const { return ___U3CLipSynchU3Ek__BackingField_6; }
	inline LipSynchAvatar_t872933325 ** get_address_of_U3CLipSynchU3Ek__BackingField_6() { return &___U3CLipSynchU3Ek__BackingField_6; }
	inline void set_U3CLipSynchU3Ek__BackingField_6(LipSynchAvatar_t872933325 * value)
	{
		___U3CLipSynchU3Ek__BackingField_6 = value;
		Il2CppCodeGenWriteBarrier((&___U3CLipSynchU3Ek__BackingField_6), value);
	}

	inline static int32_t get_offset_of_U3CAnimationU3Ek__BackingField_7() { return static_cast<int32_t>(offsetof(AdvGraphicObjectAvatar_t3592307263, ___U3CAnimationU3Ek__BackingField_7)); }
	inline AdvAnimationPlayer_t1530269518 * get_U3CAnimationU3Ek__BackingField_7() const { return ___U3CAnimationU3Ek__BackingField_7; }
	inline AdvAnimationPlayer_t1530269518 ** get_address_of_U3CAnimationU3Ek__BackingField_7() { return &___U3CAnimationU3Ek__BackingField_7; }
	inline void set_U3CAnimationU3Ek__BackingField_7(AdvAnimationPlayer_t1530269518 * value)
	{
		___U3CAnimationU3Ek__BackingField_7 = value;
		Il2CppCodeGenWriteBarrier((&___U3CAnimationU3Ek__BackingField_7), value);
	}

	inline static int32_t get_offset_of_U3CGroupU3Ek__BackingField_8() { return static_cast<int32_t>(offsetof(AdvGraphicObjectAvatar_t3592307263, ___U3CGroupU3Ek__BackingField_8)); }
	inline CanvasGroup_t3296560743 * get_U3CGroupU3Ek__BackingField_8() const { return ___U3CGroupU3Ek__BackingField_8; }
	inline CanvasGroup_t3296560743 ** get_address_of_U3CGroupU3Ek__BackingField_8() { return &___U3CGroupU3Ek__BackingField_8; }
	inline void set_U3CGroupU3Ek__BackingField_8(CanvasGroup_t3296560743 * value)
	{
		___U3CGroupU3Ek__BackingField_8 = value;
		Il2CppCodeGenWriteBarrier((&___U3CGroupU3Ek__BackingField_8), value);
	}
};

struct AdvGraphicObjectAvatar_t3592307263_StaticFields
{
public:
	// System.Action Utage.AdvGraphicObjectAvatar::<>f__am$cache0
	Action_t3226471752 * ___U3CU3Ef__amU24cache0_9;

public:
	inline static int32_t get_offset_of_U3CU3Ef__amU24cache0_9() { return static_cast<int32_t>(offsetof(AdvGraphicObjectAvatar_t3592307263_StaticFields, ___U3CU3Ef__amU24cache0_9)); }
	inline Action_t3226471752 * get_U3CU3Ef__amU24cache0_9() const { return ___U3CU3Ef__amU24cache0_9; }
	inline Action_t3226471752 ** get_address_of_U3CU3Ef__amU24cache0_9() { return &___U3CU3Ef__amU24cache0_9; }
	inline void set_U3CU3Ef__amU24cache0_9(Action_t3226471752 * value)
	{
		___U3CU3Ef__amU24cache0_9 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache0_9), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ADVGRAPHICOBJECTAVATAR_T3592307263_H
#ifndef ADVGRAPHICOBJECTRENDERTEXTUREIMAGE_T750627570_H
#define ADVGRAPHICOBJECTRENDERTEXTUREIMAGE_T750627570_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.AdvGraphicObjectRenderTextureImage
struct  AdvGraphicObjectRenderTextureImage_t750627570  : public AdvGraphicObjectUguiBase_t2728041661
{
public:
	// Utage.AdvRenderTextureSpace Utage.AdvGraphicObjectRenderTextureImage::<RenderTextureSpace>k__BackingField
	AdvRenderTextureSpace_t3482187212 * ___U3CRenderTextureSpaceU3Ek__BackingField_3;
	// UnityEngine.RenderTexture Utage.AdvGraphicObjectRenderTextureImage::copyTemporary
	RenderTexture_t2666733923 * ___copyTemporary_4;
	// UnityEngine.UI.RawImage Utage.AdvGraphicObjectRenderTextureImage::<RawImage>k__BackingField
	RawImage_t2749640213 * ___U3CRawImageU3Ek__BackingField_5;

public:
	inline static int32_t get_offset_of_U3CRenderTextureSpaceU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(AdvGraphicObjectRenderTextureImage_t750627570, ___U3CRenderTextureSpaceU3Ek__BackingField_3)); }
	inline AdvRenderTextureSpace_t3482187212 * get_U3CRenderTextureSpaceU3Ek__BackingField_3() const { return ___U3CRenderTextureSpaceU3Ek__BackingField_3; }
	inline AdvRenderTextureSpace_t3482187212 ** get_address_of_U3CRenderTextureSpaceU3Ek__BackingField_3() { return &___U3CRenderTextureSpaceU3Ek__BackingField_3; }
	inline void set_U3CRenderTextureSpaceU3Ek__BackingField_3(AdvRenderTextureSpace_t3482187212 * value)
	{
		___U3CRenderTextureSpaceU3Ek__BackingField_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CRenderTextureSpaceU3Ek__BackingField_3), value);
	}

	inline static int32_t get_offset_of_copyTemporary_4() { return static_cast<int32_t>(offsetof(AdvGraphicObjectRenderTextureImage_t750627570, ___copyTemporary_4)); }
	inline RenderTexture_t2666733923 * get_copyTemporary_4() const { return ___copyTemporary_4; }
	inline RenderTexture_t2666733923 ** get_address_of_copyTemporary_4() { return &___copyTemporary_4; }
	inline void set_copyTemporary_4(RenderTexture_t2666733923 * value)
	{
		___copyTemporary_4 = value;
		Il2CppCodeGenWriteBarrier((&___copyTemporary_4), value);
	}

	inline static int32_t get_offset_of_U3CRawImageU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(AdvGraphicObjectRenderTextureImage_t750627570, ___U3CRawImageU3Ek__BackingField_5)); }
	inline RawImage_t2749640213 * get_U3CRawImageU3Ek__BackingField_5() const { return ___U3CRawImageU3Ek__BackingField_5; }
	inline RawImage_t2749640213 ** get_address_of_U3CRawImageU3Ek__BackingField_5() { return &___U3CRawImageU3Ek__BackingField_5; }
	inline void set_U3CRawImageU3Ek__BackingField_5(RawImage_t2749640213 * value)
	{
		___U3CRawImageU3Ek__BackingField_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3CRawImageU3Ek__BackingField_5), value);
	}
};

struct AdvGraphicObjectRenderTextureImage_t750627570_StaticFields
{
public:
	// System.Action Utage.AdvGraphicObjectRenderTextureImage::<>f__am$cache0
	Action_t3226471752 * ___U3CU3Ef__amU24cache0_6;

public:
	inline static int32_t get_offset_of_U3CU3Ef__amU24cache0_6() { return static_cast<int32_t>(offsetof(AdvGraphicObjectRenderTextureImage_t750627570_StaticFields, ___U3CU3Ef__amU24cache0_6)); }
	inline Action_t3226471752 * get_U3CU3Ef__amU24cache0_6() const { return ___U3CU3Ef__amU24cache0_6; }
	inline Action_t3226471752 ** get_address_of_U3CU3Ef__amU24cache0_6() { return &___U3CU3Ef__amU24cache0_6; }
	inline void set_U3CU3Ef__amU24cache0_6(Action_t3226471752 * value)
	{
		___U3CU3Ef__amU24cache0_6 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache0_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ADVGRAPHICOBJECTRENDERTEXTUREIMAGE_T750627570_H
#ifndef ADVGRAPHICOBJECTPARTICLE_T2096513812_H
#define ADVGRAPHICOBJECTPARTICLE_T2096513812_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.AdvGraphicObjectParticle
struct  AdvGraphicObjectParticle_t2096513812  : public AdvGraphicObjectPrefabBase_t1129217777
{
public:
	// UnityEngine.ParticleSystem[] Utage.AdvGraphicObjectParticle::<ParticleArray>k__BackingField
	ParticleSystemU5BU5D_t1490986844* ___U3CParticleArrayU3Ek__BackingField_8;

public:
	inline static int32_t get_offset_of_U3CParticleArrayU3Ek__BackingField_8() { return static_cast<int32_t>(offsetof(AdvGraphicObjectParticle_t2096513812, ___U3CParticleArrayU3Ek__BackingField_8)); }
	inline ParticleSystemU5BU5D_t1490986844* get_U3CParticleArrayU3Ek__BackingField_8() const { return ___U3CParticleArrayU3Ek__BackingField_8; }
	inline ParticleSystemU5BU5D_t1490986844** get_address_of_U3CParticleArrayU3Ek__BackingField_8() { return &___U3CParticleArrayU3Ek__BackingField_8; }
	inline void set_U3CParticleArrayU3Ek__BackingField_8(ParticleSystemU5BU5D_t1490986844* value)
	{
		___U3CParticleArrayU3Ek__BackingField_8 = value;
		Il2CppCodeGenWriteBarrier((&___U3CParticleArrayU3Ek__BackingField_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ADVGRAPHICOBJECTPARTICLE_T2096513812_H
#ifndef ADVGRAPHICOBJECTCUSTOM_T976437613_H
#define ADVGRAPHICOBJECTCUSTOM_T976437613_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.AdvGraphicObjectCustom
struct  AdvGraphicObjectCustom_t976437613  : public AdvGraphicObjectPrefabBase_t1129217777
{
public:
	// UnityEngine.SpriteRenderer Utage.AdvGraphicObjectCustom::sprite
	SpriteRenderer_t1209076198 * ___sprite_8;

public:
	inline static int32_t get_offset_of_sprite_8() { return static_cast<int32_t>(offsetof(AdvGraphicObjectCustom_t976437613, ___sprite_8)); }
	inline SpriteRenderer_t1209076198 * get_sprite_8() const { return ___sprite_8; }
	inline SpriteRenderer_t1209076198 ** get_address_of_sprite_8() { return &___sprite_8; }
	inline void set_sprite_8(SpriteRenderer_t1209076198 * value)
	{
		___sprite_8 = value;
		Il2CppCodeGenWriteBarrier((&___sprite_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ADVGRAPHICOBJECTCUSTOM_T976437613_H
#ifndef ADVGRAPHICOBJECTVIDEO_T1846607759_H
#define ADVGRAPHICOBJECTVIDEO_T1846607759_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.AdvGraphicObjectVideo
struct  AdvGraphicObjectVideo_t1846607759  : public AdvGraphicObjectUguiBase_t2728041661
{
public:
	// UnityEngine.UI.RawImage Utage.AdvGraphicObjectVideo::<RawImage>k__BackingField
	RawImage_t2749640213 * ___U3CRawImageU3Ek__BackingField_3;
	// Utage.AssetFileReference Utage.AdvGraphicObjectVideo::crossFadeReference
	AssetFileReference_t4090268667 * ___crossFadeReference_4;
	// UnityEngine.Video.VideoClip Utage.AdvGraphicObjectVideo::<VideoClip>k__BackingField
	VideoClip_t3831376279 * ___U3CVideoClipU3Ek__BackingField_5;
	// UnityEngine.Video.VideoPlayer Utage.AdvGraphicObjectVideo::<VideoPlayer>k__BackingField
	VideoPlayer_t10059812 * ___U3CVideoPlayerU3Ek__BackingField_6;
	// Utage.Timer Utage.AdvGraphicObjectVideo::<FadeTimer>k__BackingField
	Timer_t2904185433 * ___U3CFadeTimerU3Ek__BackingField_7;
	// UnityEngine.RenderTexture Utage.AdvGraphicObjectVideo::<RenderTexture>k__BackingField
	RenderTexture_t2666733923 * ___U3CRenderTextureU3Ek__BackingField_8;
	// System.Int32 Utage.AdvGraphicObjectVideo::<Width>k__BackingField
	int32_t ___U3CWidthU3Ek__BackingField_9;
	// System.Int32 Utage.AdvGraphicObjectVideo::<Height>k__BackingField
	int32_t ___U3CHeightU3Ek__BackingField_10;

public:
	inline static int32_t get_offset_of_U3CRawImageU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(AdvGraphicObjectVideo_t1846607759, ___U3CRawImageU3Ek__BackingField_3)); }
	inline RawImage_t2749640213 * get_U3CRawImageU3Ek__BackingField_3() const { return ___U3CRawImageU3Ek__BackingField_3; }
	inline RawImage_t2749640213 ** get_address_of_U3CRawImageU3Ek__BackingField_3() { return &___U3CRawImageU3Ek__BackingField_3; }
	inline void set_U3CRawImageU3Ek__BackingField_3(RawImage_t2749640213 * value)
	{
		___U3CRawImageU3Ek__BackingField_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CRawImageU3Ek__BackingField_3), value);
	}

	inline static int32_t get_offset_of_crossFadeReference_4() { return static_cast<int32_t>(offsetof(AdvGraphicObjectVideo_t1846607759, ___crossFadeReference_4)); }
	inline AssetFileReference_t4090268667 * get_crossFadeReference_4() const { return ___crossFadeReference_4; }
	inline AssetFileReference_t4090268667 ** get_address_of_crossFadeReference_4() { return &___crossFadeReference_4; }
	inline void set_crossFadeReference_4(AssetFileReference_t4090268667 * value)
	{
		___crossFadeReference_4 = value;
		Il2CppCodeGenWriteBarrier((&___crossFadeReference_4), value);
	}

	inline static int32_t get_offset_of_U3CVideoClipU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(AdvGraphicObjectVideo_t1846607759, ___U3CVideoClipU3Ek__BackingField_5)); }
	inline VideoClip_t3831376279 * get_U3CVideoClipU3Ek__BackingField_5() const { return ___U3CVideoClipU3Ek__BackingField_5; }
	inline VideoClip_t3831376279 ** get_address_of_U3CVideoClipU3Ek__BackingField_5() { return &___U3CVideoClipU3Ek__BackingField_5; }
	inline void set_U3CVideoClipU3Ek__BackingField_5(VideoClip_t3831376279 * value)
	{
		___U3CVideoClipU3Ek__BackingField_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3CVideoClipU3Ek__BackingField_5), value);
	}

	inline static int32_t get_offset_of_U3CVideoPlayerU3Ek__BackingField_6() { return static_cast<int32_t>(offsetof(AdvGraphicObjectVideo_t1846607759, ___U3CVideoPlayerU3Ek__BackingField_6)); }
	inline VideoPlayer_t10059812 * get_U3CVideoPlayerU3Ek__BackingField_6() const { return ___U3CVideoPlayerU3Ek__BackingField_6; }
	inline VideoPlayer_t10059812 ** get_address_of_U3CVideoPlayerU3Ek__BackingField_6() { return &___U3CVideoPlayerU3Ek__BackingField_6; }
	inline void set_U3CVideoPlayerU3Ek__BackingField_6(VideoPlayer_t10059812 * value)
	{
		___U3CVideoPlayerU3Ek__BackingField_6 = value;
		Il2CppCodeGenWriteBarrier((&___U3CVideoPlayerU3Ek__BackingField_6), value);
	}

	inline static int32_t get_offset_of_U3CFadeTimerU3Ek__BackingField_7() { return static_cast<int32_t>(offsetof(AdvGraphicObjectVideo_t1846607759, ___U3CFadeTimerU3Ek__BackingField_7)); }
	inline Timer_t2904185433 * get_U3CFadeTimerU3Ek__BackingField_7() const { return ___U3CFadeTimerU3Ek__BackingField_7; }
	inline Timer_t2904185433 ** get_address_of_U3CFadeTimerU3Ek__BackingField_7() { return &___U3CFadeTimerU3Ek__BackingField_7; }
	inline void set_U3CFadeTimerU3Ek__BackingField_7(Timer_t2904185433 * value)
	{
		___U3CFadeTimerU3Ek__BackingField_7 = value;
		Il2CppCodeGenWriteBarrier((&___U3CFadeTimerU3Ek__BackingField_7), value);
	}

	inline static int32_t get_offset_of_U3CRenderTextureU3Ek__BackingField_8() { return static_cast<int32_t>(offsetof(AdvGraphicObjectVideo_t1846607759, ___U3CRenderTextureU3Ek__BackingField_8)); }
	inline RenderTexture_t2666733923 * get_U3CRenderTextureU3Ek__BackingField_8() const { return ___U3CRenderTextureU3Ek__BackingField_8; }
	inline RenderTexture_t2666733923 ** get_address_of_U3CRenderTextureU3Ek__BackingField_8() { return &___U3CRenderTextureU3Ek__BackingField_8; }
	inline void set_U3CRenderTextureU3Ek__BackingField_8(RenderTexture_t2666733923 * value)
	{
		___U3CRenderTextureU3Ek__BackingField_8 = value;
		Il2CppCodeGenWriteBarrier((&___U3CRenderTextureU3Ek__BackingField_8), value);
	}

	inline static int32_t get_offset_of_U3CWidthU3Ek__BackingField_9() { return static_cast<int32_t>(offsetof(AdvGraphicObjectVideo_t1846607759, ___U3CWidthU3Ek__BackingField_9)); }
	inline int32_t get_U3CWidthU3Ek__BackingField_9() const { return ___U3CWidthU3Ek__BackingField_9; }
	inline int32_t* get_address_of_U3CWidthU3Ek__BackingField_9() { return &___U3CWidthU3Ek__BackingField_9; }
	inline void set_U3CWidthU3Ek__BackingField_9(int32_t value)
	{
		___U3CWidthU3Ek__BackingField_9 = value;
	}

	inline static int32_t get_offset_of_U3CHeightU3Ek__BackingField_10() { return static_cast<int32_t>(offsetof(AdvGraphicObjectVideo_t1846607759, ___U3CHeightU3Ek__BackingField_10)); }
	inline int32_t get_U3CHeightU3Ek__BackingField_10() const { return ___U3CHeightU3Ek__BackingField_10; }
	inline int32_t* get_address_of_U3CHeightU3Ek__BackingField_10() { return &___U3CHeightU3Ek__BackingField_10; }
	inline void set_U3CHeightU3Ek__BackingField_10(int32_t value)
	{
		___U3CHeightU3Ek__BackingField_10 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ADVGRAPHICOBJECTVIDEO_T1846607759_H
#ifndef ADVGRAPHICOBJECT2DPREFAB_T325110158_H
#define ADVGRAPHICOBJECT2DPREFAB_T325110158_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.AdvGraphicObject2DPrefab
struct  AdvGraphicObject2DPrefab_t325110158  : public AdvGraphicObjectPrefabBase_t1129217777
{
public:
	// UnityEngine.SpriteRenderer Utage.AdvGraphicObject2DPrefab::sprite
	SpriteRenderer_t1209076198 * ___sprite_8;

public:
	inline static int32_t get_offset_of_sprite_8() { return static_cast<int32_t>(offsetof(AdvGraphicObject2DPrefab_t325110158, ___sprite_8)); }
	inline SpriteRenderer_t1209076198 * get_sprite_8() const { return ___sprite_8; }
	inline SpriteRenderer_t1209076198 ** get_address_of_sprite_8() { return &___sprite_8; }
	inline void set_sprite_8(SpriteRenderer_t1209076198 * value)
	{
		___sprite_8 = value;
		Il2CppCodeGenWriteBarrier((&___sprite_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ADVGRAPHICOBJECT2DPREFAB_T325110158_H
#ifndef ADVGRAPHICOBJECT3DPREFAB_T325010799_H
#define ADVGRAPHICOBJECT3DPREFAB_T325010799_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.AdvGraphicObject3DPrefab
struct  AdvGraphicObject3DPrefab_t325010799  : public AdvGraphicObjectPrefabBase_t1129217777
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ADVGRAPHICOBJECT3DPREFAB_T325010799_H





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2400 = { sizeof (AdvLocalVideoFile_t3013566383), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2401 = { sizeof (AdvGraphicGroup_t2205048404), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2401[5] = 
{
	AdvGraphicGroup_t2205048404::get_offset_of_type_0(),
	AdvGraphicGroup_t2205048404::get_offset_of_U3CDefaultLayerU3Ek__BackingField_1(),
	AdvGraphicGroup_t2205048404::get_offset_of_manager_2(),
	AdvGraphicGroup_t2205048404::get_offset_of_layers_3(),
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2402 = { sizeof (U3CDrawCharacterU3Ec__AnonStorey0_t1934820127), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2402[2] = 
{
	U3CDrawCharacterU3Ec__AnonStorey0_t1934820127::get_offset_of_name_0(),
	U3CDrawCharacterU3Ec__AnonStorey0_t1934820127::get_offset_of_layerName_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2403 = { sizeof (U3CFindLayerU3Ec__AnonStorey1_t802882485), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2403[1] = 
{
	U3CFindLayerU3Ec__AnonStorey1_t802882485::get_offset_of_name_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2404 = { sizeof (U3CFindLayerOrDefaultU3Ec__AnonStorey2_t2419501820), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2404[1] = 
{
	U3CFindLayerOrDefaultU3Ec__AnonStorey2_t2419501820::get_offset_of_name_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2405 = { sizeof (AdvGraphicInfo_t3545565645), -1, sizeof(AdvGraphicInfo_t3545565645_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2405[35] = 
{
	0,
	0,
	0,
	0,
	0,
	AdvGraphicInfo_t3545565645_StaticFields::get_offset_of_CallbackCreateCustom_5(),
	AdvGraphicInfo_t3545565645_StaticFields::get_offset_of_CallbackExpression_6(),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	AdvGraphicInfo_t3545565645::get_offset_of_U3CDataTypeU3Ek__BackingField_16(),
	AdvGraphicInfo_t3545565645::get_offset_of_U3CIndexU3Ek__BackingField_17(),
	AdvGraphicInfo_t3545565645::get_offset_of_U3CKeyU3Ek__BackingField_18(),
	AdvGraphicInfo_t3545565645::get_offset_of_U3CFileTypeU3Ek__BackingField_19(),
	AdvGraphicInfo_t3545565645::get_offset_of_U3CRowDataU3Ek__BackingField_20(),
	AdvGraphicInfo_t3545565645::get_offset_of_U3CSettingDataU3Ek__BackingField_21(),
	AdvGraphicInfo_t3545565645::get_offset_of_U3CFileNameU3Ek__BackingField_22(),
	AdvGraphicInfo_t3545565645::get_offset_of_file_23(),
	AdvGraphicInfo_t3545565645::get_offset_of_U3CPivotU3Ek__BackingField_24(),
	AdvGraphicInfo_t3545565645::get_offset_of_U3CScaleU3Ek__BackingField_25(),
	AdvGraphicInfo_t3545565645::get_offset_of_U3CPositionU3Ek__BackingField_26(),
	AdvGraphicInfo_t3545565645::get_offset_of_U3CSubFileNameU3Ek__BackingField_27(),
	AdvGraphicInfo_t3545565645::get_offset_of_U3CAnimationDataU3Ek__BackingField_28(),
	AdvGraphicInfo_t3545565645::get_offset_of_U3CEyeBlinkDataU3Ek__BackingField_29(),
	AdvGraphicInfo_t3545565645::get_offset_of_U3CLipSynchDataU3Ek__BackingField_30(),
	AdvGraphicInfo_t3545565645::get_offset_of_U3CConditionalExpressionU3Ek__BackingField_31(),
	AdvGraphicInfo_t3545565645::get_offset_of_renderTextureSetting_32(),
	0,
	AdvGraphicInfo_t3545565645_StaticFields::get_offset_of_U3CU3Ef__switchU24map3_34(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2406 = { sizeof (CreateCustom_t2292834413), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2407 = { sizeof (AdvGraphicInfoList_t3537398639), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2407[2] = 
{
	AdvGraphicInfoList_t3537398639::get_offset_of_U3CKeyU3Ek__BackingField_0(),
	AdvGraphicInfoList_t3537398639::get_offset_of_infoList_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2408 = { sizeof (AdvGraphicLayer_t3630223822), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2408[9] = 
{
	AdvGraphicLayer_t3630223822::get_offset_of_U3CManagerU3Ek__BackingField_2(),
	AdvGraphicLayer_t3630223822::get_offset_of_U3CSettingDataU3Ek__BackingField_3(),
	AdvGraphicLayer_t3630223822::get_offset_of_U3CDefaultObjectU3Ek__BackingField_4(),
	AdvGraphicLayer_t3630223822::get_offset_of_currentGraphics_5(),
	AdvGraphicLayer_t3630223822::get_offset_of_U3CCameraU3Ek__BackingField_6(),
	AdvGraphicLayer_t3630223822::get_offset_of_U3CLetterBoxCameraU3Ek__BackingField_7(),
	AdvGraphicLayer_t3630223822::get_offset_of_U3CCanvasU3Ek__BackingField_8(),
	AdvGraphicLayer_t3630223822::get_offset_of_rootObjects_9(),
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2409 = { sizeof (U3CDrawU3Ec__AnonStorey1_t3123503767), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2409[3] = 
{
	U3CDrawU3Ec__AnonStorey1_t3123503767::get_offset_of_obj_0(),
	U3CDrawU3Ec__AnonStorey1_t3123503767::get_offset_of_arg_1(),
	U3CDrawU3Ec__AnonStorey1_t3123503767::get_offset_of_U24this_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2410 = { sizeof (U3CCoDelayOutU3Ec__Iterator0_t2098714287), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2410[5] = 
{
	U3CCoDelayOutU3Ec__Iterator0_t2098714287::get_offset_of_delay_0(),
	U3CCoDelayOutU3Ec__Iterator0_t2098714287::get_offset_of_obj_1(),
	U3CCoDelayOutU3Ec__Iterator0_t2098714287::get_offset_of_U24current_2(),
	U3CCoDelayOutU3Ec__Iterator0_t2098714287::get_offset_of_U24disposing_3(),
	U3CCoDelayOutU3Ec__Iterator0_t2098714287::get_offset_of_U24PC_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2411 = { sizeof (U3CReadU3Ec__AnonStorey2_t4147561692), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2411[2] = 
{
	U3CReadU3Ec__AnonStorey2_t4147561692::get_offset_of_graphic_0(),
	U3CReadU3Ec__AnonStorey2_t4147561692::get_offset_of_U24this_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2412 = { sizeof (AdvGraphicManager_t3661251000), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2412[10] = 
{
	AdvGraphicManager_t3661251000::get_offset_of_pixelsToUnits_2(),
	AdvGraphicManager_t3661251000::get_offset_of_sortOderToZUnits_3(),
	AdvGraphicManager_t3661251000::get_offset_of_bgSpriteName_4(),
	AdvGraphicManager_t3661251000::get_offset_of_debugAutoResetCanvasPosition_5(),
	AdvGraphicManager_t3661251000::get_offset_of_renderTextureManager_6(),
	AdvGraphicManager_t3661251000::get_offset_of_videoManager_7(),
	AdvGraphicManager_t3661251000::get_offset_of_isEventMode_8(),
	AdvGraphicManager_t3661251000::get_offset_of_Groups_9(),
	AdvGraphicManager_t3661251000::get_offset_of_engine_10(),
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2413 = { sizeof (AdvGraphicOperaitonArg_t632373120), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2413[3] = 
{
	AdvGraphicOperaitonArg_t632373120::get_offset_of_U3CFadeTimeU3Ek__BackingField_0(),
	AdvGraphicOperaitonArg_t632373120::get_offset_of_U3CCommandU3Ek__BackingField_1(),
	AdvGraphicOperaitonArg_t632373120::get_offset_of_U3CGraphicU3Ek__BackingField_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2414 = { sizeof (AdvClickEvent_t2495855495), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2414[3] = 
{
	AdvClickEvent_t2495855495::get_offset_of_advGraphic_2(),
	AdvClickEvent_t2495855495::get_offset_of_U3CRowU3Ek__BackingField_3(),
	AdvClickEvent_t2495855495::get_offset_of_U3CactionU3Ek__BackingField_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2415 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2416 = { sizeof (EventEffectColor_t1679408286), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2417 = { sizeof (AdvEffectColor_t2285992559), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2417[8] = 
{
	AdvEffectColor_t2285992559::get_offset_of_animationColor_2(),
	AdvEffectColor_t2285992559::get_offset_of_tweenColor_3(),
	AdvEffectColor_t2285992559::get_offset_of_scriptColor_4(),
	AdvEffectColor_t2285992559::get_offset_of_customColor_5(),
	AdvEffectColor_t2285992559::get_offset_of_fadeAlpha_6(),
	AdvEffectColor_t2285992559::get_offset_of_OnValueChanged_7(),
	AdvEffectColor_t2285992559::get_offset_of_lastColor_8(),
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2418 = { sizeof (AdvGraphicBase_t2725442072), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2418[1] = 
{
	AdvGraphicBase_t2725442072::get_offset_of_U3CParentObjectU3Ek__BackingField_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2419 = { sizeof (U3CRuleFadeInU3Ec__AnonStorey0_t716579423), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2419[2] = 
{
	U3CRuleFadeInU3Ec__AnonStorey0_t716579423::get_offset_of_transition_0(),
	U3CRuleFadeInU3Ec__AnonStorey0_t716579423::get_offset_of_onComplete_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2420 = { sizeof (U3CRuleFadeOutU3Ec__AnonStorey1_t4086381191), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2420[2] = 
{
	U3CRuleFadeOutU3Ec__AnonStorey1_t4086381191::get_offset_of_transition_0(),
	U3CRuleFadeOutU3Ec__AnonStorey1_t4086381191::get_offset_of_onComplete_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2421 = { sizeof (AdvGraphicLoader_t202702654), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2421[2] = 
{
	AdvGraphicLoader_t202702654::get_offset_of_OnComplete_2(),
	AdvGraphicLoader_t202702654::get_offset_of_graphic_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2422 = { sizeof (U3CCoLoadWaitU3Ec__Iterator0_t2103120723), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2422[5] = 
{
	U3CCoLoadWaitU3Ec__Iterator0_t2103120723::get_offset_of_onComplete_0(),
	U3CCoLoadWaitU3Ec__Iterator0_t2103120723::get_offset_of_U24this_1(),
	U3CCoLoadWaitU3Ec__Iterator0_t2103120723::get_offset_of_U24current_2(),
	U3CCoLoadWaitU3Ec__Iterator0_t2103120723::get_offset_of_U24disposing_3(),
	U3CCoLoadWaitU3Ec__Iterator0_t2103120723::get_offset_of_U24PC_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2423 = { sizeof (AdvGraphicObject_t3651915246), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2423[11] = 
{
	AdvGraphicObject_t3651915246::get_offset_of_loader_2(),
	AdvGraphicObject_t3651915246::get_offset_of_layer_3(),
	AdvGraphicObject_t3651915246::get_offset_of_U3CLastResourceU3Ek__BackingField_4(),
	AdvGraphicObject_t3651915246::get_offset_of_U3CTargetObjectU3Ek__BackingField_5(),
	AdvGraphicObject_t3651915246::get_offset_of_U3CRenderObjectU3Ek__BackingField_6(),
	AdvGraphicObject_t3651915246::get_offset_of_U3CRenderTextureSpaceU3Ek__BackingField_7(),
	AdvGraphicObject_t3651915246::get_offset_of_U3CFadeTimerU3Ek__BackingField_8(),
	AdvGraphicObject_t3651915246::get_offset_of_effectColor_9(),
	AdvGraphicObject_t3651915246::get_offset_of_U3CrectTransformU3Ek__BackingField_10(),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2424 = { sizeof (U3CFadeInU3Ec__AnonStorey0_t2111304937), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2424[4] = 
{
	U3CFadeInU3Ec__AnonStorey0_t2111304937::get_offset_of_begin_0(),
	U3CFadeInU3Ec__AnonStorey0_t2111304937::get_offset_of_end_1(),
	U3CFadeInU3Ec__AnonStorey0_t2111304937::get_offset_of_onComplete_2(),
	U3CFadeInU3Ec__AnonStorey0_t2111304937::get_offset_of_U24this_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2425 = { sizeof (U3CFadeOutU3Ec__AnonStorey1_t4206137005), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2425[4] = 
{
	U3CFadeOutU3Ec__AnonStorey1_t4206137005::get_offset_of_begin_0(),
	U3CFadeOutU3Ec__AnonStorey1_t4206137005::get_offset_of_end_1(),
	U3CFadeOutU3Ec__AnonStorey1_t4206137005::get_offset_of_onComplete_2(),
	U3CFadeOutU3Ec__AnonStorey1_t4206137005::get_offset_of_U24this_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2426 = { sizeof (U3CRuleFadeOutU3Ec__AnonStorey2_t205145622), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2426[2] = 
{
	U3CRuleFadeOutU3Ec__AnonStorey2_t205145622::get_offset_of_onComplete_0(),
	U3CRuleFadeOutU3Ec__AnonStorey2_t205145622::get_offset_of_U24this_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2427 = { sizeof (U3CReadU3Ec__AnonStorey3_t2030984333), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2427[3] = 
{
	U3CReadU3Ec__AnonStorey3_t2030984333::get_offset_of_graphic_0(),
	U3CReadU3Ec__AnonStorey3_t2030984333::get_offset_of_buffer_1(),
	U3CReadU3Ec__AnonStorey3_t2030984333::get_offset_of_U24this_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2428 = { sizeof (AdvGraphicObject2DPrefab_t325110158), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2428[1] = 
{
	AdvGraphicObject2DPrefab_t325110158::get_offset_of_sprite_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2429 = { sizeof (AdvGraphicObject3DPrefab_t325010799), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2430 = { sizeof (AdvGraphicObjectCustom_t976437613), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2430[1] = 
{
	AdvGraphicObjectCustom_t976437613::get_offset_of_sprite_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2431 = { sizeof (AdvGraphicObjectParticle_t2096513812), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2431[1] = 
{
	AdvGraphicObjectParticle_t2096513812::get_offset_of_U3CParticleArrayU3Ek__BackingField_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2432 = { sizeof (AdvGraphicObjectPrefabBase_t1129217777), -1, sizeof(AdvGraphicObjectPrefabBase_t1129217777_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2432[5] = 
{
	AdvGraphicObjectPrefabBase_t1129217777::get_offset_of_currentObject_3(),
	AdvGraphicObjectPrefabBase_t1129217777::get_offset_of_animator_4(),
	AdvGraphicObjectPrefabBase_t1129217777::get_offset_of_U3CAnimationStateNameU3Ek__BackingField_5(),
	0,
	AdvGraphicObjectPrefabBase_t1129217777_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2433 = { sizeof (SaveType_t1557152155)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2433[3] = 
{
	SaveType_t1557152155::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2434 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2435 = { sizeof (AdvGraphicObjectRenderTextureImage_t750627570), -1, sizeof(AdvGraphicObjectRenderTextureImage_t750627570_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2435[4] = 
{
	AdvGraphicObjectRenderTextureImage_t750627570::get_offset_of_U3CRenderTextureSpaceU3Ek__BackingField_3(),
	AdvGraphicObjectRenderTextureImage_t750627570::get_offset_of_copyTemporary_4(),
	AdvGraphicObjectRenderTextureImage_t750627570::get_offset_of_U3CRawImageU3Ek__BackingField_5(),
	AdvGraphicObjectRenderTextureImage_t750627570_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2436 = { sizeof (U3CRuleFadeInU3Ec__AnonStorey0_t2998002857), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2436[2] = 
{
	U3CRuleFadeInU3Ec__AnonStorey0_t2998002857::get_offset_of_transition_0(),
	U3CRuleFadeInU3Ec__AnonStorey0_t2998002857::get_offset_of_onComplete_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2437 = { sizeof (U3CRuleFadeOutU3Ec__AnonStorey1_t788669581), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2437[3] = 
{
	U3CRuleFadeOutU3Ec__AnonStorey1_t788669581::get_offset_of_transition_0(),
	U3CRuleFadeOutU3Ec__AnonStorey1_t788669581::get_offset_of_onComplete_1(),
	U3CRuleFadeOutU3Ec__AnonStorey1_t788669581::get_offset_of_U24this_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2438 = { sizeof (U3CTryCreateCrossFadeImageU3Ec__AnonStorey2_t559212944), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2438[2] = 
{
	U3CTryCreateCrossFadeImageU3Ec__AnonStorey2_t559212944::get_offset_of_crossFade_0(),
	U3CTryCreateCrossFadeImageU3Ec__AnonStorey2_t559212944::get_offset_of_U24this_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2439 = { sizeof (AdvGraphicRenderTextureManager_t995712561), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2439[2] = 
{
	AdvGraphicRenderTextureManager_t995712561::get_offset_of_offset_2(),
	AdvGraphicRenderTextureManager_t995712561::get_offset_of_spaceList_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2440 = { sizeof (AdvRenderTextureMode_t3600907407)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2440[5] = 
{
	AdvRenderTextureMode_t3600907407::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2441 = { sizeof (AdvRenderTextureSetting_t3039724782), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2441[3] = 
{
	AdvRenderTextureSetting_t3039724782::get_offset_of_U3CRenderTextureTypeU3Ek__BackingField_0(),
	AdvRenderTextureSetting_t3039724782::get_offset_of_U3CRenderTextureSizeU3Ek__BackingField_1(),
	AdvRenderTextureSetting_t3039724782::get_offset_of_U3CRenderTextureOffsetU3Ek__BackingField_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2442 = { sizeof (AdvRenderTextureSpace_t3482187212), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2442[6] = 
{
	AdvRenderTextureSpace_t3482187212::get_offset_of_U3CRenderTextureU3Ek__BackingField_2(),
	AdvRenderTextureSpace_t3482187212::get_offset_of_U3CRenderCameraU3Ek__BackingField_3(),
	AdvRenderTextureSpace_t3482187212::get_offset_of_U3CCanvasU3Ek__BackingField_4(),
	AdvRenderTextureSpace_t3482187212::get_offset_of_U3CCanvasScalerU3Ek__BackingField_5(),
	AdvRenderTextureSpace_t3482187212::get_offset_of_U3CRenderRootU3Ek__BackingField_6(),
	AdvRenderTextureSpace_t3482187212::get_offset_of_U3CSettingU3Ek__BackingField_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2443 = { sizeof (AdvGraphicObjectAvatar_t3592307263), -1, sizeof(AdvGraphicObjectAvatar_t3592307263_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2443[7] = 
{
	AdvGraphicObjectAvatar_t3592307263::get_offset_of_U3CFadeTimerU3Ek__BackingField_3(),
	AdvGraphicObjectAvatar_t3592307263::get_offset_of_U3CAvatarU3Ek__BackingField_4(),
	AdvGraphicObjectAvatar_t3592307263::get_offset_of_U3CEyeBlinkU3Ek__BackingField_5(),
	AdvGraphicObjectAvatar_t3592307263::get_offset_of_U3CLipSynchU3Ek__BackingField_6(),
	AdvGraphicObjectAvatar_t3592307263::get_offset_of_U3CAnimationU3Ek__BackingField_7(),
	AdvGraphicObjectAvatar_t3592307263::get_offset_of_U3CGroupU3Ek__BackingField_8(),
	AdvGraphicObjectAvatar_t3592307263_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2444 = { sizeof (AdvGraphicObjectDicing_t4264806966), -1, sizeof(AdvGraphicObjectDicing_t4264806966_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2444[6] = 
{
	AdvGraphicObjectDicing_t4264806966::get_offset_of_U3CDicingU3Ek__BackingField_3(),
	AdvGraphicObjectDicing_t4264806966::get_offset_of_U3CEyeBlinkU3Ek__BackingField_4(),
	AdvGraphicObjectDicing_t4264806966::get_offset_of_U3CLipSynchU3Ek__BackingField_5(),
	AdvGraphicObjectDicing_t4264806966::get_offset_of_U3CAnimationU3Ek__BackingField_6(),
	AdvGraphicObjectDicing_t4264806966::get_offset_of_crossFadeReference_7(),
	AdvGraphicObjectDicing_t4264806966_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2445 = { sizeof (U3CTryCreateCrossFadeImageU3Ec__AnonStorey0_t2803660910), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2445[1] = 
{
	U3CTryCreateCrossFadeImageU3Ec__AnonStorey0_t2803660910::get_offset_of_crossFade_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2446 = { sizeof (AdvGraphicObjectRawImage_t1741698045), -1, sizeof(AdvGraphicObjectRawImage_t1741698045_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2446[3] = 
{
	AdvGraphicObjectRawImage_t1741698045::get_offset_of_U3CRawImageU3Ek__BackingField_3(),
	AdvGraphicObjectRawImage_t1741698045::get_offset_of_crossFadeReference_4(),
	AdvGraphicObjectRawImage_t1741698045_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2447 = { sizeof (U3CStartCrossFadeImageU3Ec__AnonStorey0_t2540212446), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2447[2] = 
{
	U3CStartCrossFadeImageU3Ec__AnonStorey0_t2540212446::get_offset_of_crossFade_0(),
	U3CStartCrossFadeImageU3Ec__AnonStorey0_t2540212446::get_offset_of_U24this_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2448 = { sizeof (AdvGraphicObjectUguiBase_t2728041661), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2449 = { sizeof (AdvGraphicObjectVideo_t1846607759), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2449[8] = 
{
	AdvGraphicObjectVideo_t1846607759::get_offset_of_U3CRawImageU3Ek__BackingField_3(),
	AdvGraphicObjectVideo_t1846607759::get_offset_of_crossFadeReference_4(),
	AdvGraphicObjectVideo_t1846607759::get_offset_of_U3CVideoClipU3Ek__BackingField_5(),
	AdvGraphicObjectVideo_t1846607759::get_offset_of_U3CVideoPlayerU3Ek__BackingField_6(),
	AdvGraphicObjectVideo_t1846607759::get_offset_of_U3CFadeTimerU3Ek__BackingField_7(),
	AdvGraphicObjectVideo_t1846607759::get_offset_of_U3CRenderTextureU3Ek__BackingField_8(),
	AdvGraphicObjectVideo_t1846607759::get_offset_of_U3CWidthU3Ek__BackingField_9(),
	AdvGraphicObjectVideo_t1846607759::get_offset_of_U3CHeightU3Ek__BackingField_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2450 = { sizeof (AdvTransitionArgs_t2412396169), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2450[3] = 
{
	AdvTransitionArgs_t2412396169::get_offset_of_U3CTextureNameU3Ek__BackingField_0(),
	AdvTransitionArgs_t2412396169::get_offset_of_U3CVagueU3Ek__BackingField_1(),
	AdvTransitionArgs_t2412396169::get_offset_of_U3CTimeU3Ek__BackingField_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2451 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2452 = { sizeof (AdvVideoManager_t946313091), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2452[2] = 
{
	AdvVideoManager_t946313091::get_offset_of_engine_2(),
	AdvVideoManager_t946313091::get_offset_of_videos_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2453 = { sizeof (VideoInfo_t2935460087), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2453[4] = 
{
	VideoInfo_t2935460087::get_offset_of_U3CCancelU3Ek__BackingField_0(),
	VideoInfo_t2935460087::get_offset_of_U3CStartedU3Ek__BackingField_1(),
	VideoInfo_t2935460087::get_offset_of_U3CCanceledU3Ek__BackingField_2(),
	VideoInfo_t2935460087::get_offset_of_U3CPlayerU3Ek__BackingField_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2454 = { sizeof (U3CPlayU3Ec__AnonStorey0_t1875904553), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2454[2] = 
{
	U3CPlayU3Ec__AnonStorey0_t1875904553::get_offset_of_info_0(),
	U3CPlayU3Ec__AnonStorey0_t1875904553::get_offset_of_U24this_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2455 = { sizeof (AdvErrorMsg_t1899702012)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2455[19] = 
{
	AdvErrorMsg_t1899702012::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2456 = { sizeof (LanguageAdvErrorMsg_t3894398370), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2456[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2457 = { sizeof (AdvConfig_t2941837115), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2457[13] = 
{
	AdvConfig_t2941837115::get_offset_of_dontUseSystemSaveData_2(),
	AdvConfig_t2941837115::get_offset_of_dontSaveFullScreen_3(),
	AdvConfig_t2941837115::get_offset_of_sendCharWaitSecMax_4(),
	AdvConfig_t2941837115::get_offset_of_autoPageWaitSecMax_5(),
	AdvConfig_t2941837115::get_offset_of_autoPageWaitSecMin_6(),
	AdvConfig_t2941837115::get_offset_of_bgmVolumeFilterOfPlayingVoice_7(),
	AdvConfig_t2941837115::get_offset_of_forceSkipInputCtl_8(),
	AdvConfig_t2941837115::get_offset_of_useMessageSpeedRead_9(),
	AdvConfig_t2941837115::get_offset_of_skipSpeed_10(),
	AdvConfig_t2941837115::get_offset_of_skipVoiceAndSe_11(),
	AdvConfig_t2941837115::get_offset_of_defaultData_12(),
	AdvConfig_t2941837115::get_offset_of_current_13(),
	AdvConfig_t2941837115::get_offset_of_isSkip_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2458 = { sizeof (AdvPageEvent_t790047568), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2459 = { sizeof (AdvPage_t2715018132), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2459[26] = 
{
	AdvPage_t2715018132::get_offset_of_onBeginPage_2(),
	AdvPage_t2715018132::get_offset_of_onBeginText_3(),
	AdvPage_t2715018132::get_offset_of_onChangeText_4(),
	AdvPage_t2715018132::get_offset_of_onUpdateSendChar_5(),
	AdvPage_t2715018132::get_offset_of_onEndText_6(),
	AdvPage_t2715018132::get_offset_of_onEndPage_7(),
	AdvPage_t2715018132::get_offset_of_onChangeStatus_8(),
	AdvPage_t2715018132::get_offset_of_onTrigWaitInputInPage_9(),
	AdvPage_t2715018132::get_offset_of_onTrigWaitInputBrPage_10(),
	AdvPage_t2715018132::get_offset_of_onTrigInput_11(),
	AdvPage_t2715018132::get_offset_of_U3CCurrentDataU3Ek__BackingField_12(),
	AdvPage_t2715018132::get_offset_of_U3CTextDataU3Ek__BackingField_13(),
	AdvPage_t2715018132::get_offset_of_U3CCurrentTextDataInPageU3Ek__BackingField_14(),
	AdvPage_t2715018132::get_offset_of_textDataList_15(),
	AdvPage_t2715018132::get_offset_of_U3CCharacterInfoU3Ek__BackingField_16(),
	AdvPage_t2715018132::get_offset_of_U3CSaveDataTitleU3Ek__BackingField_17(),
	AdvPage_t2715018132::get_offset_of_U3CCurrentTextLengthU3Ek__BackingField_18(),
	AdvPage_t2715018132::get_offset_of_U3CCurrentTextLengthMaxU3Ek__BackingField_19(),
	AdvPage_t2715018132::get_offset_of_status_20(),
	AdvPage_t2715018132::get_offset_of_U3CIsWaitingInputCommandU3Ek__BackingField_21(),
	AdvPage_t2715018132::get_offset_of_contoller_22(),
	AdvPage_t2715018132::get_offset_of_engine_23(),
	AdvPage_t2715018132::get_offset_of_isInputSendMessage_24(),
	AdvPage_t2715018132::get_offset_of_U3CLastInputSendMessageU3Ek__BackingField_25(),
	AdvPage_t2715018132::get_offset_of_deltaTimeSendMessage_26(),
	AdvPage_t2715018132::get_offset_of_waitingTimeInput_27(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2460 = { sizeof (PageStatus_t47322912)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2460[8] = 
{
	PageStatus_t47322912::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2461 = { sizeof (AdvPageControllerType_t2095572932)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2461[8] = 
{
	AdvPageControllerType_t2095572932::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2462 = { sizeof (AdvPageController_t1125789268), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2462[3] = 
{
	AdvPageController_t1125789268::get_offset_of_U3CIsKeepTextU3Ek__BackingField_0(),
	AdvPageController_t1125789268::get_offset_of_U3CIsWaitInputU3Ek__BackingField_1(),
	AdvPageController_t1125789268::get_offset_of_U3CIsBrU3Ek__BackingField_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2463 = { sizeof (AdvBacklog_t3927301290), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2463[2] = 
{
	AdvBacklog_t3927301290::get_offset_of_dataList_0(),
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2464 = { sizeof (AdvBacklogDataInPage_t474952489), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2464[4] = 
{
	AdvBacklogDataInPage_t474952489::get_offset_of_U3CLogTextU3Ek__BackingField_0(),
	AdvBacklogDataInPage_t474952489::get_offset_of_U3CCharacterLabelU3Ek__BackingField_1(),
	AdvBacklogDataInPage_t474952489::get_offset_of_U3CCharacterNameTextU3Ek__BackingField_2(),
	AdvBacklogDataInPage_t474952489::get_offset_of_U3CVoiceFileNameU3Ek__BackingField_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2465 = { sizeof (BacklogEvent_t3497267401), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2466 = { sizeof (AdvBacklogManager_t3496936997), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2466[6] = 
{
	AdvBacklogManager_t3496936997::get_offset_of_maxLog_2(),
	AdvBacklogManager_t3496936997::get_offset_of_ignoreLog_3(),
	AdvBacklogManager_t3496936997::get_offset_of_onAddPage_4(),
	AdvBacklogManager_t3496936997::get_offset_of_onAddData_5(),
	AdvBacklogManager_t3496936997::get_offset_of_backlogs_6(),
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2467 = { sizeof (AdvMessageWindow_t3415422374), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2467[6] = 
{
	AdvMessageWindow_t3415422374::get_offset_of_U3CNameU3Ek__BackingField_0(),
	AdvMessageWindow_t3415422374::get_offset_of_U3CTextU3Ek__BackingField_1(),
	AdvMessageWindow_t3415422374::get_offset_of_U3CNameTextU3Ek__BackingField_2(),
	AdvMessageWindow_t3415422374::get_offset_of_U3CCharacterLabelU3Ek__BackingField_3(),
	AdvMessageWindow_t3415422374::get_offset_of_U3CTextLengthU3Ek__BackingField_4(),
	AdvMessageWindow_t3415422374::get_offset_of_U3CMessageWindowU3Ek__BackingField_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2468 = { sizeof (MessageWindowEvent_t3103993945), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2469 = { sizeof (AdvMessageWindowManager_t1170996005), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2469[12] = 
{
	AdvMessageWindowManager_t1170996005::get_offset_of_onReset_2(),
	AdvMessageWindowManager_t1170996005::get_offset_of_onChangeActiveWindows_3(),
	AdvMessageWindowManager_t1170996005::get_offset_of_onChangeCurrentWindow_4(),
	AdvMessageWindowManager_t1170996005::get_offset_of_onTextChange_5(),
	AdvMessageWindowManager_t1170996005::get_offset_of_engine_6(),
	AdvMessageWindowManager_t1170996005::get_offset_of_isInit_7(),
	AdvMessageWindowManager_t1170996005::get_offset_of_allWindows_8(),
	AdvMessageWindowManager_t1170996005::get_offset_of_defaultActiveWindowNameList_9(),
	AdvMessageWindowManager_t1170996005::get_offset_of_activeWindows_10(),
	AdvMessageWindowManager_t1170996005::get_offset_of_U3CCurrentWindowU3Ek__BackingField_11(),
	AdvMessageWindowManager_t1170996005::get_offset_of_U3CLastWindowU3Ek__BackingField_12(),
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2470 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2471 = { sizeof (AdvSelection_t2301351953), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2471[13] = 
{
	AdvSelection_t2301351953::get_offset_of_U3CLabelU3Ek__BackingField_0(),
	AdvSelection_t2301351953::get_offset_of_text_1(),
	AdvSelection_t2301351953::get_offset_of_jumpLabel_2(),
	AdvSelection_t2301351953::get_offset_of_exp_3(),
	AdvSelection_t2301351953::get_offset_of_U3CPrefabNameU3Ek__BackingField_4(),
	AdvSelection_t2301351953::get_offset_of_U3CXU3Ek__BackingField_5(),
	AdvSelection_t2301351953::get_offset_of_U3CYU3Ek__BackingField_6(),
	AdvSelection_t2301351953::get_offset_of_spriteName_7(),
	AdvSelection_t2301351953::get_offset_of_isPolygon_8(),
	AdvSelection_t2301351953::get_offset_of_row_9(),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2472 = { sizeof (SelectionEvent_t3989025344), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2473 = { sizeof (AdvSelectionManager_t3429904078), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2473[13] = 
{
	AdvSelectionManager_t3429904078::get_offset_of_engine_2(),
	AdvSelectionManager_t3429904078::get_offset_of_selections_3(),
	AdvSelectionManager_t3429904078::get_offset_of_spriteSelections_4(),
	AdvSelectionManager_t3429904078::get_offset_of_U3CIsShowingU3Ek__BackingField_5(),
	AdvSelectionManager_t3429904078::get_offset_of_onClear_6(),
	AdvSelectionManager_t3429904078::get_offset_of_onBeginShow_7(),
	AdvSelectionManager_t3429904078::get_offset_of_onBeginWaitInput_8(),
	AdvSelectionManager_t3429904078::get_offset_of_onUpdateWaitInput_9(),
	AdvSelectionManager_t3429904078::get_offset_of_onSelected_10(),
	AdvSelectionManager_t3429904078::get_offset_of_selected_11(),
	AdvSelectionManager_t3429904078::get_offset_of_U3CIsWaitInputU3Ek__BackingField_12(),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2474 = { sizeof (U3CAddSpriteClickEventU3Ec__AnonStorey0_t689962178), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2474[2] = 
{
	U3CAddSpriteClickEventU3Ec__AnonStorey0_t689962178::get_offset_of_select_0(),
	U3CAddSpriteClickEventU3Ec__AnonStorey0_t689962178::get_offset_of_U24this_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2475 = { sizeof (VoiceStopType_t2348785448)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2475[3] = 
{
	VoiceStopType_t2348785448::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2476 = { sizeof (AdvConfigSaveData_t4190953152), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2476[20] = 
{
	AdvConfigSaveData_t4190953152::get_offset_of_isFullScreen_0(),
	AdvConfigSaveData_t4190953152::get_offset_of_isMouseWheelSendMessage_1(),
	AdvConfigSaveData_t4190953152::get_offset_of_isEffect_2(),
	AdvConfigSaveData_t4190953152::get_offset_of_isSkipUnread_3(),
	AdvConfigSaveData_t4190953152::get_offset_of_isStopSkipInSelection_4(),
	AdvConfigSaveData_t4190953152::get_offset_of_messageSpeed_5(),
	AdvConfigSaveData_t4190953152::get_offset_of_autoBrPageSpeed_6(),
	AdvConfigSaveData_t4190953152::get_offset_of_messageWindowTransparency_7(),
	AdvConfigSaveData_t4190953152::get_offset_of_soundMasterVolume_8(),
	AdvConfigSaveData_t4190953152::get_offset_of_bgmVolume_9(),
	AdvConfigSaveData_t4190953152::get_offset_of_seVolume_10(),
	AdvConfigSaveData_t4190953152::get_offset_of_ambienceVolume_11(),
	AdvConfigSaveData_t4190953152::get_offset_of_voiceVolume_12(),
	AdvConfigSaveData_t4190953152::get_offset_of_voiceStopType_13(),
	AdvConfigSaveData_t4190953152::get_offset_of_isAutoBrPage_14(),
	AdvConfigSaveData_t4190953152::get_offset_of_messageSpeedRead_15(),
	AdvConfigSaveData_t4190953152::get_offset_of_hideMessageWindowOnPlayingVoice_16(),
	AdvConfigSaveData_t4190953152::get_offset_of_taggedMasterVolumeList_17(),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2477 = { sizeof (TaggedMasterVolume_t1355152757), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2477[2] = 
{
	TaggedMasterVolume_t1355152757::get_offset_of_tag_0(),
	TaggedMasterVolume_t1355152757::get_offset_of_volume_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2478 = { sizeof (U3CSetTaggedMasterVolumeU3Ec__AnonStorey0_t876186946), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2478[1] = 
{
	U3CSetTaggedMasterVolumeU3Ec__AnonStorey0_t876186946::get_offset_of_tag_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2479 = { sizeof (U3CTryGetTaggedMasterVolumeU3Ec__AnonStorey1_t3670310314), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2479[1] = 
{
	U3CTryGetTaggedMasterVolumeU3Ec__AnonStorey1_t3670310314::get_offset_of_tag_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2480 = { sizeof (AdvGallerySaveData_t1012686890), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2480[3] = 
{
	AdvGallerySaveData_t1012686890::get_offset_of_eventSceneLabels_0(),
	AdvGallerySaveData_t1012686890::get_offset_of_eventCGLabels_1(),
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2481 = { sizeof (AdvReadHistorySaveData_t361293356), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2481[2] = 
{
	AdvReadHistorySaveData_t361293356::get_offset_of_data_0(),
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2482 = { sizeof (AdvSaveData_t4059487466), -1, sizeof(AdvSaveData_t4059487466_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2482[9] = 
{
	AdvSaveData_t4059487466::get_offset_of_U3CPathU3Ek__BackingField_0(),
	AdvSaveData_t4059487466::get_offset_of_U3CTypeU3Ek__BackingField_1(),
	AdvSaveData_t4059487466::get_offset_of_U3CTitleU3Ek__BackingField_2(),
	AdvSaveData_t4059487466::get_offset_of_texture_3(),
	AdvSaveData_t4059487466::get_offset_of_U3CDateU3Ek__BackingField_4(),
	AdvSaveData_t4059487466::get_offset_of_U3CFileVersionU3Ek__BackingField_5(),
	AdvSaveData_t4059487466_StaticFields::get_offset_of_MagicID_6(),
	0,
	AdvSaveData_t4059487466::get_offset_of_Buffer_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2483 = { sizeof (SaveDataType_t3353763446)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2483[4] = 
{
	SaveDataType_t3353763446::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2484 = { sizeof (AdvSaveManager_t2382020483), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2484[14] = 
{
	AdvSaveManager_t2382020483::get_offset_of_fileIOManager_2(),
	AdvSaveManager_t2382020483::get_offset_of_type_3(),
	AdvSaveManager_t2382020483::get_offset_of_isAutoSave_4(),
	AdvSaveManager_t2382020483::get_offset_of_directoryName_5(),
	AdvSaveManager_t2382020483::get_offset_of_fileName_6(),
	AdvSaveManager_t2382020483::get_offset_of_defaultSetting_7(),
	AdvSaveManager_t2382020483::get_offset_of_webPlayerSetting_8(),
	AdvSaveManager_t2382020483::get_offset_of_CustomSaveDataObjects_9(),
	AdvSaveManager_t2382020483::get_offset_of_saveIoList_10(),
	AdvSaveManager_t2382020483::get_offset_of_autoSaveData_11(),
	AdvSaveManager_t2382020483::get_offset_of_currentAutoSaveData_12(),
	AdvSaveManager_t2382020483::get_offset_of_quickSaveData_13(),
	AdvSaveManager_t2382020483::get_offset_of_saveDataList_14(),
	AdvSaveManager_t2382020483::get_offset_of_captureTexture_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2485 = { sizeof (SaveType_t1450193093)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2485[4] = 
{
	SaveType_t1450193093::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2486 = { sizeof (SaveSetting_t424301263), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2486[3] = 
{
	SaveSetting_t424301263::get_offset_of_captureWidth_0(),
	SaveSetting_t424301263::get_offset_of_captureHeight_1(),
	SaveSetting_t424301263::get_offset_of_saveMax_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2487 = { sizeof (AdvSelectedHistorySaveData_t4269445723), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2487[3] = 
{
	AdvSelectedHistorySaveData_t4269445723::get_offset_of_dataList_0(),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2488 = { sizeof (AdvSelectedHistoryData_t2288062212), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2488[3] = 
{
	AdvSelectedHistoryData_t2288062212::get_offset_of_U3CLabelU3Ek__BackingField_0(),
	AdvSelectedHistoryData_t2288062212::get_offset_of_U3CTextU3Ek__BackingField_1(),
	AdvSelectedHistoryData_t2288062212::get_offset_of_U3CJumpLabelU3Ek__BackingField_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2489 = { sizeof (U3CCheckU3Ec__AnonStorey0_t2585708515), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2489[1] = 
{
	U3CCheckU3Ec__AnonStorey0_t2585708515::get_offset_of_selection_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2490 = { sizeof (AdvSystemSaveData_t276732409), -1, sizeof(AdvSystemSaveData_t276732409_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2490[13] = 
{
	AdvSystemSaveData_t276732409::get_offset_of_dontUseSystemSaveData_2(),
	AdvSystemSaveData_t276732409::get_offset_of_isAutoSaveOnQuit_3(),
	AdvSystemSaveData_t276732409::get_offset_of_fileIOManager_4(),
	AdvSystemSaveData_t276732409::get_offset_of_directoryName_5(),
	AdvSystemSaveData_t276732409::get_offset_of_fileName_6(),
	AdvSystemSaveData_t276732409::get_offset_of_U3CPathU3Ek__BackingField_7(),
	AdvSystemSaveData_t276732409::get_offset_of_readData_8(),
	AdvSystemSaveData_t276732409::get_offset_of_selectionData_9(),
	AdvSystemSaveData_t276732409::get_offset_of_galleryData_10(),
	AdvSystemSaveData_t276732409::get_offset_of_engine_11(),
	AdvSystemSaveData_t276732409::get_offset_of_isInit_12(),
	AdvSystemSaveData_t276732409_StaticFields::get_offset_of_MagicID_13(),
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2491 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2492 = { sizeof (AdvEntityData_t1465354496), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2492[1] = 
{
	AdvEntityData_t1465354496::get_offset_of_originalStrings_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2493 = { sizeof (AdvIfData_t1807102644), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2493[3] = 
{
	AdvIfData_t1807102644::get_offset_of_parent_0(),
	AdvIfData_t1807102644::get_offset_of_isSkpping_1(),
	AdvIfData_t1807102644::get_offset_of_isIf_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2494 = { sizeof (AdvIfManager_t1468978853), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2494[2] = 
{
	AdvIfManager_t1468978853::get_offset_of_isLoadInit_0(),
	AdvIfManager_t1468978853::get_offset_of_current_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2495 = { sizeof (SubRoutineInfo_t2697101720), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2495[8] = 
{
	SubRoutineInfo_t2697101720::get_offset_of_reader_0(),
	SubRoutineInfo_t2697101720::get_offset_of_U3CReturnLabelU3Ek__BackingField_1(),
	SubRoutineInfo_t2697101720::get_offset_of_U3CReturnPageNoU3Ek__BackingField_2(),
	SubRoutineInfo_t2697101720::get_offset_of_U3CReturnCommandU3Ek__BackingField_3(),
	SubRoutineInfo_t2697101720::get_offset_of_U3CJumpLabelU3Ek__BackingField_4(),
	SubRoutineInfo_t2697101720::get_offset_of_U3CCalledLabelU3Ek__BackingField_5(),
	SubRoutineInfo_t2697101720::get_offset_of_U3CCalledSubroutineCommandIndexU3Ek__BackingField_6(),
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2496 = { sizeof (AdvJumpManager_t2493327610), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2496[5] = 
{
	AdvJumpManager_t2493327610::get_offset_of_U3CLabelU3Ek__BackingField_0(),
	AdvJumpManager_t2493327610::get_offset_of_U3CSubRoutineReturnInfoU3Ek__BackingField_1(),
	AdvJumpManager_t2493327610::get_offset_of_subRoutineCallStack_2(),
	AdvJumpManager_t2493327610::get_offset_of_randomInfoList_3(),
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2497 = { sizeof (RandomInfo_t3775909604), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2497[2] = 
{
	RandomInfo_t3775909604::get_offset_of_command_0(),
	RandomInfo_t3775909604::get_offset_of_rate_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2498 = { sizeof (U3CGetRandomJumpCommandU3Ec__AnonStorey0_t4280582410), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2498[1] = 
{
	U3CGetRandomJumpCommandU3Ec__AnonStorey0_t4280582410::get_offset_of_sum_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2499 = { sizeof (AdvMacroData_t2548537639), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2499[3] = 
{
	AdvMacroData_t2548537639::get_offset_of_U3CNameU3Ek__BackingField_0(),
	AdvMacroData_t2548537639::get_offset_of_U3CHeaderU3Ek__BackingField_1(),
	AdvMacroData_t2548537639::get_offset_of_U3CDataListU3Ek__BackingField_2(),
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
