﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "object-internals.h"

// System.Action
struct Action_t3226471752;
// Utage.AdvEngineStarter
struct AdvEngineStarter_t330044300;
// Utage.AdvEngineStarter/<LoadAssetBundleManifestAsync>c__Iterator4/<LoadAssetBundleManifestAsync>c__AnonStorey9
struct U3CLoadAssetBundleManifestAsyncU3Ec__AnonStorey9_t2150114403;
// Utage.ImageEffectBase
struct ImageEffectBase_t432402543;
// Utage.AdvScenarioThread
struct AdvScenarioThread_t1270526825;
// Utage.AdvCommandImageEffectBase
struct AdvCommandImageEffectBase_t67184963;
// Utage.ColorFade
struct ColorFade_t3112942085;
// Utage.AdvCommandFadeBase
struct AdvCommandFadeBase_t2761005413;
// Utage.AdvCommandZoomCamera
struct AdvCommandZoomCamera_t2560452812;
// Utage.AdvCommandTween
struct AdvCommandTween_t1326796191;
// Utage.AdvCommandRuleFadeOut
struct AdvCommandRuleFadeOut_t646159788;
// Utage.AdvCommandRuleFadeIn
struct AdvCommandRuleFadeIn_t2011331537;
// Utage.AdvCommandPlayAnimatin
struct AdvCommandPlayAnimatin_t293841659;
// Utage.IImageEffectStrength
struct IImageEffectStrength_t747561678;
// Utage.AdvCommandImageEffectBase/<OnStartEffect>c__AnonStorey1
struct U3COnStartEffectU3Ec__AnonStorey1_t3218082274;
// Utage.AdvEngineStarter/<LoadAssetBundleManifestAsync>c__Iterator4
struct U3CLoadAssetBundleManifestAsyncU3Ec__Iterator4_t3283659535;
// System.String
struct String_t;
// Utage.AssetFile
struct AssetFile_t3383013256;
// Utage.AdvImportScenarios
struct AdvImportScenarios_t1226385437;
// Utage.AdvCommandParser/CreateCustomCommandFromID
struct CreateCustomCommandFromID_t3335294381;
// System.Collections.Generic.Dictionary`2<System.String,System.Int32>
struct Dictionary_2_t3986656710;
// Utage.StringGridRow
struct StringGridRow_t4193237197;
// Utage.AdvEntityData
struct AdvEntityData_t1465354496;
// System.Collections.Generic.List`1<Utage.AssetFile>
struct List_1_t2752134388;
// System.String[]
struct StringU5BU5D_t1642385972;
// System.Collections.Generic.List`1<System.String>
struct List_1_t1398341365;
// System.Char[]
struct CharU5BU5D_t1328083999;
// System.Void
struct Void_t1841601450;
// Utage.ExpressionParser
struct ExpressionParser_t665800307;
// Utage.AdvGraphicInfoList
struct AdvGraphicInfoList_t3537398639;
// Utage.AdvGraphicInfo
struct AdvGraphicInfo_t3545565645;
// Utage.AdvGraphicOperaitonArg
struct AdvGraphicOperaitonArg_t632373120;
// Utage.AdvCharacterInfo
struct AdvCharacterInfo_t1582765630;
// Utage.AdvChapterData
struct AdvChapterData_t685691324;
// Utage.Timer
struct Timer_t2904185433;
// Utage.LetterBoxCamera
struct LetterBoxCamera_t3507617684;
// Utage.AdvCommandZoomCamera/<OnStartEffect>c__AnonStorey1
struct U3COnStartEffectU3Ec__AnonStorey1_t3601202861;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// System.DelegateData
struct DelegateData_t1572802995;
// Utage.AdvScenarioPageData
struct AdvScenarioPageData_t3333166790;
// Utage.AdvSettingDataManager
struct AdvSettingDataManager_t931476416;
// Utage.AdvCommand
struct AdvCommand_t2859960984;
// System.IAsyncResult
struct IAsyncResult_t1999651008;
// System.AsyncCallback
struct AsyncCallback_t163412349;
// Utage.iTweenData
struct iTweenData_t1904063128;
// Utage.AdvTransitionArgs
struct AdvTransitionArgs_t2412396169;




#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
#ifndef U3CLOADASSETBUNDLEMANIFESTASYNCU3EC__ITERATOR4_T3283659535_H
#define U3CLOADASSETBUNDLEMANIFESTASYNCU3EC__ITERATOR4_T3283659535_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.AdvEngineStarter/<LoadAssetBundleManifestAsync>c__Iterator4
struct  U3CLoadAssetBundleManifestAsyncU3Ec__Iterator4_t3283659535  : public RuntimeObject
{
public:
	// System.Boolean Utage.AdvEngineStarter/<LoadAssetBundleManifestAsync>c__Iterator4::fromCache
	bool ___fromCache_0;
	// System.Action Utage.AdvEngineStarter/<LoadAssetBundleManifestAsync>c__Iterator4::onFailed
	Action_t3226471752 * ___onFailed_1;
	// Utage.AdvEngineStarter Utage.AdvEngineStarter/<LoadAssetBundleManifestAsync>c__Iterator4::$this
	AdvEngineStarter_t330044300 * ___U24this_2;
	// System.Object Utage.AdvEngineStarter/<LoadAssetBundleManifestAsync>c__Iterator4::$current
	RuntimeObject * ___U24current_3;
	// System.Boolean Utage.AdvEngineStarter/<LoadAssetBundleManifestAsync>c__Iterator4::$disposing
	bool ___U24disposing_4;
	// System.Int32 Utage.AdvEngineStarter/<LoadAssetBundleManifestAsync>c__Iterator4::$PC
	int32_t ___U24PC_5;
	// Utage.AdvEngineStarter/<LoadAssetBundleManifestAsync>c__Iterator4/<LoadAssetBundleManifestAsync>c__AnonStorey9 Utage.AdvEngineStarter/<LoadAssetBundleManifestAsync>c__Iterator4::$locvar0
	U3CLoadAssetBundleManifestAsyncU3Ec__AnonStorey9_t2150114403 * ___U24locvar0_6;

public:
	inline static int32_t get_offset_of_fromCache_0() { return static_cast<int32_t>(offsetof(U3CLoadAssetBundleManifestAsyncU3Ec__Iterator4_t3283659535, ___fromCache_0)); }
	inline bool get_fromCache_0() const { return ___fromCache_0; }
	inline bool* get_address_of_fromCache_0() { return &___fromCache_0; }
	inline void set_fromCache_0(bool value)
	{
		___fromCache_0 = value;
	}

	inline static int32_t get_offset_of_onFailed_1() { return static_cast<int32_t>(offsetof(U3CLoadAssetBundleManifestAsyncU3Ec__Iterator4_t3283659535, ___onFailed_1)); }
	inline Action_t3226471752 * get_onFailed_1() const { return ___onFailed_1; }
	inline Action_t3226471752 ** get_address_of_onFailed_1() { return &___onFailed_1; }
	inline void set_onFailed_1(Action_t3226471752 * value)
	{
		___onFailed_1 = value;
		Il2CppCodeGenWriteBarrier((&___onFailed_1), value);
	}

	inline static int32_t get_offset_of_U24this_2() { return static_cast<int32_t>(offsetof(U3CLoadAssetBundleManifestAsyncU3Ec__Iterator4_t3283659535, ___U24this_2)); }
	inline AdvEngineStarter_t330044300 * get_U24this_2() const { return ___U24this_2; }
	inline AdvEngineStarter_t330044300 ** get_address_of_U24this_2() { return &___U24this_2; }
	inline void set_U24this_2(AdvEngineStarter_t330044300 * value)
	{
		___U24this_2 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_2), value);
	}

	inline static int32_t get_offset_of_U24current_3() { return static_cast<int32_t>(offsetof(U3CLoadAssetBundleManifestAsyncU3Ec__Iterator4_t3283659535, ___U24current_3)); }
	inline RuntimeObject * get_U24current_3() const { return ___U24current_3; }
	inline RuntimeObject ** get_address_of_U24current_3() { return &___U24current_3; }
	inline void set_U24current_3(RuntimeObject * value)
	{
		___U24current_3 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_3), value);
	}

	inline static int32_t get_offset_of_U24disposing_4() { return static_cast<int32_t>(offsetof(U3CLoadAssetBundleManifestAsyncU3Ec__Iterator4_t3283659535, ___U24disposing_4)); }
	inline bool get_U24disposing_4() const { return ___U24disposing_4; }
	inline bool* get_address_of_U24disposing_4() { return &___U24disposing_4; }
	inline void set_U24disposing_4(bool value)
	{
		___U24disposing_4 = value;
	}

	inline static int32_t get_offset_of_U24PC_5() { return static_cast<int32_t>(offsetof(U3CLoadAssetBundleManifestAsyncU3Ec__Iterator4_t3283659535, ___U24PC_5)); }
	inline int32_t get_U24PC_5() const { return ___U24PC_5; }
	inline int32_t* get_address_of_U24PC_5() { return &___U24PC_5; }
	inline void set_U24PC_5(int32_t value)
	{
		___U24PC_5 = value;
	}

	inline static int32_t get_offset_of_U24locvar0_6() { return static_cast<int32_t>(offsetof(U3CLoadAssetBundleManifestAsyncU3Ec__Iterator4_t3283659535, ___U24locvar0_6)); }
	inline U3CLoadAssetBundleManifestAsyncU3Ec__AnonStorey9_t2150114403 * get_U24locvar0_6() const { return ___U24locvar0_6; }
	inline U3CLoadAssetBundleManifestAsyncU3Ec__AnonStorey9_t2150114403 ** get_address_of_U24locvar0_6() { return &___U24locvar0_6; }
	inline void set_U24locvar0_6(U3CLoadAssetBundleManifestAsyncU3Ec__AnonStorey9_t2150114403 * value)
	{
		___U24locvar0_6 = value;
		Il2CppCodeGenWriteBarrier((&___U24locvar0_6), value);
	}
};

struct U3CLoadAssetBundleManifestAsyncU3Ec__Iterator4_t3283659535_StaticFields
{
public:
	// System.Action Utage.AdvEngineStarter/<LoadAssetBundleManifestAsync>c__Iterator4::<>f__am$cache0
	Action_t3226471752 * ___U3CU3Ef__amU24cache0_7;
	// System.Action Utage.AdvEngineStarter/<LoadAssetBundleManifestAsync>c__Iterator4::<>f__am$cache1
	Action_t3226471752 * ___U3CU3Ef__amU24cache1_8;

public:
	inline static int32_t get_offset_of_U3CU3Ef__amU24cache0_7() { return static_cast<int32_t>(offsetof(U3CLoadAssetBundleManifestAsyncU3Ec__Iterator4_t3283659535_StaticFields, ___U3CU3Ef__amU24cache0_7)); }
	inline Action_t3226471752 * get_U3CU3Ef__amU24cache0_7() const { return ___U3CU3Ef__amU24cache0_7; }
	inline Action_t3226471752 ** get_address_of_U3CU3Ef__amU24cache0_7() { return &___U3CU3Ef__amU24cache0_7; }
	inline void set_U3CU3Ef__amU24cache0_7(Action_t3226471752 * value)
	{
		___U3CU3Ef__amU24cache0_7 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache0_7), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache1_8() { return static_cast<int32_t>(offsetof(U3CLoadAssetBundleManifestAsyncU3Ec__Iterator4_t3283659535_StaticFields, ___U3CU3Ef__amU24cache1_8)); }
	inline Action_t3226471752 * get_U3CU3Ef__amU24cache1_8() const { return ___U3CU3Ef__amU24cache1_8; }
	inline Action_t3226471752 ** get_address_of_U3CU3Ef__amU24cache1_8() { return &___U3CU3Ef__amU24cache1_8; }
	inline void set_U3CU3Ef__amU24cache1_8(Action_t3226471752 * value)
	{
		___U3CU3Ef__amU24cache1_8 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache1_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CLOADASSETBUNDLEMANIFESTASYNCU3EC__ITERATOR4_T3283659535_H
#ifndef U3CONSTARTEFFECTU3EC__ANONSTOREY1_T3218082274_H
#define U3CONSTARTEFFECTU3EC__ANONSTOREY1_T3218082274_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.AdvCommandImageEffectBase/<OnStartEffect>c__AnonStorey1
struct  U3COnStartEffectU3Ec__AnonStorey1_t3218082274  : public RuntimeObject
{
public:
	// System.Boolean Utage.AdvCommandImageEffectBase/<OnStartEffect>c__AnonStorey1::enableAnimation
	bool ___enableAnimation_0;
	// Utage.ImageEffectBase Utage.AdvCommandImageEffectBase/<OnStartEffect>c__AnonStorey1::imageEffect
	ImageEffectBase_t432402543 * ___imageEffect_1;
	// Utage.AdvScenarioThread Utage.AdvCommandImageEffectBase/<OnStartEffect>c__AnonStorey1::thread
	AdvScenarioThread_t1270526825 * ___thread_2;
	// Utage.AdvCommandImageEffectBase Utage.AdvCommandImageEffectBase/<OnStartEffect>c__AnonStorey1::$this
	AdvCommandImageEffectBase_t67184963 * ___U24this_3;

public:
	inline static int32_t get_offset_of_enableAnimation_0() { return static_cast<int32_t>(offsetof(U3COnStartEffectU3Ec__AnonStorey1_t3218082274, ___enableAnimation_0)); }
	inline bool get_enableAnimation_0() const { return ___enableAnimation_0; }
	inline bool* get_address_of_enableAnimation_0() { return &___enableAnimation_0; }
	inline void set_enableAnimation_0(bool value)
	{
		___enableAnimation_0 = value;
	}

	inline static int32_t get_offset_of_imageEffect_1() { return static_cast<int32_t>(offsetof(U3COnStartEffectU3Ec__AnonStorey1_t3218082274, ___imageEffect_1)); }
	inline ImageEffectBase_t432402543 * get_imageEffect_1() const { return ___imageEffect_1; }
	inline ImageEffectBase_t432402543 ** get_address_of_imageEffect_1() { return &___imageEffect_1; }
	inline void set_imageEffect_1(ImageEffectBase_t432402543 * value)
	{
		___imageEffect_1 = value;
		Il2CppCodeGenWriteBarrier((&___imageEffect_1), value);
	}

	inline static int32_t get_offset_of_thread_2() { return static_cast<int32_t>(offsetof(U3COnStartEffectU3Ec__AnonStorey1_t3218082274, ___thread_2)); }
	inline AdvScenarioThread_t1270526825 * get_thread_2() const { return ___thread_2; }
	inline AdvScenarioThread_t1270526825 ** get_address_of_thread_2() { return &___thread_2; }
	inline void set_thread_2(AdvScenarioThread_t1270526825 * value)
	{
		___thread_2 = value;
		Il2CppCodeGenWriteBarrier((&___thread_2), value);
	}

	inline static int32_t get_offset_of_U24this_3() { return static_cast<int32_t>(offsetof(U3COnStartEffectU3Ec__AnonStorey1_t3218082274, ___U24this_3)); }
	inline AdvCommandImageEffectBase_t67184963 * get_U24this_3() const { return ___U24this_3; }
	inline AdvCommandImageEffectBase_t67184963 ** get_address_of_U24this_3() { return &___U24this_3; }
	inline void set_U24this_3(AdvCommandImageEffectBase_t67184963 * value)
	{
		___U24this_3 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CONSTARTEFFECTU3EC__ANONSTOREY1_T3218082274_H
#ifndef U3CONSTARTEFFECTU3EC__ANONSTOREY0_T1849549393_H
#define U3CONSTARTEFFECTU3EC__ANONSTOREY0_T1849549393_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.AdvCommandFadeBase/<OnStartEffect>c__AnonStorey0
struct  U3COnStartEffectU3Ec__AnonStorey0_t1849549393  : public RuntimeObject
{
public:
	// System.Single Utage.AdvCommandFadeBase/<OnStartEffect>c__AnonStorey0::start
	float ___start_0;
	// System.Single Utage.AdvCommandFadeBase/<OnStartEffect>c__AnonStorey0::end
	float ___end_1;
	// Utage.ColorFade Utage.AdvCommandFadeBase/<OnStartEffect>c__AnonStorey0::colorFade
	ColorFade_t3112942085 * ___colorFade_2;
	// Utage.AdvScenarioThread Utage.AdvCommandFadeBase/<OnStartEffect>c__AnonStorey0::thread
	AdvScenarioThread_t1270526825 * ___thread_3;
	// Utage.ImageEffectBase Utage.AdvCommandFadeBase/<OnStartEffect>c__AnonStorey0::imageEffect
	ImageEffectBase_t432402543 * ___imageEffect_4;
	// Utage.AdvCommandFadeBase Utage.AdvCommandFadeBase/<OnStartEffect>c__AnonStorey0::$this
	AdvCommandFadeBase_t2761005413 * ___U24this_5;

public:
	inline static int32_t get_offset_of_start_0() { return static_cast<int32_t>(offsetof(U3COnStartEffectU3Ec__AnonStorey0_t1849549393, ___start_0)); }
	inline float get_start_0() const { return ___start_0; }
	inline float* get_address_of_start_0() { return &___start_0; }
	inline void set_start_0(float value)
	{
		___start_0 = value;
	}

	inline static int32_t get_offset_of_end_1() { return static_cast<int32_t>(offsetof(U3COnStartEffectU3Ec__AnonStorey0_t1849549393, ___end_1)); }
	inline float get_end_1() const { return ___end_1; }
	inline float* get_address_of_end_1() { return &___end_1; }
	inline void set_end_1(float value)
	{
		___end_1 = value;
	}

	inline static int32_t get_offset_of_colorFade_2() { return static_cast<int32_t>(offsetof(U3COnStartEffectU3Ec__AnonStorey0_t1849549393, ___colorFade_2)); }
	inline ColorFade_t3112942085 * get_colorFade_2() const { return ___colorFade_2; }
	inline ColorFade_t3112942085 ** get_address_of_colorFade_2() { return &___colorFade_2; }
	inline void set_colorFade_2(ColorFade_t3112942085 * value)
	{
		___colorFade_2 = value;
		Il2CppCodeGenWriteBarrier((&___colorFade_2), value);
	}

	inline static int32_t get_offset_of_thread_3() { return static_cast<int32_t>(offsetof(U3COnStartEffectU3Ec__AnonStorey0_t1849549393, ___thread_3)); }
	inline AdvScenarioThread_t1270526825 * get_thread_3() const { return ___thread_3; }
	inline AdvScenarioThread_t1270526825 ** get_address_of_thread_3() { return &___thread_3; }
	inline void set_thread_3(AdvScenarioThread_t1270526825 * value)
	{
		___thread_3 = value;
		Il2CppCodeGenWriteBarrier((&___thread_3), value);
	}

	inline static int32_t get_offset_of_imageEffect_4() { return static_cast<int32_t>(offsetof(U3COnStartEffectU3Ec__AnonStorey0_t1849549393, ___imageEffect_4)); }
	inline ImageEffectBase_t432402543 * get_imageEffect_4() const { return ___imageEffect_4; }
	inline ImageEffectBase_t432402543 ** get_address_of_imageEffect_4() { return &___imageEffect_4; }
	inline void set_imageEffect_4(ImageEffectBase_t432402543 * value)
	{
		___imageEffect_4 = value;
		Il2CppCodeGenWriteBarrier((&___imageEffect_4), value);
	}

	inline static int32_t get_offset_of_U24this_5() { return static_cast<int32_t>(offsetof(U3COnStartEffectU3Ec__AnonStorey0_t1849549393, ___U24this_5)); }
	inline AdvCommandFadeBase_t2761005413 * get_U24this_5() const { return ___U24this_5; }
	inline AdvCommandFadeBase_t2761005413 ** get_address_of_U24this_5() { return &___U24this_5; }
	inline void set_U24this_5(AdvCommandFadeBase_t2761005413 * value)
	{
		___U24this_5 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CONSTARTEFFECTU3EC__ANONSTOREY0_T1849549393_H
#ifndef U3CONSTARTEFFECTU3EC__ANONSTOREY1_T3601202861_H
#define U3CONSTARTEFFECTU3EC__ANONSTOREY1_T3601202861_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.AdvCommandZoomCamera/<OnStartEffect>c__AnonStorey1
struct  U3COnStartEffectU3Ec__AnonStorey1_t3601202861  : public RuntimeObject
{
public:
	// Utage.AdvScenarioThread Utage.AdvCommandZoomCamera/<OnStartEffect>c__AnonStorey1::thread
	AdvScenarioThread_t1270526825 * ___thread_0;
	// Utage.AdvCommandZoomCamera Utage.AdvCommandZoomCamera/<OnStartEffect>c__AnonStorey1::$this
	AdvCommandZoomCamera_t2560452812 * ___U24this_1;

public:
	inline static int32_t get_offset_of_thread_0() { return static_cast<int32_t>(offsetof(U3COnStartEffectU3Ec__AnonStorey1_t3601202861, ___thread_0)); }
	inline AdvScenarioThread_t1270526825 * get_thread_0() const { return ___thread_0; }
	inline AdvScenarioThread_t1270526825 ** get_address_of_thread_0() { return &___thread_0; }
	inline void set_thread_0(AdvScenarioThread_t1270526825 * value)
	{
		___thread_0 = value;
		Il2CppCodeGenWriteBarrier((&___thread_0), value);
	}

	inline static int32_t get_offset_of_U24this_1() { return static_cast<int32_t>(offsetof(U3COnStartEffectU3Ec__AnonStorey1_t3601202861, ___U24this_1)); }
	inline AdvCommandZoomCamera_t2560452812 * get_U24this_1() const { return ___U24this_1; }
	inline AdvCommandZoomCamera_t2560452812 ** get_address_of_U24this_1() { return &___U24this_1; }
	inline void set_U24this_1(AdvCommandZoomCamera_t2560452812 * value)
	{
		___U24this_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CONSTARTEFFECTU3EC__ANONSTOREY1_T3601202861_H
#ifndef U3CONSTARTEFFECTU3EC__ANONSTOREY0_T2239811007_H
#define U3CONSTARTEFFECTU3EC__ANONSTOREY0_T2239811007_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.AdvCommandTween/<OnStartEffect>c__AnonStorey0
struct  U3COnStartEffectU3Ec__AnonStorey0_t2239811007  : public RuntimeObject
{
public:
	// Utage.AdvScenarioThread Utage.AdvCommandTween/<OnStartEffect>c__AnonStorey0::thread
	AdvScenarioThread_t1270526825 * ___thread_0;
	// Utage.AdvCommandTween Utage.AdvCommandTween/<OnStartEffect>c__AnonStorey0::$this
	AdvCommandTween_t1326796191 * ___U24this_1;

public:
	inline static int32_t get_offset_of_thread_0() { return static_cast<int32_t>(offsetof(U3COnStartEffectU3Ec__AnonStorey0_t2239811007, ___thread_0)); }
	inline AdvScenarioThread_t1270526825 * get_thread_0() const { return ___thread_0; }
	inline AdvScenarioThread_t1270526825 ** get_address_of_thread_0() { return &___thread_0; }
	inline void set_thread_0(AdvScenarioThread_t1270526825 * value)
	{
		___thread_0 = value;
		Il2CppCodeGenWriteBarrier((&___thread_0), value);
	}

	inline static int32_t get_offset_of_U24this_1() { return static_cast<int32_t>(offsetof(U3COnStartEffectU3Ec__AnonStorey0_t2239811007, ___U24this_1)); }
	inline AdvCommandTween_t1326796191 * get_U24this_1() const { return ___U24this_1; }
	inline AdvCommandTween_t1326796191 ** get_address_of_U24this_1() { return &___U24this_1; }
	inline void set_U24this_1(AdvCommandTween_t1326796191 * value)
	{
		___U24this_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CONSTARTEFFECTU3EC__ANONSTOREY0_T2239811007_H
#ifndef U3CONSTARTEFFECTU3EC__ANONSTOREY0_T2081875224_H
#define U3CONSTARTEFFECTU3EC__ANONSTOREY0_T2081875224_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.AdvCommandRuleFadeOut/<OnStartEffect>c__AnonStorey0
struct  U3COnStartEffectU3Ec__AnonStorey0_t2081875224  : public RuntimeObject
{
public:
	// Utage.AdvScenarioThread Utage.AdvCommandRuleFadeOut/<OnStartEffect>c__AnonStorey0::thread
	AdvScenarioThread_t1270526825 * ___thread_0;
	// Utage.AdvCommandRuleFadeOut Utage.AdvCommandRuleFadeOut/<OnStartEffect>c__AnonStorey0::$this
	AdvCommandRuleFadeOut_t646159788 * ___U24this_1;

public:
	inline static int32_t get_offset_of_thread_0() { return static_cast<int32_t>(offsetof(U3COnStartEffectU3Ec__AnonStorey0_t2081875224, ___thread_0)); }
	inline AdvScenarioThread_t1270526825 * get_thread_0() const { return ___thread_0; }
	inline AdvScenarioThread_t1270526825 ** get_address_of_thread_0() { return &___thread_0; }
	inline void set_thread_0(AdvScenarioThread_t1270526825 * value)
	{
		___thread_0 = value;
		Il2CppCodeGenWriteBarrier((&___thread_0), value);
	}

	inline static int32_t get_offset_of_U24this_1() { return static_cast<int32_t>(offsetof(U3COnStartEffectU3Ec__AnonStorey0_t2081875224, ___U24this_1)); }
	inline AdvCommandRuleFadeOut_t646159788 * get_U24this_1() const { return ___U24this_1; }
	inline AdvCommandRuleFadeOut_t646159788 ** get_address_of_U24this_1() { return &___U24this_1; }
	inline void set_U24this_1(AdvCommandRuleFadeOut_t646159788 * value)
	{
		___U24this_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CONSTARTEFFECTU3EC__ANONSTOREY0_T2081875224_H
#ifndef U3CONSTARTEFFECTU3EC__ANONSTOREY0_T1786609133_H
#define U3CONSTARTEFFECTU3EC__ANONSTOREY0_T1786609133_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.AdvCommandRuleFadeIn/<OnStartEffect>c__AnonStorey0
struct  U3COnStartEffectU3Ec__AnonStorey0_t1786609133  : public RuntimeObject
{
public:
	// Utage.AdvScenarioThread Utage.AdvCommandRuleFadeIn/<OnStartEffect>c__AnonStorey0::thread
	AdvScenarioThread_t1270526825 * ___thread_0;
	// Utage.AdvCommandRuleFadeIn Utage.AdvCommandRuleFadeIn/<OnStartEffect>c__AnonStorey0::$this
	AdvCommandRuleFadeIn_t2011331537 * ___U24this_1;

public:
	inline static int32_t get_offset_of_thread_0() { return static_cast<int32_t>(offsetof(U3COnStartEffectU3Ec__AnonStorey0_t1786609133, ___thread_0)); }
	inline AdvScenarioThread_t1270526825 * get_thread_0() const { return ___thread_0; }
	inline AdvScenarioThread_t1270526825 ** get_address_of_thread_0() { return &___thread_0; }
	inline void set_thread_0(AdvScenarioThread_t1270526825 * value)
	{
		___thread_0 = value;
		Il2CppCodeGenWriteBarrier((&___thread_0), value);
	}

	inline static int32_t get_offset_of_U24this_1() { return static_cast<int32_t>(offsetof(U3COnStartEffectU3Ec__AnonStorey0_t1786609133, ___U24this_1)); }
	inline AdvCommandRuleFadeIn_t2011331537 * get_U24this_1() const { return ___U24this_1; }
	inline AdvCommandRuleFadeIn_t2011331537 ** get_address_of_U24this_1() { return &___U24this_1; }
	inline void set_U24this_1(AdvCommandRuleFadeIn_t2011331537 * value)
	{
		___U24this_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CONSTARTEFFECTU3EC__ANONSTOREY0_T1786609133_H
#ifndef U3CONSTARTEFFECTU3EC__ANONSTOREY0_T2144784675_H
#define U3CONSTARTEFFECTU3EC__ANONSTOREY0_T2144784675_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.AdvCommandPlayAnimatin/<OnStartEffect>c__AnonStorey0
struct  U3COnStartEffectU3Ec__AnonStorey0_t2144784675  : public RuntimeObject
{
public:
	// Utage.AdvScenarioThread Utage.AdvCommandPlayAnimatin/<OnStartEffect>c__AnonStorey0::thread
	AdvScenarioThread_t1270526825 * ___thread_0;
	// Utage.AdvCommandPlayAnimatin Utage.AdvCommandPlayAnimatin/<OnStartEffect>c__AnonStorey0::$this
	AdvCommandPlayAnimatin_t293841659 * ___U24this_1;

public:
	inline static int32_t get_offset_of_thread_0() { return static_cast<int32_t>(offsetof(U3COnStartEffectU3Ec__AnonStorey0_t2144784675, ___thread_0)); }
	inline AdvScenarioThread_t1270526825 * get_thread_0() const { return ___thread_0; }
	inline AdvScenarioThread_t1270526825 ** get_address_of_thread_0() { return &___thread_0; }
	inline void set_thread_0(AdvScenarioThread_t1270526825 * value)
	{
		___thread_0 = value;
		Il2CppCodeGenWriteBarrier((&___thread_0), value);
	}

	inline static int32_t get_offset_of_U24this_1() { return static_cast<int32_t>(offsetof(U3COnStartEffectU3Ec__AnonStorey0_t2144784675, ___U24this_1)); }
	inline AdvCommandPlayAnimatin_t293841659 * get_U24this_1() const { return ___U24this_1; }
	inline AdvCommandPlayAnimatin_t293841659 ** get_address_of_U24this_1() { return &___U24this_1; }
	inline void set_U24this_1(AdvCommandPlayAnimatin_t293841659 * value)
	{
		___U24this_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CONSTARTEFFECTU3EC__ANONSTOREY0_T2144784675_H
#ifndef U3CONSTARTEFFECTU3EC__ANONSTOREY0_T3218082275_H
#define U3CONSTARTEFFECTU3EC__ANONSTOREY0_T3218082275_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.AdvCommandImageEffectBase/<OnStartEffect>c__AnonStorey0
struct  U3COnStartEffectU3Ec__AnonStorey0_t3218082275  : public RuntimeObject
{
public:
	// System.Single Utage.AdvCommandImageEffectBase/<OnStartEffect>c__AnonStorey0::start
	float ___start_0;
	// System.Single Utage.AdvCommandImageEffectBase/<OnStartEffect>c__AnonStorey0::end
	float ___end_1;
	// Utage.IImageEffectStrength Utage.AdvCommandImageEffectBase/<OnStartEffect>c__AnonStorey0::fade
	RuntimeObject* ___fade_2;
	// Utage.AdvCommandImageEffectBase/<OnStartEffect>c__AnonStorey1 Utage.AdvCommandImageEffectBase/<OnStartEffect>c__AnonStorey0::<>f__ref$1
	U3COnStartEffectU3Ec__AnonStorey1_t3218082274 * ___U3CU3Ef__refU241_3;

public:
	inline static int32_t get_offset_of_start_0() { return static_cast<int32_t>(offsetof(U3COnStartEffectU3Ec__AnonStorey0_t3218082275, ___start_0)); }
	inline float get_start_0() const { return ___start_0; }
	inline float* get_address_of_start_0() { return &___start_0; }
	inline void set_start_0(float value)
	{
		___start_0 = value;
	}

	inline static int32_t get_offset_of_end_1() { return static_cast<int32_t>(offsetof(U3COnStartEffectU3Ec__AnonStorey0_t3218082275, ___end_1)); }
	inline float get_end_1() const { return ___end_1; }
	inline float* get_address_of_end_1() { return &___end_1; }
	inline void set_end_1(float value)
	{
		___end_1 = value;
	}

	inline static int32_t get_offset_of_fade_2() { return static_cast<int32_t>(offsetof(U3COnStartEffectU3Ec__AnonStorey0_t3218082275, ___fade_2)); }
	inline RuntimeObject* get_fade_2() const { return ___fade_2; }
	inline RuntimeObject** get_address_of_fade_2() { return &___fade_2; }
	inline void set_fade_2(RuntimeObject* value)
	{
		___fade_2 = value;
		Il2CppCodeGenWriteBarrier((&___fade_2), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__refU241_3() { return static_cast<int32_t>(offsetof(U3COnStartEffectU3Ec__AnonStorey0_t3218082275, ___U3CU3Ef__refU241_3)); }
	inline U3COnStartEffectU3Ec__AnonStorey1_t3218082274 * get_U3CU3Ef__refU241_3() const { return ___U3CU3Ef__refU241_3; }
	inline U3COnStartEffectU3Ec__AnonStorey1_t3218082274 ** get_address_of_U3CU3Ef__refU241_3() { return &___U3CU3Ef__refU241_3; }
	inline void set_U3CU3Ef__refU241_3(U3COnStartEffectU3Ec__AnonStorey1_t3218082274 * value)
	{
		___U3CU3Ef__refU241_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__refU241_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CONSTARTEFFECTU3EC__ANONSTOREY0_T3218082275_H
#ifndef VALUETYPE_T3507792607_H
#define VALUETYPE_T3507792607_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ValueType
struct  ValueType_t3507792607  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t3507792607_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t3507792607_marshaled_com
{
};
#endif // VALUETYPE_T3507792607_H
#ifndef U3CLOADASSETBUNDLEMANIFESTASYNCU3EC__ANONSTOREY9_T2150114403_H
#define U3CLOADASSETBUNDLEMANIFESTASYNCU3EC__ANONSTOREY9_T2150114403_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.AdvEngineStarter/<LoadAssetBundleManifestAsync>c__Iterator4/<LoadAssetBundleManifestAsync>c__AnonStorey9
struct  U3CLoadAssetBundleManifestAsyncU3Ec__AnonStorey9_t2150114403  : public RuntimeObject
{
public:
	// System.Action Utage.AdvEngineStarter/<LoadAssetBundleManifestAsync>c__Iterator4/<LoadAssetBundleManifestAsync>c__AnonStorey9::onFailed
	Action_t3226471752 * ___onFailed_0;
	// Utage.AdvEngineStarter/<LoadAssetBundleManifestAsync>c__Iterator4 Utage.AdvEngineStarter/<LoadAssetBundleManifestAsync>c__Iterator4/<LoadAssetBundleManifestAsync>c__AnonStorey9::<>f__ref$4
	U3CLoadAssetBundleManifestAsyncU3Ec__Iterator4_t3283659535 * ___U3CU3Ef__refU244_1;

public:
	inline static int32_t get_offset_of_onFailed_0() { return static_cast<int32_t>(offsetof(U3CLoadAssetBundleManifestAsyncU3Ec__AnonStorey9_t2150114403, ___onFailed_0)); }
	inline Action_t3226471752 * get_onFailed_0() const { return ___onFailed_0; }
	inline Action_t3226471752 ** get_address_of_onFailed_0() { return &___onFailed_0; }
	inline void set_onFailed_0(Action_t3226471752 * value)
	{
		___onFailed_0 = value;
		Il2CppCodeGenWriteBarrier((&___onFailed_0), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__refU244_1() { return static_cast<int32_t>(offsetof(U3CLoadAssetBundleManifestAsyncU3Ec__AnonStorey9_t2150114403, ___U3CU3Ef__refU244_1)); }
	inline U3CLoadAssetBundleManifestAsyncU3Ec__Iterator4_t3283659535 * get_U3CU3Ef__refU244_1() const { return ___U3CU3Ef__refU244_1; }
	inline U3CLoadAssetBundleManifestAsyncU3Ec__Iterator4_t3283659535 ** get_address_of_U3CU3Ef__refU244_1() { return &___U3CU3Ef__refU244_1; }
	inline void set_U3CU3Ef__refU244_1(U3CLoadAssetBundleManifestAsyncU3Ec__Iterator4_t3283659535 * value)
	{
		___U3CU3Ef__refU244_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__refU244_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CLOADASSETBUNDLEMANIFESTASYNCU3EC__ANONSTOREY9_T2150114403_H
#ifndef U3CLOADSCENARIOSASYNCU3EC__ITERATOR5_T2233559140_H
#define U3CLOADSCENARIOSASYNCU3EC__ITERATOR5_T2233559140_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.AdvEngineStarter/<LoadScenariosAsync>c__Iterator5
struct  U3CLoadScenariosAsyncU3Ec__Iterator5_t2233559140  : public RuntimeObject
{
public:
	// System.String Utage.AdvEngineStarter/<LoadScenariosAsync>c__Iterator5::rootDir
	String_t* ___rootDir_0;
	// System.String Utage.AdvEngineStarter/<LoadScenariosAsync>c__Iterator5::<url>__0
	String_t* ___U3CurlU3E__0_1;
	// Utage.AssetFile Utage.AdvEngineStarter/<LoadScenariosAsync>c__Iterator5::<file>__0
	RuntimeObject* ___U3CfileU3E__0_2;
	// Utage.AdvImportScenarios Utage.AdvEngineStarter/<LoadScenariosAsync>c__Iterator5::<scenarios>__0
	AdvImportScenarios_t1226385437 * ___U3CscenariosU3E__0_3;
	// Utage.AdvEngineStarter Utage.AdvEngineStarter/<LoadScenariosAsync>c__Iterator5::$this
	AdvEngineStarter_t330044300 * ___U24this_4;
	// System.Object Utage.AdvEngineStarter/<LoadScenariosAsync>c__Iterator5::$current
	RuntimeObject * ___U24current_5;
	// System.Boolean Utage.AdvEngineStarter/<LoadScenariosAsync>c__Iterator5::$disposing
	bool ___U24disposing_6;
	// System.Int32 Utage.AdvEngineStarter/<LoadScenariosAsync>c__Iterator5::$PC
	int32_t ___U24PC_7;

public:
	inline static int32_t get_offset_of_rootDir_0() { return static_cast<int32_t>(offsetof(U3CLoadScenariosAsyncU3Ec__Iterator5_t2233559140, ___rootDir_0)); }
	inline String_t* get_rootDir_0() const { return ___rootDir_0; }
	inline String_t** get_address_of_rootDir_0() { return &___rootDir_0; }
	inline void set_rootDir_0(String_t* value)
	{
		___rootDir_0 = value;
		Il2CppCodeGenWriteBarrier((&___rootDir_0), value);
	}

	inline static int32_t get_offset_of_U3CurlU3E__0_1() { return static_cast<int32_t>(offsetof(U3CLoadScenariosAsyncU3Ec__Iterator5_t2233559140, ___U3CurlU3E__0_1)); }
	inline String_t* get_U3CurlU3E__0_1() const { return ___U3CurlU3E__0_1; }
	inline String_t** get_address_of_U3CurlU3E__0_1() { return &___U3CurlU3E__0_1; }
	inline void set_U3CurlU3E__0_1(String_t* value)
	{
		___U3CurlU3E__0_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CurlU3E__0_1), value);
	}

	inline static int32_t get_offset_of_U3CfileU3E__0_2() { return static_cast<int32_t>(offsetof(U3CLoadScenariosAsyncU3Ec__Iterator5_t2233559140, ___U3CfileU3E__0_2)); }
	inline RuntimeObject* get_U3CfileU3E__0_2() const { return ___U3CfileU3E__0_2; }
	inline RuntimeObject** get_address_of_U3CfileU3E__0_2() { return &___U3CfileU3E__0_2; }
	inline void set_U3CfileU3E__0_2(RuntimeObject* value)
	{
		___U3CfileU3E__0_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CfileU3E__0_2), value);
	}

	inline static int32_t get_offset_of_U3CscenariosU3E__0_3() { return static_cast<int32_t>(offsetof(U3CLoadScenariosAsyncU3Ec__Iterator5_t2233559140, ___U3CscenariosU3E__0_3)); }
	inline AdvImportScenarios_t1226385437 * get_U3CscenariosU3E__0_3() const { return ___U3CscenariosU3E__0_3; }
	inline AdvImportScenarios_t1226385437 ** get_address_of_U3CscenariosU3E__0_3() { return &___U3CscenariosU3E__0_3; }
	inline void set_U3CscenariosU3E__0_3(AdvImportScenarios_t1226385437 * value)
	{
		___U3CscenariosU3E__0_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CscenariosU3E__0_3), value);
	}

	inline static int32_t get_offset_of_U24this_4() { return static_cast<int32_t>(offsetof(U3CLoadScenariosAsyncU3Ec__Iterator5_t2233559140, ___U24this_4)); }
	inline AdvEngineStarter_t330044300 * get_U24this_4() const { return ___U24this_4; }
	inline AdvEngineStarter_t330044300 ** get_address_of_U24this_4() { return &___U24this_4; }
	inline void set_U24this_4(AdvEngineStarter_t330044300 * value)
	{
		___U24this_4 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_4), value);
	}

	inline static int32_t get_offset_of_U24current_5() { return static_cast<int32_t>(offsetof(U3CLoadScenariosAsyncU3Ec__Iterator5_t2233559140, ___U24current_5)); }
	inline RuntimeObject * get_U24current_5() const { return ___U24current_5; }
	inline RuntimeObject ** get_address_of_U24current_5() { return &___U24current_5; }
	inline void set_U24current_5(RuntimeObject * value)
	{
		___U24current_5 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_5), value);
	}

	inline static int32_t get_offset_of_U24disposing_6() { return static_cast<int32_t>(offsetof(U3CLoadScenariosAsyncU3Ec__Iterator5_t2233559140, ___U24disposing_6)); }
	inline bool get_U24disposing_6() const { return ___U24disposing_6; }
	inline bool* get_address_of_U24disposing_6() { return &___U24disposing_6; }
	inline void set_U24disposing_6(bool value)
	{
		___U24disposing_6 = value;
	}

	inline static int32_t get_offset_of_U24PC_7() { return static_cast<int32_t>(offsetof(U3CLoadScenariosAsyncU3Ec__Iterator5_t2233559140, ___U24PC_7)); }
	inline int32_t get_U24PC_7() const { return ___U24PC_7; }
	inline int32_t* get_address_of_U24PC_7() { return &___U24PC_7; }
	inline void set_U24PC_7(int32_t value)
	{
		___U24PC_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CLOADSCENARIOSASYNCU3EC__ITERATOR5_T2233559140_H
#ifndef U3CCOPLAYENGINEU3EC__ITERATOR7_T4126203997_H
#define U3CCOPLAYENGINEU3EC__ITERATOR7_T4126203997_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.AdvEngineStarter/<CoPlayEngine>c__Iterator7
struct  U3CCoPlayEngineU3Ec__Iterator7_t4126203997  : public RuntimeObject
{
public:
	// Utage.AdvEngineStarter Utage.AdvEngineStarter/<CoPlayEngine>c__Iterator7::$this
	AdvEngineStarter_t330044300 * ___U24this_0;
	// System.Object Utage.AdvEngineStarter/<CoPlayEngine>c__Iterator7::$current
	RuntimeObject * ___U24current_1;
	// System.Boolean Utage.AdvEngineStarter/<CoPlayEngine>c__Iterator7::$disposing
	bool ___U24disposing_2;
	// System.Int32 Utage.AdvEngineStarter/<CoPlayEngine>c__Iterator7::$PC
	int32_t ___U24PC_3;

public:
	inline static int32_t get_offset_of_U24this_0() { return static_cast<int32_t>(offsetof(U3CCoPlayEngineU3Ec__Iterator7_t4126203997, ___U24this_0)); }
	inline AdvEngineStarter_t330044300 * get_U24this_0() const { return ___U24this_0; }
	inline AdvEngineStarter_t330044300 ** get_address_of_U24this_0() { return &___U24this_0; }
	inline void set_U24this_0(AdvEngineStarter_t330044300 * value)
	{
		___U24this_0 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_0), value);
	}

	inline static int32_t get_offset_of_U24current_1() { return static_cast<int32_t>(offsetof(U3CCoPlayEngineU3Ec__Iterator7_t4126203997, ___U24current_1)); }
	inline RuntimeObject * get_U24current_1() const { return ___U24current_1; }
	inline RuntimeObject ** get_address_of_U24current_1() { return &___U24current_1; }
	inline void set_U24current_1(RuntimeObject * value)
	{
		___U24current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_1), value);
	}

	inline static int32_t get_offset_of_U24disposing_2() { return static_cast<int32_t>(offsetof(U3CCoPlayEngineU3Ec__Iterator7_t4126203997, ___U24disposing_2)); }
	inline bool get_U24disposing_2() const { return ___U24disposing_2; }
	inline bool* get_address_of_U24disposing_2() { return &___U24disposing_2; }
	inline void set_U24disposing_2(bool value)
	{
		___U24disposing_2 = value;
	}

	inline static int32_t get_offset_of_U24PC_3() { return static_cast<int32_t>(offsetof(U3CCoPlayEngineU3Ec__Iterator7_t4126203997, ___U24PC_3)); }
	inline int32_t get_U24PC_3() const { return ___U24PC_3; }
	inline int32_t* get_address_of_U24PC_3() { return &___U24PC_3; }
	inline void set_U24PC_3(int32_t value)
	{
		___U24PC_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CCOPLAYENGINEU3EC__ITERATOR7_T4126203997_H
#ifndef ADVCOMMANDPARSER_T7201233_H
#define ADVCOMMANDPARSER_T7201233_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.AdvCommandParser
struct  AdvCommandParser_t7201233  : public RuntimeObject
{
public:

public:
};

struct AdvCommandParser_t7201233_StaticFields
{
public:
	// Utage.AdvCommandParser/CreateCustomCommandFromID Utage.AdvCommandParser::OnCreateCustomCommandFromID
	CreateCustomCommandFromID_t3335294381 * ___OnCreateCustomCommandFromID_0;
	// Utage.AdvCommandParser/CreateCustomCommandFromID Utage.AdvCommandParser::OnCreateCustomCommnadFromID
	CreateCustomCommandFromID_t3335294381 * ___OnCreateCustomCommnadFromID_1;
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> Utage.AdvCommandParser::<>f__switch$map0
	Dictionary_2_t3986656710 * ___U3CU3Ef__switchU24map0_83;

public:
	inline static int32_t get_offset_of_OnCreateCustomCommandFromID_0() { return static_cast<int32_t>(offsetof(AdvCommandParser_t7201233_StaticFields, ___OnCreateCustomCommandFromID_0)); }
	inline CreateCustomCommandFromID_t3335294381 * get_OnCreateCustomCommandFromID_0() const { return ___OnCreateCustomCommandFromID_0; }
	inline CreateCustomCommandFromID_t3335294381 ** get_address_of_OnCreateCustomCommandFromID_0() { return &___OnCreateCustomCommandFromID_0; }
	inline void set_OnCreateCustomCommandFromID_0(CreateCustomCommandFromID_t3335294381 * value)
	{
		___OnCreateCustomCommandFromID_0 = value;
		Il2CppCodeGenWriteBarrier((&___OnCreateCustomCommandFromID_0), value);
	}

	inline static int32_t get_offset_of_OnCreateCustomCommnadFromID_1() { return static_cast<int32_t>(offsetof(AdvCommandParser_t7201233_StaticFields, ___OnCreateCustomCommnadFromID_1)); }
	inline CreateCustomCommandFromID_t3335294381 * get_OnCreateCustomCommnadFromID_1() const { return ___OnCreateCustomCommnadFromID_1; }
	inline CreateCustomCommandFromID_t3335294381 ** get_address_of_OnCreateCustomCommnadFromID_1() { return &___OnCreateCustomCommnadFromID_1; }
	inline void set_OnCreateCustomCommnadFromID_1(CreateCustomCommandFromID_t3335294381 * value)
	{
		___OnCreateCustomCommnadFromID_1 = value;
		Il2CppCodeGenWriteBarrier((&___OnCreateCustomCommnadFromID_1), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__switchU24map0_83() { return static_cast<int32_t>(offsetof(AdvCommandParser_t7201233_StaticFields, ___U3CU3Ef__switchU24map0_83)); }
	inline Dictionary_2_t3986656710 * get_U3CU3Ef__switchU24map0_83() const { return ___U3CU3Ef__switchU24map0_83; }
	inline Dictionary_2_t3986656710 ** get_address_of_U3CU3Ef__switchU24map0_83() { return &___U3CU3Ef__switchU24map0_83; }
	inline void set_U3CU3Ef__switchU24map0_83(Dictionary_2_t3986656710 * value)
	{
		___U3CU3Ef__switchU24map0_83 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__switchU24map0_83), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ADVCOMMANDPARSER_T7201233_H
#ifndef ADVCOMMAND_T2859960984_H
#define ADVCOMMAND_T2859960984_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.AdvCommand
struct  AdvCommand_t2859960984  : public RuntimeObject
{
public:
	// Utage.StringGridRow Utage.AdvCommand::<RowData>k__BackingField
	StringGridRow_t4193237197 * ___U3CRowDataU3Ek__BackingField_2;
	// Utage.AdvEntityData Utage.AdvCommand::<EntityData>k__BackingField
	AdvEntityData_t1465354496 * ___U3CEntityDataU3Ek__BackingField_3;
	// System.String Utage.AdvCommand::<Id>k__BackingField
	String_t* ___U3CIdU3Ek__BackingField_4;
	// System.Collections.Generic.List`1<Utage.AssetFile> Utage.AdvCommand::loadFileList
	List_1_t2752134388 * ___loadFileList_5;
	// Utage.AdvScenarioThread Utage.AdvCommand::<CurrentTread>k__BackingField
	AdvScenarioThread_t1270526825 * ___U3CCurrentTreadU3Ek__BackingField_6;

public:
	inline static int32_t get_offset_of_U3CRowDataU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(AdvCommand_t2859960984, ___U3CRowDataU3Ek__BackingField_2)); }
	inline StringGridRow_t4193237197 * get_U3CRowDataU3Ek__BackingField_2() const { return ___U3CRowDataU3Ek__BackingField_2; }
	inline StringGridRow_t4193237197 ** get_address_of_U3CRowDataU3Ek__BackingField_2() { return &___U3CRowDataU3Ek__BackingField_2; }
	inline void set_U3CRowDataU3Ek__BackingField_2(StringGridRow_t4193237197 * value)
	{
		___U3CRowDataU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CRowDataU3Ek__BackingField_2), value);
	}

	inline static int32_t get_offset_of_U3CEntityDataU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(AdvCommand_t2859960984, ___U3CEntityDataU3Ek__BackingField_3)); }
	inline AdvEntityData_t1465354496 * get_U3CEntityDataU3Ek__BackingField_3() const { return ___U3CEntityDataU3Ek__BackingField_3; }
	inline AdvEntityData_t1465354496 ** get_address_of_U3CEntityDataU3Ek__BackingField_3() { return &___U3CEntityDataU3Ek__BackingField_3; }
	inline void set_U3CEntityDataU3Ek__BackingField_3(AdvEntityData_t1465354496 * value)
	{
		___U3CEntityDataU3Ek__BackingField_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CEntityDataU3Ek__BackingField_3), value);
	}

	inline static int32_t get_offset_of_U3CIdU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(AdvCommand_t2859960984, ___U3CIdU3Ek__BackingField_4)); }
	inline String_t* get_U3CIdU3Ek__BackingField_4() const { return ___U3CIdU3Ek__BackingField_4; }
	inline String_t** get_address_of_U3CIdU3Ek__BackingField_4() { return &___U3CIdU3Ek__BackingField_4; }
	inline void set_U3CIdU3Ek__BackingField_4(String_t* value)
	{
		___U3CIdU3Ek__BackingField_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CIdU3Ek__BackingField_4), value);
	}

	inline static int32_t get_offset_of_loadFileList_5() { return static_cast<int32_t>(offsetof(AdvCommand_t2859960984, ___loadFileList_5)); }
	inline List_1_t2752134388 * get_loadFileList_5() const { return ___loadFileList_5; }
	inline List_1_t2752134388 ** get_address_of_loadFileList_5() { return &___loadFileList_5; }
	inline void set_loadFileList_5(List_1_t2752134388 * value)
	{
		___loadFileList_5 = value;
		Il2CppCodeGenWriteBarrier((&___loadFileList_5), value);
	}

	inline static int32_t get_offset_of_U3CCurrentTreadU3Ek__BackingField_6() { return static_cast<int32_t>(offsetof(AdvCommand_t2859960984, ___U3CCurrentTreadU3Ek__BackingField_6)); }
	inline AdvScenarioThread_t1270526825 * get_U3CCurrentTreadU3Ek__BackingField_6() const { return ___U3CCurrentTreadU3Ek__BackingField_6; }
	inline AdvScenarioThread_t1270526825 ** get_address_of_U3CCurrentTreadU3Ek__BackingField_6() { return &___U3CCurrentTreadU3Ek__BackingField_6; }
	inline void set_U3CCurrentTreadU3Ek__BackingField_6(AdvScenarioThread_t1270526825 * value)
	{
		___U3CCurrentTreadU3Ek__BackingField_6 = value;
		Il2CppCodeGenWriteBarrier((&___U3CCurrentTreadU3Ek__BackingField_6), value);
	}
};

struct AdvCommand_t2859960984_StaticFields
{
public:
	// System.Boolean Utage.AdvCommand::isEditorErrorCheck
	bool ___isEditorErrorCheck_0;
	// System.Boolean Utage.AdvCommand::<IsEditorErrorCheckWaitType>k__BackingField
	bool ___U3CIsEditorErrorCheckWaitTypeU3Ek__BackingField_1;

public:
	inline static int32_t get_offset_of_isEditorErrorCheck_0() { return static_cast<int32_t>(offsetof(AdvCommand_t2859960984_StaticFields, ___isEditorErrorCheck_0)); }
	inline bool get_isEditorErrorCheck_0() const { return ___isEditorErrorCheck_0; }
	inline bool* get_address_of_isEditorErrorCheck_0() { return &___isEditorErrorCheck_0; }
	inline void set_isEditorErrorCheck_0(bool value)
	{
		___isEditorErrorCheck_0 = value;
	}

	inline static int32_t get_offset_of_U3CIsEditorErrorCheckWaitTypeU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(AdvCommand_t2859960984_StaticFields, ___U3CIsEditorErrorCheckWaitTypeU3Ek__BackingField_1)); }
	inline bool get_U3CIsEditorErrorCheckWaitTypeU3Ek__BackingField_1() const { return ___U3CIsEditorErrorCheckWaitTypeU3Ek__BackingField_1; }
	inline bool* get_address_of_U3CIsEditorErrorCheckWaitTypeU3Ek__BackingField_1() { return &___U3CIsEditorErrorCheckWaitTypeU3Ek__BackingField_1; }
	inline void set_U3CIsEditorErrorCheckWaitTypeU3Ek__BackingField_1(bool value)
	{
		___U3CIsEditorErrorCheckWaitTypeU3Ek__BackingField_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ADVCOMMAND_T2859960984_H
#ifndef ADVCOMMANDMESSAGEWINDOWCHANGECURRENT_T1382911780_H
#define ADVCOMMANDMESSAGEWINDOWCHANGECURRENT_T1382911780_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.AdvCommandMessageWindowChangeCurrent
struct  AdvCommandMessageWindowChangeCurrent_t1382911780  : public AdvCommand_t2859960984
{
public:
	// System.String Utage.AdvCommandMessageWindowChangeCurrent::name
	String_t* ___name_7;

public:
	inline static int32_t get_offset_of_name_7() { return static_cast<int32_t>(offsetof(AdvCommandMessageWindowChangeCurrent_t1382911780, ___name_7)); }
	inline String_t* get_name_7() const { return ___name_7; }
	inline String_t** get_address_of_name_7() { return &___name_7; }
	inline void set_name_7(String_t* value)
	{
		___name_7 = value;
		Il2CppCodeGenWriteBarrier((&___name_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ADVCOMMANDMESSAGEWINDOWCHANGECURRENT_T1382911780_H
#ifndef ADVCOMMANDSHOWMESSAGEWINDOW_T2854851764_H
#define ADVCOMMANDSHOWMESSAGEWINDOW_T2854851764_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.AdvCommandShowMessageWindow
struct  AdvCommandShowMessageWindow_t2854851764  : public AdvCommand_t2859960984
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ADVCOMMANDSHOWMESSAGEWINDOW_T2854851764_H
#ifndef ADVCOMMANDGUIRESET_T3457969418_H
#define ADVCOMMANDGUIRESET_T3457969418_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.AdvCommandGuiReset
struct  AdvCommandGuiReset_t3457969418  : public AdvCommand_t2859960984
{
public:
	// System.String Utage.AdvCommandGuiReset::name
	String_t* ___name_7;

public:
	inline static int32_t get_offset_of_name_7() { return static_cast<int32_t>(offsetof(AdvCommandGuiReset_t3457969418, ___name_7)); }
	inline String_t* get_name_7() const { return ___name_7; }
	inline String_t** get_address_of_name_7() { return &___name_7; }
	inline void set_name_7(String_t* value)
	{
		___name_7 = value;
		Il2CppCodeGenWriteBarrier((&___name_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ADVCOMMANDGUIRESET_T3457969418_H
#ifndef ADVCOMMANDHIDEMENUBUTTON_T473930883_H
#define ADVCOMMANDHIDEMENUBUTTON_T473930883_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.AdvCommandHideMenuButton
struct  AdvCommandHideMenuButton_t473930883  : public AdvCommand_t2859960984
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ADVCOMMANDHIDEMENUBUTTON_T473930883_H
#ifndef ADVCOMMANDHIDEMESSAGEWINDOW_T2694955717_H
#define ADVCOMMANDHIDEMESSAGEWINDOW_T2694955717_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.AdvCommandHideMessageWindow
struct  AdvCommandHideMessageWindow_t2694955717  : public AdvCommand_t2859960984
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ADVCOMMANDHIDEMESSAGEWINDOW_T2694955717_H
#ifndef ADVCOMMANDGUIACTIVE_T2296013833_H
#define ADVCOMMANDGUIACTIVE_T2296013833_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.AdvCommandGuiActive
struct  AdvCommandGuiActive_t2296013833  : public AdvCommand_t2859960984
{
public:
	// System.String Utage.AdvCommandGuiActive::name
	String_t* ___name_7;
	// System.Boolean Utage.AdvCommandGuiActive::isActive
	bool ___isActive_8;

public:
	inline static int32_t get_offset_of_name_7() { return static_cast<int32_t>(offsetof(AdvCommandGuiActive_t2296013833, ___name_7)); }
	inline String_t* get_name_7() const { return ___name_7; }
	inline String_t** get_address_of_name_7() { return &___name_7; }
	inline void set_name_7(String_t* value)
	{
		___name_7 = value;
		Il2CppCodeGenWriteBarrier((&___name_7), value);
	}

	inline static int32_t get_offset_of_isActive_8() { return static_cast<int32_t>(offsetof(AdvCommandGuiActive_t2296013833, ___isActive_8)); }
	inline bool get_isActive_8() const { return ___isActive_8; }
	inline bool* get_address_of_isActive_8() { return &___isActive_8; }
	inline void set_isActive_8(bool value)
	{
		___isActive_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ADVCOMMANDGUIACTIVE_T2296013833_H
#ifndef ADVCOMMANDSHOWMENUBUTTON_T1122630226_H
#define ADVCOMMANDSHOWMENUBUTTON_T1122630226_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.AdvCommandShowMenuButton
struct  AdvCommandShowMenuButton_t1122630226  : public AdvCommand_t2859960984
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ADVCOMMANDSHOWMENUBUTTON_T1122630226_H
#ifndef ADVCOMMANDCHANGESOUNDVOLUME_T1394903671_H
#define ADVCOMMANDCHANGESOUNDVOLUME_T1394903671_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.AdvCommandChangeSoundVolume
struct  AdvCommandChangeSoundVolume_t1394903671  : public AdvCommand_t2859960984
{
public:
	// System.String[] Utage.AdvCommandChangeSoundVolume::groups
	StringU5BU5D_t1642385972* ___groups_7;
	// System.Single Utage.AdvCommandChangeSoundVolume::volume
	float ___volume_8;

public:
	inline static int32_t get_offset_of_groups_7() { return static_cast<int32_t>(offsetof(AdvCommandChangeSoundVolume_t1394903671, ___groups_7)); }
	inline StringU5BU5D_t1642385972* get_groups_7() const { return ___groups_7; }
	inline StringU5BU5D_t1642385972** get_address_of_groups_7() { return &___groups_7; }
	inline void set_groups_7(StringU5BU5D_t1642385972* value)
	{
		___groups_7 = value;
		Il2CppCodeGenWriteBarrier((&___groups_7), value);
	}

	inline static int32_t get_offset_of_volume_8() { return static_cast<int32_t>(offsetof(AdvCommandChangeSoundVolume_t1394903671, ___volume_8)); }
	inline float get_volume_8() const { return ___volume_8; }
	inline float* get_address_of_volume_8() { return &___volume_8; }
	inline void set_volume_8(float value)
	{
		___volume_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ADVCOMMANDCHANGESOUNDVOLUME_T1394903671_H
#ifndef ADVCOMMANDSE_T638350348_H
#define ADVCOMMANDSE_T638350348_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.AdvCommandSe
struct  AdvCommandSe_t638350348  : public AdvCommand_t2859960984
{
public:
	// System.String Utage.AdvCommandSe::label
	String_t* ___label_7;
	// Utage.AssetFile Utage.AdvCommandSe::file
	RuntimeObject* ___file_8;
	// System.Single Utage.AdvCommandSe::volume
	float ___volume_9;
	// System.Boolean Utage.AdvCommandSe::isLoop
	bool ___isLoop_10;

public:
	inline static int32_t get_offset_of_label_7() { return static_cast<int32_t>(offsetof(AdvCommandSe_t638350348, ___label_7)); }
	inline String_t* get_label_7() const { return ___label_7; }
	inline String_t** get_address_of_label_7() { return &___label_7; }
	inline void set_label_7(String_t* value)
	{
		___label_7 = value;
		Il2CppCodeGenWriteBarrier((&___label_7), value);
	}

	inline static int32_t get_offset_of_file_8() { return static_cast<int32_t>(offsetof(AdvCommandSe_t638350348, ___file_8)); }
	inline RuntimeObject* get_file_8() const { return ___file_8; }
	inline RuntimeObject** get_address_of_file_8() { return &___file_8; }
	inline void set_file_8(RuntimeObject* value)
	{
		___file_8 = value;
		Il2CppCodeGenWriteBarrier((&___file_8), value);
	}

	inline static int32_t get_offset_of_volume_9() { return static_cast<int32_t>(offsetof(AdvCommandSe_t638350348, ___volume_9)); }
	inline float get_volume_9() const { return ___volume_9; }
	inline float* get_address_of_volume_9() { return &___volume_9; }
	inline void set_volume_9(float value)
	{
		___volume_9 = value;
	}

	inline static int32_t get_offset_of_isLoop_10() { return static_cast<int32_t>(offsetof(AdvCommandSe_t638350348, ___isLoop_10)); }
	inline bool get_isLoop_10() const { return ___isLoop_10; }
	inline bool* get_address_of_isLoop_10() { return &___isLoop_10; }
	inline void set_isLoop_10(bool value)
	{
		___isLoop_10 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ADVCOMMANDSE_T638350348_H
#ifndef ADVCOMMANDSTOPAMBIENCE_T159697432_H
#define ADVCOMMANDSTOPAMBIENCE_T159697432_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.AdvCommandStopAmbience
struct  AdvCommandStopAmbience_t159697432  : public AdvCommand_t2859960984
{
public:
	// System.Single Utage.AdvCommandStopAmbience::fadeTime
	float ___fadeTime_7;

public:
	inline static int32_t get_offset_of_fadeTime_7() { return static_cast<int32_t>(offsetof(AdvCommandStopAmbience_t159697432, ___fadeTime_7)); }
	inline float get_fadeTime_7() const { return ___fadeTime_7; }
	inline float* get_address_of_fadeTime_7() { return &___fadeTime_7; }
	inline void set_fadeTime_7(float value)
	{
		___fadeTime_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ADVCOMMANDSTOPAMBIENCE_T159697432_H
#ifndef ADVCOMMANDSTOPBGM_T1182512534_H
#define ADVCOMMANDSTOPBGM_T1182512534_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.AdvCommandStopBgm
struct  AdvCommandStopBgm_t1182512534  : public AdvCommand_t2859960984
{
public:
	// System.Single Utage.AdvCommandStopBgm::fadeTime
	float ___fadeTime_7;

public:
	inline static int32_t get_offset_of_fadeTime_7() { return static_cast<int32_t>(offsetof(AdvCommandStopBgm_t1182512534, ___fadeTime_7)); }
	inline float get_fadeTime_7() const { return ___fadeTime_7; }
	inline float* get_address_of_fadeTime_7() { return &___fadeTime_7; }
	inline void set_fadeTime_7(float value)
	{
		___fadeTime_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ADVCOMMANDSTOPBGM_T1182512534_H
#ifndef ADVCOMMANDSTOPSE_T3930501764_H
#define ADVCOMMANDSTOPSE_T3930501764_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.AdvCommandStopSe
struct  AdvCommandStopSe_t3930501764  : public AdvCommand_t2859960984
{
public:
	// System.String Utage.AdvCommandStopSe::label
	String_t* ___label_7;
	// System.Single Utage.AdvCommandStopSe::fadeTime
	float ___fadeTime_8;

public:
	inline static int32_t get_offset_of_label_7() { return static_cast<int32_t>(offsetof(AdvCommandStopSe_t3930501764, ___label_7)); }
	inline String_t* get_label_7() const { return ___label_7; }
	inline String_t** get_address_of_label_7() { return &___label_7; }
	inline void set_label_7(String_t* value)
	{
		___label_7 = value;
		Il2CppCodeGenWriteBarrier((&___label_7), value);
	}

	inline static int32_t get_offset_of_fadeTime_8() { return static_cast<int32_t>(offsetof(AdvCommandStopSe_t3930501764, ___fadeTime_8)); }
	inline float get_fadeTime_8() const { return ___fadeTime_8; }
	inline float* get_address_of_fadeTime_8() { return &___fadeTime_8; }
	inline void set_fadeTime_8(float value)
	{
		___fadeTime_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ADVCOMMANDSTOPSE_T3930501764_H
#ifndef ADVCOMMANDSTOPSOUND_T2965519105_H
#define ADVCOMMANDSTOPSOUND_T2965519105_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.AdvCommandStopSound
struct  AdvCommandStopSound_t2965519105  : public AdvCommand_t2859960984
{
public:
	// System.String[] Utage.AdvCommandStopSound::groups
	StringU5BU5D_t1642385972* ___groups_7;
	// System.Single Utage.AdvCommandStopSound::fadeTime
	float ___fadeTime_8;

public:
	inline static int32_t get_offset_of_groups_7() { return static_cast<int32_t>(offsetof(AdvCommandStopSound_t2965519105, ___groups_7)); }
	inline StringU5BU5D_t1642385972* get_groups_7() const { return ___groups_7; }
	inline StringU5BU5D_t1642385972** get_address_of_groups_7() { return &___groups_7; }
	inline void set_groups_7(StringU5BU5D_t1642385972* value)
	{
		___groups_7 = value;
		Il2CppCodeGenWriteBarrier((&___groups_7), value);
	}

	inline static int32_t get_offset_of_fadeTime_8() { return static_cast<int32_t>(offsetof(AdvCommandStopSound_t2965519105, ___fadeTime_8)); }
	inline float get_fadeTime_8() const { return ___fadeTime_8; }
	inline float* get_address_of_fadeTime_8() { return &___fadeTime_8; }
	inline void set_fadeTime_8(float value)
	{
		___fadeTime_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ADVCOMMANDSTOPSOUND_T2965519105_H
#ifndef ADVCOMMANDSTOPVOICE_T1849776858_H
#define ADVCOMMANDSTOPVOICE_T1849776858_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.AdvCommandStopVoice
struct  AdvCommandStopVoice_t1849776858  : public AdvCommand_t2859960984
{
public:
	// System.Single Utage.AdvCommandStopVoice::fadeTime
	float ___fadeTime_7;

public:
	inline static int32_t get_offset_of_fadeTime_7() { return static_cast<int32_t>(offsetof(AdvCommandStopVoice_t1849776858, ___fadeTime_7)); }
	inline float get_fadeTime_7() const { return ___fadeTime_7; }
	inline float* get_address_of_fadeTime_7() { return &___fadeTime_7; }
	inline void set_fadeTime_7(float value)
	{
		___fadeTime_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ADVCOMMANDSTOPVOICE_T1849776858_H
#ifndef ADVCOMMANDVOICE_T1582521554_H
#define ADVCOMMANDVOICE_T1582521554_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.AdvCommandVoice
struct  AdvCommandVoice_t1582521554  : public AdvCommand_t2859960984
{
public:
	// System.String Utage.AdvCommandVoice::characterLabel
	String_t* ___characterLabel_7;
	// Utage.AssetFile Utage.AdvCommandVoice::voiceFile
	RuntimeObject* ___voiceFile_8;
	// System.Single Utage.AdvCommandVoice::volume
	float ___volume_9;
	// System.Boolean Utage.AdvCommandVoice::isLoop
	bool ___isLoop_10;

public:
	inline static int32_t get_offset_of_characterLabel_7() { return static_cast<int32_t>(offsetof(AdvCommandVoice_t1582521554, ___characterLabel_7)); }
	inline String_t* get_characterLabel_7() const { return ___characterLabel_7; }
	inline String_t** get_address_of_characterLabel_7() { return &___characterLabel_7; }
	inline void set_characterLabel_7(String_t* value)
	{
		___characterLabel_7 = value;
		Il2CppCodeGenWriteBarrier((&___characterLabel_7), value);
	}

	inline static int32_t get_offset_of_voiceFile_8() { return static_cast<int32_t>(offsetof(AdvCommandVoice_t1582521554, ___voiceFile_8)); }
	inline RuntimeObject* get_voiceFile_8() const { return ___voiceFile_8; }
	inline RuntimeObject** get_address_of_voiceFile_8() { return &___voiceFile_8; }
	inline void set_voiceFile_8(RuntimeObject* value)
	{
		___voiceFile_8 = value;
		Il2CppCodeGenWriteBarrier((&___voiceFile_8), value);
	}

	inline static int32_t get_offset_of_volume_9() { return static_cast<int32_t>(offsetof(AdvCommandVoice_t1582521554, ___volume_9)); }
	inline float get_volume_9() const { return ___volume_9; }
	inline float* get_address_of_volume_9() { return &___volume_9; }
	inline void set_volume_9(float value)
	{
		___volume_9 = value;
	}

	inline static int32_t get_offset_of_isLoop_10() { return static_cast<int32_t>(offsetof(AdvCommandVoice_t1582521554, ___isLoop_10)); }
	inline bool get_isLoop_10() const { return ___isLoop_10; }
	inline bool* get_address_of_isLoop_10() { return &___isLoop_10; }
	inline void set_isLoop_10(bool value)
	{
		___isLoop_10 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ADVCOMMANDVOICE_T1582521554_H
#ifndef ADVCOMMANDMESSAGEWINDOWINIT_T2380785243_H
#define ADVCOMMANDMESSAGEWINDOWINIT_T2380785243_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.AdvCommandMessageWindowInit
struct  AdvCommandMessageWindowInit_t2380785243  : public AdvCommand_t2859960984
{
public:
	// System.Collections.Generic.List`1<System.String> Utage.AdvCommandMessageWindowInit::names
	List_1_t1398341365 * ___names_7;

public:
	inline static int32_t get_offset_of_names_7() { return static_cast<int32_t>(offsetof(AdvCommandMessageWindowInit_t2380785243, ___names_7)); }
	inline List_1_t1398341365 * get_names_7() const { return ___names_7; }
	inline List_1_t1398341365 ** get_address_of_names_7() { return &___names_7; }
	inline void set_names_7(List_1_t1398341365 * value)
	{
		___names_7 = value;
		Il2CppCodeGenWriteBarrier((&___names_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ADVCOMMANDMESSAGEWINDOWINIT_T2380785243_H
#ifndef ENUMERATOR_T933071039_H
#define ENUMERATOR_T933071039_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.List`1/Enumerator<System.String>
struct  Enumerator_t933071039 
{
public:
	// System.Collections.Generic.List`1<T> System.Collections.Generic.List`1/Enumerator::l
	List_1_t1398341365 * ___l_0;
	// System.Int32 System.Collections.Generic.List`1/Enumerator::next
	int32_t ___next_1;
	// System.Int32 System.Collections.Generic.List`1/Enumerator::ver
	int32_t ___ver_2;
	// T System.Collections.Generic.List`1/Enumerator::current
	String_t* ___current_3;

public:
	inline static int32_t get_offset_of_l_0() { return static_cast<int32_t>(offsetof(Enumerator_t933071039, ___l_0)); }
	inline List_1_t1398341365 * get_l_0() const { return ___l_0; }
	inline List_1_t1398341365 ** get_address_of_l_0() { return &___l_0; }
	inline void set_l_0(List_1_t1398341365 * value)
	{
		___l_0 = value;
		Il2CppCodeGenWriteBarrier((&___l_0), value);
	}

	inline static int32_t get_offset_of_next_1() { return static_cast<int32_t>(offsetof(Enumerator_t933071039, ___next_1)); }
	inline int32_t get_next_1() const { return ___next_1; }
	inline int32_t* get_address_of_next_1() { return &___next_1; }
	inline void set_next_1(int32_t value)
	{
		___next_1 = value;
	}

	inline static int32_t get_offset_of_ver_2() { return static_cast<int32_t>(offsetof(Enumerator_t933071039, ___ver_2)); }
	inline int32_t get_ver_2() const { return ___ver_2; }
	inline int32_t* get_address_of_ver_2() { return &___ver_2; }
	inline void set_ver_2(int32_t value)
	{
		___ver_2 = value;
	}

	inline static int32_t get_offset_of_current_3() { return static_cast<int32_t>(offsetof(Enumerator_t933071039, ___current_3)); }
	inline String_t* get_current_3() const { return ___current_3; }
	inline String_t** get_address_of_current_3() { return &___current_3; }
	inline void set_current_3(String_t* value)
	{
		___current_3 = value;
		Il2CppCodeGenWriteBarrier((&___current_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENUMERATOR_T933071039_H
#ifndef VOID_T1841601450_H
#define VOID_T1841601450_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Void
struct  Void_t1841601450 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VOID_T1841601450_H
#ifndef COLOR_T2020392075_H
#define COLOR_T2020392075_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Color
struct  Color_t2020392075 
{
public:
	// System.Single UnityEngine.Color::r
	float ___r_0;
	// System.Single UnityEngine.Color::g
	float ___g_1;
	// System.Single UnityEngine.Color::b
	float ___b_2;
	// System.Single UnityEngine.Color::a
	float ___a_3;

public:
	inline static int32_t get_offset_of_r_0() { return static_cast<int32_t>(offsetof(Color_t2020392075, ___r_0)); }
	inline float get_r_0() const { return ___r_0; }
	inline float* get_address_of_r_0() { return &___r_0; }
	inline void set_r_0(float value)
	{
		___r_0 = value;
	}

	inline static int32_t get_offset_of_g_1() { return static_cast<int32_t>(offsetof(Color_t2020392075, ___g_1)); }
	inline float get_g_1() const { return ___g_1; }
	inline float* get_address_of_g_1() { return &___g_1; }
	inline void set_g_1(float value)
	{
		___g_1 = value;
	}

	inline static int32_t get_offset_of_b_2() { return static_cast<int32_t>(offsetof(Color_t2020392075, ___b_2)); }
	inline float get_b_2() const { return ___b_2; }
	inline float* get_address_of_b_2() { return &___b_2; }
	inline void set_b_2(float value)
	{
		___b_2 = value;
	}

	inline static int32_t get_offset_of_a_3() { return static_cast<int32_t>(offsetof(Color_t2020392075, ___a_3)); }
	inline float get_a_3() const { return ___a_3; }
	inline float* get_address_of_a_3() { return &___a_3; }
	inline void set_a_3(float value)
	{
		___a_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLOR_T2020392075_H
#ifndef ENUM_T2459695545_H
#define ENUM_T2459695545_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Enum
struct  Enum_t2459695545  : public ValueType_t3507792607
{
public:

public:
};

struct Enum_t2459695545_StaticFields
{
public:
	// System.Char[] System.Enum::split_char
	CharU5BU5D_t1328083999* ___split_char_0;

public:
	inline static int32_t get_offset_of_split_char_0() { return static_cast<int32_t>(offsetof(Enum_t2459695545_StaticFields, ___split_char_0)); }
	inline CharU5BU5D_t1328083999* get_split_char_0() const { return ___split_char_0; }
	inline CharU5BU5D_t1328083999** get_address_of_split_char_0() { return &___split_char_0; }
	inline void set_split_char_0(CharU5BU5D_t1328083999* value)
	{
		___split_char_0 = value;
		Il2CppCodeGenWriteBarrier((&___split_char_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t2459695545_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t2459695545_marshaled_com
{
};
#endif // ENUM_T2459695545_H
#ifndef NULLABLE_1_T339576247_H
#define NULLABLE_1_T339576247_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Nullable`1<System.Single>
struct  Nullable_1_t339576247 
{
public:
	// T System.Nullable`1::value
	float ___value_0;
	// System.Boolean System.Nullable`1::has_value
	bool ___has_value_1;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(Nullable_1_t339576247, ___value_0)); }
	inline float get_value_0() const { return ___value_0; }
	inline float* get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(float value)
	{
		___value_0 = value;
	}

	inline static int32_t get_offset_of_has_value_1() { return static_cast<int32_t>(offsetof(Nullable_1_t339576247, ___has_value_1)); }
	inline bool get_has_value_1() const { return ___has_value_1; }
	inline bool* get_address_of_has_value_1() { return &___has_value_1; }
	inline void set_has_value_1(bool value)
	{
		___has_value_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NULLABLE_1_T339576247_H
#ifndef VECTOR2_T2243707579_H
#define VECTOR2_T2243707579_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector2
struct  Vector2_t2243707579 
{
public:
	// System.Single UnityEngine.Vector2::x
	float ___x_0;
	// System.Single UnityEngine.Vector2::y
	float ___y_1;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Vector2_t2243707579, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Vector2_t2243707579, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}
};

struct Vector2_t2243707579_StaticFields
{
public:
	// UnityEngine.Vector2 UnityEngine.Vector2::zeroVector
	Vector2_t2243707579  ___zeroVector_2;
	// UnityEngine.Vector2 UnityEngine.Vector2::oneVector
	Vector2_t2243707579  ___oneVector_3;
	// UnityEngine.Vector2 UnityEngine.Vector2::upVector
	Vector2_t2243707579  ___upVector_4;
	// UnityEngine.Vector2 UnityEngine.Vector2::downVector
	Vector2_t2243707579  ___downVector_5;
	// UnityEngine.Vector2 UnityEngine.Vector2::leftVector
	Vector2_t2243707579  ___leftVector_6;
	// UnityEngine.Vector2 UnityEngine.Vector2::rightVector
	Vector2_t2243707579  ___rightVector_7;
	// UnityEngine.Vector2 UnityEngine.Vector2::positiveInfinityVector
	Vector2_t2243707579  ___positiveInfinityVector_8;
	// UnityEngine.Vector2 UnityEngine.Vector2::negativeInfinityVector
	Vector2_t2243707579  ___negativeInfinityVector_9;

public:
	inline static int32_t get_offset_of_zeroVector_2() { return static_cast<int32_t>(offsetof(Vector2_t2243707579_StaticFields, ___zeroVector_2)); }
	inline Vector2_t2243707579  get_zeroVector_2() const { return ___zeroVector_2; }
	inline Vector2_t2243707579 * get_address_of_zeroVector_2() { return &___zeroVector_2; }
	inline void set_zeroVector_2(Vector2_t2243707579  value)
	{
		___zeroVector_2 = value;
	}

	inline static int32_t get_offset_of_oneVector_3() { return static_cast<int32_t>(offsetof(Vector2_t2243707579_StaticFields, ___oneVector_3)); }
	inline Vector2_t2243707579  get_oneVector_3() const { return ___oneVector_3; }
	inline Vector2_t2243707579 * get_address_of_oneVector_3() { return &___oneVector_3; }
	inline void set_oneVector_3(Vector2_t2243707579  value)
	{
		___oneVector_3 = value;
	}

	inline static int32_t get_offset_of_upVector_4() { return static_cast<int32_t>(offsetof(Vector2_t2243707579_StaticFields, ___upVector_4)); }
	inline Vector2_t2243707579  get_upVector_4() const { return ___upVector_4; }
	inline Vector2_t2243707579 * get_address_of_upVector_4() { return &___upVector_4; }
	inline void set_upVector_4(Vector2_t2243707579  value)
	{
		___upVector_4 = value;
	}

	inline static int32_t get_offset_of_downVector_5() { return static_cast<int32_t>(offsetof(Vector2_t2243707579_StaticFields, ___downVector_5)); }
	inline Vector2_t2243707579  get_downVector_5() const { return ___downVector_5; }
	inline Vector2_t2243707579 * get_address_of_downVector_5() { return &___downVector_5; }
	inline void set_downVector_5(Vector2_t2243707579  value)
	{
		___downVector_5 = value;
	}

	inline static int32_t get_offset_of_leftVector_6() { return static_cast<int32_t>(offsetof(Vector2_t2243707579_StaticFields, ___leftVector_6)); }
	inline Vector2_t2243707579  get_leftVector_6() const { return ___leftVector_6; }
	inline Vector2_t2243707579 * get_address_of_leftVector_6() { return &___leftVector_6; }
	inline void set_leftVector_6(Vector2_t2243707579  value)
	{
		___leftVector_6 = value;
	}

	inline static int32_t get_offset_of_rightVector_7() { return static_cast<int32_t>(offsetof(Vector2_t2243707579_StaticFields, ___rightVector_7)); }
	inline Vector2_t2243707579  get_rightVector_7() const { return ___rightVector_7; }
	inline Vector2_t2243707579 * get_address_of_rightVector_7() { return &___rightVector_7; }
	inline void set_rightVector_7(Vector2_t2243707579  value)
	{
		___rightVector_7 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_8() { return static_cast<int32_t>(offsetof(Vector2_t2243707579_StaticFields, ___positiveInfinityVector_8)); }
	inline Vector2_t2243707579  get_positiveInfinityVector_8() const { return ___positiveInfinityVector_8; }
	inline Vector2_t2243707579 * get_address_of_positiveInfinityVector_8() { return &___positiveInfinityVector_8; }
	inline void set_positiveInfinityVector_8(Vector2_t2243707579  value)
	{
		___positiveInfinityVector_8 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_9() { return static_cast<int32_t>(offsetof(Vector2_t2243707579_StaticFields, ___negativeInfinityVector_9)); }
	inline Vector2_t2243707579  get_negativeInfinityVector_9() const { return ___negativeInfinityVector_9; }
	inline Vector2_t2243707579 * get_address_of_negativeInfinityVector_9() { return &___negativeInfinityVector_9; }
	inline void set_negativeInfinityVector_9(Vector2_t2243707579  value)
	{
		___negativeInfinityVector_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR2_T2243707579_H
#ifndef INTPTR_T_H
#define INTPTR_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTPTR_T_H
#ifndef ADVCOMMANDSENDMESSAGE_T330862209_H
#define ADVCOMMANDSENDMESSAGE_T330862209_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.AdvCommandSendMessage
struct  AdvCommandSendMessage_t330862209  : public AdvCommand_t2859960984
{
public:
	// System.Boolean Utage.AdvCommandSendMessage::isWait
	bool ___isWait_7;
	// System.String Utage.AdvCommandSendMessage::name
	String_t* ___name_8;
	// System.String Utage.AdvCommandSendMessage::arg2
	String_t* ___arg2_9;
	// System.String Utage.AdvCommandSendMessage::arg3
	String_t* ___arg3_10;
	// System.String Utage.AdvCommandSendMessage::arg4
	String_t* ___arg4_11;
	// System.String Utage.AdvCommandSendMessage::arg5
	String_t* ___arg5_12;
	// System.String Utage.AdvCommandSendMessage::text
	String_t* ___text_13;
	// System.String Utage.AdvCommandSendMessage::voice
	String_t* ___voice_14;
	// System.Int32 Utage.AdvCommandSendMessage::voiceVersion
	int32_t ___voiceVersion_15;

public:
	inline static int32_t get_offset_of_isWait_7() { return static_cast<int32_t>(offsetof(AdvCommandSendMessage_t330862209, ___isWait_7)); }
	inline bool get_isWait_7() const { return ___isWait_7; }
	inline bool* get_address_of_isWait_7() { return &___isWait_7; }
	inline void set_isWait_7(bool value)
	{
		___isWait_7 = value;
	}

	inline static int32_t get_offset_of_name_8() { return static_cast<int32_t>(offsetof(AdvCommandSendMessage_t330862209, ___name_8)); }
	inline String_t* get_name_8() const { return ___name_8; }
	inline String_t** get_address_of_name_8() { return &___name_8; }
	inline void set_name_8(String_t* value)
	{
		___name_8 = value;
		Il2CppCodeGenWriteBarrier((&___name_8), value);
	}

	inline static int32_t get_offset_of_arg2_9() { return static_cast<int32_t>(offsetof(AdvCommandSendMessage_t330862209, ___arg2_9)); }
	inline String_t* get_arg2_9() const { return ___arg2_9; }
	inline String_t** get_address_of_arg2_9() { return &___arg2_9; }
	inline void set_arg2_9(String_t* value)
	{
		___arg2_9 = value;
		Il2CppCodeGenWriteBarrier((&___arg2_9), value);
	}

	inline static int32_t get_offset_of_arg3_10() { return static_cast<int32_t>(offsetof(AdvCommandSendMessage_t330862209, ___arg3_10)); }
	inline String_t* get_arg3_10() const { return ___arg3_10; }
	inline String_t** get_address_of_arg3_10() { return &___arg3_10; }
	inline void set_arg3_10(String_t* value)
	{
		___arg3_10 = value;
		Il2CppCodeGenWriteBarrier((&___arg3_10), value);
	}

	inline static int32_t get_offset_of_arg4_11() { return static_cast<int32_t>(offsetof(AdvCommandSendMessage_t330862209, ___arg4_11)); }
	inline String_t* get_arg4_11() const { return ___arg4_11; }
	inline String_t** get_address_of_arg4_11() { return &___arg4_11; }
	inline void set_arg4_11(String_t* value)
	{
		___arg4_11 = value;
		Il2CppCodeGenWriteBarrier((&___arg4_11), value);
	}

	inline static int32_t get_offset_of_arg5_12() { return static_cast<int32_t>(offsetof(AdvCommandSendMessage_t330862209, ___arg5_12)); }
	inline String_t* get_arg5_12() const { return ___arg5_12; }
	inline String_t** get_address_of_arg5_12() { return &___arg5_12; }
	inline void set_arg5_12(String_t* value)
	{
		___arg5_12 = value;
		Il2CppCodeGenWriteBarrier((&___arg5_12), value);
	}

	inline static int32_t get_offset_of_text_13() { return static_cast<int32_t>(offsetof(AdvCommandSendMessage_t330862209, ___text_13)); }
	inline String_t* get_text_13() const { return ___text_13; }
	inline String_t** get_address_of_text_13() { return &___text_13; }
	inline void set_text_13(String_t* value)
	{
		___text_13 = value;
		Il2CppCodeGenWriteBarrier((&___text_13), value);
	}

	inline static int32_t get_offset_of_voice_14() { return static_cast<int32_t>(offsetof(AdvCommandSendMessage_t330862209, ___voice_14)); }
	inline String_t* get_voice_14() const { return ___voice_14; }
	inline String_t** get_address_of_voice_14() { return &___voice_14; }
	inline void set_voice_14(String_t* value)
	{
		___voice_14 = value;
		Il2CppCodeGenWriteBarrier((&___voice_14), value);
	}

	inline static int32_t get_offset_of_voiceVersion_15() { return static_cast<int32_t>(offsetof(AdvCommandSendMessage_t330862209, ___voiceVersion_15)); }
	inline int32_t get_voiceVersion_15() const { return ___voiceVersion_15; }
	inline int32_t* get_address_of_voiceVersion_15() { return &___voiceVersion_15; }
	inline void set_voiceVersion_15(int32_t value)
	{
		___voiceVersion_15 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ADVCOMMANDSENDMESSAGE_T330862209_H
#ifndef ADVCOMMANDCAPTUREIMAGE_T1169550925_H
#define ADVCOMMANDCAPTUREIMAGE_T1169550925_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.AdvCommandCaptureImage
struct  AdvCommandCaptureImage_t1169550925  : public AdvCommand_t2859960984
{
public:
	// System.String Utage.AdvCommandCaptureImage::objName
	String_t* ___objName_7;
	// System.String Utage.AdvCommandCaptureImage::cameraName
	String_t* ___cameraName_8;
	// System.String Utage.AdvCommandCaptureImage::layerName
	String_t* ___layerName_9;
	// System.Boolean Utage.AdvCommandCaptureImage::isWaiting
	bool ___isWaiting_10;

public:
	inline static int32_t get_offset_of_objName_7() { return static_cast<int32_t>(offsetof(AdvCommandCaptureImage_t1169550925, ___objName_7)); }
	inline String_t* get_objName_7() const { return ___objName_7; }
	inline String_t** get_address_of_objName_7() { return &___objName_7; }
	inline void set_objName_7(String_t* value)
	{
		___objName_7 = value;
		Il2CppCodeGenWriteBarrier((&___objName_7), value);
	}

	inline static int32_t get_offset_of_cameraName_8() { return static_cast<int32_t>(offsetof(AdvCommandCaptureImage_t1169550925, ___cameraName_8)); }
	inline String_t* get_cameraName_8() const { return ___cameraName_8; }
	inline String_t** get_address_of_cameraName_8() { return &___cameraName_8; }
	inline void set_cameraName_8(String_t* value)
	{
		___cameraName_8 = value;
		Il2CppCodeGenWriteBarrier((&___cameraName_8), value);
	}

	inline static int32_t get_offset_of_layerName_9() { return static_cast<int32_t>(offsetof(AdvCommandCaptureImage_t1169550925, ___layerName_9)); }
	inline String_t* get_layerName_9() const { return ___layerName_9; }
	inline String_t** get_address_of_layerName_9() { return &___layerName_9; }
	inline void set_layerName_9(String_t* value)
	{
		___layerName_9 = value;
		Il2CppCodeGenWriteBarrier((&___layerName_9), value);
	}

	inline static int32_t get_offset_of_isWaiting_10() { return static_cast<int32_t>(offsetof(AdvCommandCaptureImage_t1169550925, ___isWaiting_10)); }
	inline bool get_isWaiting_10() const { return ___isWaiting_10; }
	inline bool* get_address_of_isWaiting_10() { return &___isWaiting_10; }
	inline void set_isWaiting_10(bool value)
	{
		___isWaiting_10 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ADVCOMMANDCAPTUREIMAGE_T1169550925_H
#ifndef ADVCOMMANDMOVIE_T3101533308_H
#define ADVCOMMANDMOVIE_T3101533308_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.AdvCommandMovie
struct  AdvCommandMovie_t3101533308  : public AdvCommand_t2859960984
{
public:
	// System.String Utage.AdvCommandMovie::label
	String_t* ___label_7;
	// System.Boolean Utage.AdvCommandMovie::loop
	bool ___loop_8;
	// System.Boolean Utage.AdvCommandMovie::cancel
	bool ___cancel_9;
	// System.Single Utage.AdvCommandMovie::waitTime
	float ___waitTime_10;
	// System.Single Utage.AdvCommandMovie::time
	float ___time_11;

public:
	inline static int32_t get_offset_of_label_7() { return static_cast<int32_t>(offsetof(AdvCommandMovie_t3101533308, ___label_7)); }
	inline String_t* get_label_7() const { return ___label_7; }
	inline String_t** get_address_of_label_7() { return &___label_7; }
	inline void set_label_7(String_t* value)
	{
		___label_7 = value;
		Il2CppCodeGenWriteBarrier((&___label_7), value);
	}

	inline static int32_t get_offset_of_loop_8() { return static_cast<int32_t>(offsetof(AdvCommandMovie_t3101533308, ___loop_8)); }
	inline bool get_loop_8() const { return ___loop_8; }
	inline bool* get_address_of_loop_8() { return &___loop_8; }
	inline void set_loop_8(bool value)
	{
		___loop_8 = value;
	}

	inline static int32_t get_offset_of_cancel_9() { return static_cast<int32_t>(offsetof(AdvCommandMovie_t3101533308, ___cancel_9)); }
	inline bool get_cancel_9() const { return ___cancel_9; }
	inline bool* get_address_of_cancel_9() { return &___cancel_9; }
	inline void set_cancel_9(bool value)
	{
		___cancel_9 = value;
	}

	inline static int32_t get_offset_of_waitTime_10() { return static_cast<int32_t>(offsetof(AdvCommandMovie_t3101533308, ___waitTime_10)); }
	inline float get_waitTime_10() const { return ___waitTime_10; }
	inline float* get_address_of_waitTime_10() { return &___waitTime_10; }
	inline void set_waitTime_10(float value)
	{
		___waitTime_10 = value;
	}

	inline static int32_t get_offset_of_time_11() { return static_cast<int32_t>(offsetof(AdvCommandMovie_t3101533308, ___time_11)); }
	inline float get_time_11() const { return ___time_11; }
	inline float* get_address_of_time_11() { return &___time_11; }
	inline void set_time_11(float value)
	{
		___time_11 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ADVCOMMANDMOVIE_T3101533308_H
#ifndef ADVCOMMANDTHREAD_T4076899860_H
#define ADVCOMMANDTHREAD_T4076899860_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.AdvCommandThread
struct  AdvCommandThread_t4076899860  : public AdvCommand_t2859960984
{
public:
	// System.String Utage.AdvCommandThread::label
	String_t* ___label_7;
	// System.String Utage.AdvCommandThread::name
	String_t* ___name_8;

public:
	inline static int32_t get_offset_of_label_7() { return static_cast<int32_t>(offsetof(AdvCommandThread_t4076899860, ___label_7)); }
	inline String_t* get_label_7() const { return ___label_7; }
	inline String_t** get_address_of_label_7() { return &___label_7; }
	inline void set_label_7(String_t* value)
	{
		___label_7 = value;
		Il2CppCodeGenWriteBarrier((&___label_7), value);
	}

	inline static int32_t get_offset_of_name_8() { return static_cast<int32_t>(offsetof(AdvCommandThread_t4076899860, ___name_8)); }
	inline String_t* get_name_8() const { return ___name_8; }
	inline String_t** get_address_of_name_8() { return &___name_8; }
	inline void set_name_8(String_t* value)
	{
		___name_8 = value;
		Il2CppCodeGenWriteBarrier((&___name_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ADVCOMMANDTHREAD_T4076899860_H
#ifndef ADVCOMMANDVIBRATE_T3795758989_H
#define ADVCOMMANDVIBRATE_T3795758989_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.AdvCommandVibrate
struct  AdvCommandVibrate_t3795758989  : public AdvCommand_t2859960984
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ADVCOMMANDVIBRATE_T3795758989_H
#ifndef ADVCOMMANDWAIT_T1737713393_H
#define ADVCOMMANDWAIT_T1737713393_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.AdvCommandWait
struct  AdvCommandWait_t1737713393  : public AdvCommand_t2859960984
{
public:
	// System.Single Utage.AdvCommandWait::time
	float ___time_7;
	// System.Single Utage.AdvCommandWait::waitEndTime
	float ___waitEndTime_8;

public:
	inline static int32_t get_offset_of_time_7() { return static_cast<int32_t>(offsetof(AdvCommandWait_t1737713393, ___time_7)); }
	inline float get_time_7() const { return ___time_7; }
	inline float* get_address_of_time_7() { return &___time_7; }
	inline void set_time_7(float value)
	{
		___time_7 = value;
	}

	inline static int32_t get_offset_of_waitEndTime_8() { return static_cast<int32_t>(offsetof(AdvCommandWait_t1737713393, ___waitEndTime_8)); }
	inline float get_waitEndTime_8() const { return ___waitEndTime_8; }
	inline float* get_address_of_waitEndTime_8() { return &___waitEndTime_8; }
	inline void set_waitEndTime_8(float value)
	{
		___waitEndTime_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ADVCOMMANDWAIT_T1737713393_H
#ifndef ADVCOMMANDWAITCUSTOM_T37803082_H
#define ADVCOMMANDWAITCUSTOM_T37803082_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.AdvCommandWaitCustom
struct  AdvCommandWaitCustom_t37803082  : public AdvCommand_t2859960984
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ADVCOMMANDWAITCUSTOM_T37803082_H
#ifndef ADVCOMMANDWAITINPUT_T2267805615_H
#define ADVCOMMANDWAITINPUT_T2267805615_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.AdvCommandWaitInput
struct  AdvCommandWaitInput_t2267805615  : public AdvCommand_t2859960984
{
public:
	// System.Single Utage.AdvCommandWaitInput::time
	float ___time_7;
	// System.Single Utage.AdvCommandWaitInput::waitEndTime
	float ___waitEndTime_8;

public:
	inline static int32_t get_offset_of_time_7() { return static_cast<int32_t>(offsetof(AdvCommandWaitInput_t2267805615, ___time_7)); }
	inline float get_time_7() const { return ___time_7; }
	inline float* get_address_of_time_7() { return &___time_7; }
	inline void set_time_7(float value)
	{
		___time_7 = value;
	}

	inline static int32_t get_offset_of_waitEndTime_8() { return static_cast<int32_t>(offsetof(AdvCommandWaitInput_t2267805615, ___waitEndTime_8)); }
	inline float get_waitEndTime_8() const { return ___waitEndTime_8; }
	inline float* get_address_of_waitEndTime_8() { return &___waitEndTime_8; }
	inline void set_waitEndTime_8(float value)
	{
		___waitEndTime_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ADVCOMMANDWAITINPUT_T2267805615_H
#ifndef ADVCOMMANDWAITTHREAD_T120602403_H
#define ADVCOMMANDWAITTHREAD_T120602403_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.AdvCommandWaitThread
struct  AdvCommandWaitThread_t120602403  : public AdvCommand_t2859960984
{
public:
	// System.String Utage.AdvCommandWaitThread::label
	String_t* ___label_7;
	// System.Boolean Utage.AdvCommandWaitThread::cancelInput
	bool ___cancelInput_8;

public:
	inline static int32_t get_offset_of_label_7() { return static_cast<int32_t>(offsetof(AdvCommandWaitThread_t120602403, ___label_7)); }
	inline String_t* get_label_7() const { return ___label_7; }
	inline String_t** get_address_of_label_7() { return &___label_7; }
	inline void set_label_7(String_t* value)
	{
		___label_7 = value;
		Il2CppCodeGenWriteBarrier((&___label_7), value);
	}

	inline static int32_t get_offset_of_cancelInput_8() { return static_cast<int32_t>(offsetof(AdvCommandWaitThread_t120602403, ___cancelInput_8)); }
	inline bool get_cancelInput_8() const { return ___cancelInput_8; }
	inline bool* get_address_of_cancelInput_8() { return &___cancelInput_8; }
	inline void set_cancelInput_8(bool value)
	{
		___cancelInput_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ADVCOMMANDWAITTHREAD_T120602403_H
#ifndef ADVCOMMANDBGM_T3126670950_H
#define ADVCOMMANDBGM_T3126670950_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.AdvCommandBgm
struct  AdvCommandBgm_t3126670950  : public AdvCommand_t2859960984
{
public:
	// Utage.AssetFile Utage.AdvCommandBgm::file
	RuntimeObject* ___file_7;
	// System.Single Utage.AdvCommandBgm::volume
	float ___volume_8;
	// System.Single Utage.AdvCommandBgm::fadeInTime
	float ___fadeInTime_9;
	// System.Single Utage.AdvCommandBgm::fadeOutTime
	float ___fadeOutTime_10;

public:
	inline static int32_t get_offset_of_file_7() { return static_cast<int32_t>(offsetof(AdvCommandBgm_t3126670950, ___file_7)); }
	inline RuntimeObject* get_file_7() const { return ___file_7; }
	inline RuntimeObject** get_address_of_file_7() { return &___file_7; }
	inline void set_file_7(RuntimeObject* value)
	{
		___file_7 = value;
		Il2CppCodeGenWriteBarrier((&___file_7), value);
	}

	inline static int32_t get_offset_of_volume_8() { return static_cast<int32_t>(offsetof(AdvCommandBgm_t3126670950, ___volume_8)); }
	inline float get_volume_8() const { return ___volume_8; }
	inline float* get_address_of_volume_8() { return &___volume_8; }
	inline void set_volume_8(float value)
	{
		___volume_8 = value;
	}

	inline static int32_t get_offset_of_fadeInTime_9() { return static_cast<int32_t>(offsetof(AdvCommandBgm_t3126670950, ___fadeInTime_9)); }
	inline float get_fadeInTime_9() const { return ___fadeInTime_9; }
	inline float* get_address_of_fadeInTime_9() { return &___fadeInTime_9; }
	inline void set_fadeInTime_9(float value)
	{
		___fadeInTime_9 = value;
	}

	inline static int32_t get_offset_of_fadeOutTime_10() { return static_cast<int32_t>(offsetof(AdvCommandBgm_t3126670950, ___fadeOutTime_10)); }
	inline float get_fadeOutTime_10() const { return ___fadeOutTime_10; }
	inline float* get_address_of_fadeOutTime_10() { return &___fadeOutTime_10; }
	inline void set_fadeOutTime_10(float value)
	{
		___fadeOutTime_10 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ADVCOMMANDBGM_T3126670950_H
#ifndef ADVCOMMANDELSEIF_T976666238_H
#define ADVCOMMANDELSEIF_T976666238_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.AdvCommandElseIf
struct  AdvCommandElseIf_t976666238  : public AdvCommand_t2859960984
{
public:
	// Utage.ExpressionParser Utage.AdvCommandElseIf::exp
	ExpressionParser_t665800307 * ___exp_7;

public:
	inline static int32_t get_offset_of_exp_7() { return static_cast<int32_t>(offsetof(AdvCommandElseIf_t976666238, ___exp_7)); }
	inline ExpressionParser_t665800307 * get_exp_7() const { return ___exp_7; }
	inline ExpressionParser_t665800307 ** get_address_of_exp_7() { return &___exp_7; }
	inline void set_exp_7(ExpressionParser_t665800307 * value)
	{
		___exp_7 = value;
		Il2CppCodeGenWriteBarrier((&___exp_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ADVCOMMANDELSEIF_T976666238_H
#ifndef ADVCOMMANDENDIF_T1575863694_H
#define ADVCOMMANDENDIF_T1575863694_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.AdvCommandEndIf
struct  AdvCommandEndIf_t1575863694  : public AdvCommand_t2859960984
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ADVCOMMANDENDIF_T1575863694_H
#ifndef ADVCOMMANDBGEVENT_T467224855_H
#define ADVCOMMANDBGEVENT_T467224855_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.AdvCommandBgEvent
struct  AdvCommandBgEvent_t467224855  : public AdvCommand_t2859960984
{
public:
	// System.String Utage.AdvCommandBgEvent::label
	String_t* ___label_7;
	// Utage.AdvGraphicInfoList Utage.AdvCommandBgEvent::graphic
	AdvGraphicInfoList_t3537398639 * ___graphic_8;
	// System.Single Utage.AdvCommandBgEvent::fadeTime
	float ___fadeTime_9;

public:
	inline static int32_t get_offset_of_label_7() { return static_cast<int32_t>(offsetof(AdvCommandBgEvent_t467224855, ___label_7)); }
	inline String_t* get_label_7() const { return ___label_7; }
	inline String_t** get_address_of_label_7() { return &___label_7; }
	inline void set_label_7(String_t* value)
	{
		___label_7 = value;
		Il2CppCodeGenWriteBarrier((&___label_7), value);
	}

	inline static int32_t get_offset_of_graphic_8() { return static_cast<int32_t>(offsetof(AdvCommandBgEvent_t467224855, ___graphic_8)); }
	inline AdvGraphicInfoList_t3537398639 * get_graphic_8() const { return ___graphic_8; }
	inline AdvGraphicInfoList_t3537398639 ** get_address_of_graphic_8() { return &___graphic_8; }
	inline void set_graphic_8(AdvGraphicInfoList_t3537398639 * value)
	{
		___graphic_8 = value;
		Il2CppCodeGenWriteBarrier((&___graphic_8), value);
	}

	inline static int32_t get_offset_of_fadeTime_9() { return static_cast<int32_t>(offsetof(AdvCommandBgEvent_t467224855, ___fadeTime_9)); }
	inline float get_fadeTime_9() const { return ___fadeTime_9; }
	inline float* get_address_of_fadeTime_9() { return &___fadeTime_9; }
	inline void set_fadeTime_9(float value)
	{
		___fadeTime_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ADVCOMMANDBGEVENT_T467224855_H
#ifndef ADVCOMMANDENDPAGE_T2960206982_H
#define ADVCOMMANDENDPAGE_T2960206982_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.AdvCommandEndPage
struct  AdvCommandEndPage_t2960206982  : public AdvCommand_t2859960984
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ADVCOMMANDENDPAGE_T2960206982_H
#ifndef ADVCOMMANDENDSCENARIO_T3090793047_H
#define ADVCOMMANDENDSCENARIO_T3090793047_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.AdvCommandEndScenario
struct  AdvCommandEndScenario_t3090793047  : public AdvCommand_t2859960984
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ADVCOMMANDENDSCENARIO_T3090793047_H
#ifndef ADVCOMMANDENDSCENEGALLERY_T3250141107_H
#define ADVCOMMANDENDSCENEGALLERY_T3250141107_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.AdvCommandEndSceneGallery
struct  AdvCommandEndSceneGallery_t3250141107  : public AdvCommand_t2859960984
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ADVCOMMANDENDSCENEGALLERY_T3250141107_H
#ifndef ADVCOMMANDENDSUBROUTINE_T222520471_H
#define ADVCOMMANDENDSUBROUTINE_T222520471_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.AdvCommandEndSubroutine
struct  AdvCommandEndSubroutine_t222520471  : public AdvCommand_t2859960984
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ADVCOMMANDENDSUBROUTINE_T222520471_H
#ifndef ADVCOMMANDENDTHREAD_T574181939_H
#define ADVCOMMANDENDTHREAD_T574181939_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.AdvCommandEndThread
struct  AdvCommandEndThread_t574181939  : public AdvCommand_t2859960984
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ADVCOMMANDENDTHREAD_T574181939_H
#ifndef ADVCOMMANDELSE_T2658593445_H
#define ADVCOMMANDELSE_T2658593445_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.AdvCommandElse
struct  AdvCommandElse_t2658593445  : public AdvCommand_t2859960984
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ADVCOMMANDELSE_T2658593445_H
#ifndef ADVCOMMANDSPRITE_T3266644903_H
#define ADVCOMMANDSPRITE_T3266644903_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.AdvCommandSprite
struct  AdvCommandSprite_t3266644903  : public AdvCommand_t2859960984
{
public:
	// Utage.AdvGraphicInfoList Utage.AdvCommandSprite::graphic
	AdvGraphicInfoList_t3537398639 * ___graphic_7;
	// System.String Utage.AdvCommandSprite::layerName
	String_t* ___layerName_8;
	// System.String Utage.AdvCommandSprite::spriteName
	String_t* ___spriteName_9;
	// System.Single Utage.AdvCommandSprite::fadeTime
	float ___fadeTime_10;

public:
	inline static int32_t get_offset_of_graphic_7() { return static_cast<int32_t>(offsetof(AdvCommandSprite_t3266644903, ___graphic_7)); }
	inline AdvGraphicInfoList_t3537398639 * get_graphic_7() const { return ___graphic_7; }
	inline AdvGraphicInfoList_t3537398639 ** get_address_of_graphic_7() { return &___graphic_7; }
	inline void set_graphic_7(AdvGraphicInfoList_t3537398639 * value)
	{
		___graphic_7 = value;
		Il2CppCodeGenWriteBarrier((&___graphic_7), value);
	}

	inline static int32_t get_offset_of_layerName_8() { return static_cast<int32_t>(offsetof(AdvCommandSprite_t3266644903, ___layerName_8)); }
	inline String_t* get_layerName_8() const { return ___layerName_8; }
	inline String_t** get_address_of_layerName_8() { return &___layerName_8; }
	inline void set_layerName_8(String_t* value)
	{
		___layerName_8 = value;
		Il2CppCodeGenWriteBarrier((&___layerName_8), value);
	}

	inline static int32_t get_offset_of_spriteName_9() { return static_cast<int32_t>(offsetof(AdvCommandSprite_t3266644903, ___spriteName_9)); }
	inline String_t* get_spriteName_9() const { return ___spriteName_9; }
	inline String_t** get_address_of_spriteName_9() { return &___spriteName_9; }
	inline void set_spriteName_9(String_t* value)
	{
		___spriteName_9 = value;
		Il2CppCodeGenWriteBarrier((&___spriteName_9), value);
	}

	inline static int32_t get_offset_of_fadeTime_10() { return static_cast<int32_t>(offsetof(AdvCommandSprite_t3266644903, ___fadeTime_10)); }
	inline float get_fadeTime_10() const { return ___fadeTime_10; }
	inline float* get_address_of_fadeTime_10() { return &___fadeTime_10; }
	inline void set_fadeTime_10(float value)
	{
		___fadeTime_10 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ADVCOMMANDSPRITE_T3266644903_H
#ifndef ADVCOMMANDPARTICLE_T2486419090_H
#define ADVCOMMANDPARTICLE_T2486419090_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.AdvCommandParticle
struct  AdvCommandParticle_t2486419090  : public AdvCommand_t2859960984
{
public:
	// System.String Utage.AdvCommandParticle::label
	String_t* ___label_7;
	// System.String Utage.AdvCommandParticle::layerName
	String_t* ___layerName_8;
	// Utage.AdvGraphicInfo Utage.AdvCommandParticle::graphic
	AdvGraphicInfo_t3545565645 * ___graphic_9;
	// Utage.AdvGraphicOperaitonArg Utage.AdvCommandParticle::graphicOperaitonArg
	AdvGraphicOperaitonArg_t632373120 * ___graphicOperaitonArg_10;

public:
	inline static int32_t get_offset_of_label_7() { return static_cast<int32_t>(offsetof(AdvCommandParticle_t2486419090, ___label_7)); }
	inline String_t* get_label_7() const { return ___label_7; }
	inline String_t** get_address_of_label_7() { return &___label_7; }
	inline void set_label_7(String_t* value)
	{
		___label_7 = value;
		Il2CppCodeGenWriteBarrier((&___label_7), value);
	}

	inline static int32_t get_offset_of_layerName_8() { return static_cast<int32_t>(offsetof(AdvCommandParticle_t2486419090, ___layerName_8)); }
	inline String_t* get_layerName_8() const { return ___layerName_8; }
	inline String_t** get_address_of_layerName_8() { return &___layerName_8; }
	inline void set_layerName_8(String_t* value)
	{
		___layerName_8 = value;
		Il2CppCodeGenWriteBarrier((&___layerName_8), value);
	}

	inline static int32_t get_offset_of_graphic_9() { return static_cast<int32_t>(offsetof(AdvCommandParticle_t2486419090, ___graphic_9)); }
	inline AdvGraphicInfo_t3545565645 * get_graphic_9() const { return ___graphic_9; }
	inline AdvGraphicInfo_t3545565645 ** get_address_of_graphic_9() { return &___graphic_9; }
	inline void set_graphic_9(AdvGraphicInfo_t3545565645 * value)
	{
		___graphic_9 = value;
		Il2CppCodeGenWriteBarrier((&___graphic_9), value);
	}

	inline static int32_t get_offset_of_graphicOperaitonArg_10() { return static_cast<int32_t>(offsetof(AdvCommandParticle_t2486419090, ___graphicOperaitonArg_10)); }
	inline AdvGraphicOperaitonArg_t632373120 * get_graphicOperaitonArg_10() const { return ___graphicOperaitonArg_10; }
	inline AdvGraphicOperaitonArg_t632373120 ** get_address_of_graphicOperaitonArg_10() { return &___graphicOperaitonArg_10; }
	inline void set_graphicOperaitonArg_10(AdvGraphicOperaitonArg_t632373120 * value)
	{
		___graphicOperaitonArg_10 = value;
		Il2CppCodeGenWriteBarrier((&___graphicOperaitonArg_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ADVCOMMANDPARTICLE_T2486419090_H
#ifndef ADVCOMMANDLAYERRESET_T698095516_H
#define ADVCOMMANDLAYERRESET_T698095516_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.AdvCommandLayerReset
struct  AdvCommandLayerReset_t698095516  : public AdvCommand_t2859960984
{
public:
	// System.String Utage.AdvCommandLayerReset::name
	String_t* ___name_7;

public:
	inline static int32_t get_offset_of_name_7() { return static_cast<int32_t>(offsetof(AdvCommandLayerReset_t698095516, ___name_7)); }
	inline String_t* get_name_7() const { return ___name_7; }
	inline String_t** get_address_of_name_7() { return &___name_7; }
	inline void set_name_7(String_t* value)
	{
		___name_7 = value;
		Il2CppCodeGenWriteBarrier((&___name_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ADVCOMMANDLAYERRESET_T698095516_H
#ifndef ADVCOMMANDSPRITEOFF_T3298929962_H
#define ADVCOMMANDSPRITEOFF_T3298929962_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.AdvCommandSpriteOff
struct  AdvCommandSpriteOff_t3298929962  : public AdvCommand_t2859960984
{
public:
	// System.String Utage.AdvCommandSpriteOff::name
	String_t* ___name_7;
	// System.Single Utage.AdvCommandSpriteOff::fadeTime
	float ___fadeTime_8;

public:
	inline static int32_t get_offset_of_name_7() { return static_cast<int32_t>(offsetof(AdvCommandSpriteOff_t3298929962, ___name_7)); }
	inline String_t* get_name_7() const { return ___name_7; }
	inline String_t** get_address_of_name_7() { return &___name_7; }
	inline void set_name_7(String_t* value)
	{
		___name_7 = value;
		Il2CppCodeGenWriteBarrier((&___name_7), value);
	}

	inline static int32_t get_offset_of_fadeTime_8() { return static_cast<int32_t>(offsetof(AdvCommandSpriteOff_t3298929962, ___fadeTime_8)); }
	inline float get_fadeTime_8() const { return ___fadeTime_8; }
	inline float* get_address_of_fadeTime_8() { return &___fadeTime_8; }
	inline void set_fadeTime_8(float value)
	{
		___fadeTime_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ADVCOMMANDSPRITEOFF_T3298929962_H
#ifndef ADVCOMMANDLAYEROFF_T1981748998_H
#define ADVCOMMANDLAYEROFF_T1981748998_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.AdvCommandLayerOff
struct  AdvCommandLayerOff_t1981748998  : public AdvCommand_t2859960984
{
public:
	// System.String Utage.AdvCommandLayerOff::name
	String_t* ___name_7;
	// System.Single Utage.AdvCommandLayerOff::fadeTime
	float ___fadeTime_8;

public:
	inline static int32_t get_offset_of_name_7() { return static_cast<int32_t>(offsetof(AdvCommandLayerOff_t1981748998, ___name_7)); }
	inline String_t* get_name_7() const { return ___name_7; }
	inline String_t** get_address_of_name_7() { return &___name_7; }
	inline void set_name_7(String_t* value)
	{
		___name_7 = value;
		Il2CppCodeGenWriteBarrier((&___name_7), value);
	}

	inline static int32_t get_offset_of_fadeTime_8() { return static_cast<int32_t>(offsetof(AdvCommandLayerOff_t1981748998, ___fadeTime_8)); }
	inline float get_fadeTime_8() const { return ___fadeTime_8; }
	inline float* get_address_of_fadeTime_8() { return &___fadeTime_8; }
	inline void set_fadeTime_8(float value)
	{
		___fadeTime_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ADVCOMMANDLAYEROFF_T1981748998_H
#ifndef ADVCOMMANDCHARACTEROFF_T3898132428_H
#define ADVCOMMANDCHARACTEROFF_T3898132428_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.AdvCommandCharacterOff
struct  AdvCommandCharacterOff_t3898132428  : public AdvCommand_t2859960984
{
public:
	// System.String Utage.AdvCommandCharacterOff::name
	String_t* ___name_7;
	// System.Single Utage.AdvCommandCharacterOff::time
	float ___time_8;

public:
	inline static int32_t get_offset_of_name_7() { return static_cast<int32_t>(offsetof(AdvCommandCharacterOff_t3898132428, ___name_7)); }
	inline String_t* get_name_7() const { return ___name_7; }
	inline String_t** get_address_of_name_7() { return &___name_7; }
	inline void set_name_7(String_t* value)
	{
		___name_7 = value;
		Il2CppCodeGenWriteBarrier((&___name_7), value);
	}

	inline static int32_t get_offset_of_time_8() { return static_cast<int32_t>(offsetof(AdvCommandCharacterOff_t3898132428, ___time_8)); }
	inline float get_time_8() const { return ___time_8; }
	inline float* get_address_of_time_8() { return &___time_8; }
	inline void set_time_8(float value)
	{
		___time_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ADVCOMMANDCHARACTEROFF_T3898132428_H
#ifndef ADVCOMMANDCHARACTER_T305489079_H
#define ADVCOMMANDCHARACTER_T305489079_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.AdvCommandCharacter
struct  AdvCommandCharacter_t305489079  : public AdvCommand_t2859960984
{
public:
	// Utage.AdvCharacterInfo Utage.AdvCommandCharacter::characterInfo
	AdvCharacterInfo_t1582765630 * ___characterInfo_7;
	// System.String Utage.AdvCommandCharacter::layerName
	String_t* ___layerName_8;
	// System.Single Utage.AdvCommandCharacter::fadeTime
	float ___fadeTime_9;

public:
	inline static int32_t get_offset_of_characterInfo_7() { return static_cast<int32_t>(offsetof(AdvCommandCharacter_t305489079, ___characterInfo_7)); }
	inline AdvCharacterInfo_t1582765630 * get_characterInfo_7() const { return ___characterInfo_7; }
	inline AdvCharacterInfo_t1582765630 ** get_address_of_characterInfo_7() { return &___characterInfo_7; }
	inline void set_characterInfo_7(AdvCharacterInfo_t1582765630 * value)
	{
		___characterInfo_7 = value;
		Il2CppCodeGenWriteBarrier((&___characterInfo_7), value);
	}

	inline static int32_t get_offset_of_layerName_8() { return static_cast<int32_t>(offsetof(AdvCommandCharacter_t305489079, ___layerName_8)); }
	inline String_t* get_layerName_8() const { return ___layerName_8; }
	inline String_t** get_address_of_layerName_8() { return &___layerName_8; }
	inline void set_layerName_8(String_t* value)
	{
		___layerName_8 = value;
		Il2CppCodeGenWriteBarrier((&___layerName_8), value);
	}

	inline static int32_t get_offset_of_fadeTime_9() { return static_cast<int32_t>(offsetof(AdvCommandCharacter_t305489079, ___fadeTime_9)); }
	inline float get_fadeTime_9() const { return ___fadeTime_9; }
	inline float* get_address_of_fadeTime_9() { return &___fadeTime_9; }
	inline void set_fadeTime_9(float value)
	{
		___fadeTime_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ADVCOMMANDCHARACTER_T305489079_H
#ifndef ADVCOMMANDBGOFF_T856493800_H
#define ADVCOMMANDBGOFF_T856493800_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.AdvCommandBgOff
struct  AdvCommandBgOff_t856493800  : public AdvCommand_t2859960984
{
public:
	// System.Single Utage.AdvCommandBgOff::fadeTime
	float ___fadeTime_7;

public:
	inline static int32_t get_offset_of_fadeTime_7() { return static_cast<int32_t>(offsetof(AdvCommandBgOff_t856493800, ___fadeTime_7)); }
	inline float get_fadeTime_7() const { return ___fadeTime_7; }
	inline float* get_address_of_fadeTime_7() { return &___fadeTime_7; }
	inline void set_fadeTime_7(float value)
	{
		___fadeTime_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ADVCOMMANDBGOFF_T856493800_H
#ifndef ADVCOMMANDBGEVENTOFF_T719799962_H
#define ADVCOMMANDBGEVENTOFF_T719799962_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.AdvCommandBgEventOff
struct  AdvCommandBgEventOff_t719799962  : public AdvCommand_t2859960984
{
public:
	// System.Single Utage.AdvCommandBgEventOff::fadeTime
	float ___fadeTime_7;

public:
	inline static int32_t get_offset_of_fadeTime_7() { return static_cast<int32_t>(offsetof(AdvCommandBgEventOff_t719799962, ___fadeTime_7)); }
	inline float get_fadeTime_7() const { return ___fadeTime_7; }
	inline float* get_address_of_fadeTime_7() { return &___fadeTime_7; }
	inline void set_fadeTime_7(float value)
	{
		___fadeTime_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ADVCOMMANDBGEVENTOFF_T719799962_H
#ifndef ADVCOMMANDIF_T3367233681_H
#define ADVCOMMANDIF_T3367233681_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.AdvCommandIf
struct  AdvCommandIf_t3367233681  : public AdvCommand_t2859960984
{
public:
	// Utage.ExpressionParser Utage.AdvCommandIf::exp
	ExpressionParser_t665800307 * ___exp_7;

public:
	inline static int32_t get_offset_of_exp_7() { return static_cast<int32_t>(offsetof(AdvCommandIf_t3367233681, ___exp_7)); }
	inline ExpressionParser_t665800307 * get_exp_7() const { return ___exp_7; }
	inline ExpressionParser_t665800307 ** get_address_of_exp_7() { return &___exp_7; }
	inline void set_exp_7(ExpressionParser_t665800307 * value)
	{
		___exp_7 = value;
		Il2CppCodeGenWriteBarrier((&___exp_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ADVCOMMANDIF_T3367233681_H
#ifndef ADVCOMMANDPARTICLEOFF_T3489428065_H
#define ADVCOMMANDPARTICLEOFF_T3489428065_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.AdvCommandParticleOff
struct  AdvCommandParticleOff_t3489428065  : public AdvCommand_t2859960984
{
public:
	// System.String Utage.AdvCommandParticleOff::name
	String_t* ___name_7;

public:
	inline static int32_t get_offset_of_name_7() { return static_cast<int32_t>(offsetof(AdvCommandParticleOff_t3489428065, ___name_7)); }
	inline String_t* get_name_7() const { return ___name_7; }
	inline String_t** get_address_of_name_7() { return &___name_7; }
	inline void set_name_7(String_t* value)
	{
		___name_7 = value;
		Il2CppCodeGenWriteBarrier((&___name_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ADVCOMMANDPARTICLEOFF_T3489428065_H
#ifndef ADVCOMMANDPARAM_T1058287753_H
#define ADVCOMMANDPARAM_T1058287753_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.AdvCommandParam
struct  AdvCommandParam_t1058287753  : public AdvCommand_t2859960984
{
public:
	// Utage.ExpressionParser Utage.AdvCommandParam::exp
	ExpressionParser_t665800307 * ___exp_7;

public:
	inline static int32_t get_offset_of_exp_7() { return static_cast<int32_t>(offsetof(AdvCommandParam_t1058287753, ___exp_7)); }
	inline ExpressionParser_t665800307 * get_exp_7() const { return ___exp_7; }
	inline ExpressionParser_t665800307 ** get_address_of_exp_7() { return &___exp_7; }
	inline void set_exp_7(ExpressionParser_t665800307 * value)
	{
		___exp_7 = value;
		Il2CppCodeGenWriteBarrier((&___exp_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ADVCOMMANDPARAM_T1058287753_H
#ifndef ADVCOMMANDPAUSESCENARIO_T2854705820_H
#define ADVCOMMANDPAUSESCENARIO_T2854705820_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.AdvCommandPauseScenario
struct  AdvCommandPauseScenario_t2854705820  : public AdvCommand_t2859960984
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ADVCOMMANDPAUSESCENARIO_T2854705820_H
#ifndef ADVCOMMANDERROR_T2072998418_H
#define ADVCOMMANDERROR_T2072998418_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.AdvCommandError
struct  AdvCommandError_t2072998418  : public AdvCommand_t2859960984
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ADVCOMMANDERROR_T2072998418_H
#ifndef ADVCOMMANDSELECTIONCLICK_T477933170_H
#define ADVCOMMANDSELECTIONCLICK_T477933170_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.AdvCommandSelectionClick
struct  AdvCommandSelectionClick_t477933170  : public AdvCommand_t2859960984
{
public:
	// System.String Utage.AdvCommandSelectionClick::name
	String_t* ___name_7;
	// System.Boolean Utage.AdvCommandSelectionClick::isPolygon
	bool ___isPolygon_8;
	// System.String Utage.AdvCommandSelectionClick::jumpLabel
	String_t* ___jumpLabel_9;
	// Utage.ExpressionParser Utage.AdvCommandSelectionClick::exp
	ExpressionParser_t665800307 * ___exp_10;
	// Utage.ExpressionParser Utage.AdvCommandSelectionClick::selectedExp
	ExpressionParser_t665800307 * ___selectedExp_11;

public:
	inline static int32_t get_offset_of_name_7() { return static_cast<int32_t>(offsetof(AdvCommandSelectionClick_t477933170, ___name_7)); }
	inline String_t* get_name_7() const { return ___name_7; }
	inline String_t** get_address_of_name_7() { return &___name_7; }
	inline void set_name_7(String_t* value)
	{
		___name_7 = value;
		Il2CppCodeGenWriteBarrier((&___name_7), value);
	}

	inline static int32_t get_offset_of_isPolygon_8() { return static_cast<int32_t>(offsetof(AdvCommandSelectionClick_t477933170, ___isPolygon_8)); }
	inline bool get_isPolygon_8() const { return ___isPolygon_8; }
	inline bool* get_address_of_isPolygon_8() { return &___isPolygon_8; }
	inline void set_isPolygon_8(bool value)
	{
		___isPolygon_8 = value;
	}

	inline static int32_t get_offset_of_jumpLabel_9() { return static_cast<int32_t>(offsetof(AdvCommandSelectionClick_t477933170, ___jumpLabel_9)); }
	inline String_t* get_jumpLabel_9() const { return ___jumpLabel_9; }
	inline String_t** get_address_of_jumpLabel_9() { return &___jumpLabel_9; }
	inline void set_jumpLabel_9(String_t* value)
	{
		___jumpLabel_9 = value;
		Il2CppCodeGenWriteBarrier((&___jumpLabel_9), value);
	}

	inline static int32_t get_offset_of_exp_10() { return static_cast<int32_t>(offsetof(AdvCommandSelectionClick_t477933170, ___exp_10)); }
	inline ExpressionParser_t665800307 * get_exp_10() const { return ___exp_10; }
	inline ExpressionParser_t665800307 ** get_address_of_exp_10() { return &___exp_10; }
	inline void set_exp_10(ExpressionParser_t665800307 * value)
	{
		___exp_10 = value;
		Il2CppCodeGenWriteBarrier((&___exp_10), value);
	}

	inline static int32_t get_offset_of_selectedExp_11() { return static_cast<int32_t>(offsetof(AdvCommandSelectionClick_t477933170, ___selectedExp_11)); }
	inline ExpressionParser_t665800307 * get_selectedExp_11() const { return ___selectedExp_11; }
	inline ExpressionParser_t665800307 ** get_address_of_selectedExp_11() { return &___selectedExp_11; }
	inline void set_selectedExp_11(ExpressionParser_t665800307 * value)
	{
		___selectedExp_11 = value;
		Il2CppCodeGenWriteBarrier((&___selectedExp_11), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ADVCOMMANDSELECTIONCLICK_T477933170_H
#ifndef ADVCOMMANDSELECTIONCLICKEND_T1108642495_H
#define ADVCOMMANDSELECTIONCLICKEND_T1108642495_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.AdvCommandSelectionClickEnd
struct  AdvCommandSelectionClickEnd_t1108642495  : public AdvCommand_t2859960984
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ADVCOMMANDSELECTIONCLICKEND_T1108642495_H
#ifndef ADVCOMMANDSELECTIONEND_T1999221425_H
#define ADVCOMMANDSELECTIONEND_T1999221425_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.AdvCommandSelectionEnd
struct  AdvCommandSelectionEnd_t1999221425  : public AdvCommand_t2859960984
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ADVCOMMANDSELECTIONEND_T1999221425_H
#ifndef ADVCOMMANDAMBIENCE_T767776376_H
#define ADVCOMMANDAMBIENCE_T767776376_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.AdvCommandAmbience
struct  AdvCommandAmbience_t767776376  : public AdvCommand_t2859960984
{
public:
	// Utage.AssetFile Utage.AdvCommandAmbience::file
	RuntimeObject* ___file_7;
	// System.Single Utage.AdvCommandAmbience::volume
	float ___volume_8;
	// System.Boolean Utage.AdvCommandAmbience::isLoop
	bool ___isLoop_9;
	// System.Single Utage.AdvCommandAmbience::fadeInTime
	float ___fadeInTime_10;
	// System.Single Utage.AdvCommandAmbience::fadeOutTime
	float ___fadeOutTime_11;

public:
	inline static int32_t get_offset_of_file_7() { return static_cast<int32_t>(offsetof(AdvCommandAmbience_t767776376, ___file_7)); }
	inline RuntimeObject* get_file_7() const { return ___file_7; }
	inline RuntimeObject** get_address_of_file_7() { return &___file_7; }
	inline void set_file_7(RuntimeObject* value)
	{
		___file_7 = value;
		Il2CppCodeGenWriteBarrier((&___file_7), value);
	}

	inline static int32_t get_offset_of_volume_8() { return static_cast<int32_t>(offsetof(AdvCommandAmbience_t767776376, ___volume_8)); }
	inline float get_volume_8() const { return ___volume_8; }
	inline float* get_address_of_volume_8() { return &___volume_8; }
	inline void set_volume_8(float value)
	{
		___volume_8 = value;
	}

	inline static int32_t get_offset_of_isLoop_9() { return static_cast<int32_t>(offsetof(AdvCommandAmbience_t767776376, ___isLoop_9)); }
	inline bool get_isLoop_9() const { return ___isLoop_9; }
	inline bool* get_address_of_isLoop_9() { return &___isLoop_9; }
	inline void set_isLoop_9(bool value)
	{
		___isLoop_9 = value;
	}

	inline static int32_t get_offset_of_fadeInTime_10() { return static_cast<int32_t>(offsetof(AdvCommandAmbience_t767776376, ___fadeInTime_10)); }
	inline float get_fadeInTime_10() const { return ___fadeInTime_10; }
	inline float* get_address_of_fadeInTime_10() { return &___fadeInTime_10; }
	inline void set_fadeInTime_10(float value)
	{
		___fadeInTime_10 = value;
	}

	inline static int32_t get_offset_of_fadeOutTime_11() { return static_cast<int32_t>(offsetof(AdvCommandAmbience_t767776376, ___fadeOutTime_11)); }
	inline float get_fadeOutTime_11() const { return ___fadeOutTime_11; }
	inline float* get_address_of_fadeOutTime_11() { return &___fadeOutTime_11; }
	inline void set_fadeOutTime_11(float value)
	{
		___fadeOutTime_11 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ADVCOMMANDAMBIENCE_T767776376_H
#ifndef ADVCOMMANDJUMPSUBROUTINE_T3620058494_H
#define ADVCOMMANDJUMPSUBROUTINE_T3620058494_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.AdvCommandJumpSubroutine
struct  AdvCommandJumpSubroutine_t3620058494  : public AdvCommand_t2859960984
{
public:
	// System.String Utage.AdvCommandJumpSubroutine::scenarioLabel
	String_t* ___scenarioLabel_7;
	// System.Int32 Utage.AdvCommandJumpSubroutine::subroutineCommandIndex
	int32_t ___subroutineCommandIndex_8;
	// System.String Utage.AdvCommandJumpSubroutine::jumpLabel
	String_t* ___jumpLabel_9;
	// System.String Utage.AdvCommandJumpSubroutine::returnLabel
	String_t* ___returnLabel_10;
	// Utage.ExpressionParser Utage.AdvCommandJumpSubroutine::exp
	ExpressionParser_t665800307 * ___exp_11;

public:
	inline static int32_t get_offset_of_scenarioLabel_7() { return static_cast<int32_t>(offsetof(AdvCommandJumpSubroutine_t3620058494, ___scenarioLabel_7)); }
	inline String_t* get_scenarioLabel_7() const { return ___scenarioLabel_7; }
	inline String_t** get_address_of_scenarioLabel_7() { return &___scenarioLabel_7; }
	inline void set_scenarioLabel_7(String_t* value)
	{
		___scenarioLabel_7 = value;
		Il2CppCodeGenWriteBarrier((&___scenarioLabel_7), value);
	}

	inline static int32_t get_offset_of_subroutineCommandIndex_8() { return static_cast<int32_t>(offsetof(AdvCommandJumpSubroutine_t3620058494, ___subroutineCommandIndex_8)); }
	inline int32_t get_subroutineCommandIndex_8() const { return ___subroutineCommandIndex_8; }
	inline int32_t* get_address_of_subroutineCommandIndex_8() { return &___subroutineCommandIndex_8; }
	inline void set_subroutineCommandIndex_8(int32_t value)
	{
		___subroutineCommandIndex_8 = value;
	}

	inline static int32_t get_offset_of_jumpLabel_9() { return static_cast<int32_t>(offsetof(AdvCommandJumpSubroutine_t3620058494, ___jumpLabel_9)); }
	inline String_t* get_jumpLabel_9() const { return ___jumpLabel_9; }
	inline String_t** get_address_of_jumpLabel_9() { return &___jumpLabel_9; }
	inline void set_jumpLabel_9(String_t* value)
	{
		___jumpLabel_9 = value;
		Il2CppCodeGenWriteBarrier((&___jumpLabel_9), value);
	}

	inline static int32_t get_offset_of_returnLabel_10() { return static_cast<int32_t>(offsetof(AdvCommandJumpSubroutine_t3620058494, ___returnLabel_10)); }
	inline String_t* get_returnLabel_10() const { return ___returnLabel_10; }
	inline String_t** get_address_of_returnLabel_10() { return &___returnLabel_10; }
	inline void set_returnLabel_10(String_t* value)
	{
		___returnLabel_10 = value;
		Il2CppCodeGenWriteBarrier((&___returnLabel_10), value);
	}

	inline static int32_t get_offset_of_exp_11() { return static_cast<int32_t>(offsetof(AdvCommandJumpSubroutine_t3620058494, ___exp_11)); }
	inline ExpressionParser_t665800307 * get_exp_11() const { return ___exp_11; }
	inline ExpressionParser_t665800307 ** get_address_of_exp_11() { return &___exp_11; }
	inline void set_exp_11(ExpressionParser_t665800307 * value)
	{
		___exp_11 = value;
		Il2CppCodeGenWriteBarrier((&___exp_11), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ADVCOMMANDJUMPSUBROUTINE_T3620058494_H
#ifndef ADVCOMMANDJUMP_T3753877016_H
#define ADVCOMMANDJUMP_T3753877016_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.AdvCommandJump
struct  AdvCommandJump_t3753877016  : public AdvCommand_t2859960984
{
public:
	// System.String Utage.AdvCommandJump::jumpLabel
	String_t* ___jumpLabel_7;
	// Utage.ExpressionParser Utage.AdvCommandJump::exp
	ExpressionParser_t665800307 * ___exp_8;

public:
	inline static int32_t get_offset_of_jumpLabel_7() { return static_cast<int32_t>(offsetof(AdvCommandJump_t3753877016, ___jumpLabel_7)); }
	inline String_t* get_jumpLabel_7() const { return ___jumpLabel_7; }
	inline String_t** get_address_of_jumpLabel_7() { return &___jumpLabel_7; }
	inline void set_jumpLabel_7(String_t* value)
	{
		___jumpLabel_7 = value;
		Il2CppCodeGenWriteBarrier((&___jumpLabel_7), value);
	}

	inline static int32_t get_offset_of_exp_8() { return static_cast<int32_t>(offsetof(AdvCommandJump_t3753877016, ___exp_8)); }
	inline ExpressionParser_t665800307 * get_exp_8() const { return ___exp_8; }
	inline ExpressionParser_t665800307 ** get_address_of_exp_8() { return &___exp_8; }
	inline void set_exp_8(ExpressionParser_t665800307 * value)
	{
		___exp_8 = value;
		Il2CppCodeGenWriteBarrier((&___exp_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ADVCOMMANDJUMP_T3753877016_H
#ifndef ADVCOMMANDJUMPSUBROUTINERANDOMEND_T1852355654_H
#define ADVCOMMANDJUMPSUBROUTINERANDOMEND_T1852355654_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.AdvCommandJumpSubroutineRandomEnd
struct  AdvCommandJumpSubroutineRandomEnd_t1852355654  : public AdvCommand_t2859960984
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ADVCOMMANDJUMPSUBROUTINERANDOMEND_T1852355654_H
#ifndef ADVCOMMANDJUMPSUBROUTINERANDOM_T1026002121_H
#define ADVCOMMANDJUMPSUBROUTINERANDOM_T1026002121_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.AdvCommandJumpSubroutineRandom
struct  AdvCommandJumpSubroutineRandom_t1026002121  : public AdvCommand_t2859960984
{
public:
	// System.String Utage.AdvCommandJumpSubroutineRandom::scenarioLabel
	String_t* ___scenarioLabel_7;
	// System.Int32 Utage.AdvCommandJumpSubroutineRandom::subroutineCommandIndex
	int32_t ___subroutineCommandIndex_8;
	// System.String Utage.AdvCommandJumpSubroutineRandom::jumpLabel
	String_t* ___jumpLabel_9;
	// System.String Utage.AdvCommandJumpSubroutineRandom::returnLabel
	String_t* ___returnLabel_10;
	// Utage.ExpressionParser Utage.AdvCommandJumpSubroutineRandom::exp
	ExpressionParser_t665800307 * ___exp_11;
	// Utage.ExpressionParser Utage.AdvCommandJumpSubroutineRandom::expRate
	ExpressionParser_t665800307 * ___expRate_12;

public:
	inline static int32_t get_offset_of_scenarioLabel_7() { return static_cast<int32_t>(offsetof(AdvCommandJumpSubroutineRandom_t1026002121, ___scenarioLabel_7)); }
	inline String_t* get_scenarioLabel_7() const { return ___scenarioLabel_7; }
	inline String_t** get_address_of_scenarioLabel_7() { return &___scenarioLabel_7; }
	inline void set_scenarioLabel_7(String_t* value)
	{
		___scenarioLabel_7 = value;
		Il2CppCodeGenWriteBarrier((&___scenarioLabel_7), value);
	}

	inline static int32_t get_offset_of_subroutineCommandIndex_8() { return static_cast<int32_t>(offsetof(AdvCommandJumpSubroutineRandom_t1026002121, ___subroutineCommandIndex_8)); }
	inline int32_t get_subroutineCommandIndex_8() const { return ___subroutineCommandIndex_8; }
	inline int32_t* get_address_of_subroutineCommandIndex_8() { return &___subroutineCommandIndex_8; }
	inline void set_subroutineCommandIndex_8(int32_t value)
	{
		___subroutineCommandIndex_8 = value;
	}

	inline static int32_t get_offset_of_jumpLabel_9() { return static_cast<int32_t>(offsetof(AdvCommandJumpSubroutineRandom_t1026002121, ___jumpLabel_9)); }
	inline String_t* get_jumpLabel_9() const { return ___jumpLabel_9; }
	inline String_t** get_address_of_jumpLabel_9() { return &___jumpLabel_9; }
	inline void set_jumpLabel_9(String_t* value)
	{
		___jumpLabel_9 = value;
		Il2CppCodeGenWriteBarrier((&___jumpLabel_9), value);
	}

	inline static int32_t get_offset_of_returnLabel_10() { return static_cast<int32_t>(offsetof(AdvCommandJumpSubroutineRandom_t1026002121, ___returnLabel_10)); }
	inline String_t* get_returnLabel_10() const { return ___returnLabel_10; }
	inline String_t** get_address_of_returnLabel_10() { return &___returnLabel_10; }
	inline void set_returnLabel_10(String_t* value)
	{
		___returnLabel_10 = value;
		Il2CppCodeGenWriteBarrier((&___returnLabel_10), value);
	}

	inline static int32_t get_offset_of_exp_11() { return static_cast<int32_t>(offsetof(AdvCommandJumpSubroutineRandom_t1026002121, ___exp_11)); }
	inline ExpressionParser_t665800307 * get_exp_11() const { return ___exp_11; }
	inline ExpressionParser_t665800307 ** get_address_of_exp_11() { return &___exp_11; }
	inline void set_exp_11(ExpressionParser_t665800307 * value)
	{
		___exp_11 = value;
		Il2CppCodeGenWriteBarrier((&___exp_11), value);
	}

	inline static int32_t get_offset_of_expRate_12() { return static_cast<int32_t>(offsetof(AdvCommandJumpSubroutineRandom_t1026002121, ___expRate_12)); }
	inline ExpressionParser_t665800307 * get_expRate_12() const { return ___expRate_12; }
	inline ExpressionParser_t665800307 ** get_address_of_expRate_12() { return &___expRate_12; }
	inline void set_expRate_12(ExpressionParser_t665800307 * value)
	{
		___expRate_12 = value;
		Il2CppCodeGenWriteBarrier((&___expRate_12), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ADVCOMMANDJUMPSUBROUTINERANDOM_T1026002121_H
#ifndef ADVCOMMANDJUMPRANDOM_T3877961971_H
#define ADVCOMMANDJUMPRANDOM_T3877961971_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.AdvCommandJumpRandom
struct  AdvCommandJumpRandom_t3877961971  : public AdvCommand_t2859960984
{
public:
	// System.String Utage.AdvCommandJumpRandom::jumpLabel
	String_t* ___jumpLabel_7;
	// Utage.ExpressionParser Utage.AdvCommandJumpRandom::exp
	ExpressionParser_t665800307 * ___exp_8;
	// Utage.ExpressionParser Utage.AdvCommandJumpRandom::expRate
	ExpressionParser_t665800307 * ___expRate_9;

public:
	inline static int32_t get_offset_of_jumpLabel_7() { return static_cast<int32_t>(offsetof(AdvCommandJumpRandom_t3877961971, ___jumpLabel_7)); }
	inline String_t* get_jumpLabel_7() const { return ___jumpLabel_7; }
	inline String_t** get_address_of_jumpLabel_7() { return &___jumpLabel_7; }
	inline void set_jumpLabel_7(String_t* value)
	{
		___jumpLabel_7 = value;
		Il2CppCodeGenWriteBarrier((&___jumpLabel_7), value);
	}

	inline static int32_t get_offset_of_exp_8() { return static_cast<int32_t>(offsetof(AdvCommandJumpRandom_t3877961971, ___exp_8)); }
	inline ExpressionParser_t665800307 * get_exp_8() const { return ___exp_8; }
	inline ExpressionParser_t665800307 ** get_address_of_exp_8() { return &___exp_8; }
	inline void set_exp_8(ExpressionParser_t665800307 * value)
	{
		___exp_8 = value;
		Il2CppCodeGenWriteBarrier((&___exp_8), value);
	}

	inline static int32_t get_offset_of_expRate_9() { return static_cast<int32_t>(offsetof(AdvCommandJumpRandom_t3877961971, ___expRate_9)); }
	inline ExpressionParser_t665800307 * get_expRate_9() const { return ___expRate_9; }
	inline ExpressionParser_t665800307 ** get_address_of_expRate_9() { return &___expRate_9; }
	inline void set_expRate_9(ExpressionParser_t665800307 * value)
	{
		___expRate_9 = value;
		Il2CppCodeGenWriteBarrier((&___expRate_9), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ADVCOMMANDJUMPRANDOM_T3877961971_H
#ifndef ADVCOMMANDJUMPRANDOMEND_T2675963892_H
#define ADVCOMMANDJUMPRANDOMEND_T2675963892_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.AdvCommandJumpRandomEnd
struct  AdvCommandJumpRandomEnd_t2675963892  : public AdvCommand_t2859960984
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ADVCOMMANDJUMPRANDOMEND_T2675963892_H
#ifndef ADVCOMMANDBG_T1801149747_H
#define ADVCOMMANDBG_T1801149747_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.AdvCommandBg
struct  AdvCommandBg_t1801149747  : public AdvCommand_t2859960984
{
public:
	// Utage.AdvGraphicInfoList Utage.AdvCommandBg::graphic
	AdvGraphicInfoList_t3537398639 * ___graphic_7;
	// System.String Utage.AdvCommandBg::layerName
	String_t* ___layerName_8;
	// System.Single Utage.AdvCommandBg::fadeTime
	float ___fadeTime_9;

public:
	inline static int32_t get_offset_of_graphic_7() { return static_cast<int32_t>(offsetof(AdvCommandBg_t1801149747, ___graphic_7)); }
	inline AdvGraphicInfoList_t3537398639 * get_graphic_7() const { return ___graphic_7; }
	inline AdvGraphicInfoList_t3537398639 ** get_address_of_graphic_7() { return &___graphic_7; }
	inline void set_graphic_7(AdvGraphicInfoList_t3537398639 * value)
	{
		___graphic_7 = value;
		Il2CppCodeGenWriteBarrier((&___graphic_7), value);
	}

	inline static int32_t get_offset_of_layerName_8() { return static_cast<int32_t>(offsetof(AdvCommandBg_t1801149747, ___layerName_8)); }
	inline String_t* get_layerName_8() const { return ___layerName_8; }
	inline String_t** get_address_of_layerName_8() { return &___layerName_8; }
	inline void set_layerName_8(String_t* value)
	{
		___layerName_8 = value;
		Il2CppCodeGenWriteBarrier((&___layerName_8), value);
	}

	inline static int32_t get_offset_of_fadeTime_9() { return static_cast<int32_t>(offsetof(AdvCommandBg_t1801149747, ___fadeTime_9)); }
	inline float get_fadeTime_9() const { return ___fadeTime_9; }
	inline float* get_address_of_fadeTime_9() { return &___fadeTime_9; }
	inline void set_fadeTime_9(float value)
	{
		___fadeTime_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ADVCOMMANDBG_T1801149747_H
#ifndef TARGETTYPE_T2482913595_H
#define TARGETTYPE_T2482913595_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.AdvEffectManager/TargetType
struct  TargetType_t2482913595 
{
public:
	// System.Int32 Utage.AdvEffectManager/TargetType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(TargetType_t2482913595, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TARGETTYPE_T2482913595_H
#ifndef ADVCOMMANDWAITTYPE_T3266086913_H
#define ADVCOMMANDWAITTYPE_T3266086913_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.AdvCommandWaitType
struct  AdvCommandWaitType_t3266086913 
{
public:
	// System.Int32 Utage.AdvCommandWaitType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(AdvCommandWaitType_t3266086913, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ADVCOMMANDWAITTYPE_T3266086913_H
#ifndef U3CLOADCHAPTERSASYNCU3EC__ITERATOR6_T953487478_H
#define U3CLOADCHAPTERSASYNCU3EC__ITERATOR6_T953487478_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.AdvEngineStarter/<LoadChaptersAsync>c__Iterator6
struct  U3CLoadChaptersAsyncU3Ec__Iterator6_t953487478  : public RuntimeObject
{
public:
	// Utage.AdvImportScenarios Utage.AdvEngineStarter/<LoadChaptersAsync>c__Iterator6::<scenarios>__0
	AdvImportScenarios_t1226385437 * ___U3CscenariosU3E__0_0;
	// System.Collections.Generic.List`1/Enumerator<System.String> Utage.AdvEngineStarter/<LoadChaptersAsync>c__Iterator6::$locvar0
	Enumerator_t933071039  ___U24locvar0_1;
	// System.String Utage.AdvEngineStarter/<LoadChaptersAsync>c__Iterator6::<chapterName>__1
	String_t* ___U3CchapterNameU3E__1_2;
	// System.String Utage.AdvEngineStarter/<LoadChaptersAsync>c__Iterator6::rootDir
	String_t* ___rootDir_3;
	// System.String Utage.AdvEngineStarter/<LoadChaptersAsync>c__Iterator6::<url>__2
	String_t* ___U3CurlU3E__2_4;
	// Utage.AssetFile Utage.AdvEngineStarter/<LoadChaptersAsync>c__Iterator6::<file>__2
	RuntimeObject* ___U3CfileU3E__2_5;
	// Utage.AdvChapterData Utage.AdvEngineStarter/<LoadChaptersAsync>c__Iterator6::<chapter>__2
	AdvChapterData_t685691324 * ___U3CchapterU3E__2_6;
	// Utage.AdvEngineStarter Utage.AdvEngineStarter/<LoadChaptersAsync>c__Iterator6::$this
	AdvEngineStarter_t330044300 * ___U24this_7;
	// System.Object Utage.AdvEngineStarter/<LoadChaptersAsync>c__Iterator6::$current
	RuntimeObject * ___U24current_8;
	// System.Boolean Utage.AdvEngineStarter/<LoadChaptersAsync>c__Iterator6::$disposing
	bool ___U24disposing_9;
	// System.Int32 Utage.AdvEngineStarter/<LoadChaptersAsync>c__Iterator6::$PC
	int32_t ___U24PC_10;

public:
	inline static int32_t get_offset_of_U3CscenariosU3E__0_0() { return static_cast<int32_t>(offsetof(U3CLoadChaptersAsyncU3Ec__Iterator6_t953487478, ___U3CscenariosU3E__0_0)); }
	inline AdvImportScenarios_t1226385437 * get_U3CscenariosU3E__0_0() const { return ___U3CscenariosU3E__0_0; }
	inline AdvImportScenarios_t1226385437 ** get_address_of_U3CscenariosU3E__0_0() { return &___U3CscenariosU3E__0_0; }
	inline void set_U3CscenariosU3E__0_0(AdvImportScenarios_t1226385437 * value)
	{
		___U3CscenariosU3E__0_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CscenariosU3E__0_0), value);
	}

	inline static int32_t get_offset_of_U24locvar0_1() { return static_cast<int32_t>(offsetof(U3CLoadChaptersAsyncU3Ec__Iterator6_t953487478, ___U24locvar0_1)); }
	inline Enumerator_t933071039  get_U24locvar0_1() const { return ___U24locvar0_1; }
	inline Enumerator_t933071039 * get_address_of_U24locvar0_1() { return &___U24locvar0_1; }
	inline void set_U24locvar0_1(Enumerator_t933071039  value)
	{
		___U24locvar0_1 = value;
	}

	inline static int32_t get_offset_of_U3CchapterNameU3E__1_2() { return static_cast<int32_t>(offsetof(U3CLoadChaptersAsyncU3Ec__Iterator6_t953487478, ___U3CchapterNameU3E__1_2)); }
	inline String_t* get_U3CchapterNameU3E__1_2() const { return ___U3CchapterNameU3E__1_2; }
	inline String_t** get_address_of_U3CchapterNameU3E__1_2() { return &___U3CchapterNameU3E__1_2; }
	inline void set_U3CchapterNameU3E__1_2(String_t* value)
	{
		___U3CchapterNameU3E__1_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CchapterNameU3E__1_2), value);
	}

	inline static int32_t get_offset_of_rootDir_3() { return static_cast<int32_t>(offsetof(U3CLoadChaptersAsyncU3Ec__Iterator6_t953487478, ___rootDir_3)); }
	inline String_t* get_rootDir_3() const { return ___rootDir_3; }
	inline String_t** get_address_of_rootDir_3() { return &___rootDir_3; }
	inline void set_rootDir_3(String_t* value)
	{
		___rootDir_3 = value;
		Il2CppCodeGenWriteBarrier((&___rootDir_3), value);
	}

	inline static int32_t get_offset_of_U3CurlU3E__2_4() { return static_cast<int32_t>(offsetof(U3CLoadChaptersAsyncU3Ec__Iterator6_t953487478, ___U3CurlU3E__2_4)); }
	inline String_t* get_U3CurlU3E__2_4() const { return ___U3CurlU3E__2_4; }
	inline String_t** get_address_of_U3CurlU3E__2_4() { return &___U3CurlU3E__2_4; }
	inline void set_U3CurlU3E__2_4(String_t* value)
	{
		___U3CurlU3E__2_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CurlU3E__2_4), value);
	}

	inline static int32_t get_offset_of_U3CfileU3E__2_5() { return static_cast<int32_t>(offsetof(U3CLoadChaptersAsyncU3Ec__Iterator6_t953487478, ___U3CfileU3E__2_5)); }
	inline RuntimeObject* get_U3CfileU3E__2_5() const { return ___U3CfileU3E__2_5; }
	inline RuntimeObject** get_address_of_U3CfileU3E__2_5() { return &___U3CfileU3E__2_5; }
	inline void set_U3CfileU3E__2_5(RuntimeObject* value)
	{
		___U3CfileU3E__2_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3CfileU3E__2_5), value);
	}

	inline static int32_t get_offset_of_U3CchapterU3E__2_6() { return static_cast<int32_t>(offsetof(U3CLoadChaptersAsyncU3Ec__Iterator6_t953487478, ___U3CchapterU3E__2_6)); }
	inline AdvChapterData_t685691324 * get_U3CchapterU3E__2_6() const { return ___U3CchapterU3E__2_6; }
	inline AdvChapterData_t685691324 ** get_address_of_U3CchapterU3E__2_6() { return &___U3CchapterU3E__2_6; }
	inline void set_U3CchapterU3E__2_6(AdvChapterData_t685691324 * value)
	{
		___U3CchapterU3E__2_6 = value;
		Il2CppCodeGenWriteBarrier((&___U3CchapterU3E__2_6), value);
	}

	inline static int32_t get_offset_of_U24this_7() { return static_cast<int32_t>(offsetof(U3CLoadChaptersAsyncU3Ec__Iterator6_t953487478, ___U24this_7)); }
	inline AdvEngineStarter_t330044300 * get_U24this_7() const { return ___U24this_7; }
	inline AdvEngineStarter_t330044300 ** get_address_of_U24this_7() { return &___U24this_7; }
	inline void set_U24this_7(AdvEngineStarter_t330044300 * value)
	{
		___U24this_7 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_7), value);
	}

	inline static int32_t get_offset_of_U24current_8() { return static_cast<int32_t>(offsetof(U3CLoadChaptersAsyncU3Ec__Iterator6_t953487478, ___U24current_8)); }
	inline RuntimeObject * get_U24current_8() const { return ___U24current_8; }
	inline RuntimeObject ** get_address_of_U24current_8() { return &___U24current_8; }
	inline void set_U24current_8(RuntimeObject * value)
	{
		___U24current_8 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_8), value);
	}

	inline static int32_t get_offset_of_U24disposing_9() { return static_cast<int32_t>(offsetof(U3CLoadChaptersAsyncU3Ec__Iterator6_t953487478, ___U24disposing_9)); }
	inline bool get_U24disposing_9() const { return ___U24disposing_9; }
	inline bool* get_address_of_U24disposing_9() { return &___U24disposing_9; }
	inline void set_U24disposing_9(bool value)
	{
		___U24disposing_9 = value;
	}

	inline static int32_t get_offset_of_U24PC_10() { return static_cast<int32_t>(offsetof(U3CLoadChaptersAsyncU3Ec__Iterator6_t953487478, ___U24PC_10)); }
	inline int32_t get_U24PC_10() const { return ___U24PC_10; }
	inline int32_t* get_address_of_U24PC_10() { return &___U24PC_10; }
	inline void set_U24PC_10(int32_t value)
	{
		___U24PC_10 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CLOADCHAPTERSASYNCU3EC__ITERATOR6_T953487478_H
#ifndef U3CONSTARTEFFECTU3EC__ANONSTOREY0_T2035118920_H
#define U3CONSTARTEFFECTU3EC__ANONSTOREY0_T2035118920_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.AdvCommandZoomCamera/<OnStartEffect>c__AnonStorey0
struct  U3COnStartEffectU3Ec__AnonStorey0_t2035118920  : public RuntimeObject
{
public:
	// Utage.Timer Utage.AdvCommandZoomCamera/<OnStartEffect>c__AnonStorey0::timer
	Timer_t2904185433 * ___timer_0;
	// System.Single Utage.AdvCommandZoomCamera/<OnStartEffect>c__AnonStorey0::zoom0
	float ___zoom0_1;
	// System.Single Utage.AdvCommandZoomCamera/<OnStartEffect>c__AnonStorey0::zoomTo
	float ___zoomTo_2;
	// UnityEngine.Vector2 Utage.AdvCommandZoomCamera/<OnStartEffect>c__AnonStorey0::center0
	Vector2_t2243707579  ___center0_3;
	// UnityEngine.Vector2 Utage.AdvCommandZoomCamera/<OnStartEffect>c__AnonStorey0::centerTo
	Vector2_t2243707579  ___centerTo_4;
	// Utage.LetterBoxCamera Utage.AdvCommandZoomCamera/<OnStartEffect>c__AnonStorey0::camera
	LetterBoxCamera_t3507617684 * ___camera_5;
	// Utage.AdvCommandZoomCamera/<OnStartEffect>c__AnonStorey1 Utage.AdvCommandZoomCamera/<OnStartEffect>c__AnonStorey0::<>f__ref$1
	U3COnStartEffectU3Ec__AnonStorey1_t3601202861 * ___U3CU3Ef__refU241_6;

public:
	inline static int32_t get_offset_of_timer_0() { return static_cast<int32_t>(offsetof(U3COnStartEffectU3Ec__AnonStorey0_t2035118920, ___timer_0)); }
	inline Timer_t2904185433 * get_timer_0() const { return ___timer_0; }
	inline Timer_t2904185433 ** get_address_of_timer_0() { return &___timer_0; }
	inline void set_timer_0(Timer_t2904185433 * value)
	{
		___timer_0 = value;
		Il2CppCodeGenWriteBarrier((&___timer_0), value);
	}

	inline static int32_t get_offset_of_zoom0_1() { return static_cast<int32_t>(offsetof(U3COnStartEffectU3Ec__AnonStorey0_t2035118920, ___zoom0_1)); }
	inline float get_zoom0_1() const { return ___zoom0_1; }
	inline float* get_address_of_zoom0_1() { return &___zoom0_1; }
	inline void set_zoom0_1(float value)
	{
		___zoom0_1 = value;
	}

	inline static int32_t get_offset_of_zoomTo_2() { return static_cast<int32_t>(offsetof(U3COnStartEffectU3Ec__AnonStorey0_t2035118920, ___zoomTo_2)); }
	inline float get_zoomTo_2() const { return ___zoomTo_2; }
	inline float* get_address_of_zoomTo_2() { return &___zoomTo_2; }
	inline void set_zoomTo_2(float value)
	{
		___zoomTo_2 = value;
	}

	inline static int32_t get_offset_of_center0_3() { return static_cast<int32_t>(offsetof(U3COnStartEffectU3Ec__AnonStorey0_t2035118920, ___center0_3)); }
	inline Vector2_t2243707579  get_center0_3() const { return ___center0_3; }
	inline Vector2_t2243707579 * get_address_of_center0_3() { return &___center0_3; }
	inline void set_center0_3(Vector2_t2243707579  value)
	{
		___center0_3 = value;
	}

	inline static int32_t get_offset_of_centerTo_4() { return static_cast<int32_t>(offsetof(U3COnStartEffectU3Ec__AnonStorey0_t2035118920, ___centerTo_4)); }
	inline Vector2_t2243707579  get_centerTo_4() const { return ___centerTo_4; }
	inline Vector2_t2243707579 * get_address_of_centerTo_4() { return &___centerTo_4; }
	inline void set_centerTo_4(Vector2_t2243707579  value)
	{
		___centerTo_4 = value;
	}

	inline static int32_t get_offset_of_camera_5() { return static_cast<int32_t>(offsetof(U3COnStartEffectU3Ec__AnonStorey0_t2035118920, ___camera_5)); }
	inline LetterBoxCamera_t3507617684 * get_camera_5() const { return ___camera_5; }
	inline LetterBoxCamera_t3507617684 ** get_address_of_camera_5() { return &___camera_5; }
	inline void set_camera_5(LetterBoxCamera_t3507617684 * value)
	{
		___camera_5 = value;
		Il2CppCodeGenWriteBarrier((&___camera_5), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__refU241_6() { return static_cast<int32_t>(offsetof(U3COnStartEffectU3Ec__AnonStorey0_t2035118920, ___U3CU3Ef__refU241_6)); }
	inline U3COnStartEffectU3Ec__AnonStorey1_t3601202861 * get_U3CU3Ef__refU241_6() const { return ___U3CU3Ef__refU241_6; }
	inline U3COnStartEffectU3Ec__AnonStorey1_t3601202861 ** get_address_of_U3CU3Ef__refU241_6() { return &___U3CU3Ef__refU241_6; }
	inline void set_U3CU3Ef__refU241_6(U3COnStartEffectU3Ec__AnonStorey1_t3601202861 * value)
	{
		___U3CU3Ef__refU241_6 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__refU241_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CONSTARTEFFECTU3EC__ANONSTOREY0_T2035118920_H
#ifndef DELEGATE_T3022476291_H
#define DELEGATE_T3022476291_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Delegate
struct  Delegate_t3022476291  : public RuntimeObject
{
public:
	// System.IntPtr System.Delegate::method_ptr
	Il2CppMethodPointer ___method_ptr_0;
	// System.IntPtr System.Delegate::invoke_impl
	intptr_t ___invoke_impl_1;
	// System.Object System.Delegate::m_target
	RuntimeObject * ___m_target_2;
	// System.IntPtr System.Delegate::method
	intptr_t ___method_3;
	// System.IntPtr System.Delegate::delegate_trampoline
	intptr_t ___delegate_trampoline_4;
	// System.IntPtr System.Delegate::method_code
	intptr_t ___method_code_5;
	// System.Reflection.MethodInfo System.Delegate::method_info
	MethodInfo_t * ___method_info_6;
	// System.Reflection.MethodInfo System.Delegate::original_method_info
	MethodInfo_t * ___original_method_info_7;
	// System.DelegateData System.Delegate::data
	DelegateData_t1572802995 * ___data_8;

public:
	inline static int32_t get_offset_of_method_ptr_0() { return static_cast<int32_t>(offsetof(Delegate_t3022476291, ___method_ptr_0)); }
	inline Il2CppMethodPointer get_method_ptr_0() const { return ___method_ptr_0; }
	inline Il2CppMethodPointer* get_address_of_method_ptr_0() { return &___method_ptr_0; }
	inline void set_method_ptr_0(Il2CppMethodPointer value)
	{
		___method_ptr_0 = value;
	}

	inline static int32_t get_offset_of_invoke_impl_1() { return static_cast<int32_t>(offsetof(Delegate_t3022476291, ___invoke_impl_1)); }
	inline intptr_t get_invoke_impl_1() const { return ___invoke_impl_1; }
	inline intptr_t* get_address_of_invoke_impl_1() { return &___invoke_impl_1; }
	inline void set_invoke_impl_1(intptr_t value)
	{
		___invoke_impl_1 = value;
	}

	inline static int32_t get_offset_of_m_target_2() { return static_cast<int32_t>(offsetof(Delegate_t3022476291, ___m_target_2)); }
	inline RuntimeObject * get_m_target_2() const { return ___m_target_2; }
	inline RuntimeObject ** get_address_of_m_target_2() { return &___m_target_2; }
	inline void set_m_target_2(RuntimeObject * value)
	{
		___m_target_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_target_2), value);
	}

	inline static int32_t get_offset_of_method_3() { return static_cast<int32_t>(offsetof(Delegate_t3022476291, ___method_3)); }
	inline intptr_t get_method_3() const { return ___method_3; }
	inline intptr_t* get_address_of_method_3() { return &___method_3; }
	inline void set_method_3(intptr_t value)
	{
		___method_3 = value;
	}

	inline static int32_t get_offset_of_delegate_trampoline_4() { return static_cast<int32_t>(offsetof(Delegate_t3022476291, ___delegate_trampoline_4)); }
	inline intptr_t get_delegate_trampoline_4() const { return ___delegate_trampoline_4; }
	inline intptr_t* get_address_of_delegate_trampoline_4() { return &___delegate_trampoline_4; }
	inline void set_delegate_trampoline_4(intptr_t value)
	{
		___delegate_trampoline_4 = value;
	}

	inline static int32_t get_offset_of_method_code_5() { return static_cast<int32_t>(offsetof(Delegate_t3022476291, ___method_code_5)); }
	inline intptr_t get_method_code_5() const { return ___method_code_5; }
	inline intptr_t* get_address_of_method_code_5() { return &___method_code_5; }
	inline void set_method_code_5(intptr_t value)
	{
		___method_code_5 = value;
	}

	inline static int32_t get_offset_of_method_info_6() { return static_cast<int32_t>(offsetof(Delegate_t3022476291, ___method_info_6)); }
	inline MethodInfo_t * get_method_info_6() const { return ___method_info_6; }
	inline MethodInfo_t ** get_address_of_method_info_6() { return &___method_info_6; }
	inline void set_method_info_6(MethodInfo_t * value)
	{
		___method_info_6 = value;
		Il2CppCodeGenWriteBarrier((&___method_info_6), value);
	}

	inline static int32_t get_offset_of_original_method_info_7() { return static_cast<int32_t>(offsetof(Delegate_t3022476291, ___original_method_info_7)); }
	inline MethodInfo_t * get_original_method_info_7() const { return ___original_method_info_7; }
	inline MethodInfo_t ** get_address_of_original_method_info_7() { return &___original_method_info_7; }
	inline void set_original_method_info_7(MethodInfo_t * value)
	{
		___original_method_info_7 = value;
		Il2CppCodeGenWriteBarrier((&___original_method_info_7), value);
	}

	inline static int32_t get_offset_of_data_8() { return static_cast<int32_t>(offsetof(Delegate_t3022476291, ___data_8)); }
	inline DelegateData_t1572802995 * get_data_8() const { return ___data_8; }
	inline DelegateData_t1572802995 ** get_address_of_data_8() { return &___data_8; }
	inline void set_data_8(DelegateData_t1572802995 * value)
	{
		___data_8 = value;
		Il2CppCodeGenWriteBarrier((&___data_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DELEGATE_T3022476291_H
#ifndef CAMERACLEARFLAGS_T452084705_H
#define CAMERACLEARFLAGS_T452084705_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.CameraClearFlags
struct  CameraClearFlags_t452084705 
{
public:
	// System.Int32 UnityEngine.CameraClearFlags::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(CameraClearFlags_t452084705, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CAMERACLEARFLAGS_T452084705_H
#ifndef ADVPAGECONTROLLERTYPE_T2095572932_H
#define ADVPAGECONTROLLERTYPE_T2095572932_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.AdvPageControllerType
struct  AdvPageControllerType_t2095572932 
{
public:
	// System.Int32 Utage.AdvPageControllerType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(AdvPageControllerType_t2095572932, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ADVPAGECONTROLLERTYPE_T2095572932_H
#ifndef OBJECT_T1021602117_H
#define OBJECT_T1021602117_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Object
struct  Object_t1021602117  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Object::m_CachedPtr
	intptr_t ___m_CachedPtr_0;

public:
	inline static int32_t get_offset_of_m_CachedPtr_0() { return static_cast<int32_t>(offsetof(Object_t1021602117, ___m_CachedPtr_0)); }
	inline intptr_t get_m_CachedPtr_0() const { return ___m_CachedPtr_0; }
	inline intptr_t* get_address_of_m_CachedPtr_0() { return &___m_CachedPtr_0; }
	inline void set_m_CachedPtr_0(intptr_t value)
	{
		___m_CachedPtr_0 = value;
	}
};

struct Object_t1021602117_StaticFields
{
public:
	// System.Int32 UnityEngine.Object::OffsetOfInstanceIDInCPlusPlusObject
	int32_t ___OffsetOfInstanceIDInCPlusPlusObject_1;

public:
	inline static int32_t get_offset_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return static_cast<int32_t>(offsetof(Object_t1021602117_StaticFields, ___OffsetOfInstanceIDInCPlusPlusObject_1)); }
	inline int32_t get_OffsetOfInstanceIDInCPlusPlusObject_1() const { return ___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline int32_t* get_address_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return &___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline void set_OffsetOfInstanceIDInCPlusPlusObject_1(int32_t value)
	{
		___OffsetOfInstanceIDInCPlusPlusObject_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.Object
struct Object_t1021602117_marshaled_pinvoke
{
	intptr_t ___m_CachedPtr_0;
};
// Native definition for COM marshalling of UnityEngine.Object
struct Object_t1021602117_marshaled_com
{
	intptr_t ___m_CachedPtr_0;
};
#endif // OBJECT_T1021602117_H
#ifndef ADVCOMMANDGUISIZE_T3266247994_H
#define ADVCOMMANDGUISIZE_T3266247994_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.AdvCommandGuiSize
struct  AdvCommandGuiSize_t3266247994  : public AdvCommand_t2859960984
{
public:
	// System.String Utage.AdvCommandGuiSize::name
	String_t* ___name_7;
	// System.Nullable`1<System.Single> Utage.AdvCommandGuiSize::x
	Nullable_1_t339576247  ___x_8;
	// System.Nullable`1<System.Single> Utage.AdvCommandGuiSize::y
	Nullable_1_t339576247  ___y_9;

public:
	inline static int32_t get_offset_of_name_7() { return static_cast<int32_t>(offsetof(AdvCommandGuiSize_t3266247994, ___name_7)); }
	inline String_t* get_name_7() const { return ___name_7; }
	inline String_t** get_address_of_name_7() { return &___name_7; }
	inline void set_name_7(String_t* value)
	{
		___name_7 = value;
		Il2CppCodeGenWriteBarrier((&___name_7), value);
	}

	inline static int32_t get_offset_of_x_8() { return static_cast<int32_t>(offsetof(AdvCommandGuiSize_t3266247994, ___x_8)); }
	inline Nullable_1_t339576247  get_x_8() const { return ___x_8; }
	inline Nullable_1_t339576247 * get_address_of_x_8() { return &___x_8; }
	inline void set_x_8(Nullable_1_t339576247  value)
	{
		___x_8 = value;
	}

	inline static int32_t get_offset_of_y_9() { return static_cast<int32_t>(offsetof(AdvCommandGuiSize_t3266247994, ___y_9)); }
	inline Nullable_1_t339576247  get_y_9() const { return ___y_9; }
	inline Nullable_1_t339576247 * get_address_of_y_9() { return &___y_9; }
	inline void set_y_9(Nullable_1_t339576247  value)
	{
		___y_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ADVCOMMANDGUISIZE_T3266247994_H
#ifndef ADVCOMMANDGUIPOSITION_T1553404504_H
#define ADVCOMMANDGUIPOSITION_T1553404504_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.AdvCommandGuiPosition
struct  AdvCommandGuiPosition_t1553404504  : public AdvCommand_t2859960984
{
public:
	// System.String Utage.AdvCommandGuiPosition::name
	String_t* ___name_7;
	// System.Nullable`1<System.Single> Utage.AdvCommandGuiPosition::x
	Nullable_1_t339576247  ___x_8;
	// System.Nullable`1<System.Single> Utage.AdvCommandGuiPosition::y
	Nullable_1_t339576247  ___y_9;

public:
	inline static int32_t get_offset_of_name_7() { return static_cast<int32_t>(offsetof(AdvCommandGuiPosition_t1553404504, ___name_7)); }
	inline String_t* get_name_7() const { return ___name_7; }
	inline String_t** get_address_of_name_7() { return &___name_7; }
	inline void set_name_7(String_t* value)
	{
		___name_7 = value;
		Il2CppCodeGenWriteBarrier((&___name_7), value);
	}

	inline static int32_t get_offset_of_x_8() { return static_cast<int32_t>(offsetof(AdvCommandGuiPosition_t1553404504, ___x_8)); }
	inline Nullable_1_t339576247  get_x_8() const { return ___x_8; }
	inline Nullable_1_t339576247 * get_address_of_x_8() { return &___x_8; }
	inline void set_x_8(Nullable_1_t339576247  value)
	{
		___x_8 = value;
	}

	inline static int32_t get_offset_of_y_9() { return static_cast<int32_t>(offsetof(AdvCommandGuiPosition_t1553404504, ___y_9)); }
	inline Nullable_1_t339576247  get_y_9() const { return ___y_9; }
	inline Nullable_1_t339576247 * get_address_of_y_9() { return &___y_9; }
	inline void set_y_9(Nullable_1_t339576247  value)
	{
		___y_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ADVCOMMANDGUIPOSITION_T1553404504_H
#ifndef SCENARIOLABELTYPE_T882757673_H
#define SCENARIOLABELTYPE_T882757673_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.AdvCommandScenarioLabel/ScenarioLabelType
struct  ScenarioLabelType_t882757673 
{
public:
	// System.Int32 Utage.AdvCommandScenarioLabel/ScenarioLabelType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(ScenarioLabelType_t882757673, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SCENARIOLABELTYPE_T882757673_H
#ifndef ADVCOMMANDSELECTION_T2580391078_H
#define ADVCOMMANDSELECTION_T2580391078_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.AdvCommandSelection
struct  AdvCommandSelection_t2580391078  : public AdvCommand_t2859960984
{
public:
	// System.String Utage.AdvCommandSelection::jumpLabel
	String_t* ___jumpLabel_7;
	// Utage.ExpressionParser Utage.AdvCommandSelection::exp
	ExpressionParser_t665800307 * ___exp_8;
	// Utage.ExpressionParser Utage.AdvCommandSelection::selectedExp
	ExpressionParser_t665800307 * ___selectedExp_9;
	// System.String Utage.AdvCommandSelection::prefabName
	String_t* ___prefabName_10;
	// System.Nullable`1<System.Single> Utage.AdvCommandSelection::x
	Nullable_1_t339576247  ___x_11;
	// System.Nullable`1<System.Single> Utage.AdvCommandSelection::y
	Nullable_1_t339576247  ___y_12;

public:
	inline static int32_t get_offset_of_jumpLabel_7() { return static_cast<int32_t>(offsetof(AdvCommandSelection_t2580391078, ___jumpLabel_7)); }
	inline String_t* get_jumpLabel_7() const { return ___jumpLabel_7; }
	inline String_t** get_address_of_jumpLabel_7() { return &___jumpLabel_7; }
	inline void set_jumpLabel_7(String_t* value)
	{
		___jumpLabel_7 = value;
		Il2CppCodeGenWriteBarrier((&___jumpLabel_7), value);
	}

	inline static int32_t get_offset_of_exp_8() { return static_cast<int32_t>(offsetof(AdvCommandSelection_t2580391078, ___exp_8)); }
	inline ExpressionParser_t665800307 * get_exp_8() const { return ___exp_8; }
	inline ExpressionParser_t665800307 ** get_address_of_exp_8() { return &___exp_8; }
	inline void set_exp_8(ExpressionParser_t665800307 * value)
	{
		___exp_8 = value;
		Il2CppCodeGenWriteBarrier((&___exp_8), value);
	}

	inline static int32_t get_offset_of_selectedExp_9() { return static_cast<int32_t>(offsetof(AdvCommandSelection_t2580391078, ___selectedExp_9)); }
	inline ExpressionParser_t665800307 * get_selectedExp_9() const { return ___selectedExp_9; }
	inline ExpressionParser_t665800307 ** get_address_of_selectedExp_9() { return &___selectedExp_9; }
	inline void set_selectedExp_9(ExpressionParser_t665800307 * value)
	{
		___selectedExp_9 = value;
		Il2CppCodeGenWriteBarrier((&___selectedExp_9), value);
	}

	inline static int32_t get_offset_of_prefabName_10() { return static_cast<int32_t>(offsetof(AdvCommandSelection_t2580391078, ___prefabName_10)); }
	inline String_t* get_prefabName_10() const { return ___prefabName_10; }
	inline String_t** get_address_of_prefabName_10() { return &___prefabName_10; }
	inline void set_prefabName_10(String_t* value)
	{
		___prefabName_10 = value;
		Il2CppCodeGenWriteBarrier((&___prefabName_10), value);
	}

	inline static int32_t get_offset_of_x_11() { return static_cast<int32_t>(offsetof(AdvCommandSelection_t2580391078, ___x_11)); }
	inline Nullable_1_t339576247  get_x_11() const { return ___x_11; }
	inline Nullable_1_t339576247 * get_address_of_x_11() { return &___x_11; }
	inline void set_x_11(Nullable_1_t339576247  value)
	{
		___x_11 = value;
	}

	inline static int32_t get_offset_of_y_12() { return static_cast<int32_t>(offsetof(AdvCommandSelection_t2580391078, ___y_12)); }
	inline Nullable_1_t339576247  get_y_12() const { return ___y_12; }
	inline Nullable_1_t339576247 * get_address_of_y_12() { return &___y_12; }
	inline void set_y_12(Nullable_1_t339576247  value)
	{
		___y_12 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ADVCOMMANDSELECTION_T2580391078_H
#ifndef ADVCOMMANDSCENARIOLABEL_T2271667896_H
#define ADVCOMMANDSCENARIOLABEL_T2271667896_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.AdvCommandScenarioLabel
struct  AdvCommandScenarioLabel_t2271667896  : public AdvCommand_t2859960984
{
public:
	// System.String Utage.AdvCommandScenarioLabel::<ScenarioLabel>k__BackingField
	String_t* ___U3CScenarioLabelU3Ek__BackingField_7;
	// Utage.AdvCommandScenarioLabel/ScenarioLabelType Utage.AdvCommandScenarioLabel::<Type>k__BackingField
	int32_t ___U3CTypeU3Ek__BackingField_8;
	// System.String Utage.AdvCommandScenarioLabel::<Title>k__BackingField
	String_t* ___U3CTitleU3Ek__BackingField_9;

public:
	inline static int32_t get_offset_of_U3CScenarioLabelU3Ek__BackingField_7() { return static_cast<int32_t>(offsetof(AdvCommandScenarioLabel_t2271667896, ___U3CScenarioLabelU3Ek__BackingField_7)); }
	inline String_t* get_U3CScenarioLabelU3Ek__BackingField_7() const { return ___U3CScenarioLabelU3Ek__BackingField_7; }
	inline String_t** get_address_of_U3CScenarioLabelU3Ek__BackingField_7() { return &___U3CScenarioLabelU3Ek__BackingField_7; }
	inline void set_U3CScenarioLabelU3Ek__BackingField_7(String_t* value)
	{
		___U3CScenarioLabelU3Ek__BackingField_7 = value;
		Il2CppCodeGenWriteBarrier((&___U3CScenarioLabelU3Ek__BackingField_7), value);
	}

	inline static int32_t get_offset_of_U3CTypeU3Ek__BackingField_8() { return static_cast<int32_t>(offsetof(AdvCommandScenarioLabel_t2271667896, ___U3CTypeU3Ek__BackingField_8)); }
	inline int32_t get_U3CTypeU3Ek__BackingField_8() const { return ___U3CTypeU3Ek__BackingField_8; }
	inline int32_t* get_address_of_U3CTypeU3Ek__BackingField_8() { return &___U3CTypeU3Ek__BackingField_8; }
	inline void set_U3CTypeU3Ek__BackingField_8(int32_t value)
	{
		___U3CTypeU3Ek__BackingField_8 = value;
	}

	inline static int32_t get_offset_of_U3CTitleU3Ek__BackingField_9() { return static_cast<int32_t>(offsetof(AdvCommandScenarioLabel_t2271667896, ___U3CTitleU3Ek__BackingField_9)); }
	inline String_t* get_U3CTitleU3Ek__BackingField_9() const { return ___U3CTitleU3Ek__BackingField_9; }
	inline String_t** get_address_of_U3CTitleU3Ek__BackingField_9() { return &___U3CTitleU3Ek__BackingField_9; }
	inline void set_U3CTitleU3Ek__BackingField_9(String_t* value)
	{
		___U3CTitleU3Ek__BackingField_9 = value;
		Il2CppCodeGenWriteBarrier((&___U3CTitleU3Ek__BackingField_9), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ADVCOMMANDSCENARIOLABEL_T2271667896_H
#ifndef MULTICASTDELEGATE_T3201952435_H
#define MULTICASTDELEGATE_T3201952435_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.MulticastDelegate
struct  MulticastDelegate_t3201952435  : public Delegate_t3022476291
{
public:
	// System.MulticastDelegate System.MulticastDelegate::prev
	MulticastDelegate_t3201952435 * ___prev_9;
	// System.MulticastDelegate System.MulticastDelegate::kpm_next
	MulticastDelegate_t3201952435 * ___kpm_next_10;

public:
	inline static int32_t get_offset_of_prev_9() { return static_cast<int32_t>(offsetof(MulticastDelegate_t3201952435, ___prev_9)); }
	inline MulticastDelegate_t3201952435 * get_prev_9() const { return ___prev_9; }
	inline MulticastDelegate_t3201952435 ** get_address_of_prev_9() { return &___prev_9; }
	inline void set_prev_9(MulticastDelegate_t3201952435 * value)
	{
		___prev_9 = value;
		Il2CppCodeGenWriteBarrier((&___prev_9), value);
	}

	inline static int32_t get_offset_of_kpm_next_10() { return static_cast<int32_t>(offsetof(MulticastDelegate_t3201952435, ___kpm_next_10)); }
	inline MulticastDelegate_t3201952435 * get_kpm_next_10() const { return ___kpm_next_10; }
	inline MulticastDelegate_t3201952435 ** get_address_of_kpm_next_10() { return &___kpm_next_10; }
	inline void set_kpm_next_10(MulticastDelegate_t3201952435 * value)
	{
		___kpm_next_10 = value;
		Il2CppCodeGenWriteBarrier((&___kpm_next_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MULTICASTDELEGATE_T3201952435_H
#ifndef ADVCOMMANDVIDEO_T218209391_H
#define ADVCOMMANDVIDEO_T218209391_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.AdvCommandVideo
struct  AdvCommandVideo_t218209391  : public AdvCommand_t2859960984
{
public:
	// System.Boolean Utage.AdvCommandVideo::isEndPlay
	bool ___isEndPlay_7;
	// UnityEngine.CameraClearFlags Utage.AdvCommandVideo::cameraClearFlags
	int32_t ___cameraClearFlags_8;
	// UnityEngine.Color Utage.AdvCommandVideo::cameraClearColor
	Color_t2020392075  ___cameraClearColor_9;
	// Utage.AssetFile Utage.AdvCommandVideo::file
	RuntimeObject* ___file_10;
	// System.String Utage.AdvCommandVideo::label
	String_t* ___label_11;
	// System.Boolean Utage.AdvCommandVideo::loop
	bool ___loop_12;
	// System.Boolean Utage.AdvCommandVideo::cancel
	bool ___cancel_13;
	// System.String Utage.AdvCommandVideo::cameraName
	String_t* ___cameraName_14;

public:
	inline static int32_t get_offset_of_isEndPlay_7() { return static_cast<int32_t>(offsetof(AdvCommandVideo_t218209391, ___isEndPlay_7)); }
	inline bool get_isEndPlay_7() const { return ___isEndPlay_7; }
	inline bool* get_address_of_isEndPlay_7() { return &___isEndPlay_7; }
	inline void set_isEndPlay_7(bool value)
	{
		___isEndPlay_7 = value;
	}

	inline static int32_t get_offset_of_cameraClearFlags_8() { return static_cast<int32_t>(offsetof(AdvCommandVideo_t218209391, ___cameraClearFlags_8)); }
	inline int32_t get_cameraClearFlags_8() const { return ___cameraClearFlags_8; }
	inline int32_t* get_address_of_cameraClearFlags_8() { return &___cameraClearFlags_8; }
	inline void set_cameraClearFlags_8(int32_t value)
	{
		___cameraClearFlags_8 = value;
	}

	inline static int32_t get_offset_of_cameraClearColor_9() { return static_cast<int32_t>(offsetof(AdvCommandVideo_t218209391, ___cameraClearColor_9)); }
	inline Color_t2020392075  get_cameraClearColor_9() const { return ___cameraClearColor_9; }
	inline Color_t2020392075 * get_address_of_cameraClearColor_9() { return &___cameraClearColor_9; }
	inline void set_cameraClearColor_9(Color_t2020392075  value)
	{
		___cameraClearColor_9 = value;
	}

	inline static int32_t get_offset_of_file_10() { return static_cast<int32_t>(offsetof(AdvCommandVideo_t218209391, ___file_10)); }
	inline RuntimeObject* get_file_10() const { return ___file_10; }
	inline RuntimeObject** get_address_of_file_10() { return &___file_10; }
	inline void set_file_10(RuntimeObject* value)
	{
		___file_10 = value;
		Il2CppCodeGenWriteBarrier((&___file_10), value);
	}

	inline static int32_t get_offset_of_label_11() { return static_cast<int32_t>(offsetof(AdvCommandVideo_t218209391, ___label_11)); }
	inline String_t* get_label_11() const { return ___label_11; }
	inline String_t** get_address_of_label_11() { return &___label_11; }
	inline void set_label_11(String_t* value)
	{
		___label_11 = value;
		Il2CppCodeGenWriteBarrier((&___label_11), value);
	}

	inline static int32_t get_offset_of_loop_12() { return static_cast<int32_t>(offsetof(AdvCommandVideo_t218209391, ___loop_12)); }
	inline bool get_loop_12() const { return ___loop_12; }
	inline bool* get_address_of_loop_12() { return &___loop_12; }
	inline void set_loop_12(bool value)
	{
		___loop_12 = value;
	}

	inline static int32_t get_offset_of_cancel_13() { return static_cast<int32_t>(offsetof(AdvCommandVideo_t218209391, ___cancel_13)); }
	inline bool get_cancel_13() const { return ___cancel_13; }
	inline bool* get_address_of_cancel_13() { return &___cancel_13; }
	inline void set_cancel_13(bool value)
	{
		___cancel_13 = value;
	}

	inline static int32_t get_offset_of_cameraName_14() { return static_cast<int32_t>(offsetof(AdvCommandVideo_t218209391, ___cameraName_14)); }
	inline String_t* get_cameraName_14() const { return ___cameraName_14; }
	inline String_t** get_address_of_cameraName_14() { return &___cameraName_14; }
	inline void set_cameraName_14(String_t* value)
	{
		___cameraName_14 = value;
		Il2CppCodeGenWriteBarrier((&___cameraName_14), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ADVCOMMANDVIDEO_T218209391_H
#ifndef COMPONENT_T3819376471_H
#define COMPONENT_T3819376471_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Component
struct  Component_t3819376471  : public Object_t1021602117
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPONENT_T3819376471_H
#ifndef ADVCOMMANDWAITBASE_T4173154484_H
#define ADVCOMMANDWAITBASE_T4173154484_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.AdvCommandWaitBase
struct  AdvCommandWaitBase_t4173154484  : public AdvCommand_t2859960984
{
public:
	// Utage.AdvCommandWaitType Utage.AdvCommandWaitBase::<WaitType>k__BackingField
	int32_t ___U3CWaitTypeU3Ek__BackingField_7;

public:
	inline static int32_t get_offset_of_U3CWaitTypeU3Ek__BackingField_7() { return static_cast<int32_t>(offsetof(AdvCommandWaitBase_t4173154484, ___U3CWaitTypeU3Ek__BackingField_7)); }
	inline int32_t get_U3CWaitTypeU3Ek__BackingField_7() const { return ___U3CWaitTypeU3Ek__BackingField_7; }
	inline int32_t* get_address_of_U3CWaitTypeU3Ek__BackingField_7() { return &___U3CWaitTypeU3Ek__BackingField_7; }
	inline void set_U3CWaitTypeU3Ek__BackingField_7(int32_t value)
	{
		___U3CWaitTypeU3Ek__BackingField_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ADVCOMMANDWAITBASE_T4173154484_H
#ifndef ADVCOMMANDPAGECONTROLER_T3545276185_H
#define ADVCOMMANDPAGECONTROLER_T3545276185_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.AdvCommandPageControler
struct  AdvCommandPageControler_t3545276185  : public AdvCommand_t2859960984
{
public:
	// Utage.AdvPageControllerType Utage.AdvCommandPageControler::pageCtrlType
	int32_t ___pageCtrlType_7;

public:
	inline static int32_t get_offset_of_pageCtrlType_7() { return static_cast<int32_t>(offsetof(AdvCommandPageControler_t3545276185, ___pageCtrlType_7)); }
	inline int32_t get_pageCtrlType_7() const { return ___pageCtrlType_7; }
	inline int32_t* get_address_of_pageCtrlType_7() { return &___pageCtrlType_7; }
	inline void set_pageCtrlType_7(int32_t value)
	{
		___pageCtrlType_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ADVCOMMANDPAGECONTROLER_T3545276185_H
#ifndef ADVCOMMANDTEXT_T816833175_H
#define ADVCOMMANDTEXT_T816833175_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.AdvCommandText
struct  AdvCommandText_t816833175  : public AdvCommand_t2859960984
{
public:
	// System.Boolean Utage.AdvCommandText::<IsPageEnd>k__BackingField
	bool ___U3CIsPageEndU3Ek__BackingField_7;
	// System.Boolean Utage.AdvCommandText::<IsNextBr>k__BackingField
	bool ___U3CIsNextBrU3Ek__BackingField_8;
	// Utage.AdvPageControllerType Utage.AdvCommandText::<PageCtrlType>k__BackingField
	int32_t ___U3CPageCtrlTypeU3Ek__BackingField_9;
	// Utage.AssetFile Utage.AdvCommandText::<VoiceFile>k__BackingField
	RuntimeObject* ___U3CVoiceFileU3Ek__BackingField_10;
	// Utage.AdvScenarioPageData Utage.AdvCommandText::<PageData>k__BackingField
	AdvScenarioPageData_t3333166790 * ___U3CPageDataU3Ek__BackingField_11;
	// System.Int32 Utage.AdvCommandText::<IndexPageData>k__BackingField
	int32_t ___U3CIndexPageDataU3Ek__BackingField_12;

public:
	inline static int32_t get_offset_of_U3CIsPageEndU3Ek__BackingField_7() { return static_cast<int32_t>(offsetof(AdvCommandText_t816833175, ___U3CIsPageEndU3Ek__BackingField_7)); }
	inline bool get_U3CIsPageEndU3Ek__BackingField_7() const { return ___U3CIsPageEndU3Ek__BackingField_7; }
	inline bool* get_address_of_U3CIsPageEndU3Ek__BackingField_7() { return &___U3CIsPageEndU3Ek__BackingField_7; }
	inline void set_U3CIsPageEndU3Ek__BackingField_7(bool value)
	{
		___U3CIsPageEndU3Ek__BackingField_7 = value;
	}

	inline static int32_t get_offset_of_U3CIsNextBrU3Ek__BackingField_8() { return static_cast<int32_t>(offsetof(AdvCommandText_t816833175, ___U3CIsNextBrU3Ek__BackingField_8)); }
	inline bool get_U3CIsNextBrU3Ek__BackingField_8() const { return ___U3CIsNextBrU3Ek__BackingField_8; }
	inline bool* get_address_of_U3CIsNextBrU3Ek__BackingField_8() { return &___U3CIsNextBrU3Ek__BackingField_8; }
	inline void set_U3CIsNextBrU3Ek__BackingField_8(bool value)
	{
		___U3CIsNextBrU3Ek__BackingField_8 = value;
	}

	inline static int32_t get_offset_of_U3CPageCtrlTypeU3Ek__BackingField_9() { return static_cast<int32_t>(offsetof(AdvCommandText_t816833175, ___U3CPageCtrlTypeU3Ek__BackingField_9)); }
	inline int32_t get_U3CPageCtrlTypeU3Ek__BackingField_9() const { return ___U3CPageCtrlTypeU3Ek__BackingField_9; }
	inline int32_t* get_address_of_U3CPageCtrlTypeU3Ek__BackingField_9() { return &___U3CPageCtrlTypeU3Ek__BackingField_9; }
	inline void set_U3CPageCtrlTypeU3Ek__BackingField_9(int32_t value)
	{
		___U3CPageCtrlTypeU3Ek__BackingField_9 = value;
	}

	inline static int32_t get_offset_of_U3CVoiceFileU3Ek__BackingField_10() { return static_cast<int32_t>(offsetof(AdvCommandText_t816833175, ___U3CVoiceFileU3Ek__BackingField_10)); }
	inline RuntimeObject* get_U3CVoiceFileU3Ek__BackingField_10() const { return ___U3CVoiceFileU3Ek__BackingField_10; }
	inline RuntimeObject** get_address_of_U3CVoiceFileU3Ek__BackingField_10() { return &___U3CVoiceFileU3Ek__BackingField_10; }
	inline void set_U3CVoiceFileU3Ek__BackingField_10(RuntimeObject* value)
	{
		___U3CVoiceFileU3Ek__BackingField_10 = value;
		Il2CppCodeGenWriteBarrier((&___U3CVoiceFileU3Ek__BackingField_10), value);
	}

	inline static int32_t get_offset_of_U3CPageDataU3Ek__BackingField_11() { return static_cast<int32_t>(offsetof(AdvCommandText_t816833175, ___U3CPageDataU3Ek__BackingField_11)); }
	inline AdvScenarioPageData_t3333166790 * get_U3CPageDataU3Ek__BackingField_11() const { return ___U3CPageDataU3Ek__BackingField_11; }
	inline AdvScenarioPageData_t3333166790 ** get_address_of_U3CPageDataU3Ek__BackingField_11() { return &___U3CPageDataU3Ek__BackingField_11; }
	inline void set_U3CPageDataU3Ek__BackingField_11(AdvScenarioPageData_t3333166790 * value)
	{
		___U3CPageDataU3Ek__BackingField_11 = value;
		Il2CppCodeGenWriteBarrier((&___U3CPageDataU3Ek__BackingField_11), value);
	}

	inline static int32_t get_offset_of_U3CIndexPageDataU3Ek__BackingField_12() { return static_cast<int32_t>(offsetof(AdvCommandText_t816833175, ___U3CIndexPageDataU3Ek__BackingField_12)); }
	inline int32_t get_U3CIndexPageDataU3Ek__BackingField_12() const { return ___U3CIndexPageDataU3Ek__BackingField_12; }
	inline int32_t* get_address_of_U3CIndexPageDataU3Ek__BackingField_12() { return &___U3CIndexPageDataU3Ek__BackingField_12; }
	inline void set_U3CIndexPageDataU3Ek__BackingField_12(int32_t value)
	{
		___U3CIndexPageDataU3Ek__BackingField_12 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ADVCOMMANDTEXT_T816833175_H
#ifndef CREATECUSTOMCOMMANDFROMID_T3335294381_H
#define CREATECUSTOMCOMMANDFROMID_T3335294381_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.AdvCommandParser/CreateCustomCommandFromID
struct  CreateCustomCommandFromID_t3335294381  : public MulticastDelegate_t3201952435
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CREATECUSTOMCOMMANDFROMID_T3335294381_H
#ifndef BEHAVIOUR_T955675639_H
#define BEHAVIOUR_T955675639_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Behaviour
struct  Behaviour_t955675639  : public Component_t3819376471
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BEHAVIOUR_T955675639_H
#ifndef ADVCOMMANDEFFECTBASE_T2092483048_H
#define ADVCOMMANDEFFECTBASE_T2092483048_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.AdvCommandEffectBase
struct  AdvCommandEffectBase_t2092483048  : public AdvCommandWaitBase_t4173154484
{
public:
	// Utage.AdvEffectManager/TargetType Utage.AdvCommandEffectBase::targetType
	int32_t ___targetType_8;
	// System.String Utage.AdvCommandEffectBase::targetName
	String_t* ___targetName_9;

public:
	inline static int32_t get_offset_of_targetType_8() { return static_cast<int32_t>(offsetof(AdvCommandEffectBase_t2092483048, ___targetType_8)); }
	inline int32_t get_targetType_8() const { return ___targetType_8; }
	inline int32_t* get_address_of_targetType_8() { return &___targetType_8; }
	inline void set_targetType_8(int32_t value)
	{
		___targetType_8 = value;
	}

	inline static int32_t get_offset_of_targetName_9() { return static_cast<int32_t>(offsetof(AdvCommandEffectBase_t2092483048, ___targetName_9)); }
	inline String_t* get_targetName_9() const { return ___targetName_9; }
	inline String_t** get_address_of_targetName_9() { return &___targetName_9; }
	inline void set_targetName_9(String_t* value)
	{
		___targetName_9 = value;
		Il2CppCodeGenWriteBarrier((&___targetName_9), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ADVCOMMANDEFFECTBASE_T2092483048_H
#ifndef ADVCOMMANDTWEEN_T1326796191_H
#define ADVCOMMANDTWEEN_T1326796191_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.AdvCommandTween
struct  AdvCommandTween_t1326796191  : public AdvCommandEffectBase_t2092483048
{
public:
	// Utage.iTweenData Utage.AdvCommandTween::tweenData
	iTweenData_t1904063128 * ___tweenData_10;

public:
	inline static int32_t get_offset_of_tweenData_10() { return static_cast<int32_t>(offsetof(AdvCommandTween_t1326796191, ___tweenData_10)); }
	inline iTweenData_t1904063128 * get_tweenData_10() const { return ___tweenData_10; }
	inline iTweenData_t1904063128 ** get_address_of_tweenData_10() { return &___tweenData_10; }
	inline void set_tweenData_10(iTweenData_t1904063128 * value)
	{
		___tweenData_10 = value;
		Il2CppCodeGenWriteBarrier((&___tweenData_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ADVCOMMANDTWEEN_T1326796191_H
#ifndef ADVCOMMANDPLAYANIMATIN_T293841659_H
#define ADVCOMMANDPLAYANIMATIN_T293841659_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.AdvCommandPlayAnimatin
struct  AdvCommandPlayAnimatin_t293841659  : public AdvCommandEffectBase_t2092483048
{
public:
	// System.String Utage.AdvCommandPlayAnimatin::animationName
	String_t* ___animationName_10;

public:
	inline static int32_t get_offset_of_animationName_10() { return static_cast<int32_t>(offsetof(AdvCommandPlayAnimatin_t293841659, ___animationName_10)); }
	inline String_t* get_animationName_10() const { return ___animationName_10; }
	inline String_t** get_address_of_animationName_10() { return &___animationName_10; }
	inline void set_animationName_10(String_t* value)
	{
		___animationName_10 = value;
		Il2CppCodeGenWriteBarrier((&___animationName_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ADVCOMMANDPLAYANIMATIN_T293841659_H
#ifndef ADVCOMMANDZOOMCAMERA_T2560452812_H
#define ADVCOMMANDZOOMCAMERA_T2560452812_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.AdvCommandZoomCamera
struct  AdvCommandZoomCamera_t2560452812  : public AdvCommandEffectBase_t2092483048
{
public:
	// System.Boolean Utage.AdvCommandZoomCamera::isEmptyZoom
	bool ___isEmptyZoom_10;
	// System.Single Utage.AdvCommandZoomCamera::zoom
	float ___zoom_11;
	// System.Boolean Utage.AdvCommandZoomCamera::isEmptyZoomCenter
	bool ___isEmptyZoomCenter_12;
	// UnityEngine.Vector2 Utage.AdvCommandZoomCamera::zoomCenter
	Vector2_t2243707579  ___zoomCenter_13;
	// System.Single Utage.AdvCommandZoomCamera::time
	float ___time_14;

public:
	inline static int32_t get_offset_of_isEmptyZoom_10() { return static_cast<int32_t>(offsetof(AdvCommandZoomCamera_t2560452812, ___isEmptyZoom_10)); }
	inline bool get_isEmptyZoom_10() const { return ___isEmptyZoom_10; }
	inline bool* get_address_of_isEmptyZoom_10() { return &___isEmptyZoom_10; }
	inline void set_isEmptyZoom_10(bool value)
	{
		___isEmptyZoom_10 = value;
	}

	inline static int32_t get_offset_of_zoom_11() { return static_cast<int32_t>(offsetof(AdvCommandZoomCamera_t2560452812, ___zoom_11)); }
	inline float get_zoom_11() const { return ___zoom_11; }
	inline float* get_address_of_zoom_11() { return &___zoom_11; }
	inline void set_zoom_11(float value)
	{
		___zoom_11 = value;
	}

	inline static int32_t get_offset_of_isEmptyZoomCenter_12() { return static_cast<int32_t>(offsetof(AdvCommandZoomCamera_t2560452812, ___isEmptyZoomCenter_12)); }
	inline bool get_isEmptyZoomCenter_12() const { return ___isEmptyZoomCenter_12; }
	inline bool* get_address_of_isEmptyZoomCenter_12() { return &___isEmptyZoomCenter_12; }
	inline void set_isEmptyZoomCenter_12(bool value)
	{
		___isEmptyZoomCenter_12 = value;
	}

	inline static int32_t get_offset_of_zoomCenter_13() { return static_cast<int32_t>(offsetof(AdvCommandZoomCamera_t2560452812, ___zoomCenter_13)); }
	inline Vector2_t2243707579  get_zoomCenter_13() const { return ___zoomCenter_13; }
	inline Vector2_t2243707579 * get_address_of_zoomCenter_13() { return &___zoomCenter_13; }
	inline void set_zoomCenter_13(Vector2_t2243707579  value)
	{
		___zoomCenter_13 = value;
	}

	inline static int32_t get_offset_of_time_14() { return static_cast<int32_t>(offsetof(AdvCommandZoomCamera_t2560452812, ___time_14)); }
	inline float get_time_14() const { return ___time_14; }
	inline float* get_address_of_time_14() { return &___time_14; }
	inline void set_time_14(float value)
	{
		___time_14 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ADVCOMMANDZOOMCAMERA_T2560452812_H
#ifndef ADVCOMMANDIMAGEEFFECTBASE_T67184963_H
#define ADVCOMMANDIMAGEEFFECTBASE_T67184963_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.AdvCommandImageEffectBase
struct  AdvCommandImageEffectBase_t67184963  : public AdvCommandEffectBase_t2092483048
{
public:
	// System.String Utage.AdvCommandImageEffectBase::animationName
	String_t* ___animationName_10;
	// System.Single Utage.AdvCommandImageEffectBase::time
	float ___time_11;
	// System.String Utage.AdvCommandImageEffectBase::<imageEffectType>k__BackingField
	String_t* ___U3CimageEffectTypeU3Ek__BackingField_12;
	// System.Boolean Utage.AdvCommandImageEffectBase::inverse
	bool ___inverse_13;

public:
	inline static int32_t get_offset_of_animationName_10() { return static_cast<int32_t>(offsetof(AdvCommandImageEffectBase_t67184963, ___animationName_10)); }
	inline String_t* get_animationName_10() const { return ___animationName_10; }
	inline String_t** get_address_of_animationName_10() { return &___animationName_10; }
	inline void set_animationName_10(String_t* value)
	{
		___animationName_10 = value;
		Il2CppCodeGenWriteBarrier((&___animationName_10), value);
	}

	inline static int32_t get_offset_of_time_11() { return static_cast<int32_t>(offsetof(AdvCommandImageEffectBase_t67184963, ___time_11)); }
	inline float get_time_11() const { return ___time_11; }
	inline float* get_address_of_time_11() { return &___time_11; }
	inline void set_time_11(float value)
	{
		___time_11 = value;
	}

	inline static int32_t get_offset_of_U3CimageEffectTypeU3Ek__BackingField_12() { return static_cast<int32_t>(offsetof(AdvCommandImageEffectBase_t67184963, ___U3CimageEffectTypeU3Ek__BackingField_12)); }
	inline String_t* get_U3CimageEffectTypeU3Ek__BackingField_12() const { return ___U3CimageEffectTypeU3Ek__BackingField_12; }
	inline String_t** get_address_of_U3CimageEffectTypeU3Ek__BackingField_12() { return &___U3CimageEffectTypeU3Ek__BackingField_12; }
	inline void set_U3CimageEffectTypeU3Ek__BackingField_12(String_t* value)
	{
		___U3CimageEffectTypeU3Ek__BackingField_12 = value;
		Il2CppCodeGenWriteBarrier((&___U3CimageEffectTypeU3Ek__BackingField_12), value);
	}

	inline static int32_t get_offset_of_inverse_13() { return static_cast<int32_t>(offsetof(AdvCommandImageEffectBase_t67184963, ___inverse_13)); }
	inline bool get_inverse_13() const { return ___inverse_13; }
	inline bool* get_address_of_inverse_13() { return &___inverse_13; }
	inline void set_inverse_13(bool value)
	{
		___inverse_13 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ADVCOMMANDIMAGEEFFECTBASE_T67184963_H
#ifndef ADVCOMMANDFADEBASE_T2761005413_H
#define ADVCOMMANDFADEBASE_T2761005413_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.AdvCommandFadeBase
struct  AdvCommandFadeBase_t2761005413  : public AdvCommandEffectBase_t2092483048
{
public:
	// System.Single Utage.AdvCommandFadeBase::time
	float ___time_10;
	// System.Boolean Utage.AdvCommandFadeBase::inverse
	bool ___inverse_11;
	// UnityEngine.Color Utage.AdvCommandFadeBase::color
	Color_t2020392075  ___color_12;

public:
	inline static int32_t get_offset_of_time_10() { return static_cast<int32_t>(offsetof(AdvCommandFadeBase_t2761005413, ___time_10)); }
	inline float get_time_10() const { return ___time_10; }
	inline float* get_address_of_time_10() { return &___time_10; }
	inline void set_time_10(float value)
	{
		___time_10 = value;
	}

	inline static int32_t get_offset_of_inverse_11() { return static_cast<int32_t>(offsetof(AdvCommandFadeBase_t2761005413, ___inverse_11)); }
	inline bool get_inverse_11() const { return ___inverse_11; }
	inline bool* get_address_of_inverse_11() { return &___inverse_11; }
	inline void set_inverse_11(bool value)
	{
		___inverse_11 = value;
	}

	inline static int32_t get_offset_of_color_12() { return static_cast<int32_t>(offsetof(AdvCommandFadeBase_t2761005413, ___color_12)); }
	inline Color_t2020392075  get_color_12() const { return ___color_12; }
	inline Color_t2020392075 * get_address_of_color_12() { return &___color_12; }
	inline void set_color_12(Color_t2020392075  value)
	{
		___color_12 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ADVCOMMANDFADEBASE_T2761005413_H
#ifndef MONOBEHAVIOUR_T1158329972_H
#define MONOBEHAVIOUR_T1158329972_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.MonoBehaviour
struct  MonoBehaviour_t1158329972  : public Behaviour_t955675639
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MONOBEHAVIOUR_T1158329972_H
#ifndef ADVCOMMANDRULEFADEOUT_T646159788_H
#define ADVCOMMANDRULEFADEOUT_T646159788_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.AdvCommandRuleFadeOut
struct  AdvCommandRuleFadeOut_t646159788  : public AdvCommandEffectBase_t2092483048
{
public:
	// Utage.AdvTransitionArgs Utage.AdvCommandRuleFadeOut::data
	AdvTransitionArgs_t2412396169 * ___data_10;

public:
	inline static int32_t get_offset_of_data_10() { return static_cast<int32_t>(offsetof(AdvCommandRuleFadeOut_t646159788, ___data_10)); }
	inline AdvTransitionArgs_t2412396169 * get_data_10() const { return ___data_10; }
	inline AdvTransitionArgs_t2412396169 ** get_address_of_data_10() { return &___data_10; }
	inline void set_data_10(AdvTransitionArgs_t2412396169 * value)
	{
		___data_10 = value;
		Il2CppCodeGenWriteBarrier((&___data_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ADVCOMMANDRULEFADEOUT_T646159788_H
#ifndef ADVCOMMANDRULEFADEIN_T2011331537_H
#define ADVCOMMANDRULEFADEIN_T2011331537_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.AdvCommandRuleFadeIn
struct  AdvCommandRuleFadeIn_t2011331537  : public AdvCommandEffectBase_t2092483048
{
public:
	// Utage.AdvTransitionArgs Utage.AdvCommandRuleFadeIn::data
	AdvTransitionArgs_t2412396169 * ___data_10;

public:
	inline static int32_t get_offset_of_data_10() { return static_cast<int32_t>(offsetof(AdvCommandRuleFadeIn_t2011331537, ___data_10)); }
	inline AdvTransitionArgs_t2412396169 * get_data_10() const { return ___data_10; }
	inline AdvTransitionArgs_t2412396169 ** get_address_of_data_10() { return &___data_10; }
	inline void set_data_10(AdvTransitionArgs_t2412396169 * value)
	{
		___data_10 = value;
		Il2CppCodeGenWriteBarrier((&___data_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ADVCOMMANDRULEFADEIN_T2011331537_H
#ifndef ADVCOMMANDSHAKE_T3565450354_H
#define ADVCOMMANDSHAKE_T3565450354_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.AdvCommandShake
struct  AdvCommandShake_t3565450354  : public AdvCommandTween_t1326796191
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ADVCOMMANDSHAKE_T3565450354_H
#ifndef ADVCOMMANDIMAGEEFFECTOFF_T4223089517_H
#define ADVCOMMANDIMAGEEFFECTOFF_T4223089517_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.AdvCommandImageEffectOff
struct  AdvCommandImageEffectOff_t4223089517  : public AdvCommandImageEffectBase_t67184963
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ADVCOMMANDIMAGEEFFECTOFF_T4223089517_H
#ifndef ADVCOMMANDFADEIN_T934960843_H
#define ADVCOMMANDFADEIN_T934960843_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.AdvCommandFadeIn
struct  AdvCommandFadeIn_t934960843  : public AdvCommandFadeBase_t2761005413
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ADVCOMMANDFADEIN_T934960843_H
#ifndef ADVCOMMANDIMAGEEFFECT_T3775745660_H
#define ADVCOMMANDIMAGEEFFECT_T3775745660_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.AdvCommandImageEffect
struct  AdvCommandImageEffect_t3775745660  : public AdvCommandImageEffectBase_t67184963
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ADVCOMMANDIMAGEEFFECT_T3775745660_H
#ifndef ADVCOMMANDFADEOUT_T3953198378_H
#define ADVCOMMANDFADEOUT_T3953198378_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.AdvCommandFadeOut
struct  AdvCommandFadeOut_t3953198378  : public AdvCommandFadeBase_t2761005413
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ADVCOMMANDFADEOUT_T3953198378_H
#ifndef ADVCUSTOMCOMMANDMANAGER_T178514092_H
#define ADVCUSTOMCOMMANDMANAGER_T178514092_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utage.AdvCustomCommandManager
struct  AdvCustomCommandManager_t178514092  : public MonoBehaviour_t1158329972
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ADVCUSTOMCOMMANDMANAGER_T178514092_H





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2200 = { sizeof (U3CLoadAssetBundleManifestAsyncU3Ec__Iterator4_t3283659535), -1, sizeof(U3CLoadAssetBundleManifestAsyncU3Ec__Iterator4_t3283659535_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2200[9] = 
{
	U3CLoadAssetBundleManifestAsyncU3Ec__Iterator4_t3283659535::get_offset_of_fromCache_0(),
	U3CLoadAssetBundleManifestAsyncU3Ec__Iterator4_t3283659535::get_offset_of_onFailed_1(),
	U3CLoadAssetBundleManifestAsyncU3Ec__Iterator4_t3283659535::get_offset_of_U24this_2(),
	U3CLoadAssetBundleManifestAsyncU3Ec__Iterator4_t3283659535::get_offset_of_U24current_3(),
	U3CLoadAssetBundleManifestAsyncU3Ec__Iterator4_t3283659535::get_offset_of_U24disposing_4(),
	U3CLoadAssetBundleManifestAsyncU3Ec__Iterator4_t3283659535::get_offset_of_U24PC_5(),
	U3CLoadAssetBundleManifestAsyncU3Ec__Iterator4_t3283659535::get_offset_of_U24locvar0_6(),
	U3CLoadAssetBundleManifestAsyncU3Ec__Iterator4_t3283659535_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_7(),
	U3CLoadAssetBundleManifestAsyncU3Ec__Iterator4_t3283659535_StaticFields::get_offset_of_U3CU3Ef__amU24cache1_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2201 = { sizeof (U3CLoadAssetBundleManifestAsyncU3Ec__AnonStorey9_t2150114403), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2201[2] = 
{
	U3CLoadAssetBundleManifestAsyncU3Ec__AnonStorey9_t2150114403::get_offset_of_onFailed_0(),
	U3CLoadAssetBundleManifestAsyncU3Ec__AnonStorey9_t2150114403::get_offset_of_U3CU3Ef__refU244_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2202 = { sizeof (U3CLoadScenariosAsyncU3Ec__Iterator5_t2233559140), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2202[8] = 
{
	U3CLoadScenariosAsyncU3Ec__Iterator5_t2233559140::get_offset_of_rootDir_0(),
	U3CLoadScenariosAsyncU3Ec__Iterator5_t2233559140::get_offset_of_U3CurlU3E__0_1(),
	U3CLoadScenariosAsyncU3Ec__Iterator5_t2233559140::get_offset_of_U3CfileU3E__0_2(),
	U3CLoadScenariosAsyncU3Ec__Iterator5_t2233559140::get_offset_of_U3CscenariosU3E__0_3(),
	U3CLoadScenariosAsyncU3Ec__Iterator5_t2233559140::get_offset_of_U24this_4(),
	U3CLoadScenariosAsyncU3Ec__Iterator5_t2233559140::get_offset_of_U24current_5(),
	U3CLoadScenariosAsyncU3Ec__Iterator5_t2233559140::get_offset_of_U24disposing_6(),
	U3CLoadScenariosAsyncU3Ec__Iterator5_t2233559140::get_offset_of_U24PC_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2203 = { sizeof (U3CLoadChaptersAsyncU3Ec__Iterator6_t953487478), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2203[11] = 
{
	U3CLoadChaptersAsyncU3Ec__Iterator6_t953487478::get_offset_of_U3CscenariosU3E__0_0(),
	U3CLoadChaptersAsyncU3Ec__Iterator6_t953487478::get_offset_of_U24locvar0_1(),
	U3CLoadChaptersAsyncU3Ec__Iterator6_t953487478::get_offset_of_U3CchapterNameU3E__1_2(),
	U3CLoadChaptersAsyncU3Ec__Iterator6_t953487478::get_offset_of_rootDir_3(),
	U3CLoadChaptersAsyncU3Ec__Iterator6_t953487478::get_offset_of_U3CurlU3E__2_4(),
	U3CLoadChaptersAsyncU3Ec__Iterator6_t953487478::get_offset_of_U3CfileU3E__2_5(),
	U3CLoadChaptersAsyncU3Ec__Iterator6_t953487478::get_offset_of_U3CchapterU3E__2_6(),
	U3CLoadChaptersAsyncU3Ec__Iterator6_t953487478::get_offset_of_U24this_7(),
	U3CLoadChaptersAsyncU3Ec__Iterator6_t953487478::get_offset_of_U24current_8(),
	U3CLoadChaptersAsyncU3Ec__Iterator6_t953487478::get_offset_of_U24disposing_9(),
	U3CLoadChaptersAsyncU3Ec__Iterator6_t953487478::get_offset_of_U24PC_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2204 = { sizeof (U3CCoPlayEngineU3Ec__Iterator7_t4126203997), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2204[4] = 
{
	U3CCoPlayEngineU3Ec__Iterator7_t4126203997::get_offset_of_U24this_0(),
	U3CCoPlayEngineU3Ec__Iterator7_t4126203997::get_offset_of_U24current_1(),
	U3CCoPlayEngineU3Ec__Iterator7_t4126203997::get_offset_of_U24disposing_2(),
	U3CCoPlayEngineU3Ec__Iterator7_t4126203997::get_offset_of_U24PC_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2205 = { sizeof (AdvCommand_t2859960984), -1, sizeof(AdvCommand_t2859960984_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2205[7] = 
{
	AdvCommand_t2859960984_StaticFields::get_offset_of_isEditorErrorCheck_0(),
	AdvCommand_t2859960984_StaticFields::get_offset_of_U3CIsEditorErrorCheckWaitTypeU3Ek__BackingField_1(),
	AdvCommand_t2859960984::get_offset_of_U3CRowDataU3Ek__BackingField_2(),
	AdvCommand_t2859960984::get_offset_of_U3CEntityDataU3Ek__BackingField_3(),
	AdvCommand_t2859960984::get_offset_of_U3CIdU3Ek__BackingField_4(),
	AdvCommand_t2859960984::get_offset_of_loadFileList_5(),
	AdvCommand_t2859960984::get_offset_of_U3CCurrentTreadU3Ek__BackingField_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2206 = { sizeof (AdvCommandError_t2072998418), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2207 = { sizeof (AdvCommandParser_t7201233), -1, sizeof(AdvCommandParser_t7201233_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2207[84] = 
{
	AdvCommandParser_t7201233_StaticFields::get_offset_of_OnCreateCustomCommandFromID_0(),
	AdvCommandParser_t7201233_StaticFields::get_offset_of_OnCreateCustomCommnadFromID_1(),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	AdvCommandParser_t7201233_StaticFields::get_offset_of_U3CU3Ef__switchU24map0_83(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2208 = { sizeof (CreateCustomCommandFromID_t3335294381), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2209 = { sizeof (AdvCustomCommandManager_t178514092), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2210 = { sizeof (AdvCommandBg_t1801149747), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2210[3] = 
{
	AdvCommandBg_t1801149747::get_offset_of_graphic_7(),
	AdvCommandBg_t1801149747::get_offset_of_layerName_8(),
	AdvCommandBg_t1801149747::get_offset_of_fadeTime_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2211 = { sizeof (AdvCommandBgEvent_t467224855), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2211[3] = 
{
	AdvCommandBgEvent_t467224855::get_offset_of_label_7(),
	AdvCommandBgEvent_t467224855::get_offset_of_graphic_8(),
	AdvCommandBgEvent_t467224855::get_offset_of_fadeTime_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2212 = { sizeof (AdvCommandBgEventOff_t719799962), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2212[1] = 
{
	AdvCommandBgEventOff_t719799962::get_offset_of_fadeTime_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2213 = { sizeof (AdvCommandBgOff_t856493800), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2213[1] = 
{
	AdvCommandBgOff_t856493800::get_offset_of_fadeTime_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2214 = { sizeof (AdvCommandCharacter_t305489079), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2214[3] = 
{
	AdvCommandCharacter_t305489079::get_offset_of_characterInfo_7(),
	AdvCommandCharacter_t305489079::get_offset_of_layerName_8(),
	AdvCommandCharacter_t305489079::get_offset_of_fadeTime_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2215 = { sizeof (AdvCommandCharacterOff_t3898132428), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2215[2] = 
{
	AdvCommandCharacterOff_t3898132428::get_offset_of_name_7(),
	AdvCommandCharacterOff_t3898132428::get_offset_of_time_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2216 = { sizeof (AdvCommandLayerOff_t1981748998), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2216[2] = 
{
	AdvCommandLayerOff_t1981748998::get_offset_of_name_7(),
	AdvCommandLayerOff_t1981748998::get_offset_of_fadeTime_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2217 = { sizeof (AdvCommandLayerReset_t698095516), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2217[1] = 
{
	AdvCommandLayerReset_t698095516::get_offset_of_name_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2218 = { sizeof (AdvCommandParticle_t2486419090), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2218[4] = 
{
	AdvCommandParticle_t2486419090::get_offset_of_label_7(),
	AdvCommandParticle_t2486419090::get_offset_of_layerName_8(),
	AdvCommandParticle_t2486419090::get_offset_of_graphic_9(),
	AdvCommandParticle_t2486419090::get_offset_of_graphicOperaitonArg_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2219 = { sizeof (AdvCommandParticleOff_t3489428065), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2219[1] = 
{
	AdvCommandParticleOff_t3489428065::get_offset_of_name_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2220 = { sizeof (AdvCommandSprite_t3266644903), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2220[4] = 
{
	AdvCommandSprite_t3266644903::get_offset_of_graphic_7(),
	AdvCommandSprite_t3266644903::get_offset_of_layerName_8(),
	AdvCommandSprite_t3266644903::get_offset_of_spriteName_9(),
	AdvCommandSprite_t3266644903::get_offset_of_fadeTime_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2221 = { sizeof (AdvCommandSpriteOff_t3298929962), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2221[2] = 
{
	AdvCommandSpriteOff_t3298929962::get_offset_of_name_7(),
	AdvCommandSpriteOff_t3298929962::get_offset_of_fadeTime_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2222 = { sizeof (AdvCommandText_t816833175), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2222[6] = 
{
	AdvCommandText_t816833175::get_offset_of_U3CIsPageEndU3Ek__BackingField_7(),
	AdvCommandText_t816833175::get_offset_of_U3CIsNextBrU3Ek__BackingField_8(),
	AdvCommandText_t816833175::get_offset_of_U3CPageCtrlTypeU3Ek__BackingField_9(),
	AdvCommandText_t816833175::get_offset_of_U3CVoiceFileU3Ek__BackingField_10(),
	AdvCommandText_t816833175::get_offset_of_U3CPageDataU3Ek__BackingField_11(),
	AdvCommandText_t816833175::get_offset_of_U3CIndexPageDataU3Ek__BackingField_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2223 = { sizeof (AdvCommandVideo_t218209391), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2223[8] = 
{
	AdvCommandVideo_t218209391::get_offset_of_isEndPlay_7(),
	AdvCommandVideo_t218209391::get_offset_of_cameraClearFlags_8(),
	AdvCommandVideo_t218209391::get_offset_of_cameraClearColor_9(),
	AdvCommandVideo_t218209391::get_offset_of_file_10(),
	AdvCommandVideo_t218209391::get_offset_of_label_11(),
	AdvCommandVideo_t218209391::get_offset_of_loop_12(),
	AdvCommandVideo_t218209391::get_offset_of_cancel_13(),
	AdvCommandVideo_t218209391::get_offset_of_cameraName_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2224 = { sizeof (AdvCommandElse_t2658593445), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2225 = { sizeof (AdvCommandElseIf_t976666238), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2225[1] = 
{
	AdvCommandElseIf_t976666238::get_offset_of_exp_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2226 = { sizeof (AdvCommandEndIf_t1575863694), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2227 = { sizeof (AdvCommandEndPage_t2960206982), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2228 = { sizeof (AdvCommandEndScenario_t3090793047), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2229 = { sizeof (AdvCommandEndSceneGallery_t3250141107), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2230 = { sizeof (AdvCommandEndSubroutine_t222520471), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2231 = { sizeof (AdvCommandEndThread_t574181939), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2232 = { sizeof (AdvCommandIf_t3367233681), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2232[1] = 
{
	AdvCommandIf_t3367233681::get_offset_of_exp_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2233 = { sizeof (AdvCommandJump_t3753877016), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2233[2] = 
{
	AdvCommandJump_t3753877016::get_offset_of_jumpLabel_7(),
	AdvCommandJump_t3753877016::get_offset_of_exp_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2234 = { sizeof (AdvCommandJumpRandom_t3877961971), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2234[3] = 
{
	AdvCommandJumpRandom_t3877961971::get_offset_of_jumpLabel_7(),
	AdvCommandJumpRandom_t3877961971::get_offset_of_exp_8(),
	AdvCommandJumpRandom_t3877961971::get_offset_of_expRate_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2235 = { sizeof (AdvCommandJumpRandomEnd_t2675963892), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2236 = { sizeof (AdvCommandJumpSubroutine_t3620058494), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2236[5] = 
{
	AdvCommandJumpSubroutine_t3620058494::get_offset_of_scenarioLabel_7(),
	AdvCommandJumpSubroutine_t3620058494::get_offset_of_subroutineCommandIndex_8(),
	AdvCommandJumpSubroutine_t3620058494::get_offset_of_jumpLabel_9(),
	AdvCommandJumpSubroutine_t3620058494::get_offset_of_returnLabel_10(),
	AdvCommandJumpSubroutine_t3620058494::get_offset_of_exp_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2237 = { sizeof (AdvCommandJumpSubroutineRandom_t1026002121), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2237[6] = 
{
	AdvCommandJumpSubroutineRandom_t1026002121::get_offset_of_scenarioLabel_7(),
	AdvCommandJumpSubroutineRandom_t1026002121::get_offset_of_subroutineCommandIndex_8(),
	AdvCommandJumpSubroutineRandom_t1026002121::get_offset_of_jumpLabel_9(),
	AdvCommandJumpSubroutineRandom_t1026002121::get_offset_of_returnLabel_10(),
	AdvCommandJumpSubroutineRandom_t1026002121::get_offset_of_exp_11(),
	AdvCommandJumpSubroutineRandom_t1026002121::get_offset_of_expRate_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2238 = { sizeof (AdvCommandJumpSubroutineRandomEnd_t1852355654), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2239 = { sizeof (AdvCommandPageControler_t3545276185), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2239[1] = 
{
	AdvCommandPageControler_t3545276185::get_offset_of_pageCtrlType_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2240 = { sizeof (AdvCommandParam_t1058287753), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2240[1] = 
{
	AdvCommandParam_t1058287753::get_offset_of_exp_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2241 = { sizeof (AdvCommandPauseScenario_t2854705820), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2242 = { sizeof (AdvCommandScenarioLabel_t2271667896), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2242[3] = 
{
	AdvCommandScenarioLabel_t2271667896::get_offset_of_U3CScenarioLabelU3Ek__BackingField_7(),
	AdvCommandScenarioLabel_t2271667896::get_offset_of_U3CTypeU3Ek__BackingField_8(),
	AdvCommandScenarioLabel_t2271667896::get_offset_of_U3CTitleU3Ek__BackingField_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2243 = { sizeof (ScenarioLabelType_t882757673)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2243[3] = 
{
	ScenarioLabelType_t882757673::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2244 = { sizeof (AdvCommandSelection_t2580391078), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2244[6] = 
{
	AdvCommandSelection_t2580391078::get_offset_of_jumpLabel_7(),
	AdvCommandSelection_t2580391078::get_offset_of_exp_8(),
	AdvCommandSelection_t2580391078::get_offset_of_selectedExp_9(),
	AdvCommandSelection_t2580391078::get_offset_of_prefabName_10(),
	AdvCommandSelection_t2580391078::get_offset_of_x_11(),
	AdvCommandSelection_t2580391078::get_offset_of_y_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2245 = { sizeof (AdvCommandSelectionClick_t477933170), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2245[5] = 
{
	AdvCommandSelectionClick_t477933170::get_offset_of_name_7(),
	AdvCommandSelectionClick_t477933170::get_offset_of_isPolygon_8(),
	AdvCommandSelectionClick_t477933170::get_offset_of_jumpLabel_9(),
	AdvCommandSelectionClick_t477933170::get_offset_of_exp_10(),
	AdvCommandSelectionClick_t477933170::get_offset_of_selectedExp_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2246 = { sizeof (AdvCommandSelectionClickEnd_t1108642495), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2247 = { sizeof (AdvCommandSelectionEnd_t1999221425), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2248 = { sizeof (AdvCommandAmbience_t767776376), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2248[5] = 
{
	AdvCommandAmbience_t767776376::get_offset_of_file_7(),
	AdvCommandAmbience_t767776376::get_offset_of_volume_8(),
	AdvCommandAmbience_t767776376::get_offset_of_isLoop_9(),
	AdvCommandAmbience_t767776376::get_offset_of_fadeInTime_10(),
	AdvCommandAmbience_t767776376::get_offset_of_fadeOutTime_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2249 = { sizeof (AdvCommandBgm_t3126670950), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2249[4] = 
{
	AdvCommandBgm_t3126670950::get_offset_of_file_7(),
	AdvCommandBgm_t3126670950::get_offset_of_volume_8(),
	AdvCommandBgm_t3126670950::get_offset_of_fadeInTime_9(),
	AdvCommandBgm_t3126670950::get_offset_of_fadeOutTime_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2250 = { sizeof (AdvCommandChangeSoundVolume_t1394903671), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2250[2] = 
{
	AdvCommandChangeSoundVolume_t1394903671::get_offset_of_groups_7(),
	AdvCommandChangeSoundVolume_t1394903671::get_offset_of_volume_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2251 = { sizeof (AdvCommandSe_t638350348), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2251[4] = 
{
	AdvCommandSe_t638350348::get_offset_of_label_7(),
	AdvCommandSe_t638350348::get_offset_of_file_8(),
	AdvCommandSe_t638350348::get_offset_of_volume_9(),
	AdvCommandSe_t638350348::get_offset_of_isLoop_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2252 = { sizeof (AdvCommandStopAmbience_t159697432), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2252[1] = 
{
	AdvCommandStopAmbience_t159697432::get_offset_of_fadeTime_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2253 = { sizeof (AdvCommandStopBgm_t1182512534), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2253[1] = 
{
	AdvCommandStopBgm_t1182512534::get_offset_of_fadeTime_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2254 = { sizeof (AdvCommandStopSe_t3930501764), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2254[2] = 
{
	AdvCommandStopSe_t3930501764::get_offset_of_label_7(),
	AdvCommandStopSe_t3930501764::get_offset_of_fadeTime_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2255 = { sizeof (AdvCommandStopSound_t2965519105), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2255[2] = 
{
	AdvCommandStopSound_t2965519105::get_offset_of_groups_7(),
	AdvCommandStopSound_t2965519105::get_offset_of_fadeTime_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2256 = { sizeof (AdvCommandStopVoice_t1849776858), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2256[1] = 
{
	AdvCommandStopVoice_t1849776858::get_offset_of_fadeTime_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2257 = { sizeof (AdvCommandVoice_t1582521554), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2257[4] = 
{
	AdvCommandVoice_t1582521554::get_offset_of_characterLabel_7(),
	AdvCommandVoice_t1582521554::get_offset_of_voiceFile_8(),
	AdvCommandVoice_t1582521554::get_offset_of_volume_9(),
	AdvCommandVoice_t1582521554::get_offset_of_isLoop_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2258 = { sizeof (AdvCommandHideMenuButton_t473930883), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2259 = { sizeof (AdvCommandHideMessageWindow_t2694955717), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2260 = { sizeof (AdvCommandShowMenuButton_t1122630226), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2261 = { sizeof (AdvCommandShowMessageWindow_t2854851764), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2262 = { sizeof (AdvCommandGuiActive_t2296013833), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2262[2] = 
{
	AdvCommandGuiActive_t2296013833::get_offset_of_name_7(),
	AdvCommandGuiActive_t2296013833::get_offset_of_isActive_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2263 = { sizeof (AdvCommandGuiPosition_t1553404504), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2263[3] = 
{
	AdvCommandGuiPosition_t1553404504::get_offset_of_name_7(),
	AdvCommandGuiPosition_t1553404504::get_offset_of_x_8(),
	AdvCommandGuiPosition_t1553404504::get_offset_of_y_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2264 = { sizeof (AdvCommandGuiReset_t3457969418), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2264[1] = 
{
	AdvCommandGuiReset_t3457969418::get_offset_of_name_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2265 = { sizeof (AdvCommandGuiSize_t3266247994), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2265[3] = 
{
	AdvCommandGuiSize_t3266247994::get_offset_of_name_7(),
	AdvCommandGuiSize_t3266247994::get_offset_of_x_8(),
	AdvCommandGuiSize_t3266247994::get_offset_of_y_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2266 = { sizeof (AdvCommandMessageWindowChangeCurrent_t1382911780), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2266[1] = 
{
	AdvCommandMessageWindowChangeCurrent_t1382911780::get_offset_of_name_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2267 = { sizeof (AdvCommandMessageWindowInit_t2380785243), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2267[1] = 
{
	AdvCommandMessageWindowInit_t2380785243::get_offset_of_names_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2268 = { sizeof (AdvCommandCaptureImage_t1169550925), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2268[4] = 
{
	AdvCommandCaptureImage_t1169550925::get_offset_of_objName_7(),
	AdvCommandCaptureImage_t1169550925::get_offset_of_cameraName_8(),
	AdvCommandCaptureImage_t1169550925::get_offset_of_layerName_9(),
	AdvCommandCaptureImage_t1169550925::get_offset_of_isWaiting_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2269 = { sizeof (AdvCommandMovie_t3101533308), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2269[5] = 
{
	AdvCommandMovie_t3101533308::get_offset_of_label_7(),
	AdvCommandMovie_t3101533308::get_offset_of_loop_8(),
	AdvCommandMovie_t3101533308::get_offset_of_cancel_9(),
	AdvCommandMovie_t3101533308::get_offset_of_waitTime_10(),
	AdvCommandMovie_t3101533308::get_offset_of_time_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2270 = { sizeof (AdvCommandThread_t4076899860), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2270[2] = 
{
	AdvCommandThread_t4076899860::get_offset_of_label_7(),
	AdvCommandThread_t4076899860::get_offset_of_name_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2271 = { sizeof (AdvCommandVibrate_t3795758989), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2272 = { sizeof (AdvCommandWait_t1737713393), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2272[2] = 
{
	AdvCommandWait_t1737713393::get_offset_of_time_7(),
	AdvCommandWait_t1737713393::get_offset_of_waitEndTime_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2273 = { sizeof (AdvCommandWaitBase_t4173154484), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2273[1] = 
{
	AdvCommandWaitBase_t4173154484::get_offset_of_U3CWaitTypeU3Ek__BackingField_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2274 = { sizeof (AdvCommandWaitCustom_t37803082), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2275 = { sizeof (AdvCommandWaitInput_t2267805615), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2275[2] = 
{
	AdvCommandWaitInput_t2267805615::get_offset_of_time_7(),
	AdvCommandWaitInput_t2267805615::get_offset_of_waitEndTime_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2276 = { sizeof (AdvCommandWaitThread_t120602403), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2276[2] = 
{
	AdvCommandWaitThread_t120602403::get_offset_of_label_7(),
	AdvCommandWaitThread_t120602403::get_offset_of_cancelInput_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2277 = { sizeof (AdvCommandEffectBase_t2092483048), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2277[2] = 
{
	AdvCommandEffectBase_t2092483048::get_offset_of_targetType_8(),
	AdvCommandEffectBase_t2092483048::get_offset_of_targetName_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2278 = { sizeof (AdvCommandFadeBase_t2761005413), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2278[3] = 
{
	AdvCommandFadeBase_t2761005413::get_offset_of_time_10(),
	AdvCommandFadeBase_t2761005413::get_offset_of_inverse_11(),
	AdvCommandFadeBase_t2761005413::get_offset_of_color_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2279 = { sizeof (U3COnStartEffectU3Ec__AnonStorey0_t1849549393), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2279[6] = 
{
	U3COnStartEffectU3Ec__AnonStorey0_t1849549393::get_offset_of_start_0(),
	U3COnStartEffectU3Ec__AnonStorey0_t1849549393::get_offset_of_end_1(),
	U3COnStartEffectU3Ec__AnonStorey0_t1849549393::get_offset_of_colorFade_2(),
	U3COnStartEffectU3Ec__AnonStorey0_t1849549393::get_offset_of_thread_3(),
	U3COnStartEffectU3Ec__AnonStorey0_t1849549393::get_offset_of_imageEffect_4(),
	U3COnStartEffectU3Ec__AnonStorey0_t1849549393::get_offset_of_U24this_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2280 = { sizeof (AdvCommandFadeIn_t934960843), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2281 = { sizeof (AdvCommandFadeOut_t3953198378), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2282 = { sizeof (AdvCommandImageEffect_t3775745660), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2283 = { sizeof (AdvCommandImageEffectBase_t67184963), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2283[4] = 
{
	AdvCommandImageEffectBase_t67184963::get_offset_of_animationName_10(),
	AdvCommandImageEffectBase_t67184963::get_offset_of_time_11(),
	AdvCommandImageEffectBase_t67184963::get_offset_of_U3CimageEffectTypeU3Ek__BackingField_12(),
	AdvCommandImageEffectBase_t67184963::get_offset_of_inverse_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2284 = { sizeof (U3COnStartEffectU3Ec__AnonStorey1_t3218082274), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2284[4] = 
{
	U3COnStartEffectU3Ec__AnonStorey1_t3218082274::get_offset_of_enableAnimation_0(),
	U3COnStartEffectU3Ec__AnonStorey1_t3218082274::get_offset_of_imageEffect_1(),
	U3COnStartEffectU3Ec__AnonStorey1_t3218082274::get_offset_of_thread_2(),
	U3COnStartEffectU3Ec__AnonStorey1_t3218082274::get_offset_of_U24this_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2285 = { sizeof (U3COnStartEffectU3Ec__AnonStorey0_t3218082275), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2285[4] = 
{
	U3COnStartEffectU3Ec__AnonStorey0_t3218082275::get_offset_of_start_0(),
	U3COnStartEffectU3Ec__AnonStorey0_t3218082275::get_offset_of_end_1(),
	U3COnStartEffectU3Ec__AnonStorey0_t3218082275::get_offset_of_fade_2(),
	U3COnStartEffectU3Ec__AnonStorey0_t3218082275::get_offset_of_U3CU3Ef__refU241_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2286 = { sizeof (AdvCommandImageEffectOff_t4223089517), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2287 = { sizeof (AdvCommandPlayAnimatin_t293841659), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2287[1] = 
{
	AdvCommandPlayAnimatin_t293841659::get_offset_of_animationName_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2288 = { sizeof (U3COnStartEffectU3Ec__AnonStorey0_t2144784675), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2288[2] = 
{
	U3COnStartEffectU3Ec__AnonStorey0_t2144784675::get_offset_of_thread_0(),
	U3COnStartEffectU3Ec__AnonStorey0_t2144784675::get_offset_of_U24this_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2289 = { sizeof (AdvCommandRuleFadeIn_t2011331537), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2289[1] = 
{
	AdvCommandRuleFadeIn_t2011331537::get_offset_of_data_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2290 = { sizeof (U3COnStartEffectU3Ec__AnonStorey0_t1786609133), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2290[2] = 
{
	U3COnStartEffectU3Ec__AnonStorey0_t1786609133::get_offset_of_thread_0(),
	U3COnStartEffectU3Ec__AnonStorey0_t1786609133::get_offset_of_U24this_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2291 = { sizeof (AdvCommandRuleFadeOut_t646159788), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2291[1] = 
{
	AdvCommandRuleFadeOut_t646159788::get_offset_of_data_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2292 = { sizeof (U3COnStartEffectU3Ec__AnonStorey0_t2081875224), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2292[2] = 
{
	U3COnStartEffectU3Ec__AnonStorey0_t2081875224::get_offset_of_thread_0(),
	U3COnStartEffectU3Ec__AnonStorey0_t2081875224::get_offset_of_U24this_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2293 = { sizeof (AdvCommandShake_t3565450354), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2294 = { sizeof (AdvCommandTween_t1326796191), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2294[1] = 
{
	AdvCommandTween_t1326796191::get_offset_of_tweenData_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2295 = { sizeof (U3COnStartEffectU3Ec__AnonStorey0_t2239811007), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2295[2] = 
{
	U3COnStartEffectU3Ec__AnonStorey0_t2239811007::get_offset_of_thread_0(),
	U3COnStartEffectU3Ec__AnonStorey0_t2239811007::get_offset_of_U24this_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2296 = { sizeof (AdvCommandZoomCamera_t2560452812), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2296[5] = 
{
	AdvCommandZoomCamera_t2560452812::get_offset_of_isEmptyZoom_10(),
	AdvCommandZoomCamera_t2560452812::get_offset_of_zoom_11(),
	AdvCommandZoomCamera_t2560452812::get_offset_of_isEmptyZoomCenter_12(),
	AdvCommandZoomCamera_t2560452812::get_offset_of_zoomCenter_13(),
	AdvCommandZoomCamera_t2560452812::get_offset_of_time_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2297 = { sizeof (U3COnStartEffectU3Ec__AnonStorey1_t3601202861), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2297[2] = 
{
	U3COnStartEffectU3Ec__AnonStorey1_t3601202861::get_offset_of_thread_0(),
	U3COnStartEffectU3Ec__AnonStorey1_t3601202861::get_offset_of_U24this_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2298 = { sizeof (U3COnStartEffectU3Ec__AnonStorey0_t2035118920), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2298[7] = 
{
	U3COnStartEffectU3Ec__AnonStorey0_t2035118920::get_offset_of_timer_0(),
	U3COnStartEffectU3Ec__AnonStorey0_t2035118920::get_offset_of_zoom0_1(),
	U3COnStartEffectU3Ec__AnonStorey0_t2035118920::get_offset_of_zoomTo_2(),
	U3COnStartEffectU3Ec__AnonStorey0_t2035118920::get_offset_of_center0_3(),
	U3COnStartEffectU3Ec__AnonStorey0_t2035118920::get_offset_of_centerTo_4(),
	U3COnStartEffectU3Ec__AnonStorey0_t2035118920::get_offset_of_camera_5(),
	U3COnStartEffectU3Ec__AnonStorey0_t2035118920::get_offset_of_U3CU3Ef__refU241_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2299 = { sizeof (AdvCommandSendMessage_t330862209), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2299[9] = 
{
	AdvCommandSendMessage_t330862209::get_offset_of_isWait_7(),
	AdvCommandSendMessage_t330862209::get_offset_of_name_8(),
	AdvCommandSendMessage_t330862209::get_offset_of_arg2_9(),
	AdvCommandSendMessage_t330862209::get_offset_of_arg3_10(),
	AdvCommandSendMessage_t330862209::get_offset_of_arg4_11(),
	AdvCommandSendMessage_t330862209::get_offset_of_arg5_12(),
	AdvCommandSendMessage_t330862209::get_offset_of_text_13(),
	AdvCommandSendMessage_t330862209::get_offset_of_voice_14(),
	AdvCommandSendMessage_t330862209::get_offset_of_voiceVersion_15(),
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
