﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using UnityEngine.EventSystems;

/// <summary>  
///   
/// </summary>  
//[Serializable]
public class SettingsObjectScript : MonoBehaviour {

    #region シーン追加読み込みによる表示・非表示

    static ToSettingScript callerObject; //Settingsシーンを生成したオブジェクト

    static public void ShowWithScene(ToSettingScript caller){
        callerObject = caller;
        callerObject.DisableAll();
        Application.LoadLevelAdditive("Settings");
    }

    static private bool BackToScene(){
        if (!callerObject) {
            return false;
        }
        SceneManager.UnloadSceneAsync("Settings");
        callerObject.EnableAll();
        callerObject = null;
        return true;
    }

    public void OnPointerDown(BaseEventData data){
        
    }

    #endregion

	#region 変数、プロパティ

    public bool peripheralConnected;

    public Transform scrollContent;
    public Transform scrollCell;

    public Text scanButtonLabel;
    public Text scanStatusLabel;
    public Text logLabel;
    public Slider bgmVolumeSlider;
    public Text bgVolumeLabel;

    bool isScanning = false;

	#endregion

	#region ライフサイクルメソッド

	// Use this for initialization
	protected void Awake() {
	}

	// Use this for initialization
	protected void Start() {
        scrollCell.gameObject.SetActive(false);
        if (JinsBridgeScript.Instance != null) {
            bgmVolumeSlider.value = JinsBridgeScript.Instance.bgmVolume;
            bgVolumeLabel.text = string.Format("{0:0.0}", bgmVolumeSlider.value);
        }

        JinsBridgeScript._StopDataReportPeripheral();
        JinsBridgeScript._StartScanPeripherals();
	}

	// Update is called once per frame
	public void OnUpdate(float dt) {
	}

	#endregion

	#region スキャン

    //スキャン開始
    public void startScanPeripheral(){
        JinsBridgeScript._StartScanPeripherals(); //OnScanStarted() がコールバックされる
    }

    //スキャン終了
    public void stopScanPeripheral(){
        JinsBridgeScript._StopScanPeripherals();
        clearFoundPeripheral();
        scanStatusLabel.text = "not scanning";
        scanButtonLabel.text = "Start Scan";
        isScanning = false;
    }

    public void OnScanStarted(){
        scanStatusLabel.text = "Scanning...";
        scanButtonLabel.text = "Stop Scan";
        isScanning = true;
    }

    #endregion

    #region ボタン

    //スキャンボタン
    public void scanButton(){
        if (isScanning) {
            stopScanPeripheral();
        }
        else {
            startScanPeripheral();
        }
    }

    //機器選択
    public void connectPeripheral(Button button){
        string uuid = button.name;
        JinsBridgeScript._ConnectPeripheralWithId(uuid);
        peripheralConnected = true;

        //選んだセルだけ黄色に
        foreach (Transform child in scrollContent) {
            var b = child.GetComponent<Button>();
            b.image.color = new Color(1, 1, 1);
        }
        button.image.color = new Color(1, 1, 0);
    }

    //ゲーム開始
    public void gameStart(){
        if (!peripheralConnected) {
            log("機器が接続されていません");
            return;
        }

        JinsBridgeScript._StopScanPeripherals();
        JinsBridgeScript._StartDataReportPeripheral();
        SceneManager.LoadScene("TalkScene");
    }

    //選択画面へ
    public void gotoSelect(){
        if (!peripheralConnected) {
            log("機器が接続されていません");
            return;
        }

        JinsBridgeScript._StopScanPeripherals();
        JinsBridgeScript._StartDataReportPeripheral();
        SceneManager.LoadScene("Select");
    }

    //タイトル画面へ
    public void backToTitle(){
        if (!peripheralConnected) {
            log("機器が接続されていません");
            return;
        }

        JinsBridgeScript._StopScanPeripherals();
        JinsBridgeScript._StartDataReportPeripheral();
        SceneManager.LoadScene("Launch");
    }

    //追加読み込みしたSettingsシーンを削除する
    public void closeScene(){
        if (!peripheralConnected) {
            log("機器が接続されていません");
            return;
        }

        JinsBridgeScript._StopScanPeripherals();
        JinsBridgeScript._StartDataReportPeripheral();

        bool ret = BackToScene();
        if (!ret){
            Debug.Log("cannot back to prev scene. this scene was not loaded by additive!");
        }
    }

	#endregion

    #region スクロールビュー

    //機器追加
    public void addFoundPeripheral(string uuid) {
        //新しいセル作成
        var newCell = Instantiate(scrollCell.gameObject, scrollContent);
        newCell.GetComponentInChildren<Text>().text = uuid;
        newCell.name = uuid;
        newCell.SetActive(true);

        //座標計算
        int cellCount = scrollContent.childCount;
        Rect cellRect = scrollCell.GetComponent<RectTransform>().rect;
        Rect scrollRect = scrollContent.GetComponent<RectTransform>().rect;
        int margin = 20;
        Vector3 pos = new Vector3(scrollRect.center.x, -scrollRect.center.y - (cellRect.height + Mathf.Abs(cellRect.y)) * cellCount);
        newCell.transform.localPosition = pos;
        newCell.transform.localRotation = Quaternion.identity;
    }

    //全消し
    public void clearFoundPeripheral(){
        foreach (Transform child in scrollContent.transform) {
            if (child.name.IndexOf("Template") < 0) {
                Destroy(child.gameObject);
            }
        }
    }

    #endregion

    #region スライダー

    //bgm
    public void bgmVolumeSliderValueChanged(Slider sender){
        JinsBridgeScript.Instance.bgmVolume = sender.value;
        bgVolumeLabel.text = string.Format("{0:0.0}", sender.value);
    }

    #endregion

    #region

    public void log(string logText) {
        if (!logText.EndsWith("\n")) {
            logText += "\n";
        }
        logLabel.text = logText + logLabel.text;
    }

    #endregion
}

