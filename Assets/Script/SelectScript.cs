﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class SelectScript : MonoBehaviour {

    public Button startButton;
    public Button backButton;

    public GameObject slider;
    public GameObject girl;
    public GameObject boy;

    public AudioClip girlSelectSe;
    public AudioClip girlStartSe;
    public AudioClip boySelectSe;
    public AudioClip boyStartSe;
    private AudioSource girlAudio;
    private AudioSource boyAudio;

    public ToSettingScript toSettings;

    enum Type{
        NONE,
        GIRL,
        BOY,
    };

    Type chara;
    bool touchEnabled = true;

    Vector3 girlPos;
    Vector3 boyPos;

	// Use this for initialization
	void Start () {
        girlPos = girl.transform.position;
        boyPos = boy.transform.position;

        girlAudio = girl.GetComponent<AudioSource>();
        boyAudio = boy.GetComponent<AudioSource>();
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void TapGirl(){
        if (chara != Type.NONE || !touchEnabled) {
            return;
        }

        chara = Type.GIRL;
        StartCoroutine(ShowGirl());
        girlAudio.PlayOneShot(girlSelectSe);
    }

    public void TapBoy(){
        if (chara != Type.NONE || !touchEnabled) {
            return;
        }

        chara = Type.BOY;
        StartCoroutine(ShowBoy());
        boyAudio.PlayOneShot(boySelectSe);
    }

    public void Back(){
        if (chara == Type.NONE || !touchEnabled) {
            return;
        }

        chara = Type.NONE;
        StartCoroutine(ShowStay());
        girlAudio.Stop();
        boyAudio.Stop();
    }

    IEnumerator ShowGirl(){
        touchEnabled = false;
        yield return new WaitForSeconds(0.01f);

        float sec = 0.5f;
        iTween.MoveTo(girl, Vector3.zero, sec);
        iTween.MoveBy(boy, new Vector3(4, 0, 0), sec);
        iTween.MoveBy(slider, new Vector3(0, 3, 0), sec);
        yield return new WaitForSeconds(sec);
        touchEnabled = true;
    }

    IEnumerator ShowBoy(){
        touchEnabled = false;
        yield return new WaitForSeconds(0.01f);

        float sec = 0.5f;
        iTween.MoveTo(boy, Vector3.zero, sec);
        iTween.MoveBy(girl, new Vector3(-4, 0, 0), sec);
        iTween.MoveBy(slider, new Vector3(0, 3, 0), sec);
        yield return new WaitForSeconds(sec);
        touchEnabled = true;
    }

    IEnumerator ShowStay(){
        touchEnabled = false;
        float sec = 0.5f;
        iTween.MoveTo(girl, girlPos, sec);
        iTween.MoveTo(boy, boyPos, sec);
        yield return new WaitForSeconds(0.05f);

        iTween.MoveTo(slider, Vector3.zero, sec*1.5f);
        yield return new WaitForSeconds(sec*1.5f);
        touchEnabled = true;
    }


    public void GameStart(){
        if (!touchEnabled){
            return;
        }

        if (!JinsBridgeScript.Instance.peripheralConnected) {
            //SceneManager.LoadScene("Settings");
            SettingsObjectScript.ShowWithScene(toSettings);
            return;
        }

        touchEnabled = false;
        startButton.enabled = false;
        backButton.enabled = false;

        if (chara == Type.GIRL) {
            girlAudio.Stop();
            girlAudio.PlayOneShot(girlStartSe);
        }
        else if (chara == Type.BOY) {
            boyAudio.Stop();
            boyAudio.PlayOneShot(boyStartSe);
        }

        JinsBridgeScript.Instance.sceneName = (chara == Type.BOY ? "Start" : "MIKO");

        StartCoroutine(gotoNextScene());
    }

    IEnumerator gotoNextScene(){
        yield return new WaitForSeconds(1.5f);

        JinsBridgeScript._StartDataReportPeripheral();
        //SceneManager.LoadScene("TalkScene");
        FadeManager.Instance.LoadScene("White2Black", 2f); //se再生が終わるまで
    }


}
