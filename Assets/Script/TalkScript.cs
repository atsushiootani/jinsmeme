﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Utage;
using UnityEngine.SceneManagement;
using System;

public class TalkScript : MonoBehaviour {

    //utage
    AdvEngine Engine { get { return engine ?? (engine = FindObjectOfType<AdvEngine>()); } }
    public AdvEngine engine;

    //jins parameter
    public Text dereValue;
    public Text kyoroValue;
    public Text relaxValue;
    public RawImage dereGauge;
    public RawImage kyoroGauge;
    public RawImage relaxGauge;

    public AdvConfig config;


    void Start(){
        //セーブデータを削除。選択肢表示のため
        //Engine.SystemSaveData.Delete();


        StartCoroutine( CoTalk() );
    }

    IEnumerator CoTalk()
    {
        //「宴」のシナリオを呼び出す
        string scenarioLabel = JinsBridgeScript.Instance.sceneName;
        Engine.JumpScenario( scenarioLabel );
        config.BgmVolume = JinsBridgeScript.Instance.bgmVolume;

        //「宴」のシナリオ終了待ち
        while(!Engine.IsEndScenario)
        {
            yield return 0;
        }

        FadeManager.Instance.fadeColor = new Color(0, 0, 0);
        FadeManager.Instance.LoadScene("ToBeContinued", 1.0f);
    }

    void Update(){
        //JINSパラメータの表示
        int dere = (int)JinsBridgeScript.Instance.dere;
        int kyoro = (int)JinsBridgeScript.Instance.kyoro;
        int relax = (int)JinsBridgeScript.Instance.relax;

        dereValue.text = dere.ToString();
        kyoroValue.text = kyoro.ToString();
        relaxValue.text = relax.ToString();

        dereGauge.color = getColor(dere);
        dereGauge.rectTransform.localScale = getScale(dere);
        kyoroGauge.color = getColor(kyoro);
        kyoroGauge.rectTransform.localScale = getScale(kyoro);
        relaxGauge.color = getColor(relax);
        relaxGauge.rectTransform.localScale = getScale(relax);

        engine.Param.TrySetParameter("dere", dere);
        engine.Param.TrySetParameter("kyoro", kyoro);
        engine.Param.TrySetParameter("relax", relax);
        config.BgmVolume = JinsBridgeScript.Instance.bgmVolume;
    }

    enum Eval{
        BAD,
        NORMAL,
        GOOD,
    };
    const int DIV_1 = 30;
    const int DIV_2 = 65;
    const int MARGIN = 5;

    Eval getEval(double value){
        if (value < DIV_1) {
            return Eval.BAD;
        }
        else if (value < DIV_2) {
            return Eval.NORMAL;
        }
        else {
            return Eval.GOOD;
        }
    }

    public Color c1 = Color.red;
    public Color c2 = Color.yellow;
    public Color c3 = Color.blue;
    Color getColor(float value){

        if (value < DIV_1 - MARGIN) {
            return c1;
        }
        else if (value < DIV_1 + MARGIN) {
            float t = (value - (DIV_1 - MARGIN)) / (MARGIN * 2);
            return new Color(
                c1.r * (1 - t) + c2.r * t,
                c1.g * (1 - t) + c2.g * t,
                c1.b * (1 - t) + c2.b * t);
        }
        else if (value < DIV_2 - MARGIN) {
            return c2;
        }
        else if (value < DIV_2 + MARGIN) {
            float t = (value - (DIV_2 - MARGIN)) / (MARGIN * 2);
            return new Color(
                c2.r * (1 - t) + c3.r * t,
                c2.g * (1 - t) + c3.g * t,
                c2.b * (1 - t) + c3.b * t);
        }
        else {
            return c3;
        }

        /*
        if (getEval(value) == Eval.BAD) {
            return Color.red;
        }
        else if (getEval(value) == Eval.NORMAL) {
            return Color.yellow;
        }
        else {
            return Color.blue;
        }*/
    }
    Vector3 getScale(float value){
        return new Vector3(value / 100.0f, value / 100.0f, value / 100.0f);
    }
}
