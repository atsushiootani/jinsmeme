﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ToSettingScript : MonoBehaviour {

    public GameObject[] disableObjectList;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void ToSetting(){
        //SceneManager.LoadScene("Settings");
        SettingsObjectScript.ShowWithScene(this);
    }

    public void EnableAll(){
        foreach (var obj in disableObjectList) {
            if (obj)
                obj.SetActive(true);
        }
    }
    public void DisableAll(){
        foreach (var obj in disableObjectList) {
            if (obj)
                obj.SetActive(false);
        }
    }
}
