﻿using System;
using System.Collections;
using System.Runtime.InteropServices;
using UnityEngine;
using UnityEngine.UI;
using Live2D.Cubism.Core;
using System.Diagnostics;
using Utage;

/// <summary>  
///   
/// </summary>  
//[Serializable]
public class JinsBridgeScript : MonoBehaviour {

    #region persistent object

    const string OBJECT_NAME = "JinsBridge";
    public static JinsBridgeScript Instance { get; private set; }

    [RuntimeInitializeOnLoadMethod(RuntimeInitializeLoadType.BeforeSceneLoad)]
    static private void CreatePersistentObjectInScene(){
        GameObject obj = new GameObject(OBJECT_NAME);
        obj.AddComponent<JinsBridgeScript>();
        DontDestroyOnLoad(obj);
        Instance = obj.GetComponent<JinsBridgeScript>();
    }

    public void Start(){
        if (Instance != this) {
            Destroy(gameObject);
        }
    }


    #endregion

    #region Unity -> Native

#if UNITY_EDITOR
    //スキャン開始・終了
    public static void _StartScanPeripherals(){
        Instance.OnScanStarted();
    }
    public static void _StopScanPeripherals(){
    }

    //機器との接続・切断
    public static void _ConnectPeripheralWithId(string uuid){
        Instance.OnPeripheralConnected(uuid);
    }
    public static void _DisconnectPeripheral(){
        Instance.OnPeripheralDisconnected();
    }

    //通信開始・終了
    public static void _StartDataReportPeripheral(){
        Instance.toReportData = true;
    }
    public static void _StopDataReportPeripheral(){
        Instance.toReportData = false;
    }

    //テスト用
    private bool toReportData;
    static int vv = 0;
    public void TestUpdateData(){
        if (toReportData) {
            OnUpdateDere(UnityEngine.Random.Range(0.0f, 100.0f).ToString());
            OnUpdateKyoro(UnityEngine.Random.Range(0.0f, 100.0f).ToString());
            OnUpdateRelax(UnityEngine.Random.Range(0.0f, 100.0f).ToString());
            //OnUpdateDere("99");
            //OnUpdateKyoro("99");
            //OnUpdateRelax("99");
            vv = (vv+1) % 100;
            OnUpdateDere( (vv).ToString());
            OnUpdateKyoro((vv).ToString());
            OnUpdateRelax((vv).ToString());
        }
    }

#else
    //スキャン開始・終了
    [DllImport("__Internal")]
    public static extern IntPtr _StartScanPeripherals();
    [DllImport("__Internal")]
    public static extern IntPtr _StopScanPeripherals();

    //機器との接続・切断
    [DllImport("__Internal")]
    public static extern IntPtr _ConnectPeripheralWithId(string uuid);
    [DllImport("__Internal")]
    public static extern IntPtr _DisconnectPeripheral();

    //通信開始・終了
    [DllImport("__Internal")]
    public static extern IntPtr _StartDataReportPeripheral();
    [DllImport("__Internal")]
    public static extern IntPtr _StopDataReportPeripheral();

#endif
    #endregion //Unity -> Native


    #region Native -> Unity

    public void OnPrepared(){
        
    }
    public void OnScanStarted(){
        if (getSettings()) {
            getSettings().OnScanStarted();
        }
    }
    public void OnPeripheralFound(string uuid){
        if (getSettings()) {
            getSettings().addFoundPeripheral(uuid);
        }
    }
    public void OnPeripheralConnected(string uuid){
        peripheralConnected = true;
        if (getSettings()) {
            getSettings().peripheralConnected = true;
            getSettings().log("接続しました: " + uuid);
        }
    }
    public void OnPeripheralDisconnected(){
        peripheralConnected = false;
        if (getSettings()) {
            getSettings().peripheralConnected = false;
            getSettings().log("切断しました");
        }
    }
    public void OnErrorOccured(){
        peripheralConnected = false;
    }

    public void OnUpdateDere(string str) {
        dere = parseValue(str) ?? dere;
    }
    public void OnUpdateKyoro(string str) {
        kyoro = parseValue(str) ?? kyoro;
    }
    public void OnUpdateRelax(string str) {
        relax = parseValue(str) ?? relax;
    }


    #endregion //Native -> Unity


    #region variable

    public double dere { get; private set; }
    public double kyoro { get; private set; }
    public double relax { get; private set; }

    public bool peripheralConnected { get; private set; }

    public string sceneName = "Start"; //live2dのシーン名
    public float bgmVolume = 0.4f;

    #endregion //variable


    #region life cycle

    public void Update(){
#if UNITY_EDITOR
        TestUpdateData();
#endif
    }

    #endregion //life cycle


    #region util

    public SettingsObjectScript getSettings(){
        var obj = GameObject.Find("SettingsObject");
        if (!obj) {
            return null;
        }
        return obj.GetComponent<SettingsObjectScript>();
    }

    private double? parseValue(string str){
        try {
            return double.Parse(str);
        } catch {
            return null;
        }
    }

    #endregion // util
}
