﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TransitionScript : MonoBehaviour {

    public string toSceneName;
    public float waitTime = 3.0f;
    public float fadeTime = 1.0f;

	// Use this for initialization
	void Start () {
        StartCoroutine(Transition());
	}

    IEnumerator Transition(){
        yield return new WaitForSeconds(waitTime);

        FadeManager.Instance.fadeColor = new Color(1, 1, 1);
        FadeManager.Instance.LoadScene(toSceneName, fadeTime);
    }
	
	// Update is called once per frame
	void Update () {
		
	}
}
