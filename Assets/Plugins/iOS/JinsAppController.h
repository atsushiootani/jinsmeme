//
//  JinsAppController.h
//  Unity-iPhone
//
//  Created by OtaniAtsushi1 on 2017/11/23.
//

#ifndef JinsAppController_h
#define JinsAppController_h

#import "UnityAppController.h"

@interface JinsAppController : UnityAppController

@property (nonatomic, assign) float kkd;   //好感度
@property (nonatomic, assign) float kyoro; //キョロキョロ度
@property (nonatomic, assign) float relax; //リラックス度
@property (nonatomic, assign) float relax_m1;
@property (nonatomic, assign) float relax_tmp;
@property (nonatomic, strong) NSMutableArray* relaxBuf;

@property (nonatomic, strong) NSMutableArray *peripheralsFound; //発見したデバイス一覧

/*- (void) memeAppAuthorized:(NSNotification *) notification;
- (void) memePeripheralFound: (NSNotification *) notification;
- (void) memePeripheralConnected: (NSNotification *) notification;
- (void) memePeripheralDisconnected: (NSNotification *) notification;
- (void) memeRealTimeModeDataReceived: (NSNotification *) notification;
- (void) notificationReceived: (NSNotification *) notification;
*/


//スキャン開始・終了
//  機器を発見すると、OnPeripheralFound(uuid) が呼び出される
- (void) startScanPeripherals;
- (void) stopScanPeripherals;


//機器との接続・切断
//  接続完了すると、OnPeripheralConnected() が呼び出される。接続が切れると、OnPeripheralDisconnected() が呼び出される
- (void) connectPeripheralWithId:(NSString*) uuid;
- (void) disconnectPeripheral;


//通信開始・終了
//  データが届くたびに Update**(value) が呼び出される
- (void) startDataReportPeripheral;
- (void) stopDataReportPeripheral;


@end



#endif /* JinsAppController_h */
