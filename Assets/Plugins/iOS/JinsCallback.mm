#include "JinsCallback.h"
#include "JinsAppController.h"

JinsAppController* getGameInstance(){
    return (JinsAppController*)[UIApplication sharedApplication].delegate;
}


//スキャン開始・終了
void _StartScanPeripherals(){
    [getGameInstance() startScanPeripherals];
}
void _StopScanPeripherals(){
    [getGameInstance() stopScanPeripherals];
}

//機器との接続・切断
void _ConnectPeripheralWithId(const char* uuid){
    [getGameInstance() connectPeripheralWithId:@(uuid)];
}
void _DisconnectPeripheral(){
    [getGameInstance() disconnectPeripheral];
}


//通信開始・終了
void _StartDataReportPeripheral(){
    [getGameInstance() startDataReportPeripheral];
}
void _StopDataReportPeripheral(){
    [getGameInstance() stopDataReportPeripheral];
}


void _JinsUpdate(const char* name){}

