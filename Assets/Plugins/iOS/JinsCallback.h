#ifdef __cplusplus
extern "C" {
#endif

    //スキャン開始・終了
    void _StartScanPeripherals();
    void _StopScanPeripherals();
    
    //機器との接続・切断
    void _ConnectPeripheralWithId(const char* uuid);
    void _DisconnectPeripheral();
    
    
    //通信開始・終了
    void _StartDataReportPeripheral();
    void _StopDataReportPeripheral();
    
void _JinsUpdate(const char* name);
#ifdef __cplusplus
}
#endif
