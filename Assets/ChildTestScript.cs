﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[ExecuteInEditMode]
public class ChildTestScript : MonoBehaviour {
    [SerializeField]
    public bool show;

	// Use this for initialization
	void Start () {
	}
	
	// Update is called once per frame
	void Update () {
        Debug.Log("Update" + gameObject.name);
        if (show) {
            ChildTestScript[] children = GetComponentsInChildren<ChildTestScript>();
            foreach (var child in children){
                Debug.Log(gameObject.name + " " + child.gameObject.name);
            }
        }
	}
}
